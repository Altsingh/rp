import { browser, element, by } from 'protractor';

export class AltRecruitPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('alt-root h1')).getText();
  }
}