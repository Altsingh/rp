export const environment = {
  production: true,
  inferURL: 'https://staging-infer.peoplestrongalt.com',
  chatUserSSOLoginURL: 'https://staging-hrms.peoplestrong.com/service/jinie/chatUserSSOLogin',
  altmessengerURL: 'https://altmessenger.peoplestrong.com/messenger',
  jdCvApiUrl: 'https://staging-jdcv.peoplestrongalt.com/Match_Candidates/',
  socialMatchApiUrl : 'https://staging-j2c.peoplestrongalt.com',
  xoxoIntegration : false,
  demoURL: 'altrecruit.peoplestrong.com',
  professionalURL: 'staging-professionalrecruit.peoplestrongalt.com',
  autoMatchDisabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  hiringTeamDisabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  bulkParserEnabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  meshUrl: 'https://staging-hrms-admin.peoplestrong.com/services/apps'
  
};
