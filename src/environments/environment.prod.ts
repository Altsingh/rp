export const environment = {
  production: true,
  inferURL: 'https://infer.peoplestrong.com',
  chatUserSSOLoginURL: 'https://hrms.peoplestrong.com/service/jinie/chatUserSSOLogin',
  altmessengerURL: 'https://altmessenger.peoplestrong.com/messenger',
  jdCvApiUrl: 'https://jd-cv.peoplestrong.com/Match_Candidates/',
  socialMatchApiUrl : 'https://j2c.peoplestrong.com',
  xoxoIntegration : false,
  demoURL: 'altrecruit.peoplestrong.com',
  professionalURL: 'professionalrecruit.peoplestrongalt.com',
  autoMatchDisabledHosts: [ 'professionalrecruit.peoplestrong.com', 'professionalrecruit.peoplestrongalt.com', 'starterrecruit.peoplestrong.com', 'starterrecruit.peoplestrongalt.com' ],
  hiringTeamDisabledHosts: [ 'professionalrecruit.peoplestrong.com', 'professionalrecruit.peoplestrongalt.com', 'starterrecruit.peoplestrong.com', 'starterrecruit.peoplestrongalt.com' ],
  bulkParserEnabledHosts: [ 'professionalrecruit.peoplestrong.com', 'professionalrecruit.peoplestrongalt.com', 'altrecruit.peoplestrong.com', 'altrecruit.peoplestrongalt.com' ],
  meshUrl: 'https://hrms-admin.peoplestrong.com/services/apps'
};
