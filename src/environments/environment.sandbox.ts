export const environment = {
  production: true,
  inferURL: 'https://infer.peoplestrong.com',
  chatUserSSOLoginURL: 'https://hrms.peoplestrong.com/service/jinie/chatUserSSOLogin',
  altmessengerURL: 'https://altmessenger.peoplestrong.com/messenger',
  jdCvApiUrl: 'https://jd-cv.peoplestrong.com/Match_Candidates/',
  socialMatchApiUrl : 'https://j2c.peoplestrong.com',
  xoxoIntegration : true,
  demoURL: 'recruitment.peoplestrong.com',
  professionalURL: 'professionalrecruit.peoplestrongalt.com',
  autoMatchDisabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  hiringTeamDisabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  bulkParserEnabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  meshUrl: 'https://hrms-admin.peoplestrong.com/services/apps'
  
};
