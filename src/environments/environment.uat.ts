export const environment = {
  production: true,
  inferURL: 'https://uat-infer.peoplestrong.com',
  chatUserSSOLoginURL: 'https://uat-hrms.peoplestrong.com/service/jinie/chatUserSSOLogin',
  altmessengerURL: 'https://altmessenger.peoplestrong.com/messenger',
  jdCvApiUrl: 'https://uat-jdcv.peoplestrongalt.com/Match_Candidates/',
  socialMatchApiUrl : 'https://uat-j2c.peoplestrongalt.com',
  xoxoIntegration : false,
  demoURL: 'uat-altrecruit.peoplestrong.com',
  professionalURL: 'uat-professionalrecruit.peoplestrongalt.com',
  autoMatchDisabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  hiringTeamDisabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  bulkParserEnabledHosts: [ 'mpeoplerecruit.sohum.com' ],
  meshUrl: 'https://uat-hrms-admin.peoplestrong.com/services/apps'
};
