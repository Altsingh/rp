// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  inferURL: 'https://staging-infer.peoplestrongalt.com',
  chatUserSSOLoginURL: 'https://hrms.sohum.com/service/jinie/chatUserSSOLogin',
  altmessengerURL: 'https://altmessenger.peoplestrong.com/messenger',
  jdCvApiUrl: 'https://sohum-jdcv.peoplestrongalt.com/Match_Candidates/',
  socialMatchApiUrl : 'https://sohum-j2c.peoplestrongalt.com',
  xoxoIntegration : false,
  demoURL: 'hiredplus.sohum.com',
  professionalURL: 'professionalrecruit.sohum.com',
  autoMatchDisabledHosts: [ 'mpeoplerescruit.sohum.com' ],
  hiringTeamDisabledHosts: [ 'mpeoplesrecruit.sohum.com' ],
  bulkParserEnabledHosts: [ 'mpeoplserecruit.sohum.com' ],
  meshUrl: 'https://hrms-admin.sohum.com/services/apps'
};
