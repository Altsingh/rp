import { Component, OnInit } from '@angular/core';
import { ReportGenerationTo } from '../../common/report-generation-to';
import { SessionService } from '../../session.service';
import { DialogService } from '../../shared/dialog.service';
import { generate } from 'rxjs/observable/generate';
import { ReportListService } from './report-list.service';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { DatePipe } from '@angular/common';
import { MatDialogRef } from '@angular/material';
import { SelectItem } from '../../shared/select-item';
import { MessageCode } from '../../common/message-code';

@Component({
  selector: 'alt-report-generation',
  templateUrl: './report-generation.component.html',
  styleUrls: ['./report-generation.component.css'],
  providers: [ReportListService, DatePipe]
})
export class ReportGenerationComponent implements OnInit {

  filterList: any;
  reportData: any;
  showLoader: Boolean = false;
  message: String;
  severity:String;
  timer: any;
  severityClass:String;  
  label : String = 'SUBMIT';
  generated : boolean = false;




  constructor(
    private sessionService: SessionService,
    private dialogService: DialogService,
    private reportListService: ReportListService,
    private notificationService: NotificationService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<ReportGenerationComponent>
  ) { }

  ngOnInit() {
    this.generateReportPlaceholder();
  }

  generateReportPlaceholder() {
    this.showLoader = true;
    let generateReport: GenerateReport = new GenerateReport();

    if (generateReport.input == null) {
      generateReport.input = new ReportGenerationTo();
    }

    generateReport.input.reportId = this.sessionService.object.reportID;
    generateReport.input.reportDescription = this.sessionService.object.reportDescription;
    
    this.reportListService.generateReportPlaceholders(generateReport).subscribe(response => {
      
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          
          this.filterList = response.response.altReportParamTOList;
          this.reportData = response.response;
          this.showLoader = false;

        }
      }
      else {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Some error occured while fetching filters list");
        this.showLoader = false;
      }
    });

  }

  generateReport() {
    
    let generateReport: GenerateReport = new GenerateReport();

    if (generateReport.input == null) {
      generateReport.input = new ReportGenerationTo();
      generateReport.input = this.reportData;
    }
    
    let valid = this.validateReportPlaceholders(true);

    if(valid == true)
    {
    this.generated = true;
    this.reportListService.generateReport(generateReport).subscribe(response => {
      let messageTO: MessageCode = response.messageCode;
      let message: string = '';
      if (response != null && messageTO != null && messageTO.code != null) {
        
        if (messageTO.code == "EC200") {
          this.generated = false;
          message = messageTO.message.toString();
          this.notificationService.addMessage("INFO", generateReport.input.reportName + " has been downloaded successfully,You can find them in Downloads Folder");
          this.dialogRef.close();
        }
        else if( messageTO.message != null)
        {
          this.generated = false;
          message = messageTO.message.toString();
          this.setMessage("WARNING",message);
        }
        else
        {
          this.generated = false;
          message = "Request failed to execute!";
          this.setMessage("WARNING",message);
        }
      }
      
    }, (onError) => {
      this.generated = false;
      this.setMessage("WARNING",'Something went wrong. Please contact your System Admin.');
    }, () => {
    });
  }

  else if(valid == false)
  {
    this.generated = false;
  }


  }

  dateChangeListener(event, data) {
    //dd-MMM-yyyy
    let date: Date = event.value;
    this.datePipe.transform(date, "dd-MMM-yyyy");
    data.selectedDate = this.datePipe;
    data.selectedDate = date;
  }

  onSelectedList(data, event) {

    let temp = data.selectedMultiSelectOptions;
    if (temp == null) {
      temp = [];
    }

    temp.push(event);

    data.selectedMultiSelectOptions = temp;
  }

  onDeselectedList(data, event) {

    let multiSelectList = [];

    for (let x of data.options) {
      if (x.value != event.value) {

        multiSelectList.push(x);
      }
    }
    data.selectedMultiSelectOptions = multiSelectList;
  }

  public setMessage(severity: string, message: string) {
    this.severity = severity;
    this.message = message;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);

  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }


  validateReportPlaceholders(valid : boolean) : boolean
  {

    for(let obj of this.reportData.altReportParamTOList){
      if( obj.showtextBox == true && obj.mandatory == true && obj.selectedText == null)
      {
        valid = false;
        this.setMessage("WARNING", obj.paramName + " is required.");
        return;
  
      }
      else if( obj.showDateCalendar == true && obj.mandatory == true && obj.selectedDate == null)
      {
        valid = false;
        this.setMessage("WARNING",obj.paramName + " is required.");
        return;
  
      }
      else if( obj.showSingleSelectMenu == true && obj.mandatory == true && obj.selectedOption == null)
      {
        valid = false;
        this.setMessage("WARNING",obj.paramName + " is required.");
        return;
  
      }
      else if( obj.showMultiSelectMenu == true && obj.mandatory == true && obj.selectedMultiSelectOptions == null)
      {
        valid = false;
        this.setMessage("WARNING", obj.paramName + " is required.");
        return;
  
      }
      
  }
  return valid;


  }

}

export class GenerateReport {

  input: ReportGenerationTo
}