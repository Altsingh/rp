import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../session.service';
import { GenerateReport } from './report-generation.component';

@Injectable()
export class ReportListService {

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
}


  getReportList()
  {
    return this.http.get(this.sessionService.orgUrl + "/rest/altone/reports/getReportList/").map(
      res=>{
        this.sessionService.check401Response(res.json());
        return res.json()
      }
    );
  }

  generateReportPlaceholders(generateReport : GenerateReport)
  {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/reports/generateReportPlaceholder/' , JSON.stringify(generateReport), options).
        map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  generateReport(generateReport : GenerateReport)
  {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/reports/generateReport/' , JSON.stringify(generateReport), options).
        map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }



}
