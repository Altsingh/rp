import { Component, OnInit } from '@angular/core';
import { ReportListService } from './report-list.service';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { CommonService } from 'app/common/common.service';
import { DialogService } from '../../shared/dialog.service';
import { ReportGenerationComponent } from './report-generation.component';
import { SessionService } from '../../session.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css'],
  providers: [ReportListService,DialogService]
})
export class ReportListComponent implements OnInit {

  reportList: any;

  constructor(
    private reportListService: ReportListService,
    private notificationService: NotificationService,
    private commonService: CommonService,
    private dialogService: DialogService,
    private sessionService: SessionService,
    private i18UtilService: I18UtilService
  ) {
    this.commonService.hideRHS();
  }

  ngOnInit() {

    this.reportListService.getReportList().subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null)
       {
        if (response.messageCode.code == "EC200") 
        {
          this.reportList = response.responseData;
          
        }
      }
      else 
      {
        this.i18UtilService.get('report.error.reportFetchFailed').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING,res);
         });
      }
    });
  }

  generateReportList(data:any)
  {
  
    
    this.sessionService.object = {
      reportID : data.reportId ,
      reportDescription : data.reportDescription
    };
    this.dialogService.open(ReportGenerationComponent, { width: '680px' }).subscribe((response)=>
    {

    }
  );
  }

}
