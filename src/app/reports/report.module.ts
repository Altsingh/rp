import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import{AltRouting} from '../alt.routing';
import { ReportListComponent } from './report/report-list.component';
import { ReportRouting } from './report.routing';
import { ReportGenerationComponent } from './report/report-generation.component';
import { DialogService } from '../shared/dialog.service';
import { MatDialogModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { ReportGenerationLoaderComponent } from './report-loader/report-generation-loader/report-generation-loader.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@NgModule({
    declarations: [
        ReportListComponent,
        ReportGenerationComponent,
        ReportGenerationLoaderComponent
     
  ],
    imports: [
        CommonModule,ReportRouting,MatDialogModule,SharedModule,NgSlimScrollModule,TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
          }
        })
        ],

        entryComponents:[ReportGenerationComponent],
        
    providers: [DialogService]
   
})

export class ReportModule
{

}
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
  }
  
