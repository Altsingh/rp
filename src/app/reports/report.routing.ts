import { Routes, RouterModule } from '@angular/router';

import { ModuleWithProviders } from "@angular/core";
import { ReportListComponent } from './report/report-list.component';

const REPORT_ROUTES: Routes = [
   
   {
        path: '', component: ReportListComponent,
        children: [
            {
                path: ':id', component: ReportListComponent,
              
          }
        ]
   }


];


export const ReportRouting: ModuleWithProviders = RouterModule.forChild(REPORT_ROUTES);