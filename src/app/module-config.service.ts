import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SessionService } from './session.service';

@Injectable()
export class ModuleConfigService {

  moduleConfigCache: any[][] = [];

  constructor(private http: Http, private sessionService: SessionService) { }

  getModuleConfig(moduleName: string, parameters: string[]): Promise<any> {

    const moduleConfigRequest = {
      moduleName: moduleName,
      parameters: parameters,
      moduleConfig: null
    };

    if (this.moduleConfigCache[moduleName] == null) {

      return this.http.post(this.sessionService.orgUrl + '/rest/altone/common/moduleConfig', moduleConfigRequest).

        map((res) => {

          this.sessionService.check401Response(res.json());
          this.moduleConfigCache[moduleName] = res.json();

          return res.json();
        }).toPromise();
    } else {
      return new Promise((resolve, reject) => {
        resolve(this.moduleConfigCache[moduleName]);
      })
    }
  }
}
