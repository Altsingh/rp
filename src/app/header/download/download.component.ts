import { SessionService } from './../../session.service';
import { Subscription } from 'rxjs/Subscription';
import { DownloadService } from './download.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from '../../common/common.service';

@Component({
  selector: 'alt-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css'],
  providers: [DownloadService]
})
export class DownloadComponent implements OnInit, OnDestroy {

  pageSubscription: Subscription;
  reportsList: any[];
  loader = 'loader';

  constructor(
    private commonService: CommonService,
    private downloadService: DownloadService,
    private sessionService: SessionService
  ) {
    this.commonService.hideRHS()
  }

  ngOnInit() {
    this.pageSubscription = this.downloadService.getMyReportList().subscribe((response) => {
      if (response) {
        if (response.responseData) {
          this.reportsList = response.responseData;
          if (this.reportsList == null || this.reportsList.length == 0) {
            this.loader = 'nodata';
          } else { this.loader = 'data'; }
        }
      }
    });

  }

  ngOnDestroy() {
    if (this.pageSubscription != null) {
      this.pageSubscription.unsubscribe();
    }
  }

  fileLink(index: number, reportItem: any) {
    if (reportItem.readyToDownload == true) {
      return this.sessionService.orgUrl + '/rest/altone/report/downloadReport/' + index;
    } else {
      return ''
    }

  }

}
