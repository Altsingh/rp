import { SessionService } from './../../session.service';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class DownloadService {

  constructor(private http: Http,
    private sessionService: SessionService) {
  }

  public getMyReportList() {
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/report/reportList/', '').
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json();
      });
  }

}
