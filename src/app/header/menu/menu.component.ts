import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { MenuService } from './menu.service';
import { SessionService } from '../../session.service';
import { Location } from '@angular/common';
@Component({
  selector: 'alt-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  data: ResponseData[];
  featureDataMap: Map<any,any>;
  messageCode: MessageCode;
  jobCreationPermission: boolean;
  candidateCreationPermission: boolean;
  MenuOpenToggle: boolean[] = [];
  MenuOpenToggleMB: boolean[] = [];


  constructor(private menuService: MenuService,
    private activatedRoute: ActivatedRoute,
    public location: Location,
    private translate: TranslateService,
    private sessionService: SessionService) {
      translate.setDefaultLang('en' + this.sessionService.langVer);
    }

  ngOnInit() {

    this.translate.use(this.sessionService.langString);
    this.sessionService.getLang().subscribe((lang) => {
      this.translate.use(lang);
    });

    Promise.resolve(this.menuService.getMenuListForUser().then(res => {
      this.messageCode = res.messageCode;
      this.data=[];
      if(this.featureDataMap == undefined || this.featureDataMap == null){
        this.featureDataMap = new Map();
        this.featureDataMap.set("PUBLISH_TO_EMPLOYEE_REFERRAL","1");
        this.featureDataMap.set("PUBLISH_TO_IJP","1");
        this.featureDataMap.set("PUBLISH_TO_VENDOR","1");
        this.featureDataMap.set("ASSIGN_TO_RECRUITER","1");
        this.featureDataMap.set("INTERVIEW_PANEL","1");
        this.featureDataMap.set("PUBLISH_TO_CP","1");
        this.featureDataMap.set("PUBLISH_TO_JOBBOARD","1");
        this.featureDataMap.set("AUTOMATCH","1");
        this.featureDataMap.set("AUTOMATCH_JOBBOARD","1");
        this.featureDataMap.set("AUTOMATCH_SOCIALPROFILE","1");
        this.featureDataMap.set("AUTOMATCH_COMPANYDATA","1");
        this.featureDataMap.set("EDIT_JOB","1");
        this.featureDataMap.set("CHANGE_JOB_STATUS","1");
        this.featureDataMap.set("SOURCE_AND_TEAM","1");
        this.featureDataMap.set("EDIT_CANDIDATE","1");
        this.featureDataMap.set("PERCENT_MATCH","1");
        this.featureDataMap.set("CANDIDATE_OVERVIEW","1");
        this.featureDataMap.set("HIRING_PROCESS","1");
        this.featureDataMap.set("WORKFLOW_HISTORY","1");
        this.featureDataMap.set("CANDIDATE_HISTORY","1");
        }
      
      for (let response of res.responseData) {
        if (response.formName == "FEATURES") {
          this.featureDataMap.set(response.menuName,response.permissionID);
        }
        else{
          this.data.push(response);
        }
      }
      
      
      this.sessionService.featureDataMap=this.featureDataMap;
      this.menuService.data = this.data
      this.menuService.featureDataMap = this.featureDataMap;
      for (let menu of this.data) {
        if (menu.childMenuList == null) {
          continue;
        }
        for (let subMenu of menu.childMenuList) {
          if (subMenu.formName != null && subMenu.formName == 'NEWREQUISITION') {
            this.jobCreationPermission = true;
            break;
          } else if (subMenu.formName != null && subMenu.formName == 'NEWCANDIDATE') {
            this.candidateCreationPermission = true;
            break;
          }
        }
      }
    }));
  }

  stopClick(event) {
    event.stopPropagation();
  }

  isActive(menu) {
    if (menu != null) {
      if (menu.url != null) {
        return (location.href.indexOf("/portal" + menu.url) > -1 || location.href.indexOf(":4200" + menu.url) > -1);
      }
      if (menu.childMenuList != null) {
        let ChildUrl = null;
        for (let child of menu.childMenuList) {
          if (child.url != null) {
            ChildUrl = child.url.split("/");
            ChildUrl = ChildUrl[1];
            return ((location.href.indexOf("/portal/" + ChildUrl) > -1) || (location.href.indexOf(":4200/" + ChildUrl) > -1));
          }
        }
      }
    }
  }
  menuClick(i: number) {
    this.MenuOpenToggle[i] = !this.MenuOpenToggle[i];
  }
  menuClickClosed(i: number) {
    this.MenuOpenToggle[i] = false;
  }
  menuClickMB(i: number) {
    this.MenuOpenToggleMB[i] = !this.MenuOpenToggleMB[i];
  }
  menuClickMBClosed(i: number) {
    this.MenuOpenToggleMB[i] = false;
  }
}

export class ResponseData {
  menuName: String;
  url: String;
  formName: String;
  childMenuList: Array<ResponseData> = [];
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
