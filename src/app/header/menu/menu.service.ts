import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SessionService } from '../../session.service';
import { ResponseData } from './menu.component';
@Injectable()
export class MenuService {

  data: ResponseData[];
  featureDataMap: Map<any,any>;
  constructor(
    private http: Http,
    private sessionService: SessionService) {

  }

  public getMenuListForUser(): Promise<any> {
    return this.sessionService.readCookie().then((data) => {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/menu/menuListForUser/').map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      }).toPromise();
    });

  }

}
