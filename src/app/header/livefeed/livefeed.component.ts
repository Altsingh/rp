import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';

@Component({
  selector: 'alt-livefeed',
  templateUrl: './livefeed.component.html',
  styleUrls: ['./livefeed.component.css']
})
export class LivefeedComponent implements OnInit {

  constructor(
    private commonService: CommonService
  ) { this.commonService.showRHS() }

  ngOnInit() {
  }

}
