import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
@Component({
  selector: 'alt-my-team',
  templateUrl: './my-team.component.html',
  styleUrls: ['./my-team.component.css']
})
export class MyTeamComponent implements OnInit {

  constructor(
    private commonService: CommonService
  ) { this.commonService.showRHS() }

  ngOnInit() {
  }

}
