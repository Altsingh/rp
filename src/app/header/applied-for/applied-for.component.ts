import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
@Component({
  selector: 'alt-applied-for',
  templateUrl: './applied-for.component.html',
  styleUrls: ['./applied-for.component.css']
})
export class AppliedForComponent implements OnInit {

  constructor(
    private commonService: CommonService
  ) { this.commonService.showRHS() }

  ngOnInit() {
  }

}
