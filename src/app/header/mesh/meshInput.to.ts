export class MeshInputTO {
    userId : any;
    orgId : any;
    tenantId : any;

  constructor(userId, orgId, tenantId) {
    this.userId = userId;
    this.orgId = orgId;
    this.tenantId = tenantId;
   }

}