import { TranslateService } from '@ngx-translate/core';
import { NotificationService, NotificationSeverity } from './../common/notification-bar/notification.service';
import { ProfileService } from './profile/profile.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SessionService } from '../session.service';
import { CommonService } from '../common/common.service';
import { Http, Response, Headers, RequestOptions, ResponseOptions } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { HomePageDetailService } from '../home/home-page-detail.service';
import { map } from 'rxjs/operator/map';
import { environment } from './../../environments/environment';
import { MeshModel } from './mesh/mesh.model';
import { MeshInputTO } from './mesh/meshInput.to';
import { Observable } from "rxjs/Observable";
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { MeshAppService } from '../meshapps/meshapp.service';

declare var ga: Function;

@Component({
  selector: 'alt-header',
  templateUrl: './header.component.html',
  providers: [ProfileService]
})


export class HeaderComponent implements OnInit {

  result: any;
  isClassVisible: boolean = false;
  MobileMenu: boolean = false;
  MeshMenu: boolean = true;
  Toggle: boolean = true;
  MoreMenu: boolean = true;
  MoreMenuMb: boolean = true;
  addClass: boolean = false;
  orgLogo: String;
  clickSubscription: Subscription;
  displayClass: String = "displayNone";
  messageCode: any;
  responseData: any;
  loggedout: boolean;
  c: string;
  profilePic: string;
  @ViewChild('userProfilePic')
  profilePicInput: ElementRef;
  data: apps[];

  keycloakInfoData: any;
  keycloakUrl: string;
  keycloakEnabled: boolean;
  keycloakRefreshToken: string;
  clientId: string;
  realm: string;

  
  bulkParsingEnabled: boolean = false;
  

  meshModels: MeshModel[];
  meshUrlKey: string;
  meshUrl: string;
  errorMessage: string;
  private _urlKeyMesh = "mesh_url_";
  private serviceUrl: string = environment.inferURL;
  userId:number;
  organizationId:number;
  tenantId:number;
  loginSource:string;


  public ToggleClass() {
    this.Toggle = !this.Toggle;
  }
  public toggleMobileMenu() {
    this.MobileMenu = !this.MobileMenu;
  }
  public toggleMobileMenuOff() {
    this.MobileMenu = true;
  }
  public toggleMeshMenu() {
    this.MeshMenu = !this.MeshMenu;
  }
  public toggleMeshMenuOff() {
    this.MeshMenu = true;
  }
  public toggleClass() {
    this.addClass = !this.addClass;
  }
  public MoreMenuToggle() {
    this.MoreMenu = !this.MoreMenu;
  }
  public MoreMenuToggleOff() {
    this.MoreMenu = true;
  }
  public MoreMenuToggleMb() {
    this.MoreMenuMb = !this.MoreMenuMb;
  }
  public MoreMenuToggleOffMb() {
    this.MoreMenuMb = true;
  }


  private getMeshUrlKey() {
    if (this.serviceUrl.indexOf("sohum") > 0) {
      // this._urlKey = this._urlKey + "_" + "sohum";
      return "sohum";
    } else if (this.serviceUrl.indexOf("uat") > 0) {
      // this._urlKey = this._urlKey + "_" + "uat";
      return "uat";
    } else if (this.serviceUrl.indexOf("staging") > 0) {
      // this._urlKey = this._urlKey + "_" + "staging";
      return "staging";
    } else {
      return "prod";
    }
  }

  constructor(
    private sessionService: SessionService,
    private commonService: CommonService,
    private homePageDetailService: HomePageDetailService,
    private notificationService: NotificationService,
    private profileService: ProfileService,
    private http: Http,
    private location: Location,
    private translate: TranslateService,
    private meshAppService : MeshAppService
  ) {
   this.data = [];
   
   translate.setDefaultLang('en' + this.sessionService.langVer);
  }


    
    ngOnInit() {
      this.translate.use(this.sessionService.langString);
      this.sessionService.getLang().subscribe( (lang) => {
        this.translate.use(lang);
      } );

      this.getKeycloakInfo();
      Promise.resolve(this.sessionService.readCookie()).then((data) => {
        this.homePageDetailService.getData().subscribe((response) => {
          if (response) {
            if (response.response) {
              if (response.response.orgLogo) {
                this.orgLogo = response.response.orgLogo;
              }
            }
          }
        });
      });
      this.sessionService.getProfilePic().subscribe((url) => {
        this.profilePic = url;
      });

      this.getApplicationCategory().subscribe(res => {
        if (res != null && res[1] != null) {
          this.data = res[1].apps;
        }
      });

	this.bulkParsingEnabled = this.sessionService.isBulkParsingEnabled();

      this.meshUrl = this.getMeshUrlStatic();
      this.getMesh();
      


    }

    callApp(url: string, identifier: string, appType: string) {
      localStorage.setItem("appUrl", url);
      localStorage.setItem("customerIdentifier", identifier);
      localStorage.setItem("appType", appType);
      this.meshAppService.loadAppUrl();
    }

    setLocalStorageForKeycloak(){
 		
      localStorage.setItem("keycloakUrl", this.keycloakUrl);
      localStorage.setItem("realm", this.realm);
      localStorage.setItem("client", this.clientId);
      localStorage.setItem("refresh_token", this.keycloakRefreshToken);
     // console.log('set tenantId '+this.sessionService.tenantID.toString());

    }




  getMeshListing() {
    let meshInputTO: MeshInputTO = new MeshInputTO(this.userId,
      this.organizationId, this.tenantId);
    this.getMeshDetails(this.meshUrl, meshInputTO)
      .subscribe(res => {  
        this.meshModels = res;
        //console.log(' num meshmodel '+this.meshModels.length);
        /*this.meshModels.forEach(function (value) {
		  console.log(value.appName+' '+value.webEnabled);
		});*/  
      },




      error => this.errorMessage = error);
    }

  
    
  
 getMeshDetails(url: string, meshInputTO: MeshInputTO): Observable<any> {
    let body = `orgId=${meshInputTO.orgId}&userId=${meshInputTO.userId}&tenantId=${meshInputTO.tenantId}`;
    //console.log('inside getMeshDetails url ' + url + ' body ' + body);

    return this.http.post(url, body, this.callAppOptions())
      .map(res => res.json())
      .catch(HeaderComponent.handleError);
  }

  static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  callAppOptions(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers });
    return options;
  }

  handleProfilePicError() {
    this.profilePic = 'assets/images/svg/genericUser.svg';
  }

  keycloakLogout1(client, refresh_token, tokenUrl) {

    var data = {

      client_id: client,

      refresh_token: refresh_token,
    };

    //Call the services
    let options = new RequestOptions({ headers: this.getKeycloakHeaders() });
    this.http.post(tokenUrl, JSON.stringify(data), options).map((res) => {
      alert(res.json());
    });

  }

  private getKeycloakHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  keycloakLogout() {
    let baseUrl = this.keycloakUrl;

    let realm = this.realm;
    let client = this.clientId;

    if (realm == '290' || realm == '325') {
      realm = 'RoyalEnfield';
      client = 'OktaClient';
    }

    let refresh_token = this.keycloakRefreshToken;
    let tokenUrl = baseUrl + 'auth/realms/' + realm + '/protocol/openid-connect/logout';
    //this.keycloakLogout1(client,refresh_token,tokenUrl);
    let req = new XMLHttpRequest();
    let params = 'client_id=' + client + '&refresh_token=' + refresh_token;
    req.open('POST', tokenUrl, true);
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    req.send(params);

  } 

  public logout() {
    if (this.keycloakEnabled == true && !(this.loginSource && 'AltHome'==this.loginSource)) {
      this.keycloakLogout();
    }

    let options = new RequestOptions({ headers: this.getHeaders() });
    let orgURL = this.sessionService.orgUrl;
    this.http.post(this.sessionService.orgUrl + '/rest/altone/session/logout/', options)
      .map((res) => {
        return res.json();
      }).subscribe((out) => {
        this.messageCode = out.messageCode;
        this.responseData = out.responseData
        this.loggedout = this.responseData[0];
        let logoutServerName: string;
        logoutServerName=this.responseData[1];

        if (this.loggedout == true) {
          if(logoutServerName!=null && logoutServerName!=""){
            window.location.href = logoutServerName;
          }else{
           if (orgURL == null || !orgURL.startsWith("https://")) {
              window.location.href = "https://" + window.location.host;
            } else {
              window.location.href = orgURL;
            }
          }
        }

      });
  }

  getKeycloakInfo() {
	
    this.http.get(this.sessionService.orgUrl + '/rest/altone/session/keycloakInfo')
      .map((res) => {
        return res.json();
      }).subscribe((out) => {
        this.keycloakEnabled = out.responseData[0].keycloakEnabled;
        if (this.keycloakEnabled == true) {
        	console.log('keycloak enabled');
          this.keycloakUrl = out.responseData[0].keycloakUrl;
          this.keycloakRefreshToken = out.responseData[0].refreshToken;
          this.clientId = out.responseData[0].client;
          this.realm = out.responseData[0].realm;
		  this.loginSource=out.responseData[0].loginSource;
          this.setLocalStorageForKeycloak();
        }
      });
  }



  getMeshUrl() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/session/getMeshUrl').map(res => {
      this.sessionService.check401Response(res.json()); return res.json()
    });
  }
  
  getMeshUrlStatic() {
  	return environment.meshUrl;
    //return MeshModel.mesh_urls[this.getMeshUrlKey()];
  }


getApplicationCategory() {
  return this.http.get(this.sessionService.orgUrl + '/rest/altone/session/getMeshUrl').map(res => {
    this.sessionService.check401Response(res.json()); return res.json()
  });
}



getHeaders() {
  let headers = new Headers();
  headers.append('Accept', 'application/text');
  headers.append('Content-Type', 'application/json');
  headers.append('Access-Control-Allow-Headers', 'Content-Type');
  headers.append('Access-Control-Allow-Methods', 'POST');
  headers.append('Access-Control-Allow-Origin', '*');
  return headers;
}

getGetHeaders() {
  let headers = new Headers();
  headers.append('Accept', 'application/text');
  headers.append('Content-Type', 'application/json');
  headers.append('Access-Control-Allow-Headers', 'Content-Type');
  headers.append('Access-Control-Allow-Methods', 'GET');
  headers.append('Access-Control-Allow-Origin', '*');
  return headers;
}

picFileChange(event) {
  let fileCount: number = this.profilePicInput.nativeElement.files.length;
  let formData = new FormData();
  if (fileCount > 0) { // a file was selected

    if (this.profilePicInput.nativeElement.files.item(0).size > 2097152) {
      this.notificationService.setMessage(NotificationSeverity.WARNING, "Picture size shouldbe less than 2 MB.");
      event.target.value = "";
      return;
    }
    formData.append('file', this.profilePicInput.nativeElement.files.item(0));


    let profileUploadError = "Could not update profile picture.";
    let subscription = this.profileService.saveProfilePic(formData).subscribe((response) => {
      let updated = false;
      if (response) {
        if (response.messageCode) {
          if (response.messageCode.code == "EC200") {
            updated = true;

            this.profilePic = response.responseData[0].profilePicURL;
            this.sessionService.setProfilePic(response.responseData[0].profilePicURL);
            this.homePageDetailService.clearCache();
          }else if(response.messageCode.code == "EC201"){
            if( response.messageCode.message != null ){
              profileUploadError = response.messageCode.message;
            }
          }
        }
      }

      this.notificationService.clear();

      if (!updated) {
        this.notificationService.setMessage(NotificationSeverity.WARNING, profileUploadError);
      } else {
        this.notificationService.setMessage(NotificationSeverity.INFO, "Profile picture updated successfully.");
      }
    });
  }

  event.target.value = "";
}

getMesh(){
	let options = new RequestOptions({ headers: this.getHeaders() });
  this.http.post(this.sessionService.orgUrl + '/rest/altone/session/current',"")
    .map((res) => {
      return res.json();
    }).subscribe((out) => {  
      this.userId = out.responseData[0].userId;
      this.organizationId = out.responseData[0].organizationId;
      this.tenantId = out.responseData[0].tenantId;
      this.sessionService.userID = out.responseData[0].userId;
      this.sessionService.employeeName = out.responseData[0].employeeName;
      this.sessionService.isJinieEnabled = out.responseData[0].jinieEnabled;
      this.sessionService.isMessengerEnabled = out.responseData[0].messengerEnabled;
      this.sessionService.jinieUrl = out.responseData[0].jinieUrl;
      this.sessionService.userName = out.responseData[0].userName;
      if(out.responseData[0].portalType == "Login with Business Role"){
      this.sessionService.isBPEnabled = true;
      }
      if( out.responseData[0].lang != undefined && out.responseData[0].lang != null ){
        this.sessionService.setLang(out.responseData[0].lang);

        ga('send', {
          hitType: 'event',
          eventCategory: "HomePage",
          eventLabel: "Loaded",
          eventAction: "Load"
        });

      }
      localStorage.setItem("orgId", this.organizationId.toString());
     localStorage.setItem("tenantId", this.tenantId.toString());
     
      this.getMeshListing();

    });
}


enableStatic() {
  return this.sessionService.removeStatic;
}
}
export class apps {
  url: string;
  appLogo: string;
  name: string;
  appId: number;
}
