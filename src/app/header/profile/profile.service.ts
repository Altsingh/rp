import { SessionService } from 'app/session.service';
import { Http, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ProfileService {

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) { }


  public saveProfilePic(requestData) {
    let options = new RequestOptions();
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/common/saveProfilePic/user', requestData, options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

}
