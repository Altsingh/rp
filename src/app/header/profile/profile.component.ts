import { HomePageDetailService } from './../../home/home-page-detail.service';
import { NotificationService, NotificationSeverity } from './../../common/notification-bar/notification.service';
import { ProfileService } from './profile.service';
import { SessionService } from 'app/session.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CommonService } from '../../common/common.service';

@Component({
  selector: 'alt-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService]
})
export class ProfileComponent implements OnInit {

  profilePic: string;
  employeeName: string;

  @ViewChild('userProfilePicx')
  profilePicInput: ElementRef;


  constructor(
    private commonService: CommonService,
    private sessionService: SessionService,
    private profileService: ProfileService,
    private notificationService: NotificationService,
    private homePageDetailService: HomePageDetailService
  ) {
    this.commonService.hideRHS();

  }

  ngOnInit() {

    Promise.resolve(this.sessionService.readCookie()).then((data) => {
      this.homePageDetailService.getData().subscribe(response => {
        if (response) {
          if (response.response) {
            if (response.response.userName) {
              this.employeeName = response.response.userName;
              this.sessionService.setProfilePic(response.response.profilePic);
            }
          }
        }
      });
    });
    this.profilePic = this.sessionService.profilePic;
    this.sessionService.getProfilePic().subscribe((res) => {
      this.profilePic = res;
    });
  }


  picFileChange(event) {
    let fileCount: number = this.profilePicInput.nativeElement.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected

      if (this.profilePicInput.nativeElement.files.item(0).size > 2097152) {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Picture size shouldbe less than 2 MB.");
        event.target.value = "";
        return;
      }
      formData.append('file', this.profilePicInput.nativeElement.files.item(0));


      let profileUploadError = "Could not update profile picture.";
      let subscription = this.profileService.saveProfilePic(formData).subscribe((response) => {
        let updated = false;
        if (response) {
          if (response.messageCode) {
            if (response.messageCode.code == "EC200") {
              updated = true;

              this.profilePic = response.responseData[0].profilePicURL;
              this.sessionService.setProfilePic(response.responseData[0].profilePicURL);
              this.homePageDetailService.clearCache();
            }else if(response.messageCode.code == "EC201"){
              if( response.messageCode.message != null ){
                profileUploadError = response.messageCode.message;
              }
            }
          }
        }

        this.notificationService.clear();

        if (!updated) {
          this.notificationService.setMessage(NotificationSeverity.WARNING, profileUploadError);
        } else {
          this.notificationService.setMessage(NotificationSeverity.INFO, "Profile picture updated successfully.");
        }
      });
    }

    event.target.value = "";
  }

  handleProfilePicError() {
    this.profilePic = 'assets/images/svg/genericUser.svg';
  }
}