import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
@Component({
  selector: 'alt-pending-task',
  templateUrl: './pending-task.component.html',
  styleUrls: ['./pending-task.component.css']
})
export class PendingTaskComponent implements OnInit {
  step:string;
  constructor(
    private commonService: CommonService
  ) { this.commonService.hideRHS() }

  ngOnInit() {
  }

}
