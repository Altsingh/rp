import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadBlankScreenComponent } from './download-blank-screen.component';

describe('DownloadBlankScreenComponent', () => {
  let component: DownloadBlankScreenComponent;
  let fixture: ComponentFixture<DownloadBlankScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadBlankScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadBlankScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
