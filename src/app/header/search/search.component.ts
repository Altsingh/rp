import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';

@Component({
  selector: 'alt-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  popupShowClass: boolean = false;

  constructor(
    private commonService: CommonService
  ) { this.commonService.showRHS() }

  ngOnInit() {
  }
  public popupShow() {
    this.popupShowClass = !this.popupShowClass
  }

}
