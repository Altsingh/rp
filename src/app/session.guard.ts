import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from './session.service';
import { MenuService } from './header/menu/menu.service';

@Injectable()
export class SessionGuard implements CanActivate {

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private menuService: MenuService) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {


    if (state.url.indexOf("home") < 0 && !this.checkMenu(state.url)) {
      this.router.navigateByUrl("/home");
    }

    Promise.resolve(this.sessionService.checkCurrentSession()).then((data) => {
      if (data) {
        if (data == "false") {
          this.sessionService.loggedIn = false;
        }
      }
    });

    if (!this.sessionService.loggedIn) {
      this.router.navigateByUrl("/logout");
      return false;
    }

    return true;
  }


  checkMenu(url: string) {

    let canAccessMenu = false;

    if (this.menuService.data != null) {
      for (let x = 0; x < this.menuService.data.length; x++) {

        if (
          this.menuService.data[x].url != null
          && this.menuService.data[x].url.length >= url.length
          && this.menuService.data[x].url.substring(0, url.length) == url) {
          return true;
        }
        if (this.menuService.data[x].childMenuList) {
          if (this.menuService.data[x].childMenuList.length > 0) {
            for (let y = 0; y < this.menuService.data[x].childMenuList.length; y++) {
              if (this.menuService.data[x].childMenuList[y].url != null) {
                let urlStr = <string>this.menuService.data[x].childMenuList[y].url;
                if (url.startsWith(urlStr)) {
                  return true;
                }
              }
            }
          }
        }
      }
    }
    if(this.menuService.featureDataMap != undefined && this.menuService.featureDataMap != null){
      if(url.endsWith('performance-source-team')){
        if(this.menuService.featureDataMap.get("SOURCE_AND_TEAM") == 1){
          return true;
        }
      }
    }
    return canAccessMenu;
  }

}
