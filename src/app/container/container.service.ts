import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../session.service';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
function getWindow (): any {
  return window;
}
@Injectable()
export class ContainerService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getMesengerToken(companyURL, userName) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    let request = new MesengerTokenInput();
    request.companyURL = companyURL;
    request.userName = userName;
    request.portalType = 'Recruiter Portal';
    let serviceUrl: string = environment.chatUserSSOLoginURL;
    return this.http.post(serviceUrl, JSON.stringify(request), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  public getSessionDetails(): any {
    let _callurlDetails = this.sessionService.orgUrl + '/rest/altone/jobs/getSessionDetails/';
    return this.http.get(_callurlDetails)
      .map(res => { this.sessionService.check401Response(res.json()); return res.json() }).toPromise();
  }
  
}
export class WindowRefService {
  get nativeWindow (): any {
      return getWindow();
  }
}
export class MesengerTokenInput {
  userName: String;
  companyURL: String;
  portalType: String;
}