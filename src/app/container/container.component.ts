import { environment } from './../../environments/environment';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Subscription } from 'rxjs/Subscription';
import { ContainerService, WindowRefService } from './container.service';
import { SessionService } from '../session.service';
//import { WindowRefService } from './scroll.service'

@Component({
  selector: 'alt-container',
  templateUrl: './container.component.html',
  providers: [ContainerService, WindowRefService]
})
export class ContainerComponent implements OnInit, OnDestroy {
  win: Window;
  minHeight = window.innerHeight - 75;
  private offSet: number;
  rhsHidden = false;
  shouldStick: boolean;
  rhsSubscription: Subscription;
  openMessanger = false;
  
  constructor(private commonService: CommonService, private containerService: ContainerService, 
    private _win: WindowRefService,private sessionService: SessionService) {
          this.win = _win.nativeWindow;
  }

  ngOnInit() {
    window.addEventListener('scroll', (e) => {
      if (window.pageYOffset > 400) {
        this.shouldStick = true;
      } else {
        this.shouldStick = false;
      }
    });

    this.rhsSubscription = this.commonService.isRHSHidden().subscribe((hidden) => {
      setTimeout(() => {
        this.rhsHidden = hidden;
      });
    });
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.rhsSubscription.unsubscribe();
  }
  scrollTo(yPoint: number, duration: number) {
    setTimeout(() => {
      this.win.window.scrollTo(0, yPoint)
    }, duration);
    return;
  }
  smoothScroll(eID) {
    var startY = this.currentYPosition();
    var stopY = this.elmYPosition(eID);
    var distance = stopY > startY ? stopY - startY : startY - stopY;
    if (distance < 20) {
      this.win.window.scrollTo(0, stopY); return;
    }
    var speed = Math.round(distance / 20);
    if (speed >= 20) speed = 20;
    var step = Math.round(distance / 20);
    var leapY = stopY > startY ? startY + step : startY - step;
    var timer = 0;
    if (stopY > startY) {
      for (var i = startY; i < stopY; i += step) {
        this.scrollTo(leapY, timer * speed);
        leapY += step; if (leapY > stopY) leapY = stopY; timer++;
      } return;
    }
    for (var i = startY; i > stopY; i -= step) {
      this.scrollTo(leapY, timer * speed);
      leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
    }
  }
  currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
      return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
  }
  elmYPosition(top) {
    return top;
  }

  onMessengerIconClick() {
    this.openMessanger = !this.openMessanger;
    // if(this.openMessanger) {
    //   this.redirectToMessenger();
    // }
  }

  // redirectToMessenger() {
  //   Promise.resolve(this.containerService.getSessionDetails().then((res) => {
  //     let userName = res.username;
  //     let serverURL: string = res.serverURL;
  //     let companyURL: string = serverURL.slice(serverURL.indexOf("://") + 3);
  //     this.containerService.getMesengerToken(companyURL, userName).subscribe(response => {
  //       window.open(environment.altmessengerURL + "/?token=" + response.data, "_blank");
  //     }, (onError) => {
  //       window.open(environment.altmessengerURL, "_blank");
  //     });
  //   }));
    
  //  }

  enableStatic() {
    return this.sessionService.removeStatic;
  }

  isJinieEnabled() {
    return this.sessionService.isJinieEnabled;
  }
  isMesengerEnabled() {
    return this.sessionService.isMessengerEnabled;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.minHeight = event.target.innerHeight - 75;
  }
  
}
