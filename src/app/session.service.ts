import { Injectable } from '@angular/core';
import { Jsonp, Http, Response, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotificationService, NotificationSeverity } from './common/notification-bar/notification.service';
import { Subject, Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable()
export class SessionService {

  isUntag = false;
  organizationID: number = 0;
  tenantID: number = 0;
  userID: number = 0;
  orgUrl: string;
  employeeName: string = '';
  orgLogo: string = '';
  session = '';
  vendorId: number =0;
  vendorUserId: number=0;
  private profilePicUrl = new Subject<string>();
  public profilePic: string;
  monsterFilter : boolean = false;
  lang = new Subject<string>();
   langObservable = this.lang.asObservable();
  langString :string;
  /**
   * Used for passing objects from one object to another
   */
  object: any;
  objectType: string;

  jobSummaryData: any;
  candidateInfo: any;
  jobSkills: any[] = [];
  taggedCandidateIds: any[];
  requisitionId: number;

  dialogData: any;
  dialogData1: any;
  dialogData2: any;

  vendorTabDisabled : boolean;
  removeStatic: boolean;
  isProfessionalRecruit: boolean;

  featureDataMap:Map<any,any>;
  public isBPEnabled: Boolean = false;

  public loggedIn: boolean = true;

  isJinieEnabled:boolean= false;

  isMessengerEnabled:boolean= false;

  jinieUrl : string =null;

  userName : string =null;

  langVer : string = "_1.0";

  constructor(
    private location: Location,
    private router: Router,
    private http: Http,
    private notificationService: NotificationService
  ) {

    this.readCookie();
    this.vendorTabDisabled = false;
  }

  public getLang(): Observable<string>{
    return this.lang.asObservable();
  }

  public setLang(lang: string){
    this.langString = lang.toLowerCase();
    this.langString = this.langString + this.langVer;
    return this.lang.next(this.langString);
  }

  readCookie(): Promise<string> {

    return new Promise((resolve, reject) => {

      let serverURL = '';

      if (location.href.indexOf('sohum') > 0) {
        // local ng serve is running
        serverURL = location.href.substring(0, location.href.lastIndexOf(':')) + '/ats';
        serverURL = '/ats';
      }
      this.orgUrl = serverURL;
      resolve('');
      if(location.href.includes(environment.demoURL)) {
        this.removeStatic = true;
      }
      if(location.href.includes(environment.professionalURL)) {
        this.isProfessionalRecruit = true;
      }
    });
  }

  public isBulkParsingEnabled(){
    if( environment.bulkParserEnabledHosts.includes(location.hostname) ){
      return true;
    }
    return false;
  }

  public isAutoMatchEnabled(){
    if( environment.autoMatchDisabledHosts.includes(location.hostname) ){
      return false;
    }
    return true;
  }

  public isHiringTeamEnabled(){
    if( environment.hiringTeamDisabledHosts.includes(location.hostname) ){
      return false;
    }
    return true;
  }

  public getProfilePic(): Observable<string> {
    return this.profilePicUrl.asObservable();
  }

  public setProfilePic(url: string) {
    if (url == null || url.trim() == '') {
      this.profilePicUrl.next("assets/images/svg/genericUser.svg");
      this.profilePic = "assets/images/svg/genericUser.svg";
    } else {
      this.profilePicUrl.next(url);
      this.profilePic = url;
    }
  }

  checkCurrentSession(): Promise<any> {
    let serverURL = '';
    if (this.orgUrl != null) {
      serverURL = this.orgUrl;
    } else {
      serverURL = "/ats";
    }
    return this.http.post(serverURL + '/rest/altone/session/check/', this.session).map(response => response.text()).toPromise();
  }

  check401Response(response) {

    if (response) {
      if (response.messageCode) {
        if (response.messageCode.code) {
          if (response.messageCode.code == "EC401") {
            this.router.navigateByUrl("/logout");
            this.loggedIn = false;
          }
        }
      }
    }
  }
}
