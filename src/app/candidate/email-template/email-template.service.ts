import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from "../../session.service";

@Injectable()
export class EmailTemplateService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'POST');
      headers.append('Access-Control-Allow-Origin', '*');
      return headers;
  }

  getURL() {
      return (this.sessionService.orgUrl + '/rest/altone/sendEmail/mailToCandidate/');
  }

  public sendEmail(emailData: EmailData){
     let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getURL(), JSON.stringify(emailData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
  }

    public sendCPCredentials(applicantStatusId) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/cpcredentials/' + applicantStatusId , {"cpFilter":"cpcredentials"}, options)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json();
            });
    }
    public sendLOIEmail(applicantStatusId:number,emailData: EmailData) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/loi_email/' + applicantStatusId ,JSON.stringify(emailData), options)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json();
            });
    }
 }

export class EmailData {
  sentTo: String[];
  cc: String;
  subject: String;
  content: String;
  organizationId: Number;
  tenantId: Number;
}
