import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { EmailTemplateService } from "./email-template.service";
import { SessionService } from "../../session.service";
import { NotificationService } from "../../common/notification-bar/notification.service";
import { ValidationService } from "../../shared/custom-validation/validation.service";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NotificationSeverity } from '../candidate-list/taggedCandidates/email-candidates/email-candidates.component';
import { NotificationPopupService } from 'app/shared/notification-bar-popup/notification-popup.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.css'],
  providers: [EmailTemplateService, NotificationPopupService]
})
export class EmailTemplateComponent implements OnInit {
  lable: string;
  emailCandidate: boolean = false;
  data: EmailData;
  showloader = false;
  messageCode: MessageCode;
  responseData: ResponseData;
  cpMail: boolean = false;
  loiMail: boolean = false;
  applicantStatusId: String;
  cpHTML: SafeHtml = '';
  loiHTML: SafeHtml = '';
  message: String = '';
  total_documents: number = 0;
  attachedDocument: Attachdocument;
  emailInfo: FormGroup;
  primaryEmail: String;
  public options: Object;
  froalakey: any;
  total_attachment_size = 0;
  fileinProgress: boolean = false;

  constructor(public dialogRef: MatDialogRef<EmailTemplateComponent>, private sanitizer: DomSanitizer, private emailTemplateService: EmailTemplateService,
    private sessionService: SessionService,
    private popupnotificationService: NotificationPopupService,
    private formBuilder: FormBuilder, private notificationService: NotificationService,
    private i18UtilService: I18UtilService) {
    this.data = new EmailData();
    this.responseData = new ResponseData();
    if (window.location.hostname.includes(".peoplestrong.com")) {
      this.froalakey = 'aH3H3B8A8bA4B3E3C1I3H2C2C6B3E2uzncH-7bjcef1G-10zrB1twt=='
    } else if (window.location.hostname.includes(".peoplestrongalt.com")) {
      this.froalakey = 'yC5E5B4E4jC10D7A5A5B2A3E4E2B2B5B-16rfkujbF-7A-21xdbjocdI1qA-16y==';
    }
    this.options = {
      key: this.froalakey
    }

  }

  ngOnInit() {
    this.emailInfo = this.formBuilder.group({
      toEmail: [''],
      ccEmail: [''],
      subject: '',
      content: ''
    });
    this.data.attachdocuments = [];
    this.primaryEmail = this.sessionService.object.primaryEmail;
    if (this.sessionService.object.cpFilter == 'cpcredentials') {
      this.cpMail = true;
      this.primaryEmail = this.sessionService.object.responseData.response.emailTo;
      this.emailInfo.controls.ccEmail.setValue(this.sessionService.object.responseData.response.emailCc);
      this.emailInfo.controls.subject.setValue(this.sessionService.object.responseData.response.emailSubject);
      this.cpHTML = this.sanitizer.bypassSecurityTrustHtml(this.sessionService.object.responseData.response.emailBody);
      this.emailInfo.controls.content.setValue(this.sessionService.object.responseData.response.emailBody);
    }

    if (this.sessionService.object.loi === 'LOI') {
      this.loiMail = true;
      this.primaryEmail = this.sessionService.object.to;
      this.emailInfo.controls.ccEmail.setValue(this.sessionService.object.cc);
      this.emailInfo.controls.subject.setValue(this.sessionService.object.subject);
      this.emailInfo.controls.content.setValue(this.sessionService.object.content);
    }
    this.i18UtilService.get('common.label.send').subscribe((res: string) => {
      this.lable = res;
    });
  }

  sendEmail(event) {
    let valid: boolean = true;
    this.emailCandidate = true;
    this.data.organizationId = this.sessionService.organizationID;
    this.data.tenantId = this.sessionService.tenantID;
    this.data.sentTo = [];
    this.data.sentTo.push(this.primaryEmail);
    this.data.cc = this.emailInfo.controls.ccEmail.value;
    this.data.subject = this.emailInfo.controls.subject.value;
    this.data.content = this.emailInfo.controls.content.value;
    this.data.mailFrom = this.sessionService.employeeName;
    this.showloader = true;
    let fileUploadError = "Could not upload Document. Please try again.";

    if (ValidationService.emailValidator(this.emailInfo.controls.ccEmail)) {
      this.popupnotificationService.setMessage(NotificationSeverity.WARNING, 'candidate.warning.ccMailFormat');
      this.showloader = false;
      valid = false;
    }
    if (valid) {
      this.emailTemplateService.sendEmail(this.data).subscribe(out => {

        if (out.messageCode != null) {
          if (out.messageCode.code != null && out.messageCode.code === 'EC200') {
            this.messageCode = out.messageCode;
            this.responseData.result = out.responseData[0];
            if (this.responseData.result == "success") {
              this.dialogRef.close();
              this.i18UtilService.get('candidate.success.emailSent').subscribe((res: string) => {
                this.notificationService.setMessage("INFO", res);
               });
            }
          } else if (out.messageCode.code === 'EC201') {
            fileUploadError = out.messageCode.description.toString();
            this.popupnotificationService.setMessage(NotificationSeverity.WARNING, fileUploadError);
            this.emailCandidate = false;
          } else {
            this.dialogRef.close('FAILURE');
          }
        }
        this.fileinProgress = false;
        this.showloader = false;
      }, (onError) => {
        this.fileinProgress = false;
        this.showloader = false;
      }, () => {
        this.fileinProgress = false;
        this.showloader = false;
      });
    }
  }

  sendCPCredentials(event) {
    this.emailTemplateService.sendCPCredentials(this.sessionService.object.applicantStatusId).subscribe(response => {
      this.messageCode = response.messageCode;
      if (response.response == "success") {
        this.dialogRef.close();
        this.i18UtilService.get('candidate.success.emailSent').subscribe((res: string) => {
          this.notificationService.setMessage("INFO", res);
         });
      }
    });
  }
  sendLOIEmail(event) {
    this.data.organizationId = this.sessionService.organizationID;
    this.data.tenantId = this.sessionService.tenantID;
    this.data.sentTo = [];
    this.data.sentTo.push(this.primaryEmail);
    this.data.cc = this.emailInfo.controls.ccEmail.value;
    if (ValidationService.emailValidator(this.emailInfo.controls.ccEmail)) {
      let message: string = '';
      message = 'candidate.warning.ccMailFormat';
      this.setMessage("WARNING", message);
      return;
    }
    this.data.subject = this.emailInfo.controls.subject.value;
    this.data.content = this.emailInfo.controls.content.value;
    this.data.mailFrom = this.sessionService.employeeName;
    this.showloader = true;
    this.emailTemplateService.sendLOIEmail(this.sessionService.object.applicantStatusId, this.data).subscribe(response => {
      this.messageCode = response.messageCode;
      this.responseData.result = response.responseData[0];
      if (this.responseData.result == "success") {
        this.dialogRef.close();
        this.i18UtilService.get('candidate.success.emailSent').subscribe((res: string) => {
          this.notificationService.setMessage("INFO", res);
         });
       
      }
      this.showloader = false;
    });
  }
  severityClass: String = '';
  severity: String = '';

  public setMessage(severity: string, message: string, args?: any) {
    if(args){
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        if (severity == 'WARNING') {
          this.severityClass = "warning";
        }
       });

    }else{
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        if (severity == 'WARNING') {
          this.severityClass = "warning";
        }
       });

    }
   
   
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    this.fileinProgress = true;
    let singleFileCapaictyExceeded = false;
    let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
    this.getAttachmentsTotalSize();
    this.popupnotificationService.clear();

    if (fileList) {
      if (fileList.length > 0) {

        for (var i = 0; i < fileList.length; i++) {
          let file = fileList[i];

          let extenstion = file.name.split('.').pop();
          if (!supportedTypes.includes(extenstion.toLowerCase())) {
            this.popupnotificationService.addMessage(NotificationSeverity.WARNING, 'Only ' + supportedTypes.toString() + ' files are allowed!');
            event.target.value = null;
            this.fileinProgress = false;
            return;
          }

          if (file.size > (8 * 1024 * 1024)) {
            singleFileCapaictyExceeded = true;
          }
          this.total_attachment_size += file.size;
        }

        if (this.total_attachment_size > (10 * 1024 * 1024)) {
          this.popupnotificationService.setMessage(NotificationSeverity.WARNING, 'candidate.warning.sizeExceeded');
          event.target.value = null;
          this.fileinProgress = false;
          return;
        }

        if (singleFileCapaictyExceeded) {
          this.popupnotificationService.addMessage(NotificationSeverity.WARNING, 'candidate.error.fileSizeExceeded');
          event.target.value = null;
          this.fileinProgress = false;
          return;
        }

        this.total_documents = 0;
        for (var i = 0; i < fileList.length; i++) {
          let file = fileList[i];
          let fr = new FileReader();

          fr.readAsDataURL(file);
          fr.onloadend = (e) => {
            this.attachedDocument = new Attachdocument();
            this.attachedDocument.attachmentData_base64 = fr.result;
            this.attachedDocument.attachmentName = file.name;
            this.attachedDocument.attachmentSize = file.size;

            if (this.data.attachdocuments == null) {
              this.data.attachdocuments = new Array<Attachdocument>();
            }
            this.data.attachdocuments.push(this.attachedDocument);

            //called in loop because of delay by onloadend in async
            if (this.data.attachdocuments && this.data.attachdocuments.length != undefined) {
              this.total_documents = this.data.attachdocuments.length;
            }
          }

        }

      }
    }
    this.fileinProgress = false;
    event.target.value = null;
  }


  deleteDocumentListener(file: Attachdocument) {
    for (var i = 0; i < this.data.attachdocuments.length; i++) {
      if (file == this.data.attachdocuments[i]) {
        this.total_attachment_size = this.total_attachment_size - this.data.attachdocuments[i].attachmentSize;
        this.data.attachdocuments = this.data.attachdocuments.filter(item => item !== file);
        this.total_documents = this.data.attachdocuments.length;
      }
    }
  }

  getAttachmentsTotalSize() {
    this.total_attachment_size = 0;
    if (this.data.attachdocuments && this.data.attachdocuments.length != undefined) {
      for (var i = 0; i < this.data.attachdocuments.length; i++) {
        this.total_attachment_size += this.data.attachdocuments[i].attachmentSize;
      }
    }
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }
}



export class EmailData {
  sentTo: String[];
  cc: String;
  subject: String;
  content: String;
  mailFrom: String;
  organizationId: Number;
  tenantId: Number;
  attachdocuments: Attachdocument[];
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  result: String;
}

export class Attachdocument {
  attachmentData_base64: string | ArrayBuffer;
  attachmentName: string;
  attachmentSize: number;
}