import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'parseCandidatecodeParam'
})
export class ParseCandidatecodeParamPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) { return value; }

    return value.replace(/\-/g, '/');
  }

}
