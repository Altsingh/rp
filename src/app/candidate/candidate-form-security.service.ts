import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { Http, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { CachedData } from '../common/cached-data'

@Injectable()
export class CandidateFormSecurityService extends CachedData {

  constructor(
    http: Http,
    sessionService: SessionService) {

    super(http, sessionService);

    let requestJSON = {
      roleIds: [],
      organizationId: sessionService.organizationID,
      tenantId: sessionService.tenantID
    };

    super.initialize(
      '/rest/altone/formSecurity/newCandidateSecurity/'+((sessionService.isBPEnabled)?'?roleName=EMPLOYEE':''),
      'POST',
      requestJSON);
  }

}
