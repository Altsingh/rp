import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Form, FormBuilder, FormGroup, FormControl, FormsModule, FormArray } from '@angular/forms';
import { SessionService } from '../../../session.service';
import { JsonpModule } from '@angular/http';
import { CandidateController } from '../candidate-interfaces';
import { NewCandidateService } from '../new-candidate.service';
import { Subscription } from 'rxjs/Subscription';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { NewJobService } from '../../../job/new-job/new-job.service';

@Component({
  selector: 'alt-candidate-skills-details',
  templateUrl: './skills-details.component.html',
  providers: [NewJobService]
})
export class SkillsDetailsComponent implements OnInit {

  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;

  skillsList: any[] = [];
  skillList: any[] = [];

  subscription: Subscription;

  constructor(
    public formBuilder: FormBuilder,
    private sessionService: SessionService,
    private newCandidateService: NewCandidateService,
    private notificationService: NotificationService,
    private newJobService: NewJobService) {

  }

  continue() {
    this.candidateController.continueSkillSet();
  }

  clearAll(event) {
    event.stopPropagation();
    this.candidateController.clearAllSkillSet();
  }

  addSkill(skill) {

    if (skill != null) {
      this.notificationService.clear();

      for (let c = 0; c < this.skillsList.length; c++) {
        let skillN = this.skillsList[c];
        if (skillN.skillName.toLowerCase() == skill.trim().toLowerCase()) {
          this.formGroup.get('skillInput').setValue("");
          this.notificationService.setMessage(NotificationSeverity.WARNING, skill + " has already been added!");
          return;
        }
      }

      if (skill.trim() !== '') {
        const control = <FormArray>this.formGroup.controls['skills'];
        control.push(this.formBuilder.group({
          skillId: '0',
          skillName: skill.trim()
        }));

        this.skillsList.push({
          skillId: '0',
          skillName: skill.trim()
        });
      }

    }

    setTimeout(() => { this.formGroup.get('skillInput').setValue(""); }, 50);
    this.skillList = [];
  }

  syncList() {
    
    this.skillsList = [];

    let skillArray = <FormArray>this.formGroup.controls.skills;

    for (let x = 0; x < skillArray.length; x++) {

      let skillGroup = <FormGroup>skillArray.at(x);

      this.skillsList.push({
        skillId: skillGroup.controls.skillId.value,
        skillName: skillGroup.controls.skillName.value
      });
    }


  }


  removeSkill(i: number) {
    this.candidateController.removeSkill(i);
  }

  ngOnInit() {

    this.formGroup.controls.skillInput.valueChanges.subscribe((data) => {

      if (data != null && data.trim() != '') {
        this.newJobService.getSkillsByPattern(data, 10).subscribe((out) => {
          this.skillList = out.responseData;
          if (this.skillList.length == 0) {
            this.skillList = [data];
          } else {
            let found = false;
            for (let x = 0; x < this.skillList.length; x++) {
              let skillStr = <string>this.skillList[x];
              let skillInp = <string>data;
              if (skillStr.toLowerCase() == data.toLowerCase()) {
                found = true;
                break;
              }
            }
            if (!found) {
              this.skillList.push(data);
            }
          }
        });
      } else{
        this.skillList = [];
      }

    });

    this.subscription = this.formGroup.valueChanges.subscribe(data => {
      const control = <FormArray>this.formGroup.controls['skills'];
      if (this.skillsList.length != control.length) {
        this.syncList();
      }

    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}