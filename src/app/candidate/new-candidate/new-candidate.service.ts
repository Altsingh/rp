import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Jsonp, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from '../../session.service';

@Injectable()
export class NewCandidateService {

    constructor(
        private http: Http,
        private sessionService: SessionService) {
    }

    public fetchCandidateId(candidateCode, isDraft: boolean) {

        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch' + (isDraft ? "/draft" : "") + '/candidateId/' + this.sessionService.organizationID + "/" + candidateCode + "/", options).
            map((res) => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public fetchCandidateBasic(candidateId, isDraft: boolean) {

        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch' + (isDraft ? "/draft" : "") + '/basic/' + this.sessionService.organizationID + "/" + candidateId + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public fetchCandidatePersonal(candidateId, isDraft: boolean) {

        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch' + (isDraft ? "/draft" : "") + '/personal/' + this.sessionService.organizationID + "/" + candidateId + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public fetchCandidateWork(candidateId, isDraft: boolean, isInternship: boolean) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch' + (isDraft ? "/draft" : "") + '/work/' + this.sessionService.organizationID + "/" + candidateId + '/' + isInternship + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public deleteCandidateWork(candidateId, requestData, isDraft: boolean) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/delete' + (isDraft ? "/draft" : "") + '/work/' + this.sessionService.organizationID + "/" + candidateId + "/", JSON.stringify(requestData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public fetchCandidateEducation(candidateId, isDraft: boolean) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch' + (isDraft ? "/draft" : "") + '/education/' + this.sessionService.organizationID + "/" + candidateId + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public deleteCandidateEducation(candidateId, requestData, isDraft: boolean) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/delete' + (isDraft ? "/draft" : "") + '/education/' + this.sessionService.organizationID + "/" + candidateId + "/", JSON.stringify(requestData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public fetchCandidateSkills(candidateId, isDraft: boolean, isLanguage: boolean) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch' + (isDraft ? "/draft" : "") + '/skills/' + this.sessionService.organizationID + "/" + candidateId + '/' + isLanguage + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public deleteCandidateSkill(candidateId, requestData, isDraft: boolean) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/delete' + (isDraft ? "/draft" : "") + '/skills/' + this.sessionService.organizationID + "/" + candidateId + "/", JSON.stringify(requestData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public saveProfilePic(candidateId, requestData, isDraft: boolean) {
        let options = new RequestOptions();
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/saveProfilePic' + (isDraft ? "/draft" : "") + "/" + this.sessionService.organizationID + "/" + candidateId + "/", requestData, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    } 

    public fetchMyJobList() {
        let options = new RequestOptions();
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetchRecruiterJobs/' + this.sessionService.organizationID + "/" + this.sessionService.userID + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public parseResume(requestData, candidateId,parseOnly:boolean,uploadResumeOnly:boolean,isDraft:boolean) {
        let options = new RequestOptions();
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/parseResume/' + this.sessionService.organizationID + "/" + candidateId + "/"+parseOnly+"/" +uploadResumeOnly+"/"+isDraft+"/", requestData, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public bulkCandidateUpload(requestData) {
        let options = new RequestOptions();
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/bulkCandidateTemplateUpload', requestData, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public saveCandidateDocument(candidateId, documentTypeID: number, requestData, isDraft: boolean) {
        let options = new RequestOptions();
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/save' + (isDraft ? "/draft" : "") + '/document/' + this.sessionService.organizationID + "/" + candidateId + "/" + this.sessionService.userID + "/" + documentTypeID + "/", requestData, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public deleteCandidateDocument(candidateId, documentId, isDraft: boolean) {
        let options = new RequestOptions();
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/delete' + (isDraft ? "/draft" : "") + '/document/' + this.sessionService.organizationID + "/" + candidateId + "/" + documentId + "/" + this.sessionService.userID + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public draftToCandidate(candidateId) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/draftToCandidate/' + this.sessionService.organizationID + "/" + candidateId + "/" + this.sessionService.userID + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }

    public uploadFailures(failuresList): Observable<any> {
        const file = this.http.post(
            this.sessionService.orgUrl + '/rest/altone/candidate/bulkCandidateTemplateUploadFailures',
            failuresList).map((response) => {
                return response.json();
            });
        return file;
    }

    public downloadCandidateDocuments(arr) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        let requestObj: DownloadDocumentsRequest = new DownloadDocumentsRequest();
        requestObj.input = arr;
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/download/documents/', JSON.stringify(requestObj), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public saveCandidateDetails(candidateId, requestData, isDraft: boolean, section) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/save/?id=' + candidateId + '&type=' 
            + (isDraft ? 'draft' : 'candidate') + (section==undefined ? '' : ('&section=' + section)), JSON.stringify(requestData), options).
            map((res) => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public saveCompleteCandidateDetails(candidateId, requestData, isDraft: boolean, section, inReview) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        let url = undefined;
        if(!inReview) {
            url = this.sessionService.orgUrl + '/rest/altone/candidate/save/?id=' + candidateId + '&type=draftToCandidate' + (section==undefined ? '' : ('&section=' + section));
        } else {
            url = this.sessionService.orgUrl + '/rest/altone/candidate/save/?id=' + candidateId + '&type=candidate' + (section==undefined ? '' : ('&section=' + section));
        }
        return this.http.post(url, JSON.stringify(requestData), options).
            map((res) => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public saveCandidateDetailsFromDraft(candidateId, requestData, isDraft: boolean, section) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/save/?id=' + candidateId + '&type=draftToCandidate' + (section==undefined ? '' : ('&section=' + section)), JSON.stringify(requestData), options).
            map((res) => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public saveCandidateDetailsReviewCandidate(candidateId, requestData, isDraft: boolean, section) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/save/?id=' + candidateId + '&type=draftToCandidate' + (section==undefined ? '' : ('&section=' + section)), JSON.stringify(requestData), options).
            map((res) => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

}

export class DownloadDocumentsRequest {
    input: String[];
}