import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { CandidateController } from '../candidate-interfaces';

@Component({
  selector: 'alt-candidate-work-details',
  templateUrl: './work-details.component.html',
})
export class WorkDetailsComponent implements OnInit {
  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;

  subscription;
  industries: String[];

  totalExperienceYearsList: any[] = [];
  totalExperienceMonthsList: any[] = [];
  currencyList: any[] = [];
  denominationList: any[] = [];

  constructor(
    public formBuilder: FormBuilder,
    private notificationService: NotificationService) {

    for (let x = 0; x < 21; x++) {
      this.totalExperienceYearsList.push({ label: x + "", value: x });
    }

    for (let x = 0; x < 12; x++) {
      this.totalExperienceMonthsList.push({ label: x + "", value: x });
    }

    this.currencyList.push({ label: 'INR', value: 'INR' });
    this.currencyList.push({ label: 'USD', value: 'USD' });

    this.denominationList.push({ label: 'Thousand', value: 'Thousand' });
    this.denominationList.push({ label: 'Lakh', value: 'Lakh' });
    this.denominationList.push({ label: 'Crore', value: 'Crore' });
  }
  clearAll(event) {
    event.stopPropagation();
    this.candidateController.clearAllWork();
  }
  continue() {
    this.candidateController.continueWork();
  }

  ngOnInit() {
  }

  addEmploymentHistory() {
    this.candidateController.addWorkHistory();
  }

  deleteEmploymentHistory(index: number) {
    this.candidateController.removeWorkHistory(index);
  }

  bindTotalExperienceYearsValue(total) {
    this.formGroup.controls.totalExperienceYears.setValue(total.value);
  }
  bindTotalExperienceMonthsValue(total) {
    this.formGroup.controls.totalExperienceMonths.setValue(total.value);
  }
  bindCurrencyValue(currency, item: FormGroup) {
    item.controls.ctcCurrency.setValue(currency.value);
  }
  bindDenominationValue(denomination, item: FormGroup) {
    item.controls.ctcDenomination.setValue(denomination.value);
  }

  setStartDate(startDate: Date, item: FormGroup) {
    if (startDate != null) {
      item.controls['workStartDate'].setValue(startDate);
    } else {
      item.controls['workStartDate'].reset();
    }
  }

  setEndDate(endDate: Date, item: FormGroup) {
    if (endDate != null) {
      item.controls['workEndDate'].setValue(endDate);
    } else {
      item.controls['workEndDate'].reset();
    }
  }

  clearEndDate(item: FormGroup) {
    if (item != null) {
      let count: number = 0;
      for (let obj of this.formGroup.controls.employmentHistory.value) {
        if (obj.currentlyWorkHere) {
          count++;
        }
      }
      if (count > 1) {
        item.controls.currentlyWorkHere.setValue(false);
        this.notificationService.setMessage(NotificationSeverity.WARNING, "You can only work for single company at single time");
      } else {
        if (item.controls.currentlyWorkHere.value == true) {
          item.controls.endDateDD.setValue('');
          item.controls.endDateMM.setValue('');
          item.controls.endDateYYYY.setValue('');
        }
      }
    }
  }
}
