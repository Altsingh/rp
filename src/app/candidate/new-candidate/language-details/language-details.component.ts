import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Form, FormBuilder, FormGroup, FormControl, FormsModule, FormArray } from '@angular/forms';
import { SessionService } from '../../../session.service';
import { JsonpModule } from '@angular/http';
import { CandidateController } from '../candidate-interfaces';
import { NewCandidateService } from '../new-candidate.service';
import { Subscription } from 'rxjs/Subscription';
import { NotificationService,NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { NewJobService } from '../../../job/new-job/new-job.service';



@Component({
  selector: 'alt-language-details',
  templateUrl: './language-details.component.html',
  styleUrls: ['./language-details.component.css'],
  providers: [NewJobService]

})
export class LanguageDetailsComponent implements OnInit {
  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;

  languagesList: any[] = [];
  languageList: any[] = [];

  subscription : Subscription;



  constructor(
    public formBuilder: FormBuilder,
    private sessionService: SessionService,
    private newCandidateService: NewCandidateService,
    private notificationService: NotificationService,
    private newJobService: NewJobService
  ) {

  }
  continue() {
    this.candidateController.continueLanguageSet();
  }

  clearAll(event) {
    event.stopPropagation();
    this.candidateController.clearAllLanguageSet();
  }

  
  addlanguage(language) {
    
    if (language != null) {
      this.notificationService.clear();

      for (let c = 0; c < this.languagesList.length; c++) {
        let languageN = this.languagesList[c];
        if (languageN.languageName.toLowerCase() == language.trim().toLowerCase()) {
          this.formGroup.get('languageInput').setValue("");
          this.notificationService.addMessage(NotificationSeverity.WARNING, language +" has already been added!");
           return;
        }
      }

      if (language.trim() !== '') {
        const control = <FormArray>this.formGroup.controls['languages'];
        control.push(this.formBuilder.group({
          languageId: '0',
          languageName: language.trim()
        }));

        this.languagesList.push({
          skillId: '0',
          skillName: language.trim()
        });
      }
      
  setTimeout(() => { this.formGroup.get('languageInput').setValue(""); }, 50);
    this.languageList = [];
    }

  }

  syncList() {
    
    this.languagesList = [];

    let languageArray = <FormArray>this.formGroup.controls.languages;

    for (let x = 0; x < languageArray.length; x++) {

      let languageGroup = <FormGroup>languageArray.at(x);

      this.languagesList.push({
        languageId: languageGroup.controls.languageId.value,
       languageName: languageGroup.controls.languageName.value
      });
    }


  }

    

  


  removelanguage(i: number) {
    this.candidateController.removeLanguage(i);
  }

  ngOnInit() {
    this.formGroup.controls.languageInput.valueChanges.subscribe((data) => {
      if (data != null && data.trim() != '') {
        this.newJobService.getLanguagesByPattern(data, 10).subscribe((out) => {
          this.languageList = out.responseData;
          if (this.languageList.length == 0) {
            this.languageList = [data];
          } else {
            let found = false;
            for (let x = 0; x < this.languageList.length; x++) {
              let skillStr = <string>this.languageList[x];
              let skillInp = <string>data;
              if (skillStr.toLowerCase() == data.toLowerCase()) {
                found = true;
                break;
              }
            }
            if (!found) {
              this.languageList.push(data);
            }
          }
        });
      } else {
        this.languageList = [];
      }
    });
    this.subscription = this.formGroup.valueChanges.subscribe((data) => {
      const control = <FormArray>this.formGroup.controls['languages'];
      if (this.languagesList.length != control.length) {
        this.syncList();
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}



