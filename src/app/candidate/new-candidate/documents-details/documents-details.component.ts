import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CandidateController } from '../candidate-interfaces';

@Component({
  selector: 'alt-candidate-documents-details',
  templateUrl: './documents-details.component.html'
})
export class DocumentsDetailsComponent implements OnInit {
  lable: string = "Continue";
  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;


  constructor() { }

  ngOnInit() {
  }

  continue() {
    this.candidateController.continueAttachDocument();
  }

  download(index: number) {
    this.candidateController.downloadDocument(index);
  }
  upload(event, index: number) {

  }
  delete(index: number) {
    this.candidateController.deleteDocument(index);
  }

  documentChange(event, index: number, documentTypeID: number) {
    
    this.candidateController.uploadDocument(event, index, documentTypeID);
  }

}
