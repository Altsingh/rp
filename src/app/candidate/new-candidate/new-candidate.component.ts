import { Location } from '@angular/common';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidateAddressTO } from 'app/common/CandidateAddressTO.model';
import { EducationTO } from 'app/common/EducationTO.model';
import { SaveCandidateDetailsTO } from 'app/common/SaveCandidateDetailsTO.model';
import { SkillTO } from 'app/common/SkillTO.model';
import { WorkExpTO } from 'app/common/WorkExpTO.model';
import { Subscription } from 'rxjs/Subscription';
import { CommonService } from '../../common/common.service';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { SessionService } from '../../session.service';
import { ValidationService } from '../../shared/custom-validation/validation.service';
import { FormatCandidatecodeParamPipe } from '../../shared/format-candidatecode-param.pipe';
import { FormatCapitalizePipe } from '../../shared/format-capitalize.pipe';
import { CandidateService } from '../candidate.service';
import { CandidateController } from './candidate-interfaces';
import { NewCandidateService } from './new-candidate.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { TranslateService } from '@ngx-translate/core';
import { DocumentUploadUtilityService, DocumentValidationOptions, UploadDocumentValidationResult, DocumentValidationErrorCodes } from 'app/shared/services/document-upload-utility/document-upload-utility.service';
import { SelectItem } from '../../common/orgunit-master';

@Component({
  selector: 'alt-new-candidate',
  templateUrl: './new-candidate.component.html',
  styleUrls: ['./new-candidate.component.css'],
  providers: [NewCandidateService]
})
export class NewCandidateComponent implements OnInit, OnDestroy, CandidateController {
  candidateCodeSlug = null;
  showloader: boolean = false;

  sectionNumbers: SectionNumber[] = [];
  currentSectionNumber: number = 1;
  sectionCount: number = 2;

  formatCandidatecodeParamPipe: FormatCandidatecodeParamPipe = new FormatCandidatecodeParamPipe();
  formatCapitalizePipe: FormatCapitalizePipe = new FormatCapitalizePipe();

  isEdit: boolean = false;
  isDraft: boolean = false;
  isInReview: boolean = false;
  isBasicDetailLoaded=false;

  @ViewChild('profilePic')
  profilePicInput: ElementRef;

  @ViewChild('resumeFile')
  resumeFileInput: ElementRef;

  profilePicURL: string = ''

  resumeUploaded: boolean = false;
  resumeHTML: SafeHtml = '';
  enableResumeParsing: boolean = false;
  timeToParse: any = '';
  resumeServerPath = '';
  tempCandidateCode = '';
  isParseResumeRedirect = false;
  jobCode:any;
  reqID:any;
  roleName:any;

  parsingResume: boolean = false;
  totalcompleted: number = 0;
  candidateController: CandidateController;
  createCandidateSectionActive: boolean = true;
  createCandidateSectionBlock: boolean = false;
  basicSectionActive: boolean = false;
  basicSectionBlock: boolean = false;
  basicSectionDone: boolean = false;
  personalSectionActive: boolean = false;
  personalSectionBlock: boolean = false;
  personalSectionDone: boolean = false;
  workSectionActive: boolean = false;
  workSectionBlock: boolean = false;
  workSectionDone: boolean = false;
  internshipSectionActive: boolean = false;
  internshipSectionBlock: boolean = false;
  internshipSectionDone: boolean = false;
  educationSectionActive: boolean = false;
  educationSectionBlock: boolean = false;
  educationSectionDone: boolean = false;
  skillsSectionActive: boolean = false;
  skillsSectionBlock: boolean = false;
  skillsSectionDone: boolean = false;
  languagesSectionActive: boolean = false;
  languagesSectionBlock: boolean = false;
  languagesSectionDone: boolean = false;
  documentSectionActive: boolean = false;
  documentSectionBlock: boolean = false;
  documentSectionDone: boolean = false;
  voilaSectionActive: boolean = false;
  voilaSectionBlock: boolean = false;
  savingAsCandidate: boolean = false;

  security: any;
  allSubscriptions: Subscription[] = [];

  genderList: any[];

  industryList: any[];

  functionalAreaList: any[];

  sourceTypeList: any[] = [];
  sourceTypeSysContentType: string = '';
  sourceCodeList: any[] = [];
  sourceNameEnable: boolean = false;
  sourceNameList: any[] = [];
  actualSourceName: string = '';

  daysList: any[] = [];
  monthsList: Month[] = [];
  yearList: any[] = [];
  myJobsList: any[] = [];

  documentsList: any[];

  candidateForm: FormGroup;

  fileSize: number;
  recruiterSourceTypeID: any;
  recruiterSourceUserID: any;

  selectedSourceTypeID: any;
  selectedSourceCodeID: any;
  replaceState = false;
  referCandidate : boolean = false;
  ijpCandidate : boolean = false;
  selectedSourceName : string ='';


  constructor(
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private candidateService: CandidateService,
    private newCandidateService: NewCandidateService,
    private notificationService: NotificationService,
    private route: ActivatedRoute,
    private sessionService: SessionService,
    private location: Location,
    private router: Router,
    private i18UtilService: I18UtilService,
    private documentUploadUtilityService: DocumentUploadUtilityService
  ) {

    if(this.route.snapshot.routeConfig.path.includes('refer')){
      this.referCandidate = true;
    }
    if(this.route.snapshot.routeConfig.path.includes('ijp')){
      this.ijpCandidate = true;
    }
    this.route.params.subscribe((params) => {
      this.reqID = this.route.snapshot.queryParams['reqID'];
      this.jobCode = this.route.snapshot.queryParams['jobCode'];
    });
    this.constructorFunction();
  }

  constructorFunction() {
    this.replaceState = true;
    this.sectionCount = 0;
    this.candidateController = this;
    this.security = {};
    this.commonService.hideRHS();
    this.initFormSecurity();
    this.initGenderList();
    this.initIndustryList();
    this.initFunctionalAreaList();
    this.initMyJobList();

    this.candidateForm = this.formBuilder.group({

      candidateId: '0',
      candidateCode: '',
      resumeUploaded: false,

      createCandidateSection: this.formBuilder.group({
        taggedOrUntagged: '',
        jobTitle: ''
      }),

      basicSection: this.formBuilder.group({
        firstName: '',
        middleName: '',
        lastName: '',
        internationalID: ['', [ValidationService.ThaiIDValidator]],
        mobileNumber: ['', [ValidationService.MobileValidator]],
        primaryEmail: ['', [ValidationService.emailValidator]],
        secondaryEmail: ['', [ValidationService.emailValidator]],
        candidateAge: new FormControl({ value: '', disabled: true }),
        dateOfBirth: new FormControl,
        gender: '',
        panNumber: ['', [ValidationService.PanNoValidator]],
        voterId: '',
        passportNumber: ['', [ValidationService.PassportValidator, Validators.minLength(8), Validators.maxLength(8)]],
        aadhaarNumber: ['', [ValidationService.AdharCardValidator, Validators.minLength(12), Validators.maxLength(12)]],
        industry: '',
        functionalArea: '',
        sourceType: '',
        sourceCode: '',
        sourceName: ''
      }),

      personalSection: this.formBuilder.group({
        addressLine1: '',
        addressLine2: '',
        city: '',
        state: '',
        country: '',
        pinCode: '',
        permanentAddressLine1: '',
        permanentAddressLine2: '',
        permanentCity: '',
        permanentState: '',
        permanentCountry: '',
        permanentPinCode: '',
        facebookProfile: '',
        twitterProfile: '',
        linkedInProfile: '',
        googlePlusProfile: '',
        requiredPermanentAddress: '',
        sameAddress: ''
      }),

      workSection: this.formBuilder.group({
        totalExperienceMonths: null,
        totalExperienceYears: null,
        noticePeriod: ['', [ValidationService.NumberOnly]],
        employmentHistory: this.formBuilder.array([])
      }),

      internshipSection: this.formBuilder.group({
        internshipHistory: this.formBuilder.array([])
      }),

      educationSection: this.formBuilder.group({
        educationHistory: this.formBuilder.array([])
      }),

      skillSetSection: this.formBuilder.group({
        skillInput: '',
        skills: this.formBuilder.array([])
      }),
      languageSetSection: this.formBuilder.group({
        languageInput: '',
        languages: this.formBuilder.array([])
      }),


      documentSection: this.formBuilder.group({
        attachDocuments: this.formBuilder.array([])
      })

    });
    this.candidateForm.valueChanges.subscribe(data => {
      this.profilecompletion();
    })
    this.populateSourceTypeList();

    if(this.referCandidate || this.ijpCandidate){
      let fg = <FormGroup>this.candidateForm.controls.createCandidateSection;
      fg.controls.taggedOrUntagged.setValue('tagged');
    }

    let fg = <FormGroup>this.candidateForm.controls.createCandidateSection;
      fg.controls.jobTitle.setValue(this.reqID);
  }

  datesStorage: any[] = [];

  getGenderValue(genderId) {
    for (let gender of this.genderList) {
      if (gender.id == genderId) {
        return gender;
      }
    }

    return null;
  }
  bindJobTitleValue(requisition) {
    let fg = <FormGroup>this.candidateForm.controls.createCandidateSection;
    fg.controls.jobTitle.setValue(requisition.requisitionId);
  }
  bindDateOfBirth(dob: Date) {
    let fg = <FormGroup>this.candidateForm.controls.basicSection;
    fg.controls.dateOfBirth.setValue(dob);
    this.syncCandidateAge();
  }
  bindGenderValue(gender) {
    let fg = <FormGroup>this.candidateForm.controls.basicSection;
    fg.controls.gender.setValue(gender.id);
  }
  bindIndustryValue(industry) {
    let fg = <FormGroup>this.candidateForm.controls.basicSection;
    fg.controls.industry.setValue(industry.id);
  }
  bindFunctionalAreaValue(funArea) {
    let fg = <FormGroup>this.candidateForm.controls.basicSection;
    fg.controls.functionalArea.setValue(funArea.id);
  }

  clearAll(event) {
    if (this.replaceState) {
      this.location.replaceState('candidate/newcandidate');
      this.clearCreateCandidate(event);
      this.clearAllBasic(event);
      this.clearAllEducation();
      this.clearAllPersonal();
      this.clearAllSkillSet();
      this.clearAllLanguageSet();
      this.clearAllWork();
      this.resumeHTML = '';
    } else {
      this.router.navigateByUrl('candidate/newcandidate');
      this.replaceState = false;
    }

  }

  populateSourceTypeList() {
    if (this.sourceTypeList == undefined || this.sourceTypeList == null || this.sourceTypeList.length == 0) {
      this.candidateService.getSourceTypeList().subscribe(response => {
        if (response != null && response != undefined && response.responseData != null && response.responseData != undefined) {
          this.sourceTypeList = response.responseData;
          this.setDefaultSourceType();
        }
      });
    } else {
      this.setDefaultSourceType();
    }
  }

  setDefaultSourceType() {
    if (this.sourceTypeList != null && this.sourceTypeList != undefined && this.sourceTypeList.length > 0) {
      let candidateCode = this.route.snapshot.params['id'];
      this.selectedSourceName = 'RECRUITER';
      if (this.referCandidate) {
        this.selectedSourceName = 'EMPLOYEE REFERRAL';
      } else if (this.ijpCandidate) {
        this.selectedSourceName = 'IJP REFERRAL';
      } 
      if (candidateCode == undefined || candidateCode == null || candidateCode == '') {
        for (let source of this.sourceTypeList) {
          if (source.sysContentType == this.selectedSourceName) {
            let basicSection = <FormGroup>this.candidateForm.controls.basicSection;
            basicSection.controls.sourceType.setValue(source.id);
            this.sourceTypeSysContentType = source.sysContentType;
            this.recruiterSourceTypeID = source.id;
            this.populateSourceCodeList(source.id, source.sysContentType);
            break;
          }
        }
      }
    }
  }

  populateSourceCodeList(sourceTypeID, sourceType) {
    if (sourceType == 'VENDOR') {
      this.sourceNameEnable = true;
    } else {
      this.sourceNameEnable = false;
    }
    if (this.selectedSourceTypeID != sourceTypeID || this.sourceCodeList == null || this.sourceCodeList == undefined || this.sourceCodeList.length == 0) {
      this.selectedSourceTypeID = sourceTypeID;
      this.candidateService.getSourceCodeList(sourceTypeID, null).subscribe((response) => {
        if (response != null && response != undefined && response.responseData != null && response.responseData != undefined) {
          this.sourceCodeList = response.responseData[0].sourceTOList;
          this.setDefaultSourceCode();
        }
      });
    } else {
      this.setDefaultSourceCode();
    }
  }

  setDefaultSourceCode() {
    if (this.sourceTypeSysContentType == this.selectedSourceName) {
      if (this.sourceCodeList != null && this.sourceCodeList != undefined && this.sourceCodeList.length > 0) {
        let candidateCode = this.route.snapshot.params['id'];
        if (candidateCode == undefined || candidateCode == null || candidateCode == '') {
          for (let source of this.sourceCodeList) {
            if (source.currentUser) {
              let basicSection = <FormGroup>this.candidateForm.controls.basicSection;
              basicSection.controls.sourceCode.setValue(source.sourceID);
              this.recruiterSourceUserID = source.sourceID;
              break;
            }
          }
        }
      }
    }
  }

  populateSourceNameList(sourceTypeID, sourceCodeID, sysContentType) {
    if (this.selectedSourceCodeID != sourceCodeID || this.sourceNameList == null || this.sourceNameList == undefined || this.sourceNameList.length == 0) {
      this.selectedSourceCodeID = sourceCodeID;
      this.candidateService.getSourceNameList(sourceTypeID, sourceCodeID).subscribe(response => {
        if (response != null && response != undefined && response.responseData != null && response.responseData != undefined) {
          this.sourceNameList = response.responseData;
        }
      });
    }
  }

  private initDocumentList(candidateId) {
    let documentListSubscription = this.candidateService.getDocumentList(candidateId, this.isDraft).subscribe(
      response => {
        this.documentsList = response.responseData;
        this.showloader = false;
      }
    );
    this.allSubscriptions.push(documentListSubscription);
  }

  private initMyJobList() {
    if(this.referCandidate || this.ijpCandidate){
      let x = {
        requisitionId: this.reqID,
        requisitionCode: this.jobCode
      };
      this.myJobsList.push(x);
      
    }
    else{
    let subscription = this.newCandidateService.fetchMyJobList().subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            this.myJobsList = response.responseData;
          }
        }
      }
    });
    this.allSubscriptions.push(subscription);
  }
  }
  private initFormSecurity() {
    this.candidateService.clearCandidateFormSecurity();
    let securitySubscription = this.candidateService.getCandidateFormSecurity().subscribe(
      response => {
        this.security = response.responseData[0];
        if (this.security != null) {
          this.security.basicSectionRendered = true;

          let allKeys = Object.keys(this.security);

          for (let x = 0; x < allKeys.length; x++) {
            if (this.security[allKeys[x]] != null && allKeys[x].toLowerCase().endsWith("label")) {
              if (allKeys[x] == 'companyCTCLabel'
                || allKeys[x] == 'panNumberLabel'
                || allKeys[x] == 'dateOfBirthLabel') {
                continue;
              }
              let allKeysTemp = this.formatCapitalizePipe.transform(this.security[allKeys[x]]);
              if (allKeysTemp != null) {
                this.security[allKeys[x]] = allKeysTemp.trim();
              }
            }
          }


          this.security.createCandidateContinueRendered = true;
          this.security.createCandidateContinueDisabled = false;
          this.security.createCandidateClearAllRendered = true;

          this.security.basicClearAllRendered = true;
          this.security.basicContinueDisabled = false;
          this.security.basicContinueRendered = true;

          this.security.personalClearAllRendered = true;
          this.security.personalContinueDisabled = false;
          this.security.personalContinueRendered = true;

          this.security.workClearAllRendered = true;
          this.security.workContinueDisabled = false;
          this.security.workContinueRendered = true;

          this.security.internshipClearAllRendered = true;
          this.security.internshipContinueDisabled = false;
          this.security.internshipContinueRendered = true;

          this.security.educationClearAllRendered = true;
          this.security.educationContinueDisabled = false;
          this.security.educationContinueRendered = true;

          this.security.skillsClearAllRendered = true;
          this.security.skillsContinueDisabled = false;
          this.security.skillsContinueRendered = true;

          this.security.languagesClearAllRendered = true;
          this.security.languagesContinueDisabled = false;
          this.security.languagesContinueRendered = true;




        }
      }
    );
    this.allSubscriptions.push(securitySubscription);
  }

  private initGenderList() {
    let genderListSubscription = this.candidateService.getGenderList().subscribe(
      response => {
        this.genderList = response.responseData;

      }
    );
    this.allSubscriptions.push(genderListSubscription);
  }

  private initIndustryList() {
    let industryListSubscription = this.candidateService.getIndustryList().subscribe(
      response => {
        this.industryList = response.responseData;
      }
    );
    this.allSubscriptions.push(industryListSubscription);
  }

  private initFunctionalAreaList() {
    let functionalAreaListSubscription = this.candidateService.getFunctionalAreaList().subscribe(
      response => {
        this.functionalAreaList = response.responseData;
      }
    );
    this.allSubscriptions.push(functionalAreaListSubscription);
  }


  nextSection() {
    this.currentSectionNumber++;
    if (this.currentSectionNumber > this.sectionNumbers.length) {
      this.currentSectionNumber = 1;
    }
  }

  public continueCreateCandidate() {
    this.notificationService.clear();
    let createCandidateGroup = <FormGroup>this.candidateForm.controls.createCandidateSection;
    this.nextSection();
    this.createCandidateSectionActive = false;
    createCandidateGroup.markAsPristine();
  }
  public clearCreateCandidate(event) {
    event.stopPropagation();
    this.candidateForm.controls.createCandidateSection.reset();
    this.candidateForm.controls.createCandidateSection.markAsDirty();
  }

  public continueBasic() {
    this.continueBasicAction();
  }

  public continueBasicAction() {
    this.notificationService.clear();
    this.basicSectionDone = false;

    let requestJSON = this.setBasicDetailsRequestJSON(undefined);
    let valid = this.validateBasicDetails(true, requestJSON, !this.isDraft);

    if (valid) {

      this.basicSectionBlock = true;

      let subscription = this.newCandidateService.saveCandidateDetails(requestJSON.candidateId, requestJSON, this.isDraft, 'basic').subscribe(response => {
        if (response != null && response != undefined) {
          if (response.messageCode.code == 'EC200') {
            let basicGroup = <FormGroup>this.candidateForm.controls.basicSection;
            basicGroup.markAsPristine();
            basicGroup.markAsUntouched();
            let candidateId = response.response.candidateId;
            let candidateCode = response.response.candidateCode;
            this.candidateForm.get('candidateId').patchValue(candidateId);
            this.candidateForm.get('candidateCode').patchValue(candidateCode);
            if (!this.isEdit) {
              this.replaceState = true;
              this.location.replaceState('candidate/newcandidate/' + this.formatCandidatecodeParamPipe.transform(candidateCode));
            }
            this.nextSection();
            this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.basicSectionActive = false
            this.basicSectionDone = true;
          } else if (response.messageCode.code == 'EC201') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else if (response.messageCode.code == 'EC202') {
            this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, res);
            });
          } else if (response.messageCode.code == 'EC400') {
            this.i18UtilService.get('candidate.error.invalidThaiID').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }  else {
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        } else {
          this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
        this.basicSectionBlock = false;
      },
        () => {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          this.basicSectionBlock = false;
        });

      this.allSubscriptions.push(subscription);
    }
  }

  calculate_age(dateOfBirth: Date) {
    let today_date = new Date();
    let today_year = today_date.getFullYear();
    let today_month = today_date.getMonth();
    let today_day = today_date.getDate();

    let birth_year = dateOfBirth.getFullYear();
    let birth_month = dateOfBirth.getMonth();
    let birth_day = dateOfBirth.getDate();

    let age = today_year - birth_year;

    if (today_month < (birth_month)) {
      age--;
    }
    if (((birth_month) == today_month) && (today_day < birth_day)) {
      age--;
    }
    return age;
  }

  public clearAllPersonal() {
    this.candidateForm.controls.personalSection.reset();
    this.candidateForm.controls.personalSection.markAsDirty();
  }
  public continuePersonal() {

    this.notificationService.clear();
    this.personalSectionDone = false;

    let candidateId = this.candidateForm.controls.candidateId.value;
    if (candidateId == 0) {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    let requestJSON = this.setPersonalDetailsRequestJSON(undefined);
    let valid = this.validatePersonalDetails(true, requestJSON, !this.isDraft);
    if (valid) {
      this.personalSectionBlock = true;

      let subscription = this.newCandidateService.saveCandidateDetails(candidateId, requestJSON, this.isDraft, 'personal').subscribe(response => {

        if (response != null && response != undefined) {
          if (response.messageCode.code == 'EC200') {
            let personalFormGroup = <FormGroup>this.candidateForm.controls.personalSection;
            personalFormGroup.markAsPristine();
            personalFormGroup.markAsUntouched();
            this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.nextSection();
            this.personalSectionActive = false;
            this.personalSectionDone = true;
          } else if (response.messageCode.code == 'EC201') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else {
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        } else {
          this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
        this.personalSectionBlock = false;
      },
        () => {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          this.personalSectionBlock = false;
        });

      this.allSubscriptions.push(subscription);
    }

  }

  public addWorkHistory() {

    const workGroup = <FormGroup>this.candidateForm.controls.workSection;
    const workHistory = <FormArray>workGroup.controls['employmentHistory'];

    workHistory.push(this.formBuilder.group({
      workDetailId: '0',
      company: '',
      jobTitle: '',
      workStartDate: new FormControl(),
      workEndDate: new FormControl(),
      currentlyWorkHere: '',
      ctcCurrency: 'INR',
      ctcAmount: ['', [ValidationService.NumericWithDecimal]],
      ctcDenomination: 'Thousand',
      description: '',
    }));

  }

  public addInternshipHistory() {

    const internshipGroup = <FormGroup>this.candidateForm.controls.internshipSection;
    const internshipHistory = <FormArray>internshipGroup.controls['internshipHistory'];

    internshipHistory.push(this.formBuilder.group({
      internshipDetailId: '0',
      company: '',
      jobTitle: '',
      internshipStartDate: new FormControl(),
      internshipEndDate: new FormControl(),
      currentlyWorkHere: '',
      description: '',
    }));

  }
  public removeWorkHistory(index: number) {
    const workFormGroup = <FormGroup>this.candidateForm.controls.workSection;
    const workHistory = <FormArray>workFormGroup.controls.employmentHistory;

    if (index < workHistory.length) {

      let historyGroup = <FormGroup>workHistory.at(index);

      if (historyGroup.controls.workDetailId.value == '0') {
        workHistory.removeAt(index);
      } else {

        let candidateId = this.candidateController.getCandidateId();
        let requestJSON = {
          totalExperienceMonths: 0,
          totalExperienceYears: 0,
          noticePeriod: 0,
          employmentHistory: [{
            workDetailId: historyGroup.controls.workDetailId.value,
            company: '',
            jobTitle: '',
            startDate: '',
            endDate: '',
            currentlyWorkHere: '',
            ctcCurrency: '',
            ctcAmount: '',
            ctcDenomination: '',
            description: '',
          }],
          security: this.security,
          internship: false
        };

        let subscription = this.newCandidateService.deleteCandidateWork(candidateId, requestJSON, this.isDraft).subscribe(response => {

          let deleted: boolean = false;

          if (response) {
            if (response.responseData) {
              if (response.responseData[0]) {
                if (response.responseData[0].workDetailsIds.length > 0) {

                  let responseId = response.responseData[0].workDetailsIds[0];

                  if (responseId == historyGroup.controls.workDetailId.value) {
                    deleted = true;
                    workHistory.removeAt(index);
                  }
                }
              }
            }
          }
          if (!deleted) {
            this.i18UtilService.get('candidate.error.workDetailDeletefailed').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          } else {
            this.i18UtilService.get('candidate.success.workDetailDeleted').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
          }
        });
        this.allSubscriptions.push(subscription);
      }
    }
  }


  public removeInternshipHistory(index: number) {
    const internshipFormGroup = <FormGroup>this.candidateForm.controls.internshipSection;
    const internshipHistory = <FormArray>internshipFormGroup.controls.internshipHistory;

    if (index < internshipHistory.length) {

      let historyGroup = <FormGroup>internshipHistory.at(index);

      if (historyGroup.controls.internshipDetailId.value == '0') {
        internshipHistory.removeAt(index);
      } else {

        let candidateId = this.candidateController.getCandidateId();
        let requestJSON = {

          employmentHistory: [{
            workDetailId: historyGroup.controls.internshipDetailId.value,
            company: '',
            jobTitle: '',
            startDate: '',
            endDate: '',
            currentlyWorkHere: '',
            description: '',
          }],
          security: this.security,
          internship: true
        };

        let subscription = this.newCandidateService.deleteCandidateWork(candidateId, requestJSON, this.isDraft).subscribe(response => {

          let deleted: boolean = false;

          if (response != null && response.messageCode != null && response.messageCode.code === 'EC200') {
            deleted = true;
            internshipHistory.removeAt(index);

          }

          if (!deleted) {
            this.i18UtilService.get('candidate.success.internshipDetailDeletefailed').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          } else {
            this.i18UtilService.get('candidate.success.internshipDetailDeleted').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
          }
        });
        this.allSubscriptions.push(subscription);
      }
    }
  }


  public clearAllWork() {
    let workFormGroup = <FormGroup>this.candidateForm.controls.workSection;
    workFormGroup.reset();
    workFormGroup.controls.noticePeriod.setValue('');

    let employmentHistory = <FormArray>workFormGroup.controls.employmentHistory;

    while (employmentHistory.length != 0) {
      employmentHistory.removeAt(employmentHistory.length - 1);
    }

    workFormGroup.reset();
    workFormGroup.markAsDirty();
  }

  public clearAllInternship() {
    let internshipFormGroup = <FormGroup>this.candidateForm.controls.internshipSection;
    internshipFormGroup.reset();

    let internshipHistory = <FormArray>internshipFormGroup.controls.internshipHistory;

    while (internshipHistory.length != 0) {
      internshipHistory.removeAt(internshipHistory.length - 1);
    }

    internshipFormGroup.reset();
    internshipFormGroup.markAsDirty();
  }

  public continueWork() {
    this.notificationService.clear();
    this.workSectionDone = false;

    let candidateId = this.candidateForm.controls.candidateId.value;
    if (candidateId == 0) {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    let requestJSON = this.setWorkDetailsRequestJSON(undefined);
    let valid = this.validateWorkDetails(true, requestJSON, !this.isDraft);

    if (valid) {

      this.workSectionBlock = true;
      let subscription = this.newCandidateService.saveCandidateDetails(candidateId, requestJSON, this.isDraft, 'work').subscribe(response => {

        let workFormGroup = <FormGroup>this.candidateForm.controls.workSection;
        let employmentHistory = <FormArray>workFormGroup.controls.employmentHistory;

        if (response != null && response != undefined) {
          if (response.messageCode.code == 'EC200') {
            if (response.response.workExpTOs != null && response.response.workExpTOs != undefined) {
              let count = 0;
              for (let workExpTO of response.response.workExpTOs) {
                let history = <FormGroup>employmentHistory.at(count);
                history.controls.workDetailId.setValue(workExpTO.candidateEmpDetailId);
                count++;
              }
            }
            workFormGroup.markAsPristine();
            workFormGroup.markAsUntouched();
            this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.nextSection();
            this.workSectionActive = false;
            this.workSectionDone = true;
          } else if (response.messageCode.code == 'EC201') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else if (response.messageCode.code == 'EC202') {
            this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, res);
            });
          }  else {
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        } else {
          this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
        this.workSectionBlock = false;
      },
        () => {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          this.workSectionBlock = false;
        });

      this.allSubscriptions.push(subscription);
    }
  }

  public continueInternship() {
    this.notificationService.clear();
    this.internshipSectionDone = false;

    let candidateId = this.candidateForm.controls.candidateId.value;
    if (candidateId == 0) {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicWorkLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    let requestJSON = this.setInternshipDetailsRequestJSON(undefined);
    let valid = this.validateInternshipDetails(true, requestJSON, !this.isDraft);

    if (valid) {
      this.internshipSectionBlock = true;
      let subscription = this.newCandidateService.saveCandidateDetails(candidateId, requestJSON, this.isDraft, 'internship').subscribe(response => {

        if (response != null && response != undefined) {
          if (response.messageCode.code == 'EC200') {
            let internshipFormGroup = <FormGroup>this.candidateForm.controls.internshipSection;
            let employmentHistory = <FormArray>internshipFormGroup.controls.internshipHistory;
            if (response.response.internshipWorkExpTOs != null && response.response.internshipWorkExpTOs != undefined) {
              let count = 0;
              for (let workExpTO of response.response.internshipWorkExpTOs) {
                let history = <FormGroup>employmentHistory.at(count);
                history.controls.internshipDetailId.setValue(workExpTO.candidateEmpDetailId);
                count++;
              }
            }
            internshipFormGroup.markAsPristine();
            internshipFormGroup.markAsUntouched();
            this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.nextSection();
            this.internshipSectionActive = false;
            this.internshipSectionDone = true;
          } else if (response.messageCode.code == 'EC201') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else if (response.messageCode.code == 'EC202') {
            this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, res);
            });
          } else {
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        } else {
          this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
        this.internshipSectionBlock = false;
      },
        () => {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          this.workSectionBlock = false;
        });

      this.allSubscriptions.push(subscription);
    }

  }

  public addEducationHistory() {
    const educationGroup = <FormGroup>this.candidateForm.controls.educationSection;
    const educationHistory = <FormArray>educationGroup.controls['educationHistory'];

    educationHistory.push(this.formBuilder.group({
      eduDetailId: '0',
      degree: '',
      educationStartDate: new FormControl(),
      educationEndDate: new FormControl(),
      institute: '',
      gradePercentage: '',
    }));

  }


  public removeEducationHistory(index: number) {
    const educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
    const educationHistory = <FormArray>educationFormGroup.controls.educationHistory;

    if (index < educationHistory.length) {

      let historyGroup = <FormGroup>educationHistory.at(index);

      if (historyGroup.controls.eduDetailId.value == '0') {
        educationHistory.removeAt(index);
      } else {

        let candidateId = this.candidateController.getCandidateId();
        let requestJSON = {
          educationHistory: [{
            candidateEduDetailId: historyGroup.controls.eduDetailId.value,
            degree: '',
            batchStartDate: '',
            batchEndDate: '',
            institute: '',
            gradePercentage: ''
          }],
          security: this.security
        };

        let subscription = this.newCandidateService.deleteCandidateEducation(candidateId, requestJSON, this.isDraft).subscribe(response => {

          let deleted: boolean = false;

          if (response) {
            if (response.responseData) {
              if (response.responseData[0]) {
                if (response.responseData[0].educationDetailIds.length > 0) {
                  let responseId = response.responseData[0].educationDetailIds[0];

                  if (responseId == historyGroup.controls.eduDetailId.value) {
                    deleted = true;
                    educationHistory.removeAt(index);
                  }
                }
              }
            }
          }
          if (!deleted) {
            this.i18UtilService.get('candidate.success.eduDetailDeleteFailed').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          } else {
            this.i18UtilService.get('candidate.success.eduDetailDeleted').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
          }
        });

        this.allSubscriptions.push(subscription);

      }
    }

  }

  public clearAllBasic(event) {
    event.stopPropagation();
    this.candidateForm.controls.basicSection.reset();
    this.profilePicURL = '';
    if (this.security.basicUploadPhotoRendered) {
      if (this.profilePicInput != null && this.profilePicInput != undefined) {
        this.profilePicInput.nativeElement.value = '';
      }
    }
    this.sourceTypeSysContentType = '';
    this.sourceCodeList = [];
    this.sourceNameEnable = false;
    this.sourceNameList = [];
    this.candidateForm.controls.basicSection.markAsDirty();
    this.populateSourceTypeList();
  }

  public clearAllEducation() {
    let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
    educationFormGroup.reset();
    let educationHistory = <FormArray>educationFormGroup.controls.educationHistory;

    while (educationHistory.length != 0) {
      educationHistory.removeAt(educationHistory.length - 1);
    }

    educationFormGroup.reset();
    educationFormGroup.markAsDirty();

  }
  public continueEducation() {
    this.notificationService.clear();
    this.educationSectionDone = false;

    let candidateId = this.candidateForm.controls.candidateId.value;
    if (candidateId == 0) {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    let requestJSON = this.setEducationDetailsRequestJSON(undefined);
    let valid = this.validateEducationDetails(true, requestJSON, !this.isDraft);
    if (valid) {
      this.educationSectionBlock = true;

      let subscription = this.newCandidateService.saveCandidateDetails(candidateId, requestJSON, this.isDraft, 'education').subscribe(response => {

        if (response != null && response != undefined) {
          if (response.messageCode.code == 'EC200') {
            let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
            let educationHistory = <FormArray>educationFormGroup.controls.educationHistory;
            if (response.response.educationTOs != null && response.response.educationTOs != undefined) {
              let count = 0;
              for (let educationTO of response.response.educationTOs) {
                let history = <FormGroup>educationHistory.at(count);
                history.controls.eduDetailId.setValue(educationTO.candidateEduDetailId);
                count++;
              }
            }

            educationFormGroup.markAsPristine();
            educationFormGroup.markAsUntouched();
            this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.nextSection();
            this.educationSectionActive = false;
          } else if (response.messageCode.code == 'EC201') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else if (response.messageCode.code == 'EC202') {
            this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, res);
            });
          } else {
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        } else {
          this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
        this.educationSectionBlock = false;
      },
        () => {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          this.educationSectionBlock = false;
        });

      this.allSubscriptions.push(subscription);
    }

  }

  validateDocument(valid): boolean {
    if (this.security.documentSectionRendered == true && this.security.documentSectionRequired) {
      for (let document of this.documentsList) {
        if ((document.mandatory) && (document.fileName == null)) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': "Document Section", 'label': document.documentName }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

      }
    }
    return valid;
  }

  public removeSkill(index: number) {

    const group = <FormGroup>this.candidateForm.controls.skillSetSection;
    const array = <FormArray>group.controls.skills;

    if (index < array.length) {
      array.removeAt(index);

    }
  }

  public removeLanguage(index: number) {

    const group = <FormGroup>this.candidateForm.controls.languageSetSection;
    const array = <FormArray>group.controls.languages;

    if (index < array.length) {
      array.removeAt(index);
    }
  }

  public clearAllSkillSet() {
    this.skillsSectionDone = false;
    const group = <FormGroup>this.candidateForm.controls.skillSetSection;
    const array = <FormArray>group.controls.skills;
    group.reset();
    while (array.length != 0) {
      array.removeAt(array.length - 1);
    }
    group.markAsDirty();
  }

  public clearAllLanguageSet() {
    this.languagesSectionDone = false;
    const group = <FormGroup>this.candidateForm.controls.languageSetSection;
    const array = <FormArray>group.controls.languages;
    group.reset();
    while (array.length != 0) {
      array.removeAt(array.length - 1);
    }
    group.markAsDirty();

  }

  public continueSkillSet() {
    this.notificationService.clear();

    let candidateId = this.candidateForm.controls.candidateId.value;
    if (candidateId == 0) {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    let requestJSON = this.setSkillDetailsRequestJSON(undefined);


    this.skillsSectionBlock = true;

    let subscription = this.newCandidateService.saveCandidateDetails(candidateId, requestJSON, this.isDraft, 'skill').subscribe(response => {
      if (response != null && response != undefined) {
        if (response.messageCode.code == 'EC200') {
          let skillSetSection = <FormGroup>this.candidateForm.controls.skillSetSection;

          if (response.response.skillTOs != null && response.response.skillTOs != undefined) {
            let newSkills = [];
            for (let skillTO of response.response.skillTOs) {
              newSkills.push({
                skillId: skillTO.skillId,
                skillName: skillTO.skillName
              });
            }
            let newSkillSetSection = {
              skillInput: '',
              skills: newSkills
            }
            skillSetSection.setValue(newSkillSetSection);
          }
          skillSetSection.markAsPristine();
          skillSetSection.markAsUntouched();
          this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
          this.skillsSectionActive = false;
          this.nextSection();
          this.skillsSectionBlock = false;
          this.skillsSectionDone = true;

        } else if (response.messageCode.code == 'EC201') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else if (response.messageCode.code == 'EC202') {
          this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
        } else {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      } else {
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
      this.skillsSectionBlock = false;
    }, () => {
      this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      this.skillsSectionBlock = false;
    });
    this.allSubscriptions.push(subscription);
  }


  public continueLanguageSet() {
    this.notificationService.clear();

    let candidateId = this.candidateForm.controls.candidateId.value;
    if (candidateId == 0) {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    let requestJSON = this.setLanguageDetailsRequestJSON(undefined);

    this.languagesSectionBlock = true;

    let subscription = this.newCandidateService.saveCandidateDetails(candidateId, requestJSON, this.isDraft, 'language').subscribe(response => {
      if (response != null && response != undefined) {
        if (response.messageCode.code == 'EC200') {
          let languageSetSection = <FormGroup>this.candidateForm.controls.languageSetSection;

          if (response.response.languageTOs != null && response.response.languageTOs != undefined) {
            let newLanguages = [];
            for (let skillTO of response.response.languageTOs) {
              newLanguages.push({
                languageId: skillTO.skillId,
                languageName: skillTO.skillName
              });
            }
            let newLanguageSetSection = {
              languageInput: '',
              languages: newLanguages
            }
            languageSetSection.setValue(newLanguageSetSection);
          }
          languageSetSection.markAsPristine();
          languageSetSection.markAsUntouched();
          this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
          this.nextSection();
          this.languagesSectionActive = false;
          this.languagesSectionDone = true;

        } else if (response.messageCode.code == 'EC201') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else if (response.messageCode.code == 'EC202') {
          this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
        } else {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      } else {
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
      this.languagesSectionBlock = false;
    }, () => {
      this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      this.languagesSectionBlock = false;
    });
    this.allSubscriptions.push(subscription);
  }

  getCandidateId() {
    let candidateId = this.candidateForm.controls.candidateId.value;
    return candidateId;
  }
  public continueAttachDocument() {
    let valid = true;
    if (this.security.documentSectionRequired) {
      for (let document of this.documentsList) {
        if ((document.mandatory) && (document.fileName == null)) {
          this.i18UtilService.get('candidate.warning.uploadDocWarning', { 'documentName': document.documentName }).subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
      }
    }
    if (valid) {
      this.notificationService.clear();
      this.documentSectionActive = false;
      this.nextSection();
    }
  }
  public continueVoila() {
    this.notificationService.clear();
    this.createCandidateSectionActive = true;
  }

  public toggleCreateCandidate() {
    this.createCandidateSectionActive = !this.createCandidateSectionActive;
    if (this.createCandidateSectionActive) {
      this.currentSectionNumber = this.getSectionNumber('1');
    }
  }
  public toggleBasic() {
    this.basicSectionActive = !this.basicSectionActive;
    if (this.basicSectionActive) {
      this.currentSectionNumber = this.getSectionNumber('2');
    }
  }
  public togglePersonal() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.personalSectionActive = !this.personalSectionActive;
      if (this.personalSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('3');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }
  public toggleWork() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.workSectionActive = !this.workSectionActive;
      if (this.workSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('4');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }

  public toggleInternship() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.internshipSectionActive = !this.internshipSectionActive;
      if (this.internshipSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('5');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }

  public toggleEducation() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.educationSectionActive = !this.educationSectionActive;
      if (this.educationSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('6');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }
  public toggleSkillSet() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.skillsSectionActive = !this.skillsSectionActive;
      if (this.skillsSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('7');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }

  public toggleLanguageSet() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.languagesSectionActive = !this.languagesSectionActive;
      if (this.languagesSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('8');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }
  public toggleAttachDocument() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.documentSectionActive = !this.documentSectionActive;
      if (this.documentSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('9');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }
  public toggleVoila() {
    if (this.getCandidateId() != null && this.getCandidateId() != '0') {
      this.voilaSectionActive = !this.voilaSectionActive;
      if (this.voilaSectionActive) {
        this.currentSectionNumber = this.getSectionNumber('10');
      }
    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }

  }
  public saveToDrafts() {
  }

  ngOnInit() {
    this.showloader = true;
    this.sectionNumbers = [];
    let fg = <FormGroup>this.candidateForm.controls.createCandidateSection;

    let candidateCode = this.route.snapshot.params['id'];
    if (this.candidateCodeSlug != null) {
      candidateCode = this.candidateCodeSlug;
      this.candidateCodeSlug = null;
    }
    let sessionObject = this.sessionService.dialogData;
    if (sessionObject != null && sessionObject != undefined) {
      if (sessionObject.candidateCode == candidateCode) {
        this.isParseResumeRedirect = true;
        this.timeToParse = sessionObject.timeToParse;
        fg.controls.taggedOrUntagged.setValue(sessionObject.isTagged);
        fg.controls.jobTitle.setValue(sessionObject.jobTitle);
      }
      // this.sessionService.dialogData = null;
    }
    if (typeof candidateCode != "undefined" && candidateCode != '') {
      this.replaceState = false;
      this.enableResumeParsing = false;
      if (candidateCode.indexOf("DRAFT") == 0) {
        this.isDraft = true;
      } else {
        this.isDraft = false;
        this.isEdit = true;
      }

      let subscription = this.newCandidateService.fetchCandidateId(candidateCode, this.isDraft).subscribe(response => {
        let loaded: boolean = false;

        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {

              let candidateId = response.responseData[0];

              this.candidateForm.patchValue({
                candidateId: candidateId,
                candidateCode: candidateCode
              });

              this.loadBasicDetails(candidateId);
              this.loadPersonalDetails(candidateId);
              this.loadWorkDetails(candidateId);
              this.loadInternshipDetails(candidateId);
              this.loadEducationDetails(candidateId);
              this.loadSkillsDetails(candidateId);
              this.loadLanguagesDetails(candidateId);
              this.initDocumentList(candidateId);
              loaded = true;
            } else {
              this.i18UtilService.get('candidate.error.candidateNotFound').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            }
          }
        }
        this.showloader = false;
        if (!loaded) {
          this.i18UtilService.get('candidate.error.candidateDetailsNotFound').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      });

      this.allSubscriptions.push(subscription);

    } else {
      this.isDraft = true;
      this.enableResumeParsing = true;
      this.addEducationHistory();
      this.addWorkHistory();
      this.addInternshipHistory();
      this.initDocumentList(0);
    }

    let _fg = <FormGroup>this.candidateForm.controls.createCandidateSection;

    let _sessionObject = this.sessionService.dialogData;
    if (_sessionObject != null && _sessionObject != undefined) {
      if (_sessionObject.candidateCode == candidateCode) {
        if(_sessionObject.referCandidate){
          this.referCandidate= true;
          let x = {
            requisitionId: _sessionObject.jobTitle,
            requisitionCode: _sessionObject.jobCode
          };
          this.myJobsList.push(x);
        }
        _fg.controls.taggedOrUntagged.setValue(_sessionObject.isTagged);
        _fg.controls.jobTitle.setValue(_sessionObject.jobTitle);
      }
    }

    if (this.sessionService.objectType == 'candidateIdFromJobDetailPage' && this.sessionService.object != null) {
      let fg = <FormGroup>this.candidateForm.controls.createCandidateSection;
      fg.controls.jobTitle.setValue(this.sessionService.object);
      fg.controls.taggedOrUntagged.setValue('tagged');
      this.sessionService.objectType = null;
      this.sessionService.object = null;
    }
  }

  ngOnDestroy(): void {
    if (this.allSubscriptions != null) {
      for (let x = 0; x < this.allSubscriptions.length; x++) {
        this.allSubscriptions[x].unsubscribe();
      }
    }
  }

  loadBasicDetails(candidateId: number) {
    this.isBasicDetailLoaded=false;
    let subscription = this.newCandidateService.fetchCandidateBasic(candidateId, this.isDraft).subscribe((response) => {
      this.isBasicDetailLoaded=true;
      this.resumeServerPath = null;
      this.candidateForm.controls.resumeUploaded.setValue(false);

      let loaded: boolean = false;

      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {

            let basicDetails = response.responseData[0];

            this.isInReview = basicDetails.inReviewTrue;

            this.candidateForm.patchValue({
              candidateCode: basicDetails.candidateCode
            });

            let basicFormGroup = <FormGroup>this.candidateForm.controls.basicSection;
            let createCandidateSection = <FormGroup>this.candidateForm.controls.createCandidateSection;

            let candidateCode = basicDetails.candidateCode;
            let firstName = basicDetails.firstName;
            let middleName = basicDetails.middleName;
            let lastName = basicDetails.lastName;
            let internationalID = basicDetails.internationalID;
            if (internationalID != null) {
              internationalID = internationalID.trim();
            }
            let mobileNumber = basicDetails.mobileNumber;
            if (mobileNumber != null) {
              mobileNumber = mobileNumber.trim();
            }
            let primaryEmail = basicDetails.primaryEmail;
            if (primaryEmail != null) {
              primaryEmail = primaryEmail.trim();
            }
            let secondaryEmail = basicDetails.secondaryEmail;
            if (secondaryEmail != null) {
              secondaryEmail = secondaryEmail.trim();
            }
            let candidateAge = basicDetails.candidateAge;
            let dateOfBirth = basicDetails.dateOfBirth;
            let requisitionId = basicDetails.requisitionId;
            this.profilePicURL = basicDetails.profilePicURL;

            if (!this.isParseResumeRedirect) {
              if (requisitionId != 0) {
                createCandidateSection.controls.jobTitle.setValue(requisitionId);
                createCandidateSection.controls.taggedOrUntagged.setValue('tagged');
              } else {
                createCandidateSection.controls.taggedOrUntagged.setValue('untagged');
              }
            }


            let date: Date;

            if (dateOfBirth != null) {

              try {

                date = new Date(dateOfBirth);
                basicFormGroup.patchValue({
                  dateOfBirth: date
                });

                this.syncCandidateAge();

              } catch (e) {
                console.log("date of birth error");
              }
            }
            let genderId = basicDetails.genderId;
            let panNumber = basicDetails.panNumber;
            if (panNumber != null) {
              panNumber = panNumber.trim();
            }
            let voterId = basicDetails.voterId;
            let passportNumber = basicDetails.passportNumber;
            if (passportNumber != null) {
              passportNumber = passportNumber.trim();
            }
            let aadhaarNumber = basicDetails.aadhaarNumber;
            if (aadhaarNumber != null) {
              aadhaarNumber = aadhaarNumber.trim();
            }
            let industryId = basicDetails.industryId;
            let functionalAreaId = basicDetails.functionalAreaId;
            let sourceType = basicDetails.sourceType;
            let sourceCode = basicDetails.sourceCode;
            let sourceTypeId = basicDetails.sourceTypeId;
            let sourceCodeId = basicDetails.sourceCodeId;
            let sourceNameId = basicDetails.sourceNameId;
            let sourceName = basicDetails.sourceName;
            this.actualSourceName = basicDetails.sourceName;
            let htmlDetailedResume = basicDetails.htmlDetailedResume;
            let resumePath = basicDetails.resumePath;


            if (htmlDetailedResume != null) {
              this.resumeHTML = this.sanitizer.bypassSecurityTrustHtml(htmlDetailedResume);
            }

            if (resumePath != null) {
              this.resumeServerPath = resumePath;
              this.candidateForm.controls.resumeUploaded.setValue(true);
              this.resumeUploaded = this.candidateForm.controls.resumeUploaded.value;
            }

            if (firstName != null && firstName != '') {
              basicFormGroup.patchValue({
                firstName: firstName
              });
            }
            if (middleName != null && middleName != '') {
              basicFormGroup.patchValue({
                middleName: middleName
              });
            }
            if (lastName != null && lastName != '') {
              basicFormGroup.patchValue({
                lastName: lastName
              });
            }
            if (internationalID != null && internationalID != '') {
              basicFormGroup.patchValue({
                internationalID: internationalID
              });
            }
            if (mobileNumber != null && mobileNumber != '') {
              basicFormGroup.patchValue({
                mobileNumber: mobileNumber
              });
            }
            if (primaryEmail != null && primaryEmail != '') {
              basicFormGroup.patchValue({
                primaryEmail: primaryEmail
              });
            }
            if (secondaryEmail != null && secondaryEmail != '') {
              basicFormGroup.patchValue({
                secondaryEmail: secondaryEmail
              });
              basicFormGroup.patchValue({
                candidateAge: candidateAge
              });
            }
            if (genderId != null && genderId != '') {
              basicFormGroup.patchValue({
                gender: genderId
              });
            }
            if (panNumber != null && panNumber != '') {
              basicFormGroup.patchValue({
                panNumber: panNumber
              });
            }
            if (voterId != null && voterId != '') {
              basicFormGroup.patchValue({
                voterId: voterId
              });
            }
            if (passportNumber != null && passportNumber != '') {
              basicFormGroup.patchValue({
                passportNumber: passportNumber
              });
            }
            if (aadhaarNumber != null && aadhaarNumber != '') {
              basicFormGroup.patchValue({
                aadhaarNumber: aadhaarNumber
              });
            }
            if (industryId != null && industryId != '') {
              basicFormGroup.patchValue({
                industry: industryId
              });
            }
            if (functionalAreaId != null && functionalAreaId != '') {
              basicFormGroup.patchValue({
                functionalArea: functionalAreaId
              });
            }
            if (sourceTypeId != null && sourceTypeId != '') {
              basicFormGroup.patchValue({
                sourceType: sourceTypeId
              });
            }

            if (sourceNameId != null && sourceNameId != '' && sourceNameId != 0) {
              basicFormGroup.patchValue({
                sourceName: sourceNameId
              });

              if (sourceCodeId == 0) {
                basicFormGroup.patchValue({
                  sourceCode: sourceNameId
                });
              }
            } else if (sourceName != null) {
              basicFormGroup.patchValue({
                sourceName: sourceName
              });
            }

            if (sourceCodeId != null && sourceCodeId != '' && sourceCodeId != 0) {
              basicFormGroup.patchValue({
                sourceCode: sourceCodeId
              });
            }


            this.syncCandidateAge();


            loaded = true;
            this.basicSectionActive = true;
          }
        }
      }
    },error=>{
      this.isBasicDetailLoaded=true;
    });
    this.allSubscriptions.push(subscription);
  }

  loadPersonalDetails(candidateId: number) {
    let subscription = this.newCandidateService.fetchCandidatePersonal(candidateId, this.isDraft).subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            let personalFormGroup = <FormGroup>this.candidateForm.controls.personalSection;
            let personalDetails = response.responseData[0];
            let facebookProfile = personalDetails.faceBookProfile;
            let twitterProfile = personalDetails.twitterProfile;
            let linkedInProfile = personalDetails.linkedInProfile;
            let googlePlusProfile = personalDetails.googlePlusProfile;
            let currentAddress = personalDetails.candidateAddressTO;
            let addressLine1: string;
            let addressLine2: string;
            let city: string;
            let state: string;
            let country: string;
            let pinCode: string;
            if (currentAddress != null) {
              addressLine1 = currentAddress.addressLine1;
              addressLine2 = currentAddress.addressLine2;
              city = currentAddress.city;
              state = currentAddress.state;
              country = currentAddress.country;
              pinCode = currentAddress.pincode;
            }
            let permanentAddress = personalDetails.permanentAddressTO;
            let permanentAddressLine1: string;
            let permanentAddressLine2: string;
            let permanentCity: string;
            let permanentState: string;
            let permanentCountry: string;
            let permanentPinCode: string;
            if (permanentAddress != null) {
              permanentAddressLine1 = permanentAddress.addressLine1;
              permanentAddressLine2 = permanentAddress.addressLine2;
              permanentCity = permanentAddress.city;
              permanentState = permanentAddress.state;
              permanentCountry = permanentAddress.country;
              permanentPinCode = permanentAddress.pincode;
            }
            let sameAddress: boolean = false;
            personalFormGroup.patchValue({
              facebookProfile: facebookProfile,
              twitterProfile: twitterProfile,
              linkedInProfile: linkedInProfile,
              googlePlusProfile: googlePlusProfile,
              addressLine1: addressLine1,
              addressLine2: addressLine2,
              city: city,
              state: state,
              country: country,
              pinCode: pinCode,
              permanentAddressLine1: permanentAddressLine1,
              permanentAddressLine2: permanentAddressLine2,
              permanentCity: permanentCity,
              permanentState: permanentState,
              permanentCountry: permanentCountry,
              permanentPinCode: permanentPinCode,
              sameAddress: sameAddress
            });
            sameAddress = this.currentSameAsPermanent(this.security);
            personalFormGroup.patchValue({
              sameAddress: sameAddress
            });
          }
        }
      }
    });

    this.allSubscriptions.push(subscription);
  }

  loadWorkDetails(candidateId: number) {
    let subscription = this.newCandidateService.fetchCandidateWork(candidateId, this.isDraft, false).subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            let workFormGroup = <FormGroup>this.candidateForm.controls.workSection;
            let workDetails = response.responseData[0];
            let totalExperienceMonths = workDetails.totalExperienceMonths;
            let totalExperienceYears = workDetails.totalExperienceYears;
            let noticePeriod = workDetails.noticePeriod;
            if (noticePeriod == null) {
              noticePeriod = "";
            }
            workFormGroup.patchValue({
              totalExperienceMonths: totalExperienceMonths,
              totalExperienceYears: totalExperienceYears,
              noticePeriod: noticePeriod + ""
            });
            if (workDetails.employmentHistory) {
              if (workDetails.employmentHistory.length > 0) {
                const workHistoryArray = <FormArray>workFormGroup.controls.employmentHistory;
                for (let c = 0; c < workDetails.employmentHistory.length; c++) {
                  let workHistory = workDetails.employmentHistory[c];
                  let workDetailId = workHistory.workDetailId;
                  let company = workHistory.company;
                  let jobTitle = workHistory.jobTitle;
                  let startDate = workHistory.startDate;
                  let endDate = workHistory.endDate;
                  let currentlyWorkHere = workHistory.currentlyWorkHere;
                  let ctcCurrency = workHistory.ctcCurrency;
                  let ctcAmount = workHistory.ctcAmount + "";
                  let ctcDenomination = workHistory.ctcDenomination;
                  let description = workHistory.description;

                  workHistoryArray.push(this.formBuilder.group({
                    workDetailId: workDetailId,
                    company: company,
                    jobTitle: jobTitle,
                    workStartDate: startDate,
                    workEndDate: endDate,
                    currentlyWorkHere: currentlyWorkHere,
                    ctcCurrency: ctcCurrency,
                    ctcAmount: [ctcAmount, [ValidationService.NumericWithDecimal]],
                    ctcDenomination: ctcDenomination,
                    description: description
                  }));
                }
              }
            }
          }
        }
      }
    });
    this.allSubscriptions.push(subscription);
  }

  loadInternshipDetails(candidateId: number) {
    let subscription = this.newCandidateService.fetchCandidateWork(candidateId, this.isDraft, true).subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            let internshipFormGroup = <FormGroup>this.candidateForm.controls.internshipSection;
            let internshipDetails = response.responseData[0];
            if (internshipDetails.employmentHistory) {
              if (internshipDetails.employmentHistory.length > 0) {
                const internshipHistoryArray = <FormArray>internshipFormGroup.controls.internshipHistory;
                for (let c = 0; c < internshipDetails.employmentHistory.length; c++) {
                  let internshipHistory = internshipDetails.employmentHistory[c];
                  let internshipDetailId = internshipHistory.workDetailId;
                  let company = internshipHistory.company;
                  let jobTitle = internshipHistory.jobTitle;
                  let startDate = internshipHistory.startDate;
                  let endDate = internshipHistory.endDate;
                  let currentlyWorkHere = internshipHistory.currentlyWorkHere;
                  let description = internshipHistory.description;

                  internshipHistoryArray.push(this.formBuilder.group({
                    internshipDetailId: internshipDetailId,
                    company: company,
                    jobTitle: jobTitle,
                    internshipStartDate: startDate,
                    internshipEndDate: endDate,
                    currentlyWorkHere: currentlyWorkHere,
                    description: description
                  }));

                }
              }
            }
          }
        }
      }
    });
    this.allSubscriptions.push(subscription);
  }


  loadEducationDetails(candidateId: number) {
    let subscription = this.newCandidateService.fetchCandidateEducation(candidateId, this.isDraft).subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
            let educationDetails = response.responseData[0];
            if (educationDetails.educationHistory) {
              if (educationDetails.educationHistory.length > 0) {
                const educationHistoryArray = <FormArray>educationFormGroup.controls.educationHistory;
                for (let c = 0; c < educationDetails.educationHistory.length; c++) {
                  let educationHistory = educationDetails.educationHistory[c];
                  let candidateEduDetailId = educationHistory.candidateEduDetailId;
                  let degree = educationHistory.degree;
                  let batchStartDate: string;
                  let batchEndDate: string;
                  let institute = educationHistory.institute;
                  let gradePercentage = educationHistory.gradePercentage;
                  if (educationHistory.batchStartDate != null) {
                    batchStartDate = educationHistory.batchStartDate;
                  }
                  if (educationHistory.batchEndDate != null) {
                    batchEndDate = educationHistory.batchEndDate;
                  }
                  educationHistoryArray.push(this.formBuilder.group({
                    eduDetailId: candidateEduDetailId,
                    degree: degree,
                    educationStartDate: batchStartDate,
                    educationEndDate: batchEndDate,
                    institute: institute,
                    gradePercentage: gradePercentage,
                  }));
                }
              }
            }
          }
        }
      }
    });
    this.allSubscriptions.push(subscription);
  }
  loadSkillsDetails(candidateId: number) {
    let subscription = this.newCandidateService.fetchCandidateSkills(candidateId, this.isDraft, false).subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {

            let skillDetails = response.responseData[0];

            if (skillDetails.skills) {
              if (skillDetails.skills.length > 0) {
                let skillSetSection = <FormGroup>this.candidateForm.controls.skillSetSection
                let skillsArray = <FormArray>skillSetSection.controls.skills;

                for (let c = 0; c < skillDetails.skills.length; c++) {
                  let skill = skillDetails.skills[c];
                  skillsArray.push(this.formBuilder.group({
                    skillId: skill.id,
                    skillName: skill.competency
                  }));
                }
              }
            }
          }
        }
      }

    });
    this.allSubscriptions.push(subscription);
  }

  loadLanguagesDetails(candidateId: number) {
    let subscription = this.newCandidateService.fetchCandidateSkills(candidateId, this.isDraft, true).subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {

            let languageDetails = response.responseData[0];

            if (languageDetails.skills) {
              if (languageDetails.skills.length > 0) {
                let languageSetSection = <FormGroup>this.candidateForm.controls.languageSetSection
                let languagesArray = <FormArray>languageSetSection.controls.languages;

                for (let language of languageDetails.skills) {
                  languagesArray.push(this.formBuilder.group({
                    languageId: language.id,
                    languageName: language.competency
                  }));
                }
              }
            }
          }
        }
      }

    });
    this.allSubscriptions.push(subscription);
  }

  syncCandidateAge() {
    let basicFormGroup = <FormGroup>this.candidateForm.controls.basicSection;
    let dateOfBirth = this.candidateForm.controls.basicSection.get('dateOfBirth').value;

    this.notificationService.clear();

    if (dateOfBirth != undefined && dateOfBirth != null) {

      this.notificationService.clear();

      let candidateAge = this.calculate_age(dateOfBirth);

      basicFormGroup.patchValue({
        candidateAge: candidateAge
      });
    }

  }

  profilecompletion() {
    let rangeList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    let count: number = 0;
    let basicGroup = <FormGroup>this.candidateForm.controls.basicSection;
    let workgroup = <FormGroup>this.candidateForm.controls.workSection;
    let workgrouphist = <FormArray>workgroup.controls.employmentHistory;
    let personalsection = <FormGroup>this.candidateForm.controls.personalSection;
    let skillSetSection = <FormGroup>this.candidateForm.controls.skillSetSection;
    let skillsArray = <FormArray>skillSetSection.controls.skills;
    let languageSetSection = <FormGroup>this.candidateForm.controls.languageSetSection;
    let languagesArray = <FormArray>languageSetSection.controls.languages;
    let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
    let educationHistory = <FormArray>educationFormGroup.controls.educationHistory;
    let json = basicGroup.getRawValue();
    if (json.firstName != null && json.firstName.trim() != '') {
      count += 1
    }
    if (json.lastName != null && json.lastName.trim() != '') {
      count += 1
    }
    if (json.mobileNumber != null && json.mobileNumber.trim() != '') {
      count += 1
    }
    if (json.primaryEmail != null && json.primaryEmail.trim() != '') {
      count += 1
    }
    if (json.dateOfBirth != undefined && json.dateOfBirth != null) {
      count += 1
    }
    if (json.gender != null && json.gender != 0) {
      count += 1
    }
    if (json.aadhaarNumber != null && json.aadhaarNumber.trim() != '') {
      count += 1
    }
    if (json.panNumber != null && json.panNumber.trim() != '') {
      count += 1
    }
    if (personalsection.controls.addressLine1.value != null && personalsection.controls.addressLine1.value != '') {
      count += 1
    }

    let company: string = '';
    for (let x = 0; x < workgrouphist.length; x++) {
      let history = <FormGroup>workgrouphist.at(x);
      company = history.controls.company.value;
    }
    if ((company != '') || (workgroup.controls.totalExperienceMonths.value != null && workgroup.controls.totalExperienceYears.value != null
      && (workgroup.controls.totalExperienceMonths.value > 0 || workgroup.controls.totalExperienceYears.value > 0))) {
      count += 1
    }
    let skillname: string = '';
    for (let x = 0; x < skillsArray.length; x++) {
      let skill = <FormGroup>skillsArray.at(x);
      skillname = skill.controls.skillId.value;
    }
    if (skillname != '') {
      count += 1
    }

    let languagename: string = '';
    for (let x = 0; x < languagesArray.length; x++) {
      let lang = <FormGroup>languagesArray.at(x);
      languagename = lang.controls.languageId.value;
    }
    if (languagename != '') {
      count += 1
    }

    let education: string = '';
    for (let x = 0; x < educationHistory.length; x++) {
      let edu = <FormGroup>educationHistory.at(x);
      education = edu.controls.degree.value;
    }
    if (education != '') {
      count += 1
    }

    this.totalcompleted = (count / rangeList.length)
  }

  saveAsCandidate() {

    this.notificationService.clear();

    let requestJSON = this.setCandidateDetailsRequestJSON(undefined);

    if (requestJSON.workExpTOs != null && requestJSON.workExpTOs.length > 0) {
      for (let request of requestJSON.workExpTOs) {
        request.candidateEmpDetailId = 0;
      }
    }

    if (requestJSON.internshipWorkExpTOs != null && requestJSON.internshipWorkExpTOs.length > 0) {
      for (let request of requestJSON.internshipWorkExpTOs) {
        request.candidateEmpDetailId = 0;
      }
    }

    if (requestJSON.educationTOs != null && requestJSON.educationTOs.length > 0) {
      for (let request of requestJSON.educationTOs) {
        request.candidateEduDetailId = 0;
      }
    }

    let valid = this.validateCandidateDetails(requestJSON, true);

    if (valid) {
      let candidateId = this.candidateForm.controls.candidateId.value;
      if (candidateId != 0) {
        this.savingAsCandidate = true;
        requestJSON.inReview = false;
        let subscription = this.newCandidateService.saveCompleteCandidateDetails(requestJSON.candidateId, requestJSON, this.isDraft, undefined, this.isInReview).subscribe(response => {
          if (response != null && response != undefined) {
            if (response.messageCode.code == 'EC200') {
              if(this.referCandidate) {
                this.i18UtilService.get('candidate.success.candidateSaved').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
                this.router.navigateByUrl('/job/erp');
                return;
              }
              let candidateId = response.response.candidateId;
              let candidateCode = response.response.candidateCode;
              this.candidateForm.get('candidateId').patchValue(candidateId);
              this.candidateForm.get('candidateCode').patchValue(candidateCode);
              this.candidateCodeSlug = this.formatCandidatecodeParamPipe.transform(candidateCode);
              this.i18UtilService.get('candidate.success.candidateSaved').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.INFO, res);
              });
              this.location.replaceState('candidate/newcandidate/' + this.formatCandidatecodeParamPipe.transform(candidateCode));
              this.replaceState = true;
              this.isDraft = false;
              this.sectionNumbers = [];
              this.sectionCount = 0;
              this.isInReview = false;
              this.constructorFunction();
              this.ngOnInit();
            } else if (response.messageCode.code == 'EC201') {
              this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
            } else if (response.messageCode.code == 'EC202') {
              this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
                this.notificationService.addMessage(NotificationSeverity.WARNING, res);
              });
            } else if (response.messageCode.code == 'EC400') {
              this.i18UtilService.get('candidate.error.invalidThaiID').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            } else {
              this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            }
          } else {
            this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
          this.savingAsCandidate = false;
        },
          () => {
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
            this.savingAsCandidate = false;
          });
        this.allSubscriptions.push(subscription);
      } else {
        this.i18UtilService.get('candidate.warning.draftSaveWarning').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }

    }

  }

  postSaveAsCandidate() {

  }

  resumeFileChange(event) {
    if(this.resumeFileInput.nativeElement.files.item(0).size>8388608){
      this.i18UtilService.get("common.warning.document_upload.MAX_INDIVIDUAL_SIZE").subscribe(res=>{
        this.notificationService.setMessage(NotificationSeverity.WARNING,res);
      });
      event.target.value = "";
      return;
    }
    this.parsingResume = true;
    this.resumeUploaded = false;
    
    let fileCount: number = this.resumeFileInput.nativeElement.files.length;
    this.candidateForm.controls.resumeUploaded.setValue(false);
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      for (let i = 0; i < fileCount; i++) {
        formData.append('file', this.resumeFileInput.nativeElement.files.item(0));
      }

      let candidateId = this.candidateForm.controls.candidateId.value;
      let resumeParserError = '';
      this.i18UtilService.get('candidate.error.resumeParseError').subscribe((res: string) => {
        resumeParserError = res;
      });

      let subscription = this.newCandidateService.parseResume(formData, candidateId,!this.enableResumeParsing,!this.enableResumeParsing,this.isDraft).subscribe((response) => {

        let parsed: boolean = false;
        let duplicate: boolean = false;
        let uploadResumeOnly:boolean=false;
        if (response) {
          if (response.messageCode) {
            if (response.messageCode.code) {
              if (response.messageCode.code == "EC200") {
                this.enableResumeParsing = false;


                if (response.responseData) {
                  if (response.responseData.length > 0) {

                    parsed = response.responseData[0].parsed;
                    duplicate = response.responseData[0].duplicate;
                    uploadResumeOnly=response.responseData[0].uploadResumeOnly;

                    if (duplicate) {
                      this.i18UtilService.get('candidate.warning.existingCandidateWarning').subscribe((res: string) => {
                        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
                      });
                      return;
                    }
                    if(uploadResumeOnly){
                      this.candidateForm.controls.resumeUploaded.setValue(true);
                      if (response.responseData[0].candidateTO.htmlDetailedResume != '') {
                        this.resumeHTML = this.sanitizer.bypassSecurityTrustHtml(response.responseData[0].candidateTO.htmlDetailedResume);
                      } else {
                        this.resumeHTML = this.sanitizer.bypassSecurityTrustHtml("Preview not available");
                      }
                      this.timeToParse = response.responseData[0].timeToParse;
                      this.resumeServerPath = response.responseData[0].resumeServerPath;
                      this.showloader=false;
                      this.parsingResume=false;
                      this.resumeUploaded=true;
                      this.allSubscriptions.push(subscription);
                      return;
                    }

                    if (parsed) {

                      this.clearAllBasic(event);
                      this.clearAllEducation();
                      this.clearAllPersonal();
                      this.clearAllSkillSet();
                      this.clearAllLanguageSet();
                      this.clearAllWork();

                      this.resumeUploaded = true;
                      this.candidateForm.controls.resumeUploaded.setValue(true);

                      let basicSection = <FormGroup>this.candidateForm.controls.basicSection;
                      let workFormGroup = <FormGroup>this.candidateForm.controls.workSection;

                      let personalFormGroup = <FormGroup>this.candidateForm.controls.personalSection;
                      let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
                      let parsedResume = response.responseData[0].candidateTO;
                      this.timeToParse = response.responseData[0].timeToParse;
                      this.resumeServerPath = response.responseData[0].resumeServerPath;
                      let candidateId = response.responseData[0].candidateId;
                      let candidateCode = response.responseData[0].candidateCode;
                      this.tempCandidateCode = candidateCode;

                      this.candidateForm.controls.candidateId.setValue(candidateId);
                      this.candidateForm.controls.candidateCode.setValue(candidateCode);
                      // this.location.replaceState('candidate/newcandidate/' + this.formatCandidatecodeParamPipe.transform(candidateCode));

                      let expInMonths = parsedResume.expInMonths;
                      if (expInMonths != null) {
                        workFormGroup.controls.totalExperienceMonths.setValue(expInMonths);
                      }
                      let expInYears = parsedResume.expInYears;
                      if (expInYears != null) {
                        workFormGroup.controls.totalExperienceYears.setValue(expInYears);
                      }
                      if (response.responseData[0].candidateTO.htmlDetailedResume != '') {
                        this.resumeHTML = this.sanitizer.bypassSecurityTrustHtml(response.responseData[0].candidateTO.htmlDetailedResume);
                      } else {
                        this.resumeHTML = this.sanitizer.bypassSecurityTrustHtml("Preview not available");
                      }



                      if (parsedResume.aadhar != null && parsedResume.aadhar != '') {
                        basicSection.controls.aadhaarNumber.patchValue(parsedResume.aadhar);
                      }
                      if (parsedResume.panNumber != null && parsedResume.panNumber != '') {
                        basicSection.controls.panNumber.patchValue(parsedResume.panNumber);
                      }

                      if (parsedResume.dateOfBirth != null) {
                        try {
                          let dateOfBirth: Date = new Date(parsedResume.dateOfBirth);
                          basicSection.controls.dateOfBirth.setValue(dateOfBirth);
                          this.syncCandidateAge();
                        } catch (e) {
                          console.log("Date of Birth parse error : " + parsedResume.dateOfBirth);
                        }
                      }

                      if (parsedResume.address != null) {
                        personalFormGroup.controls.permanentAddress.patchValue(parsedResume.address);
                      }

                      if (parsedResume.educationTOs != null && parsedResume.educationTOs.length > 0) {

                        let educationHistoryArray = <FormArray>educationFormGroup.controls.educationHistory;

                        while (educationHistoryArray.length > 0) {
                          educationHistoryArray.removeAt(0);
                        }

                        for (let x = 0; x < parsedResume.educationTOs.length; x++) {

                          let startDate = parsedResume.educationTOs[x].startDateFormat;
                          let endDate = parsedResume.educationTOs[x].endDateFormat;
                          let startDateDD = null;
                          let startDateMM = null;
                          let startDateYYYY = null;
                          let endDateDD = null;
                          let endDateMM = null;
                          let endDateYYYY = null;

                          if (startDate != null) {
                            try {
                              let dateStart: Date = new Date(parseInt(startDate));
                              startDateDD = dateStart.getDate();
                              startDateMM = (dateStart.getMonth() + 1);
                              startDateYYYY = dateStart.getFullYear();
                            } catch (e) {
                              console.log("education start date error : " + startDate);
                            }
                          }
                          if (endDate != null) {
                            try {
                              let dateEnd: Date = new Date(parseInt(endDate));
                              endDateDD = dateEnd.getDate();
                              endDateMM = (dateEnd.getMonth() + 1);
                              endDateYYYY = dateEnd.getFullYear();
                            } catch (e) {
                              console.log("education end date error : " + startDate);
                            }
                          }

                          educationHistoryArray.push(this.formBuilder.group({
                            eduDetailId: '0',
                            degree: parsedResume.educationTOs[x].degree,
                            batchStartDateDD: startDateDD,
                            batchStartDateMM: startDateMM,
                            batchStartDateYYYY: startDateYYYY,
                            batchEndDateDD: endDateDD,
                            batchEndDateMM: endDateMM,
                            batchEndDateYYYY: endDateYYYY,
                            institute: parsedResume.educationTOs[x].university,
                            gradePercentage: parsedResume.educationTOs[x].percentage
                          }));
                        }
                      }

                      if (parsedResume.faceBookProfile != null && parsedResume.faceBookProfile.trim() != '') {
                        personalFormGroup.controls.facebookProfile.patchValue(parsedResume.faceBookProfile);
                      }
                      if (parsedResume.googlePlusProfile != null && parsedResume.googlePlusProfile.trim() != '') {
                        personalFormGroup.controls.googlePlusProfile.patchValue(parsedResume.googlePlusProfile);
                      }
                      if (parsedResume.linkedInProfile != null && parsedResume.linkedInProfile.trim() != '') {
                        personalFormGroup.controls.linkedInProfile.patchValue(parsedResume.linkedInProfile);
                      }
                      if (parsedResume.firstName != null) {
                        basicSection.controls.firstName.patchValue(parsedResume.firstName);
                      }
                      if (parsedResume.middleName != null) {
                        basicSection.controls.middleName.patchValue(parsedResume.middleName);
                      }
                      if (parsedResume.lastName != null) {
                        basicSection.controls.lastName.patchValue(parsedResume.lastName);
                      }
                      if (parsedResume.functionalAreaID != null && parsedResume.functionalAreaID != 0) {
                        basicSection.controls.functionalArea.patchValue(parsedResume.functionalAreaID);
                      }
                      if (parsedResume.genderID != null && parsedResume.genderID != 0) {
                        basicSection.controls.gender.patchValue(parsedResume.genderID);
                      }
                      if (parsedResume.industryID != null && parsedResume.industryID != 0) {
                        basicSection.controls.industry.patchValue(parsedResume.industryID);
                      }
                      if (parsedResume.internationalID != null && parsedResume.internationalID.trim() != '') {
                        basicSection.controls.internationalID.patchValue(parsedResume.internationalID);
                      }
                      if (parsedResume.mobileNumber != null && parsedResume.mobileNumber.trim() != '') {
                        basicSection.controls.mobileNumber.patchValue(parsedResume.mobileNumber);
                      }
                      if (parsedResume.noticePeriod != null && parsedResume.noticePeriod != 0) {
                        workFormGroup.controls.noticePeriod.patchValue(parsedResume.noticePeriod);
                      }

                      if (parsedResume.passportNo != null && parsedResume.passportNo.trim() != '') {
                        basicSection.controls.passportNumber.patchValue(parsedResume.passportNo);
                      }

                      if (parsedResume.passportNumber != null && parsedResume.passportNumber.trim() != '') {
                        basicSection.controls.passportNumber.patchValue(parsedResume.passportNumber);
                      }

                      if (parsedResume.permanentAddress != null && parsedResume.permanentAddress.trim() != '') {
                        personalFormGroup.controls.permanentAddress.patchValue(parsedResume.permanentAddress);
                      }

                      if (parsedResume.primaryEmail != null && parsedResume.primaryEmail.trim() != '') {
                        basicSection.controls.primaryEmail.patchValue(parsedResume.primaryEmail);
                      }

                      if (parsedResume.skillList != null && parsedResume.skillList.length > 0) {
                        let skillSetSection = <FormGroup>this.candidateForm.controls.skillSetSection;
                        let skills = <FormArray>skillSetSection.controls.skills;

                        while (skills.length > 0) {
                          skills.removeAt(0);
                        }

                        for (let x = 0; x < parsedResume.skillList.length; x++) {
                          skills.push(this.formBuilder.group({
                            skillId: '0',
                            skillName: parsedResume.skillList[x]
                          }));
                        }
                        this.loadSkillsDetails(candidateId);
                      }


                      if (parsedResume.workExpTOs != null && parsedResume.workExpTOs.length > 0) {
                        let empHistory = <FormArray>workFormGroup.controls.employmentHistory
                        while (empHistory.length > 0) {
                          empHistory.removeAt(0);
                        }
                        for (let x = 0; x < parsedResume.workExpTOs.length; x++) {
                          let startDate = parsedResume.workExpTOs[x].startDateFormat;
                          let endDate = parsedResume.workExpTOs[x].endDateFormat;
                          let startDateDD = null;
                          let startDateMM = null;
                          let startDateYYYY = null;
                          let endDateDD = null;
                          let endDateMM = null;
                          let endDateYYYY = null;

                          if (startDate != null) {
                            try {
                              let dateStart: Date = new Date(parseInt(startDate));
                              startDateDD = dateStart.getDate();
                              startDateMM = (dateStart.getMonth() + 1);
                              startDateYYYY = dateStart.getFullYear();
                            } catch (e) {
                              console.log("company start date error : " + startDate);
                            }
                          }
                          if (endDate != null) {
                            try {
                              let dateEnd: Date = new Date(parseInt(endDate));
                              endDateDD = dateEnd.getDate();
                              endDateMM = (dateEnd.getMonth() + 1);
                              endDateYYYY = dateEnd.getFullYear();
                            } catch (e) {
                              console.log("company end date error : " + startDate);
                            }
                          }
                          empHistory.push(this.formBuilder.group({
                            workDetailId: '0',
                            company: parsedResume.workExpTOs[x].companyName,
                            jobTitle: parsedResume.workExpTOs[x].jobTitle,
                            startDateDD: startDateDD,
                            startDateMM: startDateMM,
                            startDateYYYY: startDateYYYY,
                            endDateDD: endDateDD,
                            endDateMM: endDateMM,
                            endDateYYYY: endDateYYYY,
                            currentlyWorkHere: '',
                            ctcCurrency: 'INR',
                            ctcAmount: [parsedResume.workExpTOs[x].ctc, [ValidationService.NumericWithDecimal]],
                            ctcDenomination: 'Thousand',
                            description: ''
                          }));
                        }
                      }
                    }

                  }
                }
              } else if (response.messageCode.code == "EC201") {
                resumeParserError = response.messageCode.description;
                this.showloader = false;
                this.parsingResume = false;
              }
            }

          }

        }

        if (!parsed) {
          this.notificationService.setMessage(NotificationSeverity.WARNING, resumeParserError);
          this.showloader = false;
          this.parsingResume = false;
        } else {
          let requestJSON = this.setBasicDetailsRequestJSON(undefined);
          requestJSON.sourceTypeID = this.recruiterSourceTypeID;
          requestJSON.subSourceTypeID = undefined;
          requestJSON.sourceID = this.recruiterSourceUserID;
          requestJSON.subSourceUserID = this.recruiterSourceUserID;
          let subscription = this.newCandidateService.saveCandidateDetails(requestJSON.candidateId, requestJSON, this.isDraft, 'basic').subscribe(response => {
            this.i18UtilService.get('candidate.success.resumeParsed').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            let fg = <FormGroup>this.candidateForm.controls.createCandidateSection;
            this.sessionService.dialogData = {
              candidateCode: this.formatCandidatecodeParamPipe.transform(this.tempCandidateCode),
              timeToParse: this.timeToParse,
              isTagged: fg.controls.taggedOrUntagged.value,
              jobTitle: fg.controls.jobTitle.value,
              jobCode: this.jobCode,
              referCandidate : this.referCandidate
            }
              this.router.navigateByUrl('candidate/newcandidate/' + this.formatCandidatecodeParamPipe.transform(this.tempCandidateCode));
            this.replaceState = false;
          });
        }
      },
        (onError) => {
          this.parsingResume = false;
        }, () => {
        });

      this.allSubscriptions.push(subscription);
    }

    event.target.value = "";
  }
  fileChange(event) {

    let candidateId = this.candidateForm.controls.candidateId.value;

    let fileCount: number = this.profilePicInput.nativeElement.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      for (let i = 0; i < fileCount; i++) {
        formData.append('file', this.profilePicInput.nativeElement.files.item(0));
      }
      let fileUploadError = '';
      this.i18UtilService.get('docUploadFailed').subscribe((res: string) => {
        fileUploadError = res;
      });

      let subscription = this.newCandidateService.saveProfilePic(candidateId, formData, this.isDraft).subscribe(response => {

        let saved: boolean = false;
        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {
              let candidateId = response.responseData[0].candidateId;
              let profilePicURL = response.responseData[0].profilePicURL;
              this.candidateForm.controls.candidateId.setValue(candidateId);
              this.profilePicURL = profilePicURL;
              saved = true;
              this.i18UtilService.get('candidate.success.picUpdated').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.INFO, res);
              });
            }
          }
          if (response.messageCode && response.messageCode.code && response.messageCode.code == "EC201") {
            if (response.messageCode.message != null) {
              fileUploadError = response.messageCode.message;
            }
          }
        }

        if (!saved) {
          this.notificationService.setMessage(NotificationSeverity.WARNING, fileUploadError);
        }
      });
      this.allSubscriptions.push(subscription);
    }

  }

  downloadDocument(index: number) {

  }
  uploadDocument(event, index: number, documentTypeID: number) {
    this.setDocumentSectionDone();
    this.candidateForm.controls.documentSection.reset()
    let candidateId = this.candidateForm.controls.candidateId.value;
    let documentName = "";
    if (this.documentsList.length >= index) {
      documentName = this.documentsList[index].documentName;
      this.documentsList[index].uploading = true;
    }
    if (event) {
      if (event.target) {
        if (event.target.files.length > 0) {
          const originalFile=event.target.files[0]
          let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
          const options = new DocumentValidationOptions(supportedTypes);
          options.isTotalFileSizeChecked = true;
          options.isIndividualFileChecked = false;
          try {
            this.documentUploadUtilityService.validateFileBeforeUpload(event.target.files, options).then((result: UploadDocumentValidationResult)=>{
              if (result.unsupportedFiles.length > 0) {
                this.i18UtilService.get('candidate.warning.supportedFileWarning', { 'supportedTypes': supportedTypes.toString() }).subscribe((res: string) => {
                  this.notificationService.addMessage(NotificationSeverity.WARNING, res);
                });
                this.documentsList[index].uploading = false;
              } else {
                let formData = new FormData();
                formData.append('file', originalFile);
                formData.append('documentName', documentName);
                let fileUploadError = '';
                this.i18UtilService.get('candidate.error.docUploadFailed').subscribe((res: string) => {
                  fileUploadError = res;
                });
                let subscription = this.newCandidateService.saveCandidateDocument(candidateId, documentTypeID, formData, this.isDraft).subscribe(response => {
                  let uploaded = false;
                  if (response) {
                    if (response.responseData) {
                      if (response.responseData.length > 0) {
                        let doc = response.responseData[0];
                        this.documentsList[index].documentId = doc.documentId;
                        this.documentsList[index].uploaded = doc.uploaded;
                        this.documentsList[index].documentName = doc.documentName;
                        this.documentsList[index].fileName = doc.fileName;
                        uploaded = true;
  
                      }
                    }
                    if (response.messageCode && response.messageCode.code && response.messageCode.code == "EC201") {
                      if (response.messageCode.message != null) {
                        fileUploadError = response.messageCode.message;
                      }
                    }
                  }
                  if (!uploaded) {
                    this.notificationService.setMessage(NotificationSeverity.WARNING, fileUploadError);
                  } else {
                    this.i18UtilService.get('candidate.success.docUploaded').subscribe((res: string) => {
                      this.notificationService.setMessage(NotificationSeverity.INFO, res);
                    });
                  }
                },
                  () => {
                    this.setDocumentSectionDone();
                  },
                  () => {
                    this.documentsList[index].uploading = false;
                    this.setDocumentSectionDone();
                  });
  
                this.allSubscriptions.push(subscription);
              }
            }).catch(error=>{
              this.handleFileUploadError(error);
              this.documentsList[index].uploading = false;
              this.setDocumentSectionDone();
            })
            
          } catch (error) {
            this.handleFileUploadError(error);
            this.documentsList[index].uploading = false;
            this.setDocumentSectionDone();
          }
        }
      }
    }
    event.target.value = "";
  }

  handleFileUploadError(error:DocumentValidationErrorCodes){ 
    this.i18UtilService.get('common.warning.document_upload.'+error).subscribe(res=>{
      this.notificationService.setMessage(NotificationSeverity.WARNING,res);
    })
  }
  setDocumentSectionDone() {
    let uploading = false;
    let uploaded = true;

    for (let x = 0; x < this.documentsList.length; x++) {
      if (this.documentsList[x].uploading) {
        uploading = true;
        break;
      }
      if (this.documentsList[x].uploaded) {
        uploaded = false;
      }
    }

    if (uploaded && !uploading) {
      this.documentSectionDone = true;
      this.candidateForm.controls.documentSection.markAsPristine();
    }
    if (uploading) {
      this.documentSectionBlock = true;
      this.candidateForm.controls.documentSection.markAsDirty();
    }

  }
  deleteDocument(index: number) {
    this.setDocumentSectionDone();

    let candidateId = this.candidateForm.controls.candidateId.value;
    let documentId = 0;


    if (this.documentsList.length >= index) {
      documentId = this.documentsList[index].documentId;
      this.documentsList[index].uploading = true;
    }

    if (documentId != 0) {
      let subscription = this.newCandidateService.deleteCandidateDocument(candidateId, documentId, this.isDraft).subscribe(response => {
        let deleted = false;
        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {
              deleted = true;
              this.documentsList[index].documentId = 0;
              this.documentsList[index].uploaded = false;
            }
          }
        }

        if (!deleted) {
          this.i18UtilService.get('common.error.docDeleteError').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        } else {
          this.i18UtilService.get('common.success.docDeleted').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
        }
      },
        () => {

        },
        () => {
          this.documentsList[index].uploading = false;
          this.setDocumentSectionDone();
        });

      this.allSubscriptions.push(subscription);
    }




  }

  goBack() {
    this.location.back();
  }

  getSectionNumber(section: string): number {
    if (this.sectionCount == 0) {
      this.sectionCount = 1;
    }

    for (let x = 0; x < this.sectionNumbers.length; x++) {
      if (this.sectionNumbers[x].section == section) {
        return this.sectionNumbers[x].number;
      }
    }

    let sn = new SectionNumber();
    sn.section = section;
    sn.number = this.sectionCount;
    this.sectionNumbers.push(sn);
    this.sectionCount = this.sectionCount + 1;
    return sn.number;
  }

  validateBasicDetails(valid: boolean, requestJSON: SaveCandidateDetailsTO, isSaveCandidateDetails: boolean): boolean {

    if (requestJSON.firstName == null || requestJSON.firstName == '') {
      this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.firstNameLabel }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    
    if (this.security.internationalIDRendered && this.security.internationalIDRequired && (requestJSON.internationalID == null || requestJSON.internationalID.trim() == '')) {
      this.i18UtilService.get('common.warning.labelRequired', {'section': this.security.basicSectionLabel, 'label':  this.security.internationalIDLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });  
      valid = false;
    }

    if (!this.isDraft || isSaveCandidateDetails) {

      if (this.security.firstNameRendered && this.security.firstNameRequired && (requestJSON.firstName == null || requestJSON.firstName == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.firstNameLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.middleNameRendered && this.security.middleNameRequired && (requestJSON.middleName == null || requestJSON.middleName == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.middleNameLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.lastNameRendered && this.security.lastNameRequired && (requestJSON.lastName == null || requestJSON.lastName == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.lastNameLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.internationalIDRendered && this.security.internationalIDRequired && (requestJSON.internationalID == null || requestJSON.internationalID.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', {'section': this.security.basicSectionLabel, 'label':  this.security.internationalIDLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });  
        valid = false;
      }
      if (this.security.mobileRendered && this.security.mobileRequired && (requestJSON.mobileNumber == null || requestJSON.mobileNumber == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.mobileLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.primaryEmailRendered && this.security.primaryEmailRequired && (requestJSON.primaryEmail == null || requestJSON.primaryEmail == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.primaryEmailLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.secondaryEmailRendered && this.security.secondaryEmailRequired && (requestJSON.secondaryEmail == null || requestJSON.secondaryEmail == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.secondaryEmailLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.dateOfBirthRendered && this.security.dateOfBirthRequired && (requestJSON.dateOfBirth == undefined || requestJSON.dateOfBirth == null)) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.dateOfBirthLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.genderRendered && this.security.genderRequired && (requestJSON.genderID == null || requestJSON.genderID == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.genderLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.panNumberRendered && this.security.panNumberRequired && (requestJSON.panNumber == null || requestJSON.panNumber.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.panNumberLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.voterIdRendered && this.security.voterIdRequired && (requestJSON.voterID == null || requestJSON.voterID.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.voterIdLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }


      if (this.security.passportNumberRendered && this.security.passportNumberRequired && (requestJSON.passportNumber == null || requestJSON.passportNumber == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.passportNumberLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.aadhaarNumberRendered && this.security.aadhaarNumberRequired && (requestJSON.aadhar == null || requestJSON.aadhar == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.aadhaarNumberLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.industryRendered && this.security.industryRequired && (requestJSON.industryID == null || requestJSON.industryID == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.industryLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.functionalAreaRendered && this.security.functionalAreaRequired && (requestJSON.functionalAreaID == null || requestJSON.functionalAreaID == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.functionalAreaLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (requestJSON.sourceTypeID == 'undefined' || requestJSON.sourceTypeID == null || requestJSON.sourceTypeID == '' || requestJSON.sourceTypeID == 0) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.sourceTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

    }

    if (this.sourceTypeSysContentType == 'VENDOR') {
      if (requestJSON.subSourceTypeID == undefined || requestJSON.subSourceTypeID == null || requestJSON.subSourceTypeID == '' || requestJSON.subSourceTypeID == 0) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.sourceTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
      if (requestJSON.subSourceUserID == undefined || requestJSON.subSourceUserID == null || requestJSON.subSourceUserID == '' || requestJSON.subSourceUserID == 0) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.sourceUsersLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    } else if (this.sourceTypeSysContentType == 'RECRUITER' || this.sourceTypeSysContentType == 'EMPLOYEE REFERRAL' || this.sourceTypeSysContentType == 'IJP REFERRAL') {
      if (requestJSON.subSourceUserID == undefined || requestJSON.subSourceUserID == null || requestJSON.subSourceUserID == '' || requestJSON.subSourceUserID == 0) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.sourceUsersLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.firstNameRendered && requestJSON.firstName != null) {
      let fname = requestJSON.firstName.replace(new RegExp('[a-zA-Z]', 'g'), '');
      if (fname.length > 0) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.basicSectionLabel, 'label': this.security.sourceUsersLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        this.i18UtilService.get('candidate.warning.containsAlphabetsOnly', { 'label': this.security.firstNameLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.middleNameRendered && requestJSON.middleName != null) {
      let mname = requestJSON.middleName.replace(new RegExp('[a-zA-Z]', 'g'), '');
      if (mname.length > 0) {
        this.i18UtilService.get('candidate.warning.containsAlphabetsOnly', { 'label': this.security.middleNameLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.lastNameRendered && requestJSON.lastName != null) {
      let lname = requestJSON.lastName.replace(new RegExp('[a-zA-Z]', 'g'), '');
      if (lname.length > 0) {
        this.i18UtilService.get('candidate.warning.containsAlphabetsOnly', { 'label': this.security.lastNameLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.dateOfBirthRendered && requestJSON.dateOfBirth != undefined && requestJSON.dateOfBirth != null && requestJSON.age < 18) {
      this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.basicSectionLabel + ": " + this.security.candidateAgeLabel + " should be greater than 18");
      valid = false;
    }

    if (this.security.dateOfBirthRendered && requestJSON.dateOfBirth != undefined && requestJSON.dateOfBirth != null && requestJSON.age > 100) {
      this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.basicSectionLabel + ": " + this.security.candidateAgeLabel + " should be less than 100");
      valid = false;
    }

    if (this.security.internationalIDRendered && requestJSON.internationalID != undefined && requestJSON.internationalID != null && (requestJSON.internationalID as String).trim() != '') {
      if (!this.validateThaiID(requestJSON.internationalID)) {
        this.i18UtilService.get('candidate.warning.invalidLabel', {'label': this.security.internationalIDLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.mobileRendered && requestJSON.mobileNumber != undefined && requestJSON.mobileNumber != null && (requestJSON.mobileNumber as String).trim() != '') {
      if (!(requestJSON.mobileNumber.match(/^[0-9+\-]{10,14}$/))) {
        this.i18UtilService.get('candidate.warning.invalidLabel', { 'label': this.security.mobileLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.primaryEmailRendered && requestJSON.primaryEmail != undefined && requestJSON.primaryEmail != null && (requestJSON.primaryEmail as String).trim() != '') {
      let matches = requestJSON.primaryEmail.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
      if (!(matches != null && matches.length > 0 && matches[0] === requestJSON.primaryEmail)) {
        this.i18UtilService.get('candidate.warning.invalidLabel', { 'label': this.security.primaryEmailLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.secondaryEmailRendered && requestJSON.secondaryEmail != undefined && requestJSON.secondaryEmail != null && (requestJSON.secondaryEmail as String).trim() != '') {
      let matches = requestJSON.secondaryEmail.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
      if (!(matches != null && matches.length > 0 && matches[0] === requestJSON.secondaryEmail)) {
        this.i18UtilService.get('candidate.warning.invalidLabel', { 'label': this.security.secondaryEmailLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.primaryEmailRendered && this.security.secondaryEmailRendered && requestJSON.primaryEmail != undefined && requestJSON.secondaryEmail != undefined && requestJSON.primaryEmail != null && requestJSON.secondaryEmail != null &&
      ((requestJSON.primaryEmail as String).toLowerCase().trim() == (requestJSON.secondaryEmail as String).toLowerCase().trim())
      && (requestJSON.primaryEmail as String).toLowerCase().trim() != "" && (requestJSON.secondaryEmail as String).toLowerCase().trim() != "") {
      this.i18UtilService.get('candidate.warning.sameEmail').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.security.panNumberRendered && requestJSON.panNumber != undefined && requestJSON.panNumber != null && (requestJSON.panNumber as String).trim() != '') {
      if (!(requestJSON.panNumber.match(/[a-zA-Z]{5}\d{4}[a-zA-Z]{1}/))) {
        this.i18UtilService.get('candidate.warning.enterValidDocID', { 'docId': this.security.panNumberLabel, 'format': '5char/4Numeric/1char' }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.passportNumberRendered && requestJSON.passportNumber != undefined && requestJSON.passportNumber != null && (requestJSON.passportNumber as String).trim() != '') {
      if (!(requestJSON.passportNumber.match(/[a-zA-Z]{1}[0-9]{7}/))) {
        this.i18UtilService.get('candidate.warning.enterValidDocID', { 'docId': this.security.passportNumberLabel, 'format': '1char/7Numeric' }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      } else if ((requestJSON.passportNumber as String).length < 8) {
        this.i18UtilService.get('candidate.warning.passportLength').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.aadhaarNumberRendered && requestJSON.aadhar != undefined && requestJSON.aadhar != null && (requestJSON.aadhar as String).trim() != '') {
      if (!(requestJSON.aadhar.match(/^[0-9]*$/))) {
        this.i18UtilService.get('candidate.warning.enterValidDocID', { 'docId': this.security.aadhaarNumberLabel, 'format': '12Numeric' }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      } else if ((requestJSON.aadhar as String).length < 12) {
        this.i18UtilService.get('candidate.warning.aadharLength').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    return valid;
  }

  validatePersonalDetails(valid: boolean, requestJSON: SaveCandidateDetailsTO, isSaveCandidateDetails: boolean): boolean {

    if (!this.isDraft || isSaveCandidateDetails) {
      /* current address */
      if (this.security.addressLine1Rendered && this.security.addressLine1Required
        && (requestJSON.candidateAddressTO.addressLine1 == null || requestJSON.candidateAddressTO.addressLine1.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Current Address', 'label': this.security.addressLine1Label }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.addressLine2Rendered && this.security.addressLine2Required
        && (requestJSON.candidateAddressTO.addressLine2 == null || requestJSON.candidateAddressTO.addressLine2.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Current Address', 'label': this.security.addressLine2Label }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.cityRendered && this.security.cityRequired
        && (requestJSON.candidateAddressTO.city == null || requestJSON.candidateAddressTO.city.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Current Address', 'label': this.security.cityLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.stateRendered && this.security.stateRequired
        && (requestJSON.candidateAddressTO.state == null || requestJSON.candidateAddressTO.state.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Current Address', 'label': this.security.stateLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.countryRendered && this.security.countryRequired
        && (requestJSON.candidateAddressTO.country == null || requestJSON.candidateAddressTO.country.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Current Address', 'label': this.security.countryLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.pinCodeRendered && this.security.pinCodeRequired
        && (requestJSON.candidateAddressTO.pincode == null || requestJSON.candidateAddressTO.pincode.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Current Address', 'label': this.security.pinCodeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
      /* permanent address */

      if (this.security.permanentAddressRendered && this.security.addressLine1Rendered && this.security.permanentAddressRequired && this.security.addressLine1Required
        && (requestJSON.permanentAddressTO.addressLine1 == null || requestJSON.permanentAddressTO.addressLine1.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.addressLine1Label }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });

      }

      if (this.security.permanentAddressRendered && this.security.addressLine2Rendered && this.security.permanentAddressRequired && this.security.addressLine2Required
        && (requestJSON.permanentAddressTO.addressLine2 == null || requestJSON.permanentAddressTO.addressLine2.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.addressLine2Label }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.permanentAddressRendered && this.security.cityRendered && this.security.permanentAddressRequired && this.security.cityRequired
        && (requestJSON.permanentAddressTO.city == null || requestJSON.permanentAddressTO.city.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.cityLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.permanentAddressRendered && this.security.stateRendered && this.security.permanentAddressRequired && this.security.stateRequired
        && (requestJSON.permanentAddressTO.state == null || requestJSON.permanentAddressTO.state.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.stateLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.permanentAddressRendered && this.security.countryRendered && this.security.permanentAddressRequired && this.security.countryRequired
        && (requestJSON.permanentAddressTO.country == null || requestJSON.permanentAddressTO.country.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.countryLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.permanentAddressRendered && this.security.pinCodeRendered && this.security.permanentAddressRequired && this.security.pinCodeRequired
        && (requestJSON.permanentAddressTO.pincode == null || requestJSON.permanentAddressTO.pincode.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.pinCodeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
      if (this.security.facebookRendered
        && this.security.facebookRequired
        && (requestJSON.faceBookProfile == null || requestJSON.faceBookProfile.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.personalSectionLabel, 'label': this.security.facebookLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.twitterRendered
        && this.security.twitterRequired
        && (requestJSON.twitterProfile == null || requestJSON.twitterProfile.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.personalSectionLabel, 'label': this.security.twitterLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.linkedInRendered
        && this.security.linkedInRequired
        && (requestJSON.linkedInProfile == null || requestJSON.linkedInProfile.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.personalSectionLabel, 'label': this.security.linkedInLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (this.security.googlePlusRendered
        && this.security.googlePlusRequired
        && (requestJSON.googlePlusProfile == null || requestJSON.googlePlusProfile.trim() == '')) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.personalSectionLabel, 'label': this.security.googlePlusLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      //custom case when only permanent address is enabled
      let personalFormGroup = <FormGroup>this.candidateForm.controls.personalSection;
      if (personalFormGroup.controls.requiredPermanentAddress.value && (requestJSON.permanentAddressTO.addressLine1 == null || requestJSON.permanentAddressTO.addressLine1.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.addressLine1Label }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (personalFormGroup.controls.requiredPermanentAddress.value && (requestJSON.permanentAddressTO.addressLine2 == null || requestJSON.permanentAddressTO.addressLine2.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.addressLine2Label }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (personalFormGroup.controls.requiredPermanentAddress.value && (requestJSON.permanentAddressTO.city == null || requestJSON.permanentAddressTO.city.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.cityLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (personalFormGroup.controls.requiredPermanentAddress.value && (requestJSON.permanentAddressTO.state == null || requestJSON.permanentAddressTO.state.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.stateLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (personalFormGroup.controls.requiredPermanentAddress.value && (requestJSON.permanentAddressTO.country == null || requestJSON.permanentAddressTO.country.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.countryLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (personalFormGroup.controls.requiredPermanentAddress.value && (requestJSON.permanentAddressTO.pincode == null || requestJSON.permanentAddressTO.pincode.trim() == '')) {
        this.i18UtilService.get('common.warning.addressLabelRequired', { 'section': this.security.personalSectionLabel, 'addressType': 'Permanent Address', 'label': this.security.pinCodeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    return valid;
  }

  validateWorkDetails(valid: boolean, requestJSON: SaveCandidateDetailsTO, isSaveCandidateDetails: boolean): boolean {

    if (!this.isDraft || isSaveCandidateDetails) {
      if (this.security.totalExperienceRendered && this.security.totalExperienceRequired
        && (((requestJSON.expInYears == null || requestJSON.expInYears == '') && requestJSON.expInYears != 0)
          || (requestJSON.expInMonths == null || requestJSON.expInMonths == '') && requestJSON.expInMonths != 0)) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.totalExperienceLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
      if (this.security.noticePeriodRendered && this.security.noticePeriodRequired && (requestJSON.noticePeriod == '' || requestJSON.noticePeriod == null)) {
        this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.noticePeriodLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

      if (requestJSON.noticePeriod != null && requestJSON.noticePeriod != '' && !requestJSON.noticePeriod.match(/^[0-9]*$/)) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.workSectionLabel + ": " + this.security.noticePeriodLabel + " : Please enter numbers only. In the format 0-9");
        valid = false;
      }

    }

    let currentDate = new Date();
    let workFormGroup = <FormGroup>this.candidateForm.controls.workSection;
    let employmentHistory = <FormArray>workFormGroup.controls.employmentHistory;

    for (let x = 0; x < employmentHistory.length; x++) {
      let history = <FormGroup>employmentHistory.at(x);
      let workStartDate = history.controls.workStartDate.value;
      let workEndDate = history.controls.workEndDate.value;
      let currentlyWorkHere = history.controls.currentlyWorkHere.value;

      if (!this.isDraft || isSaveCandidateDetails) {

        if (this.security.companyStartDateRendered && this.security.companyStartDateRequired && (workStartDate == '' || workStartDate == null)) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.companyStartDateLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.companyEndDateRendered && this.security.companyEndDateRequired && (workEndDate == '' || workEndDate == null) && !currentlyWorkHere) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.companyEndDateLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

      }

      if ((workStartDate != '' && workStartDate != null) && workStartDate.toString() == 'Invalid Date') {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.workSectionLabel + ": " + this.security.companyStartDateLabel + " is invalid");
        valid = false;
      }

      if ((workEndDate != '' && workEndDate != null) && workEndDate.toString() == 'Invalid Date' && !currentlyWorkHere) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.workSectionLabel + ": " + this.security.companyEndDateLabel + " is invalid");
        valid = false;
      }

      let dateStart: Date;
      if (!(workStartDate instanceof Date)) {
        dateStart = new Date(workStartDate);
      } else {
        dateStart = workStartDate;
      }

      let dateEnd: Date;
      if (!(workEndDate instanceof Date)) {
        dateEnd = new Date(workEndDate);
      } else {
        dateEnd = workEndDate;
      }

      if ((dateEnd.getTime() - dateStart.getTime() < 0) && !currentlyWorkHere) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.workSectionLabel + ": " + this.security.companyStartDateLabel + " should be before " + this.security.companyEndDateLabel);
        valid = false;
      }

      if ((currentDate.getTime() - dateStart.getTime() < 0) && !currentlyWorkHere) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.companyStartDateLabel + " cannot be greater than Current Date");
        valid = false;
      }
    }

    if (!this.isDraft || isSaveCandidateDetails) {
      for (let workExpTO of requestJSON.workExpTOs) {
        if (this.security.companyRendered && this.security.companyRequired && (workExpTO.companyName == null || workExpTO.companyName == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.companyLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.jobTitleRendered && this.security.jobTitleRequired && (workExpTO.jobTitle == null || workExpTO.jobTitle == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.jobTitleLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.descriptionRendered && this.security.descriptionRequired && (workExpTO.jobDescription == null || workExpTO.jobDescription == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.descriptionLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.companyCTCRendered && this.security.companyCTCRequired && (workExpTO.currencyCode == null || workExpTO.currencyCode.trim() == '' || workExpTO.ctc == null
          || workExpTO.ctc.trim() == '' || workExpTO.ctcPart == null || workExpTO.ctcPart.trim() == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.workSectionLabel, 'label': this.security.companyCTCLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
      }
    }
    return valid;
  }

  validateInternshipDetails(valid: boolean, requestJSON: SaveCandidateDetailsTO, isSaveCandidateDetails: boolean): boolean {

    let internshipFormGroup = <FormGroup>this.candidateForm.controls.internshipSection;
    let employmentHistory = <FormArray>internshipFormGroup.controls.internshipHistory;
    let currentDate = new Date();

    for (let x = 0; x < employmentHistory.length; x++) {
      let history = <FormGroup>employmentHistory.at(x);
      let internshipStartDate = history.controls.internshipStartDate.value;
      let internshipEndDate = history.controls.internshipEndDate.value;
      let currentlyWorkHere = history.controls.currentlyWorkHere.value;

      if (!this.isDraft || isSaveCandidateDetails) {

        if (this.security.internshipCompanyStartDateRendered && this.security.internshipCompanyStartDateRequired && (internshipStartDate == '' || internshipStartDate == null)) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.internshipSectionLabel, 'label': this.security.internshipCompanyStartDateLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.internshipCompanyEndDateRendered && this.security.internshipCompanyEndDateRequired && (internshipEndDate == '' || internshipEndDate == null) && !currentlyWorkHere) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.internshipSectionLabel, 'label': this.security.internshipCompanyEndDateLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

      }

      if ((internshipStartDate != '' && internshipStartDate != null) && internshipStartDate.toString() == 'Invalid Date') {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.internshipSectionLabel + ": " + this.security.internshipCompanyStartDateLabel + " is invalid");
        valid = false;
      }

      if ((internshipEndDate != '' && internshipEndDate != null) && internshipEndDate.toString() == 'Invalid Date' && !currentlyWorkHere) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.internshipSectionLabel + ": " + this.security.internshipCompanyEndDateLabel + " is invalid");
        valid = false;
      }

      let dateStart: Date;

      if (!(internshipStartDate instanceof Date)) {
        dateStart = new Date(internshipStartDate);
      } else {
        dateStart = internshipStartDate;
      }

      let dateEnd: Date;
      if (!(internshipEndDate instanceof Date)) {
        dateEnd = new Date(internshipEndDate);
      } else {
        dateEnd = internshipEndDate;
      }

      if ((dateEnd.getTime() - dateStart.getTime() < 0) && !currentlyWorkHere) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.internshipSectionLabel + ": " + this.security.internshipCompanyStartDateLabel + " should be before " + this.security.internshipCompanyEndDateLabel);
        valid = false;
      }
      if ((currentDate.getTime() - dateStart.getTime() < 0) && !currentlyWorkHere) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.internshipCompanyStartDateLabel + " cannot be greater than Current Date");
        valid = false;
      }
    }

    if (!this.isDraft || isSaveCandidateDetails) {
      for (let workExpTO of requestJSON.internshipWorkExpTOs) {

        if (this.security.internshipCompanyRendered && this.security.internshipCompanyRequired && (workExpTO.companyName == null || workExpTO.companyName == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.internshipSectionLabel, 'label': this.security.internshipCompanyLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.internshipJobTitleRendered && this.security.internshipJobTitleRequired && (workExpTO.jobTitle == null || workExpTO.jobTitle == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.internshipSectionLabel, 'label': this.security.internshipJobTitleLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.internshipDescriptionRendered && this.security.internshipDescriptionRequired && (workExpTO.jobDescription == null || workExpTO.jobDescription == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.internshipSectionLabel, 'label': this.security.internshipDescriptionLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.internshipcompanyCTCRendered && this.security.internshipcompanyCTCRequired && (workExpTO.currencyCode == null || workExpTO.currencyCode.trim() == '' || workExpTO.ctc == null
          || workExpTO.ctc.trim() == '' || workExpTO.ctcPart == null || workExpTO.ctcPart.trim() == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.internshipSectionLabel, 'label': this.security.internshipcompanyCTCLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
      }
    }

    return valid;
  }

  validateEducationDetails(valid: boolean, requestJSON: SaveCandidateDetailsTO, isSaveCandidateDetails: boolean): boolean {

    let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
    let educationHistory = <FormArray>educationFormGroup.controls.educationHistory;
    let currentDate = new Date();

    for (let x = 0; x < educationHistory.length; x++) {

      let history = <FormGroup>educationHistory.at(x);
      let degree = history.controls.degree.value;

      let educationStartDate = history.controls.educationStartDate.value;
      let educationEndDate = history.controls.educationEndDate.value;

      let institute = history.controls.institute.value;
      let gradePercentage = history.controls.gradePercentage.value;

      if (!this.isDraft || isSaveCandidateDetails) {

        if (this.security.degreeRendered
          && this.security.degreeRequired
          && (degree == null || degree == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.educationSectionLabel, 'label': this.security.degreeLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.startDateRendered
          && this.security.startDateRequired
          && (educationStartDate == '' || educationStartDate == null)) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.educationSectionLabel, 'label': this.security.startDateLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.endDateRendered
          && this.security.endDateRequired
          && (educationEndDate == '' || educationEndDate == null)) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.educationSectionLabel, 'label': this.security.endDateLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.instituteRendered
          && this.security.instituteRequired
          && (institute == null || institute == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.educationSectionLabel, 'label': this.security.instituteLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

        if (this.security.gradePercentageRendered
          && this.security.gradePercentageRequired
          && (gradePercentage == null || gradePercentage == '')) {
          this.i18UtilService.get('common.warning.labelRequired', { 'section': this.security.educationSectionLabel, 'label': this.security.gradePercentageLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

      }

      if ((educationStartDate != null && educationStartDate != '')
        && educationStartDate.toString() == 'Invalid Date') {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.educationSectionLabel + ": " + this.security.startDateLabel + " is invalid");
        valid = false;
      }

      if ((educationEndDate != null && educationEndDate != '')
        && educationEndDate.toString() == 'Invalid Date') {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.educationSectionLabel + ": " + this.security.endDateLabel + " is invalid");
        valid = false;
      }
      let dateStart: Date;

      if (!(educationStartDate instanceof Date)) {
        dateStart = new Date(educationStartDate);
      } else {
        dateStart = educationStartDate;
      }

      let dateEnd: Date;
      if (!(educationEndDate instanceof Date)) {
        dateEnd = new Date(educationEndDate);
      } else {
        dateEnd = educationEndDate;
      }
      if (dateEnd.getTime() - dateStart.getTime() < 0) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.educationSectionLabel + ": " + this.security.startDateLabel + " should be before " + this.security.endDateLabel);
        valid = false;
      }
      if (currentDate.getTime() - dateStart.getTime() < 0) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.educationSectionLabel + ": " + this.security.startDateLabel + " cannot be greater than Current Date");
        valid = false;
      }
    }

    return valid;
  }

  initRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    if (requestJSON == null || requestJSON == undefined) {
      requestJSON = new SaveCandidateDetailsTO();
      requestJSON.security = this.security;
    }
    if (requestJSON.candidateAddressTO == null || requestJSON.candidateAddressTO == undefined) {
      requestJSON.candidateAddressTO = new CandidateAddressTO();
    }
    if (requestJSON.permanentAddressTO == null || requestJSON.permanentAddressTO == undefined) {
      requestJSON.permanentAddressTO = new CandidateAddressTO();
    }
    return requestJSON;
  }

  setBasicDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let ccfg = <FormGroup>this.candidateForm.controls.createCandidateSection;
    let basicGroup = <FormGroup>this.candidateForm.controls.basicSection;

    let subSourceUserID = basicGroup.controls.sourceName.value;
    let subSourceTypeID = basicGroup.controls.sourceCode.value;
    let subSourceTypeName = undefined;
    if (this.sourceCodeList.length == 0 && !this.sourceNameEnable) {
      subSourceTypeName = basicGroup.controls.sourceName.value;
      subSourceUserID = undefined;
      subSourceTypeID = undefined;
    } else if (!this.sourceNameEnable) {
      subSourceUserID = subSourceTypeID;
      subSourceTypeID = undefined;
    }

    requestJSON.age = basicGroup.controls.candidateAge.value
    requestJSON.candidateCode = this.candidateForm.controls.candidateCode.value;
    requestJSON.candidateId = this.candidateForm.controls.candidateId.value;
    if (this.isDraft) {
      requestJSON.requisitionID = (ccfg.controls.taggedOrUntagged.value != 'tagged') ? 0 : ccfg.controls.jobTitle.value;
    }
    requestJSON.firstName = basicGroup.controls.firstName.value;
    requestJSON.middleName = basicGroup.controls.middleName.value;
    requestJSON.lastName = basicGroup.controls.lastName.value;
    requestJSON.internationalID = basicGroup.controls.internationalID.value;
    requestJSON.mobileNumber = basicGroup.controls.mobileNumber.value;
    requestJSON.primaryEmail = basicGroup.controls.primaryEmail.value;
    requestJSON.secondaryEmail = basicGroup.controls.secondaryEmail.value;
    requestJSON.dateOfBirth = basicGroup.controls.dateOfBirth.value;
    requestJSON.genderID = basicGroup.controls.gender.value;
    requestJSON.panNumber = basicGroup.controls.panNumber.value;
    requestJSON.voterID = basicGroup.controls.voterId.value;
    requestJSON.passportNumber = basicGroup.controls.passportNumber.value;
    requestJSON.aadhar = basicGroup.controls.aadhaarNumber.value;
    requestJSON.industryID = basicGroup.controls.industry.value;
    requestJSON.functionalAreaID = basicGroup.controls.functionalArea.value;
    requestJSON.sourceTypeID = basicGroup.controls.sourceType.value;
    requestJSON.subSourceTypeID = subSourceTypeID;
    requestJSON.subSourceUserID = subSourceUserID;
    requestJSON.subSourceTypeName = subSourceTypeName;
    requestJSON.inReview = this.isInReview;
    requestJSON.triggerPoint = 'New Recruiter Portal Candidate Creation';

    return requestJSON;
  }

  setPersonalDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let personalFormGroup = <FormGroup>this.candidateForm.controls.personalSection;

    requestJSON.faceBookProfile = personalFormGroup.controls.facebookProfile.value;
    requestJSON.twitterProfile = personalFormGroup.controls.twitterProfile.value;
    requestJSON.linkedInProfile = personalFormGroup.controls.linkedInProfile.value;
    requestJSON.googlePlusProfile = personalFormGroup.controls.googlePlusProfile.value;
    requestJSON.candidateAddressTO.addressLine1 = personalFormGroup.controls.addressLine1.value;
    requestJSON.candidateAddressTO.addressLine2 = personalFormGroup.controls.addressLine2.value;
    requestJSON.candidateAddressTO.city = personalFormGroup.controls.city.value;
    requestJSON.candidateAddressTO.state = personalFormGroup.controls.state.value;
    requestJSON.candidateAddressTO.country = personalFormGroup.controls.country.value;
    requestJSON.candidateAddressTO.pincode = personalFormGroup.controls.pinCode.value;
    requestJSON.permanentAddressTO.addressLine1 = personalFormGroup.controls.permanentAddressLine1.value;
    requestJSON.permanentAddressTO.addressLine2 = personalFormGroup.controls.permanentAddressLine2.value;
    requestJSON.permanentAddressTO.city = personalFormGroup.controls.permanentCity.value;
    requestJSON.permanentAddressTO.state = personalFormGroup.controls.permanentState.value;
    requestJSON.permanentAddressTO.country = personalFormGroup.controls.permanentCountry.value;
    requestJSON.permanentAddressTO.pincode = personalFormGroup.controls.permanentPinCode.value;

    return requestJSON;
  }

  setWorkDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let workFormGroup = <FormGroup>this.candidateForm.controls.workSection;

    requestJSON.expInYears = workFormGroup.controls.totalExperienceYears.value;
    requestJSON.expInMonths = workFormGroup.controls.totalExperienceMonths.value;
    requestJSON.noticePeriod = workFormGroup.controls.noticePeriod.value;

    let employmentHistory = <FormArray>workFormGroup.controls.employmentHistory;

    let workExpTOs: Array<WorkExpTO> = [];
    for (let x = 0; x < employmentHistory.length; x++) {
      let history = <FormGroup>employmentHistory.at(x);

      let workExpTO: WorkExpTO = new WorkExpTO();

      let workStartDate = history.controls.workStartDate.value;
      let workEndDate = history.controls.workEndDate.value;
      if (workStartDate != null) {
        workExpTO.startDate = workStartDate;
      }
      if (workEndDate != null) {
        workExpTO.endDate = workEndDate;
      }
      workExpTO.candidateEmpDetailId = history.controls.workDetailId.value
      workExpTO.companyName = history.controls.company.value;
      workExpTO.jobTitle = history.controls.jobTitle.value;
      workExpTO.currentEmployer = history.controls.currentlyWorkHere.value;
      workExpTO.currencyCode = history.controls.ctcCurrency.value;
      workExpTO.ctc = history.controls.ctcAmount.value;
      workExpTO.ctcPart = history.controls.ctcDenomination.value;
      workExpTO.jobDescription = history.controls.description.value;

      workExpTOs.push(workExpTO);
    }
    requestJSON.workExpTOs = workExpTOs
    return requestJSON;
  }

  setInternshipDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let internshipFormGroup = <FormGroup>this.candidateForm.controls.internshipSection;
    let employmentHistory = <FormArray>internshipFormGroup.controls.internshipHistory;

    let workExpTOs: Array<WorkExpTO> = [];
    for (let x = 0; x < employmentHistory.length; x++) {
      let history = <FormGroup>employmentHistory.at(x);

      let workExpTO: WorkExpTO = new WorkExpTO();

      let internshipStartDate = history.controls.internshipStartDate.value;
      let internshipEndDate = history.controls.internshipEndDate.value;
      if (internshipStartDate != null) {
        workExpTO.startDate = internshipStartDate;
      }
      if (internshipEndDate != null) {
        workExpTO.endDate = internshipEndDate;
      }
      workExpTO.candidateEmpDetailId = history.controls.internshipDetailId.value;
      workExpTO.companyName = history.controls.company.value;
      workExpTO.jobTitle = history.controls.jobTitle.value;
      workExpTO.currentEmployer = history.controls.currentlyWorkHere.value;
      workExpTO.jobDescription = history.controls.description.value;
      workExpTOs.push(workExpTO);
    }
    requestJSON.internshipWorkExpTOs = workExpTOs
    return requestJSON;
  }

  setSkillDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let skillSetSection = <FormGroup>this.candidateForm.controls.skillSetSection;
    let skillsArray = <FormArray>skillSetSection.controls.skills;

    let skillTOs: Array<SkillTO> = []

    for (let i = 0; i < skillsArray.length; i++) {
      let skillGroup = <FormGroup>skillsArray.at(i);
      let skillTO = new SkillTO();
      skillTO.skillId = skillGroup.controls.skillId.value;
      skillTO.skillName = skillGroup.controls.skillName.value;
      skillTOs.push(skillTO);
    }

    requestJSON.skillTOs = skillTOs;

    return requestJSON;
  }

  setLanguageDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let languageSetSection = <FormGroup>this.candidateForm.controls.languageSetSection;
    let languagesArray = <FormArray>languageSetSection.controls.languages;

    let languageTOs: Array<SkillTO> = []

    for (let i = 0; i < languagesArray.length; i++) {
      let languageGroup = <FormGroup>languagesArray.at(i);
      let languageTO = new SkillTO();
      languageTO.skillId = languageGroup.controls.languageId.value;
      languageTO.skillName = languageGroup.controls.languageName.value;
      languageTOs.push(languageTO);
    }

    requestJSON.languageTOs = languageTOs;

    return requestJSON;
  }

  setEducationDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO) {
    requestJSON = this.initRequestJSON(requestJSON);

    let educationFormGroup = <FormGroup>this.candidateForm.controls.educationSection;
    let educationHistory = <FormArray>educationFormGroup.controls.educationHistory;

    let educationTOs: Array<EducationTO> = [];
    for (let x = 0; x < educationHistory.length; x++) {
      let educationTO: EducationTO = new EducationTO();

      let history = <FormGroup>educationHistory.at(x);
      let educationStartDate = history.controls.educationStartDate.value;
      let educationEndDate = history.controls.educationEndDate.value;
      if (educationStartDate != null) {
        educationTO.startDate = educationStartDate;
      }
      if (educationEndDate != null) {
        educationTO.endDate = educationEndDate;
      }
      educationTO.candidateEduDetailId = history.controls.eduDetailId.value;
      educationTO.degree = history.controls.degree.value;
      educationTO.institute = history.controls.institute.value;
      educationTO.percentage = history.controls.gradePercentage.value;

      educationTOs.push(educationTO);
    }

    requestJSON.educationTOs = educationTOs;

    return requestJSON;
  }

  setCandidateDetailsRequestJSON(requestJSON) {
    requestJSON = this.setBasicDetailsRequestJSON(requestJSON);
    requestJSON = this.setPersonalDetailsRequestJSON(requestJSON);
    requestJSON = this.setWorkDetailsRequestJSON(requestJSON);
    requestJSON = this.setInternshipDetailsRequestJSON(requestJSON);
    requestJSON = this.setSkillDetailsRequestJSON(requestJSON);
    requestJSON = this.setLanguageDetailsRequestJSON(requestJSON);
    requestJSON = this.setEducationDetailsRequestJSON(requestJSON);

    return requestJSON;
  }

  validateCandidateDetails(requestJSON, isSaveCandidateDetails: boolean) {
    let valid: boolean = true;
    valid = this.validateBasicDetails(valid, requestJSON, isSaveCandidateDetails);
    valid = this.validatePersonalDetails(valid, requestJSON, isSaveCandidateDetails);
    valid = this.validateWorkDetails(valid, requestJSON, isSaveCandidateDetails);
    valid = this.validateInternshipDetails(valid, requestJSON, isSaveCandidateDetails);
    valid = this.validateEducationDetails(valid, requestJSON, isSaveCandidateDetails);
    valid = this.validateDocument(valid);
    return valid;
  }

  fetchSessionObjectData() {
    let object = this.sessionService.object;
    this.candidateForm.get('candidateId').patchValue(object.requestJSON.candidateId);
    this.newCandidateService.saveCandidateDetails(object.requestJSON.candidateId, object.requestJSON, this.isDraft, 'basic').subscribe(response => {
      this.sessionService.object = undefined;
    });
  }

  currentSameAsPermanent(security: any): boolean {
    let personalFormGroup = <FormGroup>this.candidateForm.controls.personalSection;
    if (this.security.permanentAddressRendered) {
      if (security.addressLine1Rendered) {
        if (personalFormGroup.controls.addressLine1.value != '' && personalFormGroup.controls.addressLine1.value != personalFormGroup.controls.permanentAddressLine1.value) {
          return false;
        }
      }
      if (security.addressLine2Rendered) {
        if (personalFormGroup.controls.addressLine2.value != '' && personalFormGroup.controls.addressLine2.value != personalFormGroup.controls.permanentAddressLine2.value) {
          return false;
        }
      }
      if (security.cityRendered) {
        if (personalFormGroup.controls.city.value != '' && personalFormGroup.controls.city.value != personalFormGroup.controls.permanentCity.value) {
          return false;
        }
      }
      if (security.stateRendered) {
        if (personalFormGroup.controls.state.value != '' && personalFormGroup.controls.state.value != personalFormGroup.controls.permanentState.value) {
          return false;
        }
      }
      if (security.countryRendered) {
        if (personalFormGroup.controls.country.value != '' && personalFormGroup.controls.country.value != personalFormGroup.controls.permanentCountry.value) {
          return false;
        }
      }
      if (security.pinCodeRendered) {
        if (personalFormGroup.controls.pinCode.value != '' && personalFormGroup.controls.pinCode.value != personalFormGroup.controls.permanentPinCode.value) {
          return false;
        }
      }
      if (personalFormGroup.controls.addressLine1.value == undefined) {
        return false
      }
      return true;
    }
    return false;
  }

  validateThaiID(id :string): boolean {
    id = id.replace(/-/g,'');
    id = id.replace(/ /g,'');
    let sum = 0;
    if (id.length != 13) {
      return false
    }
    for (let i = 0; i < 12; i++) {
      sum += parseInt(id.charAt(i)) * (13 - i);
    }
    if ((11 - sum % 11) % 10 != parseInt(id.charAt(12)))
        return false;
    return true;
  }

  getTranslatedText(text: string): string {
    let result = '';
    this.i18UtilService.get('common.label.' + text).subscribe((res: string) => {
      result = res;
    });
    return result;
  }

}

export class Month {
  value: Number;
  label: String;
}

export class SectionNumber {
  public section: string;
  public number: number;
}