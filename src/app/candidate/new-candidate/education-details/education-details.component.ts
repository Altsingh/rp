import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CandidateController } from '../candidate-interfaces';

@Component({
  selector: 'alt-candidate-education-details',
  templateUrl: './education-details.component.html'
})
export class EducationDetailsComponent implements OnInit {

  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;

  degrees: String[];
  subscription;

  constructor(public formBuilder: FormBuilder) {
  }

  continue() {
    this.candidateController.continueEducation();
  }

  remove(index: number) {
    this.candidateController.removeEducationHistory(index);
  }

  clearAll(event) {
    event.stopPropagation();
    this.candidateController.clearAllEducation();
  }
  ngOnInit() {
  }

  setStartDate(startDate: Date, item: FormGroup) {
    if (startDate != null) {
      item.controls['educationStartDate'].setValue(startDate);
    } else {
      item.controls['educationStartDate'].reset();
    }
  }

  setEndDate(endDate: Date, item: FormGroup) {
    if (endDate != null) {
      item.controls['educationEndDate'].setValue(endDate);
    } else {
      item.controls['educationEndDate'].reset();
    }
  }

  addEducationHistory() {
    this.candidateController.addEducationHistory();
  }
}