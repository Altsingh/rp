import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { DialogService } from '../../../shared/dialog.service';
import { CandidateAddressService } from '../../candidate-address.service';
import { CandidateController } from '../candidate-interfaces';

@Component({
  selector: 'alt-candidate-basic-details',
  templateUrl: './basic-details.component.html',
  providers: [JsonpModule, DialogService],
})
export class BasicDetailsComponent implements OnInit {

  locationSuggestion: any;
  data: any;
  name: string;
  addresstype: string[];

  WorkClassVisible: boolean = false;
  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;

  hiddenToggle: boolean = false;
  hiddenToggle2: boolean = false;
  divClass: string = "";
  sameAddress: boolean = false;
  renderPermanentAddress: boolean = false;
  requiredPermanentAddress: boolean = false;

  subscription;

  constructor(
    public formBuilder: FormBuilder,
    private candidateAddressService: CandidateAddressService) {

  }

  continue() {
    this.candidateController.continuePersonal();
  }

  ngOnInit() {
    this.formGroup.valueChanges.subscribe(changedValue => {
      if(changedValue!=null) {
        this.sameAddress = changedValue.sameAddress; 
      }
    });
    this.addresstype = ['sublocality_level_2', 'sublocality_level_1', 'administrative_area_level_2', 'administrative_area_level_1', 'country', 'postal_code'];
    this.determineRenderingForPermanentAddress(this.security);
  }
  public clear(event) {
    event.stopPropagation();
    this.candidateController.clearAllPersonal();
  }

  public loadSuggestions(isCurrentAddress: boolean) {
    let searchString: string = "";
    if (isCurrentAddress) {
      searchString = this.formGroup.controls.addressLine1.value;
    } else {
      searchString = this.formGroup.controls.permanentAddressLine1.value;
    }
    if (searchString == null || searchString.trim() == null || searchString.trim() == '') {
      this.data = null;
    } else {
      this.candidateAddressService.getLocationSuggestion(searchString.trim()).subscribe(response => {
        if (response) {
          if (response.response) {
            if (response.response.predictions.length > 0) {
              if (isCurrentAddress) {
                this.hiddenToggle = true;
              } else {
                this.hiddenToggle2 = true;
              }

              this.data = response.response.predictions;
            } else {
              this.data = null;
            }
          }
        }
      });
    }
  }

  getPlaceDetails(placeid: string, isCurrentAddress: boolean) {
    if (isCurrentAddress) {
      this.hiddenToggle = false;
    } else {
      this.hiddenToggle2 = false;
    }
    this.candidateAddressService.getLocationDetails(placeid).subscribe(response => {
      if (response) {
        if (response.response) {
          if (response.response.result) {
            this.data = response.response.result.address_components;
            this.name = response.response.result.name;
            this.populateFields(this.data, this.name, isCurrentAddress);
            this.copyCurrentToPermanent();
          }
        }
      }
    });
  }

  toggleClose() {
    this.hiddenToggle = false;
    this.divClass = "";
  }
  toggleClose2() {
    this.hiddenToggle2 = false;
    this.divClass = "";
  }
  omit_special_char(event) {
    var k;
    k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }


  populateFields(data: any, name: string, isCurrentAddress: boolean) {
    let add = '';
    let city = '';
    let state = '';
    let country = '';
    let postal = '';
    for (let item of this.data) {
      let type = item.types[0];
      if (type == 'sublocality_level_2') {
        add = item.long_name + add;
      } else if (type == 'sublocality_level_1') {
        add = item.long_name + ' ' + add;
      } else if (type == 'administrative_area_level_2') {
        city = item.long_name;
      } else if (type == 'administrative_area_level_1') {
        state = item.long_name;
      } else if (type == 'country') {
        country = item.long_name;
      } else if (type == 'postal_code') {
        postal = item.long_name;
      }
    }
    if (isCurrentAddress) {
      if (name) {
        this.formGroup.controls.addressLine1.setValue(name);
      } else {
        this.formGroup.controls.addressLine1.setValue('');
      }
      this.formGroup.controls.addressLine2.setValue(add);
      this.formGroup.controls.city.setValue(city);
      this.formGroup.controls.state.setValue(state);
      this.formGroup.controls.country.setValue(country);
      this.formGroup.controls.pinCode.setValue(postal);
    } else {
      if (name) {
        this.formGroup.controls.permanentAddressLine1.setValue(name);
      } else {
        this.formGroup.controls.permanentAddressLine1.setValue('');
      }
      this.formGroup.controls.permanentAddressLine2.setValue(add);
      this.formGroup.controls.permanentCity.setValue(city);
      this.formGroup.controls.permanentState.setValue(state);
      this.formGroup.controls.permanentCountry.setValue(country);
      this.formGroup.controls.permanentPinCode.setValue(postal);
    }
  }
  singleSelectEvent() {
    this.sameAddress = !this.sameAddress;
    this.formGroup.controls.sameAddress.setValue(!this.formGroup.controls.sameAddress.value);
    if (this.sameAddress) {
      this.formGroup.controls.permanentAddressLine1.setValue(this.formGroup.controls.addressLine1.value);
      this.formGroup.controls.permanentAddressLine2.setValue(this.formGroup.controls.addressLine2.value);
      this.formGroup.controls.permanentCity.setValue(this.formGroup.controls.city.value);
      this.formGroup.controls.permanentState.setValue(this.formGroup.controls.state.value);
      this.formGroup.controls.permanentCountry.setValue(this.formGroup.controls.country.value);
      this.formGroup.controls.permanentPinCode.setValue(this.formGroup.controls.pinCode.value);
    } else {
      this.formGroup.controls.permanentAddressLine1.setValue('');
      this.formGroup.controls.permanentAddressLine2.setValue('');
      this.formGroup.controls.permanentCity.setValue('');
      this.formGroup.controls.permanentState.setValue('');
      this.formGroup.controls.permanentCountry.setValue('');
      this.formGroup.controls.permanentPinCode.setValue('');
    }
  }
  copyCurrentToPermanent() {
    if (this.sameAddress) {
      this.formGroup.controls.permanentAddressLine1.setValue(this.formGroup.controls.addressLine1.value);
      this.formGroup.controls.permanentAddressLine2.setValue(this.formGroup.controls.addressLine2.value);
      this.formGroup.controls.permanentCity.setValue(this.formGroup.controls.city.value);
      this.formGroup.controls.permanentState.setValue(this.formGroup.controls.state.value);
      this.formGroup.controls.permanentCountry.setValue(this.formGroup.controls.country.value);
      this.formGroup.controls.permanentPinCode.setValue(this.formGroup.controls.pinCode.value);
    }
  }
  determineRenderingForPermanentAddress(security: any) {
    if (!security.addressLine1Rendered && !security.addressLine2Rendered && !security.cityRendered && !security.stateRendered && !security.countryRendered && !security.pinCodeRendered && security.permanentAddressRendered) {
      this.renderPermanentAddress = true;
      if (security.permanentAddressRequired) {
        this.requiredPermanentAddress = true;
        this.formGroup.controls.requiredPermanentAddress.setValue(this.requiredPermanentAddress);
      }
    }
  }
}
