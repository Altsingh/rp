import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CandidateController } from '../candidate-interfaces';


@Component({
  selector: 'alt-internship-details',
  templateUrl: './internship-details.component.html',
  styleUrls: ['./internship-details.component.css']
})
export class InternshipDetailsComponent implements OnInit {

  @Input()
  security: any;
  @Input()
  candidateController: CandidateController;
  @Input()
  formGroup: FormGroup;

  subscription;
  industries: String[];
  constructor(
    public formBuilder: FormBuilder
  ) {

  }
  clearAll(event) {
    event.stopPropagation();
    this.candidateController.clearAllInternship();
  }
  continue() {
    this.candidateController.continueInternship();
  }

  ngOnInit() {
  }

  addInternshipHistory() {
    this.candidateController.addInternshipHistory();
  }

  deleteInternshipHistory(index: number) {
    this.candidateController.removeInternshipHistory(index);
  }
  setStartDate(startDate: Date, item: FormGroup) {
    if (startDate != null) {
      item.controls['internshipStartDate'].setValue(startDate);
    } else {
      item.controls['internshipStartDate'].reset();
    }
  }

  setEndDate(endDate: Date, item: FormGroup) {
    if (endDate != null) {
      item.controls['internshipEndDate'].setValue(endDate);
    } else {
      item.controls['internshipEndDate'].reset();
    }
  }
  clearEndDate(item: FormGroup) {
    if (item != null) {
      if (item.controls.currentlyWorkHere.value == true) {
        item.controls.endDateDD.setValue('');
        item.controls.endDateMM.setValue('');
        item.controls.endDateYYYY.setValue('');
      }
    }

  }

}
