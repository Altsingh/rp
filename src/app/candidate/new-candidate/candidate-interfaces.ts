import { Month,SectionNumber } from './new-candidate.component';
export interface CandidateController {
  continueCreateCandidate();
  clearCreateCandidate(event);
  continueBasic();
  clearAllBasic(event);
  continuePersonal();
  clearAllPersonal();
  addWorkHistory();
  removeWorkHistory(index: number);
  continueWork();
  clearAllWork();
  addInternshipHistory();
  removeInternshipHistory(index: number);
  continueInternship();
  clearAllInternship();
  addEducationHistory();
  removeEducationHistory(index: number);
  continueEducation();
  clearAllEducation();
  getCandidateId(): any;
  continueSkillSet();
  clearAllSkillSet();
  removeSkill(index: number);
  continueLanguageSet();
  clearAllLanguageSet();
  removeLanguage(index: number);
  continueAttachDocument();
  continueVoila();
  toggleBasic();
  togglePersonal();
  toggleWork();
  toggleInternship();
  toggleEducation();
  toggleLanguageSet();
  toggleSkillSet();
  toggleAttachDocument();
  toggleVoila();
  downloadDocument(index: number);
  uploadDocument(event, index: number, documentTypeID: number);
  deleteDocument(index: number);





  createCandidateSectionActive: boolean;
  createCandidateSectionBlock: boolean;
  basicSectionActive: boolean;
  basicSectionBlock: boolean;
  personalSectionActive: boolean;
  personalSectionBlock: boolean;
  workSectionActive: boolean;
  workSectionBlock: boolean;
  internshipSectionActive: boolean;
  internshipSectionBlock: boolean;
  educationSectionActive: boolean;
  educationSectionBlock: boolean;
  skillsSectionActive: boolean;
  skillsSectionBlock: boolean;
   languagesSectionActive: boolean;
  languagesSectionBlock: boolean;
  documentSectionActive: boolean;
  documentSectionBlock: boolean;
  voilaSectionActive: boolean;
  voilaSectionBlock: boolean;
  
  personalSectionDone: boolean;
  workSectionDone: boolean;
  internshipSectionDone: boolean;
  educationSectionDone: boolean;
  skillsSectionDone: boolean;
  languagesSectionDone:boolean;
  documentSectionDone: boolean;


  daysList: number[];
  monthsList: Month[];
  yearList: number[];
  documentsList: any[];

  isDraft:boolean;

  getSectionNumber(index:string);
}