import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { CachedData } from '../common/cached-data'
import { CandidateFormSecurityService } from './candidate-form-security.service';
import { CandidateGenderListService } from './candidate-gender-list.service';
import { CandidateIndustryListService } from './candidate-industry-list.service';
import { CandidateFunctionalAreaListService } from './candidate-functional-area-list.service';
import { CandidateSourceTypeListService } from './candidate-source-type-list.service';

@Injectable()
export class CandidateService {



  constructor(
    private formSecurityService: CandidateFormSecurityService,
    private genderListService: CandidateGenderListService,
    private industryListService: CandidateIndustryListService,
    private functionalAreaListService: CandidateFunctionalAreaListService,
    private sourceTypeListService: CandidateSourceTypeListService,
    private http: Http,
    private sessionService: SessionService) {

  }

  getCandidateFormSecurity(): Observable<any> {
    return this.formSecurityService.getData();
  }

  clearCandidateFormSecurity() {
    this.formSecurityService.clearCache();
  }

  getGenderList(): Observable<any> {
    return this.genderListService.getData();
  }

  getIndustryList(): Observable<any> {
    return this.industryListService.getData();
  }

  getFunctionalAreaList(): Observable<any> {
    return this.functionalAreaListService.getData();
  }

  getSourceTypeList(): Observable<any> {
    return this.sourceTypeListService.getData();
  }

  getPincodeByID(pincodeID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/pincode/' + pincodeID+"/").map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getPincodeMaster(searchString: String, count: Number) {
    if (searchString == null) {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count+"/").map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
    } else {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + '/' + searchString+"/").map(res =>{
        this.sessionService.check401Response(res.json());
        return res.json()});
    }
  }

  getSourceCodeList(sourceTypeId, changedValue) {
    return this.http.get(this.sessionService.orgUrl + "/rest/altone/candidate/master/sourceCode/" + this.sessionService.organizationID + "/" + sourceTypeId+"/?q=" + changedValue).map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getSourceNameList(sourceTypeId, sourceCodeId) {
    return this.http.get(this.sessionService.orgUrl + "/rest/altone/candidate/master/sourceUsers/" + this.sessionService.organizationID + "/" + sourceTypeId + "/" + sourceCodeId+"/").map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getDocumentList(candidateId, isDraft: boolean) {
    return this.http.get(this.sessionService.orgUrl + "/rest/altone/candidate/fetch"+(isDraft?"/draft":"")+"/document/" + this.sessionService.organizationID + "/" + candidateId+"/").map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

}
