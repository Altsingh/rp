import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-candidates-untagged-blank-screen',
  templateUrl: './candidates-untagged-blank-screen.component.html',
  styleUrls: ['./candidates-untagged-blank-screen.component.css']
})
export class CandidatesUntaggedBlankScreenComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
