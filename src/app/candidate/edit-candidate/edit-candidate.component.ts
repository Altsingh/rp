import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParseCandidatecodeParamPipe } from '../parse-candidatecode-param.pipe';

@Component({
  selector: 'alt-edit-candidate',
  templateUrl: './edit-candidate.component.html',
  providers: [ParseCandidatecodeParamPipe]
})
export class EditCandidateComponent implements OnInit, OnDestroy {

  id: number;
  private sub: any;

  constructor(private route: ActivatedRoute, private parser: ParseCandidatecodeParamPipe) {
    window.scroll(0, 0);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = this.parser.transform(params['id']);
    });
  }

  requisitionCodeFormat(requisitionCode) {
    return requisitionCode.replaceAll('-', '/');
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
