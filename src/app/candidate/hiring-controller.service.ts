import { Injectable } from '@angular/core';
import { CandidateTaggedScreeningComponent } from './candidate-tagged-detail/candidate-tagged-screening/candidate-tagged-screening.component';
import { HiringProcessComponent } from './candidate-tagged-detail/hiring-process/hiring-process.component';

@Injectable()
export class HiringControllerService {

  candidateTaggedScreeningComponent: CandidateTaggedScreeningComponent;
  hiringProcessComponent: HiringProcessComponent;

  constructor() { }

  setCandidateTaggedScreeningComponent(candidateTaggedScreeningComponent: CandidateTaggedScreeningComponent) {
    this.candidateTaggedScreeningComponent = candidateTaggedScreeningComponent;
  }

  setHiringProcessComponent(hiringProcessComponent: HiringProcessComponent) {
    this.hiringProcessComponent = hiringProcessComponent;
  }
  getCandidateTaggedScreeningComponent(): CandidateTaggedScreeningComponent {
    return this.candidateTaggedScreeningComponent;
  }

  getHiringProcessComponent(): HiringProcessComponent {
    return this.hiringProcessComponent;
  }
}
