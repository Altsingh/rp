
import { Routes, RouterModule } from '@angular/router';
import { CandidateComponent } from './candidate.component';
import { CandidateListComponent } from './candidate-list/candidate-all/candidate-list.component';
import { DraftCandidateListComponent } from './candidate-list/candidate-draft/draft-candidate-list.component';
import { NewCandidateComponent } from './new-candidate/new-candidate.component';
import { EditCandidateComponent } from './edit-candidate/edit-candidate.component';
import { CandidateOverviewComponent } from './candidate-detail/candidate-overview/candidate-overview.component';
import { HiringProcessComponent } from './candidate-tagged-detail/hiring-process/hiring-process.component';
import { HistoryChatsComponent } from './candidate-tagged-detail/history-chats/history-chats.component';
import { EditCandidateTaggedComponent } from './edit-candidate-tagged/edit-candidate-tagged.component';
import { CandidateTaggedOverviewComponent } from './candidate-tagged-detail/candidate-tagged-overview/candidate-tagged-overview.component';
import { TaggedCandidateListComponent } from './candidate-list/taggedCandidates/tagged-candidate-list.component';
import { CandidateUntaggedListComponent } from './candidate-list/candidate-untagged/candidate-untagged-list.component';
import { SessionGuard } from '../session.guard';
import { BulkCandidateUploadComponent } from './bulk-candidate-upload/bulk-candidate-upload.component';
import { BulkParseComponent } from './bulk-parse/bulk-parse.component';
import { CandidateHistoryComponent } from './candidate-tagged-detail/candidate-history/candidate-history.component';
import { CandidateReferralListComponent } from './candidate-referral-list/candidate-referral-list.component';

const     CANDIDATE_ROUTES: Routes = [
    {
        path: '', component: CandidateComponent, children: [
            { path: 'newcandidate', component: NewCandidateComponent, canActivate: [SessionGuard] },
            { path: 'newcandidate/:id', component: NewCandidateComponent, canActivate: [SessionGuard] },
            { path: 'newcandidaterefer', component: NewCandidateComponent, canActivate: [SessionGuard] },
            { path: 'newcandidateijp', component: NewCandidateComponent, canActivate: [SessionGuard] },
            { path: 'draftCandidatelist', component: DraftCandidateListComponent, canActivate: [SessionGuard] },
            { path: 'untaggedCandidatelist', component: CandidateUntaggedListComponent, canActivate: [SessionGuard] },
            {
                path: 'edit-candidate/:id', component: EditCandidateComponent, children: [
                    { path: '', component: CandidateOverviewComponent }
                ]
            },
            {
                path: 'edit-candidate-tagged/:id', component: EditCandidateTaggedComponent, children: [
                    { path: '', redirectTo: 'candidate-tagged-overview' },
                    { path: 'candidate-tagged-overview', component: CandidateTaggedOverviewComponent },
                    { path: 'hiring-process', component: HiringProcessComponent },
                    { path: 'history-chats', component: HistoryChatsComponent },
                    {path: 'candidate-history', component: CandidateHistoryComponent}
                ]
            },
            { path: 'taggedcandidates', component: TaggedCandidateListComponent, canActivate: [SessionGuard] },
            { path: 'referral', component: CandidateReferralListComponent, canActivate: [SessionGuard] },
            { path: 'myapplicants', component: TaggedCandidateListComponent, canActivate: [SessionGuard] },
            { path: 'candidateUpload', component: BulkCandidateUploadComponent, canActivate: [SessionGuard] },
            { path: 'resumeBulkParse', component: BulkParseComponent, canActivate: [SessionGuard] }
        ]
    }
];

export const CandidateRouting = RouterModule.forChild(CANDIDATE_ROUTES);
