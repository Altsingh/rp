import { TestBed, inject } from '@angular/core/testing';

import { CandidateAddressService } from './candidate-address.service';

describe('CandidateAddressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CandidateAddressService]
    });
  });

  it('should be created', inject([CandidateAddressService], (service: CandidateAddressService) => {
    expect(service).toBeTruthy();
  }));
});
