import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class CandidateAddressService {

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  getLocationSuggestion(searchString: string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/address_suggesion/' + searchString).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json();
    });
  }
  getLocationDetails(placeid: string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/address_details/' + placeid).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json();
    });
  }
}
