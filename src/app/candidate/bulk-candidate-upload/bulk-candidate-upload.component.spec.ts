import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkCandidateUploadComponent } from './bulk-candidate-upload.component';

describe('BulkCandidateUploadComponent', () => {
  let component: BulkCandidateUploadComponent;
  let fixture: ComponentFixture<BulkCandidateUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkCandidateUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkCandidateUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
