import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonService } from '../../common/common.service';
import { SessionService } from '../../session.service';
import { NewCandidateService } from '../new-candidate/new-candidate.service';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { CandidateService } from '../candidate.service';
import { FormatCapitalizePipe } from '../../shared/format-capitalize.pipe';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-bulk-candidate-upload',
  templateUrl: './bulk-candidate-upload.component.html',
  styleUrls: ['./bulk-candidate-upload.component.css'],
  providers: [NewCandidateService]
})
export class BulkCandidateUploadComponent implements OnInit {

  orgUrl = '';

  spinning = false;
  failureSpinning = false;
  dropEnabled = false;
  dragging = false;
  uploading = false;

  parseSuccessList: any[];
  parseFailedList: any[];
  fieldSequence: any[];
  security: any;

  errorPanelOpen = false;
  successPanelOpen = false;

  formatCapitalizePipe: FormatCapitalizePipe = new FormatCapitalizePipe();

  @ViewChild('excelFile')
  excelFileInput: ElementRef;

  constructor(
    private sessionService: SessionService,
    private commonService: CommonService,
    private notificationService: NotificationService,
    private candidateService: CandidateService,
    private newCandidateService: NewCandidateService,
    private i18UtilService: I18UtilService
  ) {
    this.orgUrl = this.sessionService.orgUrl;
  }

  ngOnInit() {
    this.commonService.hideRHS();
    this.initFormSecurity();
    try {
      const test_canvas = document.createElement('canvas');
      this.dropEnabled = (test_canvas.getContext) ? true : false;
    } catch (e) {
      console.log('drag drop check failed', e);
    }
  }

  toggleSuccessPanel() {
    this.successPanelOpen = !this.successPanelOpen;
  }
  toggleErrorPanel() {
    this.errorPanelOpen = !this.errorPanelOpen;
  }

  dragLeaveHandler(event) {
    this.dragging = false;
    event.preventDefault();
  }
  dropHandler(event) {
    this.dragging = false;

    if (this.uploading) {
      return;
    }

    event.preventDefault();

    if (event) {
      if (event.dataTransfer) {
        if (event.dataTransfer.items) {

          const file = event.dataTransfer.items[0].getAsFile();

          this.uploadExcelFile(file);
        }
      }
    }
  }

  excelFileChange(event) {
    if (this.uploading) {
      return;
    }
    const fileCount: number = this.excelFileInput.nativeElement.files.length;
    this.uploadExcelFile(this.excelFileInput.nativeElement.files.item(0));
    event.target.value = '';

  }

  uploadExcelFile(file) {
    const fileName = <string>file.name;
    if (fileName) {
      if (!fileName.toLowerCase().endsWith('.xls')) {
        this.i18UtilService.get('common.warning.UnsupportedFileFormat').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
        return;
      }
    }

    if (this.uploading) {
      return;
    }
    this.uploading = true;
    this.parseSuccessList = [];
    this.parseFailedList = [];

    const formData = new FormData();
    formData.append('file', file);

    this.newCandidateService.bulkCandidateUpload(formData).subscribe((response) => {
      this.processCandidateUploadResponse(response);
      this.uploading = false;
    },
      (error) => {
        this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
        this.uploading = false;
      });
  }

  cancelErrors() {
    this.parseFailedList = [];
  }

  processCandidateUploadResponse(response) {

    this.notificationService.clear();

    if (response) {
      if (response.response) {

        if (response.response.parsed === false) {
          this.i18UtilService.get('candidate.error.templateProcessFailed').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
        } else {

          if (response.response.parseSuccessList) {
            this.parseSuccessList = response.response.parseSuccessList;
          }

          if (response.response.parseFailedList) {
            this.parseFailedList = response.response.parseFailedList;
            if (response.response.parseFailedList.length > 0) {
              this.errorPanelOpen = true;
              this.i18UtilService.get('candidate.warning.parsedSummary', {'parsedRecordCount': this.parseSuccessList.length, 'failededRecordCount': this.parseFailedList.length }).subscribe((res: string) => {
                this.notificationService.addMessage(NotificationSeverity.WARNING, res);
              });
            } else {
              this.i18UtilService.get('candidate.success.parsedSummary', {'parsedRecordCount': this.parseSuccessList.length }).subscribe((res: string) => {
                this.notificationService.addMessage(NotificationSeverity.INFO, res);
              });
            }
          }
        }

        if (response.response.errorMessages) {

          for (const errorMessage of response.response.errorMessages) {
            this.notificationService.addMessage(NotificationSeverity.WARNING, errorMessage);
          }
        }
      }
    }
  }

  uploadFailures() {
    this.failureSpinning = true;
    this.newCandidateService.uploadFailures(this.parseFailedList).subscribe((response) => {
      if (response) {
        if (response.response) {
          const redisKey = response.response;
          const templateDownloadUrl = this.sessionService.orgUrl + '/rest/altone/candidate/bulkCandidateTemplateDownloadFailures/' + redisKey;
          window.open(templateDownloadUrl);
        }
      }

      this.failureSpinning = false;
    });
  }

  dragOverHandler(event) {
    const dt = event.dataTransfer;

    if (dt && dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') !== -1 : dt.types.contains('Files'))) {
      this.dragging = true;
    }
    event.preventDefault();
  }

  spin() {
    if (this.uploading) {
      return;
    }
    this.spinning = true;
    setTimeout(() => {

      this.spinning = false;
    }, 3000);
  }



  private initFormSecurity() {
    this.candidateService.clearCandidateFormSecurity();
    this.candidateService.getCandidateFormSecurity().subscribe(
      response => {
        this.security = response.responseData[0];
        if (this.security != null) {
          this.security.basicSectionRendered = true;

          const allKeys = Object.keys(this.security);

          for (let x = 0; x < allKeys.length; x++) {
            if (this.security[allKeys[x]] != null && allKeys[x].toLowerCase().endsWith('label')) {
              if (allKeys[x] === 'companyCTCLabel'
                || allKeys[x] === 'panNumberLabel'
                || allKeys[x] === 'dateOfBirthLabel') {
                continue;
              }
              this.security[allKeys[x]] = this.formatCapitalizePipe.transform(this.security[allKeys[x]]);
            }
          }
        }
      }
    );

  }
}
