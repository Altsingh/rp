import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { Http, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { CachedData } from '../common/cached-data'

@Injectable()
export class CandidateSourceTypeListService extends CachedData {

  constructor(
    http: Http,
    sessionService: SessionService) {

    super(http, sessionService);

    super.initialize(
      '/rest/altone/candidate/master/sourceType/'+sessionService.organizationID+"/",
      'GET',
      {});
  }

}
