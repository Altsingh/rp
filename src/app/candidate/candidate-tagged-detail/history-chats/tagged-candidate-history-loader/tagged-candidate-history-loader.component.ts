import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-tagged-candidate-history-loader',
  templateUrl: './tagged-candidate-history-loader.component.html',
  styleUrls: ['./tagged-candidate-history-loader.component.css']
})
export class TaggedCandidateHistoryLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
