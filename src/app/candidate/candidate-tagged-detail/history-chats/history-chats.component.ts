import { Component, OnInit } from '@angular/core';
import { HistoryChatsService } from './history-chats.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-history-chats',
  templateUrl: './history-chats.component.html',
  providers: [HistoryChatsService]
})
export class HistoryChatsComponent implements OnInit {

  applicantStatusID: number;
  Candidatecode:String;
  responseData: ResponseData;
  candidateHistoryResponseData:CandidateHistoryResponseData[];
  loader:boolean = false;
  candidateHistoryloader:boolean = false;
  candidateHisoryLoaded:boolean=false;

  constructor(
    private historyChatsService: HistoryChatsService,
    private route: ActivatedRoute,
    ) {
    this.route.queryParams.subscribe((queryParams) => {
      this.applicantStatusID = queryParams['appid'];
      this.initializeData(this.applicantStatusID);
    })
  }

  ngOnInit() {
  }

  loadCandidateHistoryData(Candidatecode: String) {
    this.candidateHistoryloader=true;
    this.historyChatsService.getCandidateHistory(Candidatecode).subscribe(response => {
      this.candidateHistoryResponseData=response.responseData;
      this.candidateHistoryloader = false;
    });
  }

  initializeData(applicantStatusID: number) {
    this.loader = true;
    this.historyChatsService.getHiringProcessHistory(applicantStatusID).subscribe(response => {
      this.responseData = response.responseData;
      this.loader = false;
    });
  }
}

export class ResponseData {
  hiringProcessHistory: {
    history: [{
      actionLabel: String;
      sysWorkflowStageType: String;
      sysWorkflowstageActionType: String;
      workflowStageName: String;
      isCurrentStageStatus: Boolean;
      sequence: String;
      details: [{
        applicantStatusHistoryID: Number;
        comment: String;
        createdDate: String;
        userID: Number;
        userName: String;
        attachmentPath: String;
        nextStage: {
          sysWorkflowStageType: String;
          sysWorkflowstageActionType: String;
          workflowStageName: String;
          actionLabel: String;
        }
        previousStage: {
          sysWorkflowStageType: String,
          sysWorkflowstageActionType: String
          workflowStageName: String;
          actionLabel: String;
        }
      }]
    }]
  }
}

export class CandidateHistoryResponseData {
  codeID: String;
  eventDate: String;
  actorName: String;
  eventBasedHistoryID:number;
  eventName:String;
  eventDescription: String;
  sourceName: String;
  requisitionID: String;
  oldRequisitionID: String;
  candidateHistoryID: String;
  section: String;
  candidateActionHistoryID: any;
}