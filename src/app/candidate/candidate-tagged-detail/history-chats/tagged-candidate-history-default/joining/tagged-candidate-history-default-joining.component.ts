import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JoiningService } from '../../../hiring-process/joining/joining.service';

@Component({
  selector: 'alt-tagged-candidate-history-default-joining',
  templateUrl: './tagged-candidate-history-default-joining.component.html',
  styleUrls: ['./tagged-candidate-history-default-joining.component.css'],
  providers: [JoiningService]
})
export class TaggedCandidateHistoryDefaultJoiningComponent implements OnInit {
@Input() data:any;
applicantStatusID : Number;
expectedJoiningDate : Date;
actualJoiningDate : Date;
employeeCode      : String;


  constructor(  private route: ActivatedRoute,
                private joiningService : JoiningService) {
    this.route.queryParams.subscribe((queryParams) => {
      this.applicantStatusID = queryParams['appid'];
      this.joiningService.getOnboardingDetails(this.applicantStatusID).subscribe(
        response =>
        {
          if(response.messageCode.code === 'EC200')
          {
            this.expectedJoiningDate = response.responseData.expectedJoiningDate;
            this.actualJoiningDate = response.responseData.actualJoiningDate;
            this.employeeCode = response.responseData.employeeCode;

          }
  
        }
      );

    })
   }

  ngOnInit() {

   
   
  }

}
