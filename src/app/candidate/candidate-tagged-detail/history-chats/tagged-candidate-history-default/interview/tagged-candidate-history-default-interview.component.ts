import { Component, OnInit, Input } from '@angular/core';
import { InterviewService } from "app/candidate/candidate-tagged-detail/hiring-process/interview/interview.service";


@Component({
  selector: 'alt-tagged-candidate-history-default-interview',
  templateUrl: './tagged-candidate-history-default-interview.component.html',
  styleUrls: ['./tagged-candidate-history-default-interview.component.css'],
  providers: [InterviewService]
})
export class TaggedCandidateHistoryDefaultInterviewComponent implements OnInit {

  @Input() data: any;

  startDateResponse: Date;
  endDate: Date;
  startDateDay: any;

  venue: any;
  weekDays: String[] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  interviewer: String[];

  constructor(
    private interviewService: InterviewService
  ) { }

  ngOnInit() {
    if (this.data.nextStage.sysWorkflowstageActionType == "SCHEDULED-DONE" || this.data.nextStage.sysWorkflowstageActionType == "RE-SCHEDULED") {
      this.interviewService.getContentTypeMaster("INTERVIEW_TYPE").subscribe(out => {
        this.interviewService.populateInterview(this.data.applicantStatusHistoryID).subscribe(
          (response) => {
            if (response.response.interviewStartTime != null) {
              this.startDateResponse = new Date(response.response.interviewStartTime);
            }
            if (this.startDateResponse != null && this.startDateResponse != undefined) {
              this.startDateDay = this.weekDays[this.startDateResponse.getDay()];
            }

            this.venue = response.response.venueName;
            this.interviewer = response.response.interviewerName;

          }
        );

      });
    }

  }
}
