import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FitmentService } from "app/candidate/candidate-tagged-detail/hiring-process/fitment/fitment.service";
import { ActivatedRoute } from '@angular/router';
import { MessageCode } from "app/candidate/candidate-list/taggedCandidates/email-candidates/email-candidates.component";
import { FormControl } from '@angular/forms';
import { HrProfileOtherTO } from "app/candidate/candidate-tagged-detail/hiring-process/fitment/fitment.component";

@Component({
  selector: 'alt-tagged-candidate-history-default-salary',
  templateUrl: './tagged-candidate-history-default-salary.component.html',
  styleUrls: ['./tagged-candidate-history-default-salary.component.css'],
  providers: [FitmentService]
})
export class TaggedCandidateHistoryDefaultSalaryComponent implements OnInit {
  @Input() data: any;
  @Output() outSalaryFitmentStack: EventEmitter<any> = new EventEmitter();
  @Output() outSalarySecurity: EventEmitter<any> = new EventEmitter();
  @Output() outOtherExpensesList: EventEmitter<any> = new EventEmitter();
  @Output() outStackList: EventEmitter<any> = new EventEmitter();
  @Output() paystructureEmit: EventEmitter<any> = new EventEmitter();

  payStructureName: String;

  salaryFitmentStackResponse: any;
  applicantStatusID: any;
  guaranteedCTC: String;
  nonGuaranteedCTC: String;
  totalAmtYr: String;
  totalAmtMon: String;
  totalAmtMonthWithoutHidden: String;
  totalCTCAnnotate: String;
  costToCompanyStackVisible: boolean = false;
  otherStackVisible: boolean = false;
  variableSectionVisible: boolean = false;
  showloader: boolean = false;
  otherPayHeadSummary: HrProfileOtherTO[];
  salarySecurity: any

  salaryEditType = new FormControl();
  currency = new FormControl();
  paystructure = new FormControl();
  maxSalary = new FormControl();
  effectiveDate = new FormControl();

  applicantPaystructureDetailsList: any;
  otherExpensesList: any = [];
  stackList:any = [];

  constructor(private fitmentService: FitmentService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe((queryParams) => {
      this.applicantStatusID = queryParams['appid'];
    })

    if (this.data.nextStage.sysWorkflowstageActionType == "CREATE" || this.data.nextStage.sysWorkflowstageActionType == "CREATED") {
      this.initFormSecurity();
    }

    if (this.data.nextStage.sysWorkflowstageActionType == "CREATE" || this.data.nextStage.sysWorkflowstageActionType == "CREATED") {
      this.getSalaryFitmentStackCustom();
    }

  }

  getSalaryFitmentStackCustom() {
    
    this.fitmentService.getApplicantPaystructureDetailsByHistory(this.applicantStatusID, this.data.applicantStatusHistoryID).subscribe(out => {
      this.applicantPaystructureDetailsList = out.response.applicantPaystructureDetailsTOs;
      let paystructureValue = out.response.payStructure;
      if (this.applicantPaystructureDetailsList != null) {
        for (let comp of this.applicantPaystructureDetailsList) {
          if (comp.paystructureType == 'Offered_Stack') {
            this.stackList.push(comp);
          } else if (comp.paystructureType == 'Offered_Stack_Group') {
            this.stackList.push(comp);
          } else if (comp.paystructureType == 'Offered_Stack_Total') {
            this.stackList.push(comp);
          } else if (comp.paystructureType == 'Other_Expenses') {
            this.otherExpensesList.push(comp);
          }
        }
        this.outStackList.emit(this.stackList);
        console.log(paystructureValue)
        this.payStructureName = paystructureValue;
        this.paystructureEmit.emit(paystructureValue);
        this.outOtherExpensesList.emit(this.otherExpensesList);
      }
    });
  }

  getSalaryFitmentStack() {
    this.showloader = true;
    this.fitmentService.getSalaryFitmentStack(this.applicantStatusID).subscribe(response => {
      this.salaryFitmentStackResponse = undefined;
      this.guaranteedCTC = undefined;
      this.nonGuaranteedCTC = undefined;
      this.totalAmtMon = undefined;
      this.totalAmtYr = undefined;
      this.totalAmtMonthWithoutHidden = undefined;

      let messageTO: MessageCode = response.messageCode;
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.salaryFitmentStackResponse = response.response.groupList;
          this.guaranteedCTC = response.response.guaranteedCTC;
          this.nonGuaranteedCTC = response.response.nonGuaranteedCTC;
          this.totalAmtMon = response.response.totalAmtMon;
          this.totalAmtYr = response.response.totalAmtYr;
          this.totalAmtMonthWithoutHidden = response.response.totalAmtMonthWithoutHidden;
          this.totalCTCAnnotate = response.response.totalCTCAnnotate;
          this.costToCompanyStackVisible = true;
          this.variableSectionVisible = true;

          this.salaryEditType.setValue(response.response.salaryEditType);
          this.currency.setValue(response.response.currency);
          this.paystructure.setValue(response.response.payStructure);
          this.maxSalary.setValue(response.response.maxSalary);
          this.effectiveDate.setValue(new Date(response.response.effectiveDate));

          this.maxSalary.disable();
          this.salaryEditType.disable();
          this.outSalaryFitmentStack.emit(this.salaryFitmentStackResponse);
        } else if (messageTO.message != null) {
          // this.notificationService.setMessage(NotificationSeverity.WARNING, messageTO.message.toString());
        } else {
          // this.notificationService.setMessage(NotificationSeverity.WARNING, "An error occured. Please try again later...");
        }
      }
      this.showloader = false;
    }, error => {
      this.showloader = false;
      // this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not connect to the server!!! Please try again later...");
    }, () => {
      this.showloader = false
    });

  }

  getOtherStack() {
    this.showloader = true;
    this.fitmentService.getOtherStack(this.applicantStatusID).subscribe(response => {
      let messageTO: MessageCode = response.messageCode;
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.otherPayHeadSummary = response.responseData;
          this.otherStackVisible = true;
        } else if (messageTO.message != null) {
          // this.notificationService.setMessage(NotificationSeverity.WARNING, messageTO.message.toString());
        } else {
          // this.notificationService.setMessage(NotificationSeverity.WARNING, "An error occured. Please try again later...");
        }
      }
      this.showloader = false;
    }, error => {
      this.showloader = false;
      // this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not connect to the server!!! Please try again later...");
    }, () => {
      this.showloader = false;
    });
  }


  initFormSecurity() {
    this.fitmentService.getOfferGenerationSecurity().subscribe(
      response => {
        if (response.messageCode.code === 'EC200') {
          this.salarySecurity = JSON.parse(JSON.stringify(response.responseData[0]));
          this.outSalarySecurity.emit(this.salarySecurity);
        }
      });
  }
}
