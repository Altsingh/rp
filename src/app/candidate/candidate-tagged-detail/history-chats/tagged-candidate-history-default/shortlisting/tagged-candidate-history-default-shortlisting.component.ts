import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'alt-tagged-candidate-history-default-shortlisting',
  templateUrl: './tagged-candidate-history-default-shortlisting.component.html',
  styleUrls: ['./tagged-candidate-history-default-shortlisting.component.css']
})
export class TaggedCandidateHistoryDefaultShortlistingComponent implements OnInit {

  @Input() data:any;

  constructor() { }

  ngOnInit() {
  }

}
