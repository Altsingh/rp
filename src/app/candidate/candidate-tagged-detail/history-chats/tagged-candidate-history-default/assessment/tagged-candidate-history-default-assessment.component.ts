import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssessmentService } from '../../../hiring-process/assessment/assessment.service';

@Component({
  selector: 'alt-tagged-candidate-history-default-assessment',
  templateUrl: './tagged-candidate-history-default-assessment.component.html',
  styleUrls: ['./tagged-candidate-history-default-assessment.component.css'],
  providers: [AssessmentService]
})
export class TaggedCandidateHistoryDefaultAssessmentComponent implements OnInit {
  @Input() data: any;
  applicantStatusID: number;
  applicantStatusHistoryID: number;
  filePath: any;

  constructor(
    private route: ActivatedRoute,
    private assessmentService: AssessmentService
  ) {
    this.route.queryParams.subscribe((queryParams) => {
      this.applicantStatusID = queryParams['appid'];

    })
  }

  ngOnInit() {
    this.applicantStatusHistoryID = this.data.applicantStatusHistoryID;
    this.assessmentService.getAssessmentReport(this.applicantStatusID, this.applicantStatusHistoryID).subscribe(response => {
      if(response.response != null){
        this.filePath = response.response.reportLink;
      }
    })
  }
}
