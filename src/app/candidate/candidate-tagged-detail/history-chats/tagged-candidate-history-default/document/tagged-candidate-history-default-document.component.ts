import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alt-tagged-candidate-history-default-document',
  templateUrl: './tagged-candidate-history-default-document.component.html',
  styleUrls: ['./tagged-candidate-history-default-document.component.css']
})
export class TaggedCandidateHistoryDefaultDocumentComponent implements OnInit {
  @Input() data:any;  
  

  constructor() { }

  ngOnInit() {
  }

}
