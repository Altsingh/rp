import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { NameinitialService } from '../../../../shared/nameinitial.service'

@Component({
  selector: 'alt-tagged-candidate-history-default',
  templateUrl: './tagged-candidate-history-default.component.html',
  styleUrls: ['./tagged-candidate-history-default.component.css']
})
export class TaggedCandidateHistoryDefaultComponent implements OnInit {

  @Input() inputData: any;

  outSalaryFitmentStack: any;
  salarySecurity: any;

  toggleOpen: any;
  stackList: any;
  otherExpensesList: any;

  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }

  randomcolor: string[] = [];

  constructor(private nameinitialService: NameinitialService) {
    this.randomcolor = nameinitialService.randomcolor;
  }

  ngOnInit() { }

  toggleDiv() {
    this.toggleOpen = !this.toggleOpen;
  }

  setOutSalaryFitmentStack(event) {
    this.outSalaryFitmentStack = event;
  }

  setSalarySecurity(event) {
    this.salarySecurity = event;
  }

  getUserName(userName) {

    let result = '';

    if (userName != null) {
      const split = userName.split(' ');
      if (split.length > 0) {
        result = split[0].charAt(0);
      }
      if (split.length > 1) {
        result += split[1].charAt(0);
      }

    }
    return result;
  }

  getInstalmentMonth(month) {
    if(month == 0) {
      return 'January'
    } else if(month == 1) {
      return 'February'
    } else if(month == 2) {
      return 'March'
    } else if(month == 3) {
      return 'April'
    } else if(month == 4) {
      return 'May'
    } else if(month == 5) {
      return 'June'
    } else if(month == 6) {
      return 'July'
    } else if(month == 7) {
      return 'August'
    } else if(month == 8) {
      return 'September'
    } else if(month == 9) {
      return 'October'
    } else if(month == 10) {
      return 'November'
    } else if(month == 11) {
      return 'December'
    }
  }

}
