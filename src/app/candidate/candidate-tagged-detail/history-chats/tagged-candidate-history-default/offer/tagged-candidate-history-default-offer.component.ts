import { Component, OnInit, Input } from '@angular/core';
import { OfferService } from "app/candidate/candidate-tagged-detail/hiring-process/offer-generation/offer.service";
import { MessageCode } from "app/common/message-code";
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'alt-tagged-candidate-history-default-offer',
  templateUrl: './tagged-candidate-history-default-offer.component.html',
  styleUrls: ['./tagged-candidate-history-default-offer.component.css'],
  providers: [OfferService]
})
export class TaggedCandidateHistoryDefaultOfferComponent implements OnInit {
  @Input() data: any;
  offerLetterDownloadPath: String;
  offerLetterTemplate: String;
  applicantStatusID: number;

  constructor(private offerService: OfferService,
    private route: ActivatedRoute) {
    this.route.queryParams.subscribe((queryParams) => {
      this.applicantStatusID = queryParams['appid'];
    })
  }

  ngOnInit() {
    if (this.data.nextStage.sysWorkflowstageActionType == "OFFERMADE" || this.data.nextStage.sysWorkflowstageActionType == "OFFERREGENERATE") {
      this.offerService.getOfferLetterDocumentByHistory(this.applicantStatusID, this.data.applicantStatusHistoryID).subscribe(response => {
        let messageTO: MessageCode = response.messageCode;
        if (messageTO != null && messageTO.code === 'EC200') {
          this.offerLetterDownloadPath = response.response;
        }
      });

      this.offerService.getOfferLetterTemplate(this.data.applicantStatusHistoryID).subscribe(response => {
        let messageTO: MessageCode = response.messageCode;
        if (messageTO != null && messageTO.code === 'EC200') {
          this.offerLetterTemplate = response.response;
        }
      });
    }



  }

}
