import { Component, OnInit, Input } from '@angular/core';
import { HistoryChatsService } from '../../history-chats.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-loi',
  templateUrl: './loi.component.html',
  styleUrls: ['./loi.component.css'],
  providers: [HistoryChatsService]
})
export class LoiComponent implements OnInit {

  @Input() data: any;
  loader = false;
  responseData: any;

  constructor(private historyChatsService: HistoryChatsService,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }
}
