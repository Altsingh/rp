import { Component, OnInit, Input } from '@angular/core';
import { HistoryChatsService } from '../../history-chats.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-compensation-and-benifit',
  templateUrl: './compensation-and-benifit.component.html',
  styleUrls: ['./compensation-and-benifit.component.css'],
  providers: [HistoryChatsService]
})
export class CompensationAndBenifitComponent implements OnInit {
  @Input() data: any;
  loader: boolean = false;
  responseData: any;
  constructor(private historyChatsService: HistoryChatsService,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.compensationAndBenefitData(this.data.applicantStatusHistoryID);
  }

  compensationAndBenefitData(applicantStatusHistoryID: number) {
    this.loader = true;
    this.historyChatsService.getCompensationAndBenefitHistory(applicantStatusHistoryID).subscribe(response => {
      this.responseData = response.response;
      this.loader = false;
    });
  }

}
