import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'alt-tagged-candidate-history-default-screening',
  templateUrl: './tagged-candidate-history-default-screening.component.html',
  styleUrls: ['./tagged-candidate-history-default-screening.component.css']
})
export class TaggedCandidateHistoryDefaultScreeningComponent implements OnInit {

  @Input() data: any;
  message:String;

  constructor() { }

  ngOnInit() {
  }

}
