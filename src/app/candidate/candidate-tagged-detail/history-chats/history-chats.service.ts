import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../session.service';
import { ApplicantStatusRequest } from '../../../common/applicant-status-request';

@Injectable()
export class HistoryChatsService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getHiringProcessHistory(applicantStatusID:Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/hiringProcess/history/' + applicantStatusID
      +'/').map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getCandidateHistory(candidateCode:String) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/candidateHistory/' + this.sessionService.organizationID + '?candidateCode=' + candidateCode)
    .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getCompensationAndBenefitHistory(applicantStatusHistoryID:Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/hiringProcess/compensationandbenefit/' + applicantStatusHistoryID
      +'/').map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  fetchModifiedValues(candidateActionHistoryID:any) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/fetchCandidateModifiedValues/' + this.sessionService.organizationID + '?candidateActionHistoryID=' + candidateActionHistoryID)
    .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }
}