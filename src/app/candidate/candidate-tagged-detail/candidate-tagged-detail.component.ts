import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SessionService } from '../../session.service';



@Component({
  selector: '.alt-candidate-tagged-detail',
  templateUrl: './candidate-tagged-detail.component.html'
})
export class CandidateTaggedDetailComponent implements OnInit, OnDestroy {
  currentPath = '';
  applicantStatusID: number;
  jobTitle: string;
  jobCode: string;
  isBPenabled : any;

  overviewActive = false;

  subscription: Subscription;
  featureDataMap:Map<any,any>;


  constructor(private router: Router,
    private route: ActivatedRoute,
     private location: Location,private sessionService:SessionService) {
      this.featureDataMap = this.sessionService.featureDataMap;
      this.isBPenabled = this.sessionService.isBPEnabled;
  }

  ngOnInit() {

     if (this.location.path().indexOf("candidate-tagged-overview") > 0) {

      this.currentPath = '/candidate-tagged-overview';
       }
       else if(this.location.path().indexOf("hiring-process") > 0)
       {
         this.currentPath = '/hiring-process';
       }
       else if(this.location.path().indexOf("history-chats") > 0)
       {
         this.currentPath='/history-chats';
       }
       else if(this.location.path().indexOf("candidate-history") > 0)
       {
         this.currentPath='/candidate-history';
       }

    this.route.params.subscribe((params) => {
      this.applicantStatusID = this.route.snapshot.queryParams['appid'];
      this.jobTitle = this.route.snapshot.queryParams['jobTitle'];
      this.jobCode = this.route.snapshot.queryParams['jobCode'];
    });

    this.subscription = this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.currentPath = this.router.url;
          this.applicantStatusID = this.route.snapshot.queryParams['appid'];
          this.jobTitle = this.route.snapshot.queryParams['jobTitle'];
          this.jobCode = this.route.snapshot.queryParams['jobCode'];        
        }
      })
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
  }
}
