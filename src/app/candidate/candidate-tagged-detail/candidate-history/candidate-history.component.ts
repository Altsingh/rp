import { Component, OnInit, Input } from '@angular/core';
import { NameinitialService } from '../../../shared/nameinitial.service';
import { ActivatedRoute } from '@angular/router';
import { ResponseData } from '../../candidate-detail/candidate-overview/skill-set/skill-set.component';
import { CandidateHistoryResponseData } from '../history-chats/history-chats.component';
import { HistoryChatsService } from '../history-chats/history-chats.service';

@Component({
  selector: 'alt-candidate-history',
  templateUrl: './candidate-history.component.html',
  styleUrls: ['./candidate-history.component.css'],
  providers: [HistoryChatsService]
})
export class CandidateHistoryComponent implements OnInit {
  @Input() inputData: any;
  toggleOpen: boolean[] = [];
  randomcolor: string[] = [];
  applicantStatusID: number;
  Candidatecode:String;
  responseData: ResponseData;
  modifiedValuesLoaded:boolean = false;  
  candidateFieldsHistory: CandidateFieldsHistory;
  isCandidateFieldsHistoryArray: boolean = false;
  candidateHistoryResponseData:CandidateHistoryResponseData[];
  loader:boolean = false;
  candidateHistoryloader:boolean = false;

  constructor(private nameinitialService: NameinitialService,private route: ActivatedRoute,private historyChatsService: HistoryChatsService) {
  this.randomcolor = nameinitialService.randomcolor;

  this.route.parent.params.subscribe((params) => {
    this.Candidatecode = params['id'];    
    this.loadCandidateHistoryData(this.Candidatecode);
  }); 
   }
   

  ngOnInit() {
  }

  loadCandidateHistoryData(Candidatecode: String) {
    this.loader=true;
    this.historyChatsService.getCandidateHistory(Candidatecode).subscribe(response => {
      if (response) {
        if (response.messageCode) {
          if (response.messageCode.code) {
            if (response.messageCode.code == "EC200") {
              this.candidateHistoryResponseData=response.responseData;      
              this.initToggle(this.candidateHistoryResponseData.length, this.candidateHistoryResponseData[0].candidateActionHistoryID);
              if(this.candidateHistoryResponseData != null && this.candidateHistoryResponseData.length > 0 && this.candidateHistoryResponseData[0].eventName == 'CandidateModified')
              {
                this.expandToggle(this.candidateHistoryResponseData.length, this.candidateHistoryResponseData[0].candidateActionHistoryID);
              }
            }
          }
        }
      }
      this.loader = false;
    });
  }

  getUserName(userName) {
    let result = '';
    if(userName == null)
      return result;
    userName = userName.replace(/\s\s+/g, ' ');
     if (userName != null) {
       const split = userName.split(' ');
       if (split.length > 0) {
         result = split[0].charAt(0);
       }
       if (split.length > 1) {
         result = result + split[1].charAt(0);
       }
     }
     return result;
   }

   initToggle(length,candidateActionHistoryID){
    this.toggleOpen = [];
    for( let v = 0; v < length; v++ ){
      this.toggleOpen[v] = false;
    } 
       
   }

   expandToggle(length,candidateActionHistoryID){
    if(length>0){
      this.toggleOpen[0] = true;
      this.fetchModifiedValues(candidateActionHistoryID);
    }
   }
 
   
   toggle(i : number) {
     for( let v = 0; v < this.toggleOpen.length; v++ ){
      if( v == i ){
        this.toggleOpen[i] = !this.toggleOpen[i];
      }else{
        this.toggleOpen[v] = false;
      }
     }     
   }

   fetchModifiedValues(candidateActionHistoryID: any)
   {     
      this.candidateHistoryloader = true;
      this.modifiedValuesLoaded = false;
      this.historyChatsService.fetchModifiedValues(candidateActionHistoryID).subscribe(response => {
        if (response) {
          if (response.messageCode) {
            if (response.messageCode.code) {
              if (response.messageCode.code == "EC200") {
                this.candidateFieldsHistory = JSON.parse(response.responseData);                                
                this.modifiedValuesLoaded = true;

                if(this.candidateFieldsHistory[Object.keys(this.candidateFieldsHistory)[0]] instanceof Array) {
                  this.isCandidateFieldsHistoryArray = true;
                } 
                else
                {
                  this.isCandidateFieldsHistoryArray = false;
                }
                this.candidateHistoryloader = false;
              } 
              else
              {
                this.candidateHistoryloader = false;
              }             
            }
          }
        }        
      });
    }

    getKeys(value) {
      return Object.keys(value);
    }

    isArray(value) {
      return value instanceof Array
    }
}
export class CandidateFieldsHistory
{
 
}

