import { CommonService } from 'app/common/common.service';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { PersonalDetailsService } from '../../candidate-detail/candidate-overview/personal-details/personal-details.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NameinitialService } from '../../../shared/nameinitial.service';
import { SessionService } from '../../../session.service';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { EmailTemplateComponent } from "../../email-template/email-template.component";
import { DialogService } from '../../../shared/dialog.service';
import { JobDetailService } from '../../../job/job-detail/job-detail.service';
import { environment } from 'environments/environment';
import { FormatJobcodeParamPipe } from 'app/shared/format-jobcode-param.pipe';
import { PrintingService, PRINTINGSTATE } from 'app/shared/services/printing/printing.service';
import { Subscription } from 'rxjs';
import { I18UtilService } from 'app/shared/services/i18-util.service';
@Component({
  selector: 'alt-candidate-tagged-profile',
  templateUrl: './candidate-tagged-profile.component.html',
  providers: [PersonalDetailsService, JobDetailService]
})
export class CandidateTaggedProfileComponent implements OnInit, OnDestroy {
  Candidatecode: string;
  applicantStatusId: string;
  data: any;
  jobTaggedData: JobTaggedData[];
  taggedJobsLength: number = 0;
  messageCode: MessageCode;
  subscription;
  randomcolor: string;
  jobStatus: string;
  jobCode: String;
  jobTitle: String;
  contextMenuOpen: boolean = false;
  taggedJobsMenuOpen: boolean = false;
  inReview: boolean = false;
  jobSummaryData: any;
  profileMatch: any = 0;
  automatchEnabled: boolean;
  formatJobcodeParamPipe: FormatJobcodeParamPipe = new FormatJobcodeParamPipe();
  featureDataMap: Map<any, any>;
  isBPenabled : Boolean = false;

  constructor(private PersonalDetailsService: PersonalDetailsService, private route: ActivatedRoute,
    private nameinitialService: NameinitialService, private notificationService: NotificationService,
    private commonService: CommonService, private sessionService: SessionService, private dialogsService: DialogService,
    private jobDetailService: JobDetailService, private router: Router, private i18UtilService: I18UtilService
  ) {
    this.automatchEnabled = this.sessionService.isAutoMatchEnabled();
    this.data = new ResponseData();
    this.messageCode = new MessageCode();
    this.randomcolor = nameinitialService.randomcolor[0];
    this.jobCode = this.route.snapshot.queryParams['jobCode'];
    this.jobTitle = this.route.snapshot.queryParams['jobTitle'];
    this.featureDataMap = this.sessionService.featureDataMap;
    this.isBPenabled = this.sessionService.isBPEnabled;
  }

  socialBlock(data) {
    return (data.linkedinProfile != null && data.linkedinProfile.trim() != '') ||
      (data.twitterProfile != null && data.twitterProfile.trim() != '') ||
      (data.facebookProfile != null && data.facebookProfile.trim() != '') ||
      (data.googleplusProfile != null && data.googleplusProfile.trim() != '');
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED') {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    }
  }

  hideContextMenu() {
    this.contextMenuOpen = false;
  }
  showContextMenu() {
    this.contextMenuOpen = true;
  }

  hideTaggedJobsMenu() {
    this.taggedJobsMenuOpen = false;
  }
  showTaggedJobsMenu() {
    if(!this.isBPenabled){
    this.taggedJobsMenuOpen = true;
    }
  }

  ngOnInit() {
    this.commonService.showRHS();

    this.route.params.subscribe((params) => {

      this.Candidatecode = this.route.snapshot.params['id'];
      this.applicantStatusId = this.route.snapshot.queryParams['appid'];

      this.PersonalDetailsService.getProfileMatchPercenatage(this.applicantStatusId).subscribe((res) => {
        if (res && res.response) {
          this.profileMatch = res.response.profileMatch;
        }
      });

      this.PersonalDetailsService.getTaggedJobsList(this.Candidatecode).subscribe((res) => {
        if (res.responseData.length >= 1)
          this.jobTaggedData = res.responseData;

        this.taggedJobsLength = res.responseData.length;

        for (let item of this.jobTaggedData) {
          if (item.applicantStatusID == this.applicantStatusId) {
            this.goToHiringProcess(item);
          }
        }
      });

      this.subscription = this.PersonalDetailsService.getpersonaldetailsAndIJPDocs(this.Candidatecode,this.applicantStatusId).subscribe(out => {
        this.messageCode = out.messageCode;
          this.data = out.responseData[0]
      });


      let jobCode = this.route.snapshot.queryParams['jobCode'];

      this.jobDetailService.getJobSummary(jobCode).subscribe((response) => {
        this.jobSummaryData = response;
        this.jobStatus = this.jobSummaryData.jobStatus;

      });

    });

  }
  imageError(event, candidate) {
    candidate.profileImage = null;
  }
  isEmpty() {
    if (this.data.subSourceUsername != null && this.data.subSourceUsername != '') {
      return false;
    }
    else {
      return true;
    }
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  getCPCredentials(applicantStatusId) {
    this.hideContextMenu();
    this.commonService.getCPCredentials(applicantStatusId).subscribe((response) => {
      if (response.messageCode != null && response.messageCode.code === 'EC200') {
        this.sessionService.object = {
          applicantStatusId: applicantStatusId,
          cpFilter: 'cpcredentials',
          responseData: response,
        };
        this.dialogsService.open(EmailTemplateComponent, { width: '935px' });
      } else {
        this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
          this.notificationService.setMessage("WARNING", res);
        });
      }
    });
  }

  goToHiringProcess(item) {
    this.applicantStatusId = item.applicantStatusID;
    this.jobCode = item.requisitionCode;
    this.jobTitle = item.jobTitle;
    this.jobStatus = item.reqStatus;
    this.subscription = this.PersonalDetailsService.getpersonaldetailsAndIJPDocs(this.Candidatecode,this.applicantStatusId).subscribe(out => {
      this.messageCode = out.messageCode;
        this.data = out.responseData[0]
    });
  }

  gotoJobDetail() {
    if(!this.isBPenabled){
    this.router.navigateByUrl('/job/edit-job/' + this.formatJobcodeParamPipe.transform(this.jobCode) + '/job-details');
    }
  }

  
}
export class ResponseData {
  firstName: string;
  middleName: string;
  lastName: string;
  city: string;
  sourceType: string;
  subSourceUsername: string;
  profileImage: string;
  title: string;
  resumePath: string;
  facebookProfile: string;
  linkedinProfile: string;
  googleplusProfile: string;
  twitterProfile: string;
  sysSourceType: string;

}

export class JobTaggedData {
  applicantStatusID: string;
  jobTitle: string;
  requisitionCode: string;
  reqStatus: string;
}

export class MessageCode { }