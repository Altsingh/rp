import { Component, OnInit, Input } from '@angular/core';
import { CandidateService } from '../../../candidate.service';
import { ActivatedRoute } from '@angular/router';
import { NewCandidateService } from '../../../new-candidate/new-candidate.service';
@Component({
  selector: 'alt-candidate-documents',
  templateUrl: './candidate-documents.component.html',
  styleUrls: ['./candidate-documents.component.css'],
  providers: [NewCandidateService]
})
export class CandidateDocumentsComponent implements OnInit {

  candidateDocuments1: any[];
  candidateDocuments2: any[];
  documentListSubscription: any;
  @Input()
  security: any;


  constructor(
    private candidateService: CandidateService,
    private newCandidateService: NewCandidateService,
    private route: ActivatedRoute) {
    this.candidateDocuments1 = [];
    this.candidateDocuments2 = [];
  }

  ngOnInit() {
    let candidateCode = this.route.snapshot.parent.params['id'];

    this.newCandidateService.fetchCandidateId(candidateCode, false).subscribe(response => {
      let loaded: boolean = false;

      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {

            let candidateId = response.responseData[0];

            this.candidateService.getDocumentList(candidateId, false).subscribe(response => {

              if (response) {
                if (response.responseData) {
                  if (response.responseData.length > 0) {

                    for (let x = 0; x < response.responseData.length; x++) {
                      if (x % 2 == 0) {
                        this.candidateDocuments1.push(response.responseData[x]);
                      } else {
                        this.candidateDocuments2.push(response.responseData[x]);
                      }

                    }

                  }
                }
              }
            });
          }
        }
      }


    });

  }

  isEmptyDocuments(): boolean {
    if ((this.candidateDocuments1 == null || this.candidateDocuments1.length === 0)
      && (this.candidateDocuments2 == null || this.candidateDocuments2.length === 0)) {
      return true;
    } else {
      for (let doc of this.candidateDocuments1) {
        if (doc.uploaded === true) {
          return false;
        }
      }

      for (let doc of this.candidateDocuments2) {
        if (doc.uploaded === true) {
          return false;
        }
      }
    }

    return true;
  }

  downloadCandidateDocuments() {
    let arr: Array<String> = [];
    for(let doc of this.candidateDocuments1) {
      if(doc.selected == true) {
        arr.push(doc.fileName)
      }
    }
    for(let doc of this.candidateDocuments2) {
      if(doc.selected == true) {
        arr.push(doc.fileName)
      }
    }
    if(arr.length > 1) {
      this.newCandidateService.downloadCandidateDocuments(arr).subscribe(data => {
        window.open(data.response);
      });
    } else {
      window.open(arr[0].toString());
    }
  }

  selectAllCandidateDocuments() {
    for(let doc of this.candidateDocuments1) {
      if(doc.uploaded) {
        doc.selected = true;
      }
    }
    for(let doc of this.candidateDocuments2) {
      if(doc.uploaded) {
        doc.selected = true;
      } 
    }
  }

  isDocumentSelected() {
    for(let doc of this.candidateDocuments1) {
      if(doc.selected == true) {
        return false;
      }
    }
    for(let doc of this.candidateDocuments2) {
      if(doc.selected == true) {
        return false;
      }
    }
    return true;
  }

}
