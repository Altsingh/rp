import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { CandidateService } from '../../candidate.service';
import { FormatCapitalizePipe } from '../../../shared/format-capitalize.pipe';
import { PrintingService, PRINTINGSTATE } from 'app/shared/services/printing/printing.service';
import { isNullOrUndefined } from 'util';
import { SessionService } from '../../../session.service';

@Component({
  selector: 'alt-candidate-tagged-overview',
  templateUrl: './candidate-tagged-overview.component.html'
})
export class CandidateTaggedOverviewComponent implements OnInit,OnDestroy {

  security: any;
  formatCapitalizePipe: FormatCapitalizePipe = new FormatCapitalizePipe();
  allSubscriptions: Subscription[] = [];
  printingServiceSubscription: Subscription;
  isBPenabled : any = false;

  constructor(private candidateService: CandidateService,private printingService: PrintingService, 
    private sessionService : SessionService) {
     this.security = {};
    this.isBPenabled =  this.sessionService.isBPEnabled;
     this.initFormSecurity();
   }

  ngOnInit() {
    this.subscribePrintingMode();
  }

  ngOnDestroy(){
    if(!isNullOrUndefined(this.printingServiceSubscription)){
      this.printingServiceSubscription.unsubscribe();
    }
  }

  private initFormSecurity() {
    this.candidateService.clearCandidateFormSecurity();
    let securitySubscription = this.candidateService.getCandidateFormSecurity().subscribe(
      response => {
        this.security = response.responseData[0];
        if (this.security != null) {
          this.security.basicSectionRendered = true;

          let allKeys = Object.keys(this.security);

          for (let x = 0; x < allKeys.length; x++) {
            if (this.security[allKeys[x]] != null && allKeys[x].toLowerCase().endsWith("label")) {
              if (allKeys[x] == 'companyCTCLabel'
                || allKeys[x] == 'panNumberLabel'
                || allKeys[x] == 'dateOfBirthLabel') {
                continue;
              }
              this.security[allKeys[x]] = this.formatCapitalizePipe.transform(this.security[allKeys[x]]);
            }
          }

        }
      }
    );
    this.allSubscriptions.push(securitySubscription);
  }

  setPrintingMode() {
    this.printingService.initiatePrinting();
  }

  subscribePrintingMode() {
    this.printingServiceSubscription = this.printingService.printingObservable$.subscribe(printingState => {
      if (printingState === PRINTINGSTATE.READY) {
        window.onafterprint = () => {
          setTimeout(() => { this.printingService.setPrintStateTOComplete() }, 0);;
        }
        window.print();
      }
    })
  }

}
