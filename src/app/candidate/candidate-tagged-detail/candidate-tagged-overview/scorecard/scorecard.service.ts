import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class ScorecardService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getApplicantScoreCardDetails(applicantStatusID: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getApplicantScoreCardDetails/' + applicantStatusID+"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }
}
