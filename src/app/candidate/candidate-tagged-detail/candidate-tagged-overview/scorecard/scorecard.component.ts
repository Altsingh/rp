import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScorecardService } from './scorecard.service'
import { SelectItem } from '../../../../shared/select-item'
import { AssessmentService } from '../../hiring-process/assessment/assessment.service';
import { PrintingService, PRINTINGSTATE } from 'app/shared/services/printing/printing.service';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-scorecard',
  templateUrl: './scorecard.component.html',
  providers: [ScorecardService,AssessmentService]
})
export class ScorecardComponent implements OnInit {

  applicantStatusID: number;
  scoreCardResponse: ScoreCardResponse[];
  ratings = [1,2,3,4,5];
  assessmentList : any;
  toggleOpen: boolean[] = [];
  printingServiceSubscription:Subscription;

  constructor(private route: ActivatedRoute,
    private scorecardService:ScorecardService,
    private assessmentService:AssessmentService,private printingService:PrintingService) { 
    this.applicantStatusID = this.route.snapshot.queryParams['appid'];
    this.initializeData(this.applicantStatusID);
  }

  ngOnInit() {
   this.subscribePrintingMode();
  }

  toggleDiv(i :number) {
    this.toggleOpen[i] = !this.toggleOpen[i];
  }

  initializeData(applicantStatusID:number) {
    this.scorecardService.getApplicantScoreCardDetails(applicantStatusID).subscribe(out => {
      this.scoreCardResponse = out.responseData;
    });
    this.getAssessmentDetails(this.applicantStatusID);
  }

  getAssessmentDetails(applicantStatusID)
  {
    this.assessmentService.getAssessmentDetails(applicantStatusID).subscribe(response =>{
      this.assessmentList = response.response;
    })
  }

  getEndDate(endDate){
    let date:Date;
    if(endDate != null){
      date = new Date(endDate)
    }
    return date;
  }


  getEndTime(endTime){
  let date:Date;
    if(endTime != null){
      date = new Date(endTime);
    }
    return endTime;
  }

  getRating(value:number, index:number) {
    if(value >= index) {
      return "star-rating";
    } else {
      return "";
    }
  }

  subscribePrintingMode(){
    this.printingServiceSubscription = this.printingService.printingObservable$.subscribe(printingState=>{
      if(printingState === PRINTINGSTATE.INTIALIZE){
        if(!isNullOrUndefined(this.scoreCardResponse)){
          for(let i =0;i<this.scoreCardResponse.length;i++){
            this.toggleOpen[i]=true;
            
          }
        }
        
        setTimeout(()=>{this.printingService.setPrintStateToReady()},0);
      }
      if(printingState === PRINTINGSTATE.COMPLETED){
        if(!isNullOrUndefined(this.scoreCardResponse)){
          for(let i =0;i<this.scoreCardResponse.length;i++){
            this.toggleOpen[i]=false;
            
          }
        }
      }
    });
  }

}

export class ScoreCardResponse {
  comment: String;
  stage: String;
  status: String;
  createDate: String;
  competency: SelectItem;
};
