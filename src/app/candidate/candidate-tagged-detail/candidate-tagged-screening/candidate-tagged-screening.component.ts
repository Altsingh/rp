import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { HiringProcessService } from '../hiring-process/hiring-process.service';
import { HiringProcessResponse } from '../../../common/hiring-process-response';
import { HiringControllerService } from '../../hiring-controller.service';
import { PersonalDetailsService } from '../../candidate-detail/candidate-overview/personal-details/personal-details.service';
import { CommonService } from 'app/common/common.service';
import { SessionService } from '../../../session.service';
import { EmailTemplateComponent } from "../../email-template/email-template.component";
import { DialogService } from '../../../shared/dialog.service';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';

@Component({
  selector: 'alt-candidate-tagged-screening',
  templateUrl: './candidate-tagged-screening.component.html',
  providers: [PersonalDetailsService]
})
export class CandidateTaggedScreeningComponent implements OnInit {

  @Input()
  applicantStatusID: number;
  responseData: ResponseData;
  hiringProcessList: HiringProcessResponse[];
  marginleft: String;
  counter: number = 0;
  count: number = 0;
  tot: number = 0;
  changeVal: string;
  totalMargin: number = 0;
  rightArrow: string = '';
  leftArrow: string = '';
  inReview: boolean = false;

  sendCPCredentials: boolean = false;
  reSendCPCredentials: boolean = false;

  isBPenabled : Boolean = false;

  constructor(private hiringProcessService: HiringProcessService,
    private PersonalDetailsService: PersonalDetailsService,
    private hiringControllerService: HiringControllerService,
    private commonService: CommonService,
    private sessionService: SessionService,
    private dialogsService: DialogService,
    private notificationService: NotificationService,
    private route: ActivatedRoute) {
    this.hiringControllerService.setCandidateTaggedScreeningComponent(this);
    this.isBPenabled = this.sessionService.isBPEnabled;
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.route.queryParams.subscribe((queryParams) => {
        this.applicantStatusID = queryParams['appid'];
        this.initHiringData();
      });
    });

    this.getCPflags(this.applicantStatusID);

    this.hiringProcessService.isWorkflowUpdated().subscribe(() => {
      this.getCPflags(this.applicantStatusID);
    });

  }
  initHiringData() {
    this.hiringProcessService.getHiringProcessData(this.applicantStatusID).subscribe(out => {
      this.responseData = out.responseData;
      this.hiringProcessList = this.responseData.hiringProcessResponse;
      if (window.matchMedia(this.rules.phone).matches) {
        if (this.hiringProcessList.length > 3) {
          this.rightArrow = "displayBlock";
        }
        this.tot = (this.hiringProcessList.length) - 2;
      }
      else if (window.matchMedia(this.rules.tablet).matches) {
        if (this.hiringProcessList.length > 5) {
          this.rightArrow = "displayBlock";
        }
        this.tot = (this.hiringProcessList.length) - 5;
      }
      else if (window.matchMedia(this.rules.tabletHorizontal).matches) {
        if (this.hiringProcessList.length > 7) {
          this.rightArrow = "displayBlock";
        }
        this.tot = (this.hiringProcessList.length) - 7;
      } else {
        if (this.hiringProcessList.length > 7) {
          this.rightArrow = "displayBlock";
        }
        this.tot = (this.hiringProcessList.length) - 7;
      }
    });
  }
  rules =
    {
      phone: '(max-width: 767px)',
      tablet: '(max-width: 1023px)',
      tabletHorizontal: '(max-width: 1279px)',
    }

  sliderMove(val) {
    if (this.tot > 0) {
      if (val == "L") {
        this.rightArrow = 'displayBlock';
        if (this.counter > 0) {
          this.counter = this.counter - 1;
          this.totalMargin = this.totalMargin + (110);
          this.marginleft = this.totalMargin + "px";
          if (this.counter == 0) {
            this.leftArrow = 'displayNone';
          }
        }
      } else if (val == "R") {
        this.leftArrow = 'displayBlock';
        if (this.counter < this.tot) {
          this.counter = this.counter + 1;
          this.marginleft = "-" + 110 * this.counter + "px";
          this.changeVal = "R";
          this.totalMargin = -110 * this.counter;
          if (this.tot - this.counter == 0) {
            this.rightArrow = 'displayNone';
          }
        }
      }
    }
  }

  getCPflags(applicantStatusId) {
    this.PersonalDetailsService.getCPflags(applicantStatusId).subscribe((data) => {
      if (data.messageCode != null && data.messageCode.code === 'EC200') {
        this.reSendCPCredentials = data.response.reSendCPCredentials;
        this.sendCPCredentials = data.response.sendCPCredentials;
      }
    });
  }


  getCPCredentials(applicantStatusId) {
    this.commonService.getCPCredentials(applicantStatusId).subscribe((response) => {
      if (response.messageCode != null && response.messageCode.code === 'EC200') {
        this.sessionService.object = {
          applicantStatusId: applicantStatusId,
          cpFilter: 'cpcredentials',
          responseData: response,
        };
        this.dialogsService.open(EmailTemplateComponent, { width: '935px' });
      } else {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "An error occured. Please try again later...");
      }
    });
  }

}

export class ResponseData {
  hiringProcessResponse: HiringProcessResponse[];
  requisitionId: number;
  workflowStageID: number;
}