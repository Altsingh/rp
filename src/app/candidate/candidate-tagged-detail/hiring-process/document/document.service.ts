import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Jsonp, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from '../../../../session.service';

@Injectable()
export class DocumentService{

    constructor(
        private http: Http,
    private sessionService: SessionService
    ){

    }

    fetchCandidateIdforApplicantId(applicantId: number)
    {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch/candidateId/' + applicantId+"/").
            map((res) => {
                this.sessionService.check401Response(res.json());
                return res.json()});
    }



    
}