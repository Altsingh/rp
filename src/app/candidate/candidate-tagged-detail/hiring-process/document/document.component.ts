import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { FormControl } from '@angular/forms';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { CandidateService } from '../../../../candidate/candidate.service';
import { NewCandidateService } from '../../../../candidate/new-candidate/new-candidate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentService } from '../../../../candidate/candidate-tagged-detail/hiring-process/document/document.service';
import { HiringProcessService } from '../hiring-process.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-hiring-process-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css'],
  providers: [NewCandidateService, CandidateService, DocumentService]
})
export class DocumentComponent implements OnInit {
  label: string ;
  document: boolean = false;
  @Input() statusLists: StageActionListMaster;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }
  @Input() set spinner(value: boolean) {
    this.document = false;
  }
  
  @Input() inputData: HiringProcessResponse;
  @Input() security: any;
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  toggleOpen: boolean;
  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  newActionType: string = '';
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;
  documentsList: any[];
  candidateId: number;
  applicantId: number;
  fileSize: number;
  jobStatus : boolean = true;
  //security:any;
  constructor(private notificationService: NotificationService,
    private candidateService: CandidateService,
    private route: ActivatedRoute,
    private newCandidateService: NewCandidateService,
    private documentService: DocumentService,
    private hiringProcessService: HiringProcessService,
    private i18UtilService: I18UtilService)
     {
    this.applicantId = this.route.snapshot.queryParams['appid'];
  }

  ngOnInit() {
    if (this.toggleOpen) {
      this.hiringProcessService.isJobStatus().subscribe( out => this.jobStatus = out );
      this.documentService.fetchCandidateIdforApplicantId(this.applicantId).subscribe(
        response => {
          this.candidateId = response.response;
          this.initDocumentList(this.candidateId);
        }
      );
    }
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.label=res;
    });
  }

  public initDocumentList(candidateId: number) {
    let documentListSubscription = this.candidateService.getDocumentList(this.candidateId, false).subscribe(
      response => {
        this.documentsList = response.responseData;
       
      }
    );
 
  }

  downloadDocument(index: number) {
  }

  upload(event, index: number) {

  }

  uploadDocument(event, index: number, documentTypeID:number) {
    this.setDocumentSectionDone();
    let documentName = "";

    if (this.documentsList.length >= index) {
      documentName = this.documentsList[index].documentName;
      this.documentsList[index].uploading = true;
    }
    if (event) {
      if (event.target) {
        let fileCount: number = event.target.files.length;
        if (fileCount > 0) {
          this.fileSize = event.target.files[0].size;
          let extenstion = event.target.files[0].name.split('.').pop();
          let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg','png','jpeg','bmp'];

          if (this.fileSize > 8388608) {
            this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
            this.documentsList[index].uploading = false;
          } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
            this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
            this.documentsList[index].uploading = false;
          }
          else {
            let formData = new FormData();
            formData.append('file', event.target.files.item(0));
            formData.append('documentName', documentName);

            let fileUploadError = "Could not upload Document. Please try again.";
            let subscription = this.newCandidateService.saveCandidateDocument(this.candidateId, documentTypeID, formData, false).subscribe(response => {
              let uploaded = false;

              if (response) {
                if (response.responseData) {
                  if (response.responseData.length > 0) {
                    let doc = response.responseData[0];
                    this.documentsList[index].documentId = doc.documentId;
                    this.documentsList[index].uploaded = doc.uploaded;
                    this.documentsList[index].documentName = doc.documentName;
                    this.documentsList[index].fileName = doc.fileName;
                    uploaded = true;

                  }
                }
                if(response.messageCode && response.messageCode.code && response.messageCode.code == "EC201"){
                  if( response.messageCode.message != null ){
                    fileUploadError = response.messageCode.message;
                  }
                }
              }

              if (!uploaded) {
                this.notificationService.setMessage(NotificationSeverity.WARNING, fileUploadError);
              } else {
                this.i18UtilService.get('candidate.success.docUploaded').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
              }
            },
              () => {
                //this.setDocumentSectionDone();
              },
              () => {
                this.documentsList[index].uploading = false;
                //this.setDocumentSectionDone();
              });


          }

        }

      }
    }
  }

  setDocumentSectionDone() {
    let uploading = false;
    let uploaded = true;

    for (let x = 0; x < this.documentsList.length; x++) {
      if (this.documentsList[x].uploading) {
        uploading = true;
        break;
      }
      if (this.documentsList[x].uploaded) {
        uploaded = false;
      }
    }

    // if (uploaded && !uploading) {
    //   this.documentSectionDone = true;
    //   this.candidateForm.controls.documentSection.markAsPristine();
    // }
    // if (uploading) {
    //   this.documentSectionBlock = true;
    //   this.candidateForm.controls.documentSection.markAsDirty();
    // }

  }


  deleteDocument(index: number) {
    // this.setDocumentSectionDone();

    // let candidateId = this.candidateForm.controls.candidateId.value;
    let documentId = 0;


    if (this.documentsList.length >= index) {
      documentId = this.documentsList[index].documentId;
      this.documentsList[index].uploading = true;
    }

    if (documentId != 0) {
      let subscription = this.newCandidateService.deleteCandidateDocument(this.candidateId, documentId, false).subscribe(response => {
        let deleted = false;
        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {
              deleted = true;
              this.documentsList[index].documentId = 0;
              this.documentsList[index].uploaded = false;
              this.documentsList[index].fileName = null;
            }
          }
        }

        if (!deleted) {
          this.i18UtilService.get('common.error.docDeleteError').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        } else {
          this.i18UtilService.get('common.success.docDeleted').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
        }
      },
        () => {

        },
        () => {
          this.documentsList[index].uploading = false;
          this.setDocumentSectionDone();
        });


    }




  }
  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    let valid: boolean = true;

    if(this.newActionType =='COMPLETED')
    {
      for(let document of this.documentsList)
      {
        if((document.mandatory) && (document.fileName == null))
        {
          this.i18UtilService.get('candidate.warning.uploadDocWarning', { 'documentName': document.documentName }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
        
      }
    }
    
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == "")) {
      this.i18UtilService.get('common.warning.enterLabelCommon', {'label':this.security.commentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && (this.updateStatusRequestTO.attachmentName == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    
    if (valid == false) {
      return;
    }
    this.document = true;
    this.output.emit(this.updateStatusRequestTO);
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar','jpg','png','jpeg','bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }
  statusChangeListener(event: any){
    this.newActionType = event.sysActionType;
    this.selectedStatusID.setValue(event.value);
    this.initSecurity.emit(event.value);
  }
}
