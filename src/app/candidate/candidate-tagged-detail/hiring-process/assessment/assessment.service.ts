import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class AssessmentService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getApplicantVenues() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_applicantVenue/' + this.sessionService.organizationID+"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getAssessmentFilePath(applicantID:number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/assessment/assessmentfilepath/' + applicantID + '/').map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getAssessmentDetails(applicantStatusID : number)
  {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/assessment/getAssessmentDetails/' + applicantStatusID + '/').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()});
  }

  getAssessmentReport(applicantStatusID : number,applicantStatusHistoryID : number)
  {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/assessment/assessmentReport/' + applicantStatusID + '/' + applicantStatusHistoryID ).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()});
  }

  public getAssessmentTypeList(assessmentPartner:any) {    
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/assessment/getAssessmentTypeList/' + assessmentPartner).
        map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()
        });
  }
}
