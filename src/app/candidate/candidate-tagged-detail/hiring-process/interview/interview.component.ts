import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { SessionService } from '../../../../session.service';
import { NameinitialService } from '../../../../shared/nameinitial.service';
import { SelectItem } from '../../../../shared/select-item';
import { HiringProcessService } from '../hiring-process.service';
import { InterviewService } from './interview.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-hiring-process-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.css'],
  providers: [InterviewService]
})
export class InterviewComponent implements OnInit {
  randomcolor: string[] = [];
  label: string;
  interview: boolean = false;
  newActionType: string = '';

  toggleOpen: boolean;
  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  interviewVenue: string;
  fr: FileReader;
  file: Blob;
  interviewForm: FormGroup;
  interviewTypeList: SelectItem[];
  venueList: SelectItem[];
  interviewPanelist: SelectItem[];
  selectedInterviewPanelist: SelectItem[];
  DDRange: SelectItem[];
  MMRange: SelectItem[];
  YYYYRange: SelectItem[];
  jobStatus: boolean = true;

  interviewStartDate: Date;
  interviewEndDate: Date;
  currentDate: Date;

  populateInterviewResponse: any;

  showAssessmentDoc: boolean;
  defaultSelectedInterviewPanelist: SelectItem[];

  @Input() statusLists: StageActionListMaster;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }
  @Input() set spinner(value: boolean) {
    this.interview = false;
  }

  @Input() inputData: HiringProcessResponse;
  @Input() set requisitionID(value: number) {
    if (this.toggleOpen) {
      this.interviewService.getRequisitionInterviewPanel(value).subscribe(
        out => {
          this.selectedInterviewPanelist = out.responseData;
          this.defaultSelectedInterviewPanelist = out.responseData;
        });
    }
  }
  @Input() security: any;
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();


  constructor(private sessionService: SessionService, private formBuilder: FormBuilder, private interviewService: InterviewService,
    private notificationService: NotificationService, private nameinitialService: NameinitialService,
     private hiringProcessService: HiringProcessService, private i18UtilService: I18UtilService
  ) { }

  initializeData() {

    this.interviewForm = this.formBuilder.group({
      startDate: new FormControl(),
      endDate: new FormControl(),
      interviewType: new FormControl(),
      venue: new FormControl(),
      calendarInvite: ''
    });
    this.interviewService.getContentTypeMaster("INTERVIEW_TYPE").subscribe(out => {
      this.interviewTypeList = out.responseData.interviewTypeList;
    });

    this.interviewService.getAllRequisitionInterviewPanel().subscribe(
      out => {
        this.interviewPanelist = out.responseData;
        this.interviewService.populateInterview(this.inputData.applicantStatusHistoryID).subscribe(
          (response) => {
            this.setPopulateInterviewResponse(response);
          }
        );
      });

    this.interviewService.getApplicantVenues().subscribe(out => this.venueList = out.responseData);
    this.DDRange = this.createRange(1, 31);
    this.MMRange = this.getMMRange();
    this.YYYYRange = this.createRange(2000, 20);

    if (this.inputData.sysWorkflowstageActionType == 'RE-SCHEDULE') {
      this.setInterviewResponse();
    }

  }

  setInterviewResponse() {
    this.setStartDate(null);
    this.setEndDate(null);
    this.interviewForm.controls.interviewType.setValue(null);
    this.interviewForm.controls.venue.setValue(null);
    this.comment.setValue(null);
    this.inputData.feedbackDocumentPath = null;
    this.selectedInterviewPanelist.push(null);

  }
  setPopulateInterviewResponse(response: any) {
    this.populateInterviewResponse = response;
    if (response.response.interviewStartTime != null) {
      this.setStartDate(new Date(response.response.interviewStartTime));
    }
    if (response.response.interviewEndTime != null) {
      this.setEndDate(new Date(response.response.interviewEndTime));
    }
    this.interviewForm.controls.interviewType.setValue(response.response.contentTypeId);
    this.interviewForm.controls.venue.setValue(response.response.applicantVenueId);
    this.comment.setValue(response.response.comment)
    this.inputData.feedbackDocumentPath = response.response.recruiterDocumentPath;

    if (response != null) {
      if (response.response != null) {
        if (response.response.interviewerUserIDList != null) {
          this.selectedInterviewPanelist = [];
          for (let panel of response.response.interviewerUserIDList) {
            for (let interviewer of this.interviewPanelist) {
              if (interviewer.value == panel) {
                this.selectedInterviewPanelist.push(interviewer);
                break;
              }
            }
          }
        }
      }
    }

  }

  ngOnInit() {
    this.hiringProcessService.isJobStatus().subscribe(out => this.jobStatus = out);

    if (this.inputData.sysWorkflowstageActionType == 'COMPLETED' && (this.sessionService.removeStatic || this.sessionService.isProfessionalRecruit)) {
      this.showAssessmentDoc = true;
    }
    if (this.toggleOpen) {
      this.randomcolor = this.nameinitialService.randomcolor;
      this.initializeData();

    }
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.label=res;
    });
  }

  setInterviewVenue(event) {
    this.interviewForm.controls['venue'].setValue(event.value);
    this.interviewVenue = event.label;
  }

  updateStatus() {

    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    this.updateStatusRequestTO.interviewTypeID = this.interviewForm.controls.interviewType.value;
    this.updateStatusRequestTO.applicantVenueID = this.interviewForm.controls.venue.value;
    this.updateStatusRequestTO.startDate = this.interviewForm.controls.startDate.value;
    this.updateStatusRequestTO.endDate = this.interviewForm.controls.endDate.value;
    this.updateStatusRequestTO.calendarInvite = this.interviewForm.controls.calendarInvite.value;
    this.updateStatusRequestTO.interviewVenue = this.interviewVenue;
    let interviewPaneArray = new Array<Number>();
    if (this.selectedInterviewPanelist != null) {
      for (let panel of this.selectedInterviewPanelist) {
        if (panel != null) {
          interviewPaneArray.push(panel.value);
        }
      }
    }
    this.updateStatusRequestTO.interviewPanelist = interviewPaneArray;
    let valid: boolean = true;

    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security.sendEmailAsCalendarInviteRendered && this.interviewForm.controls.calendarInvite.value == "") {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.sendEmailAsCalendarInviteLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.startDateRendered && this.security.startDateRequired && this.interviewStartDate == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.startDateLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    } else if (this.security.startDateRendered) {

      if (this.interviewStartDate != null && this.interviewStartDate < this.currentDate) {
        this.i18UtilService.get('common.warning.startDateCurrentDate').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.endDateRendered && this.security.endDateRequired && this.interviewEndDate == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.endDateLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.security.startDateRendered && this.security.endDateRendered) {
      if (this.interviewEndDate != null) {
        if (this.interviewStartDate == null && !this.security.startDateRequired) {
          this.i18UtilService.get('common.warning.selectStartDate', {'startDate': this.security.startDateLabel, 'endDate': this.security.endDateLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
          valid = false;
        } else if (this.interviewStartDate > this.interviewEndDate) {
          this.i18UtilService.get('common.warning.endDateStartDate').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
      }
    }
    if (this.security.venueRendered && this.security.venueRequired && (this.updateStatusRequestTO.applicantVenueID == null || this.updateStatusRequestTO.applicantVenueID == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.venueLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.interviewTypeRendered && this.security.interviewTypeRequired && (this.updateStatusRequestTO.interviewTypeID == 0 || this.updateStatusRequestTO.interviewTypeID == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label':this.security.interviewTypeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.interviewPanelistRendered && this.security.interviewPanelistRequired && (this.updateStatusRequestTO.interviewPanelist.length == 0)) {
      this.i18UtilService.get('candidate.warning.selectInterviewPanel').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label':this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == "")) {
      this.i18UtilService.get('common.warning.enterLabelCommon', {'label':this.security.commentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (valid == false) {
      return;
    }
    this.interview = true;
    this.output.emit(this.updateStatusRequestTO);
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  deleteInterviewer(interviewPanel) {
    var resultListNew = new Array<any>();
    for (let resultListItem of this.selectedInterviewPanelist) {
      if (resultListItem != null) {
        if (resultListItem.value != interviewPanel.value) {
          resultListNew.push(resultListItem);
        }
      }
    }
    this.selectedInterviewPanelist = resultListNew;

  }

  createRange(min, noOfItems) {
    var items: SelectItem[] = [];
    for (var i = min; i <= (min + noOfItems - 1); i++) {
      items.push(new SelectItem(i, i));
    }
    return items;
  }

  getMMRange() {
    var items: SelectItem[] = [];
    items.push(new SelectItem('Jan', 1));
    items.push(new SelectItem('Feb', 2));
    items.push(new SelectItem('Mar', 3));
    items.push(new SelectItem('Apr', 4));
    items.push(new SelectItem('May', 5));
    items.push(new SelectItem('Jun', 6));
    items.push(new SelectItem('Jul', 7));
    items.push(new SelectItem('Aug', 8));
    items.push(new SelectItem('Sep', 9));
    items.push(new SelectItem('Oct', 10));
    items.push(new SelectItem('Nov', 11));
    items.push(new SelectItem('Dec', 12));
    return items;
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
     } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result.toString();
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  addInterviewPanelistListener(event: SelectItem[]) {
    this.selectedInterviewPanelist = event;
  }

  setStartDate(startDate: Date) {
    if (startDate != null) {
      let _STARTDATE: String = startDate.getDate() + '-' + ((startDate.getMonth()) + 1) + '-' + startDate.getFullYear() + ' ' + startDate.getHours() + ':' + startDate.getMinutes();
      this.interviewForm.controls['startDate'].setValue(_STARTDATE);
    } else {
      this.interviewForm.controls['startDate'].reset();
    }
    this.interviewStartDate = startDate;

  }

  setEndDate(endDate: Date) {

    if (endDate != null) {
      let _ENDDATE: String = endDate.getDate() + '-' + ((endDate.getMonth()) + 1) + '-' + endDate.getFullYear() + ' ' + endDate.getHours() + ':' + endDate.getMinutes();
      this.interviewForm.controls['endDate'].setValue(_ENDDATE);
    } else {
      this.interviewForm.controls['endDate'].reset();
    }
    this.interviewEndDate = endDate;

  }

  statusChangeListener(value: any, sysActionType: any) {
    this.newActionType = sysActionType;
    if (!(sysActionType == 'SCHEDULED' || sysActionType == 'RE-SCHEDULED')) {
      if (this.populateInterviewResponse == undefined) {
        this.interviewService.populateInterview(this.inputData.applicantStatusHistoryID).subscribe(
          (response) => {
            this.setPopulateInterviewResponse(response);
          }
        );
      }
      this.setPopulateInterviewResponse(this.populateInterviewResponse);
    } else {
      this.selectedInterviewPanelist = this.defaultSelectedInterviewPanelist;
      this.setInterviewResponse();
    }
    this.comment.setValue(null);
    this.selectedStatusID.setValue(value);
    this.initSecurity.emit(value);
  }

}
