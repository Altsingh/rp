import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class InterviewService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getApplicantVenues() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_applicantVenue/' + this.sessionService.organizationID+"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  populateInterview(applicantStatusHistoryID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/populateInterview/' + applicantStatusHistoryID + "/")
    .map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()});
  }


  getRequisitionInterviewPanel(requisitionID : number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/requisitionInterviewPanelist/' + requisitionID + '/' + this.sessionService.organizationID+"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

 getAllRequisitionInterviewPanel() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_interviewPanelist/' + this.sessionService.organizationID +"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getContentTypeMaster(contentCategory:string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_contentTypeByCategory/' + this.sessionService.organizationID+"/", { params: { "contentCategory": contentCategory }})
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }
}
