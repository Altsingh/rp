import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../session.service';
import { ApplicantStatusRequest } from '../../../common/applicant-status-request';

@Injectable()
export class HiringProcessService {


  private workflowUpdated = new Subject<boolean>();
  private jobStatus = new Subject<boolean>();
  
  private offeredAnnualGrossValue: any;
  private offeredAnnualGrossSubject: Subject<Number> = new Subject<Number>();
  
  request: ApplicantStatusRequestService;

  private paystructurePolicyEnabled: boolean;
  private paystructureValue: any;
  private paystructureSubject: Subject<any> = new Subject<any>();

  salaryfitmentDetails: any;

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  isPaystructurePolicyEnabled() {
    return this.paystructurePolicyEnabled;
  }

  setPaystructurePolicyEnabled(value: boolean) {
    this.paystructurePolicyEnabled = value;
  }

  getOfferedAnnualGrossValue() {
    return this.offeredAnnualGrossValue;
  }

  setOfferedAnnualGrossValue(value) {
    this.offeredAnnualGrossValue = value;
  }

  updateOfferedAnnualGross(value: Number) {
    this.setOfferedAnnualGrossValue(value);
    this.offeredAnnualGrossSubject.next(value);
  }

  getOfferedAnnualGrossObservable(): Observable<any> {
    return this.offeredAnnualGrossSubject.asObservable();
  }

  getPaystructureValue() {
    return this.paystructureValue;
  }

  setPaystructureValue(value) {
    this.paystructureValue = value;
  }

  updatePaystructure(value) {
    this.setPaystructureValue(value);
    this.paystructureSubject.next(value);
  }

  getPaystructureObservable() {
    return this.paystructureSubject.asObservable();;
  }

  updateWorkFlow() {
    this.workflowUpdated.next(true);
  }

  updateJobStatus(jobStatus: boolean) {
    this.jobStatus.next(jobStatus);
  }

  isJobStatus(): Observable<boolean> {
    return this.jobStatus.asObservable();
  }

  isWorkflowUpdated(): Observable<boolean> {
    return this.workflowUpdated.asObservable();
  }

  getStatusListByStageID(stageID: number, strictWorkflow: boolean, strictStatus: boolean, weightage: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getStatusListByStageForStrictWorkflow/' + stageID + "/" + strictWorkflow + "/" + strictStatus + "/" + weightage + "/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  updateApplicantStatus(applicantStatusRequest: ApplicantStatusRequest) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    this.request = new ApplicantStatusRequestService();
    this.request.input = applicantStatusRequest;
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/updateApplicantStatus/' + this.sessionService.organizationID + "/", JSON.stringify(this.request), options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  initiateOfferApprovalWorkflow(applicantStatusRequest: ApplicantStatusRequest) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    this.request = new ApplicantStatusRequestService();
    this.request.input = applicantStatusRequest;
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/initiateOfferApprovalWorkflow/', JSON.stringify(this.request), options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getHiringProcessData(applicantStatusID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getHiringProcessData/' + applicantStatusID
      + '/' + this.sessionService.organizationID + '/').map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getHiringProcessSecurity(sysWorkflowStageType: string, workflowID: number, stageID: number, statusID: number) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/formSecurity/hiringProcessSecurity/' + sysWorkflowStageType + '/' + workflowID + '/' + stageID + '/' + statusID + "/", null, options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getCurrencyMaster(countryID: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/masters/currency/' + countryID + '/').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getPaystructureMaster() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/masters/paystructure/').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getOtherPayComponents() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/masters/otherPayComponents/').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getJobStatusByRequisitionID(requisitionId: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getJobStatus/' + requisitionId + '/').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getCtqDetails(jobcode : String) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/ctq/?jobcode='+jobcode).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getPolicyConfiguration() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/policyConfiguration').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
    
  }

  getPaystructureByGroupPolicy(applicantStatusID: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/policy/paystructure/?appID='+applicantStatusID).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getOfferTemplateByGroupPolicy(applicantStatusID: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/policy/offertemplate/?appID='+applicantStatusID).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }
}

export class ApplicantStatusRequestService {
  input: ApplicantStatusRequest;
}
