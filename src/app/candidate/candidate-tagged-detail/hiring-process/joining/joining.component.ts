import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { GradeMaster } from '../../../../common/grade-master';
import { DesignationMaster } from '../../../../common/designation-master';
import { SelectItem } from '../../../../shared/select-item';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { NewJobService } from '../../../../job/new-job/new-job.service';
import { JoiningService } from './joining.service';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { HiringProcessService } from '../hiring-process.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-hiring-process-joining',
  templateUrl: './joining.component.html',
  styleUrls: ['./joining.component.css'],
  providers: [JoiningService, NewJobService]
})
export class JoiningComponent implements OnInit {
  lable: string;
  joining: boolean = false;
  @Input() statusLists: StageActionListMaster;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }
  @Input() set spinner(value: boolean) {
    this.joining = false;
  }
  @Input() inputData: HiringProcessResponse;
  @Input() applicantStatusID : Number;
  @Input() security: any;
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  toggleOpen: boolean;

  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;
  gradeList: SelectItem[];
  designationList: SelectItem[];
  employmentTypeList: SelectItem[];
  l1ManagerList: SelectItem[];
  l2ManagerList: SelectItem[];
  hrManagerList: SelectItem[];
  DDRange: SelectItem[];
  MMRange: SelectItem[];
  YYYYRange: SelectItem[];
  _EXPECTEDDATA : Date;
  _ACTUALDATA   : Date;
  jobStatus : boolean =false;
  

  joiningForm: FormGroup;

  constructor(private joiningService: JoiningService, private newJobService: NewJobService,
    private formBuilder: FormBuilder, private notificationService: NotificationService,
    private hiringProcessService: HiringProcessService, private i18UtilService: I18UtilService) { }

  ngOnInit() {
    if (this.toggleOpen) {
      this.hiringProcessService.isJobStatus().subscribe( out => this.jobStatus = out );
      this.initializeData();
    }
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.lable=res;
    });
  }

  initializeData() {
    this.newJobService.getGradeMaster().subscribe(out => this.gradeList = out.responseData);
    this.newJobService.getDesignationMaster().subscribe(out => this.designationList = out.responseData);
    this.joiningService.getEmploymentTypes().subscribe(out => this.employmentTypeList = out.responseData);
    this.joiningService.employeesByRoleType('L1_MANAGER').subscribe(out => this.l1ManagerList = out.responseData);
    this.joiningService.employeesByRoleType('L2_MANAGER').subscribe(out => this.l2ManagerList = out.responseData);
    this.joiningService.employeesByRoleType('HR_MANAGER').subscribe(out => this.hrManagerList = out.responseData);

    this.joiningForm = this.formBuilder.group({
      grade: new FormControl(),
      designation: new FormControl(),
      employmentType: new FormControl(),
      l1Manager: new FormControl(),
      l2Manager: new FormControl(),
      hrManager: new FormControl(),
      ctc: new FormControl(),
      expectedJoiningDate: new FormControl(),
      actualJoiningDate: new FormControl(),
      employeeCode: new FormControl()
    });

    this.DDRange = this.createRange(1, 31);
    this.MMRange = this.getMMRange();
    this.YYYYRange = this.createRange(2000, 20);

    this.joiningService.getOnboardingDetails(this.applicantStatusID).subscribe(
      response =>
      {
        if(response.messageCode.code === 'EC200')
        {
          if(response.responseData.actualJoiningDate != null)
          {
            this.setActualJoiningDate(new Date(response.responseData.actualJoiningDate));
          }
          if(response.responseData.expectedJoiningDate != null)
          {
            this.setExpectedJoiningDate(new Date(response.responseData.expectedJoiningDate));
          }
        
        this.joiningForm.controls['employeeCode'].setValue(response.responseData.employeeCode);
        }

      }
    );
  }

  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    this.updateStatusRequestTO.grade = this.joiningForm.controls.grade.value;
    this.updateStatusRequestTO.designation = this.joiningForm.controls.designation.value;
    this.updateStatusRequestTO.employmentType = this.joiningForm.controls.employmentType.value;
    this.updateStatusRequestTO.l1Manager = this.joiningForm.controls.l1Manager.value;
    this.updateStatusRequestTO.l2Manager = this.joiningForm.controls.l2Manager.value;
    this.updateStatusRequestTO.hrManager = this.joiningForm.controls.hrManager.value;
    this.updateStatusRequestTO.ctc = this.joiningForm.controls.ctc.value;
    this.updateStatusRequestTO.expectedJoiningDate = this.joiningForm.controls.expectedJoiningDate.value;
    this.updateStatusRequestTO.actualJoiningDate = this.joiningForm.controls.actualJoiningDate.value;
    this.updateStatusRequestTO.tenantID = this.joiningService.getTenantID();
    this.updateStatusRequestTO.employeeCode = this.joiningForm.controls.employeeCode.value;
    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
     return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security.gradeRendered && this.security.gradeRequired && (this.updateStatusRequestTO.grade == null || this.updateStatusRequestTO.grade == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.gradeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.designationRendered && this.security.designationRequired && (this.updateStatusRequestTO.designation == null || this.updateStatusRequestTO.designation == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.designationLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.employmentTypeRendered && this.security.employmentTypeRequired && (this.updateStatusRequestTO.employmentType == null || this.updateStatusRequestTO.employmentType == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.employmentTypeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.hrManagerRendered && this.security.hrManagerRequired && (this.updateStatusRequestTO.hrManager == null || this.updateStatusRequestTO.hrManager == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.hrManagerLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.l1ManagerRendered && this.security.l1ManagerRequired && (this.updateStatusRequestTO.l1Manager == null || this.updateStatusRequestTO.l1Manager == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.l1ManagerLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.l2ManagerRendered && this.security.l2ManagerRequired && (this.updateStatusRequestTO.l2Manager == null || this.updateStatusRequestTO.l2Manager == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.l2ManagerLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.security.ctcRendered && this.security.ctcRequired && (this.updateStatusRequestTO.ctc == null)) {
      this.i18UtilService.get('common.warning.enterLabelCommon', {'label': this.security.ctcLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.employeeCodeRendered && this.security.employeeCodeRequired && (this.updateStatusRequestTO.employeeCode == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.employeeCodeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.expectedJoiningDateRendered && this.security.expectedJoiningDateRequired && (this.updateStatusRequestTO.expectedJoiningDate == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.expectedJoiningDateLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.security.actualJoiningDateRendered && this.security.actualJoiningDateRequired && (this.updateStatusRequestTO.actualJoiningDate == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.actualJoiningDateLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == "")) {
      this.i18UtilService.get('common.warning.enterLabelCommon', {'label': this.security.commentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && (this.updateStatusRequestTO.attachmentName == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (valid == false) {
      return;
    }
    this.joining = true;
    this.output.emit(this.updateStatusRequestTO);
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  createRange(min, noOfItems) {
    var items: SelectItem[] = [];
    for (var i = min; i <= (min + noOfItems - 1); i++) {
      items.push(new SelectItem(i, i));
    }
    return items;
  }

  getMMRange() {
    var items: SelectItem[] = [];
    items.push(new SelectItem('Jan', 1));
    items.push(new SelectItem('Feb', 2));
    items.push(new SelectItem('Mar', 3));
    items.push(new SelectItem('Apr', 4));
    items.push(new SelectItem('May', 5));
    items.push(new SelectItem('Jun', 6));
    items.push(new SelectItem('Jul', 7));
    items.push(new SelectItem('Aug', 8));
    items.push(new SelectItem('Sep', 9));
    items.push(new SelectItem('Oct', 10));
    items.push(new SelectItem('Nov', 11));
    items.push(new SelectItem('Dec', 12));
    return items;
  }

  setExpectedJoiningDate(expectedDate: Date) {
    this.joiningForm.controls['expectedJoiningDate'].setValue(expectedDate);
    this._EXPECTEDDATA = expectedDate;
  }

  setActualJoiningDate(actualDate: Date) {
    
    this.joiningForm.controls['actualJoiningDate'].setValue(actualDate);
    this._ACTUALDATA = actualDate;
  }
  statusChangeListener(value: any) {
    this.selectedStatusID.setValue(value);
    this.initSecurity.emit(value);
  }
}
