import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class JoiningService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getEmploymentTypes() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/employmentTypeMaster/' + this.sessionService.organizationID+"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  employeesByRoleType(roleType:string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/employeesByRoleType/' + roleType +'/' + this.sessionService.organizationID+"/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

  getOnboardingDetails(applicantStatusID:Number)
  {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getOnboardingDetails/' + applicantStatusID).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()});
  }

  getTenantID() {
    return this.sessionService.tenantID;
  }
}
