import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FitmentService } from 'app/candidate/candidate-tagged-detail/hiring-process/fitment/fitment.service';
import { FormBuilder } from '@angular/forms';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { FitmentBasicDetailsTo } from 'app/common/fitment-basic-details-to';
import { FitmentProposedRemunerationServiceService } from '../fitment-proposed-remuneration/fitment-proposed-remuneration-service.service'
import { ActivatedRoute } from '@angular/router';
import { ApplicantPaystructureDetailsTo } from '../../../../../common/applicant-paystructure-details-to';
import { SelectItem } from 'app/shared/select-item';
import { PaystructureInstalments } from '../paystructure-instalments.model';
import { FitmentComputableStackComponent } from 'app/candidate/candidate-tagged-detail/hiring-process/fitment/fitment-computable-stack/fitment-computable-stack.component';
import { SaveCTCRequestJSON } from '../fitment-computable-stack/SaveCTCRequestJSON.model';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-fitment-proposed-remuneration',
  templateUrl: './fitment-proposed-remuneration.component.html',
  styleUrls: ['./fitment-proposed-remuneration.component.css'],
  providers: [FitmentService, FitmentProposedRemunerationServiceService]
})
export class FitmentProposedRemunerationComponent implements OnInit {

  @Input() security: any;

  @Input() applicantStatusID: Number;

  @Input() stackList: any;

  @Input() otherExpensesList: any;

  @Input() saveBasicSalary: SaveBasicSalary;

  @Input() data: any;

  @Input() toggleOpen: boolean;

  @ViewChild(FitmentComputableStackComponent)
  FITMENTCOMPUTABLESTACK: FitmentComputableStackComponent;

  loader: boolean = false;

  responseData: any;

  toBeGivenList = [
    new SelectItem('Yes', true),
    new SelectItem('No', false)
  ]

  payFrequencyList = [
    new SelectItem('Select', 0),
    new SelectItem('1', 1),
    new SelectItem('2', 2),
    new SelectItem('3', 3),
    new SelectItem('4', 4),
    new SelectItem('5', 5),
    new SelectItem('6', 6),
    new SelectItem('7', 7),
    new SelectItem('8', 8),
    new SelectItem('9', 9),
    new SelectItem('10', 10),
    new SelectItem('11', 11),
    new SelectItem('12', 12)
  ];

  monthList = [];

  yearList = [];

  constructor(private fitmentService: FitmentService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private fitmentProposedRemunerationServiceService: FitmentProposedRemunerationServiceService,
    private route: ActivatedRoute, private i18UtilService: I18UtilService) {

    this.monthList = [new SelectItem('January', '0'),
    new SelectItem('February', '1'),
    new SelectItem('March', '2'),
    new SelectItem('April', '3'),
    new SelectItem('May', '4'),
    new SelectItem('June', '5'),
    new SelectItem('July', '6'),
    new SelectItem('August', '7'),
    new SelectItem('September', '8'),
    new SelectItem('October', '9'),
    new SelectItem('November', '10'),
    new SelectItem('December', '11')
    ]

    let currentYear = (new Date()).getFullYear();
    this.yearList = [];
    for(let n of this.returnArray(5)) {
      this.yearList.push(new SelectItem(currentYear+n, currentYear+n));
    }
  }

  ngOnInit() { }

  toggleDiv() {
    this.toggleOpen = !this.toggleOpen;
  }

  decimalValuesKeyPress(event: any) {
    if (event.key) {
      if (event.target.value.indexOf('.') > 0 && event.key == '.') {
        return false;
      }
      if (event.charCode) {
        if (event.charCode === 46
          || event.keyCode === 8) {
          return true;
        }
      }

      if (event.key != '.' && event.key.replace(/[^0-9]*/, '') == '') {
        return false;
      }
    }
    if ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46
      || event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37
      || event.keyCode === 39) {
      return true;
    } else {
      return false;
    }
  }

  validateProposedRenumerationFields(valid: boolean): boolean {
    let breakupSum = 0;
    let showBreakupError = true;

    if(this.security.salaryStackRendered) {
      if(this.FITMENTCOMPUTABLESTACK == null) {
        this.i18UtilService.get('candidate.warning.reviewProposedRemuneration').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;  
      } else {
        this.FITMENTCOMPUTABLESTACK.validateComputableStackFields(valid);
      }
    }

    if (this.otherExpensesList != null) {
      for(let comp of this.otherExpensesList) {
       
        if(comp.toBeGiven == 1)
        {
          
        if(comp.frequency == null || comp.frequency == '')
        {
          this.i18UtilService.get('candidate.warning.frequencyReq').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, comp.payCodeName + ": "+ res);
          });
          valid = false;

        }
        if(comp.amount == null || comp.amount == '' || comp.amount==0)
        {
          this.i18UtilService.get('candidate.warning.amountReq').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, comp.payCodeName + ": "+ res);
          });
          valid = false;
        }

        if(comp.effectiveDate == null|| comp.effectiveDate == '')
        {
          this.i18UtilService.get('candidate.warning.effectiveDateReq').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, comp.payCodeName + ": "+ res);
          });
          valid = false;
        }
      }
        if(comp.frequency != null && comp.frequency != 0 && comp.amount != null && comp.amount != 0) {
          breakupSum = 0;
          showBreakupError = true;
          for (let instalment of comp.detailsInstalments) {
            if (instalment.amount == null || instalment.amount == 0) {
              this.i18UtilService.get('candidate.warning.amountBreakupReq').subscribe((res: string) => {
                this.notificationService.addMessage(NotificationSeverity.WARNING, comp.payCodeName + ": "+ res);
              });
              valid = false;
              showBreakupError = false;
            } else {
              breakupSum = Number(breakupSum) + Number(instalment.amount);
            }

            if(instalment.payoutDate == null|| instalment.payoutDate == '')
            {
              this.i18UtilService.get('candidate.warning.payoutDateReq').subscribe((res: string) => {
                this.notificationService.addMessage(NotificationSeverity.WARNING, comp.payCodeName + ": "+ res);
              });
              valid = false;
            }

          }
          if (showBreakupError && (comp.amount != breakupSum)) {
            this.i18UtilService.get('candidate.warning.ComponentAnountMismatch').subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, comp.payCodeName + ": "+ res);
            });
            valid = false;
          }
        }
      }
    }
    return valid;
  }

  setSaveSalaryFitmentDetailsRequestObject(saveBasicSalary: SaveBasicSalary): SaveBasicSalary {
    if (saveBasicSalary == null) {
      saveBasicSalary = new SaveBasicSalary();
    }
    if (saveBasicSalary.input == null) {
      saveBasicSalary.input = new FitmentBasicDetailsTo();
    }
    let applicantPaystructureDetailsList: ApplicantPaystructureDetailsTo[] = [];
    if (this.stackList != null) {
      for (let obj of this.stackList) {
        applicantPaystructureDetailsList.push(obj);
      }
    }
    if (this.otherExpensesList != null) {
      for (let obj of this.otherExpensesList) {
        applicantPaystructureDetailsList.push(obj)
      }
    }
    saveBasicSalary.input.applicantPaystructureDetailsList = applicantPaystructureDetailsList;
    saveBasicSalary.input.sectionName = "remuneration";

    this.saveBasicSalary = saveBasicSalary;

    return saveBasicSalary;
  }

  saveProposedRenumerationDetails() {

    let valid = this.validateProposedRenumerationFields(true);

    if (valid == true) {

      this.setSaveSalaryFitmentDetailsRequestObject(this.saveBasicSalary);

      this.fitmentService.saveSalaryFitmentDetails(this.saveBasicSalary, this.applicantStatusID).subscribe(response => {
        if (response != null && response.messageCode != null && response.messageCode.code != null) {
          if (response.messageCode.code == "EC200") {
            this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
          }
        } else if (response.messageCode.code == "EC201") {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
        }
      });
    }
  }

  setSaveCTCRequestJSON(): SaveCTCRequestJSON {
    return this.FITMENTCOMPUTABLESTACK.setSaveCTCRequestJSON();
  }

  instalmentDateChangeListener(event, instalment) {
    let date: Date = event.value;
    instalment.payoutDate = date;

    instalment.month = date.getMonth();
    instalment.year = date.getFullYear();
  }

  datetimeChangeListener(event, comp) {
    //yyyy-MM-dd
    let date: Date = event.value;
    comp.effectiveDate = date;
  }

  returnArray(n: any) {
    let arr = [];
    let i = 0;
    while (i < n) {
      arr.push(i);
      i++;
    }
    return arr;
  }

  setPayFrequency(comp: any, value: any) {
    comp.frequency = value;
    this.computeDetailsInstalments(comp);
  }

  setAmount(comp: any, value: any) {
    comp.amount = value;
    this.computeDetailsInstalments(comp);
  }

  computeDetailsInstalments(comp: any) {
    let index = 0;
    let detailsInstalments: Array<PaystructureInstalments> = new Array<PaystructureInstalments>();
    if (comp.frequency != null && comp.frequency != 0 && comp.amount != null && comp.amount != 0) {
      for (let arr of this.returnArray(comp.frequency)) {
        index++;
        let instalment: PaystructureInstalments = new PaystructureInstalments();
        if (index == comp.frequency) {
          instalment.amount = comp.amount - (Math.floor(comp.amount / comp.frequency) * (index - 1));
        } else {
          instalment.amount = Math.floor(comp.amount / comp.frequency);
        }
        detailsInstalments.push(instalment)
      }

    }
    comp.detailsInstalments = detailsInstalments;
  }

  toBeGivenChangeListener(toBeGiven:any, comp:any) {
    if (!toBeGiven) {
      comp.frequency = null;
      comp.effectiveDate = null;
      comp.amount = null;
      comp.detailsInstalments = null;
    }
  }
}

export class SaveBasicSalary {
  input: FitmentBasicDetailsTo;
}
