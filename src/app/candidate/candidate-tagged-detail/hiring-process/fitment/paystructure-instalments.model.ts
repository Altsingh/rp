export class PaystructureInstalments {
    applicantPaystructureDetailsInstalmentsID: Number;
    amount: any;
    amountFormat: String;
    payoutDate: Date;
    month: Number;
    year: Number
}