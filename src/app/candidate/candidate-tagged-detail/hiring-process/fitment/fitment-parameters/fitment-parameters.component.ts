import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FitmentService } from '../fitment.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'app/shared/select-item';
import { FitmentBasicDetailsTo } from 'app/common/fitment-basic-details-to';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { ApplicantPaystructureDetailsTo } from 'app/common/applicant-paystructure-details-to';
import { HiringProcessService } from '../../hiring-process.service';
import { ModuleConfigService } from 'app/module-config.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { ValidationService } from 'app/shared/custom-validation/validation.service';
import { DialogService } from 'app/shared/dialog.service';
import { WarningDialogComponent } from 'app/shared/warning-dialog/warning-dialog.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-fitment-parameters',
  templateUrl: './fitment-parameters.component.html',
  styleUrls: ['./fitment-parameters.component.css'],
  providers: [FitmentService]
})
export class FitmentParametersComponent implements OnInit {

  parameterForm: FormGroup;
  saveBasicSalary: SaveBasicSalary;
  currencyList: SelectItem[];
  paystructureList: SelectItem[];


  @Input() security: any;

  @Input() applicantStatusID: Number;

  @Input() toggleOpen: boolean;

  @Output() toggleParam: EventEmitter<any> = new EventEmitter();

  @Output() toggleRenum: EventEmitter<any> = new EventEmitter();

  candidateName: String;
  gradeCode: String;
  orgUnitCode: String;
  joiningLocation: String;
  band: String;
  fitment: Boolean = false;
  paramterLabel: String;
  jobDesignation: String;
  moduleConfig: any;
  isHikeCalculationOnFixedOnly: boolean = false;
  
  @Output() stackList: EventEmitter<any> = new EventEmitter();

  constructor(private fitmentService: FitmentService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private hiringProcessService: HiringProcessService,
    private moduleConfigService: ModuleConfigService,
    private i18UtilService: I18UtilService, private dialogService: DialogService) {

    this.parameterForm = this.formBuilder.group({
      minCTC: '',
      maxCTC: '',
      averageCTC: '',
      financeBudget: '',
      currentCurrency: 1,
      currentFixedPay: '',
      currentVariablePay: '',
      currentAnnualGross: 0,
      offeredCurrency: 1,
      offeredFixedPay: '',
      offeredVariablePay: '',
      offeredAnnualGross: 0,
      hikePercentage: 0,
      paystructure: ''
    })
  }

  ngOnInit() {
    this.getFitmentDetails();
    this.currencyList = [new SelectItem('INR', 1)];
    this.fitmentService.getPaystructureMaster().subscribe(out => this.paystructureList = out.responseData);
    this.valueChangesListeners();
    this.i18UtilService.get('candidate.hiring_process.generateSalaryStack').subscribe((res: string) => {
      this.paramterLabel=res;
     });
  }

  fetchModuleConfig() {
    const configParameters = ['HikeCalculationOnFixedOnly'];
    Promise.resolve(this.moduleConfigService.getModuleConfig('Hiring', configParameters)).then((moduleConfigJSON) => {
      if (moduleConfigJSON) {
        if (moduleConfigJSON.response) {
          this.moduleConfig = moduleConfigJSON.response;
          if (this.moduleConfig['hikeCalculationOnFixedOnly'] == null) {
            this.isHikeCalculationOnFixedOnly = false;
          } else {
            this.isHikeCalculationOnFixedOnly = (this.moduleConfig['HikeCalculationOnFixedOnly'].value == 1) ? true : false;
          }
        }
      }
    });
  }

  toggleDiv() {
    this.toggleOpen = !this.toggleOpen;
    this.toggleParam.emit(this.toggleOpen);
  }

  getFitmentDetails() {
    if(this.hiringProcessService.salaryfitmentDetails != undefined && this.hiringProcessService.salaryfitmentDetails != null) {

      let responseData = this.hiringProcessService.salaryfitmentDetails;

      this.parameterForm.controls['minCTC'].setValue(responseData.minCTC);
      this.parameterForm.controls['maxCTC'].setValue(responseData.maxCTC);
      this.parameterForm.controls['averageCTC'].setValue(responseData.averageCTC);
      this.parameterForm.controls['financeBudget'].setValue(responseData.financeBudget);
      this.parameterForm.controls['offeredFixedPay'].setValue(responseData.offeredFixedPay);
      this.parameterForm.controls['offeredVariablePay'].setValue(responseData.offeredVariablePay);
      this.parameterForm.controls['currentVariablePay'].setValue(responseData.currentVariablePay);
      this.parameterForm.controls['currentFixedPay'].setValue(responseData.currentFixedPay);
      if(this.isPaystructurePolicyEnabled()) {
        this.parameterForm.controls['paystructure'].setValue(this.hiringProcessService.getPaystructureValue().label);
      } else {
        this.parameterForm.controls['paystructure'].setValue(responseData.paystructure);
      }
      this.parameterForm.controls['currentAnnualGross'].setValue(responseData.currentCTC);
      this.parameterForm.controls['offeredAnnualGross'].setValue(responseData.offeredCTC);
      this.parameterForm.controls['hikePercentage'].setValue(responseData.hikePercent);
      this.candidateName = responseData.candidateName;
      this.gradeCode = responseData.gradeCode;
      this.orgUnitCode = responseData.orgUnitCode;
      this.joiningLocation = responseData.joiningLocation;
      this.band = responseData.band;
      this.jobDesignation = responseData.jobDesignation;
      this.setSaveSalaryFitmentDetailsRequestObject(this.saveBasicSalary);
    }
  }

  validateParametersFields(valid: boolean): boolean {

    if (this.security.fitmentMinCTCRendered && this.security.fitmentMinCTCRequired &&
      (this.parameterForm.controls.minCTC.value == null || this.parameterForm.controls.minCTC.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.fitmentMinCTCLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentMaxCTCRendered && this.security.fitmentMaxCTCRequired &&
      (this.parameterForm.controls.maxCTC.value == null || this.parameterForm.controls.maxCTC.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.fitmentMaxCTCLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentAverageCTCRendered && this.security.fitmentAverageCTCRequired &&
      (this.parameterForm.controls.averageCTC.value == null || this.parameterForm.controls.averageCTC.value == '' || this.parameterForm.controls.averageCTC.value == 0)) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.fitmentAverageCTCLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentFinanceBudgetRendered && this.security.fitmentFinanceBudgetRequired &&
      (this.parameterForm.controls.financeBudget.value == null || this.parameterForm.controls.financeBudget.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.fitmentFinanceBudgetLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }
    if (this.security.fitmentCurrentCurrencyRendered && this.security.fitmentCurrentCurrencyRequired &&
      (this.parameterForm.controls.currentCurrency.value == null || this.parameterForm.controls.currentCurrency.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCurrent',{'label':  this.security.fitmentCurrentCurrencyLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentCurrentFixedPayRendered && this.security.fitmentCurrentFixedPayRequired &&
      (this.parameterForm.controls.currentFixedPay.value == undefined || this.parameterForm.controls.currentFixedPay.value == null || this.parameterForm.controls.currentFixedPay.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCurrent',{'label':  this.security.fitmentCurrentFixedPayLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }


    if (this.security.fitmentCurrentVariablePayRendered && this.security.fitmentCurrentVariablePayRequired &&
      (this.parameterForm.controls.currentVariablePay.value == undefined || this.parameterForm.controls.currentVariablePay.value == null || this.parameterForm.controls.currentVariablePay.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCurrent',{'label': this.security.fitmentCurrentVariablePayLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentCurrentAnnualGrossRendered && this.security.fitmentCurrentAnnualGrossRequired &&
      (this.parameterForm.controls.currentAnnualGross.value == undefined || this.parameterForm.controls.currentAnnualGross.value == null || this.parameterForm.controls.currentAnnualGross.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCurrent',{'label': this.security.fitmentCurrentAnnualGrossLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentOfferedCurrencyRendered && this.security.fitmentOfferedCurrencyRequired &&
      (this.parameterForm.controls.offeredCurrency.value == null || this.parameterForm.controls.offeredCurrency.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredOffered',{'label': this.security.fitmentOfferedCurrencyLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentOfferedFixedPayRendered && this.security.fitmentOfferedFixedPayRequired &&
      (this.parameterForm.controls.offeredFixedPay.value == undefined || this.parameterForm.controls.offeredFixedPay.value == null || this.parameterForm.controls.offeredFixedPay.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredOffered',{'label': this.security.fitmentOfferedFixedPayLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentOfferedVariablePayRendered && this.security.fitmentOfferedVariablePayRequired &&
      (this.parameterForm.controls.offeredVariablePay.value == undefined || this.parameterForm.controls.offeredVariablePay.value == null || this.parameterForm.controls.offeredVariablePay.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredOffered',{'label': this.security.fitmentOfferedVariablePayLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }
    if (this.security.fitmentOfferedAnnualGrossRendered && this.security.fitmentOfferedAnnualGrossRequired &&
      (this.parameterForm.controls.offeredAnnualGross.value == undefined || this.parameterForm.controls.offeredAnnualGross.value == null || this.parameterForm.controls.offeredAnnualGross.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredOffered',{'label': this.security.fitmentOfferedAnnualGrossLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentHikePercentageRendered && this.security.fitmentHikePercentageRequired &&
      (this.parameterForm.controls.hikePercentage.value == null || this.parameterForm.controls.hikePercentage.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.fitmentHikePercentageLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.fitmentParametersRendered && this.security.fitmentParametersRequired &&
      (this.parameterForm.controls.paystructure.value == null || this.parameterForm.controls.paystructure.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.fitmentParametersLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }
    return valid;
  }

  setSaveSalaryFitmentDetailsRequestObject(saveBasicSalary: SaveBasicSalary): SaveBasicSalary {
    if (saveBasicSalary == null) {
      saveBasicSalary = new SaveBasicSalary();
    }
    if (saveBasicSalary.input == null) {
      saveBasicSalary.input = new FitmentBasicDetailsTo();
    }
    saveBasicSalary.input.currentFixedPay = this.parameterForm.controls.currentFixedPay.value;
    saveBasicSalary.input.currentVariablePay = this.parameterForm.controls.currentVariablePay.value;
    saveBasicSalary.input.currentAnnualGross = this.parameterForm.controls.currentAnnualGross.value;
    
    saveBasicSalary.input.offeredFixedPay = this.parameterForm.controls.offeredFixedPay.value;
    saveBasicSalary.input.offeredVariablePay = this.parameterForm.controls.offeredVariablePay.value;
    saveBasicSalary.input.offeredAnnualGross = this.parameterForm.controls.offeredAnnualGross.value;

    saveBasicSalary.input.paystructure = this.parameterForm.controls.paystructure.value;
    saveBasicSalary.input.hikePercentage = this.parameterForm.controls.hikePercentage.value;
    saveBasicSalary.input.sectionName = "parameter";

    this.saveBasicSalary = saveBasicSalary;

    return saveBasicSalary;
  }

  saveFitmentDetails() {

    this.notificationService.clear();
    this.fitment = true;

    let valid = this.validateParametersFields(true);

    if (valid == true) {

      if (this.security.salaryStackRendered) {
        this.i18UtilService.get('candidate.success.fitmentParamSaved').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
        this.toggleOpen = false;
        this.toggleRenum.emit(true);
        this.fitment = false;
        this.calculateHike();
      }
      else if (this.security.customSalaryStackRendered) {

        this.fitmentService.fetchPayReviewStack(this.applicantStatusID, this.candidateName, (this.parameterForm.controls.offeredFixedPay.value),
          this.orgUnitCode, this.joiningLocation, this.gradeCode, this.band, this.parameterForm.controls.paystructure.value, this.jobDesignation).subscribe(payreviewResponse => {
            if (payreviewResponse != null && payreviewResponse.messageCode != null && payreviewResponse.messageCode.code != null) {
              if (payreviewResponse.messageCode.code == "EC200") {
                if (this.security.fitmentOfferedVariablePayDisabled) {
                  this.parameterForm.controls['offeredVariablePay'].setValue(payreviewResponse.response.employee.variablePay * 12);
                }
                this.calculateHike();

                let resultList: Array<ApplicantPaystructureDetailsTo> = [];
                for (let res of payreviewResponse.response.salary_structures) {
                  let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                  result.amount = res.monthly;
                  result.annualAmount = res.yearly;
                  result.payCodeName = res.display_name;
                  result.sysPayCode = res.display_label;
                  result.paystructureType = 'Offered_Stack';
                  resultList.push(result);
                }
                this.stackList.emit(resultList);
                this.i18UtilService.get('candidate.success.stackGenerated').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
                this.toggleOpen = false;
                this.toggleRenum.emit(true);
                this.fitment = false;
              } else if (payreviewResponse.messageCode.code == "EC201") {
                this.notificationService.setMessage(NotificationSeverity.WARNING, payreviewResponse.messageCode.message);
                this.fitment = false;
              } else {
                this.i18UtilService.get('candidate.error.stackGenerationFailed').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                });
                this.fitment = false;
              }
            } else {
              this.fitment = false;
              this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            }
          });
      }

    } else {
      this.fitment = false;
    }
  }

  calculateHike() {
    let hike: any;
    if(this.isHikeCalculationOnFixedOnly) {
      hike = this.getHike(this.parameterForm.controls.currentFixedPay.value, this.parameterForm.controls.offeredFixedPay.value);
    } else {
      hike = this.getHike(this.parameterForm.controls.currentAnnualGross.value, this.parameterForm.controls.offeredAnnualGross.value);
    }
    this.parameterForm.controls['hikePercentage'].setValue(hike);
  }

  getHike(current, offered) {
    if(current == undefined || current == null || current == '' || current == 0) {
      return 0;
    } else if(offered == undefined || offered == null || offered == '' || offered == 0) {
      return 0;
    } else {
      return (((offered - current) / current) * 100).toFixed(2);
    }
  }

  valueChangesListeners() {

    this.parameterForm.controls.currentFixedPay.valueChanges.subscribe(data => {
      this.parameterForm.controls['currentAnnualGross'].setValue(this.getSum(this.parameterForm.controls.currentVariablePay.value, data));
    });

    this.parameterForm.controls.currentVariablePay.valueChanges.subscribe(data => {
      this.parameterForm.controls['currentAnnualGross'].setValue(this.getSum(this.parameterForm.controls.currentFixedPay.value, data));
    });

    this.parameterForm.controls.offeredFixedPay.valueChanges.subscribe(data => {
      this.parameterForm.controls['offeredAnnualGross'].setValue(this.getSum(this.parameterForm.controls.offeredVariablePay.value, data));
    });


    this.parameterForm.controls.offeredVariablePay.valueChanges.subscribe(data => {
      this.parameterForm.controls['offeredAnnualGross'].setValue(this.getSum(this.parameterForm.controls.offeredFixedPay.value, data));
    });

    this.parameterForm.controls.offeredAnnualGross.valueChanges.subscribe(data => {
      this.hiringProcessService.updateOfferedAnnualGross(data);
    });

  }

  getSum(value1, value2) {
    let sum = 0;
    if(value1 != undefined && value1 != null && value1 != '') {
      sum = +sum + +value1;
    }
    if(value2 != undefined && value2 != null && value2 != '') {
      sum = +sum + +value2;
    }
    return sum;
  }

  pasteValue(event:any){
    const pastedValue=event.clipboardData.getData('TEXT');
    const matchedResult=isNullOrUndefined(pastedValue.match('[0-9]+')) ? null : pastedValue.match('[0-9]+')[0]
    
    if(matchedResult===pastedValue){
      if(event.clipboardData.getData('TEXT').length + event.target.value.length > 11){
        this.i18UtilService.get("common.warning.numberOfCharacter").subscribe(response=>{
          this.dialogService.setDialogData(response);
          this.dialogService.setDialogData1(true);
          this.dialogService.open(WarningDialogComponent,{'width':'350px'})
        })
        event.preventDefault();
      }
      event.clipboardData.clearData();
      return true;
    }else{
      this.i18UtilService.get("common.warning.onlyNumberAllowed").subscribe(response=>{
        this.dialogService.setDialogData(response);
        this.dialogService.setDialogData1(true);
        this.dialogService.open(WarningDialogComponent,{'width':'350px'})
      })
      event.preventDefault();
    }
  }

  decimalValuesKeyPress(event: any) {
    if (event.key) {
      if (event.key == '.') {
        return false;
      }
      if (event.charCode) {
        if (event.charCode === 46
          || event.keyCode === 8
          || event.keyCode === 39) {
          return true;
        }
      }

      if (event.key != '.' && event.key.replace(/[^0-9]*/, "") == '') {
        return false;
      }

      if (event.target.value.length > 10) {
        return false;
      }
    }

    if ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46
      || event.keyCode === 8 || event.keyCode === 46
      || event.keyCode === 39) {
      return true;
    } else {
      return false;
    }
  }

  updatePaystructure(event) {
    this.hiringProcessService.updatePaystructure(event);
  }

  isPaystructurePolicyEnabled() {
    return this.hiringProcessService.isPaystructurePolicyEnabled();
  }

}

export class SaveBasicSalary {
  input: FitmentBasicDetailsTo;
}
