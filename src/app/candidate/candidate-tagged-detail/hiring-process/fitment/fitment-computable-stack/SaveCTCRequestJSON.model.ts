export class SaveCTCRequestJSON {
    groupList: any;
    applicantStatusId: any;
    currencyCode: any;
    payStructureId: any;
    effectiveFrom: any;
    guaranteedCTCAmt: any;
    nonGuaranteedCTCAmt: any;
    formulaEdit: any;
    freeTextEdit: any;
    totalAmtMon: any;
    totalAmtYr: any;
    totalAmtLabel: any;
    totalCTCAmt: any;
    guaranteedAmtYr: any;
    nonGuaranteedAmtYr: any;
    totalAmtMonthWithoutHidden: any;
    totalAmtMonthWithoutHiddenLabel: any;
    payGroupName: any;
}