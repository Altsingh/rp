import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FitmentService } from '../fitment.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FitmentBasicDetailsTo } from 'app/common/fitment-basic-details-to';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { SelectItem } from 'app/shared/select-item';
import { HiringProcessService } from 'app/candidate/candidate-tagged-detail/hiring-process/hiring-process.service';
import { MessageCode } from '../../../../../common/message-code';
import { SaveCTCRequestJSON } from './SaveCTCRequestJSON.model';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-fitment-computable-stack',
  templateUrl: './fitment-computable-stack.component.html',
  styleUrls: ['./fitment-computable-stack.component.css'],
  providers: [FitmentService]
})
export class FitmentComputableStackComponent implements OnInit {

  @Input() applicantStatusID: number;

  @Input() security: any;

  salarySecurity: any;
  currencyList: SelectItem[];
  paystructureList: SelectItem[];

  currency = new FormControl();
  paystructure = new FormControl();
  maxSalary = new FormControl();
  effectiveDate = new FormControl();
  guaranteedCTC: String;
  nonGuaranteedCTC: String;
  salaryFitmentStackResponse: any;
  costToCompanyStackVisible: boolean = false;
  salaryEditType = new FormControl();

  showloader: boolean = false;
  fbp: boolean = false;
  totalAmtYr: String;
  totalAmtMon: String;
  totalAmtMonthWithoutHidden: String;
  totalCTCAnnotate: String;
  variableSectionVisible: boolean = false;
  dateSwitch: boolean = false;
  isResetComputableFields: boolean;

  constructor(private hiringProcessService: HiringProcessService,
    private fitmentService: FitmentService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,private i18UtilService: I18UtilService) {

  }

  ngOnInit() {
      this.getMastersData();
      this.initFormSecurity();
      this.initializeData();
      
      this.hiringProcessService.getOfferedAnnualGrossObservable().subscribe( value => {
        this.maxSalary.setValue(value);
        this.resetComputeFields();
      });

      this.hiringProcessService.getPaystructureObservable().subscribe( paystructure => {
        this.paystructure.setValue(paystructure.value);
        this.resetComputeFields();
      });
    
  }

  
  initializeData() {
    this.getSalaryFitmentStack();
  }


  getSalaryFitmentStack() {
    this.showloader = true;
    this.fitmentService.getSalaryFitmentStack(this.applicantStatusID).subscribe(response => {
      this.salaryFitmentStackResponse = undefined;
      this.guaranteedCTC = undefined;
      this.nonGuaranteedCTC = undefined;
      this.totalAmtMon = undefined;
      this.totalAmtYr = undefined;
      this.totalAmtMonthWithoutHidden = undefined;
      let messageTO: MessageCode = response.messageCode;
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.salaryFitmentStackResponse = response.response.groupList;
          this.guaranteedCTC = response.response.guaranteedCTC;
          this.nonGuaranteedCTC = response.response.nonGuaranteedCTC;
          this.totalAmtMon = response.response.totalAmtMon;
          this.totalAmtYr = response.response.totalAmtYr;
          this.totalAmtMonthWithoutHidden = response.response.totalAmtMonthWithoutHidden;
          this.totalCTCAnnotate = response.response.totalCTCAnnotate;
          this.costToCompanyStackVisible = true;
          this.variableSectionVisible = true;

          this.salaryEditType.setValue(response.response.salaryEditType);
          
          this.effectiveDate.setValue(new Date(response.response.effectiveDate));

          if(this.hiringProcessService.salaryfitmentDetails != undefined && this.hiringProcessService.salaryfitmentDetails != null) {
            let maxSalary = this.hiringProcessService.salaryfitmentDetails.maxCTC;
            if(this.hiringProcessService.getOfferedAnnualGrossValue() != null && this.hiringProcessService.getOfferedAnnualGrossValue() != undefined) {
              let tempmaxSalary = this.hiringProcessService.getOfferedAnnualGrossValue();
              if(parseFloat(maxSalary) != parseFloat(tempmaxSalary)) {
                this.maxSalary.setValue(tempmaxSalary);
                this.resetComputeFields();
              } else {
                this.maxSalary.setValue(maxSalary);
              }
            } else {
              this.maxSalary.setValue(maxSalary);
            }
          }
          
          if(this.salaryFitmentStackResponse != null) {
            this.currency.setValue(response.response.currency);
            this.maxSalary.disable();
            this.salaryEditType.disable();
          }
          if(this.isResetComputableFields) {
            this.resetComputeFields();
            this.isResetComputableFields = false;
          }
        } else if (messageTO.message != null) {
          // this.notificationService.setMessage(NotificationSeverity.WARNING, messageTO.message.toString());
        } else {
          // this.notificationService.setMessage(NotificationSeverity.WARNING, 'An error occured. Please try again later...');
        }
      }
      this.showloader = false;
    }, error => {
      this.showloader = false;
      // this.notificationService.setMessage(NotificationSeverity.WARNING, 'Could not connect to the server!!! Please try again later...');
    }, () => {
      this.showloader = false
    });

  }

  initFormSecurity() {
    this.fitmentService.getOfferGenerationSecurity().subscribe(
      response => {
        if (response.messageCode.code === 'EC200') {
          this.salarySecurity = JSON.parse(JSON.stringify(response.responseData[0]));
        }
      });
  }

  getMastersData() {
    this.hiringProcessService.getCurrencyMaster(0).subscribe(out => {
      this.currencyList = out.responseData;
      for(let currency of this.currencyList) {
        if(currency.value == 'INR') {
          this.currency.setValue(currency.value);
          break;
        }
      }
    });

    this.hiringProcessService.getPaystructureMaster().subscribe(out => {
      this.paystructureList = out.responseData;

      if(this.isPaystructurePolicyEnabled()) {
        this.paystructure.setValue(this.hiringProcessService.getPaystructureValue().value);
        if(this.hiringProcessService.salaryfitmentDetails != undefined && this.hiringProcessService.salaryfitmentDetails != null) {
          let paystructureCode = this.hiringProcessService.salaryfitmentDetails.paystructure;
          if(paystructureCode != this.hiringProcessService.getPaystructureValue().label) {
            this.resetComputeFields();
            this.isResetComputableFields = true;
          }
        }
      } else {
        
        if(this.hiringProcessService.salaryfitmentDetails != undefined && this.hiringProcessService.salaryfitmentDetails != null) {
          let paystructureCode = this.hiringProcessService.salaryfitmentDetails.paystructure;
          if(this.hiringProcessService.getPaystructureValue() != null && this.hiringProcessService.getPaystructureValue() != undefined) {
            let tempPaystructureLabel = this.hiringProcessService.getPaystructureValue().label;
            if(tempPaystructureLabel != paystructureCode) {
              this.paystructure.setValue(this.hiringProcessService.getPaystructureValue().value);
              this.resetComputeFields();
              this.isResetComputableFields = true;
            } else {
              if(paystructureCode != undefined && paystructureCode != null && this.paystructureList != undefined && this.paystructureList != null) {
                for(let paystructure of this.paystructureList) {
                  if(paystructure.label == paystructureCode) {
                    this.paystructure.setValue(paystructure.value);
                    break;
                  }
                }
              }
            }
          } else {
            if(paystructureCode != undefined && paystructureCode != null && this.paystructureList != undefined && this.paystructureList != null) {
              for(let paystructure of this.paystructureList) {
                if(paystructure.label == paystructureCode) {
                  this.paystructure.setValue(paystructure.value);
                  break;
                }
              }
            }
          }
        }
      }
    });
  }

  salaryEditTypeChange() {
    this.resetComputeFields();
  }

  setEffectiveDate(effectiveDate: Date) {
    let currentDate: Date = new Date();
    currentDate.setHours(0);
    currentDate.setMinutes(0);
    currentDate.setSeconds(0);
    currentDate.setMilliseconds(0);
    this.effectiveDate.setValue(effectiveDate);
    if (effectiveDate < currentDate) {
      this.dateSwitch = !this.dateSwitch;
      this.effectiveDate.setValue(undefined);
      this.notificationService.clear();
      this.i18UtilService.get('common.warning.effectiveDateCurrentDate').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
    }
  }

  decimalValuesKeyPress(event: any) {
    if (event.key) {
      if (event.target.value.indexOf('.') > 0 && event.key == '.') {
        return false;
      }
      if (event.charCode || event.keyCode) {
        if (event.charCode === 46
          || event.keyCode === 8
          || event.keyCode === 46
          || event.keyCode === 37
          || event.keyCode === 39) {
          return true;
        }
      }

      if (event.key != '.' && event.key.replace(/[^0-9]*/, '') == '') {
        return false;
      }
    }
    if (event.charCode === 46 && this.maxSalary.value.indexOf('.') > 0) {
      return false;
    }
    if ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46
      || event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37
      || event.keyCode === 39) {
      return true;
    } else {
      return false;
    }
  }

  updateAmountAdjustments(event: any, payStructureCodeId: any, columnType: any) {
    if (event.target) {
      if (event.target.value == null || event.target.value == '') {
        event.target.value = '0';
      }
    }
    this.showloader = true;
    this.fitmentService.updateAmountAdjustments(this.salaryFitmentStackResponse, this.salaryEditType.value,
      this.maxSalary.value, this.fbp, payStructureCodeId, columnType, event.target.value).subscribe(response => {
        this.showloader = false;
        this.salaryFitmentStackResponse = undefined;
        this.guaranteedCTC = undefined;
        this.nonGuaranteedCTC = undefined;
        this.totalAmtMon = undefined;
        this.totalAmtYr = undefined;
        this.totalAmtMonthWithoutHidden = undefined;
        let messageTO: MessageCode = response.messageCode;
        if (messageTO != null) {
          if (messageTO.code != null && messageTO.code === 'EC200') {
            this.salaryFitmentStackResponse = response.response.groupList;
            this.guaranteedCTC = response.response.guaranteedCTC;
            this.nonGuaranteedCTC = response.response.nonGuaranteedCTC;
            this.totalAmtMon = response.response.totalAmtMon;
            this.totalAmtYr = response.response.totalAmtYr;
            this.totalAmtMonthWithoutHidden = response.response.totalAmtMonthWithoutHidden;

            let statusMessages = response.response.statusMessages;
            if (statusMessages != null && statusMessages.length > 0) {
              for (let message of statusMessages) {
                if (message.code == 'EC203') {
                  this.notificationService.addMessage(NotificationSeverity.WARNING, message.description);
                }
              }
            }
          } else if (messageTO.message != null) {
            this.notificationService.setMessage(NotificationSeverity.WARNING, messageTO.message.toString());
          } else {
            this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        }
      }, error => {
        this.showloader = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
  }

  resetComputeFields() {
    this.effectiveDate.reset();
    this.guaranteedCTC = undefined;
    this.nonGuaranteedCTC = undefined;
    this.salaryFitmentStackResponse = undefined;
    this.costToCompanyStackVisible = false;
    this.salaryEditType.enable();
  }

  compute() {
    this.showloader = true;
    this.notificationService.clear();
    let valid: boolean = true;

    if (this.currency.value == null) {
      this.i18UtilService.get('candidate.warning.currencyRequired').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.paystructure.value == null) {
      this.i18UtilService.get('candidate.warning.payStrRequired').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
      
    }

    if (this.effectiveDate.value == null) {
      this.i18UtilService.get('candidate.warning.effectiveFromDateReq').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.salaryEditType.value == 'FORMULA BASED' && (this.maxSalary.value == null || this.maxSalary.value == '')) {
      this.i18UtilService.get('candidate.warning.ctcReq').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (valid == false) {
      this.showloader = false;
      return;
    }
    else {
      this.fitmentService.computeSalaryFitmentStack(this.applicantStatusID, this.salaryEditType.value,
        this.currency.value, this.paystructure.value, this.maxSalary.value, this.effectiveDate.value,
        this.fbp).subscribe(response => {
          this.salaryFitmentStackResponse = undefined;
          this.guaranteedCTC = undefined;
          this.nonGuaranteedCTC = undefined;
          this.totalAmtMon = undefined;
          this.totalAmtYr = undefined;
          this.totalAmtMonthWithoutHidden = undefined;
          let messageTO: MessageCode = response.messageCode;
          if (messageTO != null) {
            if (messageTO.code != null && messageTO.code === 'EC200') {
              this.salaryFitmentStackResponse = response.response.groupList;
              this.guaranteedCTC = response.response.guaranteedCTC;
              this.nonGuaranteedCTC = response.response.nonGuaranteedCTC;
              this.totalAmtMon = response.response.totalAmtMon;
              this.totalAmtYr = response.response.totalAmtYr;
              this.totalAmtMonthWithoutHidden = response.response.totalAmtMonthWithoutHidden;
              this.totalCTCAnnotate = response.response.totalCTCAnnotate;
              this.costToCompanyStackVisible = true;
              this.variableSectionVisible = true;
              this.maxSalary.disable();
              this.salaryEditType.disable();
            } else if (messageTO.message != null) {
              this.notificationService.setMessage(NotificationSeverity.WARNING, messageTO.message.toString());
            } else {
              this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            }
          }
          this.showloader = false;
        }, error => {
          this.showloader = false;
          this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }, () => {
          this.showloader = false
        });
    }
  }

  validateComputableStackFields(valid: boolean): boolean {

    if (this.salaryFitmentStackResponse == null) {
      this.i18UtilService.get('candidate.warning.createSalaryStack').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    return valid;
  }

  setSaveCTCRequestJSON(): SaveCTCRequestJSON {
    let saveCTCRequestJSON = new SaveCTCRequestJSON();
    
    saveCTCRequestJSON.groupList= this.salaryFitmentStackResponse;
    saveCTCRequestJSON.applicantStatusId= this.applicantStatusID;
    saveCTCRequestJSON.currencyCode= this.currency.value;
    saveCTCRequestJSON.payStructureId= this.paystructure.value;
    saveCTCRequestJSON.effectiveFrom= this.effectiveDate.value;
    saveCTCRequestJSON.guaranteedCTCAmt= this.guaranteedCTC;
    saveCTCRequestJSON.nonGuaranteedCTCAmt= this.nonGuaranteedCTC;
    saveCTCRequestJSON.formulaEdit= this.salaryEditType.value == 'FORMULA BASED';
    saveCTCRequestJSON.freeTextEdit= this.salaryEditType.value == 'FREE TEXT';
    saveCTCRequestJSON.totalAmtMon = this.totalAmtMon;
    saveCTCRequestJSON.totalAmtYr= this.totalAmtYr;
    saveCTCRequestJSON.totalAmtLabel = this.salarySecurity.totalCtcAddLabel+' '+this.totalCTCAnnotate;
    saveCTCRequestJSON.totalCTCAmt= this.maxSalary.value;
    saveCTCRequestJSON.guaranteedAmtYr= this.guaranteedCTC;
    saveCTCRequestJSON.nonGuaranteedAmtYr= this.nonGuaranteedCTC;
    saveCTCRequestJSON.totalAmtMonthWithoutHidden= this.totalAmtMonthWithoutHidden;
    saveCTCRequestJSON.totalAmtMonthWithoutHiddenLabel= this.salarySecurity.grossHiddenMonthlyLabel;

    return saveCTCRequestJSON;
  }

  isPaystructurePolicyEnabled() {
    return this.hiringProcessService.isPaystructurePolicyEnabled();
  }
}

export class SaveBasicSalary {
  input: FitmentBasicDetailsTo;
}
