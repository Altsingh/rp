import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FitmentService } from '../fitment.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FitmentBasicDetailsTo } from 'app/common/fitment-basic-details-to';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { SelectItem } from 'app/shared/select-item';
import { HiringProcessService } from 'app/candidate/candidate-tagged-detail/hiring-process/hiring-process.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-fitment-basic',
  templateUrl: './fitment-basic.component.html',
  styleUrls: ['./fitment-basic.component.css'],
  providers: [FitmentService]
})
export class FitmentBasicComponent implements OnInit {

  saveBasicSalary: SaveBasicSalary;
  fitmentForm: FormGroup;
  optionsList: SelectItem[];
  _lastIncrementDate: Date;
  _dateOfJoining: Date;
  currentDate : Date;
  fitment: boolean = false;
  basicLabel  : String ;
  prevOrgDetailsRequired: boolean= true;


  @Input() applicantStatusID: number;

  @Input() security: any;

  @Output() toggle: EventEmitter<any> = new EventEmitter();

  @Input() toggleOpen: boolean;

  fitmentBasicDetailsTo: FitmentBasicDetailsTo = new FitmentBasicDetailsTo();

  constructor(private fitmentService: FitmentService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private hiringProcessService: HiringProcessService,
    private i18UtilService: I18UtilService) {


    this.currentDate = new Date(Date.now());
    this.optionsList = [];
    this.optionsList.push(new SelectItem('Yes', 1));
    this.optionsList.push(new SelectItem('No', 0));

    this.fitmentForm = this.formBuilder.group({
      gender: new FormControl({value: '', disabled: true}),
      dateOfBirth: new FormControl({value: '', disabled: true}),
      aadhaarNumber: new FormControl({value: '', disabled: true}),
      hiringManagerName: new FormControl({value: '', disabled: true}),
      joiningLocation: new FormControl({value: '', disabled: true}),
      orgUnitCode: new FormControl({value: '', disabled: true}),
      jobDesignation: new FormControl({value: '', disabled: true}),
      band: new FormControl({value: '', disabled: true}),
      jobFamilyCode: new FormControl({value: '', disabled: true}),
      jobFamily: new FormControl({value: '', disabled: true}),
      requisitionType: new FormControl({value: '', disabled: true}),
      companyName: '',
      totalExperience: '',
      tenureWithCurrentOrganization:'',
      candidateDesignation: '',
      lastIncrementDate: new FormControl(),
      lastIncrementAmount: '',
      currentProfile: '',
      highestEducation: '',
      address: '',
      nationality: '',
      employedWithSameOrg: '',
      previousOrgDetails: '',
      dateOfJoining: new FormControl()
    });
  }

  ngOnInit() {
    this.getBasicSalaryDetails();
    this.i18UtilService.get('common.label.save&Proceed').subscribe((res: string) => {
     this.basicLabel=res;
    });
  }

  checkForDetails(value : any)
  {
    if(value == "1")
    {
      this.prevOrgDetailsRequired = true;
      this.security.basicPreviousOrgDetailsRequired = true;
      this.security.basicPreviousOrgDetailsRendered = true;
    }
    else if(value == "0")
    {
      this.prevOrgDetailsRequired = false;
      this.security.basicPreviousOrgDetailsRequired = false;
      this.security.basicPreviousOrgDetailsRendered = false;
    }
  }

  getBasicSalaryDetails() {
    if(this.hiringProcessService.salaryfitmentDetails != undefined && this.hiringProcessService.salaryfitmentDetails != null) {
      
      let responseData = this.hiringProcessService.salaryfitmentDetails;
      
      this.fitmentForm.controls['gender'].setValue(responseData.gender);
      this.fitmentForm.controls['aadhaarNumber'].setValue(responseData.aadhaarNumber);
      this.fitmentForm.controls['dateOfBirth'].setValue(responseData.dateOfBirth);
      this.fitmentForm.controls['joiningLocation'].setValue(responseData.joiningLocation);
      this.fitmentForm.controls['orgUnitCode'].setValue(responseData.orgUnitCode);
      this.fitmentForm.controls['jobDesignation'].setValue(responseData.jobDesignation);
      this.fitmentForm.controls['band'].setValue(responseData.band);
      this.fitmentForm.controls['jobFamily'].setValue(responseData.jobFamily);
      this.fitmentForm.controls['jobFamilyCode'].setValue(responseData.jobFamilyCode);
      this.fitmentForm.controls['requisitionType'].setValue(responseData.requisitionType);
      this.fitmentForm.controls['hiringManagerName'].setValue(responseData.hiringManagerName);
      this.fitmentForm.controls['companyName'].setValue(responseData.companyName);
      this.fitmentForm.controls['totalExperience'].setValue(responseData.totalExperience);
      this.fitmentForm.controls['tenureWithCurrentOrganization'].setValue(responseData.tenurePeriod);
      this.fitmentForm.controls['candidateDesignation'].setValue(responseData.proposedDesignation);
      if (responseData.lastIncrementDate != null) {
        this.setLastIncrementValue(new Date(responseData.lastIncrementDate));
      }
      this.fitmentForm.controls['lastIncrementAmount'].setValue(responseData.lastIncrementAmount);
      this.fitmentForm.controls['currentProfile'].setValue(responseData.currentProfile);
      this.fitmentForm.controls['highestEducation'].setValue(responseData.highestEducation);
      this.fitmentForm.controls['address'].setValue(responseData.residenceAddress);
      this.fitmentForm.controls['nationality'].setValue(responseData.nationality);
      this.fitmentForm.controls['employedWithSameOrg'].setValue(responseData.employedWithSameOrg);
      this.fitmentForm.controls['previousOrgDetails'].setValue(responseData.previousOrgDetails);
      if (responseData.actualDOJ != null) {
        this.setDateOfJoining(new Date(responseData.actualDOJ));
      }
      if(responseData.employedWithSameOrg==null || responseData.employedWithSameOrg == false)
      {
        this.prevOrgDetailsRequired = false;
      }
      if(responseData.employedWithSameOrg == true)
      {
        this.prevOrgDetailsRequired = true;
      }
      
      this.setSaveSalaryFitmentDetailsRequestObject(this.saveBasicSalary);
    
    }
  }

  toggleDiv() {
    this.toggleOpen = !this.toggleOpen;
  }

  validateBasicFields(valid: boolean): boolean {

    if (this.security.basicGenderRendered && this.security.basicGenderRequired &&
      (this.fitmentForm.controls.gender.value == null || this.fitmentForm.controls.gender.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicGenderLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicCompanyNameRendered && this.security.basicCompanyNameRequired &&
      (this.fitmentForm.controls.companyName.value == null || this.fitmentForm.controls.companyName.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicCompanyNameLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicTotalExperienceRendered && this.security.basicTotalExperienceRequired &&
      (this.fitmentForm.controls.totalExperience.value == null || this.fitmentForm.controls.totalExperience.value == '') && this.fitmentForm.controls.lastIncrementAmount.value != 0) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicTotalExperienceLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicTenureWithCurrentOrganizationRendered && this.security.basicTenureWithCurrentOrganizationRequired && 
      (this.fitmentForm.controls.tenureWithCurrentOrganization == null || this.fitmentForm.controls.tenureWithCurrentOrganization.value == '') && this.fitmentForm.controls.lastIncrementAmount.value != 0) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicTenureWithCurrentOrganizationLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }


    if (this.security.basicCandidateDesignationRendered && this.security.basicCandidateDesignationRequired &&
      (this.fitmentForm.controls.candidateDesignation.value == null || this.fitmentForm.controls.candidateDesignation.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicCandidateDesignationLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }
    if (this.security.basicLastIncrementDateRendered && this.security.basicLastIncrementDateRequired &&
      (this.fitmentForm.controls.lastIncrementDate.value == null || this.fitmentForm.controls.lastIncrementDate.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicLastIncrementDateLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicLastIncrementDateRendered  && this.fitmentForm.controls.lastIncrementDate.value != null && 
       this.fitmentForm.controls.lastIncrementDate.value > new Date()) {
        this.i18UtilService.get('common.warning.lastIncrementDateError',{'label': this.security.basicLastIncrementDateLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
   }
    
    if (this.security.basicLastIncrementAmountRendered && this.security.basicLastIncrementAmountRequired &&
      (this.fitmentForm.controls.lastIncrementAmount.value == null || this.fitmentForm.controls.lastIncrementAmount.value == '') && this.fitmentForm.controls.lastIncrementAmount.value != 0) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicLastIncrementAmountLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicCurrentProfileRendered && this.security.basicCurrentProfileRequired &&
      (this.fitmentForm.controls.currentProfile.value == null || this.fitmentForm.controls.currentProfile.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicCurrentProfileLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }


    if (this.security.basicHighestEducationRendered && this.security.basicHighestEducationRequired &&
      (this.fitmentForm.controls.highestEducation.value == null || this.fitmentForm.controls.highestEducation.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicHighestEducationLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }
    if (this.security.basicAadharNumberRendered && this.security.basicAadharNumberRequired &&
      (this.fitmentForm.controls.aadhaarNumber.value == null || this.fitmentForm.controls.aadhaarNumber.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':  this.security.basicAadharNumberLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicDateOfBirthRendered && this.security.basicDateOfBirthRequired &&
      (this.fitmentForm.controls.dateOfBirth.value == null || this.fitmentForm.controls.dateOfBirth.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':  this.security.basicDateOfBirthLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicAddressRendered && this.security.basicAddressRequired &&
      (this.fitmentForm.controls.address.value == null || this.fitmentForm.controls.address.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicAddressLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicNationalityRendered && this.security.basicNationalityRequired &&
      (this.fitmentForm.controls.nationality.value == null || this.fitmentForm.controls.nationality.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicNationalityLabel  }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }
    
    if (this.security.basicPreviousOrgDetailsRendered && this.security.basicPreviousOrgDetailsRequired && this.prevOrgDetailsRequired &&
      (this.fitmentForm.controls.previousOrgDetails.value == null || this.fitmentForm.controls.previousOrgDetails.value == '' || this.fitmentForm.controls.previousOrgDetails.value.trim().length === 0) )
   {
    this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicPreviousOrgDetailsLabel  }).subscribe((res: string) => {
      this.notificationService.addMessage(NotificationSeverity.WARNING, res);
    });
      valid = false;

    }

    if (this.security.basicJobLocationRendered && this.security.basicJobLocationRequired &&
      (this.fitmentForm.controls.joiningLocation.value == null || this.fitmentForm.controls.joiningLocation.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':  this.security.basicJobLocationLabel   }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicEmployedWithSameOrgRendered && this.security.basicEmployedWithSameOrgRequired && this.fitmentForm.controls.employedWithSameOrg.value != 0 &&
      (this.fitmentForm.controls.employedWithSameOrg.value == null || this.fitmentForm.controls.employedWithSameOrg.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':  this.security.basicEmployedWithSameOrgLabel  }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicBusinessUnitRendered && this.security.basicBusinessUnitRequired &&
      (this.fitmentForm.controls.orgUnitCode.value == null || this.fitmentForm.controls.orgUnitCode.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':  this.security.basicBusinessUnitLabel   }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicJobDesignationRendered && this.security.basicJobDesignationRequired &&
      (this.fitmentForm.controls.jobDesignation.value == null || this.fitmentForm.controls.jobDesignation.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicJobDesignationLabel   }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicJobFamilyCodeRendered && this.security.basicJobFamilyCodeRequired &&
      (this.fitmentForm.controls.jobFamilyCode.value == null || this.fitmentForm.controls.jobFamilyCode.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicJobFamilyCodeLabel    }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicJobFamilyRendered && this.security.basicJobFamilyRequired &&
      (this.fitmentForm.controls.jobFamily.value == null || this.fitmentForm.controls.jobFamily.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicJobFamilyLabel    }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicRequisitionTypeRendered && this.security.basicRequisitionTypeRequired &&
      (this.fitmentForm.controls.requisitionType.value == null || this.fitmentForm.controls.requisitionType.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label':   this.security.basicRequisitionTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;

    }

    if (this.security.basicHiringManagerRendered && this.security.basicHiringManagerRequired &&
      (this.fitmentForm.controls.hiringManagerName.value == null || this.fitmentForm.controls.hiringManagerName.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicHiringManagerLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }

    if (this.security.basicDateOfJoiningRendered && this.security.basicDateOfJoiningRequired &&
      (this.fitmentForm.controls.dateOfJoining.value == null || this.fitmentForm.controls.dateOfJoining.value == '')) {
        this.i18UtilService.get('common.warning.lablelRequiredCommon',{'label': this.security.basicDateOfJoiningLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      valid = false;
    }
    if(Number(this.fitmentForm.controls.tenureWithCurrentOrganization.value) > Number(this.fitmentForm.controls.totalExperience.value))
    {
      this.i18UtilService.get('candidate.warning.currentOrgExpExceeded',{'currentOrgExp': this.security.basicTenureWithCurrentOrganizationLabel, 'totalExp':  this.security.basicTotalExperienceLabel  }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid=false;
    }

    return valid;
  }

  setSaveSalaryFitmentDetailsRequestObject(saveBasicSalary: SaveBasicSalary): SaveBasicSalary {
    if(saveBasicSalary == null) {
      saveBasicSalary = new SaveBasicSalary();
    }
    if(saveBasicSalary.input == null) {
      saveBasicSalary.input = new FitmentBasicDetailsTo();
    }
    saveBasicSalary.input.companyName = (this.fitmentForm.controls.companyName.value);
    saveBasicSalary.input.totalExperience = (this.fitmentForm.controls.totalExperience.value);
    saveBasicSalary.input.tenureWithCurrentOrganization = (this.fitmentForm.controls.tenureWithCurrentOrganization.value);
    saveBasicSalary.input.candidateDesignation = (this.fitmentForm.controls.candidateDesignation.value);
    saveBasicSalary.input.lastIncrementDate = (this.fitmentForm.controls.lastIncrementDate.value);
    saveBasicSalary.input.lastIncrementAmount = (this.fitmentForm.controls.lastIncrementAmount.value);
    saveBasicSalary.input.currentProfile = (this.fitmentForm.controls.currentProfile.value);
    saveBasicSalary.input.highestEducation = (this.fitmentForm.controls.highestEducation.value);
    saveBasicSalary.input.address = (this.fitmentForm.controls.address.value);
    saveBasicSalary.input.nationality = (this.fitmentForm.controls.nationality.value);
    saveBasicSalary.input.employedWithSameOrg = (this.fitmentForm.controls.employedWithSameOrg.value);
    saveBasicSalary.input.previousOrgDetails = (this.fitmentForm.controls.previousOrgDetails.value);
    saveBasicSalary.input.hiringManagerName = (this.fitmentForm.controls.hiringManagerName.value);
    saveBasicSalary.input.dateOfJoining = (this.fitmentForm.controls.dateOfJoining.value);
    saveBasicSalary.input.sectionName = "basic";
    this.saveBasicSalary = saveBasicSalary;

    return saveBasicSalary;
  }

  saveBasicDetails() {
    this.notificationService.clear();
    this.fitment = true;

    let valid = this.validateBasicFields(true);

    if (valid == true) {
      
      this.setSaveSalaryFitmentDetailsRequestObject(this.saveBasicSalary);

      this.fitmentService.saveSalaryFitmentDetails(this.saveBasicSalary, this.applicantStatusID).subscribe(response => {
        if (response != null && response.messageCode != null && response.messageCode.code != null) {
          if (response.messageCode.code == "EC200") {
            this.fitment = false;
            if(response.messageCode.message!=null){
              this.notificationService.setMessage(NotificationSeverity.INFO, response.messageCode.message);  
            }else{
              this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.INFO, res);
              });
            }
            this.toggle.emit(true);
            this.toggleOpen = false;

          }
          else if (response.messageCode.code == "EC201") {
            this.fitment = false;
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else {
            this.fitment = false;
            this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        }
      });
    } else {
      this.fitment = false;
    }
  }


  setLastIncrementValue(date: Date) {
    this.fitmentForm.controls['lastIncrementDate'].setValue(date);
    this._lastIncrementDate = date;
  }

  setDateOfJoining(date: Date) {
    this.fitmentForm.controls['dateOfJoining'].setValue(date);
    this._dateOfJoining = date;
  }

  decimalValuesKeyPress(event: any) {
    if (event.key) {

      if(event.target.value.length > 10) return false;

      if (event.target.value.indexOf('.') > 0 && event.key == '.') {
        return false;
      }
      if (event.charCode) {
        if (event.charCode === 46
          || event.keyCode === 8
          || event.keyCode === 37
          || event.keyCode === 39) {
          return true;
        }
      }

      if (event.key != '.' && event.key.replace(/[^0-9]*/, "") == '') {
        return false;
      }
    }

    if ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46
      || event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37
      || event.keyCode === 39) {
      return true;
    } else {
      return false;
    }
  }


}


export class SaveBasicSalary {
  input: FitmentBasicDetailsTo;
}
