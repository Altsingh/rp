import { Component, Input, Output, OnInit, EventEmitter, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { FormControl } from '@angular/forms';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { SelectItem } from '../../../../shared/select-item';
import { HiringProcessService } from '../hiring-process.service';
import { CurrencyMaster } from './../../../../common/currency-master';
import { FitmentService } from './fitment.service';
import { MessageCode } from '../../../../common/message-code';
import { DatePipe } from '@angular/common';
import { ApplicantPaystructureDetailsTo } from 'app/common/applicant-paystructure-details-to';
import { FitmentBasicComponent, SaveBasicSalary } from './fitment-basic/fitment-basic.component';
import { FitmentParametersComponent } from './fitment-parameters/fitment-parameters.component';
import { FitmentProposedRemunerationComponent } from './fitment-proposed-remuneration/fitment-proposed-remuneration.component';
import { PaystructureInstalments } from './paystructure-instalments.model';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-hiring-process-fitment',
  templateUrl: './fitment.component.html',
  styleUrls: ['./fitment.component.css'],
  providers: [FitmentService],

})
export class FitmentComponent implements OnInit {
  salaryStackResponse: any;
  label: string;
  showloader: boolean = false;
  fitment: boolean = false;
  ctcSaveButtonLabel: string = 'Save';
  ctcDetailSaving: boolean = false;
  ctcDetailSaved: boolean = false;
  basicToggle: boolean = false;
  paramToggle: boolean = false;
  renumToggle: boolean = false;
  jobStatus: boolean = true;
  
  @ViewChild(FitmentBasicComponent)
  FITMENTBASIC: FitmentBasicComponent;

  @ViewChild(FitmentParametersComponent)
  FITMENTPARAMETERS: FitmentParametersComponent;

  @ViewChild(FitmentProposedRemunerationComponent)
  FITMENTRENUMERATION: FitmentProposedRemunerationComponent;

  @Input() statusLists: StageActionListMaster[];

  @Input() workflowID: number;

  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();

  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }
  @Input() set spinner(value: boolean) {
    this.fitment = false;
  }

  @Input() inputData: HiringProcessResponse;

  @Input() applicantStatusID: number;

  @Input() set setSecurity(value: any) {
    this.security = value;
    if (this.security != null) {
      if (this.security.basicDetailsRendered) {
        this.basicToggle = true;
        this.paramToggle = false;
        this.renumToggle = false;
      } else if (this.security.fitmentParametersRendered) {
        this.basicToggle = false;
        this.paramToggle = true;
        this.renumToggle = false;
      } else if (this.security.proposedRenumerationRendered) {
        this.basicToggle = false;
        this.paramToggle = false;
        this.renumToggle = true;
      }

      if (this.security.fitmentOfferedAnnualGrossRendered && !this.security.fitmentOfferedAnnualGrossDisabled) {
        this.security.fitmentOfferedVariablePayDisabled = true;
        this.security.fitmentOfferedFixedPayDisabled = true;
      }

      if (this.security.fitmentCurrentAnnualGrossRendered && !this.security.fitmentCurrentAnnualGrossDisabled) {
        this.security.fitmentCurrentVariablePayDisabled = true;
        this.security.fitmentCurrentFixedPayDisabled = true;
      }
    }
  };

  @Output() initSecurity: EventEmitter<any> = new EventEmitter();

  toggleOpen: boolean;

  security: any;
  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  selectedStatus: string = '';
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;

  applicantPaystructureDetailsList: any;
  otherExpensesList: any = [];
  stackList: any = [];

  _fitment_getPaystructureByGroupPolicy:boolean = false;
  _fitment_getSalaryFitmentDetails:boolean = false;
  _error_message: string;
  _show_error_message: boolean = false;
  
  constructor(private notificationService: NotificationService, private hiringProcessService: HiringProcessService,
    private fitmentService: FitmentService, private i18UtilService: I18UtilService) {

    this._fitment_getPaystructureByGroupPolicy = false;
    this._fitment_getSalaryFitmentDetails = false;
    this._error_message = undefined;
    this._show_error_message = false;
    this.hiringProcessService.setPaystructurePolicyEnabled(false);

    Promise.resolve(this.applicantStatusID).then(res => {
      if (this.inputData.sysWorkflowstageActionType == 'CREATE') {
        this.getPolicyConfig();
      }
      this.getApplicantPaystructureDetails();
      this.getSalaryFitmentDetails();
    });
  }

  ngOnInit() {
    this.hiringProcessService.isJobStatus().subscribe(out => this.jobStatus = out);
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.label = res;
    });
  }

  getPolicyConfig() {
    if(!this._fitment_getPaystructureByGroupPolicy) {
      this.hiringProcessService.getPolicyConfiguration().subscribe((response) => {
        for (let policy of response) {
          if (policy == 'PAY-STRUCTURE-POLICY') {
            this.hiringProcessService.setPaystructurePolicyEnabled(true);
            break;
          }
        }
        if (this.hiringProcessService.isPaystructurePolicyEnabled()) {
          this.getPaystructureByGroupPolicy(this.applicantStatusID);
        } else {
          this._fitment_getPaystructureByGroupPolicy = true;
        }
      });
    }
  }

  getPaystructureByGroupPolicy(applicantStatusID: number) {
    this.hiringProcessService.getPaystructureByGroupPolicy(applicantStatusID).subscribe((response) => {
      if (response != undefined && response != null) {
        let messageTO: MessageCode = response.messageCode;
        if (messageTO != undefined && messageTO != null && messageTO.code != null) {
          if(messageTO.code === 'EC200') {
            let paystructureFromPolicy: SelectItem = new SelectItem(response.responseData.paystructureName,response.responseData.paystructureID);
            this.hiringProcessService.setPaystructureValue(paystructureFromPolicy);
            this._fitment_getPaystructureByGroupPolicy = true;
          } else if(messageTO.code === 'EC202') {
            this._show_error_message = true;
            this._error_message = 'Paystructure mapping missing. Please contact your system administrator';
            this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
          } else if(messageTO.code === 'EC203') {
            this._show_error_message = true;
            this._error_message = 'Paystructure mapping missing. Please contact your system administrator';
            this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
          }
        }
      }
    }, (onError) => {
      this._show_error_message = true;
      this._error_message = 'Paystructure mapping missing. Please contact your system administrator.';
      this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
    }, () => {
    });
  }

  getSalaryFitmentDetails() {
    this.fitmentService.getSalaryFitmentDetails(this.applicantStatusID).subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          this.hiringProcessService.salaryfitmentDetails = response.responseData[0];
          this._fitment_getSalaryFitmentDetails = true;
          console.log(this._fitment_getSalaryFitmentDetails)
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, 'Internal Server Error occured while fetching salary fitment details.');    
        }
      }
    }, (onError) => {
      this.notificationService.setMessage(NotificationSeverity.WARNING, 'Error occured while fetching salary fitment details.');
    }, () => {
    });
  }

  getApplicantPaystructureDetails() {
    this.fitmentService.getApplicantPaystructureDetails(this.applicantStatusID).subscribe(out => {
      this.applicantPaystructureDetailsList = out.responseData;
      if (this.applicantPaystructureDetailsList != null) {
        for (let comp of this.applicantPaystructureDetailsList) {
          if (comp.paystructureType == 'Offered_Stack') {
            this.stackList.push(comp);
          }
        }
      }

      let resultList: Array<ApplicantPaystructureDetailsTo> = [];
      this.fitmentService.getAllPayCodeForOthers('Others').subscribe(response => {
        let messageTO: MessageCode = response.messageCode;
        let message: string = '';
        if (messageTO != null) {
          if (messageTO.code != null && messageTO.code === 'EC200') {
            let exists = false;
            if (response.responseData != null) {
              for (let other of response.responseData) {
                exists = false;
                if (this.applicantPaystructureDetailsList != null) {
                  for (let comp of this.applicantPaystructureDetailsList) {
                    if (comp.paystructureType == 'Other_Expenses' && comp.sysPayCode == other.sysPayCode) {
                      exists = true;
                      comp.toBeGiven = true;
                      resultList.push(comp);
                      break;
                    }
                  }
                }
                if (!exists) {

                  let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                  // result.amount = '';
                  result.payCodeName = other.payCodeName;
                  result.sysPayCode = other.sysPayCode;
                  result.frequency = other.frequency;
                  result.paystructureType = 'Other_Expenses';
                  result.amount = '';
                  let detailsInstalments: Array<PaystructureInstalments> = new Array<PaystructureInstalments>();
                  result.detailsInstalments = detailsInstalments;
                  result.toBeGiven = false;
                  resultList.push(result);
                }
              }
            }
            this.otherExpensesList = resultList;
          }
        }
      });
    });
  }

  updateStatus() {

    this.notificationService.clear();

    if(this._show_error_message) {
      this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
      return;
    }

    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;

    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
      return;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.attachmentLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
      return;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == "")) {
      this.i18UtilService.get('common.warning.enterLabelCommon', { 'label': this.security.commentLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
      return;
    }

    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.attachmentLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.commentRendered && this.security.commentRequired && this.updateStatusRequestTO.comment == null) {
      this.i18UtilService.get('common.warning.enterLabelCommon', { 'label': this.security.commentLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }

    if (this.selectedStatus.toLocaleLowerCase().includes('create')
      || this.selectedStatus.toLocaleLowerCase().includes('modify')) {

      let valid: boolean = true;
      if (this.security.basicDetailsRendered) {
        valid = this.FITMENTBASIC.validateBasicFields(valid);
      }
      if (this.security.fitmentParametersRendered) {
        valid = this.FITMENTPARAMETERS.validateParametersFields(valid);
      }
      if (this.security.proposedRenumerationRendered) {
        valid = this.FITMENTRENUMERATION.validateProposedRenumerationFields(valid);
      }

      if (valid) {
        if (this.security.salaryStackRendered) {
          this.ctcDetailSaving = true;
          this.ctcSaveButtonLabel = 'Saving';

          let requestJSON = this.FITMENTRENUMERATION.setSaveCTCRequestJSON();

          Promise.resolve(this.fitmentService.saveCTCDetails(requestJSON).toPromise()).then((response) => {
            this.fitment = true;
            this.ctcDetailSaving = false;
            let success = false;

            if (response) {
              if (response.messageCode) {
                if (response.messageCode.code) {
                  if (response.messageCode.code == 'EC200') {
                    let resultList: Array<ApplicantPaystructureDetailsTo> = [];

                    if (requestJSON.groupList != null) {
                      for (let res of requestJSON.groupList) {
                        if (res.payComponent != null) {
                          for (let payComponent of res.payComponent) {
                            let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                            result.amount = payComponent.monthCtcAmount;
                            result.annualAmount = payComponent.yearCtcAmount;
                            result.payCodeName = payComponent.payComponentName;
                            // result.sysPayCode = payComponent.payComponentName;
                            result.paystructureType = 'Offered_Stack_Group';
                            resultList.push(result);
                            for (let compBreakUp of payComponent.compBreakUp) {
                              let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                              result.amount = compBreakUp.monthlyAmount;
                              result.annualAmount = compBreakUp.annualAmount;
                              result.payCodeName = compBreakUp.payCode;
                              result.sysPayCode = compBreakUp.sysPayCode;
                              result.paystructureType = 'Offered_Stack';
                              result.effectiveDate = requestJSON.effectiveFrom;
                              resultList.push(result);
                            }
                          }
                        } else {
                          let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                          result.amount = res.monthCtcAmount;
                          result.annualAmount = res.yearCtcAmount;
                          result.payCodeName = res.payGroupName;
                          // result.sysPayCode = res.payComponentName;
                          result.paystructureType = 'Offered_Stack_Total';
                          resultList.push(result);
                        }
                      }
                    }

                    let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                    result.amount = requestJSON.totalAmtMon;
                    result.annualAmount = requestJSON.totalAmtYr;
                    result.payCodeName = requestJSON.totalAmtLabel;
                    // result.sysPayCode = requestJSON.totalAmtLabel;
                    result.paystructureType = 'Offered_Stack_Total';
                    resultList.push(result);

                    let result1: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                    result1.amount = requestJSON.totalAmtMonthWithoutHidden;
                    result1.payCodeName = requestJSON.totalAmtMonthWithoutHiddenLabel;
                    // result1.sysPayCode = requestJSON.totalAmtMonthWithoutHiddenLabel;
                    result1.paystructureType = 'Offered_Stack_Total';
                    resultList.push(result1);

                    this.FITMENTRENUMERATION.stackList = resultList;

                    this.savePostSalaryStackCalculation();

                  } else if (response.messageCode.code == 'EC201') {
                    if (response.response.errorMessages) {
                      if (response.response.errorMessages.length > 0) {
                        for (let message of response.response.errorMessages) {
                          this.notificationService.addMessage(NotificationSeverity.WARNING, message.description);
                        }
                        this.fitment = false;
                      } else {
                        this.fitment = false;
                        this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.description);
                      }
                    } else {
                      this.fitment = false;
                      this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.description);
                    }

                  } else if (response.messageCode.code == 'EC204') {
                    this.fitment = false;
                    this.i18UtilService.get('candidate.warning.approverNotFound').subscribe((res: string) => {
                      this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                    });
                  }
                }
              }
            }
          },
            (failure) => {
              this.fitment = false;
              this.ctcDetailSaving = false;
              this.ctcSaveButtonLabel = 'Save';
              this.ctcDetailSaved = false;
              this.i18UtilService.get('candidate.error.ctcSaveFail').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            });
        } else if (this.security.customSalaryStackRendered) {
          this.fitment = true;
          this.fitmentService.fetchPayReviewStack(
            this.applicantStatusID,
            this.FITMENTPARAMETERS.candidateName,
            (this.FITMENTPARAMETERS.parameterForm.controls.offeredFixedPay.value),
            this.FITMENTPARAMETERS.orgUnitCode,
            this.FITMENTPARAMETERS.joiningLocation,
            this.FITMENTPARAMETERS.gradeCode,
            this.FITMENTPARAMETERS.band,
            this.FITMENTPARAMETERS.parameterForm.controls.paystructure.value,
            this.FITMENTBASIC.fitmentForm.controls.jobDesignation.value
          ).subscribe(payreviewResponse => {
            if (payreviewResponse != null && payreviewResponse.messageCode != null && payreviewResponse.messageCode.code != null) {
              if (payreviewResponse.messageCode.code == "EC200") {
                if (this.security.fitmentOfferedVariablePayDisabled) {
                  this.FITMENTPARAMETERS.parameterForm.controls['offeredVariablePay'].setValue(payreviewResponse.response.employee.variablePay * 12);
                }
                this.FITMENTPARAMETERS.calculateHike();
                let resultList: Array<ApplicantPaystructureDetailsTo> = [];
                for (let res of payreviewResponse.response.salary_structures) {
                  let result: ApplicantPaystructureDetailsTo = new ApplicantPaystructureDetailsTo();
                  result.amount = res.monthly;
                  result.annualAmount = res.yearly;
                  result.payCodeName = res.display_name;
                  result.sysPayCode = res.display_label;
                  result.paystructureType = 'Offered_Stack';
                  resultList.push(result);
                }

                this.FITMENTRENUMERATION.stackList = resultList;

                this.savePostSalaryStackCalculation();

              } else if (payreviewResponse.messageCode.code == "EC201") {
                this.fitment = false;
                this.notificationService.setMessage(NotificationSeverity.WARNING, payreviewResponse.messageCode.message);
              } else {
                this.fitment = false;
                this.i18UtilService.get('candidate.error.salaryGenerationFailed').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                });
              }
            } else {
              this.fitment = false;
              this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            }
          }, (onError) => {
            this.fitment = false;
            this.i18UtilService.get('common.error.somthingWrong').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          });
        }
      }
    } else {
      this.fitment = true;
      this.output.emit(this.updateStatusRequestTO);
    }
  }

  savePostSalaryStackCalculation() {
    let saveBasicSalary: SaveBasicSalary = this.setSaveBasicSalary();
    saveBasicSalary.input.sectionName = null;

    this.fitmentService.saveSalaryFitmentDetails(saveBasicSalary, this.applicantStatusID).subscribe(saveFitmentResponse => {
      if (saveFitmentResponse != null && saveFitmentResponse.messageCode != null && saveFitmentResponse.messageCode.code != null) {
        if (saveFitmentResponse.messageCode.code == 'EC200') {
          Promise.resolve(this.fitmentService.initiateFitmentApprovalWorkflow(this.applicantStatusID, this.workflowID).toPromise()).then((initiateFitmentResponse) => {
            let messageTO: MessageCode = initiateFitmentResponse.messageCode;
            if (messageTO != null) {
              if (messageTO.code != null && messageTO.code === 'EC209') {
                this.updateStatusRequestTO.payStructureCode = saveBasicSalary.input.paystructure;
                for (let status of this.statusLists) {
                  if (status.sysActionType == 'CREATED') {
                    this.updateStatusRequestTO.selectedStatusID = status.value;
                    this.updateStatusRequestTO.updateApplicantPaystructureDetailsHistoryID = true;
                    this.output.emit(this.updateStatusRequestTO);
                  }
                }

              } else if (messageTO.code != null && messageTO.code === 'EC200') {
                this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
                this.updateStatusRequestTO.updateApplicantPaystructureDetailsHistoryID = true;
                this.updateStatusRequestTO.workflowActorList = initiateFitmentResponse.response;
                this.updateStatusRequestTO.payStructureCode = saveBasicSalary.input.paystructure;
                this.updateStatusRequestTO.userWorkflowHistoryData = initiateFitmentResponse.responseData
                this.output.emit(this.updateStatusRequestTO);
              } else if (messageTO.message != null) {
                this.fitment = false;
                this.notificationService.setMessage(NotificationSeverity.WARNING, messageTO.message.toString());
              } else {
                this.fitment = false;
                this.i18UtilService.get('candidate.error.workflowInitializationFailed').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                });
              }
            }
          }, (onError) => {
            this.fitment = false;
            this.i18UtilService.get('candidate.error.workflowInitializationFailed').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          });
        } else if (saveFitmentResponse.messageCode.code == 'EC201') {
          this.fitment = false;
          this.notificationService.setMessage(NotificationSeverity.WARNING, saveFitmentResponse.messageCode.message);
        } else {
          this.fitment = false;
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      } else {
        this.fitment = false;
        this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
    });
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', { 'supportedTypes': supportedTypes.toString() }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  statusChangeListener(event: any) {
    this.selectedStatusID.setValue(event.value);
    this.selectedStatus = event.label;
    this.initSecurity.emit(event.value);
    if(event.sysActionType==='CREATE') {
      this.hiringProcessService.setOfferedAnnualGrossValue(undefined);
      this.hiringProcessService.setPaystructureValue(undefined);
      this.getPolicyConfig();
    }
  }

  setSaveBasicSalary() {
    let saveBasicSalary: SaveBasicSalary = new SaveBasicSalary();
    if (this.security.basicDetailsRendered) {
      saveBasicSalary = this.FITMENTBASIC.setSaveSalaryFitmentDetailsRequestObject(saveBasicSalary);
    }
    if (this.security.fitmentParametersRendered) {
      saveBasicSalary = this.FITMENTPARAMETERS.setSaveSalaryFitmentDetailsRequestObject(saveBasicSalary);
    }
    if (this.security.proposedRenumerationRendered) {
      saveBasicSalary = this.FITMENTRENUMERATION.setSaveSalaryFitmentDetailsRequestObject(saveBasicSalary);
    }
    return saveBasicSalary;
  }
}


export class HrProfileOtherTO {
  hrSalaryCompensationOtherId;
  hrProfileOtherId;
  loggedInUser;
  payCodeId;
  amount;
  payCode;
  payCodeInstruction;
  effectiveDate;
  effectiveDateId;
  frequency;
  valueAssociatedFormula;
  formula;
  oldFormula;
  uniqueFormula;
  userCalculatedAmount;
  systemCalculatedAmount;
  otherDetail: HrProfileOtherDetailTO[];
  completed;
  allowInvalidate;
}

export class HrProfileOtherDetailTO {
  hrProfileOtherDetailId;
  hrProfileOtherId;
  amount;
  month;
  year;
  payoutDateId;
  payoutDate;
  renderDelete;
  monthMap;
  yearMap;
  monthList;
  yearList;
  dateHint;
  active;
  valid;
  processed;
}