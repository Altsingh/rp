import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';
import { FitmentBasicDetailsTo } from 'app/common/fitment-basic-details-to';
import { SaveBasicSalary } from './fitment-basic/fitment-basic.component';

@Injectable()
export class FitmentService {

    constructor(private http: Http, private sessionService: SessionService) { }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
        return headers;
    }

    getSalaryFitmentStack(applicantStatusID) {
        let input: ComputeSalaryFitmentStackRequestInput = new ComputeSalaryFitmentStackRequestInput();
        input.applicantStatusID = applicantStatusID;
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/getSalaryFitmentStack/', JSON.stringify(input), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    getOtherStack(applicantStatusID) {
        let input: ComputeSalaryFitmentStackRequestInput = new ComputeSalaryFitmentStackRequestInput();
        input.applicantStatusID = applicantStatusID;
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/getOtherStack/', JSON.stringify(input), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    computeSalaryFitmentStack(applicantStatusID, salaryEditType, currency, paystructureID,
        maxSalary, effectiveDate, fbp) {
        let input: ComputeSalaryFitmentStackRequestInput = new ComputeSalaryFitmentStackRequestInput();
        input.applicantStatusID = applicantStatusID;
        input.salaryEditType = salaryEditType;
        input.currency = currency;
        input.paystructureID = paystructureID;
        input.maxSalary = maxSalary;
        input.effectiveDate = effectiveDate;
        input.fbp = fbp;
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/computeSalaryFitmentStack/', JSON.stringify(input), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });

    }

    updateAmountAdjustments(groupList: any, salaryEditType: String, maxSalary: String, fbp: Boolean, payStructureCodeId: any, columnType: any, changedAmount: any) {
        let input: UpdateAmountAdjustmentsRequest = new UpdateAmountAdjustmentsRequest();
        input.groupList = groupList;
        input.salaryEditType = salaryEditType;
        input.maxSalary = maxSalary;
        input.fbp = fbp;
        input.payStructureCodeId = payStructureCodeId;
        input.columnType = columnType;
        input.changedAmount = changedAmount;
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/updateAmountAdjustments/', JSON.stringify(input), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });

    }

    otherCompute(paystructureID, payStructure, amount, effectiveDate, frequency, otherPayHeadSummary) {
        let input: OtherComputeRequestInput = new OtherComputeRequestInput();
        input.paystructureID = paystructureID;
        input.payStructure = payStructure;
        input.maxSalary = amount;
        input.effectiveDate = effectiveDate;
        input.frequency = frequency;
        input.otherPayHeadSummary = otherPayHeadSummary;
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/otherCompute/', JSON.stringify(input), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });

    }

    getOfferGenerationSecurity() {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/formSecurity/offerGenerationSecurity/', null, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getSalaryFitmentDetails(applicantStatusID: Number) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/salary/getSalaryFitmentDetails/' + applicantStatusID)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });

    }

    saveSalaryFitmentDetails(saveBasicSalary: SaveBasicSalary, applicantStatusID: Number) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/salary/saveSalaryFitmentDetails/' + applicantStatusID + '/', JSON.stringify(saveBasicSalary), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    saveCTCDetails(requestJSON) {
        let options = new RequestOptions();
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/saveCandidateOffer/', requestJSON, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json();
            });
    }

    initiateFitmentApprovalWorkflow(applicantStatusID, workflowID) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        let request = new InitiateFitmentApprovalWorkflowRequest();
        let input = new InitiateFitmentApprovalWorkflowRequestInput();
        input.applicantStatusID = applicantStatusID;
        input.workflowID=workflowID;
        request.input = input;
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/initiateFitmentApprovalWorkflow/', JSON.stringify(request), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    fetchPayReviewStack(applicantStatusID:any, candidateName:any, offeredAnnualGross:any, 
        orgUnitCode:any, joiningLocation:any, gradeCode:any, band:any, paystructure:any, designation: any) {

        let payReviewCandidateFields: PayReviewCandidateFields = new PayReviewCandidateFields();
        payReviewCandidateFields.employeegradeid = gradeCode;

        let payReviewEmployee: PayReviewEmployee = new PayReviewEmployee();
        payReviewEmployee.employee_code = applicantStatusID;
        payReviewEmployee.employee_name = candidateName;
        payReviewEmployee.salary = offeredAnnualGross;
        payReviewEmployee.location = joiningLocation;
        payReviewEmployee.candidate_fields = payReviewCandidateFields;
        payReviewEmployee.bcsla = orgUnitCode;
        payReviewEmployee.band = band;
        payReviewEmployee.template = paystructure;
        payReviewEmployee.designation = designation;
       
        let payReviewRequest: PayReviewRequest = new PayReviewRequest();
        payReviewRequest.employee = payReviewEmployee;

        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/salary/fetchPayReviewStack/', JSON.stringify(payReviewRequest), options).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    getApplicantPaystructureDetails(applicantStatusID: any) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/salary/getApplicantPaystructureDetails/' + applicantStatusID).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    getApplicantPaystructureDetailsByHistory(applicantStatusID: any, applicantStatusHistoryID: any) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/salary/getApplicantPaystructureDetailsByHistory/' 
        + applicantStatusID + '/' + applicantStatusHistoryID + '/').
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    getAllPayCodeForOthers(payCodeType: String) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/offer/getAllPayCodeTypeForOthers/' + payCodeType).
            map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }

    getPaystructureMaster() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/masters/paystructure/').map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()});
      }
}

export class ComputeSalaryFitmentStackRequestInput {
    applicantStatusID: String;
    salaryEditType: String;
    currency: String;
    paystructureID: String;
    maxSalary: String;
    effectiveDate: Date;
    fbp: Boolean;
}

export class OtherComputeRequestInput {
    paystructureID: String;
    payStructure: String;
    maxSalary: String;
    effectiveDate: Date;
    frequency: any;
    otherPayHeadSummary: any;
}

export class UpdateAmountAdjustmentsRequest {
    groupList: any;
    salaryEditType: String;
    maxSalary: String;
    fbp: Boolean;
    payStructureCodeId: Number;
    columnType: String;
    changedAmount: any;
}

export class InitiateFitmentApprovalWorkflowRequest {
    input: InitiateFitmentApprovalWorkflowRequestInput;
}

export class InitiateFitmentApprovalWorkflowRequestInput {
    applicantStatusID: any;
    workflowID: any;
    userWorkflowHistoryData:any[];
}

export class PayReviewRequest {
    employee: PayReviewEmployee;
}

export class PayReviewEmployee {
    employee_code: any;
    employee_name: any;
    salary: any;
    location: any;
    candidate_fields: PayReviewCandidateFields;
    bcsla: any;
    band: any;
    template: any;
    designation: any;
}

export class PayReviewCandidateFields {
    employeegradeid: any;
}

