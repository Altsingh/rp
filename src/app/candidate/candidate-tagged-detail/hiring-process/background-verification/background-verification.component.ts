import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { FormControl } from '@angular/forms';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';

@Component({
  selector: 'alt-hiring-process-background-verification',
  templateUrl: './background-verification.component.html',
  styleUrls: ['./background-verification.component.css']
})
export class BackgroundVerificationComponent implements OnInit {

  @Input() statusLists: StageActionListMaster;

  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();

  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }

  @Input() inputData: HiringProcessResponse;

  toggleOpen: boolean;

  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();

  constructor(private notificationService: NotificationService) { }

  ngOnInit() { }

  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.notificationService.addMessage(NotificationSeverity.WARNING, "Please Choose Status");
      valid = false;
    }
    if (valid == false) {
      return;
    }
    this.output.emit(this.updateStatusRequestTO);
  }

  toggleDiv() {
    if(this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

}
