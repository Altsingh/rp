import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';

@Component({
  selector: 'alt-hiring-process-shortlisting',
  templateUrl: './shortlisting.component.html',
  styleUrls: ['./shortlisting.component.css']
})
export class ShortlistingComponent implements OnInit {
  lable: string = "GO";
  shortlist: boolean = false;
  @Input() statusLists: StageActionListMaster;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }
  @Input() set spinner(value:boolean) {
    this.shortlist=false;
  }
  @Input() inputData: HiringProcessResponse;
  toggleOpen: boolean;

  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;


  constructor(private notificationService: NotificationService) { }

  ngOnInit() { }

  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    let valid: boolean = true;

    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.notificationService.addMessage(NotificationSeverity.WARNING, "Please Choose Status");
      valid = false;
    }

    if (valid == false) {
      return;
    }
    this.shortlist = true;
    this.output.emit(this.updateStatusRequestTO);
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'File size can not be more than 8 MB');
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'Only '+supportedTypes.toString()+ ' files are allowed!');
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }
  
  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }
}
