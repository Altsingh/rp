import { Component, OnInit, OnDestroy } from '@angular/core';
import { DialogService } from "app/shared/dialog.service";
import { MatDialogRef, MatNativeDateModule } from '@angular/material';

@Component({
  selector: 'alt-preview-offer',
  templateUrl: './preview-offer.component.html',
  styleUrls: ['./preview-offer.component.css']
})
export class PreviewOfferComponent implements OnInit, OnDestroy {

  response: any;

  constructor(private dialogService: DialogService, public dialogRef: MatDialogRef<PreviewOfferComponent>) {

  }

  ngOnInit() {
    this.response = this.dialogService.getDialogData();
  }

  ngOnDestroy() {
  }

}
