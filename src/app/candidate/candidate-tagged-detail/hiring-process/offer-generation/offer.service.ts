import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class OfferService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getEmploymentTypes() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/employmentTypeMaster/' + this.sessionService.organizationID + "/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  employeesByRoleType(roleType: string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/employeesByRoleType/' + roleType + '/' + this.sessionService.organizationID + "/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

 
  getTenantID() {
    return this.sessionService.tenantID;
  }

  getTemplateList() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/offer/offerTemplateList/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getOfferLetterDocument(applicantStatusID: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/offer/getOfferLetterDocument/'+applicantStatusID+'/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  

  getOfferLetterDocumentByHistory(applicantStatusID: number, applicantStatusHistoryID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/offer/getOfferLetterDocument/'+applicantStatusID+'/'+applicantStatusHistoryID+'/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getOfferLetterTemplate(applicantStatusHistoryID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/offer/getOfferLetterTemplate/'+applicantStatusHistoryID+'/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  previewOfferLetterContent(applicantStatusID, templateID,placeholderDisplayList){
   let input: PreviewPlaceholdersInput = new PreviewPlaceholdersInput();
    input.offerLetterID = templateID;
    input.placeholderList = placeholderDisplayList;
    input.applicantStatusID = applicantStatusID;
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/previewOffer/', JSON.stringify(input), options).
      map(res => {
        this.sessionService.check401Response(res.json()); return res.json()
      });
  }

  generateOfferLetter(applicantStatusID:any, offerLetterID:any, generateOnLetterHead:any, placeholderDisplayList:any){
    let input: PopulatePlaceholdersInput = new PopulatePlaceholdersInput();
    input.offerLetterID = offerLetterID;
    input.applicantStatusID = applicantStatusID;
    input.generateOnLetterHead = generateOnLetterHead.value;
    input.placeholderList = placeholderDisplayList;
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/generateOffer/', JSON.stringify(input), options).
      map(res => {
        this.sessionService.check401Response(res.json()); return res.json()
      });
   }

   getOnboardingDetails(applicantStatusID:any) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/offer/getOnboardingDetails/'+applicantStatusID+'/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  populatePlaceholders(applicantStatusID, offerLetterID, generateOnLetterHead) {
    let input: PopulatePlaceholdersInput = new PopulatePlaceholdersInput();
    input.applicantStatusID = applicantStatusID;
    input.offerLetterID = offerLetterID;
    input.generateOnLetterHead = generateOnLetterHead.value;
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/populatePlaceHolders/123/', JSON.stringify(input), options).
      map(res => {
        this.sessionService.check401Response(res.json()); return res.json()
      });
  }

  resetPlaceholders(applicantStatusID, offerLetterID, generateOnLetterHead) {
    let input: PopulatePlaceholdersInput = new PopulatePlaceholdersInput();
    input.applicantStatusID = applicantStatusID;
    input.offerLetterID = offerLetterID;
    input.generateOnLetterHead = generateOnLetterHead.value;
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/offer/resetPlaceHolders/123/', JSON.stringify(input), options).
      map(res => {
        this.sessionService.check401Response(res.json()); return res.json()
      });
  }


}

export class PopulatePlaceholdersInput {
  generateOnLetterHead: boolean;
  offerLetterID: number;
  applicantStatusID: number;
  placeholderList: any;
}
export class PreviewPlaceholdersInput{
  offerLetterID : number;
  placeholderList : any;
  applicantStatusID: number;
}
