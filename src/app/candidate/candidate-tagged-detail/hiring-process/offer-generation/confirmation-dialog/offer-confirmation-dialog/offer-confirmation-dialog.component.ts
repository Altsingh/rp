import { Component, OnInit } from '@angular/core';
import { DialogService } from '../../../../../../shared/dialog.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'alt-offer-confirmation-dialog',
  templateUrl: './offer-confirmation-dialog.component.html',
  styleUrls: ['./offer-confirmation-dialog.component.css']
})
export class OfferConfirmationDialogComponent implements OnInit {

  message: string;
  isWarningORConfirmation:boolean;

  constructor(public dialogRef: MatDialogRef<OfferConfirmationDialogComponent>, private dialogService: DialogService) {
    this.message = "";
    this.isWarningORConfirmation=false;
   }

  ngOnInit() {
    if (this.dialogService.getDialogData() != null) {
      this.message=this.dialogService.getDialogData();
    }
    if (this.dialogService.getDialogData1() != null) {
      this.isWarningORConfirmation=this.dialogService.getDialogData1();
    }
  }

}
