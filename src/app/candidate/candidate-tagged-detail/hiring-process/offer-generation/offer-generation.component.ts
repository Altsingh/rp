import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { GradeMaster } from '../../../../common/grade-master';
import { DesignationMaster } from '../../../../common/designation-master';
import { SelectItem } from '../../../../shared/select-item';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { NewJobService } from '../../../../job/new-job/new-job.service';
import { OfferService } from './offer.service';
import { DialogService } from '../../../../shared/dialog.service';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { PreviewOfferComponent } from "app/candidate/candidate-tagged-detail/hiring-process/offer-generation/preview-offer/preview-offer.component";
import { MessageCode } from "app/common/message-code";
import { DatePipe } from '@angular/common';
import { HiringProcessService } from '../hiring-process.service';
import { OfferConfirmationDialogComponent } from './confirmation-dialog/offer-confirmation-dialog/offer-confirmation-dialog.component';
import { MatDialogConfig } from '@angular/material';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-offer-hiring-process-generation',
  templateUrl: './offer-generation.component.html',
  styleUrls: ['./offer-generation.component.css'],
  providers: [OfferService, NewJobService, DatePipe]
})

export class OfferGenerationComponent implements OnInit {
  disablePreview : boolean=false;
  lable: string ;
  showloader: boolean = false;
  offerGeneration: boolean = false;
  jobStatus : boolean = true;
  displayClass: String = "displayNone";
  @Input() applicantStatusID: number;
  @Input() statusLists: StageActionListMaster;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }
  @Input() set spinner(value: boolean) {
    this.offerGeneration = false;
  }
  @Input() inputData: HiringProcessResponse;
  @Input() security: any;
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  toggleOpen: boolean;

  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  isOnboardingInitiated:Boolean;
  detailsAvailableinOB:Boolean;
  expectedJoiningDate:Date;
  selectedStatusID = new FormControl();
  selectedStatus: String;
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;
  gradeList: SelectItem[];
  designationList: SelectItem[];
  employmentTypeList: SelectItem[];
  l1ManagerList: SelectItem[];
  l2ManagerList: SelectItem[];
  hrManagerList: SelectItem[];
  DDRange: SelectItem[];
  MMRange: SelectItem[];
  YYYYRange: SelectItem[];

  templateList: SelectItem[];
  selectedTemplateID = new FormControl();
  isGenerateOnLetterHead = new FormControl();
  placeholderDisplayList: any;

  offerForm: FormGroup;
  offerLetterDownloadPath: String;

  _offer_getTemplateByGroupPolicy:boolean = false;
  _error_message: string;
  _show_error_message: boolean = false;
  offerTemplatePolicyEnabled:boolean = false;
  offerTemplateFromPolicy:any;

  constructor(private offerService: OfferService, private newJobService: NewJobService, private DialogService: DialogService,
    private formBuilder: FormBuilder, private notificationService: NotificationService, private datePipe: DatePipe,
    private hiringProcessService:HiringProcessService, private i18UtilService: I18UtilService ) { 
      this.offerTemplatePolicyEnabled = false;
      this._offer_getTemplateByGroupPolicy = false;
      this._error_message = undefined;
      this._show_error_message = false;
      this.offerTemplatePolicyEnabled = false;
      this.offerTemplateFromPolicy = undefined;
    }

  ngOnInit() {
    this.getOfferLetterDocument();
    if(this.toggleOpen) {
      this.hiringProcessService.isJobStatus().subscribe( out => this.jobStatus = out );
      this.isGenerateOnLetterHead.setValue(false);
      this.initializeForm();
      this.populateMasters();

      if (this.inputData.sysWorkflowstageActionType == 'OFFERMADE') {
        this.getPolicyConfig();
      }

    }
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.lable=res;
    });
  }

  initializeForm() {
    this.offerForm = this.formBuilder.group({
      grade: new FormControl(),
      designation: new FormControl(),
      employmentType: new FormControl(),
      l1Manager: new FormControl(),
      l2Manager: new FormControl(),
      hrManager: new FormControl(),
      ctc: new FormControl(),
      expectedJoiningDate: new FormControl(),
      employeeCode: new FormControl()
    });
  }

  populateMasters() {
    this.newJobService.getGradeMaster().subscribe(out => this.gradeList = out.responseData);
    this.newJobService.getDesignationMaster().subscribe(out => this.designationList = out.responseData);
    this.offerService.getEmploymentTypes().subscribe(out => this.employmentTypeList = out.responseData);
    this.offerService.employeesByRoleType('L1_MANAGER').subscribe(out => this.l1ManagerList = out.responseData);
    this.offerService.employeesByRoleType('L2_MANAGER').subscribe(out => this.l2ManagerList = out.responseData);
    this.offerService.employeesByRoleType('HR_MANAGER').subscribe(out => this.hrManagerList = out.responseData);
    this.DDRange = this.createRange(1, 31);
    this.MMRange = this.getMMRange();
    this.YYYYRange = this.createRange(2000, 20);
    this.offerService.getTemplateList().subscribe(out => this.templateList = out.responseData);
  }

  getPolicyConfig() {
    if(!this._offer_getTemplateByGroupPolicy) {
      this.hiringProcessService.getPolicyConfiguration().subscribe((response) => {
        for (let policy of response) {
          if (policy == 'OFFER-LETTER-TEMPLATE-POLICY') {
            this.offerTemplatePolicyEnabled = true;
            break;
          }
        }
        if (this.offerTemplatePolicyEnabled) {
          this.getOfferTemplateByGroupPolicy(this.applicantStatusID);
        } else {
          this._offer_getTemplateByGroupPolicy = true;
        }
      });
    }
  }

  getOfferTemplateByGroupPolicy(applicantStatusID: number) {
    this.hiringProcessService.getOfferTemplateByGroupPolicy(applicantStatusID).subscribe((response) => {
      if (response != undefined && response != null) {
        let messageTO: MessageCode = response.messageCode;
        if (messageTO != undefined && messageTO != null && messageTO.code != null) {
          if(messageTO.code === 'EC200') {
            let offerTemplateFromPolicy: SelectItem = new SelectItem(response.responseData.letterName,response.responseData.letterID);
            this.offerTemplateFromPolicy = offerTemplateFromPolicy;
            this.selectedTemplateID.setValue(this.offerTemplateFromPolicy.value);
            this.offerTemplateSelectListener();
            this._offer_getTemplateByGroupPolicy = true;
          } else if(messageTO.code === 'EC202') {
            this._show_error_message = true;
            this._error_message = 'Offer letter template mapping not found. Please contact your system administrator.';
            this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
          } else if(messageTO.code === 'EC203') {
            this._show_error_message = true;
            this._error_message = 'Offer letter template mapping not found. Please contact your system administrator.';
            this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
          }
        }
      }
    }, (onError) => {
      this._show_error_message = true;
      this._error_message = 'Offer letter template mapping not found. Please contact your system administrator.';
      this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
    }, () => {
    });
  }

  updateStatus() {
    this.notificationService.clear();
    if(this._show_error_message) {
      this.notificationService.setMessage(NotificationSeverity.WARNING, this._error_message);
      return;
    }
    this.isOnboardingInitiated=false;
    this.offerGeneration=true;
    this.detailsAvailableinOB=false;
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    this.updateStatusRequestTO.grade = this.offerForm.controls.grade.value;
    this.updateStatusRequestTO.designation = this.offerForm.controls.designation.value;
    this.updateStatusRequestTO.employmentType = this.offerForm.controls.employmentType.value;
    this.updateStatusRequestTO.l1Manager = this.offerForm.controls.l1Manager.value;
    this.updateStatusRequestTO.l2Manager = this.offerForm.controls.l2Manager.value;
    this.updateStatusRequestTO.hrManager = this.offerForm.controls.hrManager.value;
    this.updateStatusRequestTO.ctc = this.offerForm.controls.ctc.value;
    this.updateStatusRequestTO.expectedJoiningDate = this.offerForm.controls.expectedJoiningDate.value;
    this.updateStatusRequestTO.tenantID = this.offerService.getTenantID();
    this.updateStatusRequestTO.employeeCode = this.offerForm.controls.employeeCode.value;
    
    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      this.offerGeneration=false;
      return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      this.offerGeneration=false;
      return;
    }
    if (this.security.gradeRendered && this.security.gradeRequired && (this.updateStatusRequestTO.grade == null || this.updateStatusRequestTO.grade == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label':this.security.gradeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      this.notificationService.addMessage(NotificationSeverity.WARNING, "Please select " + this.security.gradeLabel);
      valid = false;
    }
    if (this.security.designationRendered && this.security.designationRequired && (this.updateStatusRequestTO.designation == null || this.updateStatusRequestTO.designation == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.designationLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.employmentTypeRendered && this.security.employmentTypeRequired && (this.updateStatusRequestTO.employmentType == null || this.updateStatusRequestTO.employmentType == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.employmentTypeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.hrManagerRendered && this.security.hrManagerRequired && (this.updateStatusRequestTO.hrManager == null || this.updateStatusRequestTO.hrManager == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.hrManagerLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.l1ManagerRendered && this.security.l1ManagerRequired && (this.updateStatusRequestTO.l1Manager == null || this.updateStatusRequestTO.l1Manager == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.l1ManagerLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.l2ManagerRendered && this.security.l2ManagerRequired && (this.updateStatusRequestTO.l2Manager == null || this.updateStatusRequestTO.l2Manager == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.l2ManagerLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.ctcRendered && this.security.ctcRequired && (this.updateStatusRequestTO.ctc == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.ctcLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.employeeCodeRendered && this.security.employeeCodeRequired && (this.updateStatusRequestTO.employeeCode == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.employeeCodeLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.expectedJoiningDateRendered && this.security.expectedJoiningDateRequired && (this.updateStatusRequestTO.expectedJoiningDate == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.expectedJoiningDateLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }    
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == "")) {
      this.notificationService.addMessage(NotificationSeverity.WARNING, "Please enter " + this.security.commentLabel);
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && (this.updateStatusRequestTO.attachmentName == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label':this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if(this.security.offerTemplateRendered && this.security.offerTemplateRequired && this.selectedTemplateID.value == null) {
      this.i18UtilService.get('candidate.warning.selectOfferTemplate', {'label':this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    } else {
      if(this.placeholderDisplayList != null && this.placeholderDisplayList != undefined){
      let check=this.checkmandatory(this.placeholderDisplayList);
      if(check===false)
      {
        valid = false;
      }
    }
  }

    if (valid == false) {
      this.offerGeneration=false;
      return;
    }

    if (this.selectedStatus.toLowerCase().includes('made') || this.selectedStatus.toLowerCase().includes('regenerate')) {

      Promise.all([this.offerService.getOnboardingDetails(this.applicantStatusID).toPromise()]).then((res) => {
        if (res.length > 0) {
          let messageTOupdateDOJ: MessageCode = res[0].messageCode;
          let message: string = '';
          if (messageTOupdateDOJ.code != 'EC200') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, message);
            this.offerGeneration = false;
          } else {
            this.updateStatusRequestTO.onboardingData=res[0].responseData;
            if(this.updateStatusRequestTO.onboardingData!=null && this.updateStatusRequestTO.onboardingData.length>1
              && this.updateStatusRequestTO.onboardingData[0]){
                this.detailsAvailableinOB=this.updateStatusRequestTO.onboardingData[0];
                this.isOnboardingInitiated=this.updateStatusRequestTO.onboardingData[1];
            }
            const dialogConfig = new MatDialogConfig();
            dialogConfig.width = '350px';
            dialogConfig.disableClose = true;

            if(this.detailsAvailableinOB && this.isOnboardingInitiated){
              this.DialogService.setDialogData("Please Note: Onboarding has been initiated for the candidate. Do you still wish to update the offer details as they will not be updated in Onboarding Portal?");
              this.DialogService.setDialogData1(false);
                this.DialogService.open(OfferConfirmationDialogComponent, dialogConfig).subscribe(result => {
                  if (result != undefined && result == 'yes') {
                  // generate offer and emit parent event
                    this.updateAndGenerateOffer();
                  }else{
                    this.offerGeneration=false;
                  }
              });
          
            }else if(this.detailsAvailableinOB  && !this.isOnboardingInitiated){
              this.DialogService.setDialogData(" Any update in candidate or offer details will not be updated in Onboarding Portal incase onboarding has been initiated.");
              this.DialogService.setDialogData1(true);

              this.DialogService.open(OfferConfirmationDialogComponent, dialogConfig).subscribe(result => {
                  // generate offer and emit parent event
                  if (result != undefined && result == 'ok') {
                    this.updateAndGenerateOffer();
                  }else{
                    this.offerGeneration=false;
                  }
              });
            }else if(!this.detailsAvailableinOB  && !this.isOnboardingInitiated){
              // generate offer and emit parent event
              this.updateAndGenerateOffer();
            }

            // this.output.emit(this.updateStatusRequestTO);
          }
        }else{
          this.updateAndGenerateOffer();
        }
      });

    } else {
      this.offerGeneration = true;
      this.output.emit(this.updateStatusRequestTO);
    }
  }

  updateAndGenerateOffer(){
      this.offerGeneration = true;
      this.offerService.generateOfferLetter(this.applicantStatusID, this.selectedTemplateID.value, this.isGenerateOnLetterHead, this.placeholderDisplayList).subscribe(out => {
        let messageTO: MessageCode = out.messageCode;
        let message: string = '';
        if (messageTO != null) {
          if (messageTO.code != null  && messageTO.code === 'EC200') {
            this.updateStatusRequestTO.hrOfferLetterID = out.response;
            this.updateStatusRequestTO.offerLetterPath = out.responseData[0];
            this.updateStatusRequestTO.offerLetterTemplateID = this.selectedTemplateID.value;
            if(messageTO.message != null && messageTO.message!='Success'){
              message = messageTO.message.toString();
              this.notificationService.setMessage(NotificationSeverity.WARNING, message);
            }
            this.output.emit(this.updateStatusRequestTO);
          } else if (messageTO.message != null) {
            message = messageTO.message.toString();
          } else {
            message = "Request failed to execute."
          }

          if (messageTO.code != 'EC200') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, message);
            this.offerGeneration = false;
          }
        }
      }, (onError) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, 'Something went wrong. Please contact your System Admin.');
        this.offerGeneration = false;
      }, () => {
      });

  }

  getOfferLetterDocument() {
    this.offerService.getOfferLetterDocument(this.applicantStatusID).subscribe(response => {
      let messageTO: MessageCode = response.messageCode;
      if (messageTO != null && messageTO.code === 'EC200') {
        this.offerLetterDownloadPath = response.response;
      }
    });
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }


  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result.toString();
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  createRange(min, noOfItems) {
    var items: SelectItem[] = [];
    for (var i = min; i <= (min + noOfItems - 1); i++) {
      items.push(new SelectItem(i, i));
    }
    return items;
  }

  getMMRange() {
    var items: SelectItem[] = [];
    items.push(new SelectItem('Jan', 1));
    items.push(new SelectItem('Feb', 2));
    items.push(new SelectItem('Mar', 3));
    items.push(new SelectItem('Apr', 4));
    items.push(new SelectItem('May', 5));
    items.push(new SelectItem('Jun', 6));
    items.push(new SelectItem('Jul', 7));
    items.push(new SelectItem('Aug', 8));
    items.push(new SelectItem('Sep', 9));
    items.push(new SelectItem('Oct', 10));
    items.push(new SelectItem('Nov', 11));
    items.push(new SelectItem('Dec', 12));
    return items;
  }

  setExpectedJoiningDate(expectedDate: Date) {
    let _EXPECTEDDATA: String = expectedDate.getDate() + '-' + expectedDate.getMonth() + '-' + expectedDate.getFullYear() + ' ' + expectedDate.getHours() + ':' + expectedDate.getMinutes();
    this.offerForm.controls['expectedJoiningDate'].setValue(_EXPECTEDDATA);
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }
  statusChangeListener(event: any) {
    this.selectedStatus = event.label;
    this.selectedStatusID.setValue(event.value);
    this.initSecurity.emit(event.value);
    if(event.sysActionType==='OFFERMADE') {
      this.getPolicyConfig();
    }
  }

  previewOfferLetter() {
    this.notificationService.clear();
    let valid:boolean=true;
    if(this.disablePreview ==true){
      return;
    }
    this.disablePreview = true;
    // valid=this.checkmandatory(this.placeholderDisplayList);
    if(!valid)
    { 
      return;
    }
    else
    {
      // this.offerGeneration = true;
      this.offerService.previewOfferLetterContent(this.applicantStatusID, this.selectedTemplateID.value, this.placeholderDisplayList).subscribe(response => {
      this.DialogService.setDialogData(response);
      let messageTO: MessageCode = response.messageCode;
      let message: string = '';
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.DialogService.open(PreviewOfferComponent, { width: '935px' }).subscribe((response)=>{
            this.disablePreview = false;
          });
        }
        else if (messageTO.message != null) {
          message = messageTO.message.toString();
        } else {
          message = "Request failed to execute."
        }
      }
    }, (onError) => {
      this.i18UtilService.get('common.error.somthingWrong').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      // this.offerGeneration = false;
    }, () => {
      // this.offerGeneration = false;
    });
  }
  }

  offerTemplateSelectListener() {
    this.showloader = true;
    this.offerService.populatePlaceholders(this.applicantStatusID, this.selectedTemplateID.value,
      this.isGenerateOnLetterHead.value).subscribe(response => {
        this.placeholderDisplayList = response;
        this.showloader = false;
      }, (onError) => {
        this.showloader = false;
      }, () => {
        this.showloader = false;
      }
      );
  }

  resetPlaceholders() {
    this.showloader = true;
    this.offerService.resetPlaceholders(this.applicantStatusID, this.selectedTemplateID.value,
      this.isGenerateOnLetterHead.value).subscribe(response => {
        this.placeholderDisplayList = response;
        this.showloader = false;
      }, (onError) => {
        this.showloader = false;
      }, () => {
        this.showloader = false;
      }
      );
  }

  decimalValuesKeyPress(event: any, value: String) {
    // if (event.charCode === 46 && value.indexOf('.') > 0) {
    //   return false;
    // }
    if ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46
      || event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 37
      || event.keyCode === 39) {
      return true;
    } else {
      return false;
    }
  }

  listPlaceholderChangeListener(selectItemList: SelectItem[], value: string) {
    if (value.trim() != '') {
      for (let placeholder of this.placeholderDisplayList) {
        if (placeholder.placeHolderName.toLowerCase().includes('spocemail')) {
          for (let item of selectItemList) {
            if (item.label === value) {
              placeholder.placeHolderValue = item.value;
              break;
            }
          }
        }
      }
    } else {
      for (let placeholder of this.placeholderDisplayList) {
        if (placeholder.placeHolderName.toLowerCase().includes('spocemail')) {
          placeholder.placeHolderValue = '';
          break;
        }
      }
    }
  }

  datetimeChangeListener(event,placeholder) {
    //yyyy-MM-dd
    let date:Date = event.value;
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var formattedDate = y+'-'+(m+1)+'-'+d;
    placeholder.placeHolderValue = formattedDate;
    placeholder.placeHolderDateValue = date;
  }

  dateChangeListener(event,placeholder) {
    //dd-MMM-yyyy
    let date:Date = event.value;
    if(placeholder.fieldType==='date'){
      placeholder.placeHolderValue = this.datePipe.transform(date, "dd-MMM-yyyy");
      placeholder.placeHolderDateValue=date;
    }else if(placeholder.fieldType==='datetime1'){
      placeholder.placeHolderValue = this.datePipe.transform(date, "dd-MMMM-yyyy");
      placeholder.placeHolderDateValue=date;
    }
  }

  checkmandatory(placeholderDisplayList)
  {
    let valid: boolean = true;
    for(let placeholder of this.placeholderDisplayList)
    {
      if(placeholder.mandatory===true)
      {
        if(placeholder.placeHolderValue===null || placeholder.placeHolderValue==='')
        {
          this.i18UtilService.get('common.warning.isRequiredCommon', {'label':  placeholder.placeHolderDescription}).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid=false;
        }
        if(placeholder.placeHolderValue != null && placeholder.placeHolderName ==='@@DateOfBirth@@')
        {         
          var year = new Date(placeholder.placeHolderDateValue).getFullYear();
          var today = new Date().getFullYear();
          if(today - year < 18)
          {
            this.i18UtilService.get('common.warning.ageValidation', {'label':  placeholder.placeHolderDescription}).subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, res);
            });
            valid=false;
          }
        }
      }
    }
    return valid;
  }
}
