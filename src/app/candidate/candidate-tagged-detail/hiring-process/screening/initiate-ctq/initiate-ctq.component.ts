import { Component, OnInit, Input } from '@angular/core';
import { HiringProcessService } from 'app/candidate/candidate-tagged-detail/hiring-process/hiring-process.service';
import { CtqMaster } from 'app/common/ctq-master';
import { SelectItem } from '../../../../../shared/select-item';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-initiate-ctq',
  templateUrl: './initiate-ctq.component.html',
  styleUrls: ['./initiate-ctq.component.css']
})
export class InitiateCtqComponent implements OnInit {

  ctqData: any;

  @Input()
  jobCode: String;

  constructor(private hiringProcessService: HiringProcessService,
    private notificationService: NotificationService,
    private i18UtilService: I18UtilService) { }

  changeEvent(option,index){
    this.ctqData[index].answer=option;
  }

  ngOnInit() {
    this.hiringProcessService.getCtqDetails(this.jobCode).subscribe( out => {
      this.ctqData = out.responseData;
      if(this.ctqData != undefined && this.ctqData != null) {
        for(let ctq of this.ctqData) {
          ctq.answer = '';
          if(ctq.questionType=='SELECTONEMENU') {
            ctq.selectItemOptions = this.getOptions(ctq.options)
          }
        }
      }
    });
  }

  getOptions(options) {
    let selectItemOptions = [];
    for(let option of options) {
      selectItemOptions.push(new SelectItem(option, option));
    }
    return selectItemOptions
  }

  validateCTQ(): boolean {
    if(this.ctqData != undefined && this.ctqData != null) {
      for(let ctq of this.ctqData) {
        if(ctq.mandatory && (ctq.answer == undefined || ctq.answer == null || ctq.answer.trim().length == 0)) {
          this.i18UtilService.get('candidate.warning.answerMandatoryQuestions').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          return false;
        } 
        if(ctq.answer != undefined && ctq.answer != null && ctq.answer.trim().length > 1000) {
          this.i18UtilService.get('candidate.warning.answerLengthExceeded').subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          return false;
        }
      }
    }
    return true;
  }

}
