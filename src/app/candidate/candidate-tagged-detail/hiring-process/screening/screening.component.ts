import { Component, Input, Output, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { FormControl } from '@angular/forms';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { HiringProcessService } from '../hiring-process.service';
import { InitiateCtqComponent } from 'app/candidate/candidate-tagged-detail/hiring-process/screening/initiate-ctq/initiate-ctq.component';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-hiring-process-screening',
  templateUrl: './screening.component.html',
  styleUrls: ['./screening.component.css']
})
export class ScreeningComponent implements OnInit {
  
  label: string;
  screened: boolean = false;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  @Input() statusLists: StageActionListMaster;
  @Input() set spinner(value:boolean) {
    this.screened=false;
  }
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }

  @Input() inputData: HiringProcessResponse;
  @Input() security: any;

  @Input() applicantStatusID: any;
  @Input() jobCode: String;

  @ViewChild(InitiateCtqComponent)
  INITIATECTQCOMPONENT : InitiateCtqComponent;

  newActionType: any;
  toggleOpen: boolean;
  jobStatus : boolean = true;


  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;

  constructor(private notificationService: NotificationService,
              private hiringProcessService: HiringProcessService,
              private i18UtilService: I18UtilService) { }

  ngOnInit() { 
    this.hiringProcessService.isJobStatus().subscribe( out => this.jobStatus = out );
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.label=res;
    });
  }

  updateStatus() {
    this.notificationService.clear();

    if(this.newActionType === 'INITIATE_CTQ') {
      let valid = this.INITIATECTQCOMPONENT.validateCTQ();
      if(!valid) {
        return;
      }
    }

    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == "")) {
      this.i18UtilService.get('common.warning.enterLabelCommon', {'label': this.security.commentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (valid == false) {
      return;
    }
    this.screened = true;
    if(this.INITIATECTQCOMPONENT != undefined) {
      this.updateStatusRequestTO.ctqData = this.INITIATECTQCOMPONENT.ctqData;
    }
    this.output.emit(this.updateStatusRequestTO);
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  statusChangeListener(value: any, sysActionType: any) {
    this.newActionType = sysActionType;
    this.selectedStatusID.setValue(value);
    this.initSecurity.emit(value);
  }
}
