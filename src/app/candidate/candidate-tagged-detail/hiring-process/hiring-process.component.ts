import { Component, OnInit } from '@angular/core';
import { HiringProcessService } from './hiring-process.service';
import { StageActionListMaster } from '../../../common/stage-action-list-master';
import { HiringProcessResponse } from '../../../common/hiring-process-response';
import { HiringProcessUpdateStatusTo } from '../../../common/hiring-process-update-status-to';
import { ApplicantStatusRequest } from '../../../common/applicant-status-request';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { MessageCode } from '../../../common/message-code';
import { ActivatedRoute } from '@angular/router';
import { HiringControllerService } from '../../hiring-controller.service';
import { CommonService } from "app/common/common.service";
import { DialogService } from 'app/shared/dialog.service';
import { CustomEmailComponent } from 'app/shared/custom-email/custom-email.component';
import { MatDialogConfig } from '@angular/material';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-hiring-process',
  templateUrl: './hiring-process.component.html',
  styleUrls: ['./hiring-process.component.css'],
  providers: [DialogService]
})
export class HiringProcessComponent implements OnInit {

  showloader: boolean = false;
  selectedStatusID: number;
  statusList: StageActionListMaster;
  currentSequence = 1;
  responseData: ResponseData;
  hiringProcessList: HiringProcessResponse;
  applicantStatusID: number;
  workflowStageID: number;
  workflowID: number;
  strictWorkflow: boolean;
  strictStatus: boolean;
  statusWeightage: number;
  workflowActionID: number;
  requisitionId: number;
  sysWorkflowStageType: string;
  applicantStatusRequest: ApplicantStatusRequest;
  security: any;
  securitySubscription: any;
  spinner: boolean;
  jobStatusString: String;
  jobStatus: boolean = true;  
  jobCode: String;

  constructor(private hiringProcessService: HiringProcessService, private notificationService: NotificationService, private route: ActivatedRoute,
    private hiringControllerService: HiringControllerService, private commonService: CommonService,
    private dialogsService: DialogService, private i18UtilService: I18UtilService) {
    hiringControllerService.setHiringProcessComponent(this);

    this.route.queryParams.subscribe((queryParams) => {
      this.applicantStatusID = queryParams['appid'];
      this.jobCode = this.route.snapshot.queryParams['jobCode'];  
      this.initializeData(this.applicantStatusID);
    })


  }

  ngOnInit() {

    this.commonService.showRHS();


  }

  initFormSecurityByStatusID(statusID: number) {
    this.workflowActionID = statusID;
    this.initFormSecurity();
  }

  initFormSecurity() {
    this.securitySubscription = this.hiringProcessService.getHiringProcessSecurity(this.sysWorkflowStageType, this.workflowID, this.workflowStageID, this.workflowActionID).subscribe(
      response => {
        if (response.messageCode.code === 'EC200')
          this.security = JSON.parse(JSON.stringify(response.responseData[0]));
        else
          this.security = undefined;
      });

  }

  initializeData(applicantStatusID: number) {
    this.showloader = true;
    this.hiringProcessService.getHiringProcessData(applicantStatusID).subscribe(out => {
      this.responseData = out.responseData;
      this.sysWorkflowStageType = this.responseData.sysWorkflowStageType;
      this.hiringProcessList = this.responseData.hiringProcessResponse;
      this.workflowStageID = this.responseData.workflowStageID;
      this.workflowID = this.responseData.workflowID;
      this.strictWorkflow = this.responseData.strictWorkflow;
      this.strictStatus = this.responseData.strictStatus;
      this.statusWeightage = this.responseData.statusWeightage;
      this.workflowActionID = this.responseData.workflowActionID;
      this.requisitionId = this.responseData.requisitionId;
      this.initFormSecurity();
      this.hiringProcessService.getStatusListByStageID(this.workflowStageID, this.strictWorkflow, this.strictStatus, this.statusWeightage).subscribe(out => {
        this.statusList = out.responseData
        this.showloader = false;
        this.hiringProcessService.getJobStatusByRequisitionID(this.requisitionId).subscribe(out => {
          if (out.response != null) {
            this.jobStatusString = out.response;
            if (this.jobStatusString == "CLOSED" || this.jobStatusString == "ONHOLD" || this.jobStatusString == "CANCELLED")
              this.jobStatus = true;
            else
              this.jobStatus = false;
            this.hiringProcessService.updateJobStatus(this.jobStatus);
          }
        });
      });
    });
  }

  updateStatus(event: HiringProcessUpdateStatusTo): void {
    this.applicantStatusRequest = new ApplicantStatusRequest();
    this.applicantStatusRequest.applicantStatusID = this.applicantStatusID;
    this.applicantStatusRequest.comment = event.comment;
    this.applicantStatusRequest.selectedWorkflowActionID = event.selectedStatusID;
    this.applicantStatusRequest.attachmentData_base64 = event.attachmentData_base64;
    this.applicantStatusRequest.attachmentName = event.attachmentName;
    this.applicantStatusRequest.interviewTypeID = event.interviewTypeID;
    this.applicantStatusRequest.applicantVenueID = event.applicantVenueID;
    this.applicantStatusRequest.startDate = event.startDate;
    this.applicantStatusRequest.endDate = event.endDate;
    this.applicantStatusRequest.interviewPanelist = event.interviewPanelist;
    this.applicantStatusRequest.calendarInvite = event.calendarInvite;
    this.applicantStatusRequest.interviewVenue = event.interviewVenue;
    this.applicantStatusRequest.grade = event.grade;
    this.applicantStatusRequest.designation = event.designation;
    this.applicantStatusRequest.employmentType = event.employmentType;
    this.applicantStatusRequest.l1Manager = event.l1Manager;
    this.applicantStatusRequest.l2Manager = event.l2Manager;
    this.applicantStatusRequest.hrManager = event.hrManager;
    this.applicantStatusRequest.ctc = event.ctc;
    this.applicantStatusRequest.expectedJoiningDate = event.expectedJoiningDate;
    this.applicantStatusRequest.actualJoiningDate = event.actualJoiningDate;
    this.applicantStatusRequest.tenantID = event.tenantID;
    this.applicantStatusRequest.employeeCode = event.employeeCode;
    this.applicantStatusRequest.hrOfferLetterID = event.hrOfferLetterID;
    this.applicantStatusRequest.updateApplicantPaystructureDetailsHistoryID = event.updateApplicantPaystructureDetailsHistoryID;
    this.applicantStatusRequest.workflowActorList = event.workflowActorList;
    this.applicantStatusRequest.offerLetterTemplateID = event.offerLetterTemplateID;
    this.applicantStatusRequest.payStructureID = event.payStructureID;
    this.applicantStatusRequest.payStructureCode = event.payStructureCode;    
    this.applicantStatusRequest.userWorkflowHistoryData=event.userWorkflowHistoryData;
    this.applicantStatusRequest.assessmentType =event.assessmentType;
    this.applicantStatusRequest.assessmentTypeID =event.assessmentTypeID;
    this.applicantStatusRequest.offerLetterPath = event.offerLetterPath;
    this.applicantStatusRequest.ctqData = event.ctqData;
    let subs = this.hiringProcessService.updateApplicantStatus(this.applicantStatusRequest).subscribe(out => {
      let messageTO: MessageCode = out.messageCode;
      let message: string = '';
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.workflowStageID = out.responseData.workflowStageID;
          this.initializeData(this.applicantStatusID);
          this.hiringControllerService.getCandidateTaggedScreeningComponent().initHiringData();
          this.hiringProcessService.updateWorkFlow();

          if (out.responseData.mailTemplates != null && out.responseData.mailTemplates.length > 0) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.data = out.responseData.mailTemplates;

            if( this.applicantStatusRequest.calendarInvite && this.applicantStatusRequest.startDate ){
              for(let daata of dialogConfig.data) {
                daata.calendarInvite = this.applicantStatusRequest.calendarInvite;
                daata.startDate = this.applicantStatusRequest.startDate;
                daata.endDate = this.applicantStatusRequest.endDate;
                daata.meetingLocation = this.applicantStatusRequest.interviewVenue;
                daata.applicantStatusID = this.applicantStatusRequest.applicantStatusID;
              }
            }

            dialogConfig.width = '935px';
            dialogConfig.disableClose = true;
            this.dialogsService.open(CustomEmailComponent, dialogConfig)
              .subscribe(result => {
                if (result != undefined && result == 'CANCEL') {
                  message = 'candidate.success.statusUpdatedEmailNotSent'
                } else if (result != undefined && result == 'SUCCESS') {
                  message = 'candidate.success.statusUpdated'
                }
                this.i18UtilService.get(message).subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
              });
          } else {
            message = 'candidate.success.statusUpdated'
          }
        } else if (messageTO.message != null) {
          message = messageTO.message.toString();
        } else {
          message = "Request failed to execute."
        }

        if (messageTO.code === 'EC200') {
          if (out.responseData.mailTemplates == null || out.responseData.mailTemplates.length == 0) {
            this.i18UtilService.get(message).subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.hiringProcessService.updateWorkFlow();
          }
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, message);
        }
      }
    }, (onError) => {
      this.i18UtilService.get('common.error.somthingWrong').subscribe((res: string) => {
        this.notificationService.setMessage("WARNING", res);
      });
      this.spinner = !this.spinner;
      subs.unsubscribe();
    }, () => {
      this.spinner = !this.spinner;
      subs.unsubscribe();
    });
  }
}

export class ResponseData {
  hiringProcessResponse: HiringProcessResponse;
  requisitionId: number;
  workflowStageID: number;
  sysWorkflowStageType: string;
  workflowID: number;
  strictWorkflow: boolean;
  strictStatus: boolean;
  statusWeightage: number;
  workflowActionID: number;
  feedbackDocumentPath: string;
  sysWorkflowstageActionType: string;
}