import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class LOIService {

  constructor(private http: Http, private sessionService: SessionService) { }
    private getHeaders() {
      const headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'POST');
      headers.append('Access-Control-Allow-Origin', '*');
      return headers;
    }


  getTemplateList() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/template_list')
      .map(res => { 
        this.sessionService.check401Response(res.json()); 
        return res.json();
      });
  }

  getTemplateDetails(request: Number,applicantStatusId: Number) {
    const options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidateDetail/loi_template/'+ applicantStatusId, JSON.stringify(request), options)
      .map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

}