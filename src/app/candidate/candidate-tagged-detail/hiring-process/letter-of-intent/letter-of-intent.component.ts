import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { LOIService } from './letter-of-intent.service';
import { StageActionListMaster } from '../../../../common/stage-action-list-master';
import { HiringProcessUpdateStatusTo } from '../../../../common/hiring-process-update-status-to';
import { HiringProcessResponse } from '../../../../common/hiring-process-response';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { EmailTemplateComponent } from '../../../email-template/email-template.component';
import { DialogService } from '../../../../shared/dialog.service';
import { SessionService } from '../../../../session.service';
import { ActivatedRoute } from '@angular/router';
import { HiringProcessService } from 'app/candidate/candidate-tagged-detail/hiring-process/hiring-process.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-hiring-process-loi',
  templateUrl: './letter-of-intent.component.html',
  styleUrls: ['./letter-of-intent.component.css'],
  providers: [LOIService]
})

export class LetterOfIntentComponent implements OnInit {
  applicantStatusId: any;
  templateId: number;
  lable: string;
  loi = false;
  showloader = false;
  basicInfo: FormGroup;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  @Input() statusLists: StageActionListMaster;
  @Input() set spinner(value: boolean) {
    this.loi = false;
  }
  @Input() set inputToggle(value: boolean) {
    this.toggleOpen = value;
  }

  @Input() inputData: HiringProcessResponse;
  @Input() security: any;

  toggleOpen: boolean;


  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;
  statusType : string;
  selectedTemplate : string;
  jobStatus : boolean = true;


  templateList: any[];
  template: LOITemplate;

  constructor(private notificationService: NotificationService,
    private dialogsService :DialogService ,
    private sessionService:SessionService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private loiService: LOIService,
    private hiringProcessService : HiringProcessService,
    private i18UtilService: I18UtilService ) {

  }

  ngOnInit() {
    this.hiringProcessService.isJobStatus().subscribe( out => this.jobStatus = out );
    this.initializeForm();
    this.applicantStatusId = this.route.snapshot.queryParams['appid'];
    this.getTemplateList();
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.lable=res;
    });
  }

  initializeForm() {
    this.basicInfo = this.formBuilder.group(
      {
        template: '',
      });
  }

  getTemplateList() {
    this.loiService.getTemplateList().subscribe((response) => {
      this.templateList = response.responseData;
      if (this.templateList != null) {
        for (let item of this.templateList) {
          item.id = item.templateId;
          item.name = item.templateName;
        }
      }
    })
  }

  getTemplateDetails(templateId: number, applicantStatusId: number) {
    this.showloader=true;
    this.loiService.getTemplateDetails(templateId, applicantStatusId).subscribe((response) => {
      if (response.messageCode != null && response.messageCode.code === 'EC200'){
        this.sessionService.object = response.response;
        this.sessionService.object.loi='LOI';
        this.sessionService.object.applicantStatusId= applicantStatusId;
        this.dialogsService.open(EmailTemplateComponent, { width: '935px' }).subscribe((event)=>{
          if(!(event=="Cancel"))
            this.output.emit(this.updateStatusRequestTO);
        });
      } else{
        this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
      this.showloader=false;
    })
  }

  openEmailTemplate() {
    this.templateId=this.basicInfo.controls.template.value;
    if (this.templateId != null || this.templateId !== undefined) {
      this.getTemplateDetails(this.templateId, this.applicantStatusId);
    }
  }

  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment.trim() == '')) {
      this.i18UtilService.get('common.warning.enterLabelCommon', {'label': this.security.commentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', {'label': this.security.attachmentLabel}).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if(this.statusType == 'CREATE' && (this.selectedTemplate == null || this.selectedTemplate== undefined)){
      this.i18UtilService.get('candidate.warning.selectTemplateType').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (valid == false) {
      return;
    }
    if(this.statusType=== 'CREATE' && (this.selectedTemplate != null || this.selectedTemplate !== 'undefined') ){
      this.openEmailTemplate();
    } else{
      this.output.emit(this.updateStatusRequestTO);
    }
  }

  toggleDiv() {
    if (this.inputData.isCurrentStageStatus) {
      this.toggleOpen = !this.toggleOpen;
    }
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }
  statusChangeListener(event: any) {
    this.selectedStatusID.setValue(event.value);
    this.initSecurity.emit(event.value);
    this.statusType= event.sysActionType;
  }
  templateChangeListener(event: any) {
    this.basicInfo.controls['template'].setValue(event.id)
    this.selectedTemplate = event.name;
  }
}


export class LOITemplate {
  templateId: Number;
  templateName: String;
  description: String;
  subject: String;
  content: String;
  from: String;
  to: String;
  cc: String;
  bcc: String;
}