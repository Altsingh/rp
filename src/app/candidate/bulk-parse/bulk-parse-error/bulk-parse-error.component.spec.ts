import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkParseErrorComponent } from './bulk-parse-error.component';

describe('BulkParseErrorComponent', () => {
  let component: BulkParseErrorComponent;
  let fixture: ComponentFixture<BulkParseErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkParseErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkParseErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
