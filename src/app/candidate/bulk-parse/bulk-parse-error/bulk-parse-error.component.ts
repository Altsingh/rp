import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../session.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'alt-bulk-parse-error',
  templateUrl: './bulk-parse-error.component.html',
  styleUrls: ['./bulk-parse-error.component.css']
})
export class BulkParseErrorComponent implements OnInit {
  unsupportedFiles:String;
  sizeNotSupportedFiles:String;
  invalidFileCount = 0;
  constructor(private sessionService: SessionService,
    public dialogRef: MatDialogRef<BulkParseErrorComponent>) { }

  ngOnInit() {
    this.unsupportedFiles = this.sessionService.object.unsupportedFiles;
    this.sizeNotSupportedFiles = this.sessionService.object.sizeNotSupportedFiles;
    this.invalidFileCount = this.sessionService.object.invalidFileCount;    
  }

  contunueUpload()
  {
    this.dialogRef.close(true);
  }
}
