import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../session.service';
import { DownloadDocumentsRequest } from '../new-candidate/new-candidate.service';
@Injectable()
export class BulkParseService {

    constructor(private http: Http, private sessionService: SessionService) { }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
        return headers;
      }

      uploadBulkResume(requestObject)
      {
        console.log(requestObject);
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/bulkparse/uploadbulkresume/', JSON.stringify(requestObject),options)
        .map(res => { this.sessionService.check401Response(res.json()); return res.json() });
      }

      getUploadDocumentList()
      {
        return this.http.get(this.sessionService.orgUrl + "/rest/altone/bulkparse/getUploadDocumentList/").map(
          res=>{
            this.sessionService.check401Response(res.json());
            return res.json()
          }
        );
      }
      public downloadBulkDocuments(arr, fileType) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        let requestObj : DownloadDocumentsRequest = new DownloadDocumentsRequest();
        requestObj.input = arr;
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/bulkparse/download/documents/?filetype=' + fileType, JSON.stringify(requestObj,fileType), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
}