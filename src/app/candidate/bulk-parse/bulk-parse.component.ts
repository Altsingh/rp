import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
import { SelectItem } from '../../shared/select-item';
import { BulkParseService } from './bulk-parse.service';
import { NewCandidateService } from '../new-candidate/new-candidate.service';
import { FormControl } from '@angular/forms';
import { NotificationSeverity } from '../../job/job-action/post-to-ijp/post-to-ijp.component';
import { NotificationService } from '../../common/notification-bar/notification.service';
import { DialogService } from '../../shared/dialog.service';
import { BulkParseErrorComponent } from './bulk-parse-error/bulk-parse-error.component';
import { SessionService } from '../../session.service';
import * as JSZip from 'jszip';
import { I18NHtmlParser } from '@angular/compiler';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { DocumentUploadUtilityService, DocumentValidationOptions, UploadDocumentValidationResult, DocumentValidationErrorCodes } from 'app/shared/services/document-upload-utility/document-upload-utility.service';


@Component({
  selector: 'alt-bulk-parse',
  templateUrl: './bulk-parse.component.html',
  styleUrls: ['./bulk-parse.component.css'],
  providers: [BulkParseService, NewCandidateService]
})
export class BulkParseComponent implements OnInit {
  uploadDocumentList: any;
  jobTitleSelected: any[];
  jobTitleList: any[];
  candidateResumeList: Array<CandidateResume> = new Array<CandidateResume>();
  candidateTagUntagged = new FormControl();
  requisitionID: Number;
  showFileUpload: boolean;
  unsupportedFiles = "";
  sizeNotSupportedFiles = "";
  invalidFileCount = 0;
  constructor(
    private commonService: CommonService,
    private bulkParseService: BulkParseService,
    private newCandidateService: NewCandidateService,
    private notificationService: NotificationService,
    private sessionService: SessionService,
    private dialogService: DialogService,
    private i18UtilService: I18UtilService,
    private documentUploadUtilityService: DocumentUploadUtilityService) { this.commonService.hideRHS(); }

  ngOnInit() {
    this.newCandidateService.fetchMyJobList().subscribe(response => {
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            this.jobTitleList = response.responseData;
          }
        }
      }
    });
    this.getUploadedResumeList();
  }

  getUploadedResumeList() {
    this.bulkParseService.getUploadDocumentList().subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          this.uploadDocumentList = response.responseData;          
        }
      }
      else {
        this.i18UtilService.get('candidate.error.docFetchFailed').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
    });
  }

  candidateTagUntaggedChange() {
    this.candidateTagUntagged.enable;
    this.requisitionID = null;
    if (this.candidateTagUntagged.value == "untagged" || (this.candidateTagUntagged.value == "tagged" && this.requisitionID != null)) {
      this.showFileUpload = true;
    }
    else {
      this.showFileUpload = false;
    }
  }

  downloadZipFiles(data: any, fileType: string) {
    let arr: Array<String> = [];    
    let list: any;
    if(fileType == "duplicate")
    {
      list = data.duplicatePathList;
    }
    else if(fileType == "unsupported")
    {
      list = data.unsupportedPathList;
    }
    else if(fileType == "invalid")
    {
      list = data.invalidPathList;
    }
    for(let path of list) {
     
      if(path != null) {
        arr.push(path)        
      }
    }    
    //if(arr.length > 1) {
      this.bulkParseService.downloadBulkDocuments(arr,fileType).subscribe(data => {
        window.open(data.response);
      });
    /*} else {
      window.open(arr[0].toString());
    }*/
  }

  bindJobTitleValue(requisition) {
    this.requisitionID = requisition.requisitionId;
    if (this.candidateTagUntagged.value == "untagged" || (this.candidateTagUntagged.value == "tagged" && this.requisitionID != null)) {
      this.showFileUpload = true;
    }
    else {
      this.showFileUpload = false;
    }
  }

  saveBulkResume() {
    if (this.candidateTagUntagged.value == "untagged") {
      this.requisitionID = null;
    }
    let requestObject = {
      "requisitionID": this.requisitionID,
      "bulkResumeArray": this.candidateResumeList
    }
    
    let MaliciousFileError = "Invalid File format";
    this.bulkParseService.uploadBulkResume(requestObject).subscribe(response => {

      if(response.messageCode && response.messageCode.code && response.messageCode.code == "EC201"){
        if( response.messageCode.message != null ){
          MaliciousFileError = response.messageCode.message;
          this.notificationService.setMessage(NotificationSeverity.WARNING, MaliciousFileError);
        }
      }else{
        this.getUploadedResumeList();
        //this.candidateTagUntagged = undefined;
        this.i18UtilService.get('candidate.success.resumeUploaded').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
        this.candidateTagUntagged.reset();
        this.showFileUpload = false;
      }

    });
  }

  uploadDocumentListener(event: any) {
    this.unsupportedFiles = "";
    this.sizeNotSupportedFiles = "";
    this.invalidFileCount = 0;
    this.candidateResumeList = [];
    let fileList = event.target.files;   
    this.validateAndSave(fileList);
    event.target.value = null;
  }


  validateAndSave(fileList) {
    let count = 0;
    let supportedTypes = ['pdf', 'doc', 'docx','zip'];
    try {
      const options = new DocumentValidationOptions(supportedTypes);
      options.isTotalFileSizeChecked = false;
      options.isIndividualFileChecked = true;

      this.documentUploadUtilityService.validateFileBeforeUpload(fileList, options).then((result: UploadDocumentValidationResult) => {
        if (result.unsupportedFiles.length > 0) {
          this.i18UtilService.get('candidate.warning.supportedFileWarning', { 'supportedTypes': supportedTypes.toString() }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
        }
        else
          if (result.sizeNotSupportedFiles.length > 0) {
            this.i18UtilService.get('candidate.warning.resumeSizeExceeded').subscribe((res: string) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, res);
            });
          }
          else {
            for (let tempFile of result.fileList) {
              let fr = new FileReader();
              fr.readAsDataURL(tempFile);
              fr.onloadend = (e) => {
                count++;
                let candidateResume = new CandidateResume();
                candidateResume.attachmentData_base64 = fr.result;
                candidateResume.attachmentName = tempFile.name;
                this.candidateResumeList.push(candidateResume);
                if (count == result.fileList.length) {
                  this.saveBulkResume();
                }
              }
            }
          }
      }).catch(error => {
        this.handleFileUploadError(error);
      })
    } catch (error) {
      this.handleFileUploadError(error);
    }
  }


  handleFileUploadError(error:DocumentValidationErrorCodes){ 
    this.i18UtilService.get('common.warning.document_upload.'+error).subscribe(res=>{
      this.notificationService.setMessage(NotificationSeverity.WARNING,res);
    })
  }
}


export class CandidateResume {
  attachmentData_base64: String | ArrayBuffer;;
  attachmentName: String;
}
