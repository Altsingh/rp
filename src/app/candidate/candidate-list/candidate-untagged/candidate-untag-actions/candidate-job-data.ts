export class TagCandidateToJobData {
    requisitionIDList: Array<Number>;
    organizationID: Number;
    candidateID: Number;
    userID: Number;
}
