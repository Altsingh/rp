import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { CandidateListService } from '../../candidate-list.service';
import { TagCandidateToJobData } from './candidate-job-data';
import { SessionService } from '../../../../session.service';
import { NotificationService } from '../../../../common/notification-bar/notification.service';
import { SelectItem } from "../../../../shared/select-item";
import { TaggedCandidateService } from '../../taggedCandidates/tagged-candidate.service';
import { DialogService } from '../../../../shared/dialog.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-candidate-untag-actions',
  templateUrl: './candidate-untag-actions.component.html'
})
export class CandidateUntagActionsComponent implements OnInit {
  lable: string;
  tagJob: boolean = false;
  data: SelectItem[];
  subscription;
  messageCode: MessageCode;
  newTagCandidateData: TagCandidateData;
  candidateInfo: FormGroup;
  candidateSubscription: any;
  selectedJobId: Number;
  tagResponse: ResponseData;
  message: String;
  severity: String;
  severityClass: string;
  timer: any;
  
  constructor(private candidateListService: CandidateListService, private sessionService: SessionService,
    private formBuilder: FormBuilder, public dialogRef: MatDialogRef<CandidateUntagActionsComponent>,
    private notificationService: NotificationService, private taggedCandidateService: TaggedCandidateService,
    private dialogService: DialogService, private i18UtilService: I18UtilService) {
    this.tagResponse = new ResponseData();
    this.message = "";
  }

  ngOnInit() {
    this.selectedJobId = 0;
    this.candidateInfo = this.formBuilder.group({
      jobIdSelected: [],
    });
    let res: any;
    res = this.dialogService.getDialogData();
    this.data = res.responseData,
      this.messageCode = res.messageCode;
    this.i18UtilService.get('common.label.tag').subscribe((res: string) => {
      this.lable = res;
    });
  }

  saveTagCandidateData(event) {    
    if (this.candidateInfo.controls.jobIdSelected.value == null || this.candidateInfo.controls.jobIdSelected.value.length == 0) {
      this.setMessage("WARNING", 'job.warning.selectJob');
      return;
    }
    this.tagJob = true;
    this.newTagCandidateData = new TagCandidateData();
    this.newTagCandidateData.input = new TagCandidateToJobData();
    this.newTagCandidateData.input.organizationID = this.sessionService.organizationID;
    this.newTagCandidateData.input.requisitionIDList = this.candidateInfo.controls.jobIdSelected.value;
    this.newTagCandidateData.input.candidateID = this.sessionService.object.candidateID;
    let index = this.sessionService.object.index;
    this.newTagCandidateData.input.userID = this.sessionService.userID;
    this.candidateSubscription = this.candidateListService.tagCandidateToJob(this.newTagCandidateData).subscribe(out => {
      this.tagResponse.result = out.responseData[0],
        this.messageCode = out.messageCode;
      if (this.tagResponse.result == "success") {
        this.dialogRef.close();
        this.i18UtilService.get('candidate.success.candidateTagged').subscribe((res: string) => {
          this.notificationService.setMessage("success", res);
         });
        this.candidateListService.getUntaggedCandidateComponent().removeItem(index);
        this.candidateListService.getUntaggedCandidateComponent().ngOnInit();
        //this.candidateListService.getUntaggedCandidateComponent().removeCardFromList(this.sessionService.object.index);
      }
    });
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  public setMessage(severity: string, message: string, args?: any) {
    if (args) {
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
      });

    } else {
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
      });

    }

  }

}

export class ResponseData {
  result: string;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class TagCandidateData {
  input: TagCandidateToJobData;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}

