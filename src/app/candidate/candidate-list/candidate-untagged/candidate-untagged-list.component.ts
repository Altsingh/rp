import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { CandidateListService } from './../candidate-list.service';
import { JsonpModule } from '@angular/http';
import { DialogService } from '../../../shared/dialog.service';
import { CandidateUntagActionsComponent } from './candidate-untag-actions/candidate-untag-actions.component';
import { SessionService } from '../../../session.service';
import { CommonService } from '../../../common/common.service';
import { NameinitialService } from '../../../shared/nameinitial.service'
import { TagCandidateToJobComponent } from '../candidate-untagged/tag-candidate-to-job/tag-candidate-to-job.component'
import { PageEvent, DataTable } from '../../../shared/alt-data-table/DataTable';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { CandidateFilterTo } from '../candidate-filter-to';
import { UntaggedCandidatesPagedService } from './untagged-candidate-paged.service';
import { PageState } from '../../../shared/paginator/paginator.component';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-candidate-untagged-list',
  templateUrl: './candidate-untagged-list.component.html',
  providers: [JsonpModule, DialogService, UntaggedCandidatesPagedService]
})
export class CandidateUntaggedListComponent implements OnInit, OnDestroy {

  pageState: PageState = new PageState();
  isToggled: boolean;
  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;

  showloader: boolean = false;


  loaderIndicator: string;
  data: ResponseData[];
  messageCode: MessageCode;
  subscription;
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";
  selected: any[] = [];
  selectAll: boolean;
  filterTO: CandidateFilterTo;

  constructor(private candidateListService: CandidateListService, private dialogsService: DialogService,
    private sessionService: SessionService, private commonService: CommonService,
    private nameinitialService: NameinitialService,
    private notificationService: NotificationService,
    private untaggedCandidatesPagedService: UntaggedCandidatesPagedService,
    private formBuilder: FormBuilder,
    private i18UtilService: I18UtilService) {
    this.data = [];
    this.candidateListService.setUntaggedCandidateComponent(this);
    this.commonService.showRHS();

    this.filterTO = new CandidateFilterTo();

    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });

    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {

        let query = this.queryFormGroup.controls.query.value;

        if (query.length >= 3) {

          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          this.untaggedCandidatesPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.untaggedCandidatesPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
      }, 200);

    });
  }
  getLength(str: String) {
    if (str) {
      return str.length;
    } else {
      return 0;
    }
  }

  ngOnInit() {

    this.selected = [];
    this.loaderIndicator = "loader";
    this.pageState.currentPageNumber = 1;
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    
    this.nameinitialService.randomize();
    this.randomcolor = this.nameinitialService.randomcolor;


    this.loadData();
    this.loadTotalRows();
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;

    this.loadData();


  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.untaggedCandidatesPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      if(response!=null && response!=-1){
        this.pageState.totalRows = response;
      }
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.untaggedCandidatesPagedService.getPageWithTotalRecords(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.data = response.data;
        if(response.totalRecords !=null && response.totalRecords != -1){
          this.pageState.totalRows = response.totalRecords;
        }
        if (response.data.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  areAllSelected() {
    if (this.data != null && this.data.length > 0) {
      for (let x = 0; x < this.data.length; x++) {
        if (typeof this.data[x].selected == 'undefined' || (typeof this.data[x].selected == 'boolean' && !this.data[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }
  imageError(event, candidate) {
    candidate.profileImage = null;
  }
  ngOnDestroy(): void {
  }

  removeItem(candidateItemIndex: number) {
    this.data.splice(candidateItemIndex, 1);
    this.untaggedCandidatesPagedService.removeCachedItem(this.pageState.currentPageNumber, this.pageState.pageSize, candidateItemIndex);
  }

  tagCandidateToJobDlg(candidateId: number, index: number) {
    this.sessionService.object = {
      candidateID: candidateId,
      index: index
    };
    this.showloader = true;
    this.subscription = this.candidateListService.getJobsOfRecruiter().subscribe(res => {
      this.dialogsService.setDialogData(res);
      this.showloader = false;
      this.dialogsService.open(CandidateUntagActionsComponent, { width: '285px' });
    });


  }

  public removeCardFromList(index: number) {
    if (index !== -1) {
      this.data.splice(index, 1);
    }
  }

  clearCache() {
    this.untaggedCandidatesPagedService.clearCache();
  }
  openBulkAction() {
    this.notificationService.clear();
    if (this.selected.length == 0) {
      this.i18UtilService.get('candidate.warning.selectCandidate').subscribe((res: string) => {
        this.notificationService.setMessage("WARNING", res);
      });
      return;
    }
    this.candidateListService.setDataList(this.selected)
    this.showloader = true;
    this.candidateListService.getJobsOfRecruiter().subscribe(out => {
      this.dialogsService.setDialogData(out);
      this.showloader = false;
      this.dialogsService.open(TagCandidateToJobComponent, { width: '935px' })
    });
  }

  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();
  }

  allSelectEvent() {
    let index = 0;
    for (let data of this.data) {
      data.selected = this.selectAll;
      index++;
      if (this.selectAll && !this.selected.includes(data)) {
        this.selected.push(data);
      } else if (!this.selectAll) {
        let i = this.selected.indexOf(data);
        this.selected.splice(i, 1);
      }
    }
  }

  onSorted(params: string[]) {
    this.filterTO.sortBy = params[0];
    this.filterTO.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.untaggedCandidatesPagedService.clearCache();
    this.loadData();
    this.loadTotalRows();
  }

  onFilter(params: any[]) {
    let genderFilter = false;
    let sourceFilter = false;
    let industryFilter = false;
    let locationFilter = false;
    let functionalAreaFilter = false;
    let minExperienceFilter = false;
    let maxExperienceFilter = false;
    for (let param of params) {
      if (param.name == 'LOCATION') {
        this.filterTO.location = param.value.value;
        locationFilter = true;
      } else if (param.name == 'GENDER') {
        this.filterTO.genderID = param.value.id;
        genderFilter = true;
      } else if (param.name == 'INDUSTRY') {
        this.filterTO.industryID = param.value.id;
        industryFilter = true;
      } else if (param.name == 'FUNCTIONAL_AREA') {
        this.filterTO.functionalAreaID = param.value.id;
        functionalAreaFilter = true;
      } else if (param.name == 'SOURCE') {
        this.filterTO.sourceID = param.value.id;
        sourceFilter = true;
      } else if(param.name == 'MIN_EXPERIENCE'){
        this.filterTO.minExperience = param.value.value;
        minExperienceFilter = true;
      } else if(param.name == 'MAX_EXPERIENCE'){
        this.filterTO.maxExperience = param.value.value;
        maxExperienceFilter = true;
      }

    }
    if (!genderFilter) {
      this.filterTO.genderID = null;
    }
    if (!industryFilter) {
      this.filterTO.industryID = null;
    }
    if (!functionalAreaFilter) {
      this.filterTO.functionalAreaID = null;
    }
    if (!locationFilter) {
      this.filterTO.location = null;
    }
    if (!sourceFilter) {
      this.filterTO.sourceID = null;
    }
    if(!minExperienceFilter){
      this.filterTO.minExperience = null;
    }
    if(!maxExperienceFilter){
      this.filterTO.maxExperience = null;
    }
    this.untaggedCandidatesPagedService.clearCache();
    this.ngOnInit();
  }
  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    } else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    this.untaggedCandidatesPagedService.clearCache();
    this.ngOnInit();
  }

  getProfileCompletionPercentage(item) {
    if (item != null) {
      if (item.requesting) {
        return 'Loading...';
      }
      item.requesting = true;
      if (item.profileCompletionLoading) {
        return 'Loading...';
      }

      if (item.profileCompletionLoaded) {
        item.profileCompletionLoading = false;
        item.requesting = false;
        return item.profileCompletionPercentage + " %";
      } else {

        Promise.resolve(this.candidateListService.getProfileCompletionPercentage(item.candidateID)).then((response) => {
          if (response) {
            if (response.responseData) {
              if (response.responseData.length > 0) {
                let percentage = response.responseData[0].percentage;
                item.profileCompletionLoaded = true;
                item.profileCompletionLoading = false;
                item.profileCompletionPercentage = percentage;
                item.requesting = false;
                item.percentClass = "p" + item.profileCompletionPercentage;
                return item.profileCompletionPercentage + " %";
              }
            }
          }
          return item.profileCompletionPercentage;
        });
      }

      return 'Loading...';
    } else {
      return 'Loading...';
    }
  }

  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }

}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export interface ResponseData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  primaryEmail: String;
  mobileNumber: String;
  profileImage: String;
  city: String;
  skills: String;
  candidateID: number;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
}