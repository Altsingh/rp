import { Component, OnInit, Input } from '@angular/core';
import { SessionService } from '../../../../session.service';

@Component({
  selector: 'alt-untagged-context-menu',
  templateUrl: './untagged-context-menu.component.html',
  styleUrls: ['./untagged-context-menu.component.css']
})
export class UntaggedContextMenuComponent implements OnInit {

  @Input()
  candidateCode: string;
  featureDataMap:Map<any,any>;
  displayClass: String = "displayNone";

  constructor(private sessionService: SessionService) { 
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  ngOnInit() {
  }

  hideContextMenu() {
    this.displayClass = "displayNone";
  }

  showContextMenu() {
    this.displayClass = "displayBlock";
  }

}
