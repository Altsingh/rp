import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { MatDialogRef } from '@angular/material';
import { CandidateListService } from '../../candidate-list.service';
import { JobListService } from '../../../../job/job-list/job-list.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SessionService } from '../../../../session.service';
import { SelectItem } from "../../../../shared/select-item";
import { DialogService } from '../../../../shared/dialog.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-tag-candidate-to-job',
  templateUrl: './tag-candidate-to-job.component.html',
  styleUrls: ['./tag-candidate-to-job.component.css'],
  providers: [JobListService]
})
export class TagCandidateToJobComponent implements OnInit {
  tagCandidate: boolean = false;
  lable: string ;
  candidatelist: candidateListResponse[];
  data: SelectItem[];
  selected: any[] = [];
  selectAll: boolean;
  candidateInfo: FormGroup;
  newTagCandidateData: candidateListData;
  tagResponse: ResponseData;
  messageCode: MessageCode;
  selectedJobId: Number;
  subscription: any;
  timer: any;
  severity: String;
  severityClass: string;
  message: string;

  constructor(public dialogRef: MatDialogRef<TagCandidateToJobComponent>,
    private candidateListService: CandidateListService,
    private jobListService: JobListService, private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private sessionService: SessionService, private dialogService: DialogService,
    private i18UtilService: I18UtilService) {
    this.tagResponse = new ResponseData();
    this.message = "";
  }

  ngOnInit() {
    this.selectedJobId = 0;
    this.candidateInfo = this.formBuilder.group({
      jobIdSelected: [],
    });

    this.candidatelist = this.candidateListService.getDataList();
    let out = this.dialogService.getDialogData();
    this.messageCode = out.messageCode,
      this.data = out.responseData
      this.i18UtilService.get('common.label.tag').subscribe((res: string) => {
        this.lable=res;
      });
  }
  assignCandidateJob(event) {
    this.notificationService.clear();    
    if (this.candidateInfo.controls.jobIdSelected.value == null || Object.keys(this.candidateInfo.controls.jobIdSelected.value).length == 0) {
      this.setMessage("WARNING", 'job.warning.selectJob');
      return;
    }
    this.tagCandidate = true;
    let candidateIdList: any[] = [];
    for (let canId of this.candidatelist) {
      candidateIdList.push(canId.candidateID);
    }
    this.newTagCandidateData = new candidateListData();
    this.newTagCandidateData.requisitionIDList = this.candidateInfo.controls.jobIdSelected.value;
    this.newTagCandidateData.candidateIDList = candidateIdList;
    this.newTagCandidateData.userID = this.sessionService.userID;
    this.newTagCandidateData.organizationID = this.sessionService.organizationID;
    this.candidateListService.tagCandidateToJobBulk(this.newTagCandidateData).subscribe(out => {
      this.tagResponse.result = out.responseData[0],
        this.messageCode = out.messageCode;
      if (this.tagResponse.result == "success") {
        this.dialogRef.close();
        this.i18UtilService.get('candidate.success.candidateTagged').subscribe((res: string) =>{
          this.notificationService.setMessage("success", res );
        });
        this.candidateListService.getUntaggedCandidateComponent().clearCache();
        this.candidateListService.getUntaggedCandidateComponent().ngOnInit();
        //this.candidateListService.getUntaggedCandidateComponent().removeCardFromList(this.sessionService.object.index);
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  public setMessage(severity: string, message: string) {
    
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
      this.message = res;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
    });
    
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
export class ResponseData {
  result: string;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class candidatelist {
  candidateCode: Array<string>;
}
export class joblist {
  reqid: Array<string>;
}
export class candidateListData {
  candidateIDList: Array<Number>;
  requisitionIDList: Array<Number>;
  userID: Number;
  organizationID: Number;
}
export class candidateListResponse {
  candidateID: Array<Number>;
}