import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'untaggedCandidateDataFilter'
})
export class UntaggedCandidateDataFilterPipe implements PipeTransform {

  transform(array: any[], query: string): any {
    if (query) {
      query = query.replace(new RegExp(' ', 'g'),  '');
      return _.filter(array, row => (
          (row.firstName != null && row.firstName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1)
       || (row.middleName != null && row.middleName.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.lastName != null && row.lastName.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.firstName != null && row.middleName != null && (row.firstName.trim().toLowerCase()+row.middleName.trim().toLowerCase()).indexOf(query.trim().toLowerCase()) > -1)
       || (row.firstName != null && row.lastName != null && (row.firstName.trim().toLowerCase()+row.lastName.trim().toLowerCase()).indexOf(query.trim().toLowerCase()) > -1)
       || (row.firstName != null && row.middleName != null && row.lastName != null && (row.firstName.trim().toLowerCase()+row.middleName.trim().toLowerCase()+row.lastName.trim().toLowerCase()).indexOf(query.trim().toLowerCase()) > -1)
       || (row.requisitionCode != null && row.requisitionCode.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.candidateCode != null && row.candidateCode.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.location != null && row.location.toLowerCase().indexOf(query.toLowerCase()) > -1)));
    }
    return array;
  }

}
