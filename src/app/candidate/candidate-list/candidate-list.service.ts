import { Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Jsonp, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from '../../session.service';

import { TagCandidateToJobData } from './candidate-untagged/candidate-untag-actions/candidate-job-data';
import { CandidateUntaggedListComponent } from './candidate-untagged/candidate-untagged-list.component';
import { CandidateFilterTo } from './candidate-filter-to';

@Injectable()
export class CandidateListService {
    datalist: any[];
    untaggedCandidateList: CandidateUntaggedListComponent;

    constructor(private http: Http, private sessionService: SessionService) {
    }

    public getList() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch/all/list/' + this.sessionService.organizationID + "/").
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });

    }

    public getUntaggedList(filterTO: CandidateFilterTo) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/fetch/untagged/list/', JSON.stringify(filterTO), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json();
            });

    }
    setDataList(list) {
        this.datalist = list
    }
    getDataList() {
        return this.datalist;
    }


    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }

    public getJobsOfRecruiter() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/recruiterJobs/' + this.sessionService.userID + "/").
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getProfileCompletionPercentage(candidateId): Promise<any> {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/getProfileCompletionPercentageActual/', { candidateId: candidateId }, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            }).toPromise();
    }

    getTagCandidateURL() {
        return (this.sessionService.orgUrl + '/rest/altone/candidateDetail/tagCandidate/');
    }

    getTagCandidateBulkURL() {
        return (this.sessionService.orgUrl + '/rest/altone/candidateDetail/tagMultipleCandidates');
    }

    public tagCandidateToJobBulk(tagCandidateData: TagCandidateDataBulk) {
        let options = new RequestOptions();
        return this.http.post(this.getTagCandidateBulkURL(), tagCandidateData, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json();
            });
    }

    public tagCandidateToJob(tagCandidateData: TagCandidateData) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getTagCandidateURL(), JSON.stringify(tagCandidateData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json();
            });
    }

    public setUntaggedCandidateComponent(untaggedCandidates: CandidateUntaggedListComponent) {
        this.untaggedCandidateList = untaggedCandidates;
    }

    public getUntaggedCandidateComponent(): CandidateUntaggedListComponent {
        return this.untaggedCandidateList;
    }

    getJobStatusByRequisitionID(requisitionId: number) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getJobStatus/' + requisitionId + '/').map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()
        });
    }
}

export class TagCandidateData {
    input: TagCandidateToJobData;
}
export class TagCandidateDataBulk {
    candidateIDList: Array<Number>;
    requisitionIDList: Array<Number>;
    userID: Number;
}