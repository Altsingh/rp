export class TransferCandidateToJobData {
    requisitionID: Number;
    candidateID: Number;
    userID: Number;
    applicantStatusId: Number;
    transferToRequisitionId: Number;
    transferToStage: String;
    transferActionType: String;
}