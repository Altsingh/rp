import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../../common/notification-bar/notification.service';
import { SessionService } from '../../../../session.service';
import { DialogService } from '../../../../shared/dialog.service';
import { SelectItem } from "../../../../shared/select-item";
import { TaggedCandidateService } from '../../taggedCandidates/tagged-candidate.service';
import { TransferCandidateToJobData } from './copy-candidate-data';
import { ArrayObservable } from 'rxjs/observable/ArrayObservable';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-candidate-actions',
  templateUrl: './candidate-actions.component.html',
  styleUrls: ['./candidate-actions.component.css']
})
export class CandidateActionsComponent implements OnInit {
  label: string;
  moveLabel: string;
  copyLabel: string;
  transferCandidate: boolean = false;
  data: SelectItem[];
  jobList: any[];
  filteredJobList: any[];

  subscription;
  messageCode: MessageCode;
  candidateInfo: FormGroup;
  selectedJobId: Number;
  message: String;
  severity: String;
  severityClass: string;
  timer: any;
  candidateNewJobInfo: TransferCandidateToAnotherJobData;
  copyCandidateRespone: ResponseData;
  selected: any;
  actionList: Actions[] = [];
  candidateName: String = "";
  candidateCode: String = "";

  constructor(private sessionService: SessionService,
    private formBuilder: FormBuilder, public dialogRef: MatDialogRef<CandidateActionsComponent>,
    private notificationService: NotificationService, private taggedCandidateService: TaggedCandidateService,
    private dialogsService: DialogService, private i18UtilService: I18UtilService) {
    this.copyCandidateRespone = new ResponseData();
    this.message = "";
    this.i18UtilService.get('common.label.move').subscribe((res: string) => {
      this.moveLabel = res;
    });
    this.i18UtilService.get('common.label.copy').subscribe((res: string) => {
      this.copyLabel = res;
    });
  }

  ngOnInit() {
    this.selectedJobId = 0;
    this.initializeCandidateInfo();
    this.initActionList();
    this.initData();
    let res = this.dialogsService.getDialogData();
    this.data = res.responseData;
    this.messageCode = res.messageCode;
    this.filterJobList();
    this.i18UtilService.get('common.label.transfer').subscribe((res: string) => {
      this.label = res;
    });
  }

  public transferCandidateToAnotherJob(event) {
    if (!this.validateCandidateTransfer()) {
      return;
    }
    this.transferCandidate = true;
    if ((this.candidateInfo.controls.stageSelected.value == "current" && this.sessionService.object.workflowID == this.selected.description) || this.candidateInfo.controls.stageSelected.value == "initial") {
      this.candidateNewJobInfo = new TransferCandidateToAnotherJobData();
      this.candidateNewJobInfo.input = new TransferCandidateToJobData();
      this.candidateNewJobInfo.input.transferToRequisitionId = this.candidateInfo.controls.jobIdSelected.value;
      this.candidateNewJobInfo.input.transferToStage = this.candidateInfo.controls.stageSelected.value;
      this.candidateNewJobInfo.input.transferActionType = this.label;
      this.candidateNewJobInfo.input.userID = this.sessionService.userID;
      this.candidateNewJobInfo.input.applicantStatusId = this.sessionService.object.applicantStatusId;
      this.candidateNewJobInfo.input.requisitionID = this.sessionService.object.currentReqId;
      this.candidateNewJobInfo.input.candidateID = this.sessionService.object.candidateId;
      this.taggedCandidateService.transferCandidateToAnotherJob(this.candidateNewJobInfo).subscribe(out => {
        this.copyCandidateRespone.result = out.responseData[0];
        this.messageCode = out.messageCode;
        if (this.copyCandidateRespone.result == "success") {
          this.dialogRef.close();
          this.taggedCandidateService.getTaggedCandidateComponent().clearCache();
          this.taggedCandidateService.getTaggedCandidateComponent().ngOnInit();
          this.i18UtilService.get('candidate.success.candidateTransferred').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
        } else {
          this.transferCandidate = false;
          this.setMessage("WARNING", this.copyCandidateRespone.result);
        }
      });
    } else {
      this.setMessage("WARNING",'candidate.warning.differentWorkflowForCopy', {'candidateName': this.candidateName});
      this.transferCandidate = false;
    }
  }

  public getTaggedCandidates() {
    this.taggedCandidateService.getTaggedCandidates(null).subscribe(res => {
      this.messageCode = res.messageCode;
      this.taggedCandidateService.setTaggedCandidateComponent(res.responseData[0].taggedCandidates);
    });
  }
  public setMessage(severity: string, message: string, args?: any) {
    if(!isNullOrUndefined(args)){
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
        this.timer = setTimeout(() => {
          this.clearMessages();
          this.timer = null;
        }, 3000);
       });
     
    }else{
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
        this.timer = setTimeout(() => {
          this.clearMessages();
          this.timer = null;
        }, 3000);
       });
    }
    
  }
  bindAction(event) {
    this.i18UtilService.get(event.label).subscribe((res: string) => {
      this.label = res;
    });
  }
  bindWorkflowID(event) {
    this.selected = event;
  }
  clearMessages() {
    this.severity = "";
    this.message = "";
  }
  initActionList() {
    this.actionList.push({
      value: 1,
      label:  this.copyLabel,
    });
    this.actionList.push({
      value: 2,
      label: this.moveLabel,
    });
  }
  initializeCandidateInfo() {
    this.candidateInfo = this.formBuilder.group({
      jobIdSelected: [],
      actionSelected: '',
      stageSelected: ''
    });
  }
  initData() {
    this.candidateName = this.sessionService.object.candidateName;
    this.candidateCode = this.sessionService.object.candidateCode;
  }
  validateCandidateTransfer() {
    let valid: boolean = true;
    this.notificationService.clear();
    if (this.candidateInfo.controls.actionSelected.value == null || this.candidateInfo.controls.actionSelected.value == '') {
      this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.selectAction');
      valid = false;
      return;
    }
    if (this.candidateInfo.controls.stageSelected.value == null || this.candidateInfo.controls.stageSelected.value == '') {
      this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.selectStage');
      valid = false;
      return;
    }
    if (this.candidateInfo.controls.jobIdSelected.value == null) {
      this.setMessage(NotificationSeverity.WARNING,'job.warning.selectJob');
      valid = false;
    }
   
    return valid;
  }
  syncJobList(flag: boolean) {
    if (flag) {
      this.data = this.filteredJobList;
    } else {
      this.data = this.jobList;
    }
  }
  filterJobList() {
    this.jobList = this.data.slice();
    this.filteredJobList = this.data.slice();
    for (let item of this.filteredJobList) {
      if (item.description != this.sessionService.object.workflowID) {
        let index = this.filteredJobList.indexOf(item);
        if (index > -1)
          this.filteredJobList.splice(index, 1);
      }
    }
  }
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
export class ResponseData {
  jobCode: String;
  jobTitle: String;
  requisitionID: number;
  result: string;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class TransferCandidateToAnotherJobData {
  input: TransferCandidateToJobData;
}
export class Actions {
  value: Number;
  label: String;
}