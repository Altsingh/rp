import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TaggedCandidateService } from '../taggedCandidates/tagged-candidate.service';
import { MessageCode } from '../../../common/message-code';
import { ActivatedRoute } from '@angular/router';
import { NotificationService,NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { DialogService } from '../../../shared/dialog.service';
import { CandidateActionsComponent } from './candidate-actions/candidate-actions.component';
import { SessionService } from '../../../session.service';
import { CommonService } from '../../../common/common.service';
import { EmailTemplateComponent } from "../../email-template/email-template.component";
import { CandidateListService } from '../candidate-list.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-candidate-context-menu',
  templateUrl: './candidate-context-menu.component.html'
})
export class CandidateContextMenuComponent implements OnInit {
  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() applicantStatusId: number;
  @Input() candidateCode: string;
  @Input() candidateName: string;
  @Input() index: number;
  @Input() sourceType: String;
  @Input() currentReqId: Number;
  @Input() candidateId: Number;
  @Input() allowCopyMove: boolean;
  @Input() primaryEmail: String;
  @Input() inReview:boolean;
  @Input() sendCPCredentials:boolean;
  @Input() reSendCPCredentials:boolean;
  @Input() untagCandidateEnabled:boolean;
  @Input() candidateItemIndex: number;
  @Output() showloader:EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() workflowID: any;
  @Input() jobStatus: String;
  
  displayClass: String = "displayNone";
  data: ResponseData;
  messageCode: MessageCode;
  subscription;
  taggedCandidateListSubsciption: any;
  isActive:boolean = true;
  featureDataMap:Map<any,any>;

  constructor(private taggedCandidateService: TaggedCandidateService, private route: ActivatedRoute,
    private notificationService: NotificationService, private dialogsService: DialogService,
    private sessionService: SessionService, private commonService: CommonService,
     private candidateListService: CandidateListService,
     private i18UtilService : I18UtilService) {
    this.data = new ResponseData();
    this.messageCode = new MessageCode();
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  ngOnInit() {
    
    if(this.jobStatus == 'CLOSED' || this.jobStatus == 'ONHOLD' || this.jobStatus == 'CANCELLED')
    {
      this.isActive =false;
    }
  }

  public unTagApplicantFromJob() {
    this.subscription = this.taggedCandidateService.unTagApplicantFromJob(this.applicantStatusId).subscribe(res => {
      this.data.result = res.response.responseData[0];
      this.messageCode = res.messageCode;
      if (this.data.result == true) {
        this.hideContextMenu();
        this.i18UtilService.get('candidate.success.candidateUntagged').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
        this.taggedCandidateService.getTaggedCandidateComponent().removeItem(this.candidateItemIndex);
        this.taggedCandidateService.getTaggedCandidateComponent().pageState.totalRows--;
        this.taggedCandidateService.getTaggedCandidateComponent().ngOnInit();
      }
    });
  }

  hideContextMenu() {
    this.displayClass = "displayNone";
  }
  showContextMenu() {
    this.displayClass = "displayBlock";
  }
  public openCopyCandidateToAnotherJobDlg() {
    this.hideContextMenu();
    if (this.sourceType == "IJP REFERRAL") {
      this.i18UtilService.get('candidate.error.cantCopyIJPCandidate').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    } else {
      this.sessionService.object = {
        applicantStatusId: this.applicantStatusId,
        currentReqId: this.currentReqId,
        candidateId: this.candidateId,
        workflowID: this.workflowID,
        index: this.index,
        candidateCode: this.candidateCode,
        candidateName: this.candidateName
      };
      this.change.emit(true);
      this.subscription = this.candidateListService.getJobsOfRecruiter().subscribe(res => {
        this.dialogsService.setDialogData(res);
        this.change.emit(false);
        this.dialogsService.open(CandidateActionsComponent, { width: '480px' });
      });
    }
  }
  public openEmailPopUp() {
    this.hideContextMenu();
    this.sessionService.object = {
      applicantStatusId: this.applicantStatusId,
      currentReqId: this.currentReqId,
      candidateId: this.candidateId,
      index: this.index,
      primaryEmail: this.primaryEmail
    };
    this.dialogsService.open(EmailTemplateComponent, { width: '935px' });
  }

  public getCPCredentials(applicantStatusId) {
    this.hideContextMenu();
    this.showloader.emit(true);
    this.commonService.getCPCredentials(applicantStatusId).subscribe((response) => {
      if (response.messageCode != null && response.messageCode.code === 'EC200'){
        this.sessionService.object = {
          applicantStatusId: this.applicantStatusId,
          cpFilter:'cpcredentials' ,
          responseData: response,
        };
        this.showloader.emit(false);
        this.dialogsService.open(EmailTemplateComponent, { width: '935px' });
      } else{
        this.showloader.emit(false);
        this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
      });
  }
}
export class ResponseData {
  result: boolean;
}

