import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { Router } from '@angular/router';
import { BulkUpdateStatusComponent } from "app/candidate/candidate-list/taggedCandidates/bulk-update-status/bulk-update-status.component";
import { SessionService } from "app/session.service";
import { Subscription } from 'rxjs/Subscription';
import { CommonService } from '../../../common/common.service';
import { NotificationService } from "../../../common/notification-bar/notification.service";
import { DialogService } from '../../../shared/dialog.service';
import { NameinitialService } from '../../../shared/nameinitial.service';
import { PageState } from '../../../shared/paginator/paginator.component';
import { WorkflowStageMapService } from '../../../shared/services/workflow-stage-map.service';
import { TransferCandidateToJobData } from "../candidate-context-menu/candidate-actions/copy-candidate-data";
import { CandidateFilterTo } from '../candidate-filter-to';
import { CandidateListService } from '../candidate-list.service';
import { CopyToAnotherJobComponent } from './copy-to-another-job/copy-to-another-job.component';
import { EmailCandidatesComponent } from './email-candidates/email-candidates.component';
import { NotifyRecruiterComponent } from './notify-recruiter/notify-recruiter.component';
import { TaggedCandidateService } from './tagged-candidate.service';
import { TaggedCandidatesPagedService } from './tagged-candidates-paged.service';
import { UntaggedCandidateBulkActionComponent } from './untagged-candidate-bulk-action/untagged-candidate-bulk-action.component';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-tagged-candidate-list',
  templateUrl: './tagged-candidate-list.component.html',
  providers: [JsonpModule, DialogService, TaggedCandidatesPagedService]
})
export class TaggedCandidateListComponent implements OnInit, OnDestroy {

  isToggled: boolean;
  pageLoading: boolean = false;
  pageState: PageState = new PageState();

  queryFormGroup: FormGroup;

  pageLoadingSubscription: Subscription;
  totalRowsSubscription: Subscription;
  workflowStageMapSubscription: Subscription;
  workflowStageMap: any;

  queryTimeout: any;
  showloader: boolean = false;
  loaderIndicator: string;

  data: ResponseData[];
  messageCode: MessageCode;
  subscription;
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  selected: any[] = [];

  selectAll: boolean;
  filterTO: CandidateFilterTo;
  automatchEnabled: boolean;


  dataAfterUntag: ResponseData[];
  featureDataMap: Map<any, any>;

  isMyApplicants: boolean = false;
  pageTitle: string;
  disableTakeAction : boolean = false;

  responseData: ResponseData;
  constructor(private taggedCandidateService: TaggedCandidateService,
    private commonService: CommonService, private nameinitialService: NameinitialService,
    private notificationService: NotificationService, private candidateListService: CandidateListService,
    private dialogsService: DialogService,
    private taggedCandidatesPagedService: TaggedCandidatesPagedService,
    private formBuilder: FormBuilder,
    private router: Router,
    private sessionService: SessionService,
    private workflowStageMapService: WorkflowStageMapService,
    private i18UtilService: I18UtilService) {
    this.automatchEnabled = this.sessionService.isAutoMatchEnabled();
    this.featureDataMap = this.sessionService.featureDataMap;
    this.workflowStageMapSubscription = this.workflowStageMapService.getMap().subscribe((response) => {
      this.workflowStageMap = response;
      this.totalRowsSubscription = null
    });

    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });

    this.pageTitle = "TAGGED CANDIDATES";
    if (this.router.url.indexOf('/myapplicants') > -1) {
      this.isMyApplicants = true;
      this.pageTitle = "MY APPLICANTS";
      if (this.filterTO != undefined && this.filterTO != null) {
        this.filterTO.myApplicants = true;
      }
    }

    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {

        let query = this.queryFormGroup.controls.query.value;

        if (query.length >= 3) {

          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          this.taggedCandidatesPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.taggedCandidatesPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
      }, 200);

    });

    commonService.showRHS();
    this.randomcolor = nameinitialService.randomcolor;
    this.data = [];
    this.taggedCandidateService.setTaggedCandidateComponent(this);
    this.filterTO = new CandidateFilterTo();
    this.filterTO.myApplicants = this.isMyApplicants;

    if(this.featureDataMap.get('CANDIDATE_OVERVIEW') != 1 && this.featureDataMap.get('HIRING_PROCESS') != 1 && this.featureDataMap.get('CANDIDATE_HISTORY') != 1 && this.featureDataMap.get('WORKFLOW_HISTORY') != 1){
      this.disableTakeAction = true;
    }
  }

  clearCache() {
    this.taggedCandidatesPagedService.clearCache();
  }
  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;

    this.loadData();


  }

  removeItem(candidateItemIndex: number) {
    this.data.splice(candidateItemIndex, 1);
    this.taggedCandidatesPagedService.removeCachedItem(this.pageState.currentPageNumber, this.pageState.pageSize, candidateItemIndex);
  }

  ngOnInit() {
    this.nameinitialService.randomize();
    this.randomcolor = this.nameinitialService.randomcolor;
    this.selected = [];
    this.loaderIndicator = "loader";
    this.pageState.currentPageNumber = 1;
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;



    this.taggedCandidatesPagedService.clearCache();
    this.loadData();
    this.loadTotalRows();

  }
  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.taggedCandidatesPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    this.pageLoadingSubscription = this.taggedCandidatesPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.data = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }
  ngOnDestroy(): void {

    // this.subscription.unsubscribe();
  }
  public removeCardFromList(indexlist) {
    if (indexlist instanceof Array) {
      for (let index of indexlist) {
        if (index !== -1) {
          this.data.splice(index, 1);
        }
      }
    } else if (indexlist !== -1) {
      this.data.splice(indexlist, 1);
    }
  }

  imageError(event, candidate) {
    candidate.profileImage = null;
  }
  changed(event) {
    this.showloader = event;
  }
  openBulkAction(actionType) {
    this.notificationService.clear();
    if (this.selected.length == 0) {
      this.i18UtilService.get('candidate.warning.selectCandidate').subscribe((res: string) => {
        this.notificationService.setMessage("WARNING", res);
      });
      return;
    }
    this.candidateListService.setDataList(this.selected);
    if (actionType == 'U') {
      this.dialogsService.open(UntaggedCandidateBulkActionComponent, { width: '935px' });
    } else if (actionType == 'C') {
      this.showloader = true;
      this.candidateListService.getJobsOfRecruiter().subscribe(out => {
        this.dialogsService.setDialogData(out);
        this.showloader = false;
        this.dialogsService.open(CopyToAnotherJobComponent, { width: '480px' });
      });
    } else if (actionType == 'E') {
      this.dialogsService.open(EmailCandidatesComponent, { width: '935px' });
    } else if (actionType == 'R') {
      this.dialogsService.open(NotifyRecruiterComponent, { width: '935px' });
    }
    else if (actionType == 'T') {
      let index = 0;
      let flag = true;
      let selectedList: any[];
      let workflowStageID: any = this.selected[0].workflowStageID;
      let stageStatusType: any = this.selected[0].stageStatusType;



      for (let data of this.selected) {
        if (data.workflowStageID != workflowStageID || data.stageStatusType != stageStatusType) {
          flag = false;
        }
      }
      if (flag == true) {
        this.sessionService.object = {
          selectedList: this.selected
        };
        if (this.selected[0].sysWorkflowStageType == "SHORTLISTING" || this.selected[0].sysWorkflowStageType == "SCREENING" || this.selected[0].sysWorkflowStageType == "INTERVIEW_1" || this.selected[0].sysWorkflowStageType == "INTERVIEW_1" || this.selected[0].sysWorkflowStageType == "INTERVIEW_2" || this.selected[0].sysWorkflowStageType == "INTERVIEW_3" || this.selected[0].sysWorkflowStageType == "INTERVIEW_4" || this.selected[0].sysWorkflowStageType == "INTERVIEW_5" || this.selected[0].sysWorkflowStageType === "HR_INTERVIEW") {
          this.dialogsService.open(BulkUpdateStatusComponent, { width: '680px' });
        }
        else {
          this.notificationService.addMessage("WARNING", "This stage is not allowed for bulk action!");
        }
      }
      else {
        this.notificationService.addMessage("WARNING", "The selected candidates are not at the same stage and status");
      }
    }
  }
  getLength(str: String) {
    if (str) {
      return str.length;
    } else {
      return 0;
    }
  }

  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();
  }

  allSelectEvent() {
    let index = 0;


    for (let data of this.data) {
      let isActive = true;
      if (data.requisitionStatus == "CLOSED" || data.requisitionStatus == "ONHOLD" || data.requisitionStatus == "CANCELLED") {
        isActive = false;
      }
      if ((data.inReview == false || data.inReview == null) && isActive) {
        data.selected = this.selectAll;
        index++;
        if (this.selectAll && !this.selected.includes(data)) {
          this.selected.push(data);
        } else if (!this.selectAll) {
          let i = this.selected.indexOf(data);
          this.selected.splice(i, 1);
        }

      }
    }

  }

  getProfileCompletionPercentage(item) {
    if (item != null) {
      if (item.requesting) {
        return 'Loading...';
      }
      item.requesting = true;
      if (item.profileCompletionLoading) {
        return 'Loading...';
      }

      if (item.profileCompletionLoaded) {
        item.profileCompletionLoading = false;
        item.requesting = false;
        return item.profileCompletionPercentage + " %";
      } else {

        Promise.resolve(this.candidateListService.getProfileCompletionPercentage(item.candidateID)).then((response) => {
          if (response) {
            if (response.responseData) {
              if (response.responseData.length > 0) {
                let percentage = response.responseData[0].percentage;
                item.profileCompletionLoaded = true;
                item.profileCompletionLoading = false;
                item.profileCompletionPercentage = percentage;
                item.requesting = false;
                item.percentClass = "p" + item.profileCompletionPercentage;
                return item.profileCompletionPercentage + " %";
              }
            }
          }
          return item.profileCompletionPercentage;
        });
      }

      return 'Loading...';
    } else {
      return 'Loading...';
    }
  }

  areAllSelected() {
    if (this.data != null && this.data.length > 0) {
      for (let x = 0; x < this.data.length; x++) {
        if (typeof this.data[x].selected == 'undefined' || (typeof this.data[x].selected == 'boolean' && !this.data[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }

  onSorted(params: string[]) {
    this.filterTO.sortBy = params[0];
    this.filterTO.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    this.filterTO.pageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.pageState.currentPageNumber = 1;
    this.taggedCandidatesPagedService.clearCache();
    this.loadData();
    this.loadTotalRows();
  }

  onFilter(params: any[]) {
    let genderFilter = false;
    let sourceFilter = false;
    let industryFilter = false;
    let locationFilter = false;
    let functionalAreaFilter = false;
    let stageStatusFilter = false;
    let minExperienceFilter = false;
    let maxExperienceFilter = false;
    let orgUnitFilter = false;
    let workSiteFilter = false;

    for (let param of params) {
      if (param.name == 'LOCATION') {
        this.filterTO.location = param.value.value;
        locationFilter = true;
      } else if (param.name == 'GENDER') {
        this.filterTO.genderID = param.value.id;
        genderFilter = true;
      } else if (param.name == 'INDUSTRY') {
        this.filterTO.industryID = param.value.id;
        industryFilter = true;
      } else if (param.name == 'FUNCTIONAL_AREA') {
        this.filterTO.functionalAreaID = param.value.id;
        functionalAreaFilter = true;
      } else if (param.name == 'SOURCE') {
        this.filterTO.sourceID = param.value.id;
        sourceFilter = true;
      } else if (param.name == 'STAGE/STATUS') {
        this.filterTO.stageStatusFilter = param.value;
        stageStatusFilter = true;
      } else if (param.name == 'MIN_EXPERIENCE') {
        this.filterTO.minExperience = param.value.value;
        minExperienceFilter = true;
      } else if (param.name == 'MAX_EXPERIENCE') {
        this.filterTO.maxExperience = param.value.value;
        maxExperienceFilter = true;
      } else if (param.name == 'ORG_UNIT') {
        this.filterTO.orgUnit = param.value.value;
        orgUnitFilter = true;
      } else if (param.name == 'WORKSITE') {
        this.filterTO.worksite = param.value.value;
        workSiteFilter = true;
      }

    }
    if (!genderFilter) {
      this.filterTO.genderID = null;
    }
    if (!industryFilter) {
      this.filterTO.industryID = null;
    }
    if (!functionalAreaFilter) {
      this.filterTO.functionalAreaID = null;
    }
    if (!locationFilter) {
      this.filterTO.location = null;
    }
    if (!sourceFilter) {
      this.filterTO.sourceID = null;
    }
    if (!stageStatusFilter) {
      this.filterTO.stageStatusFilter = null;
    }
    if (!minExperienceFilter) {
      this.filterTO.minExperience = null;
    }
    if (!maxExperienceFilter) {
      this.filterTO.maxExperience = null;
    }
    if (!orgUnitFilter) {
      this.filterTO.orgUnit = null;
    }
    if (!workSiteFilter) {
      this.filterTO.worksite = null;
    }
    this.taggedCandidatesPagedService.clearCache();
    this.ngOnInit();
  }
  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    } else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    this.taggedCandidatesPagedService.clearCache();
    this.ngOnInit();
  }

  getWorkflowProgressBar(workflowID: Number, item: ResponseData) {
    item.stages = this.workflowStageMap[workflowID + ''];
    return true;
  }
  indexOfStage(stage: string, stageList: string[]) {
    return stageList.indexOf(stage);
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED') {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  updateProfileMatch(item) {
    this.showloader = true;
    this.taggedCandidatesPagedService.updateProfileMatchPercenatage(item.applicantStatusID).subscribe((res) => {
      if (res && res.response) {
        for (let i = 0; i < this.data.length; i++) {
          if (this.data[i]['applicantStatusID'] == res.response.applicantStatusID) {
            this.data[i]['profileMatch'] = res.response.profileMatch;
            if (res.response.profileMatch == 0) {
              this.notificationService.addMessage("WARNING", "Something went wrong! Please try again after some time.");
            }
          }
        }
      }
      this.showloader = false;
    });
  }

  profileCompletion(data: ResponseData) {
    let candidateID = data.candidateId + "";
    if (this.sessionService.removeStatic) {
      candidateID = candidateID.substring(candidateID.length - 2, candidateID.length);
      return candidateID;
    } else {
      return "75";
    }
  }
  private fullName(candidate: ResponseData): String {
    let candidateName: String;
    if (candidate.firstName) {
      candidateName = candidate.firstName;
    }
    if (candidate.middleName) {
      candidateName = candidateName + ' ' + candidate.middleName;
    }
    if (candidate.lastName) {
      candidateName = candidateName + ' ' + candidate.lastName;
    }
    return candidateName;
  }

  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }
  
}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class ResponseData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowID: number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
  stages: string[];
  statgeStatusType: string;
  subSourceUserName: string;
  inReview: boolean;
  requisitionStatus: String;
}

export class CopyCandidateToAnotherJobData {
  input: TransferCandidateToJobData;
}