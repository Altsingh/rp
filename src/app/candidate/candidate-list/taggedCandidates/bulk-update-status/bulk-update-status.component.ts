import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogService } from "app/shared/dialog.service";
import { SessionService } from "app/session.service";
import { BulkUpdateStatusService } from "app/candidate/candidate-list/taggedCandidates/bulk-update-status/bulk-update-status.service";
import { HiringProcessService } from "app/candidate/candidate-tagged-detail/hiring-process/hiring-process.service";
import { HiringProcessUpdateStatusTo } from "app/common/hiring-process-update-status-to";
import { ApplicantStatusRequest } from "app/common/applicant-status-request";
import { MessageCode } from "app/common/message-code";
import { HiringControllerService } from "app/candidate/hiring-controller.service";
import { NotificationService, NotificationSeverity } from "app/common/notification-bar/notification.service";
import { StageActionListMaster } from "app/common/stage-action-list-master";
import { MatDialogRef } from '@angular/material';
import { TaggedCandidateService } from "app/candidate/candidate-list/taggedCandidates/tagged-candidate.service";
import { BulkUpdateStatusScreeningComponent } from "app/candidate/candidate-list/taggedCandidates/bulk-update-status/bulk-update-status-screening/bulk-update-status-screening.component";
import { BulkUpdateStatusInterviewComponent } from "app/candidate/candidate-list/taggedCandidates/bulk-update-status/bulk-update-status-interview/bulk-update-status-interview.component";
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-bulk-update-status',
  templateUrl: './bulk-update-status.component.html',
  styleUrls: ['./bulk-update-status.component.css'],
  providers: [BulkUpdateStatusService]
})
export class BulkUpdateStatusComponent implements OnInit {


  selectedList: any[];
  workflowActionID: number;
  sysWorkflowStageType: string;
  workflowID: number;
  workflowStageID: number;
  securitySubscription: any;
  security: any;
  applicantStatusRequest: ApplicantStatusRequest;
  spinner: boolean;
  toggleCandidate: boolean;
  showloader: boolean = false;
  statusList: StageActionListMaster;
  message: MessageCode;
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;
  selectedStatus: any;

  @ViewChild(BulkUpdateStatusScreeningComponent) SCREENING: BulkUpdateStatusScreeningComponent;
  ngAfterViewInitScreening() {
    this.SCREENING.updateStatus();
  }

  @ViewChild(BulkUpdateStatusInterviewComponent) INTERVIEW: BulkUpdateStatusInterviewComponent;
  ngAfterViewInitInterview() {
    this.INTERVIEW.updateStatus();
  }



  constructor(
    private dialogsService: DialogService,
    private sessionService: SessionService,
    private bulkUpdateStatusService: BulkUpdateStatusService,
    private hiringProcessService: HiringProcessService,
    private hiringControllerService: HiringControllerService,
    private notificationService: NotificationService,
    public dialogRef: MatDialogRef<BulkUpdateStatusComponent>,
    private taggedCandidateService: TaggedCandidateService,
    private i18UtilService: I18UtilService
  ) { }

  ngOnInit() {
    this.selectedList = this.sessionService.object.selectedList;
    this.sysWorkflowStageType = this.selectedList[0].sysWorkflowStageType;
    this.workflowID = this.selectedList[0].workflowID;
    this.workflowStageID = this.selectedList[0].workflowStageID;

    this.hiringProcessService.getStatusListByStageID(this.workflowStageID, false, false, 0).subscribe(out => {
      this.statusList = out.responseData
      this.showloader = false;

    });

  }

  initFormSecurityByStatusID(statusID: number) {
    this.workflowActionID = statusID;
    this.initFormSecurity();
  }

  initFormSecurity() {
    this.securitySubscription = this.hiringProcessService.getHiringProcessSecurity(this.sysWorkflowStageType, this.workflowID, this.workflowStageID, this.workflowActionID).subscribe(
      response => {
        if (response.messageCode.code === 'EC200') {

          this.security = JSON.parse(JSON.stringify(response.responseData[0]));
        }
        else
          this.security = undefined;
      });
  }
  clickCandidate() {
    this.toggleCandidate = !this.toggleCandidate;
  }

  preUpdateStatus() {
    if (this.selectedList[0].sysWorkflowStageType === "SCREENING" || this.selectedList[0].sysWorkflowStageType === "SHORTLISTING") {
      this.SCREENING.updateStatus();
    }
    else if (this.selectedList[0].sysWorkflowStageType === "INTERVIEW_1" || this.selectedList[0].sysWorkflowStageType === "INTERVIEW_2" || this.selectedList[0].sysWorkflowStageType === "INTERVIEW_3" || this.selectedList[0].sysWorkflowStageType === "INTERVIEW_4" || this.selectedList[0].sysWorkflowStageType === "INTERVIEW_5" || this.selectedList[0].sysWorkflowStageType === "HR_INTERVIEW") {
      this.INTERVIEW.updateStatus();
    }
  }

  updateStatus(event: HiringProcessUpdateStatusTo): void {
    this.spinner = true;
    this.applicantStatusRequest = new ApplicantStatusRequest();
    this.applicantStatusRequest.comment = event.comment;
    this.applicantStatusRequest.selectedWorkflowActionID = event.selectedStatusID;
    this.applicantStatusRequest.attachmentData_base64 = this.attachmentData_base64;
    this.applicantStatusRequest.attachmentName = this.attachmentName;
    this.applicantStatusRequest.interviewTypeID = event.interviewTypeID;
    this.applicantStatusRequest.applicantVenueID = event.applicantVenueID;
    this.applicantStatusRequest.startDate = event.startDate;
    this.applicantStatusRequest.endDate = event.endDate;
    this.applicantStatusRequest.interviewPanelist = event.interviewPanelist;
    this.applicantStatusRequest.calendarInvite = event.calendarInvite;
    this.applicantStatusRequest.interviewVenue = event.interviewVenue;
    this.applicantStatusRequest.grade = event.grade;
    this.applicantStatusRequest.designation = event.designation;
    this.applicantStatusRequest.employmentType = event.employmentType;
    this.applicantStatusRequest.l1Manager = event.l1Manager;
    this.applicantStatusRequest.l2Manager = event.l2Manager;
    this.applicantStatusRequest.hrManager = event.hrManager;
    this.applicantStatusRequest.ctc = event.ctc;
    this.applicantStatusRequest.expectedJoiningDate = event.expectedJoiningDate;
    this.applicantStatusRequest.tenantID = event.tenantID;
    this.applicantStatusRequest.employeeCode = event.employeeCode;
    this.applicantStatusRequest.bulk = true;
    this.applicantStatusRequest.applicantStatusIDList = [];
    for (let data of this.selectedList) {
      this.applicantStatusRequest.applicantStatusIDList.push(data.applicantStatusID);

    }


    let subs = this.hiringProcessService.updateApplicantStatus(this.applicantStatusRequest).subscribe(out => {
     
      let messageTO: MessageCode = out.messageCode;
      let message: string = '';
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.workflowStageID = out.responseData.workflowStageID;
          message = 'candidate.success.statusUpdated'
        }
        else if (messageTO.message != null) {
          message = messageTO.message.toString();
        }
        else {
          message = "Request failed to execute."
        }

        if (messageTO.code === 'EC200') {
          this.i18UtilService.get(message).subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
          this.dialogRef.close(0);
          this.taggedCandidateService.getTaggedCandidateComponent().clearCache();
          this.taggedCandidateService.getTaggedCandidateComponent().ngOnInit();
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, message);
        }
      }
    }, (onError) => {
      this.i18UtilService.get('common.error.somthingWrong').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      this.spinner = !this.spinner;
      subs.unsubscribe();
    }, () => {
      this.spinner = !this.spinner;
      subs.unsubscribe();
    });
  }

  clearMessages() {
    this.message.severity = "";
    this.message.message = "";
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', { 'supportedTypes': supportedTypes.toString() }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
  
  }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }



}
