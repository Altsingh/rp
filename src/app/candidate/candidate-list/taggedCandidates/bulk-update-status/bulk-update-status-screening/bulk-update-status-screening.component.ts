import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NotificationService, NotificationSeverity } from "app/common/notification-bar/notification.service";
import { HiringProcessUpdateStatusTo } from "app/common/hiring-process-update-status-to";
import { HiringProcessResponse } from "app/common/hiring-process-response";
import { StageActionListMaster } from "app/common/stage-action-list-master";
import { FormControl } from '@angular/forms';
import { SelectItem } from "app/shared/select-item";
import { MessageCode } from "app/common/message-code";
import { MatDialogRef } from '@angular/material';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-bulk-update-status-screening',
  templateUrl: './bulk-update-status-screening.component.html',
  styleUrls: ['./bulk-update-status-screening.component.css']
})
export class BulkUpdateStatusScreeningComponent implements OnInit {
  lable: string;
  screened: boolean = false;
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  @Output() message: EventEmitter<MessageCode> = new EventEmitter();
  @Input() statusLists: StageActionListMaster;
  @Input() set spinner(value: boolean) {
    this.screened = false;
  }

  @Input() inputData: HiringProcessResponse;
  @Input() security: any;
  @Input() stageName: any;

  toggleOpen: boolean;


  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  fr: FileReader;
  file: Blob;
  stageList: SelectItem[];
  messageCodeTO: MessageCode;

  constructor(private notificationService: NotificationService,
    public dialogRef: MatDialogRef<BulkUpdateStatusScreeningComponent>, private i18UtilService: I18UtilService) { }

  ngOnInit() {
    this.stageList = [new SelectItem(this.stageName, this.stageName)];
    this.i18UtilService.get('common.label.go').subscribe((res: string) => {
      this.lable =res;
    });
  }
  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.messageCodeTO = new MessageCode();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    let valid: boolean = true;
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      this.message.emit(this.messageCodeTO);
      return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.messageCodeTO.message =res;
      });
      this.messageCodeTO.severity = "warning";
      this.message.emit(this.messageCodeTO);
      return;
    }
    if (this.security.commentRendered && this.security.commentRequired && (this.updateStatusRequestTO.comment == null || this.updateStatusRequestTO.comment == "")) {
      this.i18UtilService.get('common.warning.enterLabelCommon', { 'label': this.security.commentLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.attachmentLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (valid == false) {
      this.message.emit(this.messageCodeTO);
      return;
    }
    this.screened = true;
    this.output.emit(this.updateStatusRequestTO);
  }



  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.i18UtilService.get('candidate.warning.fileSizeExceeded').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.i18UtilService.get('candidate.warning.supportedFileWarning', {'supportedTypes': supportedTypes.toString()}).subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  statusChangeListener(value: any) {
    this.selectedStatusID.setValue(value);
    this.initSecurity.emit(value);
  }

}
