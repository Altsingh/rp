import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { StageActionListMaster } from "app/common/stage-action-list-master";
import { HiringProcessUpdateStatusTo } from "app/common/hiring-process-update-status-to";
import { HiringProcessResponse } from "app/common/hiring-process-response";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SelectItem } from "app/shared/select-item";
import { InterviewService } from "app/candidate/candidate-tagged-detail/hiring-process/interview/interview.service";
import { NotificationService,NotificationSeverity } from "app/common/notification-bar/notification.service";
import { NameinitialService } from "app/shared/nameinitial.service";
import { MessageCode } from "app/common/message-code";
import { MatDialogRef } from '@angular/material';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-bulk-update-status-interview',
  templateUrl: './bulk-update-status-interview.component.html',
  styleUrls: ['./bulk-update-status-interview.component.css'],
  providers: [ InterviewService ]
})
export class BulkUpdateStatusInterviewComponent implements OnInit {

  randomcolor: string[] = [];
  lable: string ;
  interview: boolean = false;
  
  newActionType: string = '';
  @Input() set statusLists(list: StageActionListMaster[]) {
    
    this.filteredStatus = [];
    for(let data of list)
    {
      
        if(data.sysActionType=="SCHEDULED" || data.sysActionType == "RE-SCHEDULED" || data.sysActionType == "REJECTED")
        {
          this.filteredStatus.push(data);
        }
    }
  }
  @Output() output: EventEmitter<HiringProcessUpdateStatusTo> = new EventEmitter();
  @Output() message: EventEmitter<MessageCode> = new EventEmitter();
  @Output() selectedStatusEmitter: EventEmitter<any> = new EventEmitter();
  @Input() set spinner(value: boolean) {
    this.interview = false;
  }
  @Input() inputData: HiringProcessResponse;

  @Input() security: any;
  @Input() stageName: any;
  @Output() initSecurity: EventEmitter<any> = new EventEmitter();
  filteredStatus: StageActionListMaster[];
  toggleOpen: boolean;
  updateStatusRequestTO: HiringProcessUpdateStatusTo;
  messageCodeTO: MessageCode;
  selectedStatusID = new FormControl();
  comment = new FormControl();
  attachmentData_base64: String;
  attachmentName: String;
  interviewVenue: string;
  fr: FileReader;
  file: Blob;
  interviewForm: FormGroup;
  interviewTypeList: SelectItem[];
  venueList: SelectItem[];
  interviewPanelist: SelectItem[];
  selectedInterviewPanelist: SelectItem[];
  DDRange: SelectItem[];
  MMRange: SelectItem[];
  YYYYRange: SelectItem[];
  stageList: SelectItem[];

  interviewStartDate: Date;
  interviewEndDate: Date;
  currentDate: Date;


  populateInterviewResponse: any;

  constructor( private formBuilder: FormBuilder, private interviewService: InterviewService,
    private notificationService: NotificationService, private nameinitialService: NameinitialService,
    public dialogRef: MatDialogRef<BulkUpdateStatusInterviewComponent>, private i18UtilService: I18UtilService) { }


  initializeData() {
    this.interviewForm = this.formBuilder.group({
      startDate: new FormControl(),
      endDate: new FormControl(),
      interviewType: new FormControl(),
      venue: new FormControl(),
      calendarInvite: ''
    });

    this.interviewService.getContentTypeMaster("INTERVIEW_TYPE").subscribe(out => {
      this.interviewTypeList = out.responseData.interviewTypeList;
      if (this.inputData.sysWorkflowstageActionType == 'SCHEDULED-DONE') {
        this.interviewService.populateInterview(this.inputData.applicantStatusHistoryID).subscribe(
          (response) => {

            this.setPopulateInterviewResponse(response);
          }
        );
      }

    });
    this.interviewService.getApplicantVenues().subscribe(out => this.venueList = out.responseData);
    this.DDRange = this.createRange(1, 31);
    this.MMRange = this.getMMRange();
    this.YYYYRange = this.createRange(2000, 20);
  }

setPopulateInterviewResponse(response: any) {
    this.populateInterviewResponse = response;
    this.setStartDate(new Date(response.response.interviewStartTime));
    this.setEndDate(new Date(response.response.interviewEndTime));
    this.interviewForm.controls.interviewType.setValue(response.response.contentTypeId);
    this.interviewForm.controls.venue.setValue(response.response.applicantVenueId);
    this.comment.setValue(response.response.comment)
    this.inputData.feedbackDocumentPath = response.response.recruiterDocumentPath;
    this.selectedInterviewPanelist = [];

    for (let panel of response.response.interviewerUserIDList) {
      for (let interviewer of this.interviewPanelist) {
        if (interviewer.value == panel) {
          this.selectedInterviewPanelist.push(interviewer);
          break;
        }
      }
    }
  }


  ngOnInit() {
    this.stageList = [new SelectItem(this.stageName, this.stageName)];
    this.randomcolor = this.nameinitialService.randomcolor;

    this.initializeData();
    this.interviewService.getAllRequisitionInterviewPanel().subscribe(
      out => this.interviewPanelist = out.responseData);

      this.i18UtilService.get('common.label.go').subscribe((res: string) => {
        this.lable =res;
      });
    
  }

  updateStatus() {
    this.notificationService.clear();
    this.updateStatusRequestTO = new HiringProcessUpdateStatusTo();
    this.messageCodeTO = new MessageCode();
    this.updateStatusRequestTO.comment = this.comment.value;
    this.updateStatusRequestTO.selectedStatusID = this.selectedStatusID.value;
    this.updateStatusRequestTO.attachmentData_base64 = this.attachmentData_base64;
    this.updateStatusRequestTO.attachmentName = this.attachmentName;
    this.updateStatusRequestTO.interviewTypeID = this.interviewForm.controls.interviewType.value;
    this.updateStatusRequestTO.applicantVenueID = this.interviewForm.controls.venue.value;
    this.updateStatusRequestTO.startDate = this.interviewForm.controls.startDate.value;
    this.updateStatusRequestTO.endDate = this.interviewForm.controls.endDate.value;
    this.updateStatusRequestTO.calendarInvite = this.interviewForm.controls.calendarInvite.value;
    this.updateStatusRequestTO.interviewVenue = this.interviewVenue;
    let interviewPanelist = new Array<Number>();
    if (this.selectedInterviewPanelist != null) {
      for (let panel of this.selectedInterviewPanelist) {
        interviewPanelist.push(panel.value);
      }
    }
    this.updateStatusRequestTO.interviewPanelist = interviewPanelist;
    let valid: boolean = true;
    
    if (this.updateStatusRequestTO.selectedStatusID == null) {
      this.i18UtilService.get('candidate.warning.chooseStatus').subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity="warning";
      this.message.emit(this.messageCodeTO);
      return;
    }
    if (this.security == undefined) {
      this.i18UtilService.get('candidate.warning.secNotConfigured').subscribe((res: string) => {
        this.messageCodeTO.message =res;
      });
      this.messageCodeTO.severity = "warning";
      this.message.emit(this.messageCodeTO);
      return;
    }
    if (this.security.sendEmailAsCalendarInviteRendered && this.interviewForm.controls.calendarInvite.value=="") {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.sendEmailAsCalendarInviteLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.startDateRendered && this.security.startDateRequired && this.interviewStartDate == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.startDateLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    } else {
      this.currentDate = new Date();
      if (this.interviewStartDate > this.interviewEndDate) {
        this.i18UtilService.get('common.warning.endDateStartDate').subscribe((res: string) => {
          this.messageCodeTO.message =res;
        }); 
      this.messageCodeTO.severity = "warning";
        valid = false;
      }
      else if (this.interviewStartDate < this.currentDate) {
        this.i18UtilService.get('common.warning.startDateCurrentDate').subscribe((res: string) => {
          this.messageCodeTO.message =res;
        });
      this.messageCodeTO.severity = "warning";
        valid = false;
      }
    }
    if (this.security.endDateRendered && this.security.endDateRequired && this.interviewEndDate == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.endDateLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.venueRendered && this.security.venueRequired && (this.updateStatusRequestTO.applicantVenueID == null || this.updateStatusRequestTO.applicantVenueID == 0)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.venueLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.interviewTypeRendered && this.security.interviewTypeRequired && (this.updateStatusRequestTO.interviewTypeID == 0 || this.updateStatusRequestTO.interviewTypeID == null)) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.interviewTypeLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.interviewPanelistRendered && this.security.interviewPanelistRequired && (this.updateStatusRequestTO.interviewPanelist.length == 0)) {
      this.i18UtilService.get('candidate.warning.selectInterviewPanel').subscribe((res: string) => {
        this.messageCodeTO.message =res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.attachmentRendered && this.security.attachmentRequired && this.updateStatusRequestTO.attachmentName == null) {
      this.i18UtilService.get('common.warning.selectLabelCommon', { 'label': this.security.attachmentLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (this.security.commentRendered && this.security.commentRequired && this.updateStatusRequestTO.comment == null) {
      this.i18UtilService.get('common.warning.enterLabelCommon', { 'label': this.security.commentLabel }).subscribe((res: string) => {
        this.messageCodeTO.message = res;
      });
      this.messageCodeTO.severity = "warning";
      valid = false;
    }
    if (valid == false) {
      this.message.emit(this.messageCodeTO);
      return;

    }
    this.interview = true;
    this.output.emit(this.updateStatusRequestTO);
  }

  deleteInterviewer(interviewPanel) {
    var resultListNew = new Array<any>();
    for (let resultListItem of this.selectedInterviewPanelist) {
      if (resultListItem.value != interviewPanel.value) {
        resultListNew.push(resultListItem);
      }
    }
    this.selectedInterviewPanelist = resultListNew;

  }

  createRange(min, noOfItems) {
    var items: SelectItem[] = [];
    for (var i = min; i <= (min + noOfItems - 1); i++) {
      items.push(new SelectItem(i, i));
    }
    return items;
  }

  getMMRange() {
    var items: SelectItem[] = [];
    items.push(new SelectItem('Jan', 1));
    items.push(new SelectItem('Feb', 2));
    items.push(new SelectItem('Mar', 3));
    items.push(new SelectItem('Apr', 4));
    items.push(new SelectItem('May', 5));
    items.push(new SelectItem('Jun', 6));
    items.push(new SelectItem('Jul', 7));
    items.push(new SelectItem('Aug', 8));
    items.push(new SelectItem('Sep', 9));
    items.push(new SelectItem('Oct', 10));
    items.push(new SelectItem('Nov', 11));
    items.push(new SelectItem('Dec', 12));
    return items;
  }

  uploadDocumentListener(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file = fileList[0];
      let fr = new FileReader();
      let extenstion = file.name.split('.').pop();
      let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
      if (file.size > 8388608) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'File size can not be more than 8 MB');
      } else if (!supportedTypes.includes(extenstion.toLowerCase())) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'Only ' + supportedTypes.toString() + ' files are allowed!');
      } else {
        fr.readAsDataURL(file);
        fr.onloadend = (e) => {
          this.attachmentData_base64 = fr.result;
          this.attachmentName = file.name;
          this.file = file;
        }
      }
    }
  }

  deleteDocumentListener() {
    this.attachmentData_base64 = undefined;
    this.attachmentName = undefined;
  }
  downloadDocumentListener() {
    var blob = new Blob([this.file], { type: this.file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url);
  }


  addInterviewPanelistListener(event: SelectItem[]) {
    this.selectedInterviewPanelist = event;
  }

   setInterviewVenue(event) {
    this.interviewForm.controls['venue'].setValue(event.value);
    this.interviewVenue = event.label;
  }

  setStartDate(startDate: Date) {
    let _STARTDATE: String = startDate.getDate() + '-' + ((startDate.getMonth()) + 1) + '-' + startDate.getFullYear() + ' ' + startDate.getHours() + ':' + startDate.getMinutes();
    this.interviewForm.controls['startDate'].setValue(_STARTDATE);
    this.interviewStartDate = startDate;

  }

  setEndDate(endDate: Date) {

    let _ENDDATE: String = endDate.getDate() + '-' + ((endDate.getMonth()) + 1) + '-' + endDate.getFullYear() + ' ' + endDate.getHours() + ':' + endDate.getMinutes();
    this.interviewForm.controls['endDate'].setValue(_ENDDATE);
    this.interviewEndDate = endDate;

  }

  statusChangeListener(value: any, sysActionType: any) {
    this.newActionType = sysActionType;
    if (sysActionType == 'SCHEDULED' || sysActionType == 'RE-SCHEDULED') {
      this.interviewForm.controls['startDate'].setValue(undefined);
      this.interviewStartDate = undefined;
      this.interviewForm.controls['endDate'].setValue(undefined);
      this.interviewEndDate = undefined;
      this.interviewForm.controls.interviewType.setValue(undefined);
      this.interviewForm.controls.venue.setValue(undefined);
      this.comment.setValue(undefined)
      this.inputData.feedbackDocumentPath = undefined;
      this.selectedInterviewPanelist = [];
    } else {
      if (this.populateInterviewResponse != undefined) {
        this.setPopulateInterviewResponse(this.populateInterviewResponse)
      }
    }
    
    this.selectedStatusEmitter.emit(sysActionType);
    this.selectedStatusID.setValue(value);
    this.initSecurity.emit(value);
  }

}
