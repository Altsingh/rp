import { Injectable } from '@angular/core';
import { PagedDataService } from '../../../common/paged-data.service';
import { SessionService } from '../../../session.service';
import { Http, RequestOptions } from '@angular/http';

@Injectable()
export class TaggedCandidatesPagedService extends PagedDataService {

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/candidate/taggedCandidates/byOrganization/',
      '/rest/altone/candidate/taggedCandidates/byOrganization/totalRows/'
    );
  }

  public updateProfileMatchPercenatage( applicationStatusId: Number ) {

    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/updateProfileMatchPercentage/' + applicationStatusId + "/", "", options).
        map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()
        });

}


}
