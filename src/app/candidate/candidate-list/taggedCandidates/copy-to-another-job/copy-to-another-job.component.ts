import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { SessionService } from '../../../../session.service';
import { DialogService } from '../../../../shared/dialog.service';
import { CandidateListService } from '../../candidate-list.service';
import { TaggedCandidateService } from '../../taggedCandidates/tagged-candidate.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-copy-to-another-job',
  templateUrl: './copy-to-another-job.component.html',
  styleUrls: ['./copy-to-another-job.component.css']
})
export class CopyToAnotherJobComponent implements OnInit {
  label: string;
  moveLabel: string;
  copyLabel: string;
  data: ResponseData[];
  jobList: any[];
  filteredJobList: any[];
  candidatelist: candidateData[];
  messageCode: MessageCode;
  candidateInfo: FormGroup;
  timer: any;
  severity: String;
  severityClass: string;
  message: string;
  subscription;
  candidateNewJobInfo: TransferCandidateToAnotherJobData;
  copyCandidateRespone: ResponseData;
  transferCandidate: boolean = false;
  disableSubmit: boolean = false;
  selected: any;
  actionList: Actions[] = [];

  constructor(public dialogRef: MatDialogRef<CopyToAnotherJobComponent>,
    private candidateListService: CandidateListService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private sessionService: SessionService, private taggedCandidateService: TaggedCandidateService,
    private dialogService: DialogService, private i18UtilService: I18UtilService) {

    this.data = [];
    this.copyCandidateRespone = new ResponseData();
    this.message = "";
    this.i18UtilService.get('common.label.move').subscribe((res: string) => {
      this.moveLabel = res;
    });
    this.i18UtilService.get('common.label.copy').subscribe((res: string) => {
      this.copyLabel = res;
    });
  }
  ngOnInit() {
    this.initializeCandidateInfo();
    this.initActionList();
    this.candidatelist = this.candidateListService.getDataList();
    let res = this.dialogService.getDialogData();
    this.messageCode = res.messageCode;
    this.data = res.responseData;
    this.filterJobList();
    let error = this.checkForDuplicateCandidate();
    if (error.length > 0) {
      this.severityClass = "warning";
      this.message = error;
      this.disableSubmit = true;
    }
    this.i18UtilService.get('common.label.transfer').subscribe((res: string) => {
      this.label = res;
    });
   
  }
  public copyCandidateToAnotherJob(event) {
    if (!this.validateCandidateTransfer()) {
      return;
    }
    let transferCandidateData: TransferCandidateToAnotherJobData[] = [];
    for (let candidate of this.candidatelist) {
      this.candidateNewJobInfo = new TransferCandidateToAnotherJobData();
      this.candidateNewJobInfo.transferToRequisitionId = this.candidateInfo.controls.jobIdSelected.value;
      this.candidateNewJobInfo.transferToStage = this.candidateInfo.controls.stageSelected.value;
      this.candidateNewJobInfo.transferActionType = this.label;
      this.candidateNewJobInfo.userID = this.sessionService.userID;
      this.candidateNewJobInfo.applicantStatusId = candidate.applicantStatusID;
      this.candidateNewJobInfo.requisitionID = candidate.requisitionId;
      this.candidateNewJobInfo.candidateID = candidate.candidateId;
      this.candidateNewJobInfo.candidateCode = candidate.candidateCode;
      let candidateName: String = "";
      candidateName = this.fullName(candidate, candidateName);
      this.candidateNewJobInfo.candidateName = candidateName;
      transferCandidateData.push(this.candidateNewJobInfo);
    }
    this.transferCandidate = true;
    this.taggedCandidateService.transferCandidateToAnotherJobBulk(transferCandidateData).subscribe(out => {
      this.copyCandidateRespone.result = out.responseData[0];
      this.messageCode = out.messageCode;
      if (this.copyCandidateRespone.result == "success") {
        this.dialogRef.close();
        this.i18UtilService.get('candidate.success.candidateTransferred').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
        this.taggedCandidateService.getTaggedCandidateComponent().ngOnInit();
      } else {
        this.transferCandidate = false;
        this.setMessage("WARNING", this.copyCandidateRespone.result);
      }
    });
  }

  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 5000);
     });
   
  }
  clearMessages() {
    this.severity = "";
    this.message = "";
  }
  bindAction(event) {
      this.label = event.label;
  }
  bindWorkflowID(event) {
    this.selected = event;
  }
  initActionList() {
    this.actionList.push({
      value: 1,
      label: this.copyLabel,
    });
    this.actionList.push({
      value: 2,
      label: this.moveLabel,
    });
  }
  initializeCandidateInfo() {
    this.candidateInfo = this.formBuilder.group({
      jobIdSelected: [],
      actionSelected: '',
      stageSelected: ''
    });
  }
  validateCandidateTransfer() {
    let valid: boolean = true;
    let errorString1: string = '';
    let errorString2: string = '';
    let allowTransfer: boolean = true;
    let sameWF: boolean = true;
    let flag1 : boolean = false;
    let flag2 : boolean = false;
    this.notificationService.clear();
    if (this.candidateInfo.controls.actionSelected.value == null || this.candidateInfo.controls.actionSelected.value == '') {
      
      this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.selectAction');
      return false;
    }
    if (this.candidateInfo.controls.stageSelected.value == null || this.candidateInfo.controls.stageSelected.value == '') {
      this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.selectStage');
      return false;
    }
    if (this.candidateInfo.controls.jobIdSelected.value == null) {
      this.setMessage(NotificationSeverity.WARNING, 'job.warning.selectJob');
      valid = false;
      return false;
    }
    for (let candidate of this.candidatelist) {
      let candidateName: String = "";
      if (!candidate.allowCopyMove) {
        candidateName = this.fullName(candidate, candidateName);
        if(flag1){
          errorString1 = errorString1  + ", ";
        }
        errorString1 = errorString1 + candidateName + "(" + candidate.candidateCode + ")";
        allowTransfer = false;
        flag1= true;
      }
      if (this.candidateInfo.controls.stageSelected.value == "current" && candidate.workflowID != this.selected.description) {
        candidateName = this.fullName(candidate, candidateName);
        if(flag2){
          errorString2 = errorString2  + ", ";
        }
        errorString2 = errorString2 + candidateName + "(" + candidate.candidateCode + ")";
        sameWF = false;
        flag2 = true;
      }
    }
    this.i18UtilService.get('candidate.list.canNotTransferMsg1').subscribe((res: string) => {
      errorString1 = res + errorString1;
    });
    this.i18UtilService.get('candidate.list.canNotTransferMsg2').subscribe((res: string) => {
      errorString2 = res + errorString2;
    });

    if (!allowTransfer) {
      this.setMessage("WARNING", errorString1);
      return false;
    }
    if (!sameWF) {
      this.setMessage("WARNING", errorString2);
      return false;
    }
    return valid;
  }

  private checkForDuplicateCandidate() {
    let set: Set<Number> = new Set();
    let set2: Set<Number> = new Set();
    let errorList: candidateData[] = [];
    let errorString: string = '';
    let flag : boolean = false;
    for (let candidate of this.candidatelist) {
      if (set.has(candidate.candidateId)) {
        errorList.push(candidate);
        set2.add(candidate.candidateId);
      }
      set.add(candidate.candidateId);
    }
    this.candidatelist = this.candidatelist.filter(
      function (x) {
        return errorList.indexOf(x) < 0;
      }
    );
    set2.forEach((item) => {
      for (let candidate of this.candidatelist) {
        if (item == candidate.candidateId) {
          let name: String = '';
          name = this.fullName(candidate, name);
          if(flag){
            errorString = errorString  + ", ";
          }
          errorString = errorString + name + "(" + candidate.candidateCode + ")";
          flag = true;
          this.candidatelist.splice(this.candidatelist.indexOf(candidate), 1);
        }
      }
    });
    if (errorString.length > 0) {
      errorString = "Following candidate(s) have been removed from the list as they were tagged to multiple jobs in the selection list -" + errorString ;
    }
    return errorString;
  }
  private fullName(candidate: candidateData, candidateName: String) {
    if (candidate.firstName) {
      candidateName = candidate.firstName;
    }
    if (candidate.middleName) {
      candidateName = candidateName + ' ' + candidate.middleName;
    }
    if (candidate.lastName) {
      candidateName = candidateName + ' ' + candidate.lastName;
    }
    return candidateName;
  }
  syncJobList(flag: boolean) {
    if (flag) {
      this.data = this.filteredJobList;
    } else {
      this.data = this.jobList;
    }
  }
  filterJobList() {
    this.jobList = this.data.slice();
    this.filteredJobList = this.data.slice();
    for (let item of this.filteredJobList) {
      for (let candidate of this.candidatelist) {
        if (item.description != candidate.workflowID) {
          let index = this.filteredJobList.indexOf(item);
          if (index > -1)
            this.filteredJobList.splice(index, 1);
        }
      }
    }
  }
  alterCandidateList(candidate: candidateData) {
    let index = this.candidatelist.indexOf(candidate);
    if (index >= 0) {
      this.candidatelist.splice(index, 1);
    }
    if(this.candidatelist.length==0){
      this.closePopUp()
    }
  }
  clearNotification() {
    this.disableSubmit = false;
    this.message = '';
    if(this.candidatelist.length==0){
      this.closePopUp()
    }
  }
  closePopUp(){
    this.dialogRef.close('Cancel');
  }
}
export class ResponseData {
  jobCode: String;
  jobTitle: String;
  requisitionID: number;
  result: string;
}
export class candidateData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
  workflowID: Number;
}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
export class TransferCandidateToAnotherJobData {
  requisitionID: Number;
  candidateID: Number;
  userID: Number;
  applicantStatusId: Number;
  transferToRequisitionId: Number;
  transferToStage: String;
  transferActionType: String;
  candidateCode: String;
  candidateName: String;
}
export class Actions {
  value: Number;
  label: String;
}