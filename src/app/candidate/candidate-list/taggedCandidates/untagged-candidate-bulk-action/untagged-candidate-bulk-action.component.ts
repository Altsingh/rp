import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MessageCode } from '../../../../common/message-code';
import { CandidateListService } from '../../candidate-list.service';
import { TaggedCandidateService } from '../../taggedCandidates/tagged-candidate.service';
import { NotificationService, NotificationSeverity } from "../../../../common/notification-bar/notification.service";

@Component({
  selector: 'alt-untagged-candidate-bulk-action',
  templateUrl: './untagged-candidate-bulk-action.component.html',
  styleUrls: ['./untagged-candidate-bulk-action.component.css']
})
export class UntaggedCandidateBulkActionComponent implements OnInit {
  lable: string = "Untag";
  untaggCandidate: boolean = false;
  candidatelist: candidateListResponse[];
  subscription;
  datalist: ResponseData;
  messageCode: MessageCode;
  constructor(public dialogRef: MatDialogRef<UntaggedCandidateBulkActionComponent>,
    private candidateListService: CandidateListService,
    private taggedCandidateService: TaggedCandidateService,
    private notificationService: NotificationService) {
    this.candidatelist = [];
    this.datalist = new ResponseData();
  }

  ngOnInit() {
    this.candidatelist = this.candidateListService.getDataList();
  }
  untagCanidate(event) {
    this.untaggCandidate = true;
    let list: any[] = [];
    for (let id of this.candidatelist) {
      list.push(id.applicantStatusID);
    }
    this.subscription = this.taggedCandidateService.unTagApplicantBulkFromJob(list).subscribe(res => {
      this.datalist.result = res.responseData[0],
        this.messageCode = res.messageCode;
      if (this.datalist.result == true) {
        this.dialogRef.close();
        this.notificationService.setMessage(NotificationSeverity.INFO, "Candidates untagged successfully!");
        this.taggedCandidateService.getTaggedCandidateComponent().clearCache();
        this.taggedCandidateService.getTaggedCandidateComponent().ngOnInit();
      }
    });
    this.candidatelist = [];
  }
}

export class candidateListResponse {
  firstName: Array<string>;
  applicantStatusID: Array<number>;
}
export class ResponseData {
  result: boolean;
}