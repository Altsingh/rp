import { Injectable } from '@angular/core';
import { Jsonp, Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from '../../../session.service';
import 'rxjs/add/operator/map';

import { TaggedCandidateListComponent } from './tagged-candidate-list.component'
import { TransferCandidateToJobData } from '../candidate-context-menu/candidate-actions/copy-candidate-data';
import { CandidateFilterTo } from '../candidate-filter-to';

@Injectable()
export class TaggedCandidateService {

    taggedCandidateListComponent: TaggedCandidateListComponent;

    constructor(private http: Http, private sessionService: SessionService) { }

    public getTaggedCandidates(filterTO: CandidateFilterTo) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/taggedCandidatesOrganization/', JSON.stringify(filterTO), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public setSessioServiceObject(object: any) {
        this.sessionService.object = object;
    }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }

    getURL() {
        return (this.sessionService.orgUrl + '/rest/altone/candidate/untagCandidate/');
    }

    public unTagApplicantFromJob(applicantStatusId: number) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getURL(), JSON.stringify(applicantStatusId), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    geBulktURL() {
        return (this.sessionService.orgUrl + '/rest/altone/candidateDetail/untagMultipleCandidates/');
    }

    public unTagApplicantBulkFromJob(applicantStatusId: Array<number>) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.geBulktURL(), JSON.stringify(applicantStatusId), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public setTaggedCandidateComponent(taggedCandidates: TaggedCandidateListComponent) {
        this.taggedCandidateListComponent = taggedCandidates;
    }

    public getTaggedCandidateComponent(): TaggedCandidateListComponent {
        return this.taggedCandidateListComponent;
    }

    getTransferURL() {
        return (this.sessionService.orgUrl + '/rest/altone/candidateDetail/transferCandidateToOtherJob/');
    }
    getTransferBulkURL() {
        return (this.sessionService.orgUrl + '/rest/altone/candidateDetail/transferMultipleCandidatesToOtherJob/');
    }

    public transferCandidateToAnotherJob(transferCandidateToOtherJobData: TransferCandidateToAnotherJobData) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getTransferURL(), JSON.stringify(transferCandidateToOtherJobData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public transferCandidateToAnotherJobBulk(transferCandidateToOtherJobData: Array<TransferCandidateToAnotherJobDataBulk>) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getTransferBulkURL(), JSON.stringify(transferCandidateToOtherJobData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
}

export class TransferCandidateToAnotherJobData {
    input: TransferCandidateToJobData;
}
export class TransferCandidateToAnotherJobDataBulk {
    requisitionID: Number;
    candidateID: Number;
    userID: Number;
    applicantStatusId: Number;
    transferToRequisitionId: Number;
    transferToStage: String;
    transferActionType: String;
}

