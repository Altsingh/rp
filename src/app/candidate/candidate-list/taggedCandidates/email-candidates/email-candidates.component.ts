import { Component, OnInit } from '@angular/core';
import { EmailTemplateService } from "../../../email-template/email-template.service";
import { SessionService } from "../../../../session.service";
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { ValidationService } from "../../../../shared/custom-validation/validation.service";
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TaggedCandidateService } from '../../taggedCandidates/tagged-candidate.service';
import { CandidateListService } from '../../candidate-list.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-email-candidates',
  templateUrl: './email-candidates.component.html',
  styleUrls: ['./email-candidates.component.css'],
  providers: [EmailTemplateService]
})
export class EmailCandidatesComponent implements OnInit {
  lable: string;
  emailCandidate: boolean = false;
  emailInfo: FormGroup;
  primaryEmail: String[] = [];
  data: EmailData;
  messageCode: MessageCode;
  responseData: ResponseData;
  candidatelist: candidateData[];
  timer: any;
  severity: String;
  severityClass: string;
  message: string;

  constructor(public dialogRef: MatDialogRef<EmailCandidatesComponent>, private emailTemplateService: EmailTemplateService,
    private sessionService: SessionService, private formBuilder: FormBuilder,
    private notificationService: NotificationService, private taggedCandidateService: TaggedCandidateService,
    private candidateListService: CandidateListService, private i18UtilService: I18UtilService) {
    this.data = new EmailData();
    this.responseData = new ResponseData();
    this.message = "";
  }

  ngOnInit() {
    this.emailInfo = this.formBuilder.group({
      toEmail: ['', [ValidationService.emailValidator]],
      ccEmail: ['', [ValidationService.emailValidator]],
      subject: '',
      content: ''
    });
    this.candidatelist = this.candidateListService.getDataList();
    for (let i of this.candidatelist) {
      if (i.primaryEmail) {
        this.primaryEmail.push(i.primaryEmail);
      }
    }
    this.i18UtilService.get('common.label.send').subscribe((res: string) => {
      this.lable = res;
    });
  }
  sendEmail(event) {
    this.emailCandidate = true;
    this.data.sentTo = this.primaryEmail;
    this.data.cc = this.emailInfo.controls.ccEmail.value;
    this.data.subject = this.emailInfo.controls.subject.value;
    this.data.content = this.emailInfo.controls.content.value;
    this.emailTemplateService.sendEmail(this.data).subscribe(out => {
      this.messageCode = out.messageCode;
      this.responseData.result = out.responseData[0];
      if (this.responseData.result == "success") {
        this.dialogRef.close();
        this.taggedCandidateService.getTaggedCandidateComponent().ngOnInit();
        this.i18UtilService.get('candidate.success.emailSent').subscribe((res: string) => {
          this.notificationService.setMessage("INFO", res);
        }); 
      }else {
        this.emailCandidate = false;
        this.setMessage("WARNING", this.responseData.result);
      }
    });
  }
  public setMessage(severity: string, message: string) {
    this.severity = severity;
    this.message = message;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 5000);
  }
  clearMessages() {
    this.severity = "";
    this.message = "";
  }
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
export class EmailData {
  sentTo: String[];
  cc: String;
  subject: String;
  content: String;
  mailFrom: String;
  organizationId: Number;
  tenantId: Number;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  result: string;
}
export class candidateData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
}