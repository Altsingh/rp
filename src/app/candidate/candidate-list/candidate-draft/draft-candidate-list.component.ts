import { SessionService } from 'app/session.service';
import { Component, OnInit, OnDestroy, AfterViewInit, ApplicationRef, ChangeDetectorRef, ViewChild } from '@angular/core';
import { DraftCandidateListService } from './draft-candidate-list.service';
import { JsonpModule } from '@angular/http';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { NameinitialService } from '../../../shared/nameinitial.service';
import { DraftCandidatePagedService } from './draft-candidate-paged.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PageState } from '../../../shared/paginator/paginator.component';
import { CandidateFilterTo } from '../candidate-filter-to';
import { environment } from 'environments/environment.staging';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-candidate-list',
  templateUrl: './draft-candidate-list.component.html',
  providers: [DraftCandidateListService, JsonpModule, DraftCandidatePagedService],
})
export class DraftCandidateListComponent implements OnInit, OnDestroy {

  isToggled: boolean;
  filtterToggle: boolean;
  pageState: PageState = new PageState();

  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;

  currentSortOrder: string = ""



  loaderIndicator: string;
  filterDropDown: boolean = true;
  filterDropDown2: boolean = true;
  data: any[];
  messageCode: MessageCode;
  subscription;
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "createdDate";
  public sortOrder = "desc";
  randomcolor: string[] = [];
  indexList: number[] = [];
  selected: any[] = [];
  selectAll: boolean;
  candidateDraftID: Array<number>;
  isClassVisible: boolean = false;
  displayClass: String = "displayNone";
  filterTO: CandidateFilterTo;
  automatchEnabled: boolean;


  constructor(private draftCandidateListService: DraftCandidateListService,
    private notificationService: NotificationService,
    private nameinitialService: NameinitialService,
    private sessionService: SessionService,
    private draftCandidatePagedService: DraftCandidatePagedService,
    private formBuilder: FormBuilder,
    private i18UtilService: I18UtilService
  ) {
    this.automatchEnabled =  this.sessionService.isAutoMatchEnabled();
    this.filterTO = new CandidateFilterTo();
    this.filterTO.sortBy = 'createdDate';
    this.filterTO.sortOrder = 'desc';
    this.setCurrentOrder();

    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });

    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {

        let query = this.queryFormGroup.controls.query.value;

        if (query.length >= 3) {

          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          this.draftCandidatePagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.draftCandidatePagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
      }, 200);

    });
  }

  setCurrentOrder() {
    this.currentSortOrder = "Select...";

    if (this.filterTO != null) {

      if (this.filterTO.sortBy == 'createdDate') {
        this.currentSortOrder = "";
        if (this.filterTO.sortOrder == 'asc') {
          this.currentSortOrder += "First to Last";
        } else if (this.filterTO.sortOrder == 'desc') {
          this.currentSortOrder += "Last to First";
        }
      } else if (this.filterTO.sortBy == 'modifiedDate') {
        this.currentSortOrder = "";
        if (this.filterTO.sortOrder == 'asc') {
          this.currentSortOrder += "First to Last";
        } else if (this.filterTO.sortOrder == 'desc') {
          this.currentSortOrder += "Last to First";
        }
      } else if (this.filterTO.sortBy == 'firstName') {
        this.currentSortOrder = "";
        if (this.filterTO.sortOrder == 'asc') {
          this.currentSortOrder += "A to Z";
        } else if (this.filterTO.sortOrder == 'desc') {
          this.currentSortOrder += "Z to A";
        }
      }
    }
  }

  private toggleFilter2() {
    this.filterDropDown2 = !this.filterDropDown2;
    if (!this.filterDropDown2) {
      this.filterDropDown = true;
    }
  }
  ngOnInit() {

    this.nameinitialService.randomize();
    this.randomcolor = this.nameinitialService.randomcolor;

    this.pageState.currentPageNumber = 1;
    this.pageState.pageSize = 15;
    this.selected = [];
    this.loaderIndicator = "loader";
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;



    this.loadData();
    this.loadTotalRows();
  
  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.draftCandidatePagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.draftCandidatePagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.data = response;
 
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
     
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;

    this.loadData();


  }

  areAllSelected() {
    if (this.data != null && this.data.length > 0) {
      for (let x = 0; x < this.data.length; x++) {
        if (typeof this.data[x].selected == 'undefined' || (typeof this.data[x].selected == 'boolean' && !this.data[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }


  getProfileCompletionPercentage(item) {
    if (item != null) {
      if (item.requesting) {
        return 'Loading...';
      }
      item.requesting = true;
      if (item.profileCompletionLoading) {
        return 'Loading...';
      }

      if (item.profileCompletionLoaded) {
        item.profileCompletionLoading = false;
        item.requesting = false;
        return item.profileCompletionPercentage + " %";
      } else {

        Promise.resolve(this.draftCandidateListService.getProfileCompletionPercentage(item.candidateID)).then((response) => {
          if (response) {
            if (response.responseData) {
              if (response.responseData.length > 0) {
                let percentage = response.responseData[0].percentage;
                item.profileCompletionLoaded = true;
                item.profileCompletionLoading = false;
                item.profileCompletionPercentage = percentage;
                item.requesting = false;
                item.percentClass = "p" + item.profileCompletionPercentage;
                return item.profileCompletionPercentage + " %";
              }
            }
          }
          return item.profileCompletionPercentage;
        });
      }

      return 'Loading...';
    } else {
      return 'Loading...';
    }
  }
  draftCandidatelist() {
    this.subscription = this.draftCandidateListService.getList().subscribe(res => {
      this.data = res.responseData,
        this.messageCode = res.messageCode;
      if (this.data == null) {
        this.loaderIndicator = "nodata";
      } else {
        this.loaderIndicator = "data";
      }
    });
  }
  deleteDraftCandidate(ReqID, index, item) {
    this.subscription = this.draftCandidateListService.deleteDraftCandidate(ReqID).subscribe((out) => {
      this.messageCode = out.messageCode;
      this.draftCandidatePagedService.removeCachedItem(this.filterTO.pageNumber, this.filterTO.pageSize, index);
      this.data.splice(index, 1);
      this.pageState.totalRows--;
      this.loadData();
      this.i18UtilService.get('candidate.success.draftDeleted').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
    });
  }
  deleteAll() {
    if (this.selected.length == 0) {
      this.i18UtilService.get('candidate.warning.selectCandidate').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    let candidateDraftIDList: any[] = [];
    for (let i of this.selected) {
      candidateDraftIDList.push(i.candidateID)
    };
    this.candidateDraftID = [];
    this.candidateDraftID = candidateDraftIDList;
    this.draftCandidateListService.deleteDraftCandidateBulk(this.candidateDraftID).subscribe(res => {
      this.draftCandidatePagedService.clearCache();
      this.ngOnInit();
      this.i18UtilService.get('candidate.success.draftDeleted').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
    });
    this.selected = [];
  }

  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();
  }

  allSelectEvent() {
    let index = 0;
    for (let data of this.data) {
      data.selected = this.selectAll;
      index++;
      if (this.selectAll && !this.selected.includes(data)) {
        this.selected.push(data);
      } else if (!this.selectAll) {
        let i = this.selected.indexOf(data);
        this.selected.splice(i, 1);
      }
    }
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  ngOnDestroy(): void {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
  }
  getLength(str: String) {
    if (str) {
      return str.length;
    } else {
      return 0;
    }
  }

  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }
  filtterToggleClick() {
    this.filtterToggle = !this.filtterToggle;
  }
  filtterToggleClosed() {
    this.filtterToggle = false;
  }

  public sort(sortParam: string, order: string) {
    this.setCurrentOrder();
    this.filterTO.sortBy = sortParam;
    this.filterTO.sortOrder = order;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.draftCandidatePagedService.clearCache();
    this.loadTotalRows();
    this.loadData();
  }

}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class ResponseData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  primaryEmail: String;
  mobileNumber: String;
  profileImage: String;
  city: String;
  skills: String;
  selected: boolean;
  subSourceUserName: string;
}
