import { Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Jsonp, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from '../../../session.service';
import { CommonService } from '../../../common/common.service';

@Injectable()
export class DraftCandidateListService {

    constructor(private http: Http, private sessionService: SessionService, private commonService: CommonService) {
        this.commonService.showRHS();
    }
    public getList() {
        let options = new RequestOptions({ headers: this.getHeaders() });
        // return this._jsonp.request('https://mpeoplerecruit.sohum.com:8443/ats/rest/altone/candidate/fetch/list/19', options)
        //    .map(res => res.json()).catch((error: any) => Observable.throw(error + 'Server error'));
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/fetch/draft/list/' + this.sessionService.organizationID + "/", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
        //return person;
        //  return this._jsonp.request('https://mpeoplerecruit.sohum.com/ats/rest/candidate/fetch/19/ABbbb', options)
        //  .map(res => res.json);
    }
    getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }
    //Delete Candidate Draft by Requested ID.
    deleteDraftCandidate(CandidateDraftID) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/delete/draft/' + CandidateDraftID + "/", options)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    deleteDraftCandidateBulk(reqDraftIdList: Array<Number>) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/deleteMultipleDraftCandidates/', JSON.stringify(reqDraftIdList), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getProfileCompletionPercentage(candidateId): Promise<any> {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/getProfileCompletionPercentage/', { candidateId: candidateId }, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            }).toPromise();
    }
}

