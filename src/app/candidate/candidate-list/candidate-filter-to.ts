import { CustomTreeNode } from "app/shared/custom-tree/CustomTreeNode.model";

export class CandidateFilterTo {
  genderID: number;
  industryID: number;
  functionalAreaID: number;
  sourceID: number;
  location: string;
  startCreationDate: Date;
  endCreationDate: Date;
  startModificationDate: Date;
  endModificationDate: Date;
  pageNumber: number;
  pageSize: number;
  sortBy: string;
  sortOrder: string;
  searchString: string;
  stageName: string[];
  stageStatusFilter: CustomTreeNode;
  minExperience: number;
  maxExperience: number;
  myApplicants: boolean ;
  orgUnit: string;
  worksite: string;
  jobStatus: string;

  isFiltered(): boolean {
    return this.genderID != null
      || this.industryID != null
      || this.functionalAreaID != null
      || this.sourceID != null
      || this.location != null
      || this.startCreationDate != null
      || this.endCreationDate != null
      || this.startModificationDate != null
      || this.endModificationDate != null
      || this.searchString != null
      || this.stageStatusFilter != null
      || this.minExperience != null
      || this.maxExperience != null
      || this.orgUnit != null
      || this.worksite != null
      || this.jobStatus != null
  }
}
