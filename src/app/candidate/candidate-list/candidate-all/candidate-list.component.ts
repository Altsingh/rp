import { Component, OnInit, OnDestroy } from '@angular/core';
import { CandidateListService } from './../candidate-list.service';
import { JsonpModule } from '@angular/http';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'alt-candidate-list',
  templateUrl: './candidate-list.component.html',
  providers: [CandidateListService, JsonpModule]
})
export class CandidateListComponent implements OnInit, OnDestroy {
  data: ResponseData[];
  messageCode: MessageCode;
  subscription;
  constructor(private candidateListService: CandidateListService, private commonService: CommonService) {
    this.commonService.showRHS();
  }
    getLength(str:String){
    if(str){
      return str.length;
    }else{
      return 0;
    }
  }
  ngOnInit() {
    this.subscription = this.candidateListService.getList().subscribe(res => {
      this.data = res.responseData,
        this.messageCode = res.messageCode;
    });
  }
  
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export interface ResponseData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  primaryEmail: String;
  mobileNumber: String;
  profileImage: String;
  city: String;
  skills: String;
}