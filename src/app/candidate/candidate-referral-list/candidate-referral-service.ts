import { Injectable } from '@angular/core';
import { Jsonp, Http, RequestOptions, Headers } from '@angular/http';
import { CandidateReferralListComponent } from './candidate-referral-list.component';
import { SessionService } from '../../session.service';

@Injectable()
export class CandidateReferralService {

    candidateReferralListComponent: CandidateReferralListComponent;

    constructor(private http: Http, private sessionService: SessionService) { }


    public setSessioServiceObject(object: any) {
        this.sessionService.object = object;
    }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }
    public setCandidateReferralListComponent(referredCandidates: CandidateReferralListComponent) {
        this.candidateReferralListComponent = referredCandidates;
    }

    public getCandidateReferralListComponent(): CandidateReferralListComponent {
        return this.candidateReferralListComponent;
    }
   
}

