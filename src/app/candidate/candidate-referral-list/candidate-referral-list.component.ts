import { Component, OnInit } from '@angular/core';
import { MessageCode } from '../../common/message-code';
import { PageState } from '../../shared/paginator/paginator.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CandidateFilterTo } from '../candidate-list/candidate-filter-to';
import { CandidateReferralService } from './candidate-referral-service';
import { CommonService } from '../../common/common.service';
import { NameinitialService } from '../../shared/nameinitial.service';
import { NotificationService } from '../../common/notification-bar/notification.service';
import { DialogService } from 'app/shared/dialog.service';
import { CandidateReferralPagedService } from './candidate-referral-paged.service';
import { Router } from '@angular/router';
import { SessionService } from '../../session.service';
import { WorkflowStageMapService } from '../../shared/services/workflow-stage-map.service';
import { I18UtilService } from '../../shared/services/i18-util.service';

@Component({
  selector: 'alt-candidate-referral-list',
  templateUrl: './candidate-referral-list.component.html',
  styleUrls: ['./candidate-referral-list.component.css'],
  providers:[CandidateReferralService, CandidateReferralPagedService]
})
export class CandidateReferralListComponent implements OnInit {

  pageLoading: boolean = false;

  pageState: PageState = new PageState();

  queryFormGroup: FormGroup;

  pageLoadingSubscription: Subscription;
  totalRowsSubscription: Subscription;
  workflowStageMapSubscription: Subscription;
  workflowStageMap: any;

  queryTimeout: any;
  showloader: boolean = false;
  loaderIndicator: string;

  data: ResponseData[];
  messageCode: MessageCode;
  subscription;
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  selected: any[] = [];

  selectAll: boolean;
  filterTO: CandidateFilterTo;
  automatchEnabled: boolean;

  dataAfterUntag: ResponseData[];
  pageTitle: string;
  
  responseData: ResponseData;
  constructor(private candidateReferralService : CandidateReferralService,
    private commonService: CommonService, private nameinitialService: NameinitialService,
    private notificationService: NotificationService,private dialogsService: DialogService,
     private candidateReferralPagedService : CandidateReferralPagedService,
     private formBuilder: FormBuilder,
    private router: Router,
    private sessionService: SessionService,
    private workflowStageMapService: WorkflowStageMapService,
    private i18UtilService: I18UtilService) {
      this.workflowStageMapSubscription = this.workflowStageMapService.getMap().subscribe((response) => {
        this.workflowStageMap = response;
        this.totalRowsSubscription = null
      });
  
      this.queryFormGroup = this.formBuilder.group({
        query: ['']
      });
      this.pageTitle="MY REFERRALS";

      this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

        if (this.queryTimeout != null) {
          clearTimeout(this.queryTimeout);
        }
  
        this.queryTimeout = setTimeout(() => {
  
          let query = this.queryFormGroup.controls.query.value;
  
          if (query.length >= 3) {
  
            this.filterTO.pageNumber = 1;
            this.pageState.currentPageNumber = 1;
            this.filterTO.pageSize = this.pageState.pageSize;
            this.filterTO.searchString = query;
            this.candidateReferralPagedService.clearCache();
            this.loadData();
            this.loadTotalRows();
          }
          if (query.trim().length == 0) {
            this.filterTO.searchString = null;
            this.filterTO.pageNumber = 1;
            this.pageState.currentPageNumber = 1;
            this.filterTO.pageSize = this.pageState.pageSize;
            this.candidateReferralPagedService.clearCache();
            this.loadData();
            this.loadTotalRows();
          }
        }, 200);
  
      });

      commonService.showRHS();
      this.randomcolor = nameinitialService.randomcolor;
      this.data = [];
      this.candidateReferralService.setCandidateReferralListComponent(this);
      this.filterTO = new CandidateFilterTo();

     }

     clearCache() {
      this.candidateReferralPagedService.clearCache();
    }
    getPageNumber(pageConfig) {
      this.filterTO.pageNumber = pageConfig.pageNumber;
      this.filterTO.pageSize = pageConfig.pageSize;
  
      this.loadData();
  
  
    }

  ngOnInit() {

    this.nameinitialService.randomize();
    this.randomcolor = this.nameinitialService.randomcolor;
    this.selected = [];
    this.loaderIndicator = "loader";
    this.pageState.currentPageNumber = 1;
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;



    this.candidateReferralPagedService.clearCache();
    this.loadData();
    this.loadTotalRows();

  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.candidateReferralPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    this.pageLoadingSubscription = this.candidateReferralPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.data = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  areAllSelected() {
    if (this.data != null && this.data.length > 0) {
      for (let x = 0; x < this.data.length; x++) {
        if (typeof this.data[x].selected == 'undefined' || (typeof this.data[x].selected == 'boolean' && !this.data[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }

  imageError(event, candidate) {
    candidate.profileImage = null;
  }
  changed(event) {
    this.showloader = event;
  }
  getLength(str: String) {
    if (str) {
      return str.length;
    } else {
      return 0;
    }
  }
  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();
  }

  onSorted(params: string[]) {
    this.filterTO.sortBy = params[0];
    this.filterTO.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    this.filterTO.pageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.pageState.currentPageNumber = 1;
    this.candidateReferralPagedService.clearCache();
    this.loadData();
    this.loadTotalRows();
  }

  onFilter(params: any[]) {
    let genderFilter = false;
    let sourceFilter = false;
    let industryFilter = false;
    let locationFilter = false;
    let functionalAreaFilter = false;
    let stageStatusFilter = false;
    let minExperienceFilter = false;
    let maxExperienceFilter = false;
    let orgUnitFilter = false;
    let workSiteFilter = false;
    let jobStatusFilter= false;
    for (let param of params) {
      if (param.name == 'LOCATION') {
        this.filterTO.location = param.value.value;
        locationFilter = true;
      } else if (param.name == 'GENDER') {
        this.filterTO.genderID = param.value.id;
        genderFilter = true;
      } else if (param.name == 'INDUSTRY') {
        this.filterTO.industryID = param.value.id;
        industryFilter = true;
      } else if (param.name == 'FUNCTIONAL_AREA') {
        this.filterTO.functionalAreaID = param.value.id;
        functionalAreaFilter = true;
      } else if (param.name == 'SOURCE') {
        this.filterTO.sourceID = param.value.id;
        sourceFilter = true;
      } else if (param.name == 'STAGE/STATUS') {
        this.filterTO.stageStatusFilter = param.value;
        stageStatusFilter = true;
      } else if (param.name == 'MIN_EXPERIENCE') {
        this.filterTO.minExperience = param.value.value;
        minExperienceFilter = true;
      } else if (param.name == 'MAX_EXPERIENCE') {
        this.filterTO.maxExperience = param.value.value;
        maxExperienceFilter = true;
      } else if (param.name == 'ORG_UNIT') {
        this.filterTO.orgUnit = param.value.value;
        orgUnitFilter = true;
      } else if (param.name == 'WORKSITE') {
        this.filterTO.worksite = param.value.value;
        workSiteFilter = true;
      } else if(param.name == 'JOB_STATUS'){
        this.filterTO.jobStatus  = param.value.value;
        jobStatusFilter = true;
      }

    }
    if (!genderFilter) {
      this.filterTO.genderID = null;
    }
    if (!industryFilter) {
      this.filterTO.industryID = null;
    }
    if (!functionalAreaFilter) {
      this.filterTO.functionalAreaID = null;
    }
    if (!locationFilter) {
      this.filterTO.location = null;
    }
    if (!sourceFilter) {
      this.filterTO.sourceID = null;
    }
    if (!stageStatusFilter) {
      this.filterTO.stageStatusFilter = null;
    }
    if (!minExperienceFilter) {
      this.filterTO.minExperience = null;
    }
    if (!maxExperienceFilter) {
      this.filterTO.maxExperience = null;
    }
    if (!orgUnitFilter) {
      this.filterTO.orgUnit = null;
    }
    if (!workSiteFilter) {
      this.filterTO.worksite = null;
    }
    if( !jobStatusFilter){
      this.filterTO.jobStatus = null;
    }
    this.candidateReferralPagedService.clearCache();
    this.ngOnInit();
  }
  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    } else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    this.candidateReferralPagedService.clearCache();
    this.ngOnInit();
  }

  getWorkflowProgressBar(workflowID: Number, item: ResponseData) {
    item.stages = this.workflowStageMap[workflowID + ''];
    return true;
  }
  indexOfStage(stage: string, stageList: string[]) {
    return stageList.indexOf(stage);
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED') {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }
  private fullName(candidate: ResponseData): String {
    let candidateName: String;
    if (candidate.firstName) {
      candidateName = candidate.firstName;
    }
    if (candidate.middleName) {
      candidateName = candidateName + ' ' + candidate.middleName;
    }
    if (candidate.lastName) {
      candidateName = candidateName + ' ' + candidate.lastName;
    }
    return candidateName;
  }

}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class ResponseData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowID: number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
  stages: string[];
  statgeStatusType: string;
  subSourceUserName: string;
  inReview: boolean;
  requisitionStatus: String;
}
