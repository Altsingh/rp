import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { PagedDataService } from '../../common/paged-data.service';
import { SessionService } from '../../session.service';

@Injectable()
export class CandidateReferralPagedService extends PagedDataService {

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/posting/myreferrals/',
      '/rest/altone/posting/myreferrals/totalRows/'
    );
  }
}
