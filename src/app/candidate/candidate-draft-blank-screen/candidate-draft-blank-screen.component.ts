import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-candidate-draft-blank-screen',
  templateUrl: './candidate-draft-blank-screen.component.html',
  styleUrls: ['./candidate-draft-blank-screen.component.css']
})
export class CandidateDraftBlankScreenComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
