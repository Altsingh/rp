import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { SessionService } from '../../session.service';
import { CandidateListService, TagCandidateData } from '../../candidate/candidate-list/candidate-list.service';
import { TagCandidateToJobData } from '../../candidate/candidate-list/candidate-untagged/candidate-untag-actions/candidate-job-data';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: '.alt-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  providers: [CandidateListService]
})
export class CandidateDetailComponent implements OnInit {
  highlightedDiv: number;

  fromCompanyDataPage: boolean = false;
  candidateInfo: any;
  jobSummaryData: any;
  jobSkills: any;
  taggedCandidateIds: number[];

  tagging: boolean = false;
  tagged: boolean = false;

  constructor(
    private sessionService: SessionService,
    private location: Location,
    private candidateListService: CandidateListService,
    private notificationService: NotificationService,
    private i18UtilService: I18UtilService) {
    if (this.location.path().endsWith("f=1")) {
      this.fromCompanyDataPage = true;
    } else {
      this.fromCompanyDataPage = false;
    }
    this.candidateInfo = this.sessionService.candidateInfo;
    this.taggedCandidateIds = this.sessionService.taggedCandidateIds;

    if (this.taggedCandidateIds != null && this.taggedCandidateIds.includes(this.candidateInfo.candidate_id)) {
      this.tagged = true;
    }
    if (this.sessionService.jobSummaryData == null) {
      this.fromCompanyDataPage = false;
    } else {
      this.jobSummaryData = this.sessionService.jobSummaryData;
    }


    this.jobSkills = this.sessionService.jobSkills;
  }

  ngOnInit() {
    this.highlightedDiv = 0;
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    }else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  toggleHighlight(newValue: number) {
    if (this.highlightedDiv === newValue) {
      this.highlightedDiv = 0;
    } else {
      this.highlightedDiv = newValue;
    }
  }

  tagToThisJob(event) {

    if (this.tagged) {
      return;
    }

    this.tagging = true;

    this.saveTagCandidateData(this.candidateInfo.candidate_id);

  }
  getlengthLessThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length > 40
  }
  getlengthGreatThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length <= 40
  }
  getlengthLessThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length > 40
  }
  getlengthGreatThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length <= 40
  }
  getlengthZero() {
    return this.jobSummaryData.summaryWorkSite == null || this.jobSummaryData.summaryWorkSite.length == 0
  }

  saveTagCandidateData(candidateId: number) {
    let newTagCandidateData = new TagCandidateData();
    newTagCandidateData.input = new TagCandidateToJobData();
    newTagCandidateData.input.requisitionIDList = [];
    newTagCandidateData.input.requisitionIDList.push(this.sessionService.requisitionId);
    newTagCandidateData.input.candidateID = candidateId;
    this.candidateListService.tagCandidateToJob(newTagCandidateData).subscribe((out) => {
      let result = out.responseData[0];
      let messageCode = out.messageCode;
      if (result == "success") {
        this.tagged = true;
        this.tagging = false;
        this.i18UtilService.get('candidate.success.candidateTagged').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
        this.taggedCandidateIds.push(candidateId);
      } else {
        this.i18UtilService.get('candidate.error.candidateTagError').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
        this.tagging = false;
      }
    });
  }
}
