import { CommonService } from 'app/common/common.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { PersonalDetailsService } from '../candidate-overview/personal-details/personal-details.service';
import { ActivatedRoute } from '@angular/router';
import { NameinitialService } from '../../../shared/nameinitial.service';
import { CandidateService } from '../../candidate.service';
import { FormatCapitalizePipe } from '../../../shared/format-capitalize.pipe';
import { SessionService } from '../../../session.service';
import { ParseCandidatecodeParamPipe } from '../../parse-candidatecode-param.pipe'

@Component({
  selector: 'alt-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  providers: [PersonalDetailsService]
})
export class CandidateProfileComponent implements OnInit, OnDestroy {
  Candidatecode: string;
  data: any;
  messageCode: MessageCode;
  subscription;
  randomcolor: string;
  profileLoader: string;
  contextMenuOpen: boolean = false;
  automatchEnabled: boolean;

  displayClass = "displayNone";

  parser: ParseCandidatecodeParamPipe = new ParseCandidatecodeParamPipe();

  security: any;
  formatCapitalizePipe: FormatCapitalizePipe = new FormatCapitalizePipe();
  allSubscriptions: Subscription[] = [];

  fromCompanyDataPage: boolean = false;
  candidateInfo: any;

  featureDataMap: Map<any,any>;

  constructor(private PersonalDetailsService: PersonalDetailsService, private candidateService: CandidateService,
    private route: ActivatedRoute, private nameinitialService: NameinitialService, private sessionService: SessionService,
    private location: Location, private commonService: CommonService) {

    if (this.location.path().endsWith("f=1")) {
      this.fromCompanyDataPage = true;
    } else {
      this.fromCompanyDataPage = false;
    }

    if (this.sessionService.jobSummaryData == null) {
      this.fromCompanyDataPage = false;
    }
    this.candidateInfo = this.sessionService.candidateInfo;
    this.automatchEnabled =  this.sessionService.isAutoMatchEnabled();

    this.data = new ResponseData()
    this.messageCode = new MessageCode()
    this.randomcolor = nameinitialService.randomcolor[0];
    this.profileLoader = 'loader';
    this.security = {};
    this.initFormSecurity();
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  getPercentage(candidate) {
    let percentage = "0";
    if (candidate.percentage) {
      percentage = ((candidate.percentage) + "").split(".")[0];
    }
    return percentage;
  }
  socialBlock(data) {
    return (data.linkedinProfile != null && data.linkedinProfile.trim() != '' && this.security.linkedInRendered) ||
      (data.twitterProfile != null && data.twitterProfile.trim() != '' && this.security.twitterRendered) ||
      (data.facebookProfile != null && data.facebookProfile.trim() != '' && this.security.facebookRendered) ||
      (data.googleplusProfile != null && data.googleplusProfile.trim() != '' && this.security.googlePlusRendered);
  }
  ngOnInit() {
    this.commonService.showRHS();
    this.Candidatecode = this.parser.transform(this.route.snapshot.params['id']);
    this.subscription = this.PersonalDetailsService.getpersonaldetails(this.route.snapshot.params['id']).subscribe(out => {
      this.messageCode = out.messageCode,
        this.data = out.responseData[0];
      if (this.data == null) {
        this.profileLoader = 'nodata';
      } else {
        this.profileLoader = 'data';
      }
    });
  }

  hideContextMenu() {
    this.contextMenuOpen = false;
  }
  showContextMenu() {
    this.contextMenuOpen = true;
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  imageError(event, candidate) {
    candidate.profileImage = null;
  }

  isEmpty()
  {
    if(this.data.subSourceUsername != null)
    {
      return false;
    }
    else return true;
  }
  private initFormSecurity() {
    this.candidateService.clearCandidateFormSecurity();
    let securitySubscription = this.candidateService.getCandidateFormSecurity().subscribe(
      response => {
        this.security = response.responseData[0];
        if (this.security != null) {
          this.security.basicSectionRendered = true;

          let allKeys = Object.keys(this.security);

          for (let x = 0; x < allKeys.length; x++) {
            if (this.security[allKeys[x]] != null && allKeys[x].toLowerCase().endsWith("label")) {
              if (allKeys[x] == 'companyCTCLabel'
                || allKeys[x] == 'panNumberLabel'
                || allKeys[x] == 'dateOfBirthLabel') {
                continue;
              }
              this.security[allKeys[x]] = this.formatCapitalizePipe.transform(this.security[allKeys[x]]);
            }
          }

        }
      }
    );
    this.allSubscriptions.push(securitySubscription);
  }

}
export class ResponseData {
  firstName: string;
  middleName: string;
  lastName: string;
  city: string;
  sourceType: string;
  subSourceUsername: string;
  profileImage: string;
  title: string;
  resumePath: string;
  facebookProfile: string;
  linkedinProfile: string;
  googleplusProfile: string;
  twitterProfile: string;
  sysSourceType: string;
}
export class MessageCode { }