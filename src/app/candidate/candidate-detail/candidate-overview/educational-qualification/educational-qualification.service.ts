import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class EducationalQualificationService {
  data: String = JSON.stringify('');
  constructor(private http: Http, private sessionService: SessionService) { }

  geteduqualification(id) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/educationInformation/' + this.sessionService.organizationID + '?candidateCode=' + id)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
}
