import { Component, OnInit, Input } from '@angular/core';
import { EducationalQualificationService } from './educational-qualification.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-educational-qualification',
  templateUrl: './educational-qualification.component.html',
  providers: [EducationalQualificationService]
})
export class EducationalQualificationComponent implements OnInit {
  EducationData: ResponseData[];
  messageCode: MessageCode;
  subscription: any;
  educationLoader: string;
  @Input()
  security: any;

  constructor(private EducationalQualificationService: EducationalQualificationService, private route: ActivatedRoute) {
    this.EducationData = [];
    this.messageCode = new MessageCode();
    this.educationLoader = 'loader';
  }

  ngOnInit() {
    let id = this.route.snapshot.parent.params['id'];
    this.subscription = this.EducationalQualificationService.geteduqualification(id).subscribe(out => {
    this.messageCode = out.messageCode,
      this.EducationData = out.responseData;
      if(this.EducationData == null){
        this.educationLoader = 'nodata';
      }
      else if(this.EducationData.length==0){
        this.educationLoader = 'nodata';
      }else{
        this.educationLoader = 'data';
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
export class ResponseData { }
export class MessageCode { }