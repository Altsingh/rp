import { Component, OnInit, Input } from '@angular/core';
import { SkillSetService } from '../skill-set/skill-set.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css'],
  providers: [SkillSetService]
})
export class LanguagesComponent implements OnInit {

  data: string[];
  messageCode: MessageCode;
  subscription: any;
  langugaesLoader: string;
  @Input()
  security: any;
  constructor(private skillsetservice: SkillSetService, private route: ActivatedRoute) {
    this.data = [];
    this.messageCode = new MessageCode();
    this.langugaesLoader = 'loader';
  }

  ngOnInit() {
    let id = this.route.snapshot.parent.params['id'];
    let isLanguage = true;
    this.subscription = this.skillsetservice.getskillset(id, isLanguage).subscribe(out => {
      this.messageCode = out.messageCode,
        this.data = out.responseData;
      if (this.data == null) {
        this.langugaesLoader = 'nodata';
      } else {
        this.langugaesLoader = 'data';
      }
    });
  }
}
export class ResponseData {

}
export class MessageCode {
  message: String;
  description: String;
  code: String;
}