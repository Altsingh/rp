import { Component, OnInit, Input } from '@angular/core';
import { WorkExperiencesService } from './work-experiences.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-work-experiences',
  templateUrl: './work-experiences.component.html',
  providers: [WorkExperiencesService]
})
export class WorkExperiencesComponent implements OnInit {
  WorkExperiences: ResponseData[];
  messageCode: MessageCode;
  subscription: any;
  todaysDate = '';
  workExp: ResponseData;
  workExpLoader: string;
  @Input()
  security: any;
  constructor(private WorkExperiencesService: WorkExperiencesService, private route: ActivatedRoute) {
    this.workExp = new ResponseData();
    this.WorkExperiences = []
    this.messageCode = new MessageCode()
    let date: Date = new Date();
    let month = date.getMonth() + 1;
    let monthStr = month + "";
    if (month < 10) {
      monthStr = "0" + month;
    }
    this.todaysDate = date.getFullYear() + "-" + monthStr + "-" + date.getDate();
    this.workExpLoader = 'loader';
  }
  ngOnInit() {
    let id = this.route.snapshot.parent.params['id'];
    let isInternship = false;
    this.subscription = this.WorkExperiencesService.getWorkExperience(id, isInternship).subscribe(out => {
        this.messageCode = out.messageCode;
        this.WorkExperiences = out.responseData;
        if(this.WorkExperiences != null){
          this.workExp = this.WorkExperiences[0];
        }
      if (this.workExp == null) {
        this.workExpLoader = 'nodata';
      } else {
        this.workExpLoader = 'data';
      }
    });
  }
  getNoticePeriod(workExp: ResponseData): number {
    if (workExp != null && workExp.noticePeriod != null) {
      return workExp.noticePeriod;
    } else {
      return null;
    }
  }
}

export class ResponseData {
  expInYears: number;
  expInMonths: number;
  noticePeriod: number;
  jobTitle: String;
  officialMailId: String;
  companyName: String;
  role: String;
  startDate: Date;
  endDate: Date;
  currentlyWorkingHere: boolean;
}
export class MessageCode { }