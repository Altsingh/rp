import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class WorkExperiencesService {
  data: String = JSON.stringify('');
  constructor(private http: Http, private sessionService: SessionService) { }

  getWorkExperience(id,isInternship) {

    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/workInformation/' + this.sessionService.organizationID  + "/" + isInternship + '?candidateCode=' + id)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

}
