import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { CandidateService } from '../../candidate.service';
import { FormatCapitalizePipe } from '../../../shared/format-capitalize.pipe';

@Component({
  selector: '.alt-candidate-overview',
  templateUrl: './candidate-overview.component.html'
})
export class CandidateOverviewComponent implements OnInit {

  security: any;
  formatCapitalizePipe: FormatCapitalizePipe = new FormatCapitalizePipe();
  allSubscriptions: Subscription[] = [];

  constructor(private candidateService: CandidateService) {
    this.security = {};
    this.initFormSecurity();
   }

  ngOnInit() {
  }

  private initFormSecurity() {
    this.candidateService.clearCandidateFormSecurity();
    let securitySubscription = this.candidateService.getCandidateFormSecurity().subscribe(
      response => {
        this.security = response.responseData[0];
      }
    );
    this.allSubscriptions.push(securitySubscription);
  }
}
