import { Component, OnInit, Input } from '@angular/core';
import { WorkExperiencesService } from '../work-experiences/work-experiences.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-internship-experiences',
  templateUrl: './internship-experiences.component.html',
  styleUrls: ['./internship-experiences.component.css'],
  providers: [WorkExperiencesService]
})

export class InternshipExperiencesComponent implements OnInit {
  internshipExperiences: any[] = [];
  subscription: any;
  internshipExpLoader: string;
   todaysDate = '';
  @Input()
  security: any;
  constructor(private WorkExperiencesService: WorkExperiencesService, private route: ActivatedRoute) {
    
    let date: Date = new Date();
    let month = date.getMonth() + 1;
    let monthStr = month + "";
    if (month < 10) {
      monthStr = "0" + month;
    }
    this.todaysDate = date.getFullYear() + "-" + monthStr + "-" + date.getDate();
    this.internshipExpLoader = 'loader';

  }
  ngOnInit() {
    let id = this.route.snapshot.parent.params['id'];
    let isInternship = true;
    this.subscription = this.WorkExperiencesService.getWorkExperience(id,isInternship).subscribe(out => {
      this.internshipExperiences = out.responseData;
      if (this.internshipExperiences == null) {
        this.internshipExpLoader = 'nodata';
      } else if(this.internshipExperiences.length == 0) {
        this.internshipExpLoader = 'nodata';
      }else{
        this.internshipExpLoader = 'data';
      }
    });
  }
}