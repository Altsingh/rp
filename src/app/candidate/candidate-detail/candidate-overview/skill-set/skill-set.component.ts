import { Component, OnInit, Input } from '@angular/core';
import { SkillSetService } from './skill-set.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-skill-set',
  templateUrl: './skill-set.component.html',
  providers: [SkillSetService]
})
export class SkillSetComponent implements OnInit {
  data: string[];
  messageCode: MessageCode;
  subscription: any;
  skillLoader: string;
  @Input()
  security: any;
  constructor(private skillsetservice: SkillSetService, private route: ActivatedRoute) {
    this.data = [];
    this.messageCode = new MessageCode();
    this.skillLoader = 'loader';
  }

  ngOnInit() {
    let id = this.route.snapshot.parent.params['id'];
    let isLanguage = false;
    this.subscription = this.skillsetservice.getskillset(id,isLanguage).subscribe(out => {
    this.messageCode = out.messageCode,
      this.data = out.responseData;
      if(this.data == null){
        this.skillLoader = 'nodata';
      }else{
        this.skillLoader = 'data';
      }
    });
  }
}
export class ResponseData {

}
export class MessageCode {
  message: String;
  description: String;
  code: String;
}