import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';

@Injectable()
export class SkillSetService {
  data: String = JSON.stringify('');
  constructor(private http: Http, private sessionService: SessionService) { }

  getskillset(id,isLanguage) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/skills/' + this.sessionService.organizationID + "/" + isLanguage +'?candidateCode=' + id)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

}
