import { Component, OnInit, Input } from '@angular/core';
import { PersonalDetailsService } from './personal-details.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'alt-personal-details',
  templateUrl: './personal-details.component.html',
  providers: [PersonalDetailsService]
})

export class PersonalDetailsComponent implements OnInit {
  data: ResponseData;
  messageCode: MessageCode;
  subscription;
  personalDetailLoader: string;
  address: string;

  @Input()
  security: any;

  constructor(private PersonalDetailsService: PersonalDetailsService, private route: ActivatedRoute) {
    this.data = new ResponseData();
    this.messageCode = new MessageCode();
    this.personalDetailLoader = 'loader';
  }

  ngOnInit() {
    let id = this.route.snapshot.parent.params['id'];
    this.subscription = this.PersonalDetailsService.getpersonaldetails(id).subscribe(out => {
      this.messageCode = out.messageCode;
        this.data = out.responseData[0];
        if(this.data ==null){
          this.personalDetailLoader = 'nodata';
        }else{
          this.personalDetailLoader = 'data';
        }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
export class ResponseData {
  firstName: string;
  middleName: string;
  lastName: string;
  mobileNumber: string;
  primaryEmailId: string;
  secondaryEmailId: string;
  panNumber: string;
  aadharNumber: string;
  sourceType: string;
  sourceCode: string;
  sourceName: string;
  industryName: string;
  funcArea: string;
  permanentAdd: string;
  country: string;
  district: string;
  city: string;
  pincode: string;
  landlineNumber: string;
  title: string;
  subSourceUsername: string;

  tital: string;
  firstname: string;
  middlename: string;
  lastname: string;
  mobilenumber: number;
  dob: string;
  age: string;
  pemail: string;
  semail: string;
  panno: string;
  voterId: string;
  passportNumber: string;
  AADHARNumber: string;
  sourcetype: string;
  sourcecode: string;
  sourcename: string;
  industry: string;
  functionalarea: string;
  gender: string;
  language: string;
  permanentresidentialaddress: string;
  Country: string;
  statedistrict: string;
  citylocation: string;
  pincod: string;
  landline: string;
}
export class MessageCode {
  message: String;
  description: String;
  code: String;
}