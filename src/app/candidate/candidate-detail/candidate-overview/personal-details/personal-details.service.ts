import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../../../session.service';


@Injectable()
export class PersonalDetailsService {
    data: String = JSON.stringify('');
    constructor(private http: Http, private sessionService: SessionService) { }

    getpersonaldetails(id) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/personalInformation/' + this.sessionService.organizationID + '?candidateCode=' + id)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    getpersonaldetailsAndIJPDocs(candidateCode,applicantStatusId ) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/personalInformation/' + this.sessionService.organizationID + '?candidateCode=' + candidateCode  + '&applicantStatusId=' + applicantStatusId)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    getCPflags(id) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidateDetail/cpflags/' + this.sessionService.organizationID + '?applicantStatusId=' + id)
            .map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getProfileMatchPercenatage( applicationStatusId ) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/updateProfileMatchPercentage/' + applicationStatusId + "/", "", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getTaggedJobsList( candidateCode ) {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/taggedJobs/' + candidateCode)
        .map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()
        });
    }

    getHeaders(): Headers {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
      }

}
