import { NgModule } from '@angular/core';
import { CandidateComponent } from './candidate.component';
import { CandidateRouting } from './candidate.routing';
import { NewCandidateComponent } from './new-candidate/new-candidate.component';
import { CandidateListComponent } from './candidate-list/candidate-all/candidate-list.component';
import { DraftCandidateListComponent } from './candidate-list/candidate-draft/draft-candidate-list.component';
import { HttpModule, JsonpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { BasicDetailsComponent } from './new-candidate/basic-details/basic-details.component';
import { CandidateUntaggedListComponent } from './candidate-list/candidate-untagged/candidate-untagged-list.component';
import { WorkDetailsComponent } from './new-candidate/work-details/work-details.component';
import { EducationDetailsComponent } from './new-candidate/education-details/education-details.component';
import { SkillsDetailsComponent } from './new-candidate/skills-details/skills-details.component';
import { DocumentsDetailsComponent } from './new-candidate/documents-details/documents-details.component';
import { ParseCandidatecodeParamPipe } from './parse-candidatecode-param.pipe';
import { EditCandidateComponent } from './edit-candidate/edit-candidate.component';
import { CandidateDetailComponent } from './candidate-detail/candidate-detail.component';
import { CandidateOverviewComponent } from './candidate-detail/candidate-overview/candidate-overview.component';
import { CandidateProfileComponent } from './candidate-detail/candidate-profile/candidate-profile.component';
import { PersonalDetailsComponent } from './candidate-detail/candidate-overview/personal-details/personal-details.component';
import { WorkExperiencesComponent } from './candidate-detail/candidate-overview/work-experiences/work-experiences.component';
import { EducationalQualificationComponent } from './candidate-detail/candidate-overview/educational-qualification/educational-qualification.component';
import { SkillSetComponent } from './candidate-detail/candidate-overview/skill-set/skill-set.component';
import { ScorecardComponent } from './candidate-tagged-detail/candidate-tagged-overview/scorecard/scorecard.component';
import { CandidateTaggedDetailComponent } from './candidate-tagged-detail/candidate-tagged-detail.component';
import { CandidateTaggedOverviewComponent } from './candidate-tagged-detail/candidate-tagged-overview/candidate-tagged-overview.component';
import { CandidateTaggedProfileComponent } from './candidate-tagged-detail/candidate-tagged-profile/candidate-tagged-profile.component';
import { CandidateTaggedScreeningComponent } from './candidate-tagged-detail/candidate-tagged-screening/candidate-tagged-screening.component';
import { EditCandidateTaggedComponent } from './edit-candidate-tagged/edit-candidate-tagged.component';
import { HiringProcessComponent } from './candidate-tagged-detail/hiring-process/hiring-process.component';
import { HistoryChatsComponent } from './candidate-tagged-detail/history-chats/history-chats.component';
import { TaggedCandidateListComponent } from './candidate-list/taggedCandidates/tagged-candidate-list.component';
import { CandidateContextMenuComponent } from './candidate-list/candidate-context-menu/candidate-context-menu.component';
import { MatSelectModule, MatOption, MatSliderModule, MatInputModule, MatDialogModule, MatTabsModule, MatAutocompleteModule, MatRadioModule, MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { ScreeningComponent } from './candidate-tagged-detail/hiring-process/screening/screening.component';
import { AssessmentComponent } from './candidate-tagged-detail/hiring-process/assessment/assessment.component';
import { CandidateUntagActionsComponent } from './candidate-list/candidate-untagged/candidate-untag-actions/candidate-untag-actions.component';
import { ShortlistingComponent } from './candidate-tagged-detail/hiring-process/shortlisting/shortlisting.component';
import { InterviewComponent } from './candidate-tagged-detail/hiring-process/interview/interview.component';
import { DocumentComponent } from './candidate-tagged-detail/hiring-process/document/document.component';
import { FitmentComponent } from './candidate-tagged-detail/hiring-process/fitment/fitment.component';
import { OfferGenerationComponent } from './candidate-tagged-detail/hiring-process/offer-generation/offer-generation.component';
import { JoiningComponent } from './candidate-tagged-detail/hiring-process/joining/joining.component';
import { BackgroundVerificationComponent } from './candidate-tagged-detail/hiring-process/background-verification/background-verification.component';
import { CandidateListService } from './candidate-list/candidate-list.service';
import { CandidateService } from './candidate.service';
import { CandidateFormSecurityService } from './candidate-form-security.service';
import { CandidateGenderListService } from './candidate-gender-list.service';
import { CandidateIndustryListService } from './candidate-industry-list.service';
import { CandidateFunctionalAreaListService } from './candidate-functional-area-list.service';
import { CandidateSourceTypeListService } from './candidate-source-type-list.service';
import { TaggedCandidateService } from './candidate-list/taggedCandidates/tagged-candidate.service';
import { SharedModule } from '../shared/shared.module';
import { CandidateActionsComponent } from './candidate-list/candidate-context-menu/candidate-actions/candidate-actions.component';
import { CandidateDraftBlankScreenComponent } from './candidate-draft-blank-screen/candidate-draft-blank-screen.component';
import { CandidatesTaggedBlankScreenComponent } from './candidates-tagged-blank-screen/candidates-tagged-blank-screen.component';
import { CandidatesUntaggedBlankScreenComponent } from './candidates-untagged-blank-screen/candidates-untagged-blank-screen.component';
import { TaggedCandidateDataFilterPipe } from './candidate-list/taggedCandidates/tagged-candidate-data-filter.pipe';
import { UntaggedCandidateDataFilterPipe } from './candidate-list/candidate-untagged/untagged-candidate-data-filter.pipe';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { HiringControllerService } from './hiring-controller.service';
import { CandidateDocumentsComponent } from './candidate-tagged-detail/candidate-tagged-overview/candidate-documents/candidate-documents.component';
import { UntaggedContextMenuComponent } from './candidate-list/candidate-untagged/untagged-context-menu/untagged-context-menu.component';
import { TagCandidateToJobComponent } from './candidate-list/candidate-untagged/tag-candidate-to-job/tag-candidate-to-job.component';
import { CandidateActionService } from './candidate-list/candidate-untagged/candidate-action.service';
import { UntaggedCandidateBulkActionComponent } from './candidate-list/taggedCandidates/untagged-candidate-bulk-action/untagged-candidate-bulk-action.component';
import { CopyToAnotherJobComponent } from './candidate-list/taggedCandidates/copy-to-another-job/copy-to-another-job.component';
import { EmailCandidatesComponent } from './candidate-list/taggedCandidates/email-candidates/email-candidates.component';
import { NotifyRecruiterComponent } from './candidate-list/taggedCandidates/notify-recruiter/notify-recruiter.component';
import { CandidteProfileLoaderComponent } from './candidate-detail/candidate-profile/candidte-profile-loader/candidte-profile-loader.component';
import { CandidatePersonalDetailLoaderComponent } from './candidate-detail/candidate-overview/personal-details/candidate-personal-detail-loader/candidate-personal-detail-loader.component';
import { EducationalQualificationLoaderComponent } from './candidate-detail/candidate-overview/educational-qualification/educational-qualification-loader/educational-qualification-loader.component';
import { SkillSetLoaderComponent } from './candidate-detail/candidate-overview/skill-set/skill-set-loader/skill-set-loader.component';
import { WorkExperienceLoaderComponent } from './candidate-detail/candidate-overview/work-experiences/work-experience-loader/work-experience-loader.component';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { InternshipDetailsComponent } from './new-candidate/internship-details/internship-details.component';
import { LanguageDetailsComponent } from './new-candidate/language-details/language-details.component'
import { DialogService } from '../shared/dialog.service';
import { InternshipExperiencesComponent } from './candidate-detail/candidate-overview/internship-experiences/internship-experiences.component';
import { LanguagesComponent } from './candidate-detail/candidate-overview/languages/languages.component';
import { NewCandidateLoaderComponent } from './new-candidate/new-candidate-loader/new-candidate-loader.component';
import { PreviewOfferComponent } from './candidate-tagged-detail/hiring-process/offer-generation/preview-offer/preview-offer.component';
import { TaggedCandidateHistoryDefaultComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/tagged-candidate-history-default.component';
import { TaggedCandidateHistoryDefaultScreeningComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/screening/tagged-candidate-history-default-screening.component';
import { TaggedCandidateHistoryDefaultShortlistingComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/shortlisting/tagged-candidate-history-default-shortlisting.component';
import { TaggedCandidateHistoryDefaultInterviewComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/interview/tagged-candidate-history-default-interview.component';
import { TaggedCandidateHistoryDefaultOfferComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/offer/tagged-candidate-history-default-offer.component';
import { TaggedCandidateHistoryDefaultJoiningComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/joining/tagged-candidate-history-default-joining.component';
import { TaggedCandidateHistoryDefaultSalaryComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/salary/tagged-candidate-history-default-salary.component';
import { TaggedCandidateHistoryDefaultAssessmentComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/assessment/tagged-candidate-history-default-assessment.component';
import { TaggedCandidateHistoryLoaderComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-loader/tagged-candidate-history-loader.component';
import { BulkUpdateStatusComponent } from './candidate-list/taggedCandidates/bulk-update-status/bulk-update-status.component';
import { BulkUpdateStatusScreeningComponent } from './candidate-list/taggedCandidates/bulk-update-status/bulk-update-status-screening/bulk-update-status-screening.component';
import { BulkUpdateStatusInterviewComponent } from './candidate-list/taggedCandidates/bulk-update-status/bulk-update-status-interview/bulk-update-status-interview.component';
import { FitmentBasicComponent } from './candidate-tagged-detail/hiring-process/fitment/fitment-basic/fitment-basic.component';
import { TaggedCandidateHistoryDefaultDocumentComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/document/tagged-candidate-history-default-document.component';
import { CompensationAndBenifitComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/compensation-and-benifit/compensation-and-benifit.component';
import { BulkCandidateUploadComponent } from './bulk-candidate-upload/bulk-candidate-upload.component';
import { FitmentParametersComponent } from './candidate-tagged-detail/hiring-process/fitment/fitment-parameters/fitment-parameters.component';
import { FitmentProposedRemunerationComponent } from './candidate-tagged-detail/hiring-process/fitment/fitment-proposed-remuneration/fitment-proposed-remuneration.component';
import { LetterOfIntentComponent } from './candidate-tagged-detail/hiring-process/letter-of-intent/letter-of-intent.component';
import { LoiComponent } from './candidate-tagged-detail/history-chats/tagged-candidate-history-default/loi/loi.component';
import { HiringProcessService } from './candidate-tagged-detail/hiring-process/hiring-process.service';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FitmentComputableStackComponent } from './candidate-tagged-detail/hiring-process/fitment/fitment-computable-stack/fitment-computable-stack.component';
import { BulkParseComponent } from 'app/candidate/bulk-parse/bulk-parse.component';
import { BulkParseErrorComponent } from './bulk-parse/bulk-parse-error/bulk-parse-error.component';
import { OfferConfirmationDialogComponent } from './candidate-tagged-detail/hiring-process/offer-generation/confirmation-dialog/offer-confirmation-dialog/offer-confirmation-dialog.component';
import { CandidateHistoryComponent } from './candidate-tagged-detail/candidate-history/candidate-history.component';
import { CandidateHistoryLoaderComponent } from './candidate-tagged-detail/candidate-history-loader/candidate-history-loader.component';
import { CandidateHistoryDataLoaderComponent } from './candidate-tagged-detail/candidate-history-data-loader/candidate-history-data-loader.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { InitiateCtqComponent } from './candidate-tagged-detail/hiring-process/screening/initiate-ctq/initiate-ctq.component';
import { CandidateReferralListComponent } from './candidate-referral-list/candidate-referral-list.component';



@NgModule({
    
    declarations: [
        CandidateComponent, NewCandidateComponent, CandidateListComponent,
        CandidateUntaggedListComponent, CandidateContextMenuComponent,
        DraftCandidateListComponent, BasicDetailsComponent, WorkDetailsComponent,
        EducationDetailsComponent, SkillsDetailsComponent, DocumentsDetailsComponent,
        ParseCandidatecodeParamPipe, EditCandidateComponent,
        CandidateDetailComponent, CandidateOverviewComponent, CandidateProfileComponent,
        PersonalDetailsComponent, WorkExperiencesComponent, EducationalQualificationComponent,
        SkillSetComponent, ScorecardComponent,
        CandidateTaggedDetailComponent, CandidateTaggedOverviewComponent,
        CandidateTaggedProfileComponent, CandidateTaggedScreeningComponent, AssessmentComponent, EditCandidateTaggedComponent,
        HiringProcessComponent, HistoryChatsComponent, TaggedCandidateListComponent,
        CandidateUntagActionsComponent, ScreeningComponent, ShortlistingComponent,
        InterviewComponent, DocumentComponent, FitmentComponent, EmailTemplateComponent,
        OfferGenerationComponent, JoiningComponent, BackgroundVerificationComponent, CandidateActionsComponent,
        CandidateDraftBlankScreenComponent, CandidatesTaggedBlankScreenComponent, CandidatesUntaggedBlankScreenComponent, TaggedCandidateDataFilterPipe, UntaggedCandidateDataFilterPipe,
        CandidateDocumentsComponent, UntaggedContextMenuComponent, TagCandidateToJobComponent, UntaggedCandidateBulkActionComponent,
        CopyToAnotherJobComponent, EmailCandidatesComponent, NotifyRecruiterComponent,
        CandidteProfileLoaderComponent, CandidatePersonalDetailLoaderComponent,
        EducationalQualificationLoaderComponent, SkillSetLoaderComponent, WorkExperienceLoaderComponent, InternshipExperiencesComponent,
        InternshipDetailsComponent, LanguageDetailsComponent, LanguagesComponent, NewCandidateLoaderComponent, PreviewOfferComponent, TaggedCandidateHistoryDefaultComponent,
        TaggedCandidateHistoryDefaultScreeningComponent, TaggedCandidateHistoryDefaultShortlistingComponent, TaggedCandidateHistoryDefaultInterviewComponent,
        TaggedCandidateHistoryDefaultOfferComponent, TaggedCandidateHistoryDefaultJoiningComponent, TaggedCandidateHistoryDefaultSalaryComponent,
        TaggedCandidateHistoryDefaultAssessmentComponent, TaggedCandidateHistoryLoaderComponent, BulkUpdateStatusComponent, BulkUpdateStatusScreeningComponent, BulkUpdateStatusInterviewComponent,
        CompensationAndBenifitComponent,FitmentBasicComponent,TaggedCandidateHistoryDefaultDocumentComponent,
        BulkCandidateUploadComponent,
        FitmentParametersComponent,
        FitmentProposedRemunerationComponent,
        LetterOfIntentComponent,
        LoiComponent,
        FitmentComputableStackComponent,
        CandidateHistoryComponent,
        BulkParseComponent,
        BulkParseErrorComponent,
        OfferConfirmationDialogComponent,
        CandidateHistoryLoaderComponent,
        CandidateHistoryDataLoaderComponent,
        InitiateCtqComponent,
        CandidateReferralListComponent
    ],

    imports: [
        CandidateRouting, HttpModule, JsonpModule,
        CommonModule, FormsModule, ReactiveFormsModule,
        MatSelectModule, MatInputModule, MatSliderModule,
        MatDialogModule, MatTabsModule, MatAutocompleteModule, MatRadioModule, SharedModule, NgSlimScrollModule, MatProgressSpinnerModule, MatTooltipModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
          }
        })
    ],
    entryComponents: [
        CandidateUntagActionsComponent, CandidateActionsComponent, EmailTemplateComponent, TagCandidateToJobComponent, UntaggedCandidateBulkActionComponent, CopyToAnotherJobComponent, EmailCandidatesComponent, NotifyRecruiterComponent, PreviewOfferComponent, BulkUpdateStatusComponent,BulkParseComponent,BulkParseErrorComponent,OfferConfirmationDialogComponent,CandidateHistoryLoaderComponent,CandidateHistoryDataLoaderComponent
    ],

    providers: [
        CandidateListService, CandidateService, CandidateFormSecurityService, CandidateGenderListService,
        CandidateIndustryListService, CandidateFunctionalAreaListService, CandidateSourceTypeListService,
        TaggedCandidateService, HiringControllerService, CandidateActionService, DialogService, HiringProcessService
    ]
})
export class CandidateModule { }

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
  }
