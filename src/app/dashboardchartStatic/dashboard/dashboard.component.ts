import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { DashboardCharts } from '../common/dashboard-charts';
import { WidgetRequestTO } from '../common/WidgetRequestTO';
import { CommonService } from '../../common/common.service';

import { DashboardLoaderComponent } from "app/dashboardchartStatic/loader/dashboard-loader.component";
import { DashboardTO } from '../common/dashboard';
import { SessionService } from 'app/session.service'

import { ChartTO } from '../common/widget'
import {
    Router, ActivatedRoute,
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
} from '@angular/router';
import { DashboardChartService } from '../dashboard-chart.service';
import * as FusionCharts from "fusioncharts";
import { OrgUnitTypeTO, OrgUnitTO } from "app/dashboardchartStatic/common/orgnuit";
//import { MessageCode } from '../../common/message-code';
@Component({
    changeDetection: ChangeDetectionStrategy.Default,
    selector: 'alt-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],

})
export class DashboardComponent implements OnInit {
    chartId: string = "object-";
    rest: boolean = false;
    dashbaordData: Array<DashboardCharts>;
    xyz: number = -1;
    loaderIndex: number = -1;
    loaderForIndex: number = -1;
    zoomIn: boolean = false;
    popup: boolean = false;
    orgUnitPopup: boolean = false;
    layoutVal: number = 2;
    layoutLabel: string = "Layout 1X2";
    loading: boolean = false;
    userId: number;
    organizationID: number;
    roleType: string;
    active: string = ""
    dropdownActive: string = ""
    selectedUserFilterId: number;
    openlist: number = -1;
    openTimelist: number = -1;
    openlistdrop: boolean = false;
    activeTimeFilter: string = "";
    openTimelistdrop: boolean = false;
    openOrgUnitListdrop: boolean = false;
    header: boolean;
    errorMessage: string;
    renderchartElements: boolean = true;
    renderSelectedchartElement: boolean = false;
    error: boolean = false;
    dashboardList: Array<DashboardTO>;
    unitTypeList: Array<OrgUnitTypeTO> = null;
    selectedFusionChartElement: any;
    dashboadLoad: boolean = false;
    dashboardActive: boolean = false;
    selectedUnitTypeName: string = "Company";
    selectedUnitName: string;
    overlay: boolean = false;
    zoomOut: boolean = false;
    isClassZoom: boolean = false;
	isClassZoom15: boolean = false;
	isClassVisible15: boolean = false;
    isClassVisible: boolean = false;
    isClassVisible1: boolean = false;
    isClassVisible2: boolean = false;
    isClassVisible3: boolean = false;
    isClassVisible4: boolean = false;
    isClassVisible5: boolean = false;
    isClassVisible6: boolean = false;
    isClassVisible7: boolean = false;
    isClassVisible8: boolean = false;
    isClassVisible9: boolean = false;
    isClassVisible10: boolean = false;
    isClassVisible11: boolean = false;
    isClassVisible12: boolean = false;
    isClassVisible13: boolean = false;
    isClassVisible14: boolean = false;
    isClassZoom1: boolean = false;
    isClassZoom2: boolean = false;
    isClassZoom3: boolean = false;
    isClassZoom4: boolean = false;
    isClassZoom5: boolean = false;
    isClassZoom6: boolean = false;
    isClassZoom7: boolean = false;
    isClassZoom8: boolean = false;
    isClassZoom9: boolean = false;
    isClassZoom10: boolean = false;
    isClassZoom11: boolean = false;
    isClassZoom12: boolean = false;
    isClassZoom13: boolean = false;
    isClassZoom14: boolean = false;

    displayClass: String = "displayNone";
    constructor(private cdr: ChangeDetectorRef, private session: SessionService, private _router: Router, private _route: ActivatedRoute, private _dashboardChartServcies: DashboardChartService, private commonService: CommonService) {

    }


    /**
    * ngOnInit
    */

    ngOnInit() {
        this.commonService.hideRHS();
        this.dashboadLoad = false;
        this.dashboardActive = false;
        this.openlist = -1;
        this.active = "";
        this.openlistdrop = false;
        this.openTimelist = -1;
        this.openTimelistdrop = false;
        Promise.resolve(this._dashboardChartServcies.getSessionDetails().then((res) => {
            this.organizationID = res.organizationId;
            this.userId = res.userId;
            this.selectedUserFilterId = res.userId;
            this.roleType = res.roleTypes.toString().replace('[', '').replace(']', '')
            while(this.roleType.indexOf(",")>0) {
                this.roleType = this.roleType.replace(',', '|');
            }
            this.populateInitialDashboard();
        }));
    }


    /**
    * populating initial dashboard
    */

    populateInitialDashboard() {
        this.rest = false;
        let dashboardId: number = 0;
        this.error = false;
        let i = 0;
        this.orgUnitPopup = false;
        this.errorMessage = "unexpected error occured.";
        this.error = true;
        this._dashboardChartServcies.getDashboardChartList(dashboardId, this.roleType, this.userId, this.organizationID).subscribe(data => {
            if (data.responseData != null) {
                this.dashbaordData = data.responseData;
                this.dashbaordData.forEach(dashboard => {
                    this.dashboardList = dashboard.dashboardList;
                    this.unitTypeList = dashboard.orgUnitTypeList;
                    dashboard.widgetTOList.forEach(
                        widget => {
                            widget.filterName = "My Data"
                            widget.selectedUnitTypeName = this.selectedUnitTypeName;
                        }
                    )
                }
                )
            } else {
                this.errorMessage = data.messageCode.message;
                this.error = true;
            }
            this.rest = true;
            this.dashboadLoad = true;
            this.dashboardActive = true;
            this.error = false;
        });
    }

    /**
     * on changing dashboard
     */


    dashboardClicked(dahsboardID) {
        this.dashboadLoad = true;
        let roleType: string = null;
        this.dashboardActive = false;
        this.dashboardActive = false;
        this.openlist = -1;
        this.active = "";
        this.openlistdrop = false;
        this.openTimelist = -1;
        this.openTimelistdrop = false;
        this.rest = false;
        this.orgUnitPopup = false;
        this.selectedUserFilterId = this.userId;
        this._dashboardChartServcies.getDashboardChartList(dahsboardID, roleType, this.userId, this.organizationID).subscribe(data => {
            this.dashbaordData = data.responseData;
            this.dashbaordData.forEach(dashboard => {
                this.unitTypeList = dashboard.orgUnitTypeList;
                dashboard.widgetTOList.forEach(
                    widget => {
                        widget.filterName = "My Data"
                        widget.selectedUnitTypeName = this.selectedUnitTypeName;
                    }
                )
            })
            this.rest = true;
            this.renderchartElements = true;
        });
    }

    addClass(event, ind) {
        if (this.layoutVal > 1) {
            this.xyz = ind;
            this.zoomIn = !this.zoomIn;
            this.overlay = !this.overlay;
            this.zoomOut = !this.zoomOut;


        }
    }

    /**
     * showing more option dropdown
     */

    moreOption(event, ind) {
        this.xyz = ind;
        this.popup = !this.popup;

    }

    /**
     * showing team filter dropdown
     */

    showDropdownLayoutList(event, ind) {
        this.openlist = ind;
        if (this.dropdownActive == "active") {
            this.openlistdrop = false;
            this.dropdownActive = "";
        }
        else {

            this.dropdownActive = "active"
            this.openlistdrop = true;
        }
    }

    showDropdownList(event, ind) {
        this.openlist = ind;
        if (this.active == "active") {
            this.openlistdrop = false;
            this.active = "";
        }
        else {

            this.active = "active"
            this.openlistdrop = true;
        }
    }

    /**
     * showing time filter dropdown
     */

    showDropdownTimeList(event, ind) {
        this.openTimelist = ind;
        if (this.activeTimeFilter == "active") {
            this.openTimelistdrop = false;
            this.activeTimeFilter = "";
        }
        else {

            this.activeTimeFilter = "active"
            this.openTimelistdrop = true;
        }
    }

    /**
     * redering chart on pre-defined dom element with unique id
     */

    renderCharts() {
        this.dashbaordData.forEach(dashboard =>
            dashboard.widgetTOList.forEach(
                widget => {
                    widget.filterName = "My Data"
                    widget.chartTOList.forEach(chart => {
                        let selectedChart = FusionCharts.items[this.chartId + dashboard.dashBoardName + chart.chartID];
                        if (selectedChart != undefined)
                            selectedChart.dispose();
                        new FusionCharts({
                            id: this.chartId + dashboard.dashBoardName + chart.chartID,
                            type: chart.chartType,
                            renderAt: 'container-' + chart.chartID,
                            dataFormat: 'xml',
                            dataSource: chart.chartXml,
                            width: "100%",
                            height: 275
                        }).render();
                    });

                }))
        this.renderchartElements = false;
        this.cdr.detectChanges();
    }

    /**
     * on selection of layout
     */

    changeLayout(event, val) {
        this.active = "active";
        this.layoutVal = val;
        if (this.layoutVal == 1) {
            this.layoutLabel = "Layout 1X1";
        } else if (this.layoutVal == 2) {
            this.layoutLabel = "Layout 1X2";

        } else if (this.layoutVal == 3) {
            this.layoutLabel = "Layout 1X3";
        }
        this.openlistdrop = false;
        this.active = "";
    }

    /**
     * on selection of team filers
     */


    onTeamSelect(event, index, selectedUserId, name) {
        this.loaderIndex = index;
        let widgetRequestTO: WidgetRequestTO = new WidgetRequestTO();
        widgetRequestTO.sysChartTO = this.dashbaordData[0].widgetTOList[index].chartTOList[0];
        widgetRequestTO.teamList = this.dashbaordData[0].widgetTOList[index].teamList;
        this.dashbaordData[0].widgetTOList[index].filterName = name;
        widgetRequestTO.userID = selectedUserId;
        widgetRequestTO.organizationID = this.organizationID;
        widgetRequestTO.orgUnitId = 0;
        this.selectedUserFilterId = selectedUserId;
        this._dashboardChartServcies.getFilterChartData(widgetRequestTO)
            .subscribe(
            data => {
                let selectedChart = FusionCharts.items[this.chartId + this.dashbaordData[0].dashBoardName + this.dashbaordData[0].widgetTOList[index].chartTOList[0].chartID];
                selectedChart.setXMLData(data.response.xmlData);
                this.loaderIndex = -1;
                this.selectedFusionChartElement = selectedChart;
                this.renderSelectedchartElement = true;
                //  selectedChart.render();

            }
            );
        this.openlistdrop = false;
        this.active = "";
    }

    /**
     * on selection of time filers
     */

    onTimeFilterSelect(event, index, selectedTimeFilterId, timeFilterName) {
        this.loaderIndex = index;
        let widgetRequestTO: WidgetRequestTO = new WidgetRequestTO();
        widgetRequestTO.sysChartTO = this.dashbaordData[0].widgetTOList[index].chartTOList[0];
        widgetRequestTO.teamList = this.dashbaordData[0].widgetTOList[index].teamList;
        this.dashbaordData[0].widgetTOList[index].timeFilterName = timeFilterName;
        widgetRequestTO.userID = this.selectedUserFilterId;
        widgetRequestTO.timeFileterId = selectedTimeFilterId;
        widgetRequestTO.organizationID = this.organizationID;
        widgetRequestTO.orgUnitId = 0;
        this._dashboardChartServcies.getFilterChartData(widgetRequestTO)
            .subscribe(
            data => {
                this.loading = true;
                let selectedChart = FusionCharts.items[this.chartId + this.dashbaordData[0].dashBoardName + this.dashbaordData[0].widgetTOList[index].chartTOList[0].chartID];
                selectedChart.setXMLData(data.response.xmlData);
                this.loaderIndex = -1;
                this.selectedFusionChartElement = selectedChart;
                this.renderSelectedchartElement = true;
                //  selectedChart.render();
            }
            );
        this.openTimelistdrop = false;
        this.activeTimeFilter = "";

    }

    renderSelectedChart() {
        this.selectedFusionChartElement.render();
        this.renderSelectedchartElement = false;
        this.cdr.detectChanges();
    }





    /**
     * on selection of orgunit Type filer
     */

    onUnitTypeSelect(event, index, selectedUnitTypeId, selectedUnitTypeName) {
        this.loaderForIndex = index;
        this.dashbaordData[0].widgetTOList[index].selectedUnitTypeName = selectedUnitTypeName;
        this._dashboardChartServcies.getOrgUnitListByType(selectedUnitTypeId)
            .subscribe(
            data => {

                if (data.responseData != null) {
                    this.dashbaordData[0].widgetTOList[index].orgUnitList = data.responseData;
                    this.loading = true;
                    this.loaderForIndex = -1;
                    this.orgUnitPopup = true;
                    this.moreOption(event, index);
                }
            }
            );
    }

    onOrgUnitSelect(event, index, selectedUnitId, selectedUnitName) {
        this.dashbaordData[0].widgetTOList[index].selectedUnitTypeName = selectedUnitName;
        this.loaderIndex = index;
        let widgetRequestTO: WidgetRequestTO = new WidgetRequestTO();
        widgetRequestTO.sysChartTO = this.dashbaordData[0].widgetTOList[index].chartTOList[0];
        widgetRequestTO.userID = this.userId;
        widgetRequestTO.orgUnitId = selectedUnitId;
        widgetRequestTO.organizationID = this.organizationID;
        this._dashboardChartServcies.getFilterChartData(widgetRequestTO)
            .subscribe(
            data => {
                this.loading = true;
                let selectedChart = FusionCharts.items[this.chartId + this.dashbaordData[0].dashBoardName + this.dashbaordData[0].widgetTOList[index].chartTOList[0].chartID];
                selectedChart.setXMLData(data.response.xmlData);
                this.loaderIndex = -1;
                this.selectedFusionChartElement = selectedChart;
                this.renderSelectedchartElement = true;

            }
            );
        this.openOrgUnitListdrop = false;


    }
    showUnitDropdownList(event, ind) {
        this.openlist = ind;
        if (this.active == "active") {
            this.openOrgUnitListdrop = false;
            this.active = "";
        }
        else {

            this.active = "active"
            this.openOrgUnitListdrop = true;
        }
    }

}
