import { Routes, RouterModule } from '@angular/router';
import { DashboardChartResolver } from '../dashboardchartStatic/route-resolver/dashboard-chart-resolver';
import { DashboardComponent } from '../dashboardchartStatic/dashboard/dashboard.component';
import { ModuleWithProviders } from "@angular/core";

const DASHBOARD_ROUTES: Routes = [
   
   {
        path: '', component: DashboardComponent,
        children: [
            {
                path: ':id', component: DashboardComponent,
              
          }
        ]
   }


];


export const DashboardRouting: ModuleWithProviders = RouterModule.forChild(DASHBOARD_ROUTES);