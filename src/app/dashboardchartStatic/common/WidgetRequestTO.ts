import{ChartTO} from './widget'
import{MyTeamTO} from './widget'

export class WidgetRequestTO {
    sysChartTO:ChartTO;
    teamList: Array<MyTeamTO>;
    organizationID:Number;
    userID:Number;
    timeFileterId:Number
    orgUnitId:Number
    hbasePointer: String;
    roleType: String;
    dashboardID: Number;
    portalType: Number;
}
