

export class DashboardChartTO {
    dashBoardName: string
    chartName: string
    chartID: Number
    chartXml: string
    chartWidth: Number
    chartHeight: Number
    chartType: string
  
}
