
import { OrgUnitTO } from "app/dashboardchartStatic/common/orgnuit";

export class WidgetTO {
    widgetName: string
    widgetId: Number
    widgetWidth: Number
    widgetHeight: Number
    chartTOList: Array<ChartTO>;
    filterList: Array<DimTimeTO>;
    teamList: Array<MyTeamTO>;
    userID: Number;
    filterName:string
    filterId:string
    showFilter:boolean
    timeFilterName:string
    selectedUnitTypeName:string;
     orgUnitList: Array<OrgUnitTO>;
}

export class ChartTO {
    chartName: string
    chartID: Number
    chartXml: string
    lableName: string
    parentMemberId: string
    drillpathID: Number
    chartWidth: Number
    chartHeight: Number
    chartType: string
    filterValue: Number
    measureTOList: Array<MeasureTO>
    dimensionLevelID: Number
    filterSecurityString: string
    userIdFilter: string
    parentCategoryDimensionID: Number
    parentCategoryDimensionLevelId: Number
    relatedLevelID: Number
    relatedDimensionID: Number
    status: string
    errorDetail: string
    dataFilter: String;
    periodValue: String;
    yearPeriodFilterList: any[]
    levelID:Number;
    className:string;
    methodName:string

}


export class MeasureTO {
    measureID: Number
    measureName: string
    displayName: string
    renderAs: string
    axisToScale: string
    parentYAxis: string
    type: string
    parentMeasureId1: Number
    parentMeasureId2: Number
    dataFilter: string
}


export class DimTimeTO {
    id: Number
    name: string
  
}


export class MyTeamTO {
    id: Number
    name: string
}
