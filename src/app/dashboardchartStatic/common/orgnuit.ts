
export class OrgUnitTypeTO {
    unitTypeId: Number
    unitTypeName: string
}

export class OrgUnitTO {
    orgUnitId: Number
    unitName: string
	orgunitHierarchy:string
}
