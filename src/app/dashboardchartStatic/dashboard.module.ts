import { NgModule } from '@angular/core';

import { DashboardChartService } from '../dashboardchartStatic/dashboard-chart.service';
import { DashboardComponent } from '../dashboardchartStatic/dashboard/dashboard.component';
import { CommonModule }       from '@angular/common';
import{AltRouting} from '../alt.routing';
import { DashboardLoaderComponent } from "app/dashboardchartStatic/loader/dashboard-loader.component";
import{DashboardRouting} from './dashboard-routing'
@NgModule({
    declarations: [
       DashboardComponent,DashboardLoaderComponent,
       
  ],
    imports: [
        CommonModule,DashboardRouting],
        
    providers: [DashboardChartService]
   
})
  

export class DashboardModule { }
