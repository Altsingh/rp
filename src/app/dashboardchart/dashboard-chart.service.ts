import { Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { WidgetRequestTO } from './common/WidgetRequestTO';
import { SessionService } from 'app/session.service'
import { environment } from '../../environments/environment';

@Injectable()
export class DashboardChartService {
  private _callurl: string;
  private serviceUrl: string = environment.inferURL;
  private _hbasePointer: any;
  constructor(private _http: Http, private session: SessionService) {
    this.getHbasePointer();

    if( window.location.hostname.includes( ".peoplestrongalt.com" ) ){
      this.serviceUrl = this.serviceUrl.replace( ".peoplestrong.com", ".peoplestrongalt.com" );
    }
   }

  getOrgUnitListByType(unitTypeId: number): any {
    let options = new RequestOptions({ headers: this.getHeaders() });
    this._callurl = this.serviceUrl + '/insights/services/infer/filterOrgunit/' + unitTypeId;
    return this._http.get(this._callurl)
      .map((res: Response) =>
        res.json()
      ).toPromise().catch(this._errorhandle);
  }


  getFilterChartData(widgetRequestTO: WidgetRequestTO): any {
    let options = new RequestOptions({ headers: this.getHeaders() });
    this._callurl = this.serviceUrl + '/insights/services/infer/filterdashboardcharts';
    return this._http.post(this._callurl, JSON.stringify(widgetRequestTO), options)
      .map((res: Response) =>
        res.json()
      ).catch(this._errorhandle);

  }

  getHbasePointer() {
    if (this.serviceUrl.indexOf("sohum") > 0) {
      // this._urlKey = this._urlKey + "_" + "sohum";
      this._hbasePointer = "_" + "sohum";
    } else if (this.serviceUrl.indexOf("uat") > 0) {
      // this._urlKey = this._urlKey + "_" + "uat";
      this._hbasePointer = "_" + "uat";
    } else if (this.serviceUrl.indexOf("staging") > 0) {
      // this._urlKey = this._urlKey + "_" + "staging";
      this._hbasePointer = "_" + "staging";
    }
  }

  public getDashboardChartList(dashboardID: number, roleType: string, userID: number,organizationID: number) {

    let callUrl = this.serviceUrl + '/insights/services/infer/dashboardcharts/';// + this.session.userInfo.organizationId + '/' + this.session.userInfo.userId + '/' + dahsboardID + '/' + roleType + '/' + this._hbasePointer + '/1';

    let options =
      new RequestOptions({
        headers: this.getHeaders()
      });

    let widgetRequestTO:
      WidgetRequestTO =
      new WidgetRequestTO();

    widgetRequestTO.organizationID = organizationID;
    widgetRequestTO.hbasePointer = this._hbasePointer;
    widgetRequestTO.userID = userID;
    widgetRequestTO.dashboardID = dashboardID;
    widgetRequestTO.roleType = roleType;
    widgetRequestTO.portalType = 5;
    return this._http.post(callUrl,JSON.stringify(widgetRequestTO),options).map((res:Response) =>res.json());
  }

  public getSessionDetails(): any {
    let _callurlDetails = this.session.orgUrl + '/rest/altone/jobs/getSessionDetails/';
    return this._http.get(_callurlDetails)
      .map(res => { this.session.check401Response(res.json()); return res.json() }).toPromise();
  }

  public getInitailDashboard(organizationID:number): any {


    this._callurl = this.serviceUrl + '/insights/services/infer/initialDashboard/' + organizationID;
    return this._http.get(this._callurl)
      .map((res: Response) =>
        res.json()
      ).catch(this._errorhandle);

  }



  _errorhandle(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error");
  }

  httpGet(theUrl): string {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false);
    xmlHttp.send(null);
    return xmlHttp.responseText;
  }
  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }

}

