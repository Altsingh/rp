import { Injectable } from '@angular/core';
import {
  Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot, Params
} from '@angular/router';
import { DashboardCharts } from '../common/dashboard-charts';
import { DashboardChartService } from '../dashboard-chart.service';
import { Observable } from 'rxjs/';

@Injectable()
export class DashboardChartResolver implements Resolve<DashboardCharts> {
  private _dahsboardId: number;
  constructor(private _dashboardChartServcies: DashboardChartService,
    private _router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DashboardCharts> {
    this._dahsboardId = route.params['id'];
    return Observable.fromPromise(Promise.resolve(this._dashboardChartServcies.getSessionDetails().then((res) => {
      return this._dashboardChartServcies.getDashboardChartList(this._dahsboardId, null, 0, res.organizationId);
    })))
  }
}