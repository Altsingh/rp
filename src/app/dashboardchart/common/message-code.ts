export class MessageCode {
    code        : String;
    message     : String;
    description : String;
    severity    : String;
}
