import{WidgetTO} from './widget'
import{DashboardTO} from './dashboard'
import { OrgUnitTypeTO } from "app/dashboardchart/common/orgnuit";

export class DashboardCharts {
    dashboardID: Number
    dashboardLabel: string
    dashBoardName: string
    dashBoardDataFilter: string
    widgetTOList: Array<WidgetTO>;
    dashboardList: Array<DashboardTO>;
    orgUnitTypeList: Array<OrgUnitTypeTO>;
}

export class DashboardLayout {
    id: number
    label:string
}