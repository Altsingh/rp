
export class OrgUnitTypeTO {
    unitTypeId: number
    unitTypeName: string
    id: number
    label:string
}

export class OrgUnitTO {
    orgUnitId: Number
    unitName: string
    orgunitHierarchy:string
    id: number
    label:string
}
