import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './dashboard-loader.component.html',
  styleUrls: ['./dashboard-loader.component.css']
})


export class DashboardLoaderComponent implements OnInit {

  @Input("isWelcomeLoader") isWelcomeLoader: Boolean;

  constructor() { }

  ngOnInit() {
  }

}
