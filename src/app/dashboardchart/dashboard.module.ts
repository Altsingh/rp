import { NgModule } from '@angular/core';

import { DashboardChartService } from '../dashboardchart/dashboard-chart.service';
import { DashboardComponent } from '../dashboardchart/dashboard/dashboard.component';
import { FormsModule, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { CommonModule }       from '@angular/common';
import{AltRouting} from '../alt.routing';
import { DashboardLoaderComponent } from "app/dashboardchart/loader/dashboard-loader.component";
import{DashboardRouting} from './dashboard-routing'
import { SharedModule } from '../shared/shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@NgModule({
    declarations: [
       DashboardComponent,DashboardLoaderComponent,
       
  ],
    imports: [CommonModule, SharedModule, DashboardRouting, FormsModule, ReactiveFormsModule, TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
    }
  }) ],
        
    providers: [DashboardChartService]
   
})
  

export class DashboardModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
