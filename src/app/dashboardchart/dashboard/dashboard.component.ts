import { CustomSingleSelectComponent } from './../../shared/custom-single-select/custom-single-select.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef, QueryList, ViewChildren, ViewChild } from '@angular/core';
import { DashboardCharts, DashboardLayout } from '../common/dashboard-charts';
import { WidgetRequestTO } from '../common/WidgetRequestTO';
import { CommonService } from '../../common/common.service';

import { DashboardLoaderComponent } from "app/dashboardchart/loader/dashboard-loader.component";
import { DashboardTO } from '../common/dashboard';
import { SessionService } from 'app/session.service'

import { ChartTO } from '../common/widget'
import {
    Router, ActivatedRoute,
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
} from '@angular/router';
import { DashboardChartService } from '../dashboard-chart.service';
import * as FusionCharts from 'fusioncharts';
import { OrgUnitTypeTO, OrgUnitTO } from "app/dashboardchart/common/orgnuit";

//import { MessageCode } from '../../common/message-code';
@Component({
    changeDetection: ChangeDetectionStrategy.Default,
    selector: 'alt-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],

})
export class DashboardComponent implements OnInit {

    chartId: string = "object-";
    rest: boolean = false;
    dashbaordData: Array<DashboardCharts>;
    xyz: number = -1;
    loaderIndex: number = -1;
    loaderForIndex: number = -1;
    zoomIn: boolean = false;
    popup: boolean = false;
    orgUnitPopup: boolean = false;
    layoutVal: number = 2;
    layoutLabel: string = "Layout 1X2";
    loading: boolean = false;
    userId: number;
    organizationID: number;
    roleType: string;
    active: string = ""
    dropdownActive: string = ""
    selectedUserFilterId: number;
    openlist: number = -1;
    openTimelist: number = -1;
    openlistdrop: boolean = false;
    activeTimeFilter: string = "";
    openTimelistdrop: boolean = false;
    openOrgUnitListdrop: boolean = false;
    header: boolean;
    errorMessage: string;
    renderchartElements: boolean = true;
    renderSelectedchartElement: boolean = false;
    error: boolean = false;
    dashboardList: Array<DashboardTO>;
    unitTypeList: Array<OrgUnitTypeTO> = null;
    selectedFusionChartElement: any;
    dashboadLoad: boolean = false;
    dashboardActive: boolean = false;
    selectedUnitTypeName: string = "Company";
    selectedUnitName: string;
    overlay: boolean = false;
    zoomOut: boolean = false;
    isClassZoom: boolean = false;
	isClassZoom15: boolean = false;
	isClassVisible15: boolean = false;
    isClassVisible: boolean = false;
    isClassVisible1: boolean = false;
    isClassVisible2: boolean = false;
    isClassVisible3: boolean = false;
    isClassVisible4: boolean = false;
    isClassVisible5: boolean = false;
    isClassVisible6: boolean = false;
    isClassVisible7: boolean = false;
    isClassVisible8: boolean = false;
    isClassVisible9: boolean = false;
    isClassVisible10: boolean = false;
    isClassVisible11: boolean = false;
    isClassVisible12: boolean = false;
    isClassVisible13: boolean = false;
    isClassVisible14: boolean = false;
    isClassZoom1: boolean = false;
    isClassZoom2: boolean = false;
    isClassZoom3: boolean = false;
    isClassZoom4: boolean = false;
    isClassZoom5: boolean = false;
    isClassZoom6: boolean = false;
    isClassZoom7: boolean = false;
    isClassZoom8: boolean = false;
    isClassZoom9: boolean = false;
    isClassZoom10: boolean = false;
    isClassZoom11: boolean = false;
    isClassZoom12: boolean = false;
    isClassZoom13: boolean = false;
    isClassZoom14: boolean = false;
    isWelcomeLoader: boolean = true;

    layoutValues: Array<DashboardLayout>;

    orgUnitFilterExclusion: Array<string> = [];
    
    chartForm: FormGroup;
    localOrgUnitList: Array<Array<OrgUnitTO>>;

    displayClass: String = "displayNone";
    constructor(
        private cdr: ChangeDetectorRef, 
        private session: SessionService, 
        private _router: Router, 
        private _route: ActivatedRoute, 
        private _dashboardChartServcies: DashboardChartService, 
        private commonService: CommonService,
        private formBuilder: FormBuilder
    ) {
        
        this.fillExclusions();
        this.fillLayouts();
        this.localOrgUnitList = new Array<Array<OrgUnitTO>>();
        this.chartForm = this.formBuilder.group({
            layoutSelectedId: 2,
            orgUnitTypeSelectedId: [] = [],
            orgUnitListSelectedId: [] = [],
            yearSelectedId: [] = []
        });
        this.chartForm.controls.orgUnitTypeSelectedId.setValue(new Array());
        this.chartForm.controls.orgUnitListSelectedId.setValue(new Array());
        this.chartForm.controls.yearSelectedId.setValue(new Array());

    }

    getDummySelectElement(){
        let dummyyy = new OrgUnitTO();
        dummyyy.id = 0;
        dummyyy.label = "Select";
        dummyyy.unitName = "Select";
        return dummyyy;
    }
    
    bindLayoutValue(dashboardLayout: DashboardLayout) {
        this.layoutVal = dashboardLayout.id;
        this.chartForm.controls.layoutSelectedId.setValue(dashboardLayout.id);
    }
    
    bindOrgUnitTypeListValue(orgUnitType, chartIndex) {
        let existing = this.chartForm.controls.orgUnitTypeSelectedId.value;
        existing[chartIndex] = orgUnitType.id;
        this.chartForm.controls.orgUnitTypeSelectedId.setValue(existing);
        this.onUnitTypeSelect(chartIndex, orgUnitType.id);
    }
    
    bindOrgUnitListValue(orgUnit, chartIndex) {
        let existing = this.chartForm.controls.orgUnitListSelectedId.value;
        existing[chartIndex] = orgUnit.id;
        this.chartForm.controls.orgUnitListSelectedId.setValue(existing);
        this.onOrgUnitSelect(chartIndex, orgUnit.id);
    }
    
    bindTimeFilterValue(year, chartIndex) {
        let existing = this.chartForm.controls.yearSelectedId.value;
        existing[chartIndex] = year.id;
        this.chartForm.controls.yearSelectedId.setValue(existing);
        this.onTimeFilterSelect(chartIndex, year.id);
    }

    fillExclusions(){
        this.orgUnitFilterExclusion = [];       //must be initalized always
        this.orgUnitFilterExclusion.push("MIX SOURCE MIX");
        this.orgUnitFilterExclusion.push("MIX DEMOGRAPHICS");
    }

    fillLayouts(){
        this.layoutValues = new Array<DashboardLayout>();
        let dashbardLayout = new DashboardLayout();

        dashbardLayout.id = 1;
        dashbardLayout.label = "Layout 1X1";
        this.layoutValues.push(dashbardLayout);

        dashbardLayout = new DashboardLayout()
        dashbardLayout.id = 2;
        dashbardLayout.label = "Layout 1X2";
        this.layoutValues.push(dashbardLayout);

        dashbardLayout = new DashboardLayout()
        dashbardLayout.id = 3;
        dashbardLayout.label = "Layout 1X3";
        this.layoutValues.push(dashbardLayout);
    }

    /**
    * ngOnInit
    */

    ngOnInit() {
        this.commonService.hideRHS();
        this.dashboadLoad = false;
        this.dashboardActive = false;
        this.openlist = -1;
        this.active = "";
        this.openlistdrop = false;
        this.openTimelist = -1;
        this.openTimelistdrop = false;
        Promise.resolve(this._dashboardChartServcies.getSessionDetails().then((res) => {
            this.organizationID = res.organizationId;
            this.userId = res.userId;
            this.selectedUserFilterId = res.userId;
            this.roleType = res.roleTypes.toString().replace('[', '').replace(']', '')
            while(this.roleType.indexOf(",")>0) {
                this.roleType = this.roleType.replace(',', '|');
            }
            this.populateInitialDashboard();
        }));
    }


    /**
    * populating initial dashboard
    */

    populateInitialDashboard() {
        this.rest = false;
        let dashboardId: number = 0;
        this.error = false;
        let i = 0;
        this.orgUnitPopup = false;
        this.errorMessage = "unexpected error occured.";
        this.error = true;
        this._dashboardChartServcies.getDashboardChartList(dashboardId, this.roleType, this.userId, this.organizationID).subscribe(data => {
            if (data.responseData != null) {
                this.dashbaordData = data.responseData;
                this.dashbaordData.forEach(dashboard => {
                    this.dashboardList = dashboard.dashboardList;

                    dashboard.orgUnitTypeList.forEach(element => {
                        element.id = element.unitTypeId;
                        element.label = element.unitTypeName;
                    });
                    this.unitTypeList = dashboard.orgUnitTypeList;

                    Promise.resolve(this._dashboardChartServcies.getOrgUnitListByType(this.unitTypeList[0].id).then((data) => {
                        data.responseData.unshift(this.getDummySelectElement());
                        this.localOrgUnitList[this.unitTypeList[0].id] = data.responseData;
                        
                        let thiss = this;
                        dashboard.widgetTOList.forEach(function (widget, i) {
                            widget.filterName = "My Data";

                            if (widget.filterList != null) {
                                widget.filterList.forEach(filterElement => {
                                    filterElement.label = filterElement.name;
                                });
                            }

                            
                            let existingYearList = thiss.chartForm.controls.yearSelectedId.value;
                            existingYearList[i] = widget.filterList[0].id;
                            thiss.chartForm.controls.yearSelectedId.setValue(existingYearList);

                            let existingList = thiss.chartForm.controls.orgUnitListSelectedId.value;
                            existingList[i] = thiss.localOrgUnitList[thiss.unitTypeList[0].id][0].id;
                            thiss.chartForm.controls.orgUnitListSelectedId.setValue(existingList);
                            
                            let existing = thiss.chartForm.controls.orgUnitTypeSelectedId.value;
                            existing[i] = thiss.unitTypeList[0].id
                            thiss.chartForm.controls.orgUnitTypeSelectedId.setValue(existing);
                            thiss.onUnitTypeSelect(i, thiss.unitTypeList[0].id);
                            
                        }
                        )
                        
                    } ) );
                }
                )
            } else {
                this.errorMessage = data.messageCode.message;
                this.error = true;
            }
            this.rest = true;
            this.dashboadLoad = true;
            this.dashboardActive = true;
            this.error = false;
            this.isWelcomeLoader = false;
        });
    }

    /**
     * on changing dashboard
     */


    dashboardClicked(dahsboardID) {
        this.dashboadLoad = true;
        let roleType: string = null;
        this.dashboardActive = false;
        this.dashboardActive = false;
        this.openlist = -1;
        this.active = "";
        this.openlistdrop = false;
        this.openTimelist = -1;
        this.openTimelistdrop = false;
        this.rest = false;
        this.orgUnitPopup = false;
        this.selectedUserFilterId = this.userId;
        this._dashboardChartServcies.getDashboardChartList(dahsboardID, roleType, this.userId, this.organizationID).subscribe(data => {
            this.dashbaordData = data.responseData;
            this.dashbaordData.forEach(dashboard => {
                this.unitTypeList = dashboard.orgUnitTypeList;
                dashboard.widgetTOList.forEach(
                    widget => {
                        widget.filterName = "My Data"
                    }
                )
            })
            this.rest = true;
            this.renderchartElements = true;
        });
    }

    addClass(event, ind) {
        if (this.layoutVal > 1) {
            this.xyz = ind;
            this.zoomIn = !this.zoomIn;
            this.overlay = !this.overlay;
            this.zoomOut = !this.zoomOut;


        }
    }
    
    /**
     * redering chart on pre-defined dom element with unique id
     */

    renderCharts() {
        this.dashbaordData.forEach(dashboard =>
            dashboard.widgetTOList.forEach(
                widget => {
                    widget.filterName = "My Data"
                    widget.chartTOList.forEach(chart => {
                        let selectedChart = FusionCharts.items[this.chartId + dashboard.dashBoardName + chart.chartID];
                        if (selectedChart != undefined)
                            selectedChart.dispose();
                        new FusionCharts({
                            id: this.chartId + dashboard.dashBoardName + chart.chartID,
                            type: chart.chartType,
                            renderAt: 'container-' + chart.chartID,
                            dataFormat: 'xml',
                            dataSource: chart.chartXml,
                            width: "100%",
                            height: 275
                        }).render();
                    });

                }))
        this.renderchartElements = false;
        this.cdr.detectChanges();
    }

    /**
     * on selection of team filers
     */


    onTeamSelect(event, index, selectedUserId, name) {
        this.loaderIndex = index;
        let widgetRequestTO: WidgetRequestTO = new WidgetRequestTO();
        widgetRequestTO.sysChartTO = this.dashbaordData[0].widgetTOList[index].chartTOList[0];
        widgetRequestTO.teamList = this.dashbaordData[0].widgetTOList[index].teamList;
        this.dashbaordData[0].widgetTOList[index].filterName = name;
        widgetRequestTO.userID = selectedUserId;
        widgetRequestTO.organizationID = this.organizationID;
        widgetRequestTO.orgUnitId = 0;
        this.selectedUserFilterId = selectedUserId;
        this._dashboardChartServcies.getFilterChartData(widgetRequestTO)
            .subscribe(
            data => {
                let selectedChart = FusionCharts.items[this.chartId + this.dashbaordData[0].dashBoardName + this.dashbaordData[0].widgetTOList[index].chartTOList[0].chartID];
                selectedChart.setXMLData(data.response.xmlData);
                this.loaderIndex = -1;
                this.selectedFusionChartElement = selectedChart;
                this.renderSelectedchartElement = true;
                //  selectedChart.render();

            }
            );
        this.openlistdrop = false;
        this.active = "";
    }

    /**
     * on selection of time filers
     */

    onTimeFilterSelect(index, selectedTimeFilterId) {
        this.loaderIndex = index;
        let widgetRequestTO: WidgetRequestTO = new WidgetRequestTO();
        widgetRequestTO.sysChartTO = this.dashbaordData[0].widgetTOList[index].chartTOList[0];
        widgetRequestTO.teamList = this.dashbaordData[0].widgetTOList[index].teamList;
        widgetRequestTO.userID = this.selectedUserFilterId;
        widgetRequestTO.timeFileterId = selectedTimeFilterId;
        widgetRequestTO.organizationID = this.organizationID;
        widgetRequestTO.orgUnitId = this.chartForm.controls.orgUnitListSelectedId.value[index];
        this._dashboardChartServcies.getFilterChartData(widgetRequestTO)
            .subscribe(
            data => {
                this.loading = true;
                let selectedChart = FusionCharts.items[this.chartId + this.dashbaordData[0].dashBoardName + this.dashbaordData[0].widgetTOList[index].chartTOList[0].chartID];
                selectedChart.setXMLData(data.response.xmlData);
                this.loaderIndex = -1;
                this.selectedFusionChartElement = selectedChart;
                this.renderSelectedchartElement = true;
                //  selectedChart.render();
            }
            );
        this.openTimelistdrop = false;
        this.activeTimeFilter = "";

    }

    renderSelectedChart() {
        this.selectedFusionChartElement.render();
        this.renderSelectedchartElement = false;
        this.cdr.detectChanges();
    }

    /**
     * on selection of orgunit Type filer
     */

    onUnitTypeSelect(index, selectedUnitTypeId) {
        this.loaderForIndex = index;
        if( this.localOrgUnitList == null || this.localOrgUnitList[selectedUnitTypeId] === undefined ){
            Promise.resolve(this._dashboardChartServcies.getOrgUnitListByType(selectedUnitTypeId).then((data) => {
                data.responseData.unshift(this.getDummySelectElement());
                this.localOrgUnitList[selectedUnitTypeId] = data.responseData;
                this.onUnitTypeSelectFunction(index, selectedUnitTypeId);
            } ) );
        }else{
            this.onUnitTypeSelectFunction(index, selectedUnitTypeId);
        }
        
    }

    onUnitTypeSelectFunction(index, selectedUnitTypeId){
        this.localOrgUnitList[selectedUnitTypeId].forEach(orgUnit => {
            orgUnit.label = orgUnit.unitName;
        } );

        this.dashbaordData[0].widgetTOList[index].orgUnitList = this.localOrgUnitList[selectedUnitTypeId];

        let existingList = this.chartForm.controls.orgUnitListSelectedId.value;
        existingList[index] = this.localOrgUnitList[selectedUnitTypeId][0].id;
        this.chartForm.controls.orgUnitListSelectedId.setValue(existingList);
        this.onOrgUnitSelect(index, this.localOrgUnitList[selectedUnitTypeId][0].id);

        this.loading = true;
        this.loaderForIndex = -1;
        this.orgUnitPopup = true;
    }

    onOrgUnitSelect(index, selectedUnitId) {
        this.loaderIndex = index;
        
        let widgetRequestTO: WidgetRequestTO = new WidgetRequestTO();
        widgetRequestTO.sysChartTO = this.dashbaordData[0].widgetTOList[index].chartTOList[0];
        widgetRequestTO.userID = this.userId;
        widgetRequestTO.orgUnitId = selectedUnitId;
        widgetRequestTO.organizationID = this.organizationID;
        widgetRequestTO.timeFileterId = this.chartForm.controls.yearSelectedId.value[index];
        this._dashboardChartServcies.getFilterChartData(widgetRequestTO)
            .subscribe(
            data => {
                this.loading = true;
                let selectedChart = FusionCharts.items[this.chartId + this.dashbaordData[0].dashBoardName + this.dashbaordData[0].widgetTOList[index].chartTOList[0].chartID];
                selectedChart.setXMLData(data.response.xmlData);
                this.loaderIndex = -1;
                this.selectedFusionChartElement = selectedChart;
                this.renderSelectedchartElement = true;

            }
            );
        this.openOrgUnitListdrop = false;
    }

}
