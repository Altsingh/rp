import { Headers, RequestOptions, Response, Jsonp } from '@angular/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../session.service';

@Injectable()
export class RhsService {
  data: String = JSON.stringify('');
  constructor(private http: Http, private sessionService: SessionService) {
  }

  getInterviews(requestJSON) {
    const options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.getURL(), requestJSON, options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json();
      });
  }

  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  getURL() {
    return (this.sessionService.orgUrl + '/rest/altone/candidate/getInterviewforCurrentDate');
  }
}