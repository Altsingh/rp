import { Component, OnInit, ViewChild } from '@angular/core';
import { MonsterService } from 'app/job/job-detail/auto-matched-candidates/job-board-profiles/monster/monster/monster.service';
import { Subscription } from 'rxjs';
import { CommonService } from '../../common/common.service';
import { SessionService } from '../../session.service';
import { PostToJobBoardService } from '../../job/job-boards/post-to-job-board.service';
import { SelectItem } from '../../shared/select-item';
import { AutoMatchMonsterPagedService } from '../../job/job-detail/auto-match-monster-paged.service';

@Component({
  selector: 'alt-rhs-jobboard',
  templateUrl: './rhs-jobboard.component.html',
  styleUrls: ['./rhs-jobboard.component.css'],
  providers: [PostToJobBoardService]
})
export class RhsJobboardComponent implements OnInit {

  toggleFilter: boolean = false;
  currentRequestJSON: any;
  dropdownLabel: string = 'SORT BY';
  rhsMonsterDisabled: boolean;
  rhsSubscription: Subscription;
  locationList: SelectItem[];
  industryList: SelectItem[];
  functionalAreaList: SelectItem[];
  roleList: SelectItem[];
  clearall: boolean = false;
  jobBoardID: number;
  minSalaryList: SelectItem[];
  maxSalaryList: SelectItem[];
  salaryRange: SelectItem[];
  minSalary : number;
  maxSalary : number;
  jobBoardStatus : any;


  autoMatchMonsterFilters: any[][] = [];
  currentAppliedFilters: string[] = [];
  salaryFilter : string ='';
  toggleCurrentLocation: boolean;
  toggleIndustry: boolean;
  toggleFunctionalArea: boolean;
  toggleRole: boolean;
  toggleSalary: boolean;

  popUpActiveSection: string = "";
  suggestion: string = "";
  suggestionResult: string[] = [];
  suggestionLoading: boolean = false;
  suggestionLoaded: boolean = false;
  filterLoading: boolean = true;
  labelCTC :string = '';

  @ViewChild('locationInput')
  locationInput: any;

  constructor(
    private monsterService: MonsterService,
    private commonService: CommonService,
    private sessionService: SessionService,
    private postToJobBoardService: PostToJobBoardService,
    private autoMatchMonsterPagedService: AutoMatchMonsterPagedService
  ) {

  }

  ngOnInit() {
    
    this.locationList = new Array<SelectItem>();
    
    this.rhsMonsterDisabled = false;
    this.rhsSubscription = this.commonService.isrhsMonsterDisabled().subscribe((enabled) => {
      setTimeout(() => {
        this.rhsMonsterDisabled = enabled;
      });
    });

    this.minSalaryList = this.getSalaryRange();
    this.maxSalaryList = this.getSalaryRange();
    this.salaryRange = this.getSalaryRange();
    this.autoMatchMonsterFilters['location'] = [];
    this.autoMatchMonsterFilters['industry'] = [];
    this.autoMatchMonsterFilters['functionalArea'] = [];
    this.autoMatchMonsterFilters['role'] = [];
    this.autoMatchMonsterFilters['salary'] = [];
    this.autoMatchMonsterPagedService.getClearFilters().subscribe((clear) => {
      this.autoMatchMonsterFilters['location'] = [];
      this.autoMatchMonsterFilters['industry'] = [];
      this.autoMatchMonsterFilters['functionalArea'] = [];
      this.autoMatchMonsterFilters['role'] = [];
      this.autoMatchMonsterFilters['salary'] = [];
    });
    

    let lastRequestJSON = this.autoMatchMonsterPagedService.getLastRequestJSON();
    if(this.sessionService.monsterFilter){
    if (lastRequestJSON) {
      if(lastRequestJSON['salary'] && lastRequestJSON['salary'].length>0){
        let salary : string[] = lastRequestJSON['salary'][0].split('-');
        this.minSalary = parseInt(salary[0]);
        this.maxSalary = parseInt(salary[1]);
        this.labelCTC = this.getSalaryLabel(this.minSalary,this.maxSalary);
      }
      let allFilters : string[] = ['location','industry','role','functionalArea'];
      for(let item of allFilters){
          if(lastRequestJSON[item]){
            for(let elem of lastRequestJSON[item])
              this.currentAppliedFilters.push(elem);
          }
      }
      if(this.labelCTC.length>0)
        this.currentAppliedFilters.push(this.labelCTC);
    }
    }
    this.postToJobBoardService.getMonsterJobBoardID().subscribe(response =>{
      this.jobBoardID = response.response; 
    Promise.resolve(this.postToJobBoardService.getJobBoardMaster("MONSTER", "INDIANLOC").toPromise()).then((response) => {
      if (response) {
        if (response.responseData.length > 0) {
          this.locationList = response.responseData;
          this.filterLoading = false;
          for (let item of this.locationList) {
              this.autoMatchMonsterFilters['location'].push({ key: item.label, doc_count: '' });
              if (this.autoMatchMonsterFilters['location'].length >= 4) {
                  break;
              }
          }
        }
      }
    });

    Promise.resolve(this.postToJobBoardService.getJobBoardMaster("MONSTER", "INDUSTRY").toPromise()).then((response) => {
      if (response) {
        if (response.responseData.length > 0) {
          this.industryList = response.responseData;
          this.filterLoading = false;
          for (let item of this.industryList) {
              this.autoMatchMonsterFilters['industry'].push({ key: item.label, doc_count: '' });
              if (this.autoMatchMonsterFilters['industry'].length >= 4) {
                  break;
              }
          }
        }
      }
    });

    Promise.resolve(this.postToJobBoardService.getJobBoardMaster("MONSTER", "FUNCTIONAL_AREA").toPromise()).then((response) => {
      if (response) {
        if (response.responseData.length > 0) {
          this.functionalAreaList = response.responseData;
          this.filterLoading = false;
          for (let item of this.functionalAreaList) {
              this.autoMatchMonsterFilters['functionalArea'].push({ key: item.label, doc_count: '' });
              if (this.autoMatchMonsterFilters['functionalArea'].length >= 4) {
                  break;
              }
          }
        }
      }
    });

    Promise.resolve(this.postToJobBoardService.getJobBoardMaster("MONSTER", "ROLE").toPromise()).then((response) => {
      if (response) {
        if (response.responseData.length > 0) {
          this.roleList = response.responseData;
          this.filterLoading = false;
          for (let item of this.roleList) {
              this.autoMatchMonsterFilters['role'].push({ key: item.label, doc_count: '' });
              if (this.autoMatchMonsterFilters['role'].length >= 4) {
                  break;
              }
          }
        }
      }
    });
    this.sessionService.monsterFilter = false;
  });
  }

  ngOnDestroy(): void {
    if (this.rhsSubscription != null) {
      this.rhsSubscription.unsubscribe();
    }
  }

  changeListener(value: any) {
    if( this.rhsMonsterDisabled ){
      return;
    }
    this.autoMatchMonsterPagedService.getFilterApplied().next({
      'action': 'add',
      'type': 'sort',
      'event': value
    });
    this.autoMatchMonsterPagedService.sortBy = value;
  }

  setSortByLabel(value: string) {
    if( this.rhsMonsterDisabled ){
      return;
    }
    this.autoMatchMonsterPagedService.sortByLabel = value;
  }

  clickCurrentLocation() {
    this.toggleCurrentLocation = !this.toggleCurrentLocation;
  }

  clickIndustry() {
    this.toggleIndustry = !this.toggleIndustry;
  }

  clickFunctionalArea() {
    this.toggleFunctionalArea = !this.toggleFunctionalArea;
  }

  clickRole() {
    this.toggleRole = !this.toggleRole;
  }

  clickSalary() {
    this.toggleSalary = !this.toggleSalary;
  }

  addMore(filterType) {
    if( this.rhsMonsterDisabled ){
      return;
    }
    this.popUpActiveSection = filterType;
    this.suggestion = "";
    this.suggestionResult = [];
  
    setTimeout(() => {
      this.locationInput.nativeElement.value = '';
      this.locationInput.nativeElement.focus();
      
    }, 100);
  
  }
  addFilter(filterType: string, filterValue: any) {
    this.popUpActiveSection = "";
    this.suggestion = "";
    this.autoMatchMonsterFilters[filterType].push({ key: filterValue.label, doc_count: '' });
    if(filterType != 'salary'){
      this.currentAppliedFilters.push(filterValue.label);
    }
    this.autoMatchMonsterPagedService.getFilterApplied().next({
      'action': 'add',
      'type': filterType,
      'event': filterValue.label
    });
  }

  autoMatchMonsterToggleFilter(event, filterType: string) {
    if( this.rhsMonsterDisabled ){
      return;
    }
    if (event.target.checked) {
      if(this.currentAppliedFilters.indexOf(event.target.value) != -1){
        this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(event.target.value),1);
      }
      this.currentAppliedFilters.push(event.target.value);
      this.autoMatchMonsterPagedService.getFilterApplied().next({
        'action': 'add',
        'type': filterType,
        'event': event
      });
    } else {
      if(this.currentAppliedFilters.indexOf(event.target.value) != -1){
        this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(event.target.value),1);
      }
      // this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(event.target.value),1);
      this.autoMatchMonsterPagedService.getFilterApplied().next({
        'action': 'remove',
        'type': filterType,
        'event': event
      });
    }
  }

  loadMasterSuggestions(filterType: string, queryString: string) {
    this.suggestionResult = [];
    this.suggestion = queryString;
    this.suggestionLoading = true;
    this.suggestionLoaded = false;
    let resultList = [];
    if(filterType=='location'){
      for (let option of this.locationList) {
        if (queryString.trim() != '' && option.label.toLowerCase().indexOf(queryString.toLowerCase()) >= 0 && !this.existsInFilter(filterType, option) && !this.existsInOptions(filterType, option)) {
          resultList.push(option);
        }
      }
    } else if(filterType=='industry'){
      for (let option of this.industryList) {
        if (queryString.trim() != '' && option.label.toLowerCase().indexOf(queryString.toLowerCase()) >= 0 && !this.existsInFilter(filterType, option) && !this.existsInOptions(filterType, option)) {
          resultList.push(option);
        }
      }
    } else if(filterType=='functionalArea'){
      for (let option of this.functionalAreaList) {
        if (queryString.trim() != '' && option.label.toLowerCase().indexOf(queryString.toLowerCase()) >= 0 && !this.existsInFilter(filterType, option) && !this.existsInOptions(filterType, option)) {
          resultList.push(option);
        }
      }
    } else if(filterType=='role'){
      for (let option of this.roleList) {
        if (queryString.trim() != '' && option.label.toLowerCase().indexOf(queryString.toLowerCase()) >= 0 && !this.existsInFilter(filterType, option) && !this.existsInOptions(filterType, option)) {
          resultList.push(option);
        }
      }
    }
    this.suggestionLoading = false;
    this.suggestionLoaded = true;
    this.suggestionResult = resultList;
  }  

  existsInFilter(filterType: string, filterValue) {
    let lastRequestJSON = this.autoMatchMonsterPagedService.getLastRequestJSON();
    if (lastRequestJSON) {
      let includeJSON = lastRequestJSON;
      if (includeJSON[filterType]) {
        if (includeJSON[filterType].length > 0) { }
        for (let filterJSON of includeJSON[filterType]) {
          if (filterJSON == filterValue) {
            return true;
          }
        }
      }
    }
    return false;
  }
  existsInOptions(filterType: string, filterValue) {
    let filterOptions = this.autoMatchMonsterFilters[filterType];
    if (filterOptions) {
      for (let filter of filterOptions) {
        if (filter.key.trim().toLowerCase() == filterValue.label.trim().toLowerCase()) {
          return true;
        }
      }
    }
    return false;
  }
  getCurrentFilters() {
    let lastRequestJSON = this.autoMatchMonsterPagedService.getLastRequestJSON();
    if (lastRequestJSON) {
      this.currentRequestJSON = lastRequestJSON;
      if(Object.keys(this.currentRequestJSON).length !== 0)
        this.clearall =true;
      else
        this.clearall =false;
    }
    return this.currentRequestJSON;
  }
  showMoreFilters(){
    if( this.rhsMonsterDisabled ){
      return;
    }
    this.toggleFilter = !this.toggleFilter;
  }
  clearAll(event, filterType: string) {
    this.currentAppliedFilters = [];
    this.minSalary = 0;
    this.maxSalary = 0;
    this.minSalaryList = this.salaryRange;
    this.maxSalaryList = this.salaryRange;
    this.autoMatchMonsterPagedService.getFilterApplied().next({
      'action': 'remove',
      'type': filterType,
      'event': event
    });
  } 
  removeFilter(event, filterType: string) {
    if(this.currentAppliedFilters.indexOf(this.salaryFilter) != -1){
      this.minSalary=0;this.maxSalary=0;
      this.minSalaryList = this.salaryRange;
      this.maxSalaryList = this.salaryRange;
    }
    if(this.currentAppliedFilters.indexOf(event) != -1){
      this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(event),1);
    }
    // this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(event),1);
    this.autoMatchMonsterPagedService.getFilterApplied().next({
      'action': 'remove',
      'type': filterType,
      'event': event
    });
  }
  getSalaryRange(){
    var items: SelectItem[] = [];
    for(let i=0;i<=100;i++){
      items.push(new SelectItem(i.toString(), i));
    }
    return items;
  }
  setMinSalary(event){
    this.minSalary = event.value;
    this.maxSalaryList = this.salaryRange.slice(this.minSalary+1,this.salaryRange.length);
    if(this.maxSalary == undefined)
      this.maxSalary = 0;
    this.applySalaryFilter();
  }
  setMaxSalary(event){
    this.maxSalary = event.value;
    this.minSalaryList = this.salaryRange.slice(0,this.maxSalary);
    if(this.minSalary== undefined)
      this.minSalary = 0;
    this.applySalaryFilter();
  }
  applySalaryFilter(){
    if(this.minSalary != undefined && this.maxSalary != undefined){
        if(this.minSalary>0 && this.maxSalary>0 && this.maxSalary <= this.minSalary ){
          return;
        }
        let filterType :string = 'salary';
        let filterValue : any = {}; 
        filterValue.label = this.minSalary + '-' + this.maxSalary;
        let label :string = this.getSalaryLabel(this.minSalary,this.maxSalary);
        if(this.currentAppliedFilters.indexOf(this.salaryFilter) != -1){
          this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(this.salaryFilter),1);
        }
        if(this.labelCTC.length>0 &&  this.currentAppliedFilters.indexOf(this.labelCTC) != -1){
          this.currentAppliedFilters.splice(this.currentAppliedFilters.indexOf(this.labelCTC),1);
          this.labelCTC='';
        }
        if(label.length>0){
          this.currentAppliedFilters.push(label);
          this.salaryFilter = label;
        }
        this.addFilter(filterType,filterValue);
    }
  }
  getSalaryLabel(minSalary,maxSalary):string{
    let label:string='';
    if(minSalary==0 && maxSalary > 0){
      label = 'max sal: ' + this.maxSalary + ' lacs';
    } else if(maxSalary==0 && minSalary > 0){
      label = 'min sal: ' +  this.minSalary + ' lacs';
    } else if(minSalary > 0 && maxSalary > 0){
      label = minSalary + '-' + maxSalary + ' lacs';
    }
    return label;
  }
}
