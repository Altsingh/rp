import { JobCommonServiceService } from './../job/job-common-service.service';
import { AutoMatchSocialPagedService } from './../job/job-detail/auto-match-social-paged.service';
import { HomeCardsService } from './../home/home-cards/home-cards.service';
import { CardSructure, HomeCardsResponse } from './../home/home-cards/home-cards-request-response';
import { HomeCardsCachedService } from './../home/home-cards/home-cards-cached.service';
import { Component, OnInit, Input, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { CommonService } from '../common/common.service';
import { RhsService } from './rhs.service';
import { InterviewLoaderComponent } from './rhs-loader/interview-loader.component';
import { Subscription } from 'rxjs';
import { SessionService } from '../session.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-rhs',
  templateUrl: './rhs.component.html',
  styleUrls: ['./rhs.component.css'],
  providers: [RhsService, HomeCardsService]
})
export class RhsComponent implements OnInit, OnDestroy {
  Jobs: any;
  Interviews: any;
  messageCode: any;
  subscription: Subscription;
  interview: any;
  interviewLoader: string;
  selectedDate: Date = new Date();
  whichSideBar: string = 'default';
  currentDate: Date;
  mintDate: Date;
  maxDate: Date;
  activeSection: string = '';
  toggleInterviews: boolean;
  toggleActionPending: boolean;
  toggleExperience: boolean;
  toggleCurrentLocation: boolean;
  toggleIndustry: boolean;
  toggleCurrentCompany: boolean;
  togglePastCompany: boolean;
  toggleOthers: boolean;
  homeCardsResponse: HomeCardsResponse;
  targetDateCards: CardSructure[];
  assignedCards: CardSructure[];
  prepareSalaryFitmentCards: CardSructure[];
  shortlistingRejectedCards: CardSructure[];
  offerPendingCards: CardSructure[];
  actionsPending: CardSructure[];
  dataInitialized: boolean;
  actionsPendingloaderIndicator: string = '';

  tasksCount: number = 0;
  isVisible: boolean;
  countPendingAction: number = 0;

  autoMatchSocialFilters: any[][] = [];

  popUpActiveSection: string = "";
  suggestion: string = "";
  suggestionLoading: boolean = false;
  suggestionLoaded: boolean = false;
  suggestionResult: string[] = [];
  suggestionSubscription: Subscription;
  filterLoading: boolean = true;
  
  rhsSocialDisabled: boolean;
  rhsSubscription: Subscription;

  @ViewChild('locationInput')
  locationInput: any;

  @ViewChild('industryInput')
  industryInput: any;

  @ViewChild('currentCompanyInput')
  currentCompanyInput: any;

  @ViewChild('pastCompanyInput')
  pastCompanyInput: any;
  autoMatchSocialFilterMaster: any[][] = [];



  clickInterview() {
    this.toggleInterviews = !this.toggleInterviews;
  }
  clickActionPending() {
    this.toggleActionPending = !this.toggleActionPending;
  }
  clickCurrentLocation() {
    this.toggleCurrentLocation = !this.toggleCurrentLocation;
  }
  clickExperience() {
    this.toggleExperience = !this.toggleExperience;
  }
  clickIndustry() {
    this.toggleIndustry = !this.toggleIndustry;
  }
  clickCurrentCompany() {
    this.toggleCurrentCompany = !this.toggleCurrentCompany;
  }
  clickPastCompany() {
    this.togglePastCompany = !this.togglePastCompany;
  }
  clickOthers() {
    this.toggleOthers = !this.toggleOthers;
  }





  @Input()
  security: any;

  constructor(private commonService: CommonService,
    private rhsService: RhsService,
    private homeCardsCachedService: HomeCardsCachedService,
    private autoMatchSocialPagedService: AutoMatchSocialPagedService,
    private sessionService: SessionService,
    private jobCommonServiceService:JobCommonServiceService,
    private homeCardsService: HomeCardsService,
    private i18UtilService: I18UtilService) {
    
    this.rhsSocialDisabled = true;
    this.rhsSubscription = this.commonService.isrhsMonsterDisabled().subscribe((enabled) => {
        this.rhsSocialDisabled = enabled;
    });

    this.autoMatchSocialFilterMaster['industry'] = ['Accounting', 'Airlines/Aviation', 'Alternative Dispute Resolution', 'Alternative Medicine', 'Animation', 'Apparel & Fashion', 'Architecture & Planning', 'Arts and Crafts', 'Automotive', 'Aviation & Aerospace', 'Banking', 'Biotechnology', 'Broadcast Media', 'Building Materials', 'Business Supplies and Equipment', 'Capital Markets', 'Chemicals', 'Civic & Social Organization', 'Civil Engineering', 'Commercial Real Estate', 'Computer & Network Security', 'Computer Games', 'Computer Hardware', 'Computer Networking', 'Computer Software', 'Construction', 'Consumer Electronics', 'Consumer Goods', 'Consumer Services', 'Cosmetics', 'Dairy', 'Defense & Space', 'Design', 'Education Management', 'E-Learning', 'Electrical/Electronic Manufacturing', 'Entertainment', 'Environmental Services', 'Events Services', 'Executive Office', 'Facilities Services', 'Farming', 'Financial Services', 'Fine Art', 'Fishery', 'Food & Beverages', 'Food Production', 'Fund-Raising', 'Furniture', 'Gambling & Casinos', 'Glass, Ceramics & Concrete', 'Government Administration', 'Government Relations', 'Graphic Design', 'Health, Wellness and Fitness', 'Higher Education', 'Hospital & Health Care', 'Hospitality', 'Human Resources', 'Import and Export', 'Individual & Family Services', 'Industrial Automation', 'Information Services', 'Information Technology and Services', 'Insurance', 'International Affairs', 'International Trade and Development', 'Internet', 'Investment Banking', 'Investment Management', 'Judiciary', 'Law Enforcement', 'Law Practice', 'Legal Services', 'Legislative Office', 'Leisure, Travel & Tourism', 'Libraries', 'Logistics and Supply Chain', 'Luxury Goods & Jewelry', 'Machinery', 'Management Consulting', 'Maritime', 'Marketing and Advertising', 'Market Research', 'Mechanical or Industrial Engineering', 'Media Production', 'Medical Devices', 'Medical Practice', 'Mental Health Care', 'Military', 'Mining & Metals', 'Motion Pictures and Film', 'Museums and Institutions', 'Music', 'Nanotechnology', 'Newspapers', 'Nonprofit Organization Management', 'Oil & Energy', 'Online Media', 'Outsourcing/Offshoring', 'Package/Freight Delivery', 'Packaging and Containers', 'Paper & Forest Products', 'Performing Arts', 'Pharmaceuticals', 'Philanthropy', 'Photography', 'Plastics', 'Political Organization', 'Primary/Secondary Education', 'Printing', 'Professional Training & Coaching', 'Program Development', 'Public Policy', 'Public Relations and Communications', 'Public Safety', 'Publishing', 'Railroad Manufacture', 'Ranching', 'Real Estate', 'Recreational Facilities and Services', 'Religious Institutions', 'Renewables & Environment', 'Research', 'Restaurants', 'Retail', 'Security and Investigations', 'Semiconductors', 'Shipbuilding', 'Sporting Goods', 'Sports', 'Staffing and Recruiting', 'Supermarkets', 'Telecommunications', 'Textiles', 'Think Tanks', 'Tobacco', 'Translation and Localization', 'Transportation/Trucking/Railroad', 'Utilities', 'Venture Capital & Private Equity', 'Veterinary', 'Warehousing', 'Wholesale', 'Wine and Spirits', 'Wireless ', 'Writing and Editing'];
    this.autoMatchSocialFilterMaster['location'] = ["Achhnera", "Adalaj", "Adilabad", "Adityapur", "Adoni", "Adoor", "Adra", "Adyar", "Afzalpur", "Agartala", "Agra", "Ahmedabad", "Ahmednagar", "Aizawl", "Ajmer", "Akola", "Akot", "Alappuzha", "Aligarh", "Alipurduar", "Alirajpur", "Allahabad", "Alwar", "Amalapuram", "Amalner", "Ambejogai", "Ambikapur", "Amravati", "Amreli", "Amritsar", "Amroha", "Anakapalle", "Anand", "Anantapur", "Anantnag", "Anjangaon", "Anjar", "Ankleshwar", "Arakkonam", "Arambagh", "Araria", "Arrah", "Arsikere", "Aruppukkottai", "Arvi", "Arwal", "Asansol", "Asarganj", "Ashok Nagar", "Athni", "Attingal", "Aurangabad", "Azamgarh", "Bagaha", "Bageshwar", "Bahadurgarh", "Baharampur", "Bahraich", "Balaghat", "Balangir", "Baleshwar Town", "Ballari", "Balurghat", "Bankura", "Bapatla", "Baramula", "Barbil", "Bargarh", "Barh", "Baripada Town", "Barnala", "Barpeta", "Batala", "Bathinda", "Begusarai", "Belagavi", "Bellampalle", "Belonia", "Bengaluru (Bangalore)", "Bettiah", "Bhabua", "Bhadrachalam", "Bhadrak", "Bhagalpur", "Bhainsa", "Bharatpur", "Bharuch", "Bhatapara", "Bhavnagar", "Bhawanipatna", "Bheemunipatnam", "Bhilai Nagar", "Bhilwara", "Bhimavaram", "Bhiwandi", "Bhiwani", "Bhongir", "Bhopal", "Bhubaneswar", "Bhuj", "Bikaner", "Bilaspur", "Bobbili", "Bodhan", "Bokaro Steel City", "Bongaigaon City", "Brahmapur", "Buxar", "Byasanagar", "Chaibasa", "Chalakudy", "Chandausi", "Chandigarh", "Changanassery", "Charkhi Dadri", "Chatra", "Chennai", "Cherthala", "Chhapra", "Chikkamagaluru", "Chilakaluripet", "Chirala", "Chirkunda", "Chirmiri", "Chittoor", "Chittur-Thathamangalam", "Coimbatore", "Cuttack", "Dalli-Rajhara", "Darbhanga", "Darjiling", "Davanagere", "Deesa", "Dehradun", "Dehri-on-Sone", "Delhi", "Deoghar", "Dhamtari", "Dhanbad", "Dharmanagar", "Dharmavaram", "Dhenkanal", "Dhoraji", "Dhubri", "Dhule", "Dhuri", "Dibrugarh", "Dimapur", "Diphu", "Dumka", "Dumraon", "Durg", "Eluru", "English Bazar", "Erode", "Etawah", "Faridabad", "Faridkot", "Farooqnagar", "Fatehabad", "Fatehpur Sikri", "Fazilka", "Firozabad", "Firozpur", "Firozpur Cantt.", "Forbesganj", "Gadwal", "Gangarampur", "Ganjbasoda", "Gaya", "Ghaziabad", "Giridih", "Goalpara", "Gobichettipalayam", "Gobindgarh", "Godhra", "Gohana", "Gokak", "Gooty", "Gopalganj", "Greater Mumbai", "Gudivada", "Gudur", "Gumia", "Guntakal", "Guntur", "Gurdaspur", "Gurgaon", "Guruvayoor", "Guwahati", "Gwalior", "Habra", "Hajipur", "Haldwani-cum-Kathgodam", "Hansi", "Hapur", "Hardoi", "Hardwar", "Hazaribag", "Hindupur", "Hisar", "Hoshiarpur", "Hubli-Dharwad", "Hugli-Chinsurah", "Hyderabad", "Ichalkaranji", "Imphal", "Indore", "Itarsi", "Jabalpur", "Jagdalpur", "Jaggaiahpet", "Jagraon", "Jagtial", "Jaipur", "Jalandhar", "Jalandhar Cantt.", "Jalpaiguri", "Jamalpur", "Jammalamadugu", "Jammu", "Jamnagar", "Jamshedpur", "Jamui", "Jangaon", "Jatani", "Jehanabad", "Jhansi", "Jhargram", "Jharsuguda", "Jhumri Tilaiya", "Jind", "Jodhpur", "Jorhat", "Kadapa", "Kadi", "Kadiri", "Kagaznagar", "Kailasahar", "Kaithal", "Kakinada", "Kalimpong", "Kalpi", "Kalyan-Dombivali", "Kamareddy", "Kancheepuram", "Kandukur", "Kanhangad", "Kannur", "Kanpur", "Kapadvanj", "Kapurthala", "Karaikal", "Karimganj", "Karimnagar", "Karjat", "Karnal", "Karur", "Karwar", "Kasaragod", "Kashipur", "Kathua", "Katihar", "Kavali", "Kayamkulam", "Kendrapara", "Kendujhar", "Keshod", "Khair", "Khambhat", "Khammam", "Khanna", "Kharagpur", "Kharar", "Khowai", "Kishanganj", "Kochi", "Kodungallur", "Kohima", "Kolar", "Kolkata", "Kollam", "Koratla", "Korba", "Kot Kapura", "Kothagudem", "Kottayam", "Kovvur", "Koyilandy", "Kozhikode", "Kunnamkulam", "Kurnool", "Kyathampalle", "Lachhmangarh", "Ladnu", "Ladwa", "Lahar", "Laharpur", "Lakheri", "Lakhimpur", "Lakhisarai", "Lakshmeshwar", "Lal Gopalganj Nindaura", "Lalganj", "Lalgudi", "Lalitpur", "Lalsot", "Lanka", "Lar", "Lathi", "Latur", "Lilong", "Limbdi", "Lingsugur", "Loha", "Lohardaga", "Lonar", "Lonavla", "Longowal", "Loni", "Losal", "Lucknow", "Ludhiana", "Lumding", "Lunawada", "Lunglei", "Macherla", "Machilipatnam", "Madanapalle", "Maddur", "Madhepura", "Madhubani", "Madhugiri", "Madhupur", "Madikeri", "Madurai", "Magadi", "Mahad", "Mahalingapura", "Maharajganj", "Maharajpur", "Mahasamund", "Mahbubnagar", "Mahe", "Mahemdabad", "Mahendragarh", "Mahesana", "Mahidpur", "Mahnar Bazar", "Mahuva", "Maihar", "Mainaguri", "Makhdumpur", "Makrana", "Malaj Khand", "Malappuram", "Malavalli", "Malda", "Malegaon", "Malerkotla", "Malkangiri", "Malkapur", "Malout", "Malpura", "Malur", "Manachanallur", "Manasa", "Manavadar", "Manawar", "Mancherial", "Mandalgarh", "Mandamarri", "Mandapeta", "Mandawa", "Mandi", "Mandi Dabwali", "Mandideep", "Mandla", "Mandsaur", "Mandvi", "Mandya", "Manendragarh", "Maner", "Mangaldoi", "Mangaluru", "Mangalvedhe", "Manglaur", "Mangrol", "Mangrulpir", "Manihari", "Manjlegaon", "Mankachar", "Manmad", "Mansa", "Manuguru", "Manvi", "Manwath", "Mapusa", "Margao", "Margherita", "Marhaura", "Mariani", "Marigaon", "Markapur", "Marmagao", "Masaurhi", "Mathabhanga", "Mathura", "Mattannur", "Mauganj", "Mavelikkara", "Mavoor", "Mayang Imphal", "Medak", "Medininagar (Daltonganj)", "Medinipur", "Meerut", "Mehkar", "Memari", "Merta City", "Mhaswad", "Mhow Cantonment", "Mhowgaon", "Mihijam", "Mira-Bhayandar", "Mirganj", "Miryalaguda", "Modasa", "Modinagar", "Moga", "Mohali", "Mokameh", "Mokokchung", "Monoharpur", "Moradabad", "Morena", "Morinda", "Morshi", "Morvi", "Motihari", "Motipur", "Mount Abu", "Mudabidri", "Mudalagi", "Muddebihal", "Mudhol", "Mukerian", "Mukhed", "Muktsar", "Mul", "Mulbagal", "Multai", "Mumbai", "Mundargi", "Mundi", "Mungeli", "Munger", "Murliganj", "Murshidabad", "Murtijapur", "Murwara", "Musabani", "Mussoorie", "Muvattupuzha", "Muzaffarpur", "Mysore", "Nabadwip", "Nabarangapur", "Nabha", "Nadbai", "Nadiad", "Nagaon", "Nagapattinam", "Nagar", "Nagari", "Nagarkurnool", "Nagaur", "Nagda", "Nagercoil", "Nagina", "Nagla", "Nagpur", "Nahan", "Naharlagun", "Naidupet", "Naihati", "Naila Janjgir", "Nainital", "Nainpur", "Najibabad", "Nakodar", "Nakur", "Nalbari", "Namagiripettai", "Namakkal", "Nanded-Waghala", "Nandgaon", "Nandivaram-Guduvancheri", "Nandura", "Nandurbar", "Nandyal", "Nangal", "Nanjangud", "Nanjikottai", "Nanpara", "Narasapuram", "Narasaraopet", "Naraura", "Narayanpet", "Nargund", "Narkatiaganj", "Narkhed", "Narnaul", "Narsinghgarh", "Narsipatnam", "Narwana", "Nashik", "Nasirabad", "Natham", "Nathdwara", "Naugachhia", "Naugawan Sadat", "Nautanwa", "Navalgund", "Navi Mumbai", "Navsari", "Nawabganj", "Nawada", "Nawanshahr", "Nawapur", "Nedumangad", "Neem-Ka-Thana", "Neemuch", "Nehtaur", "Nelamangala", "Nellikuppam", "Nellore", "Nepanagar", "Neyveli (TS)", "Neyyattinkara", "Nidadavole", "Nilambur", "Nilanga", "Nimbahera", "Nirmal", "Niwai", "Niwari", "Nizamabad", "Nohar", "Noida", "Nokha", "Nongstoin", "Noorpur", "North Lakhimpur", "Nowgong", "Nowrozabad (Khodargama)", "Nuzvid", "O Valley", "Obra", "Oddanchatram", "Ongole", "Orai", "Osmanabad", "Ottappalam", "Ozar", "P.N.Patti", "Pachora", "Pachore", "Pacode", "Padmanabhapuram", "Padra", "Padrauna", "Paithan", "Pakaur", "Palacole", "Palai", "Palakkad", "Palampur", "Palani", "Palanpur", "Palasa Kasibugga", "Palghar", "Pali", "Palia Kalan", "Palitana", "Palladam", "Pallapatti", "Pallikonda", "Palwal", "Palwancha", "Panagar", "Panagudi", "Panaji", "Panamattom", "Panchkula", "Panchla", "Pandharkaoda", "Pandharpur", "Pandhurna", "Pandua", "Panipat", "Panna", "Panniyannur", "Panruti", "Panvel", "Pappinisseri", "Paradip", "Paramakudi", "Parangipettai", "Parasi", "Paravoor", "Parbhani", "Pardi", "Parlakhemundi", "Parli", "Partur", "Parvathipuram", "Pasan", "Paschim Punropara", "Pasighat", "Patan", "Pathanamthitta", "Pathankot", "Pathardi", "Pathri", "Patiala", "Patna", "Patratu", "Pattamundai", "Patti", "Pattran", "Pattukkottai", "Patur", "Pauni", "Pauri", "Pavagada", "Pedana", "Peddapuram", "Pehowa", "Pen", "Perambalur", "Peravurani", "Peringathur", "Perinthalmanna", "Periyakulam", "Periyasemur", "Pernampattu", "Perumbavoor", "Petlad", "Phagwara", "Phalodi", "Phaltan", "Phillaur", "Phulabani", "Phulera", "Phulpur", "Phusro", "Pihani", "Pilani", "Pilibanga", "Pilibhit", "Pilkhuwa", "Pindwara", "Pinjore", "Pipar City", "Pipariya", "Piriyapatna", "Piro", "Pithampur", "Pithapuram", "Pithoragarh", "Pollachi", "Polur", "Pondicherry", "Ponnani", "Ponneri", "Ponnur", "Porbandar", "Porsa", "Port Blair", "Powayan", "Prantij", "Pratapgarh", "Prithvipur", "Proddatur", "Pudukkottai", "Pudupattinam", "Pukhrayan", "Pulgaon", "Puliyankudi", "Punalur", "Punch", "Pune", "Punganur", "Punjaipugalur", "Puranpur", "Puri", "Purna", "Purnia", "Purquazi", "Purulia", "Purwa", "Pusad", "Puthuppally", "Puttur", "Qadian", "Raayachuru", "Rabkavi Banhatti", "Radhanpur", "Rae Bareli", "Rafiganj", "Raghogarh-Vijaypur", "Raghunathganj", "Raghunathpur", "Rahatgarh", "Rahuri", "Raiganj", "Raigarh", "Raikot", "Raipur", "Rairangpur", "Raisen", "Raisinghnagar", "Rajagangapur", "Rajahmundry", "Rajakhera", "Rajaldesar", "Rajam", "Rajampet", "Rajapalayam", "Rajauri", "Rajgarh", "Rajgarh (Alwar)", "Rajgarh (Churu)", "Rajgir", "Rajkot", "Rajnandgaon", "Rajpipla", "Rajpura", "Rajsamand", "Rajula", "Rajura", "Ramachandrapuram", "Ramagundam", "Ramanagaram", "Ramanathapuram", "Ramdurg", "Rameshwaram", "Ramganj Mandi", "Ramgarh", "Ramnagar", "Ramngarh", "Rampur", "Rampur Maniharan", "Rampura Phul", "Rampurhat", "Ramtek", "Ranaghat", "Ranavav", "Ranchi", "Ranebennuru", "Rangia", "Rania", "Ranibennur", "Ranipet", "Rapar", "Rasipuram", "Rasra", "Ratangarh", "Rath", "Ratia", "Ratlam", "Ratnagiri", "Rau", "Raurkela", "Raver", "Rawatbhata", "Rawatsar", "Raxaul Bazar", "Rayachoti", "Rayadurg", "Rayagada", "Reengus", "Rehli", "Renigunta", "Renukoot", "Reoti", "Repalle", "Revelganj", "Rewa", "Rewari", "Rishikesh", "Risod", "Robertsganj", "Robertson Pet", "Rohtak", "Ron", "Roorkee", "Rosera", "Rudauli", "Rudrapur", "Rupnagar", "Sabalgarh", "Sadabad", "Sadalagi", "Sadasivpet", "Sadri", "Sadulpur", "Sadulshahar", "Safidon", "Safipur", "Sagar", "Sagara", "Sagwara", "Saharanpur", "Saharsa", "Sahaspur", "Sahaswan", "Sahawar", "Sahibganj", "Sahjanwa", "Saidpur", "Saiha", "Sailu", "Sainthia", "Sakaleshapura", "Sakti", "Salaya", "Salem", "Salur", "Samalkha", "Samalkot", "Samana", "Samastipur", "Sambalpur", "Sambhal", "Sambhar", "Samdhan", "Samthar", "Sanand", "Sanawad", "Sanchore", "Sandi", "Sandila", "Sanduru", "Sangamner", "Sangareddy", "Sangaria", "Sangli", "Sangole", "Sangrur", "Sankarankoil", "Sankari", "Sankeshwara", "Santipur", "Sarangpur", "Sardarshahar", "Sardhana", "Sarni", "Sarsod", "Sasaram", "Sasvad", "Satana", "Satara", "Sathyamangalam", "Satna", "Sattenapalle", "Sattur", "Saunda", "Saundatti-Yellamma", "Sausar", "Savanur", "Savarkundla", "Savner", "Sawai Madhopur", "Sawantwadi", "Sedam", "Sehore", "Sendhwa", "Seohara", "Seoni", "Seoni-Malwa", "Shahabad", "Shahade", "Shahbad", "Shahdol", "Shahganj", "Shahjahanpur", "Shahpur", "Shahpura", "Shajapur", "Shamgarh", "Shamli", "Shamsabad", "Shegaon", "Sheikhpura", "Shendurjana", "Shenkottai", "Sheoganj", "Sheohar", "Sheopur", "Sherghati", "Sherkot", "Shiggaon", "Shikaripur", "Shikarpur", "Shikohabad", "Shillong", "Shimla", "Shirdi", "Shirpur-Warwade", "Shirur", "Shishgarh", "Shivamogga", "Shivpuri", "Sholavandan", "Sholingur", "Shoranur", "Shrigonda", "Shrirampur", "Shrirangapattana", "Shujalpur", "Siana", "Sibsagar", "Siddipet", "Sidhi", "Sidhpur", "Sidlaghatta", "Sihor", "Sihora", "Sikanderpur", "Sikandra Rao", "Sikandrabad", "Sikar", "Silao", "Silapathar", "Silchar", "Siliguri", "Sillod", "Silvassa", "Simdega", "Sindagi", "Sindhagi", "Sindhnur", "Singrauli", "Sinnar", "Sira", "Sircilla", "Sirhind Fatehgarh Sahib", "Sirkali", "Sirohi", "Sironj", "Sirsa", "Sirsaganj", "Sirsi", "Siruguppa", "Sitamarhi", "Sitapur", "Sitarganj", "Sivaganga", "Sivagiri", "Sivakasi", "Siwan", "Sohagpur", "Sohna", "Sojat", "Solan", "Solapur", "Sonamukhi", "Sonepur", "Songadh", "Sonipat", "Sopore", "Soro", "Soron", "Soyagaon", "Sri Madhopur", "Srikakulam", "Srikalahasti", "Srinagar", "Srinivaspur", "Srirampore", "Srisailam Project (Right Flank Colony) Township", "Srivilliputhur", "Suar", "Sugauli", "Sujangarh", "Sujanpur", "Sullurpeta", "Sultanganj", "Sultanpur", "Sumerpur", "Sunabeda", "Sunam", "Sundargarh", "Sundarnagar", "Supaul", "Surandai", "Surapura", "Surat", "Suratgarh", "Suri", "Suriyampalayam", "Suryapet", "Tadepalligudem", "Tadpatri", "Takhatgarh", "Taki", "Talaja", "Talcher", "Talegaon Dabhade", "Talikota", "Taliparamba", "Talode", "Talwara", "Tamluk", "Tanda", "Tandur", "Tanuku", "Tarakeswar", "Tarana", "Taranagar", "Taraori", "Tarbha", "Tarikere", "Tarn Taran", "Tasgaon", "Tehri", "Tekkalakote", "Tenali", "Tenkasi", "Tenu dam-cum-Kathhara", "Terdal", "Tezpur", "Thakurdwara", "Thammampatti", "Thana Bhawan", "Thane", "Thanesar", "Thangadh", "Thanjavur", "Tharad", "Tharamangalam", "Tharangambadi", "Theni Allinagaram", "Thirumangalam", "Thirupuvanam", "Thiruthuraipoondi", "Thiruvalla", "Thiruvallur", "Thiruvananthapuram", "Thiruvarur", "Thodupuzha", "Thoubal", "Thrissur", "Thuraiyur", "Tikamgarh", "Tilda Newra", "Tilhar", "Tindivanam", "Tinsukia", "Tiptur", "Tirora", "Tiruchendur", "Tiruchengode", "Tiruchirappalli", "Tirukalukundram", "Tirukkoyilur", "Tirunelveli", "Tirupathur", "Tirupati", "Tiruppur", "Tirur", "Tiruttani", "Tiruvannamalai", "Tiruvethipuram", "Tiruvuru", "Tirwaganj", "Titlagarh", "Tittakudi", "Todabhim", "Todaraisingh", "Tohana", "Tonk", "Tuensang", "Tuljapur", "Tulsipur", "Tumkur", "Tumsar", "Tundla", "Tuni", "Tura", "Uchgaon", "Udaipur", "Udaipurwati", "Udgir", "Udhagamandalam", "Udhampur", "Udumalaipettai", "Udupi", "Ujhani", "Ujjain", "Umarga", "Umaria", "Umarkhed", "Umbergaon", "Umred", "Umreth", "Una", "Unjha", "Unnamalaikadai", "Unnao", "Upleta", "Uran", "Uran Islampur", "Uravakonda", "Urmar Tanda", "Usilampatti", "Uthamapalayam", "Uthiramerur", "Utraula", "Vadakkuvalliyur", "Vadalur", "Vadgaon Kasba", "Vadipatti", "Vadnagar", "Vadodara", "Vaijapur", "Vaikom", "Valparai", "Valsad", "Vandavasi", "Vaniyambadi", "Vapi", "Varanasi", "Varkala", "Vasai-Virar", "Vatakara", "Vedaranyam", "Vellakoil", "Vellore", "Venkatagiri", "Veraval", "Vidisha", "Vijainagar", "Vijapur", "Vijayapura", "Vijayawada", "Vijaypur", "Vikarabad", "Vikramasingapuram", "Viluppuram", "Vinukonda", "Viramgam", "Virudhachalam", "Virudhunagar", "Visakhapatnam", "Visnagar", "Viswanatham", "Vita", "Vizianagaram", "Vrindavan", "Vyara", "Wadgaon Road", "Wadhwan", "Wadi", "Wai", "Wanaparthy", "Wani", "Wankaner", "Wara Seoni", "Warangal", "Wardha", "Warhapur", "Warisaliganj", "Warora", "Warud", "Washim", "Wokha", "Yadgir", "Yamunanagar", "Yanam", "Yavatmal", "Yawal", "Yellandu", "Yemmiganur", "Yerraguntla", "Yevla", "Zaidpur", "Zamania", "Zira", "Zirakpur", "Zunheboto"]

    this.commonService.getSideBar().subscribe((sideBar) => {
      this.whichSideBar = sideBar;
      this.Interviews = [];
      this.messageCode = {};
    });
    this.commonService.onclick().subscribe((event) => {
      if (event) {
        if (event.target.className.indexOf("ignoreClick") < 0) {
          this.popUpActiveSection = "";
          this.suggestion = "";
        }
      }
    });
    this.interviewLoader = 'loader';
    this.currentDate = new Date();
    this.mintDate = new Date();
    this.maxDate = new Date();
    this.mintDate.setFullYear(2000);
    this.maxDate.setFullYear(2050);

  }

  ngOnInit() {

    const requestJSON = {
      selectedDate: this.selectedDate
    };

    this.jobCommonServiceService.isJobDisabled().subscribe( (res) => {
      this.interviewLoader = 'loader';
      this.homeCardsCachedService.clearCache();
      this.loadActionsPending();
      this.processInterviewsList(requestJSON);
    } );

    this.processInterviewsList(requestJSON);
    this.loadActionsPending();
    this.autoMatchSocialFilters['experience'] = [];
    this.autoMatchSocialFilters['location'] = [];
    this.autoMatchSocialFilters['industry'] = [];
    this.autoMatchSocialFilters['current_company'] = [];
    this.autoMatchSocialFilters['past_company'] = [];
    this.autoMatchSocialFilters['institute'] = [];

    for (let i = 0; i < 4; i++) {
      this.autoMatchSocialFilters['industry'].push({ key: this.autoMatchSocialFilterMaster['industry'][i] })
    }
    this.autoMatchSocialPagedService.getClearFilters().subscribe((clear) => {
      this.autoMatchSocialFilters['experience'] = [];
      this.autoMatchSocialFilters['location'] = [];
      this.autoMatchSocialFilters['industry'] = [];
      this.autoMatchSocialFilters['current_company'] = [];
      this.autoMatchSocialFilters['past_company'] = [];
      this.autoMatchSocialFilters['institute'] = [];

      for (let i = 0; i < 4; i++) {
        this.autoMatchSocialFilters['industry'].push({ key: this.autoMatchSocialFilterMaster['industry'][i] })
      }
    });
    this.autoMatchSocialPagedService.getFilterOptions().subscribe((filterOptions) => {
      this.filterLoading = false;
      if (filterOptions) {
        if (filterOptions.institute) {
          if (filterOptions.institute.buckets) {
            this.autoMatchSocialFilters['institute'] = filterOptions.institute.buckets;
          }
        }

        if (filterOptions.location) {
          if (filterOptions.location.buckets) {

            let locationSelected = false;
            for (let location of this.autoMatchSocialFilters['location']) {
              if (this.existsInFilter('location', location)) {
                locationSelected = true;
                break;
              }
            }
            if (!locationSelected && this.autoMatchSocialFilters['location'].length == 0) {
              this.autoMatchSocialFilters['location'] = filterOptions.location.buckets;
            }
          }
        }

        if (filterOptions.skill) {
          if (filterOptions.skill.buckets) {
            this.autoMatchSocialFilters['skills'] = filterOptions.skill.buckets;
          }
        }

        if (filterOptions.top_companies) {
          if (filterOptions.top_companies.buckets) {


            let includeJSON = this.autoMatchSocialPagedService.getLastRequestJSON().Filter.include;

            if (includeJSON.current_company) {

            } else {
              this.autoMatchSocialFilters['current_company'] = filterOptions.top_companies.buckets;
            }

            if (includeJSON.past_company) {

            } else {
              this.autoMatchSocialFilters['past_company'] = filterOptions.top_companies.buckets;
            }

          }
        }
      

      if (filterOptions.work_exp) {
        if (filterOptions.work_exp.buckets) {
          let noneChecked = true;
          // let workFilterOptions = this.autoMatchSocialFilters['experience'];
          // for (let work of ['0-365', '365-1095', '1095-2190', '2190-3650', '3650+']) {
          //   if (this.existsInExperienceFilter('experience', work)) {
          //     noneChecked = false;
          //     break;
          //   }
          // }
          if (noneChecked) {
            this.autoMatchSocialFilters['experience'] = filterOptions.work_exp.buckets;
          }

        }
      }
    }
    });
}

processInterviewsList(requestJSON){
  this.subscription = this.rhsService.getInterviews(requestJSON).subscribe(out => {
    this.messageCode = out.messageCode;
    this.Jobs = [];
    var jobCodes = Object.keys(out.responseMap);
    for (var i = 0; i < jobCodes.length; i++) {
      this.Jobs.push({
        "jobCode": out.responseMap[jobCodes[i]][0]["jobCode"],
        "jobTitle": out.responseMap[jobCodes[i]][0]["jobTitle"],
        "jobLocation": out.responseMap[jobCodes[i]][0]["jobLocation"],
        "interviews": out.responseMap[jobCodes[i]]
      });
    }
    if (this.Jobs.length == 0) {
      this.interviewLoader = 'nodata';
    } else {
      this.interviewLoader = 'data';
    }
  });
}

loadMasterSuggestions(filterType: string, queryString: string) {
  this.suggestionResult = [];
  this.suggestion = queryString;
  this.suggestionLoading = true;
  this.suggestionLoaded = false;
  let resultList = [];
  for (let option of this.autoMatchSocialFilterMaster[filterType]) {
    if (queryString.trim() != '' && option.toLowerCase().indexOf(queryString.toLowerCase()) >= 0 && !this.existsInFilter(filterType, option) && !this.existsInOptions(filterType, option)) {
      resultList.push(option);
    }
  }
  this.suggestionLoading = false;
  this.suggestionLoaded = true;
  this.suggestionResult = resultList;
}

loadSuggestions(filterType: string, queryString: string) {
  this.suggestionResult = [];
  this.suggestionLoading = true;
  this.suggestionLoaded = false;
  this.suggestion = queryString;

  if (this.suggestionSubscription != null) {
    this.suggestionSubscription.unsubscribe();
  }
  this.suggestionSubscription = this.autoMatchSocialPagedService.getFilterSuggestion(filterType.indexOf('company') > 0 ? 'company' : filterType, queryString).subscribe((response) => {
    let resultList = [];

    if (response) {
      if (response.res) {
        if (response.res.hits) {
          if (response.res.hits.hits) {
            if (response.res.hits.hits.length > 0) {
              for (let result of response.res.hits.hits) {
                if (result._source) {
                  if (result._source.name && !this.existsInOptions(filterType, result._source.name)) {
                    resultList.push(result._source.name);
                  }
                }
              }
            }
          }
        }
      }
    }

    this.suggestionResult = resultList;
    this.suggestionLoading = false;
    this.suggestionLoaded = true;
  });
}

loadActionsPending() {
  this.homeCardsCachedService.getData().subscribe((response) => {
  
    this.homeCardsResponse = response.responseData;
    this.targetDateCards = response.responseData.targetDate;
    this.assignedCards = response.responseData.assigned;
    this.prepareSalaryFitmentCards = response.responseData.prepareSalaryFitment;
    this.shortlistingRejectedCards = response.responseData.shortlistingRejected;
    this.offerPendingCards = response.responseData.offerPending;
    this.actionsPending = [];
    this.tasksCount = 0;

    if (this.targetDateCards != null) {
      for (let card of this.targetDateCards) {
        card.cardType = 'assignedCardType';
        this.i18UtilService.get('home.response.Job target date today').subscribe((res: string) => {
          card.label = res;
         });
       
        card.flipFront = 'active';
        card.flipBack = 'backinactive';
        card.active = true;
        this.actionsPending.push(card);
      }
    }
    if (this.assignedCards != null) {
      for (let card of this.assignedCards) {
        card.cardType = 'assignedCardType';
        this.i18UtilService.get('home.response.New job assigned').subscribe((res: string) => {
          card.label = res;
         });
        card.flipFront = 'active';
        card.flipBack = 'backinactive';
        card.active = true;
        this.actionsPending.push(card);
      }
    }
    if (this.prepareSalaryFitmentCards != null) {
      for (let card of this.prepareSalaryFitmentCards) {
        card.cardType = 'stageStatusCardType';
        this.i18UtilService.get('home.response.Prepare Salary Fitment').subscribe((res: string) => {
          card.label = res;
         });
        card.flipFront = 'active';
        card.flipBack = 'backinactive';
        card.stage = 'SALARY_FITMENT';
        card.status = 'PENDING';
        this.actionsPending.push(card);
      }
    }

    if (this.offerPendingCards != null) {
      for (let card of this.offerPendingCards) {
        card.cardType = 'stageStatusCardType';
        this.i18UtilService.get('home.response.Offer Pending').subscribe((res: string) => {
          card.label = res;
         });
        card.flipFront = 'active';
        card.flipBack = 'backinactive';
        card.stage = 'OFFER';
        card.status = 'OFFERPENDING';
        card.active = true;
        this.actionsPending.push(card);
      }
    }

    if (this.shortlistingRejectedCards != null) {
      for (let card of this.shortlistingRejectedCards) {
        card.cardType = 'stageStatusCardType';
        this.i18UtilService.get('home.response.Shortlist Candidate Rejected').subscribe((res: string) => {
          card.label = res;
         });
        card.flipFront = 'active';
        card.flipBack = 'backinactive';
        card.stage = 'SHORTLISTING';
        card.status = 'REJECTED';
        card.active = true;
        this.actionsPending.push(card);
      }
    }
    this.dataInitialized = true;
    this.tasksCount = this.actionsPending.length;


    if (this.tasksCount == 0) {
      this.actionsPendingloaderIndicator = "nodata";
    } else {
      this.actionsPendingloaderIndicator = "data";
    }

  });
}

loadApplicants(pendingAction: CardSructure) {
  this.isVisible = !this.isVisible;
  if (pendingAction.applicantLoading) {
    return;
  }

  if (pendingAction.assignedCardTypeResponse == null && pendingAction.stageStatusCardTypeResponse == null) {

    pendingAction.applicantLoading = true;

    this.homeCardsService.getHomeCardsFlipDetails(pendingAction.cardType, pendingAction.requisitionCode, pendingAction.stage, pendingAction.status, pendingAction.active).subscribe(out => {
      if (pendingAction.cardType === 'assignedCardType') {
        pendingAction.assignedCardTypeResponse = out.responseData.assignedCardType;
        this.countPendingAction = out.responseData.assignedCardType.length;
      } else if (pendingAction.cardType === 'stageStatusCardType') {
        pendingAction.stageStatusCardTypeResponse = out.responseData.stageStatusCardType;
        this.countPendingAction = out.responseData.stageStatusCardType.length;
      }
      pendingAction.applicantLoading = false;
      pendingAction.rhsExpanded = !pendingAction.rhsExpanded;
    });

  } else {
    pendingAction.rhsExpanded = !pendingAction.rhsExpanded;
  }
}

existsInExperienceFilter(filterType: string, filterValue: string) {
  
  if( this.rhsSocialDisabled ){
    return;
  }

  let start = 0;

  if (filterValue.indexOf('-') > 0) {
    start = parseInt(filterValue.split("-")[0]);
  }
  if (filterValue.indexOf('+') > 0) {
    start = parseInt(filterValue.split("+")[0]);
  }

  let lastRequestJSON = this.autoMatchSocialPagedService.getLastRequestJSON();

  if (lastRequestJSON) {
    let includeJSON = lastRequestJSON.Filter.include;

    if (includeJSON[filterType]) {

      if (includeJSON[filterType].length > 0) {
        for (let filterJSON of includeJSON[filterType]) {
          if (filterJSON['start'] == start) {
            return true;
          }
        }
      }
    }
  }

  return false;
}
existsInFilter(filterType: string, filterValue) {

  if( this.rhsSocialDisabled ){
    return;
  }

  let lastRequestJSON = this.autoMatchSocialPagedService.getLastRequestJSON();
  if (lastRequestJSON) {
    let includeJSON = lastRequestJSON.Filter.include;

    if (includeJSON[filterType]) {
      if (filterType == 'contact') {
        return true;
      }
      if (includeJSON[filterType].length > 0) { }
      for (let filterJSON of includeJSON[filterType]) {
        if (filterJSON['name'] == filterValue) {
          return true;
        }
      }
    }
  }

  return false;
}

existsInOptions(filterType: string, filterValue) {
  let filterOptions = this.autoMatchSocialFilters[filterType];

  if (filterOptions) {
    for (let filter of filterOptions) {
      if (filter.key.trim().toLowerCase() == filterValue.trim().toLowerCase()) {
        return true;
      }
    }
  }
  return false;
}

autoMatchSocialToggleFilter(event, filterType: string) {

  if( this.rhsSocialDisabled ){
    return;
  }

  if (event.target.checked) {
    this.autoMatchSocialPagedService.getFilterApplied().next({
      'action': 'add',
      'type': filterType,
      'event': event
    });
  } else {
    this.autoMatchSocialPagedService.getFilterApplied().next({
      'action': 'remove',
      'type': filterType,
      'event': event
    });
  }
}

sortByMonster(event)
{
  
}

addMore(filterType) {

  if( this.rhsSocialDisabled ){
    return;
  }

  this.popUpActiveSection = filterType;
  this.suggestion = "";
  this.suggestionResult = [];

  setTimeout(() => {
    this.locationInput.nativeElement.value = '';
    this.industryInput.nativeElement.value = '';
    this.currentCompanyInput.nativeElement.value = '';
    this.pastCompanyInput.nativeElement.value = '';

    this.locationInput.nativeElement.focus();
    this.industryInput.nativeElement.focus();
    this.currentCompanyInput.nativeElement.focus();
    this.pastCompanyInput.nativeElement.focus();
  }, 100);

}

ngOnDestroy(): void {
  if(this.subscription != null) {
    this.subscription.unsubscribe();
  }
  if(this.rhsSubscription != null) {
    this.rhsSubscription.unsubscribe();
  }
}

  private updateRHSForDate(event) {
  this.interviewLoader = 'loader';
  this.selectedDate = event;
  this.currentDate = event;
  const requestJSON = {
    selectedDate: this.selectedDate
  };
  this.subscription = this.rhsService.getInterviews(requestJSON).subscribe(out => {
    this.messageCode = out.messageCode;
    this.Jobs = [];
    var jobCodes = Object.keys(out.responseMap);
    for (var i = 0; i < jobCodes.length; i++) {
      this.Jobs.push({
        "jobCode": out.responseMap[jobCodes[i]][0]["jobCode"],
        "jobTitle": out.responseMap[jobCodes[i]][0]["jobTitle"],
        "jobLocation": out.responseMap[jobCodes[i]][0]["jobLocation"],
        "interviews": out.responseMap[jobCodes[i]]
      });
    }
    if (this.Jobs.length == 0) {
      this.interviewLoader = 'nodata';
    } else {
      this.interviewLoader = 'data';
    }
  });
}
  private getInterviewIcon(interviewType: string): string {
  if (interviewType === 'FaceToFace') {
    return 'people';
  } else if (interviewType === 'Video') {
    return 'videocam';
  } else if (interviewType === 'Telephonic') {
    return 'phone';
  } else {
    return null;
  }
}
  private getTime(timeInMillis: string): string {
  const passedDate: Date = new Date(timeInMillis);
  const time: string = (passedDate.getDate() < 10 ? '0' : '') + passedDate.getDate() + ' ' + this.getMonthString(passedDate.getMonth()) + ' ' +
    (passedDate.getHours() < 10 ? '0' : '') + passedDate.getHours() + ':'
    + (passedDate.getMinutes() < 10 ? '0' : '') + passedDate.getMinutes();

  return time;
}

addFilter(filterType: string, filterValue: any) {

  if( this.rhsSocialDisabled ){
    return;
  }

  this.popUpActiveSection = "";
  this.suggestion = "";
  this.autoMatchSocialFilters[filterType].push({ key: filterValue, doc_count: '' });
  this.autoMatchSocialPagedService.getFilterApplied().next({
    'action': 'add',
    'type': filterType,
    'event': filterValue
  });
}

  private getLocation(location: string): string {
  if (location == null)
    return 'NA';
  else
    return location;
}
  private getMonthString(month: number): string {
  if (month == 0)
    return 'Jan |';
  else if (month == 1)
    return 'Feb |';
  else if (month == 2)
    return 'Mar |';
  else if (month == 3)
    return 'Apr |';
  else if (month == 4)
    return 'May |';
  else if (month == 5)
    return 'Jun |';
  else if (month == 6)
    return 'Jul |';
  else if (month == 7)
    return 'Aug |';
  else if (month == 8)
    return 'Sep |';
  else if (month == 9)
    return 'Oct |';
  else if (month == 10)
    return 'Nov |';
  else if (month == 11)
    return 'Dec |';
  else
    return null;
}

enableStatic() {
  return this.sessionService.removeStatic;
}
}