import { TranslateService } from '@ngx-translate/core';
import { HomeCardsCachedService } from './home-cards-cached.service';
import { Component, OnInit, trigger, state, style, transition, animate, Output, EventEmitter } from '@angular/core';
import { HomeCardsService } from './home-cards.service';
import { HomeCardsResponse, CardSructure, AssignedCardTypeResponse } from './home-cards-request-response';
import { SessionService } from '../../session.service';
import { NameinitialService } from '../../shared/nameinitial.service'
import { Router } from '@angular/router';

@Component({
  selector: 'alt-home-cards',
  templateUrl: './home-cards.component.html',
  styleUrls: ['./home-cards.component.css'],
  providers: [HomeCardsService],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(0deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(-180deg)'
      })),
      state('backactive', style({
        transform: 'rotateY(0deg)'
      })),
      state('backinactive', style({
        transform: 'rotateY(180deg)'
      })),
      transition('active => inactive', animate('400ms ease-out')),
      transition('inactive => active', animate('400ms ease-in')),
      transition('backactive => backinactive', animate('400ms ease-out')),
      transition('backinactive => backactive', animate('400ms ease-in'))
    ])
  ]
})
export class HomeCardsComponent implements OnInit {

  @Output() count: EventEmitter<any> = new EventEmitter();

  homeCardsResponse: HomeCardsResponse;
  targetDateCards: CardSructure[];
  assignedCards: CardSructure[];
  prepareSalaryFitmentCards: CardSructure[];
  shortlistingRejectedCards: CardSructure[];
  offerPendingCards: CardSructure[];
  newCandidateCards: CardSructure[];
  newJobApprovalCards: CardSructure[];
  rejectedJobsCards: CardSructure[];
  totalCards: CardSructure[];
  tasksCount: Number;
  loaderIndicator: string;
  randomcolor: string[] = [];
  publishedToTotal: number;
  constructor(private homeCardsService: HomeCardsService,
    private sessionService: SessionService, private nameinitialService: NameinitialService,
    private translate: TranslateService,
    private homeCardsCachedService: HomeCardsCachedService,
    private router: Router) {
    translate.setDefaultLang('en' + this.sessionService.langVer);
    this.loaderIndicator = "loader";
    this.tasksCount = 0;
    this.randomcolor = nameinitialService.randomcolor;
  }

  initializeData() {
    Promise.resolve(this.sessionService.readCookie()).then((data) => {
      this.getHomeCardsData();
    })
  }

  getHomeCardsData() {
    this.homeCardsService.getPublishToTotalCount().subscribe(out => {
      this.publishedToTotal = out.response
    });
    this.homeCardsCachedService.clearCache();
    this.homeCardsCachedService.getData().subscribe(response => {
      if (!this.totalCards) {
        this.homeCardsResponse = response.responseData;
        this.targetDateCards = response.responseData.targetDate;
        this.assignedCards = response.responseData.assigned;
        this.prepareSalaryFitmentCards = response.responseData.prepareSalaryFitment;
        this.shortlistingRejectedCards = response.responseData.shortlistingRejected;
        this.offerPendingCards = response.responseData.offerPending;
        this.newCandidateCards = response.responseData.newCandidateCard;
        this.newJobApprovalCards = response.responseData.newJobApprovalCard;
        this.rejectedJobsCards = response.responseData.rejectedJobsCards;
        this.totalCards = [];
        this.tasksCount = 0;
        if (this.targetDateCards != null) {
          for (let card of this.targetDateCards) {
            card.cardType = 'assignedCardType';
            card.label = 'Job target date today';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.active = true;
            this.totalCards.push(card);

          }
        }
        if (this.assignedCards != null) {
          for (let card of this.assignedCards) {
            card.cardType = 'assignedCardType';
            card.label = 'New job assigned';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.active = true;
            this.totalCards.push(card);
          }
        }
        if (this.prepareSalaryFitmentCards != null) {
          for (let card of this.prepareSalaryFitmentCards) {
            card.cardType = 'stageStatusCardType';
            card.label = 'Prepare Salary Fitment';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.stage = 'SALARY_FITMENT';
            card.status = 'PENDING';
            card.active = true;
            this.totalCards.push(card);
          }
        }

        if (this.offerPendingCards != null) {
          for (let card of this.offerPendingCards) {
            card.cardType = 'stageStatusCardType';
            card.label = 'Offer Pending';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.stage = 'OFFER';
            card.status = 'OFFERPENDING';
            card.active = true;
            this.totalCards.push(card);
          }
        }

        if (this.shortlistingRejectedCards != null) {
          for (let card of this.shortlistingRejectedCards) {
            card.cardType = 'stageStatusCardType';
            card.label = 'Shortlist Candidate Rejected';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.stage = 'SHORTLISTING';
            card.status = 'REJECTED';
            card.active = true;
            this.totalCards.push(card);
          }
        }

        if (this.newCandidateCards != null) {
          for (let card of this.newCandidateCards) {
            card.cardType = 'newCandidateCard';
            card.label = 'New Candidate Received';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.active = true;            
            this.totalCards.push(card);
          }
        }
        if (this.rejectedJobsCards != null) {
          for (let card of this.rejectedJobsCards) {
            card.cardType = 'assignedCardType';
            card.label = 'Job Rejected';
            card.flipFront = 'active';
            card.flipBack = 'backinactive'; 
            card.active = false;           
            this.totalCards.push(card);
          }
        }
        if (this.newJobApprovalCards != null) {
          for (let card of this.newJobApprovalCards) {
            card.cardType = 'assignedCardType';
            card.label = 'Job Approval Pending';
            card.flipFront = 'active';
            card.flipBack = 'backinactive';
            card.active = false;            
            this.totalCards.push(card);
          }
        }
        this.sortAndAssignCardDetails(this.totalCards);

        this.tasksCount = this.totalCards.length;
        this.count.emit(this.tasksCount);
      }
      if (this.tasksCount == 0) {
        this.loaderIndicator = "nodata";
      } else {
        this.loaderIndicator = "data";
      }
    });
  }

  ngOnInit() {
    
    this.translate.use(this.sessionService.langString);
    this.sessionService.getLang().subscribe((lang) => {
      this.translate.use(lang);
    });
    
    this.initializeData();
  }

  toggleFlip(response: CardSructure) {
    if (response.assignedCardTypeResponse == null && response.stageStatusCardTypeResponse == null) {
      this.homeCardsService.getHomeCardsFlipDetails(response.cardType, response.requisitionCode, response.stage, response.status ,response.active).subscribe(out => {
        if (response.cardType === 'assignedCardType') {
          response.assignedCardTypeResponse = out.responseData.assignedCardType;
        } else if (response.cardType === 'stageStatusCardType') {
          response.stageStatusCardTypeResponse = out.responseData.stageStatusCardType;
          this.sortAndAssignCardDetails(response.stageStatusCardTypeResponse);
        }
        else if (response.cardType === 'newCandidateCard') {
          response.newCandidateCardTypeResponse = out.responseData.newCandidateCard;
          this.sortAndAssignCardDetails(response.newCandidateCardTypeResponse);
        }        
        response.flipFront = (response.flipFront == 'inactive') ? 'active' : 'inactive';
        response.flipBack = (response.flipBack == 'backinactive') ? 'backactive' : 'backinactive';
      });
    } else {
      response.flipFront = (response.flipFront == 'inactive') ? 'active' : 'inactive';
      response.flipBack = (response.flipBack == 'backinactive') ? 'backactive' : 'backinactive';
    }

  }


  sortAndAssignCardDetails(array: any[]) {
    array.sort(function (a, b) {
      if (a['modifiedDate'] < b['modifiedDate']) {
        return 1;
      }
      else if (a['modifiedDate'] > b['modifiedDate']) {
        return -1;
      }
      else {
        return 0;
      }
    });
  }


  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }
  routePage(requisitionCode: String)
  {
    this.router.navigateByUrl('job/edit-job/' + requisitionCode + '/tagged-candidates');
  }
}
