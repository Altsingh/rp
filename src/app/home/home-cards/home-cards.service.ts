import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../session.service';
import { Observable } from 'rxjs/Rx';
import { HomeCardsRequestResponse, HomeCardsRequest, HomeCardsResponse, GetHomeCardsFlipDetailsRequestResponse, GetHomeCardsFlipDetailsRequest } from './home-cards-request-response';

@Injectable()
export class HomeCardsService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getHomeCardsData() {
    let homeCardsRequestResponse: HomeCardsRequestResponse = new HomeCardsRequestResponse();
    let homeCardsRequest: HomeCardsRequest = new HomeCardsRequest();
    homeCardsRequestResponse.input = homeCardsRequest;
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/getHomeCardsData/', JSON.stringify(homeCardsRequestResponse), options).
      map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
  }

  getHomeCardsFlipDetails(cardType:String, requisitionCode:String, stage:String, status:String, active: Boolean) {
    let getHomeCardsFlipDetailsRequestResponse: GetHomeCardsFlipDetailsRequestResponse = new GetHomeCardsFlipDetailsRequestResponse();
    let getHomeCardsFlipDetailsRequest: GetHomeCardsFlipDetailsRequest = new GetHomeCardsFlipDetailsRequest();
    getHomeCardsFlipDetailsRequest.cardType = cardType;
    getHomeCardsFlipDetailsRequest.requisitionCode = requisitionCode;
    getHomeCardsFlipDetailsRequest.stage = stage;
    getHomeCardsFlipDetailsRequest.status = status;
    getHomeCardsFlipDetailsRequest.active = active;
    getHomeCardsFlipDetailsRequestResponse.input = getHomeCardsFlipDetailsRequest;
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/getHomeCardsFlipDetails/', JSON.stringify(getHomeCardsFlipDetailsRequestResponse), options).
      map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
  }

  getPublishToTotalCount() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/allPublishCount/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
}
