export class HomeCardsRequestResponse {
    input: HomeCardsRequest;
}

export class HomeCardsRequest {
    recruiterID: Number;
}

export class HomeCardsResponse {
    assigned: CardSructure[];
    targetDate: CardSructure[];
}

export class CardSructure {
    jobTitle: String;
    requisitionCode: String;
    worksiteHierarchy: String;
    flipFront: string;
    flipBack: string;
    label: string;
    cardType: string;
    stage: String;
    status: String;
    assignedCardTypeResponse: AssignedCardTypeResponse;
    stageStatusCardTypeResponse: StageStatusCardTypeResponse[];
    newCandidateCardTypeResponse: NewCandidateCardTypeResponse[];    
    applicantLoading: boolean = false;
    rhsExpanded: boolean = false;
    active : boolean = true;
    modifiedDate: Date;
}

export class GetHomeCardsFlipDetailsRequestResponse {
    input: GetHomeCardsFlipDetailsRequest
}

export class GetHomeCardsFlipDetailsRequest {
    cardType: String;
    requisitionCode: String;
    stage: String;
    status: String;
    active: Boolean;
}


export class AssignedCardTypeResponse {
    jobTitle: String;
    requisitionCode: String;
    hiringManager: String;
    noOfOpening: Number;
    orgUnitCode: String;
    joiningLocation: String;
    requisitionId: Number;
    sysRequisitionStatus: String;
    openingsCompleted: Number;
    createdDate: String;
    profileImage: string;
    remainingDays: number;
    remainingOpenings: number;
    teamCount: number;
    publishedToCount: number;
    stageID : number;
    sysStageStatus :  String;
    actorUserName:string;
    comments:string;
}

export class StageStatusCardTypeResponse {
    candidateName: String;
    candidateCode: String;
    modifiedDate: Date;
}
export class NewCandidateCardTypeResponse {
    candidateName: String;
    candidateCode: String;
    modifiedDate: Date;
}