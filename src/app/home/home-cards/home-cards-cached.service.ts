import { HomeCardsRequestResponse, HomeCardsRequest } from './home-cards-request-response';
import { SessionService } from './../../session.service';
import { Http } from '@angular/http';
import { CachedData } from './../../common/cached-data';
import { Injectable } from '@angular/core';

@Injectable()
export class HomeCardsCachedService extends CachedData {

  constructor(
    http: Http,
    sessionService: SessionService
  ) {

    super(http, sessionService);

    let homeCardsRequestResponse: HomeCardsRequestResponse = new HomeCardsRequestResponse();
    let homeCardsRequest: HomeCardsRequest = new HomeCardsRequest();
    homeCardsRequestResponse.input = homeCardsRequest;

    super.initialize(
      '/rest/altone/jobs/getHomeCardsData/',
      'POST',
      homeCardsRequestResponse
    );

  }  

  getData() {
    return super.getData().map((response) => {
      return response;
    })
  }

}
