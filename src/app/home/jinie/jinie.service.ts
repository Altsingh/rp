import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Http,Headers, RequestOptions } from '@angular/http';
import { SessionService } from '../../session.service';
import { JinieTo } from '../jinie/jinie-to'


@Injectable()
export class JinieService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getJinieToken(jinie):Observable<any> {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jinie/getJinieToken', JSON.stringify(jinie), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json();
      });
  }

}
