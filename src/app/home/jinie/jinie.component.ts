import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { SessionService } from '../../session.service';
import { JinieService } from './jinie.service';
import { JinieTo } from './jinie-to';

@Component({
  selector: 'alt-jinie',
  templateUrl: './jinie.component.html',
  styleUrls: ['./jinie.component.css']
})
export class JinieComponent implements OnInit {

  openMessanger = true;
  messengerUrl:SafeResourceUrl=null; 
  jinie: JinieTo = new JinieTo("", "", "Recruiter Portal");
  showGenie: boolean =false;

  constructor(private sanitizer: DomSanitizer,private session: SessionService, private jinieService: JinieService) {
    this.jinie.organizationUrl = window.location.hostname;
    this.jinie.userName =session.userName;
   }

  ngOnInit() {
    this.doPrepareMessengerUrl();
  }

  doPrepareMessengerUrl() {
    
    let jinieURL= this.session.jinieUrl;
    let isJinieEnabled = this.session.isJinieEnabled;
  
    this.jinieService.getJinieToken(this.jinie).subscribe(
      info => {
        if (info.messageCode.code === 'EC200') {
          console.log(JSON.stringify(info))
          const token: string = info.response.trim();
          const url = jinieURL + '/login?authToken=' + token + '&OnlyJinny='+ isJinieEnabled +'&domain=' + this.jinie.organizationUrl;
          // const domainName ="staging-hrms.peoplestrong.com";
          // const url = `${jinieURL}/login?authToken=${token}&OnlyJinny=${isJinieEnabled}&domain=${domainName}`;
          this.messengerUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
          this.showGenie = true;
        }else{
          this.showGenie = false;
        }
                         
      });
   
  }


}
