import { Injectable } from '@angular/core';
import { CachedData } from '../common/cached-data';
import { SessionService } from '../session.service';
import { Http, Response } from '@angular/http';

@Injectable()
export class HomePageDetailService extends CachedData{

  constructor(
    http: Http,
    sessionService: SessionService) {

    super(http, sessionService);

    super.initialize(
      '/rest/altone/common/homePageDetails/',
      'GET',
      '');
  }

}
