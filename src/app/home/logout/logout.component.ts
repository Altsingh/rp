import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
import { SessionService } from '../../session.service';
@Component({
  selector: 'alt-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private commonService: CommonService,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.commonService.hideRHS();
    this.sessionService.loggedIn = false;

  }

}
