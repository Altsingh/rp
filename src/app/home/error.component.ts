import { Component } from '@angular/core';

@Component({
    selector: 'alt-error',
    template: `
    <div class="error-container">
    <h1>Error 404</h1>
    <p>Sorry, but the page you were trying to view does not exist.</p>
    </div>
  `,
    styles: [`.error-container{
        text-align:center;
        margin-top: 150px;
        height:500px;
    }`]
})
export class ErrorComponent {


    constructor() { }

}
