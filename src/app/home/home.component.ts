import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { } from '@angular/common';
import { CommonService } from '../common/common.service';
import { NotificationService, NotificationSeverity } from '../common/notification-bar/notification.service';
import { SessionService } from '../session.service';
import { Location } from '@angular/common';
import { CardSructure } from './home-cards/home-cards-request-response'
import { HomePageDetailService } from './home-page-detail.service';
import { environment } from 'environments/environment';

import { TranslateService } from '@ngx-translate/core';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(0deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(-180deg)'
      })),
      state('backactive', style({
        transform: 'rotateY(0deg)'
      })),
      state('backinactive', style({
        transform: 'rotateY(180deg)'
      })),
      transition('active => inactive', animate('400ms ease-out')),
      transition('inactive => active', animate('400ms ease-in')),
      transition('backactive => backinactive', animate('400ms ease-out')),
      transition('backinactive => backactive', animate('400ms ease-in'))
    ])
  ]
})
export class HomeComponent implements OnInit {

  flip: string = 'active';
  isToggled: boolean;
  flip2: string = 'backinactive';
  count: any;
  profilePic: string;
  xoxoIntegration= false;
  employeeName: string = "";

  constructor(private commonService: CommonService,
    private notificationService: NotificationService,
    private sessionService: SessionService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private translate: TranslateService,
    private homePageDetailService: HomePageDetailService) {
      
  }
  toggleFlip() {

    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
    this.flip2 = (this.flip2 == 'backinactive') ? 'backactive' : 'backinactive';
  }
  redeem(){
    window.open("https://enterprise.xoxoday.com/", "_blank");
  }

  toggleFlipNew(response: CardSructure) {

    response.flipFront = (response.flipFront == 'inactive') ? 'active' : 'inactive';
    response.flipBack = (response.flipBack == 'backinactive') ? 'backactive' : 'backinactive';
  }

  handleProfilePicError() {
    this.profilePic = 'assets/images/svg/genericUser.svg';
  }

  ngOnInit() {
    this.translate.use(this.sessionService.langString);
    this.sessionService.getLang().subscribe((lang) => {
      this.translate.use(lang);
    });
    
    this.xoxoIntegration = environment.xoxoIntegration;
    this.profilePic = this.sessionService.profilePic;
    
    this.sessionService.getProfilePic().subscribe((res) => {
      this.profilePic = res;
    })

    Promise.resolve(this.sessionService.readCookie()).then((data) => {
      this.loadHomePageDetails();
    });

    this.commonService.hideRHS();
  }

  private loadHomePageDetails() {
    this.homePageDetailService.getData().subscribe((response) => {
      if (response) {
        if (response.response) {
          if (response.response.userName) {
            this.employeeName = response.response.userName;
          }
          if (response.response.profilePic) {
            this.sessionService.setProfilePic(response.response.profilePic);
            this.profilePic = response.response.profilePic;
          }
        }
      }
    });
  }

 setCount(event: any) {
    this.count = event;
  }

  enableStatic() {
    return this.sessionService.removeStatic;
  }

  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }
}

