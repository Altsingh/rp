import { Component } from '@angular/core';
import { CommonService } from './common/common.service'
import { SessionService } from './session.service';
import { NavigationEnd, Router } from '@angular/router';

declare var ga: Function;

@Component({
  selector: 'alt-root',
  templateUrl: './app.component.html'
})

export class AppComponent {
  title = 'alt works!';

  constructor(
    private commonService: CommonService,
    public sessionService: SessionService,
    _router: Router
  ) {

    // Using Rx's built in `distinctUntilChanged ` feature to handle url change c/o @dloomb's answer
 
    _router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        var dimensionValue = sessionService.employeeName + "_" + sessionService.userID;
        ga('set', 'dimension2', dimensionValue);
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    });
    
  }

  onclick(event) {
    this.commonService.click(event);
  } 
}
