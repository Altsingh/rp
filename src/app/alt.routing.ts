import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './home/error.component';
import { AppliedForComponent } from './header/applied-for/applied-for.component';
import { LivefeedComponent } from './header/livefeed/livefeed.component';
import { DownloadComponent } from './header/download/download.component'
import { PendingTaskComponent } from './header/pending-task/pending-task.component';
import { SearchComponent } from './header/search/search.component';
import { ProfileComponent } from './header/profile/profile.component';
import { TaskDayComponent } from './header/pending-task/task-day/task-day.component';
import { TaskWeekComponent } from './header/pending-task/task-week/task-week.component';
import { TaskMonthComponent } from './header/pending-task/task-month/task-month.component';
import { MyTeamComponent } from './header/my-team/my-team.component';
import { MyReferralsComponent } from './header/my-referrals/my-referrals.component';
import { LogoutComponent } from './home/logout/logout.component'
import { SessionGuard } from './session.guard';

const ALT_ROUTES: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'Applied', component: AppliedForComponent},
    { path: 'LiveFeed', component: LivefeedComponent},
    { path: 'Download', component: DownloadComponent},
    { path: 'Tasks', component: PendingTaskComponent, children:[
        {path: 'Day', component: TaskDayComponent},
        {path: 'Week', component: TaskWeekComponent},
        {path: 'Month', component: TaskMonthComponent}
    ] },
    { path: 'Search', component: SearchComponent},
     { path: 'MyProfile', component: ProfileComponent},   
     { path: 'MyTeam', component: MyTeamComponent},
     { path: 'MyReferrals', component: MyReferralsComponent},
    { path: "dashboard", loadChildren: "app/dashboardchart/dashboard.module#DashboardModule"},
    { path: "reports" , loadChildren:   "app/reports/report.module#ReportModule"},
    { path: "dashboards", loadChildren: "app/dashboardchartStatic/dashboard.module#DashboardModule"},
    { path: 'job', loadChildren: 'app/job/job.module#JobModule'},
    { path: 'candidate', loadChildren: 'app/candidate/candidate.module#CandidateModule'},
    { path: 'vendor', loadChildren: 'app/vendor/vendor.module#VendorModule'},
    { path: 'error', component: ErrorComponent },
    { path: 'logout', component: LogoutComponent },
    { path: '**', redirectTo: 'error' }
];

export const AltRouting = RouterModule.forRoot(ALT_ROUTES);
