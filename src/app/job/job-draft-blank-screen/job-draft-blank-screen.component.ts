import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-job-draft-blank-screen',
  templateUrl: './job-draft-blank-screen.component.html',
  styleUrls: ['./job-draft-blank-screen.component.css']
})
export class JobDraftBlankScreenComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
