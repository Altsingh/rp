import { Component, OnInit } from '@angular/core';
import { PageState } from '../../shared/paginator/paginator.component';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { JobFilterTO } from '../job-list/job-filter-to';
import { JobListResponse } from '../../common/job-list-response';
import { MessageCode } from '../../common/message-code';
import { FormatJobcodeParamPipe } from '../../shared/format-jobcode-param.pipe';
import { DialogService } from '../../shared/dialog.service';
import { SessionService } from '../../session.service';
import { NameinitialService } from '../../shared/nameinitial.service';
import { CommonService } from '../../common/common.service';
import { Router } from '@angular/router';
import { JobActionService } from '../job-action/job-action.service';
import { NotificationService } from '../../common/notification-bar/notification.service';
import { JobDetailService } from '../job-detail/job-detail.service';
import { JobListAlljobsPagedService } from '../job-list/job-list-alljobs-paged.service';
import { JobCommonServiceService } from '../job-common-service.service';
import { JobListMyjobsPagedService } from '../job-list/job-list-myjobs-paged.service';
import { PublishToERPagedService } from './publish-to-er-paged.service';
import { PublishToIJPPagedService } from './publish-to-ijp-paged.service';

@Component({
  selector: 'alt-publish-to-er-job-list',
  templateUrl: './publish-to-er-job-list.component.html',
  styleUrls: ['./publish-to-er-job-list.component.css'],
  providers:[JobListAlljobsPagedService, JobListMyjobsPagedService,PublishToERPagedService,PublishToIJPPagedService]
})
export class PublishToErJobListComponent implements OnInit {

  showloader: boolean = false;
  load: number = 3;
  pageState: PageState = new PageState();
  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;
  filterTO: JobFilterTO;

  loaderIndicator: string;
  filterHidden: boolean = true;
  applyRecruiterFilter: boolean = true;
  showStyle: false;
  isClassVisible: false;
  jobListData: JobListResponse[];
  messageCode: MessageCode;
  subscription: any;
  JOB_STATUS_CLASS: String = "";
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";
  selected: any[] = [];
  selectAll: boolean;
  currentURL: string;
  pageTitle: string;
  publishedToTotal: number;
  jobDetails: any;
  RecruitersOfJob: any;
  RecruiterList: any;
  formatJobcodeParamPipe: FormatJobcodeParamPipe = new FormatJobcodeParamPipe();
  featureDataMap: Map<any,any>;
  isBulkMenu : Boolean = true;


  constructor(
     private DialogService: DialogService,
    private sessionService: SessionService, private commonService: CommonService,
    private nameinitialService: NameinitialService,
    private router: Router, private jobActionBulkService: JobActionService,
    private notificationService: NotificationService,
    private jobDetailService: JobDetailService,
    private jobListAllJobsPagedService: JobListAlljobsPagedService,
    private jobListMyJobsPagedService: JobListMyjobsPagedService,
    private jobCommonServiceService: JobCommonServiceService,
    private formBuilder: FormBuilder,
    private publishToERJobsPagedService: PublishToERPagedService,
    private publishToIJPPagedService : PublishToIJPPagedService
  ) { 
    this.randomcolor = this.nameinitialService.randomcolor;
    this.filterTO = new JobFilterTO();
    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });
    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {
      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }
      this.queryTimeout = setTimeout(() => {
        let query = this.queryFormGroup.controls.query.value;
        if (query.length >= 2) {
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          if (this.pageTitle === 'ERP') {
            this.publishToERJobsPagedService.clearCache();
            this.loadERPJobsData();
            this.loadERPJobsTotalRows();
          } else if (this.pageTitle === 'INTERNAL POSTINGS') {
            this.publishToIJPPagedService.clearCache();
            this.loadIJPJobsData();
            this.loadIJPJobsTotalRows();
          } 
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          if (this.pageTitle === 'ERP') {
            this.publishToERJobsPagedService.clearCache();
            this.loadERPJobsData();
            this.loadERPJobsTotalRows();
          } else if (this.pageTitle === 'INTERNAL POSTINGS') {
            this.publishToIJPPagedService.clearCache();
            this.loadIJPJobsData();
            this.loadIJPJobsTotalRows();
          } 
        }
      }, 200);

    });
  }

  ngOnInit() {

    this.selected = [];
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.loaderIndicator = "loader";
    this.filterHidden = true;
    this.commonService.showRHS();

    if (this.router.url.endsWith('erp')) {
      this.pageTitle = "ERP";
      this.applyRecruiterFilter = true;
      this.loadERPJobsData();
      this.loadERPJobsTotalRows();
    } 
    else if(this.router.url.endsWith('ijp')){
      this.pageTitle = "INTERNAL POSTINGS";
      this.applyRecruiterFilter = true;
      this.loadIJPJobsData();
      this.loadIJPJobsTotalRows();
    }
  }

  loadERPJobsData(){

    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.publishToERJobsPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.jobListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });

  }

  loadERPJobsTotalRows(){

    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.publishToERJobsPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }

  loadIJPJobsData(){

    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.publishToIJPPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.jobListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });

  }
  loadIJPJobsTotalRows(){

    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.publishToIJPPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;
    if (this.pageTitle === 'ERP') {
      this.loadERPJobsData();
    } else if( this.pageTitle === 'INTERNAL POSTINGS'){
      this.loadIJPJobsData();
    }
  }


  areAllSelected() {
    if (this.jobListData != null && this.jobListData.length > 0) {
      for (let x = 0; x < this.jobListData.length; x++) {
        if (typeof this.jobListData[x].selected == 'undefined' || (typeof this.jobListData[x].selected == 'boolean' && !this.jobListData[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }

  ngOnDestroy(): void {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }

  }

  gotoJobDetail(data) {
    this.jobDetailService.object = data.requisitionId;
    this.jobDetailService.objectType = 'REQUISITION_ID';
    if(this.pageTitle == 'ERP'){
    this.router.navigateByUrl('/job/edit-job-er/' + this.formatJobcodeParamPipe.transform(data.requisitionCode) +'/job-details');
  } else if(this.pageTitle == 'INTERNAL POSTINGS'){
    this.router.navigateByUrl('/job/edit-job-ijp/' + this.formatJobcodeParamPipe.transform(data.requisitionCode) +'/job-details');
  }
}

  onFilter(params: any[]) {
    let hiringManagerFilter = false;
    let recruiterFilter = false;
    let jobStatusFilter = false;
    let locationFilter = false;
    let postedFilter = false;
    let gradeFilter = false;
    for (let param of params) {
      if (param.name == 'HIRING_MANAGER') {
        this.filterTO.hiringManagerID = param.value.value;
        hiringManagerFilter = true;
      } else if (param.name == 'RECRUITER') {
        this.filterTO.recruiterID = param.value.value;
        recruiterFilter = true;
      } else if (param.name == 'JOB_STATUS') {
        this.filterTO.jobStatus = param.value.value;
        jobStatusFilter = true;
      } else if (param.name == 'LOCATION') {
        this.filterTO.joiningLocation = param.value.value;
        locationFilter = true;
      } else if (param.name == 'POSTED_ON') {
        this.filterTO.postedON = param.value.value;
        postedFilter = true;
      } else if( param.name == 'GRADE'){
        this.filterTO.grade = param.value.value;
        gradeFilter = true;
      }
    }
    if (!hiringManagerFilter) {
      this.filterTO.hiringManagerID = null;
    }
    if (!recruiterFilter) {
      this.filterTO.recruiterID = null;
    }
    if (!jobStatusFilter) {
      this.filterTO.jobStatus = null;
    }
    if (!locationFilter) {
      this.filterTO.joiningLocation = null;
    }
    if (!postedFilter) {
      this.filterTO.postedON = null;
    }
    if(!gradeFilter){
      this.filterTO.grade = null;
    }
    if (this.pageTitle === 'ERP') {
      this.publishToERJobsPagedService.clearCache();
    } 
    else if(this.pageTitle === 'INTERNAL POSTINGS'){
      this.publishToIJPPagedService.clearCache();
    }
    this.ngOnInit();
  }

}
