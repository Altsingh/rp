import { Http } from '@angular/http';
import { PagedDataService } from '../../common/paged-data.service';
import { Injectable } from '@angular/core';
import { SessionService } from '../../session.service';

@Injectable()
export class PublishToIJPPagedService extends PagedDataService{

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/posting/ijp/',
      '/rest/altone/posting/ijp/totalRows/'
    );
  }

}
