import { Http } from '@angular/http';
import { PagedDataService } from '../../common/paged-data.service';
import { Injectable } from '@angular/core';
import { SessionService } from '../../session.service';

@Injectable()
export class PublishToERPagedService extends PagedDataService{

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/posting/erp/',
      '/rest/altone/posting/erp/totalRows/'
    );
  }

}
