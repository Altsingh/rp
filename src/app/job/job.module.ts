
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobComponent } from './job.component';
import { JobRouting } from './job.routing';
import { NewJobComponent } from './new-job/new-job.component';
import { JobListComponent } from './job-list/job-list.component';
import { JobListService } from './job-list/job-list.service'
import { JobDetailComponent } from './job-detail/job-detail.component';
import { JobSummaryComponent } from './job-detail/job-summary/job-summary.component';
import { JobSummaryContextMenuComponent } from './job-detail/job-summary-context-menu/job-summary-context-menu.component';
import { JobsStageListComponent } from './job-detail/jobs-stage-list/jobs-stage-list.component';
import { JobDetailsMainComponent } from './job-detail/job-details-main/job-details-main.component';
import { BasicInformationComponent } from './job-detail/job-details-main/basic-information/basic-information.component';
import { OrganizationDetailComponent } from './job-detail/job-details-main/organization-detail/organization-detail.component';
import { RequiredSkillSetComponent } from './job-detail/job-details-main/required-skill-set/required-skill-set.component';
import { JobDescriptionComponent } from './job-detail/job-details-main/job-description/job-description.component';
import { EditJobComponent } from './edit-job/edit-job.component';
import { WorkflowAndNotificationsComponent } from './job-detail/job-details-main/workflow-and-notifications/workflow-and-notifications.component';
import { CtqComponent } from './job-detail/job-details-main/ctq/ctq.component';
import { ActionsComponent } from './job-detail/job-details-main/actions/actions.component';
import { CommentComponent } from './job-detail/job-details-main/comment/comment.component';
import { PerformanceSourceTeamComponent } from './job-detail/performance-source-team/performance-source-team.component';
import { TaggedCandidatesComponent } from './job-detail/tagged-candidates/tagged-candidates.component';
import { AutoMatchedCandidatesComponent } from './job-detail/auto-matched-candidates/auto-matched-candidates.component';
import { RejectedCandidatesComponent } from './job-detail/rejected-candidates/rejected-candidates.component';
import { HistoryChatsComponent } from './job-detail/history-chats/history-chats.component';
import { ParseJobcodeParamPipe } from './parse-jobcode-param.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatSliderModule, MatInputModule, MatTabsModule, MatDialogModule, MatOption,  MatAutocompleteModule, MatRadioModule, MatNativeDateModule, MatTooltipModule, MatDatepickerModule, MatProgressSpinnerModule } from '@angular/material';
import { CandidateFilterComponent } from './job-detail/tagged-candidates/candidate-filter/candidate-filter.component';
import { CandidateListComponent } from './job-detail/tagged-candidates/candidate-list/candidate-list.component';
import { CandidateCardComponent } from './job-detail/tagged-candidates/candidate-card/candidate-card.component';
import { DraftJobListComponent } from './draft-job-list/draft-job-list.component';
import { AssignToRecruiterComponent } from './job-action/assign-to-recruiter/assign-to-recruiter.component';
import { JobDetailService } from './job-detail/job-detail.service';
import { SharedModule } from '../shared/shared.module';
import { JobDraftBlankScreenComponent } from './job-draft-blank-screen/job-draft-blank-screen.component';
import { JobListDataFilterPipe } from './job-list/job-list-data-filter.pipe';
import { JobTemplateListDataFilterPipe } from './new-job/job-template-list-data-filter.pipe';
import { FormsModule } from '@angular/forms';
import { MyJobBlankScreenComponent } from './my-job-blank-screen/my-job-blank-screen.component';
import { NewJobFormSecurityService } from './new-job/new-job-form-security.service';
import { PostToIjpComponent } from './job-action/post-to-ijp/post-to-ijp.component';
import { PostToEmpRefComponent } from './job-action/post-to-emp-ref/post-to-emp-ref.component';
import { JobActionService } from "./job-action/job-action.service";
import { RejectedCandidatesCardComponent } from './job-detail/rejected-candidates/rejected-candidates-card/rejected-candidates-card.component';
import { PostToCandidatePortalComponent } from './job-action/post-to-candidate-portal/post-to-candidate-portal.component';
import { AddInterviewPanelistComponent } from './job-action/add-interview-panelist/add-interview-panelist.component';
import { InterviewPanelService } from './new-job/interview-panel.service';
import { JobContextMenuComponent } from './job-list/job-context-menu/job-context-menu.component';
import { AssignToVendorsComponent } from './job-action/assign-to-vendors-bulk/assign-to-vendors-bulk.component';
import { AssignToRecuiterBulkComponent } from './job-action/assign-to-recuiter-bulk/assign-to-recuiter-bulk.component';
import { PublishToCandidateBulkComponent } from './job-action/publish-to-candidate-bulk/publish-to-candidate-bulk.component';
import { PublishToInternalBulkComponent } from './job-action/publish-to-internal-bulk/publish-to-internal-bulk.component';
import { PublishToEmployeeBulkComponent } from './job-action/publish-to-employee-bulk/publish-to-employee-bulk.component';
import { CopyToAnotherJobComponent } from './job-detail/tagged-candidates/copy-to-another-job/copy-to-another-job.component';
import { EmailCandidatesComponent } from './job-detail/tagged-candidates/email-candidates/email-candidates.component';
import { NotifyRecruiterComponent } from './job-detail/tagged-candidates/notify-recruiter/notify-recruiter.component';
import { UntaggedCandidateComponent } from './job-detail/tagged-candidates/untagged-candidate/untagged-candidate.component';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { PublishToVendorComponent } from './job-detail/performance-source-team/publish-to-vendor/publish-to-vendor.component';
import { PublishedVendorsListComponent } from './job-detail/performance-source-team/published-vendors-list/published-vendors-list.component';
import { PublishedVendorsListPopupComponent } from './job-detail/performance-source-team/published-vendors-list-popup/published-vendors-list-popup.component';
import { AutoMatchCandidatesService } from './job-detail/auto-match-candidates.service';
import {DialogService} from '../shared/dialog.service';
import { SocialProfilesComponent } from './job-detail/auto-matched-candidates/social-profiles/social-profiles.component';
import { CompanyProfilesComponent } from './job-detail/auto-matched-candidates/company-profiles/company-profiles.component';
import { JobBoardProfilesComponent } from './job-detail/auto-matched-candidates/job-board-profiles/job-board-profiles.component';
import { NewJobComponentLoaderComponent } from './new-job/new-job-component-loader/new-job-component-loader.component';
import { SocialCandidateDetailComponent } from './job-detail/auto-matched-candidates/social-profiles/social-candidate-detail/social-candidate-detail.component';
import { CandidateContextMenuComponent } from './job-detail/tagged-candidates/candidate-list/candidate-context-menu/candidate-context-menu.component';
import { SocialCandidateDetailLoadingScreenComponent } from './job-detail/auto-matched-candidates/social-profiles/social-candidate-detail-loading-screen/social-candidate-detail-loading-screen.component';
import { PostToNaukriComponent } from './job-boards/naukri/post-to-naukri/post-to-naukri.component';
import { MonsterComponent } from './job-detail/auto-matched-candidates/job-board-profiles/monster/monster/monster.component';
import { MonsterLoaderComponent } from './job-detail/auto-matched-candidates/job-board-profiles/monster/monster-loader/monster-loader.component';
import { MonsterDetailComponent } from './job-detail/auto-matched-candidates/job-board-profiles/monster/monster-detail/monster-detail.component';
import { JobDetailWiringService } from './job-detail/job-detail-wiring.service';
import { MonsterDetailLoaderComponent } from './job-detail/auto-matched-candidates/job-board-profiles/monster/monster-detail-loader/monster-detail-loader.component';
import { PostToMonsterComponent } from './job-boards/monster/post-to-monster/post-to-monster.component';
import { AutomatchEmailComponent } from './job-detail/auto-matched-candidates/automatch-email/automatch-email.component';
import { MonsterBlankScreenComponent } from './job-detail/auto-matched-candidates/job-board-profiles/monster/monster-blank-screen/monster-blank-screen.component';
import { StatusChangeComponent } from './job-detail/status-change/status-change.component';
import { PostToIimjobsComponent } from './job-boards/iimjobs/post-to-iimjobs/post-to-iimjobs.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CreatedJobListComponent } from './created-job-list/created-job-list.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { PublishToErJobListComponent } from './publish-to-er-job-list/publish-to-er-job-list.component';
import { JobDetailBusinessComponent } from './job-detail/job-detail-business.component';
import { EditJobBusinessComponent } from './edit-job-business/edit-job-business.component';
import { JobSummaryBpComponent } from './job-detail/job-summary-bp/job-summary-bp.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({

  declarations: [JobComponent, NewJobComponent, JobListComponent, JobDetailComponent,
    JobSummaryComponent, JobSummaryContextMenuComponent, JobsStageListComponent, JobDetailsMainComponent,
    BasicInformationComponent, OrganizationDetailComponent, RequiredSkillSetComponent, JobDescriptionComponent,
    EditJobComponent, WorkflowAndNotificationsComponent, CtqComponent, ActionsComponent, CommentComponent,
    PerformanceSourceTeamComponent, TaggedCandidatesComponent, AutoMatchedCandidatesComponent, JobContextMenuComponent,
    RejectedCandidatesComponent, HistoryChatsComponent, ParseJobcodeParamPipe, CandidateFilterComponent, CandidateListComponent, CandidateCardComponent,
    DraftJobListComponent, AssignToRecruiterComponent, JobListDataFilterPipe, JobDraftBlankScreenComponent, MyJobBlankScreenComponent, JobTemplateListDataFilterPipe, PostToIjpComponent, PostToEmpRefComponent, RejectedCandidatesCardComponent, PostToCandidatePortalComponent, AddInterviewPanelistComponent, AssignToVendorsComponent, AssignToRecuiterBulkComponent, PublishToCandidateBulkComponent, PublishToInternalBulkComponent, PublishToEmployeeBulkComponent, CopyToAnotherJobComponent, EmailCandidatesComponent, NotifyRecruiterComponent, UntaggedCandidateComponent, PublishToVendorComponent, PublishedVendorsListComponent, PublishedVendorsListPopupComponent, SocialProfilesComponent, CompanyProfilesComponent, JobBoardProfilesComponent, NewJobComponentLoaderComponent, SocialCandidateDetailComponent, CandidateContextMenuComponent, SocialCandidateDetailLoadingScreenComponent, PostToNaukriComponent, MonsterComponent, MonsterLoaderComponent, MonsterDetailComponent, MonsterDetailLoaderComponent, PostToMonsterComponent, AutomatchEmailComponent, MonsterBlankScreenComponent, StatusChangeComponent, PostToIimjobsComponent, CreatedJobListComponent, PublishToErJobListComponent,JobDetailBusinessComponent,EditJobBusinessComponent,JobSummaryBpComponent],
  imports: [SharedModule, NgSlimScrollModule, JobRouting, CommonModule, ReactiveFormsModule, MatSelectModule, MatRadioModule, MatInputModule, MatTabsModule, MatSliderModule, MatDialogModule, MatAutocompleteModule, MatNativeDateModule, MatTooltipModule, MatDatepickerModule, MatProgressSpinnerModule, FormsModule, 
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
    }
  })
  ],
  entryComponents: [
    AssignToRecruiterComponent, PostToIjpComponent, PostToEmpRefComponent, PostToCandidatePortalComponent, AddInterviewPanelistComponent, AssignToVendorsComponent, AssignToRecruiterComponent, PublishToCandidateBulkComponent, PublishToEmployeeBulkComponent, PublishToInternalBulkComponent, AssignToRecuiterBulkComponent
    , CopyToAnotherJobComponent, EmailCandidatesComponent, NotifyRecruiterComponent, UntaggedCandidateComponent, PublishToVendorComponent, PublishedVendorsListPopupComponent, 
    PostToNaukriComponent, PostToMonsterComponent, AutomatchEmailComponent, StatusChangeComponent, PostToIimjobsComponent
  ],

  providers: [JobListService, JobDetailService, NewJobFormSecurityService, JobActionService, InterviewPanelService, AutoMatchCandidatesService,DialogService,JobDetailWiringService]
})

export class JobModule {

}
