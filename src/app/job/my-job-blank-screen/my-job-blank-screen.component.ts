import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alt-my-job-blank-screen',
  templateUrl: './my-job-blank-screen.component.html',
  styleUrls: ['./my-job-blank-screen.component.css']
})
export class MyJobBlankScreenComponent implements OnInit {

  @Input() title : string;

  constructor() { }

  ngOnInit() {
  }

}
