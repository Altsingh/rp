import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../parse-jobcode-param.pipe';

@Component({
  selector: 'alt-edit-job',
  templateUrl: './edit-job.component.html',
  providers: [ParseJobcodeParamPipe]
})

export class EditJobComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;

  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = this.parser.transform(params['id']);
          });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
