export class PostJobToCandPortal{
  requisitionID: Number;
  cpStartDate: Date;
  cpEndDate: Date;
  jobCode: String;
}