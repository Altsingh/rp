import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatNativeDateModule } from '@angular/material';
import { JobBasicInfo } from '../job-basic-info';
import { JobActionService } from '../job-action.service';
import { SessionService } from '../../../session.service';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { PostJobToCandPortal } from './post-to-candidateportal-data';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-post-to-candidate-portal',
  templateUrl: './post-to-candidate-portal.component.html',
  styleUrls: ['./post-to-candidate-portal.component.css']
})
export class PostToCandidatePortalComponent implements OnInit {
  lable: string = "post";
  publishCandidate: boolean = false;
  jobData: JobBasicInfo;
  messageCode: MessageCode;
  cpStartDate: Date;
  cpEndDate: Date;
  jobCode: String;
  postToCandPortal: PostJobToCandPortal;
  postToCandPortalResponseData: PostToCandPortalResponse;
  message: String;
  severity: String;
  severityClass: string;
  disableCandPortalStartDate: boolean;
  landscape: "landscape";
  timer:any;
  
  constructor(public jobActionService: JobActionService, public dialogRef: MatDialogRef<PostToCandidatePortalComponent>,
    private sessionService: SessionService, private notificationService: NotificationService,private i18UtilService:I18UtilService) {
    this.message = "";
    this.jobData = new JobBasicInfo();
    this.disableCandPortalStartDate = false;
    this.postToCandPortalResponseData = new PostToCandPortalResponse();
  }

  ngOnInit() {
    this.jobActionService.getJobDetails().subscribe(res => {
      this.jobData = res.responseData[0],
      this.messageCode = res.messageCode;
      if(this.jobData != null) {
        if (this.jobData.cpPosted == true) {
          this.jobCode = this.jobData.requisitionCode;
          this.lable='update'
          this.disableCandPortalStartDate = true;
          if(this.jobData.cpStartDate != null) {
            this.cpStartDate = new Date(this.jobData.cpStartDate);
          }
          if(this.jobData.cpEndDate != null) {
            this.cpEndDate = new Date(this.jobData.cpEndDate)
          }
        } else {
          this.disableCandPortalStartDate = false;
          this.jobCode = this.jobData.requisitionCode;
          this.cpStartDate = new Date();
          this.cpEndDate = new Date();
          this.cpEndDate.setDate(this.cpStartDate.getDate() + 7);
        }
      }
    });
  }

  postJobToCandPortal(event) {
    if (this.cpStartDate != null) {
      this.cpStartDate = new Date(this.cpStartDate.toDateString())
    }
    if (this.cpEndDate != null) {
      this.cpEndDate = new Date(this.cpEndDate.toDateString())
    }
    if (this.cpStartDate > this.cpEndDate) {
      this.setMessage("WARNING", "common.warning.endDateStartDate");
    } else if (this.jobData.jobStatus == "ONHOLD" || this.jobData.jobStatus === 'PHOLD') {
      this.setMessage("WARNING", "job.warning.failedPostHoldJob");
    } else if (this.jobData.jobStatus === 'CLOSED' || this.jobData.jobStatus === 'PCLOSE') {
      this.setMessage("WARNING", "job.warning.failedPostCloseJob");
    } else if (this.cpEndDate == null) {
      this.setMessage("WARNING", "common.warning.endDateRequired");
    } else {
      this.publishCandidate = true;
      let postToCandPortalList: PostJobToCandPortal[] = [];
      this.postToCandPortal = new PostJobToCandPortal();
      this.postToCandPortal.requisitionID = this.sessionService.object.requisitionId;
      this.postToCandPortal.cpStartDate = this.cpStartDate;
      this.postToCandPortal.cpEndDate = this.cpEndDate;
      this.postToCandPortal.jobCode = this.jobCode;
      postToCandPortalList.push(this.postToCandPortal);
      this.jobActionService.postJobToCandPortal(postToCandPortalList).subscribe(out => {
        this.messageCode = out.messageCode;
        this.postToCandPortalResponseData.result = out.responseData[0];
        if (this.postToCandPortalResponseData.result == "success") {
          this.i18UtilService.get("postCP.publish.success").subscribe(res=>{
            this.notificationService.setMessage("INFO", res);
          })
          if (this.disableCandPortalStartDate) {
            this.dialogRef.close(0);
          } else {
            this.publishCandidate = false;
            this.dialogRef.close(1);
          }
        }
      });
    }
  }

  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe(res=>{
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);
    })
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  selectCpStartDate(cpStartDate: Date) {
    this.cpStartDate = cpStartDate;
  }

  selectCpEndDate(cpEndDate: Date) {
    this.cpEndDate = cpEndDate;
  }

}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class PostToCandPortalResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
