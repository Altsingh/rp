import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../common/notification-bar/notification.service";
import { MatDialogRef } from '@angular/material';
import { JobActionService } from '../job-action.service'
import { SessionService } from '../../../session.service';
import { DialogService } from '../../../shared/dialog.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-assign-to-recuiter-bulk',
  templateUrl: './assign-to-recuiter-bulk.component.html',
  styleUrls: ['./assign-to-recuiter-bulk.component.css']
})
export class AssignToRecuiterBulkComponent implements OnInit {
  assignRecruiter: boolean = false;
  lable: string;
  datalist: JobListResponse[] = [];
  assignReqToRecData: AssignReqToRecruiterData;
  reqRecruiterDataList: AssignReqToRecruiterData[];
  reqRecruiterData: RequisitionRecruiterData;
  recruiterList: SelectItem[];
  message: String;
  messageCode: MessageCode;
  data: ResponseData[];
  jobhold: string;
  timer: any;
  severity: String;
  severityClass: string;
  assignRecToRecResponseData: AssignReqToRecResponse;
  minOpening: number = 0;
  disableAssign: boolean;
  todayDate:Date;

  constructor(public dialogRef: MatDialogRef<AssignToRecuiterBulkComponent>, private notificationService: NotificationService,
    private jobActionService: JobActionService, private SessionService: SessionService, private dialogService: DialogService,
    private i18UtilService: I18UtilService) {
    this.reqRecruiterDataList = [];
    this.assignRecToRecResponseData = new AssignReqToRecResponse();
    this.message = "";
    this.todayDate=new Date();
    this.todayDate.setHours(0,0,0,0);
  }

  ngOnInit() {
    this.reqRecruiterDataList = [];
    this.datalist = this.jobActionService.getDataList();
    let reqidlist: any[] = [];
    for (let requisitionId of this.datalist) {
      if (requisitionId) {
        reqidlist.push(requisitionId.noOfOpening);
      }
    }
    this.minOpening = Math.min.apply(null, reqidlist);
    let res = this.dialogService.getDialogData();
    this.recruiterList = res.responseData,
      this.messageCode = res.messageCode;
    this.addRow();
    this.i18UtilService.get('common.label.assign').subscribe((res: string) => {
      this.lable = res;
     });
  }

  public restrictNumeric(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) //for restricting backsapce
    {
      return false;
    }
    if (e.which === 0) //for enabling escape
    {
      return true;
    }
    if (e.which < 33) //for only numeric values
    {
      return true;
    }
    input = String.fromCharCode(e.which);
    if (/[\d\s]/.test(input) == false) {
      this.setMessage("WARNING", 'job.warning.openingNumeric');
    }
    return !!/[\d\s]/.test(input);

  }

  public deleteRow(index: number) {
    if (index != -1) {
      this.reqRecruiterDataList.splice(index, 1);
      this.disableAssign=false;
    }
  }
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      this.jobhold = "CLOSED";
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      this.jobhold = "HOLD";
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  public addRow() {
    this.reqRecruiterData = new RequisitionRecruiterData();
    this.assignReqToRecData = new AssignReqToRecruiterData();
    this.assignReqToRecData.reqIdList = [];
    this.assignReqToRecData.assignedOpenings = 0;
    this.assignReqToRecData.targetDate = new Date();
    this.assignReqToRecData.userId = 0;
    this.reqRecruiterDataList.push(this.assignReqToRecData);

  }

  assignMultipleJobsToRecruiter(event) {
    this.notificationService.clear();
    if (this.jobhold == "HOLD") {
      this.setMessage("WARNING", 'job.warning.removeOnHoldJob');
      return;
    }
    if (this.jobhold == "CLOSED") {
      this.setMessage("WARNING", 'job.warning.removeClosedJob');
      return;
    }
    let assignReqToRecruiter: AssignReqToRecruiterData;
    let assignReqToRecruiterList: AssignReqToRecruiterData[] = [];
    let reqidlist: number[] = [];

    for (let r of this.reqRecruiterDataList) {
      r.userId = parseInt(r.userIdStrVal);
      if (r.userIdStrVal == null || r.userIdStrVal == "") {
        this.setMessage("WARNING", "job.warning.selectRecruiter1");
        return;
      }
      if(r.targetDate < this.todayDate) {
        this.setMessage("WARNING", 'job.warning.target_CurrentDateforNewRecruiter');
        return;
      }
      assignReqToRecruiter = new AssignReqToRecruiterData();
      assignReqToRecruiter.reqIdList = [];
      assignReqToRecruiter.jobCodeList = [];

      for (let requisition of this.datalist) {
        assignReqToRecruiter.reqIdList.push(requisition.requisitionId);
      }
      for (let r of this.datalist) {
        assignReqToRecruiter.jobCodeList.push(r.requisitionCode);
      }
      if (r.assignedOpenings == 0) {
        this.setMessage("WARNING", 'job.warning.assignedOpeningNotZero');
        return;
      }
      if (r.assignedOpenings > this.minOpening) {
        this.setMessage("WARNING", 'job.warning.AssignMinimumOpeningCheck');
        return;
      }
      assignReqToRecruiter.assignedOpenings = r.assignedOpenings;
      assignReqToRecruiter.targetDate = r.targetDate;
      assignReqToRecruiter.userId = r.userId;
      assignReqToRecruiterList.push(assignReqToRecruiter);
    }
    if (this.reqRecruiterData != null) {
      this.assignRecruiter = true;
      this.disableAssign = true;
      this.jobActionService.assignMultipleJobsToRecruiter(assignReqToRecruiterList).subscribe(out => {
        this.messageCode = out.messageCode;
        this.assignRecToRecResponseData.result = out.responseData[0];
        if (this.assignRecToRecResponseData.result == "success") {
          this.dialogRef.close("success");
          this.i18UtilService.get('job.success.jobAssigned').subscribe(res=>{
            this.notificationService.setMessage("INFO", res);
          });
          this.jobActionService.getJobComponent().ngOnInit();
        } else {
          this.assignRecruiter = false;
          this.setMessage("WARNING", this.assignRecToRecResponseData.result);
          this.disableAssign = false;
        }
      });
    }
  }

  selectAssignedDate(targetDate: Date) {

  }
  public setMessage(severity: string, message: string, args?: any) {
    if(!isNullOrUndefined(args)){
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
        this.timer = setTimeout(() => {
          this.clearMessages();
          this.timer = null;
        }, 3000);
       });
     
    }else{
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
        this.timer = setTimeout(() => {
          this.clearMessages();
          this.timer = null;
        }, 3000);
       });
    }
    
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }
   getUserName(index:Number)
  {
    for(let rec of this.recruiterList)
    {
      if(rec.value == index)
      {
        return rec.label;
      }
    }
  }

  changeRecruiter(event, index) {
    let reqRecruiterList: any[] = [];
    for (let recruiter of this.reqRecruiterDataList) {
      reqRecruiterList.push(recruiter.userIdStrVal);
    }
    reqRecruiterList.splice(index, 1);
    for (let i = 0; i < (reqRecruiterList.length); i++) {
      if (event.value == parseInt(reqRecruiterList[i])) {
     
       this.setMessage("WARNING",'job.warning.recruiterAlreadySelected',{'name': this.getUserName(parseInt(reqRecruiterList[i]))} );
        this.disableAssign = true;
        return;
      } else {
        this.disableAssign = false;
        this.clearMessages();
      }

    }
  }

}
export interface ResponseData {
  firstName: String;
  middleName: String;
  lastName: String;
  remainingOpenings: number;
  assignedOpenings: number;
  jobCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLoc: String;
  assignedDate: Date;
  targetDate: Date;
  requisitionRecruiterID: number;
  userID: number;
}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class RequisitionRecruiterData {
  input: AssignReqToRecruiterData[];
}
export class AssignReqToRecruiterData {
  reqIdList: number[];
  userId: Number;
  assignedOpenings: number;
  targetDate: Date;
  userIdStrVal: string;
  jobCodeList: String[];
  minOpening: number[];
}
export class SelectItem {
  label: string;
  value: any;

  constructor(label: string, value: any) {
    this.label = label;
    this.value = value;
  }
}

export class AssignReqToRecResponse {
  result: string;
}

export class JobListResponse {
  draftRequisitionID: Number;
  requisitionId: number;
  hiringManager: String;
  requisitionCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLocation: String;
  sysRequisitionStatus: String;
  jobStatusClass: String;
  createdDate: String;
  modifiedDate: String;
  noOfOpening: number;
  selected: boolean;
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}