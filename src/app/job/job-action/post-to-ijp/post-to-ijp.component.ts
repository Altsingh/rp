import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatNativeDateModule } from '@angular/material';

import { JobBasicInfo } from '../job-basic-info';
import { JobActionService } from '../job-action.service';
import { SessionService } from '../../../session.service';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { PostJobToIJP } from "./post-to-ijp-data";
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-post-to-ijp',
  templateUrl: './post-to-ijp.component.html',
  styleUrls: ['./post-to-ijp.component.css']
})
export class PostToIjpComponent implements OnInit {
  lable: string = "post";
  publishCandidate: boolean = false;
  jobData: JobBasicInfo;
  messageCode: MessageCode;
  ijpStartDate: Date;
  ijpEndDate: Date;
  jobCode: String;
  postToIJP: PostJobToIJP;
  postToIJPResponseData: PostToIJPResponse;
  message: String;
  severity: String;
  severityClass: string;
  disableIJPStartDate: boolean;
  landscape = "landscape";
  timer: any;

  constructor(public jobActionService: JobActionService, public dialogRef: MatDialogRef<PostToIjpComponent>,
    private sessionService: SessionService, private notificationService: NotificationService,private i18UtilService:I18UtilService) {
    this.message = "";
    this.jobData = new JobBasicInfo();
    this.postToIJPResponseData = new PostToIJPResponse();
    this.disableIJPStartDate = false;
  }

  ngOnInit() {
    this.jobActionService.getJobDetails().subscribe(res => {
      this.jobData = res.responseData[0],
      this.messageCode = res.messageCode;
      if(this.jobData != null) {
        if (this.jobData.internalJobPosted == true) {
          this.jobCode = this.jobData.requisitionCode;
          this.disableIJPStartDate = true;
          this.lable='update'
          if(this.jobData.ijpStartDate != null) {
            this.ijpStartDate = new Date(this.jobData.ijpStartDate);
          } else{
            this.ijpStartDate = new Date();
          }
          if(this.jobData.ijpEndDate != null) {
            this.ijpEndDate = new Date(this.jobData.ijpEndDate)
          } else{
            this.ijpEndDate = new Date();
            this.ijpEndDate.setDate(this.ijpStartDate.getDate() + 7);
          }
        } else {
          this.disableIJPStartDate = false;
          this.ijpStartDate = new Date();
          this.ijpEndDate = new Date();
          this.ijpEndDate.setDate(this.ijpStartDate.getDate() + 7);
          this.jobCode = this.jobData.requisitionCode;
        }
      }
    });
  }

  postJobToIJP(event) {
    if (this.ijpStartDate != null) {
      this.ijpStartDate = new Date(this.ijpStartDate.toDateString())
    }
    if (this.ijpEndDate != null) {
      this.ijpEndDate = new Date(this.ijpEndDate.toDateString())
    }
    if (this.ijpStartDate > this.ijpEndDate) {
      this.setMessage("WARNING", "common.warning.endDateStartDate");
    } else if (this.jobData.jobStatus == "ONHOLD" || this.jobData.jobStatus === 'PHOLD') {
      this.setMessage("WARNING", "job.warning.failedPostHoldJob");
    } else if (this.jobData.jobStatus === 'CLOSED' || this.jobData.jobStatus === 'PCLOSE') {
      this.setMessage("WARNING", "job.warning.failedPostCloseJob");
    } else if (this.ijpEndDate == null) {
      this.setMessage("WARNING", "common.warning.endDateRequired");
    } else {
      this.publishCandidate = true;
      let postToIJPList: PostJobToIJP[] = [];
      this.postToIJP = new PostJobToIJP();
      this.postToIJP.requisitionID = this.sessionService.object.requisitionId;
      this.postToIJP.ijpStartDate = this.ijpStartDate;
      this.postToIJP.ijpEndDate = this.ijpEndDate;
      this.postToIJP.jobCode = this.jobCode;
      postToIJPList.push(this.postToIJP);
      this.jobActionService.postJobToIJP(postToIJPList).subscribe(out => {
        this.messageCode = out.messageCode;
        this.postToIJPResponseData.result = out.responseData[0];
        if (this.postToIJPResponseData.result == "success") {
          this.i18UtilService.get("postIjp.publish.success").subscribe(res=>{
            this.notificationService.setMessage("INFO", res);
          })
          if (this.disableIJPStartDate) {
            this.dialogRef.close(0);
          } else {
            this.publishCandidate = false;
            this.dialogRef.close(1);
          }
        }
      });
    }
  }

  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe(res=>{
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);
    });
    
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }


  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  selectIjpStartDate(startDate: Date) {
    this.ijpStartDate = startDate;
  }

  selectIjpEndDate(endDate: Date) {
    this.ijpEndDate = endDate;
  }


}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class PostToIJPResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
