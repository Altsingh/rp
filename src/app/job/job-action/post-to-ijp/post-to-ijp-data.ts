export class PostJobToIJP{
  requisitionID: Number;
  ijpStartDate: Date;
  ijpEndDate: Date;
  jobCode: String;
}
