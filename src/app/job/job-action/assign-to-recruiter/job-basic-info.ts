export class JobBasicInfo {
    requisitionCode     :   String;
    jobTitle            :   String;
    requisitionId       :   Number;
    orgUnitCode         :   String;
    joiningLoc          :   String;
    totalOpenings       :   number;
}