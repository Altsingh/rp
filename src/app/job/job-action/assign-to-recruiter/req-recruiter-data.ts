export class AssignReqToRecruiterData{
    requisitionId     : number;
    userId            : Number;
    assignedOpenings  : number;
    assignedDate      : Date;
    userIdStrVal      : string;
    organizationId    : number;
    jobCode           : string;
}