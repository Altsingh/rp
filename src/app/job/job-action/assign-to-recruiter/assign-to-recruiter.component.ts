import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatNativeDateModule } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { JobActionService } from '../job-action.service';
import { JobBasicInfo } from '../job-basic-info';
import { RecruiterDetails } from './recruiter-details';
import { AssignReqToRecruiterData } from './req-recruiter-data';
import { SessionService } from '../../../session.service';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { SelectItem } from '../../../shared/select-item';
import { IOption } from "../../../shared/alt-select/option.interface";
import { DialogService } from '../../../shared/dialog.service';
import { NameinitialService } from '../../../shared/nameinitial.service'
import { isNullOrUndefined } from 'util';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-assign-to-recruiter',
  templateUrl: './assign-to-recruiter.component.html',
  styleUrls: ['./assign-to-recruiter.component.css']
})
export class AssignToRecruiterComponent implements OnInit {
  data: ResponseData[];
  messageCode: MessageCode;
  jobData: JobBasicInfo;
  recruiterList: SelectItem[];
  reqRecruiterData: RequisitionRecruiterData;
  assignReqToRecData: AssignReqToRecruiterData;
  reqRecruiterDataList: AssignReqToRecruiterData[];
  assignedRecruiterList: AssignReqToRecruiterData[];
  remReqFromRecResponse: RemoveReqFromRecResponse;
  message: String;
  severity: String;
  severityClass: string;
  assignRecToRecResponseData: AssignReqToRecResponse;
  remainingOpenings: number;
  totalOpeningsAssigned: number;
  deleteCount: number;
  displayClass: String = "displayNone";
  disableAssign: boolean;
  recruiterIdSelected: Array<Number>;
  timer: any;
  assignRecruiter: boolean = false;
  randomcolor: string[] = [];
  sameRecruiter: string[] = [];
  edited:boolean;
  todayDate:Date;

  constructor(public dialogRef: MatDialogRef<AssignToRecruiterComponent>, public jobActionService: JobActionService,
    private formBuilder: FormBuilder, private sessionService: SessionService,
    private notificationService: NotificationService, private dialogService: DialogService,
    private nameinitialService: NameinitialService, private i18UtilService: I18UtilService) {
    this.jobData = new JobBasicInfo();
    this.message = "";
    this.data = [];
    this.remReqFromRecResponse = new RemoveReqFromRecResponse();
    this.assignRecToRecResponseData = new AssignReqToRecResponse();
    this.totalOpeningsAssigned = 0;
    this.remainingOpenings = 0;
    this.disableAssign = false;
    this.edited=false;
    this.reqRecruiterData = new RequisitionRecruiterData();
    this.randomcolor = nameinitialService.randomcolor;  
    this.deleteCount=0;
    this.todayDate=new Date();
    this.todayDate.setHours(0,0,0,0);
  }

  ngOnInit() {
    this.reqRecruiterDataList = [];
    let jobDetails: any, recruitersOfJob: any, recruiterList: any;

    jobDetails = this.dialogService.getDialogData();
    this.jobData = jobDetails.responseData[0],
    this.messageCode = jobDetails.messageCode;

    recruitersOfJob = this.dialogService.getDialogData1();
    if(recruitersOfJob.responseData != null){
      this.convertdateInResponseData(recruitersOfJob.responseData);  
    }
    this.data = recruitersOfJob.responseData,
    this.messageCode = recruitersOfJob.messageCode;
    recruiterList = this.dialogService.getDialogData2();
    this.recruiterList = recruiterList.responseData,
      this.messageCode = recruiterList.messageCode;
    this.addRow();
  }
  getUserName(index:Number)
  {
    for(let rec of this.recruiterList)
    {
      if(rec.value == index)
      {
        return rec.label;
      }
    }
  }

  convertdateInResponseData(responseData:any){
      for(let data of responseData){
          let dateParts = data.assignedDate.split('/');
          data.assignedDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
          dateParts = data.targetDate.split('/');
          data.targetDate= new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
          data.edit=(data.targetDate<=this.todayDate);
          data.editRecruiter=(data.targetDate<=this.todayDate);
      }
  }

  assignJobToRecruiter() {
    let valid:boolean=true;
    this.reqRecruiterData.input = [];
    this.reqRecruiterData.input2 = [];
    if (this.jobData.jobStatus == "ONHOLD" || this.jobData.jobStatus === 'PHOLD') {
      this.setMessage("WARNING", 'job.warning.failedAssignOnHoldJob');
      this.disableAssign = true;
    } else if (this.jobData.jobStatus === 'CLOSED' || this.jobData.jobStatus === 'PCLOSE') {
      this.setMessage("WARNING", 'job.warning.failedPostCloseJob');
      this.disableAssign = true;
    } else if ((this.reqRecruiterDataList==null || this.reqRecruiterDataList.length == 0) && (this.data==null || this.data.length==0)) {
      this.setMessage("WARNING", 'job.warning.selectRecruiter');
      return;
    }
    else {
      this.disableAssign = false;
      for (let r of this.reqRecruiterDataList) {
        r.userId = parseInt(r.userIdStrVal);
        if (r.userIdStrVal == null || r.userIdStrVal == "") {
          this.setMessage("WARNING", 'job.warning.selectRecruiter');
          return;
        }
        if (this.data !== null) {
          for (let opening of this.data) {
            if (opening.userID == parseInt(r.userIdStrVal)) {

              this.setMessage("WARNING", 'job.warning.recruiterAlreadySelected', {'name': this.getUserName(parseInt(r.userIdStrVal))});
              this.disableAssign = true;
              return;
            }
          }
        }
        if (r.assignedDate == null) {
          this.setMessage("WARNING", 'common.warning.targetDateRequired');
          return;
        }else if(r.assignedDate < this.todayDate) {
          this.setMessage("WARNING", 'common.warning.targetDateCurrentDate');
          return;
        }
        if (r.assignedOpenings == 0) {
          r.assignedOpenings = 0;
          this.setMessage("WARNING", 'job.warning.openingCountNotZero');
          return;
        } else if (r.assignedOpenings > this.jobData.totalOpenings) {
          this.setMessage("WARNING", 'job.warning.assign_totalOpening');
          return;
        }
        else {
          this.disableAssign = false;
          this.clearMessages();
          this.assignReqToRecData = new AssignReqToRecruiterData();
          this.assignReqToRecData.requisitionId = this.sessionService.object.requisitionId;
          this.assignReqToRecData.assignedOpenings = r.assignedOpenings;
          this.assignReqToRecData.assignedDate = r.assignedDate;
          this.assignReqToRecData.userId = r.userId;
          this.assignReqToRecData.jobCode = this.jobData.requisitionCode.toString();
          this.reqRecruiterData.input.push(this.assignReqToRecData);
         
          // this.preparelist();
        }
      }

      if(this.data != null){
        for (let d of this.data) {
          if(d.targetDate < this.todayDate){
            this.setMessage("WARNING", 'job.warning.targetDateExpired', {'name': d.firstName});
            // this.disableAssign = true;
            return;
          }

          if(d.targetDate < d.assignedDate){
            this.setMessage("WARNING", 'job.warning.targetDateWarningFor', {'name': d.firstName});
            // this.disableAssign = true;
            return;
          }

          if (d.assignedOpenings == 0) {
            this.setMessage("WARNING", 'job.warning.openingExceededFor', {'name': d.firstName});
            return;
          } else if (d.assignedOpenings > this.jobData.totalOpenings) {
            this.setMessage("WARNING", 'job.warning.Assign_TotalOpeningFor', {'name': d.firstName});
            return;
          }
          d.requisitionID = this.sessionService.object.requisitionId;
          if(d.deleteRecruiter===true){
            this.deleteCount++;
          }
          // d.targetDateString=d.targetDate.toDateString();
          d.targetDateString=d.targetDate.toLocaleDateString();
          let dateParts = d.targetDateString.split('/');
          d.targetDateString=dateParts[2]+"-"+dateParts[0]+"-"+dateParts[1];
          this.reqRecruiterData.input2.push(d);

          if(d.editRecruiter){
            d.assignedDate=this.todayDate;
          }
        }
      }
    }
    if (this.reqRecruiterData != null) {
      this.disableAssign = true;
      for (let i = 0; i < this.reqRecruiterData.input.length; i++) {
        if (this.reqRecruiterData.input[i + 1] != null) {
          if (this.reqRecruiterData.input[i].userId == this.reqRecruiterData.input[i + 1].userId) {
            this.sameRecruiter[i]=this.getUserName(this.reqRecruiterData.input[i].userId);
            valid=false;
          }
        }
      }
      if(valid==false)
          {
            this.setMessage("WARNING", 'job.warning.recruiterAlreadySelected', {'name': this.sameRecruiter});
            return;
          }
      this.assignRecruiter = true;
      this.jobActionService.assignJobToRecruiter(this.reqRecruiterData).subscribe(out => {
        this.messageCode = out.messageCode;
        this.assignRecToRecResponseData.result = out.responseData[0];
        if (this.assignRecToRecResponseData.result == "success") {
          this.i18UtilService.get('job.success.jobAssigned').subscribe(res=>{
            this.notificationService.setMessage("INFO", res);
          })
          this.dialogRef.close(this.reqRecruiterData.input.length - this.deleteCount);
        }
      });
    }

  }
  public restrictNumeric(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) //for restricting backsapce
    {
      return false;
    }
    if (e.which === 0) //for enabling escape
    {
      return true;
    }
    if (e.which < 33) //for only numeric values
    {
      return true;
    }
    input = String.fromCharCode(e.which);
    if (/[\d\s]/.test(input) == false) {
      this.setMessage("WARNING", 'job.warning.openingNumeric');
    }
    return !!/[\d\s]/.test(input);

  }

  public deleteRow(index: number) {
    this.disableAssign = false;
    if (index != -1) {
      this.reqRecruiterDataList.splice(index, 1);
    }
    if (this.reqRecruiterDataList == null) {
      this.disableAssign = true;
    }
  }

   
  public deleteRecruiter(index: number) {
    this.disableAssign = false;
    if (index != -1) {
      this.data[index].deleteRecruiter = true;
    }
    if (this.reqRecruiterDataList == null) {
      this.disableAssign = true;
    }
  }

  public undeleteRecruiter(index: number) {
    this.disableAssign = false;
    if (index != -1) {
      this.data[index].deleteRecruiter = false;
    }
    if (this.reqRecruiterDataList == null) {
      this.disableAssign = true;
    }
  }
  
  public editRow(index: number) {
    this.disableAssign = false;
    if (index != -1) {
        this.data[index].edit=true;
    }
    if (this.reqRecruiterDataList == null) {
      this.disableAssign = true;
    }
  }

  
  public updateRow(index: number) {
    this.disableAssign = false;
    if (index != -1) {
      this.data[index].edit=false;
      this.data[index].editRecruiter = true;
    }
    if (this.reqRecruiterDataList == null) {
      this.disableAssign = true;
    }
  }

  public addRow() {
    this.reqRecruiterData = new RequisitionRecruiterData();
    this.assignReqToRecData = new AssignReqToRecruiterData();
    this.assignReqToRecData.assignedOpenings = 1;
    this.assignReqToRecData.assignedDate = new Date();
    this.assignReqToRecData.userId = 0;
    this.assignReqToRecData.userIdStrVal = "";
    this.reqRecruiterDataList.push(this.assignReqToRecData);
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  public setMessage(severity: string, message: string, args? : any) {
    
    
    if(isNullOrUndefined(args)){
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
    this.message = res;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
      });
      
    }
    else{
      this.i18UtilService.get(message,args).subscribe((res: string) => {
        this.severity = severity;
    this.message = res;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
      });

    }
  }

  changeRecruiter(event, index) {
    if (this.data !== null) {
      for (let opening of this.data) {
        if (opening.userID == parseInt(this.reqRecruiterDataList[index].userIdStrVal)) {
          this.setMessage("WARNING",  'job.warning.recruiterAlreadyAssigned', {'recruiter': this.getUserName(parseInt(this.reqRecruiterDataList[index].userIdStrVal))});
          this.disableAssign = true;
          return;
        } else {
          this.disableAssign = false;
          this.clearMessages();
        }
      }
    }
    let reqRecruiterList: any[] = [];
    for (let recruiter of this.reqRecruiterDataList) {
      reqRecruiterList.push(recruiter.userIdStrVal);
    }
    reqRecruiterList.splice(index, 1);
   
    for (let i = 0; i < (reqRecruiterList.length); i++) {
      if (event.value == parseInt(reqRecruiterList[i])) {
        this.setMessage("WARNING", 'job.warning.recruiterAlreadySelected', {'name': this.getUserName(reqRecruiterList[i])});
        this.disableAssign = true;
        return;
      } else {
        this.disableAssign = false;
        this.clearMessages();
      }

    }
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  selectAssignedDate(rec: ResponseData) {

  }


}

export interface ResponseData {
  firstName: String;
  middleName: String;
  lastName: String;
  remainingOpenings: number;
  assignedOpenings: number;
  jobCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLoc: String;
  assignedDate: Date;
  targetDate: Date;
  targetDateString: String;
  requisitionID: number;
  requisitionRecruiterID: number;
  userID: number;
  profileImage: string;
  edit : boolean;
  editRecruiter: boolean;
  deleteRecruiter: boolean;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class RequisitionRecruiterData {
  input: AssignReqToRecruiterData[];
  input2: any[];
}

export class RemoveReqFromRecResponse {
  result: boolean;
}
export class AssignReqToRecResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
