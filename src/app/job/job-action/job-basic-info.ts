export class JobBasicInfo {
    requisitionCode     :   String;
    jobTitle            :   String;
    requisitionId       :   Number;
    orgUnitCode         :   String;
    joiningLoc          :   String;
    totalOpenings       :   number;
    createdDate         :   String;
    jobStatus           :   String;
    internalJobPosted   :   Boolean;
    ijpStartDate        :   Date;
    ijpEndDate          :   Date;
    empRefPosted        :   Boolean;
    empRefStartDate     :   Date;
    empRefEndDate       :   Date;
    cpPosted            :   Boolean;
    cpStartDate         :   Date;
    cpEndDate           :   Date;
    referralScheme           :   String;
}