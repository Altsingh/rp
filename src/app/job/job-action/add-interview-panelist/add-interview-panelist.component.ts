import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { SessionService } from '../../../session.service';
import { SelectItem } from '../../../shared/select-item';
import { JobActionService } from '../job-action.service';
import { JobBasicInfo } from '../job-basic-info';
import { isNullOrUndefined } from 'util';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-add-interview-panelist',
  templateUrl: './add-interview-panelist.component.html',
  styleUrls: ['./add-interview-panelist.component.css']
})
export class AddInterviewPanelistComponent implements OnInit {
  jobData: JobBasicInfo;
  data = [];
  jobSubscription;
  interviewPanelist: SelectItem[];
  message: String;
  severity: String;
  severityClass: string;
  remainingOpenings: number;
  totalOpeningsAssigned: number;
  displayClass: String = "displayNone";
  disableAssign: boolean;
  assignInterviewer:boolean
  selection = [];
  formGroup: FormGroup;
  requisitionId;

  constructor(public dialogRef: MatDialogRef<AddInterviewPanelistComponent>, public jobActionService: JobActionService,
    private formBuilder: FormBuilder, private sessionService: SessionService,
    private notificationService: NotificationService, private i18UtilService: I18UtilService) {
    this.jobData = new JobBasicInfo();
    this.message = "";
    this.totalOpeningsAssigned = 0;
    this.remainingOpenings = 0;
    this.disableAssign = false;
    this.formGroup = this.formBuilder.group({
      interviewPanelistSelected: []
    });
  }

  ngOnInit() {
    this.assignInterviewer=false;
    this.requisitionId = this.sessionService.object.requisitionId;
    this.jobSubscription = this.jobActionService.getJobDetails().subscribe(res => {
      this.jobData = res.responseData[0]
    });

    this.jobActionService.getInterviewPanel().subscribe((out) => {
      this.interviewPanelist = [];
      let responseList = out.responseData;
      let currentList = this.jobActionService.getCurrentInterviewPanel();
      if (currentList != null && currentList.length > 0) {
        let removeCount = 0;
        for (let x = 0; x < responseList.length; x++) {
          let present = false;
          for (let y = 0; y < currentList.length; y++) {
            if (currentList[y].userId == responseList[x].value) {
              present = true;
              removeCount++;
              break;
            }
          }
          if (!present) {
            this.interviewPanelist.push(responseList[x]);
          }
        }
      } else {
        this.interviewPanelist = responseList;
      }

    });
  }

  onSelectInterviewer(event) {
    let currentInterviewPanel = this.jobActionService.getCurrentInterviewPanel();
    if (currentInterviewPanel != null && currentInterviewPanel.length > 0) {
      for (let x = 0; x < currentInterviewPanel.length; x++) {
        if (currentInterviewPanel[x].userId == event.value) {
          this.setMessage("WARNING", 'job.warning.interviewerAlreadyExist');
          return;
        }
      }
    }
    this.selection.push(event);
  }
  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  public setMessage(severity: string, message: string, args?: any) {

    if(isNullOrUndefined(args)){
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
      });
    }else{
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
      });

    }
   
  }

  addInterviewPanel() {
    this.assignInterviewer=true;
    if (this.selection.length == 0) {
      this.setMessage("WARNING", 'job.warning.selectInterviewer');
      this.assignInterviewer=false;
      return;
    }
    let selection = this.formGroup.controls.interviewPanelistSelected;
    let requestJSON = [];

    for (let x = 0; x < selection.value.length; x++) {
      let userName = this.getUserNameFromSelection(selection.value[x]);

      if (userName != null) {
        requestJSON.push({
          userId: selection.value[x],
          requisitionId: this.requisitionId,
          userName: userName
        });
      }
    }

    this.jobActionService.addInterviewPanel(requestJSON).subscribe(response => {
      let saved = false;
      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            if (response.responseData[0] == true) {
              saved = true;
              this.i18UtilService.get('job.success.interviewerAdded').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.INFO, res);
              });
              this.dialogRef.close();
              this.assignInterviewer=false;
            }
          }
        }
      }
      if (!saved) {
        this.setMessage(NotificationSeverity.WARNING, 'job.error.interviewerAddFail');
        this.assignInterviewer=false;
      }
    },
      () => {
        this.setMessage(NotificationSeverity.WARNING, 'job.error.interviewerAddFail');
        this.assignInterviewer=false;
      },
      () => {
        this.assignInterviewer=false;
      });
  }

  getUserNameFromSelection(id) {
    for (let x = 0; x < this.selection.length; x++) {
      if (this.selection[x].value == id) {
        return this.selection[x].label;
      }
    }
    return null;
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED') {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}