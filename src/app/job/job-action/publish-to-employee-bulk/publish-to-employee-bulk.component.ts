import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../common/notification-bar/notification.service";
import { MatDialogRef } from '@angular/material';
import { JobActionService } from '../job-action.service';
import { SessionService } from '../../../session.service';
import { PostJobToEmpPortal } from "../post-to-emp-ref/post-to-emp-ref-data";
import { DatePipe } from '@angular/common';
import { I18UtilService } from 'app/shared/services/i18-util.service';
@Component({
  selector: 'alt-publish-to-employee-bulk',
  templateUrl: './publish-to-employee-bulk.component.html',
  styleUrls: ['./publish-to-employee-bulk.component.css'],
  providers:[DatePipe]
})
export class PublishToEmployeeBulkComponent implements OnInit {
  publishEmployee:boolean=false;
  lable:string;
  datalist: JobListResponse[] = [];
  messageCode: MessageCode;
  empRefStartDate: Date;
  empRefEndDate: Date;
  postToEmpPortal: PostJobToEmpPortal;
  postToEmpPortalResponseData: PostToEmpPortalResponse;
  severity: String;
  severityClass: string;
  landscape = "landscape";
  message: string;
  count: number;
  timer: any;
  jobhold: string;
  posted: boolean = false;
  currentDate : Date;

  constructor(public dialogRef: MatDialogRef<PublishToEmployeeBulkComponent>, private jobActionService: JobActionService,
    private sessionService: SessionService, private notificationService: NotificationService,
    private datePipe:DatePipe, private i18UtilService: I18UtilService) {
    this.postToEmpPortalResponseData = new PostToEmpPortalResponse();
    this.count = 0;
    this.message = "";
    this.currentDate = new Date(Date.now());
  }

  ngOnInit() {
    this.datalist = this.jobActionService.getDataList();
    if (Object.keys(this.datalist).length != 0) {
      this.count = this.datalist.length;
    }
    for (let i of this.datalist) {
      if (i.postedOnEmpPortal == true) {
        this.posted = true;
        break;
      }
      this.i18UtilService.get('common.label.post').subscribe((res: string) => {
        this.lable = res;
       });
    }

    if (this.posted == false) {
      this.empRefStartDate = new Date();
      this.empRefEndDate = new Date();
      this.empRefEndDate.setDate(this.empRefStartDate.getDate() + 7);
    } else { this.empRefStartDate = null }
  }

  postMultipleJobsToEmpPortal(event) {
    this.notificationService.clear();
    let reqidlist: any = [];
    for (let requisition of this.datalist) {
      reqidlist.push(requisition.requisitionId);
    }
    let reqCodelist: any = [];
    for (let req of this.datalist) {
      reqCodelist.push(req.requisitionCode);
    }
    if (this.empRefEndDate == null) {
      this.setMessage("WARNING", 'common.warning.endDateRequired');
      return;
    }
    let sDate=Date.parse(this.datePipe.transform(this.empRefStartDate));
    let eDate=Date.parse(this.datePipe.transform(this.empRefEndDate));
    if (sDate > eDate) {
      this.setMessage("WARNING", 'employeeRefreal.warning.EREnd_StartDate');
      return;
    }
    if (this.jobhold == "HOLD") {
      this.setMessage("WARNING", 'job.warning.removeOnHoldJob');
      return;
    }
    if (this.jobhold == "CLOSED") {
      this.setMessage("WARNING", 'job.warning.removeClosedJob');
      return;
    }
    this.publishEmployee=true;
    let postToEmpPortal: PostJobToEmpPortal;
    let postToEmpPortalList: PostJobToEmpPortal[] = [];
    for(let req of this.datalist){
      postToEmpPortal = new PostJobToEmpPortal();
      postToEmpPortal.requisitionID = req.requisitionId;
      if(req.postedOnEmpPortal == true){
        postToEmpPortal.empRefStartDate = new Date(req.empPortalStartDate);
      }else{
        postToEmpPortal.empRefStartDate = this.empRefStartDate;
        if(this.empRefStartDate == null){
          postToEmpPortal.empRefStartDate = new Date();
        }
      }
      postToEmpPortal.empRefEndDate = this.empRefEndDate;
      postToEmpPortal.jobCode = req.requisitionCode;
      postToEmpPortalList.push(postToEmpPortal);
    }
    this.jobActionService.postJobToEmpPortal(postToEmpPortalList).subscribe(out => {
      this.messageCode = out.messageCode;
      this.postToEmpPortalResponseData.result = out.responseData[0];
      if (this.postToEmpPortalResponseData.result == "success") {
        this.dialogRef.close("success");
        this.i18UtilService.get('employeeRefreal.publish.success').subscribe((res: string) => {
          this.notificationService.setMessage("INFO", res);
         });
        this.jobActionService.getJobComponent().ngOnInit();
      }
    });
  }
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      this.jobhold = "CLOSED";
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      this.jobhold = "HOLD";
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
    this.message = message;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
     });
   
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  selectEmpRefStartDate(empRefStartDate: Date) {
    this.empRefStartDate = empRefStartDate;
  }

  selectEmpRefEndDate(empRefEndDate: Date) {
    this.empRefEndDate = empRefEndDate;
  }

}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class JobListResponse {
  draftRequisitionID: Number;
  requisitionId: Number;
  hiringManager: String;
  requisitionCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLocation: String;
  sysRequisitionStatus: String;
  jobStatusClass: String;
  createdDate: Date;
  modifiedDate: String;
  noOfOpening: number;
  selected: boolean;
  postedOnEmpPortal: boolean;
  empPortalStartDate: Date;
  disableEmpRefStartDate : boolean;
}

export class PostToEmpPortalResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
