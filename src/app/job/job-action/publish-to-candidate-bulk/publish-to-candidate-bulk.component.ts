import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../common/notification-bar/notification.service";
import { MatDialogRef } from '@angular/material';
import { JobActionService } from '../job-action.service'
import { DatePipe } from '@angular/common';
import { PostJobToCandPortal } from '../post-to-candidate-portal/post-to-candidateportal-data';
import { SessionService } from "../../../session.service";
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-publish-to-candidate-bulk',
  templateUrl: './publish-to-candidate-bulk.component.html',
  styleUrls: ['./publish-to-candidate-bulk.component.css'],
  providers: [DatePipe]
})
export class PublishToCandidateBulkComponent implements OnInit {
  publishCandidate:boolean=false;
  lable: string;
  datalist: JobListResponse[] = [];
  messageCode: MessageCode;
  cpStartDate: Date;
  cpEndDate: Date;
  postToCandPortal: PostJobToCandPortal;
  postToCandPortalResponseData: PostToCandPortalResponse;
  message: String;
  severity: String;
  severityClass: string;
  landscape = "landscape";
  count: number;
  jobhold: string;
  timer: any;
  posted: boolean = false;
  currentDate : Date;

  constructor(public dialogRef: MatDialogRef<PublishToCandidateBulkComponent>, private jobActionService: JobActionService,
    private sessionService: SessionService, private notificationService: NotificationService,
    private datePipe: DatePipe, private i18UtilService: I18UtilService) {
    this.postToCandPortalResponseData = new PostToCandPortalResponse();
    this.count = 0;
    this.message = "";
    this.currentDate = new Date(Date.now());
  }

  ngOnInit() {
    this.datalist = this.jobActionService.getDataList();
    if (Object.keys(this.datalist).length != 0) {
      this.count = this.datalist.length;
    }
    for (let i of this.datalist) {
      if (i.postedOnCP == true) {
        this.posted = true;
        break;
      }
    }
    if (this.posted == false) {
      this.cpStartDate = new Date();
      this.cpEndDate = new Date();
      this.cpEndDate.setDate(this.cpStartDate.getDate() + 7);
    } else { this.cpStartDate = null }
    this.i18UtilService.get('common.label.post').subscribe((res: string) => {
      this.lable = res;
     });
  }

  postMultipleJobsToCandPortal(event) {
    this.notificationService.clear();
    let reqidlist: any = [];
    for (let requisition of this.datalist) {
      reqidlist.push(requisition.requisitionId);
    }
    let reqCodelist: any = [];
    for (let req of this.datalist) {
      reqCodelist.push(req.requisitionCode);
    }
    if (this.cpStartDate > this.cpEndDate) {
      let sDate = Date.parse(this.datePipe.transform(this.cpStartDate));
      let eDate = Date.parse(this.datePipe.transform(this.cpEndDate));
      if (sDate > eDate) {
        this.setMessage("WARNING", 'postCP.warning.CPEnd_StartDate');
      }
    } else if (this.cpEndDate == null) {
      this.setMessage("WARNING", 'common.warning.endDateRequired');
    } else if (this.jobhold == "HOLD") {
      this.setMessage("WARNING", 'job.warning.removeOnHoldJob');
    } else if (this.jobhold == "CLOSED") {
      this.setMessage("WARNING", 'job.warning.removeClosedJob'); 
    }
    else {
      this.publishCandidate=true;
      let postToCandPortal: PostJobToCandPortal;
      let postToCandPortalList: PostJobToCandPortal[] = [];
      for (let req of this.datalist) {
        postToCandPortal = new PostJobToCandPortal();
        postToCandPortal.requisitionID = req.requisitionId;
        if (req.postedOnCP == true) {
          postToCandPortal.cpStartDate = new Date(req.cpStartDate);
        } else {
          postToCandPortal.cpStartDate = this.cpStartDate;
          if (this.cpStartDate == null) {
            postToCandPortal.cpStartDate = new Date();
          }
        }
        postToCandPortal.cpEndDate = this.cpEndDate;
        postToCandPortal.jobCode = req.requisitionCode;
        postToCandPortalList.push(postToCandPortal);
      }
      this.jobActionService.postJobToCandPortal(postToCandPortalList).subscribe(out => {
        this.messageCode = out.messageCode;
        this.postToCandPortalResponseData.result = out.responseData[0];
        if (this.postToCandPortalResponseData.result == "success") {
          this.dialogRef.close("success");
          this.i18UtilService.get('postCP.publish.success').subscribe((res: string) => {
            this.notificationService.setMessage("INFO", res);
          });
          this.jobActionService.getJobComponent().ngOnInit();
        }
      });
    }
  }
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      this.jobhold = "CLOSED";
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      this.jobhold = "HOLD";
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }
  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);
    });
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  selectCpStartDate(cpStartDate: Date) {
    this.cpStartDate = cpStartDate;
  }

  selectCpEndDate(cpEndDate: Date) {
    this.cpEndDate = cpEndDate;
  }

}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class JobListResponse {
  draftRequisitionID: Number;
  requisitionId: Number;
  hiringManager: String;
  requisitionCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLocation: String;
  sysRequisitionStatus: String;
  jobStatusClass: String;
  createdDate: String;
  modifiedDate: String;
  noOfOpening: number;
  selected: boolean;
  postedOnCP: boolean;
  cpStartDate: Date;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class PostToCandPortalResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}

