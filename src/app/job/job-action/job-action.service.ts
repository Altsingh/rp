import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from '../../session.service';
import { Observable } from 'rxjs/Rx';
import { AssignReqToRecruiterData } from './assign-to-recruiter/req-recruiter-data';
import { AssignReqToVendorDatabulk } from './assign-to-vendors-bulk/req-vendor-data-bulk';
import { InterviewPanelService } from '../new-job/interview-panel.service';
import { JobListComponent } from '../job-list/job-list.component'
import { CandidateListComponent } from '../job-detail/tagged-candidates/candidate-list/candidate-list.component';
import { CustomHttpService } from 'app/shared/services/custom-http-service.service';

@Injectable()
export class JobActionService {
    isUntag = false;
    jobListComponent: JobListComponent;
    candidateListComponent: CandidateListComponent;
    datalist: any[];
    setDataList(list) {
        
        this.datalist = list
    }
    getDataList() {
        return this.datalist;
    }
    interviewPanel;

    constructor(private http: Http, private sessionService: SessionService,
        private interviewPanelService: InterviewPanelService,private httpService:CustomHttpService) { }

    public getRecruitersOfJob() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/recruitersAssignedToJob/'
            + this.sessionService.object.requisitionId + '/' + this.sessionService.organizationID +'/').
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public getRecruitersOfJobToPromise(): Promise<any> {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/recruitersAssignedToJob/'
            + this.sessionService.object.requisitionId + '/' + this.sessionService.organizationID + "/").
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            }).toPromise();
    }

    public getJobDetails() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/jobBasicInfo/'
            + this.sessionService.object.requisitionId + "/").map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public getJobDetailtoPromise(): Promise<any> {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/jobBasicInfo/'
            + this.sessionService.object.requisitionId + "/").map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            }).toPromise();
    }

    public getRecruiterList() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/recruitersList/'
            + this.sessionService.organizationID + "/").map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public getRecruiterListtoPromise(): Promise<any> {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/recruitersList/'
            + this.sessionService.organizationID + "/").map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            }).toPromise();
    }


    getInterviewPanel(): Observable<any> {
        return this.interviewPanelService.getData();
    }
    addInterviewPanel(requestJSON) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/interViewPanel/add/' + this.sessionService.organizationID + "/", requestJSON, options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    setCurrentInterviewPanel(interviewPanel) {
        this.interviewPanel = interviewPanel;
    }
    getCurrentInterviewPanel() {
        return this.interviewPanel;
    }
    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }

    getURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobs/assignJobToRecruiter');
    }

    public assignJobToRecruiter(reqRecruiterData: RequisitionRecruiterData) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getURL()+"/", JSON.stringify(reqRecruiterData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public getVendorsForOrganization() {
        return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/vendorList/'
            + this.sessionService.organizationID + "/").map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    public assignJobToVendorbulk(reqVendorData: RequisitionVendorDatabulk[]) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/assignMultipleJobsToVendor/', JSON.stringify(reqVendorData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getAssignToVendorURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobs/assignJobToVendor');
    }

    public assignJobToVendor(reqVendorData: RequisitionVendorData) {
        return this.httpService.post(this.getAssignToVendorURL() + "/", JSON.stringify(reqVendorData));
    }

    getUnassignFromRecURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobs/unassignJobFromRecruiter');
    }

    public unassignJobFromRecruiter(requisitionRecruiterId: Number) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getUnassignFromRecURL() + "/", JSON.stringify(requisitionRecruiterId), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getPostToIJPURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobs/postJobToIJP');
    }

    public postJobToIJP(postToIJPData: PostJobToIJP[]) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getPostToIJPURL() + "/", JSON.stringify(postToIJPData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getPostToEmpPortalURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobs/postJobToEmployeeRefferal');
    }

    public postJobToEmpPortal(postToEmpPortalData: PostJobToEmpPortal[]) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getPostToEmpPortalURL() + "/", JSON.stringify(postToEmpPortalData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getPostToCandPortalURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobs/postJobToCandidatePortal');
    }

    public postJobToCandPortal(postToCandPortalData: PostJobToCandPortal[]) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getPostToCandPortalURL() + "/", JSON.stringify(postToCandPortalData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }

    getAssignMultipleJobsToRecURL() {
        return (this.sessionService.orgUrl + '/rest/altone/jobDetail/assignMultipleJobsToRecruiter');
    }

    public assignMultipleJobsToRecruiter(reqRecruiterDataList: RequisitionRecruiterDatabulk[]) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getAssignMultipleJobsToRecURL() + "/", JSON.stringify(reqRecruiterDataList), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public setJobComponent(jobComponent: JobListComponent) {
        this.jobListComponent = jobComponent;
    }

    public getJobComponent(): JobListComponent {
        return this.jobListComponent;
    }
    public setcandidateComponent(candidateComponent: CandidateListComponent) {
        this.candidateListComponent = candidateComponent;
    }

    public getcandidateComponent(): CandidateListComponent {
        return this.candidateListComponent;
    }

}

export class RequisitionRecruiterData {
    input: AssignReqToRecruiterData[];
}

export class RequisitionVendorData {
    input: any[];
}
export class RequisitionVendorDatabulk {
    vendorId: Number;
    requisitionIdList: Array<Number>;
    jobCodeList: String[];
    assignedOpenings: number;
    targetDate: Date;
    vendorIdStrVal: String;
}
export class PostJobToIJP {
    requisitionID: Number;
    ijpStartDate: Date;
    ijpEndDate: Date;
}

export class PostJobToEmpPortal {
    requisitionID: Number;
    empRefStartDate: Date;
    empRefEndDate: Date;
    referralScheme: String;
    jobCode: String;
}

export class PostJobToCandPortal {
    requisitionID: Number;
    cpStartDate: Date;
    cpEndDate: Date;
}

export class RequisitionRecruiterDatabulk {
    reqIdList: Array<Number>;
    userId: Number;
    assignedOpenings: number;
    targetDate: Date;
    userIdStrVal: string;
}

