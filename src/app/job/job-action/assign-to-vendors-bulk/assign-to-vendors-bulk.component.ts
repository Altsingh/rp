import { Component, OnInit, Input } from '@angular/core';
import { JobActionService } from '../job-action.service'
import { SelectItem } from '../../../shared/select-item';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AssignReqToVendorDatabulk } from '../../job-action/assign-to-vendors-bulk/req-vendor-data-bulk';
import { SessionService } from '../../../session.service';
import { NotificationService } from "../../../common/notification-bar/notification.service";
import { MatDialogRef } from '@angular/material';
import { DialogService } from '../../../shared/dialog.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-assign-to-vendors',
  templateUrl: './assign-to-vendors-bulk.component.html',
  styleUrls: ['./assign-to-vendors-bulk.component.css']
})
export class AssignToVendorsComponent implements OnInit {
  assignVendor: boolean = false;
  lable: string;
  datalist: JobListResponse[] = [];
  vendorList: SelectItem[];
  messageCode: MessageCode;
  reqVendorResponseData: RequisitionVendorResponseData;
  jobhold: string;
  timer: any;
  message: String;
  severity: String;
  severityClass: string;
  reqVendorData: RequisitionVendorData;
  reqVendorDataList: RequisitionVendorData[];
  minOpening: number = 0;
  disableAssign: boolean;

  constructor(public dialogRef: MatDialogRef<AssignToVendorsComponent>, private jobActionService: JobActionService, private formBuilder: FormBuilder,
    private sessionService: SessionService, private notificationService: NotificationService, private dialogService: DialogService, private i18UtilService: I18UtilService) {
    this.reqVendorResponseData = new RequisitionVendorResponseData();
    this.message = "";
  }

  ngOnInit() {
    this.reqVendorDataList = [];
    this.datalist = this.jobActionService.getDataList();
    let res = this.dialogService.getDialogData();
    this.vendorList = res.responseData,
      this.messageCode = res.messageCode;

    let reqidlist: any[] = [];
    for (let requisitionId of this.datalist) {
      if (requisitionId) {
        reqidlist.push(requisitionId.noOfOpening);
      }
    }
    this.minOpening = Math.min.apply(null, reqidlist);
    this.addRow();
    this.i18UtilService.get('common.label.assign').subscribe((res: string) => {
     this.lable = res;
    });

  }
  public assignJobToVendor(event) {
    this.notificationService.clear();
    let reqidlist: any[] = [];
    for (let requisitionId of this.datalist) {
      reqidlist.push(requisitionId.requisitionId);
    }
    let jobCodes: any[] = [];
    for (let req of this.datalist) {
      jobCodes.push(req.requisitionCode);
    }
    if (this.jobhold == "HOLD") {
      this.i18UtilService.get('job.warning.removeOnHoldJob').subscribe((res: string) => {
        this.setMessage("WARNING", res);
      });
      return;
    } else if (this.jobhold == "CLOSED") {
      this.i18UtilService.get('job.warning.removeClosedJob').subscribe((res: string) => {
        this.setMessage("WARNING", res);
      });
      return;
    }
    let assignReqToVendor: RequisitionVendorData;
    let assignReqToVendorList: RequisitionVendorData[] = [];
    for (let v of this.reqVendorDataList) {
      v.vendorId = parseInt(v.vendorIdStrVal);
      let openings : number = Number(v.assignedOpenings);
      if (v.vendorIdStrVal == null || v.vendorIdStrVal.length == 0) {
        this.i18UtilService.get('job.warning.selectVendorForAssign').subscribe((res: string) => {
          this.setMessage("WARNING", res);
        });
        return;
      }
      if(Number.isInteger(openings)){
        if (openings <= 0) {
          this.i18UtilService.get('job.warning.openingCountNotZero').subscribe((res: string) => {
            this.setMessage("WARNING", res);
          });
          return;
        }
      } else{
        this.i18UtilService.get('job.warning.openingCountInvalid').subscribe((res: string) => {
          this.setMessage("WARNING", res);
        });
          return;
      }
      if (v.assignedOpenings > this.minOpening) {
        this.i18UtilService.get('job.warning.AssignMinimumOpeningCheck').subscribe((res: string) => {
          this.setMessage("WARNING", res);
        });
        return;
      }
      assignReqToVendor = new RequisitionVendorData();
      assignReqToVendor.requisitionIdList = [];
      assignReqToVendor.jobCodeList = [];
      assignReqToVendor.requisitionIdList = reqidlist;
      assignReqToVendor.jobCodeList = jobCodes;
      assignReqToVendor.vendorId = v.vendorId;
      assignReqToVendor.targetDate = v.targetDate;
      assignReqToVendor.assignedOpenings = v.assignedOpenings;
      assignReqToVendorList.push(assignReqToVendor);
    }
    if (assignReqToVendorList != null) {
      this.assignVendor = true;
      this.disableAssign = true;
      this.jobActionService.assignJobToVendorbulk(assignReqToVendorList).subscribe(out => {
        this.messageCode = out.messageCode,
          this.reqVendorResponseData.result = out.responseData[0];
        if (this.reqVendorResponseData.result == "success") {
          this.dialogRef.close("success");
          this.i18UtilService.get('job.success.requisitionAssignedToVendors').subscribe((res: string) => {
            this.notificationService.setMessage("INFO", res);
          });
          this.jobActionService.getJobComponent().ngOnInit();
        } else {
          this.assignVendor = false;
          this.setMessage("WARNING", this.reqVendorResponseData.result);
        }
      });
    }
  }
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      this.jobhold = "CLOSED";
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      this.jobhold = "HOLD";
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  public setMessage(severity: string, message: string) {
    this.severity = severity;
    this.message = message;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  selectTargetDate(targetDate: Date) {

  }

  public deleteRow(index: number) {
    if (index != -1) {
      this.reqVendorDataList.splice(index, 1);
    }
  }

  public addRow() {
    this.reqVendorData = new RequisitionVendorData();
    this.reqVendorData.assignedOpenings = 0;
    this.reqVendorData.targetDate = new Date();
    this.reqVendorData.vendorId = 0;
    this.reqVendorData.vendorIdStrVal = "";
    this.reqVendorDataList.push(this.reqVendorData);
  }

  changeVendor(event, index) {
    let reqVendorList: any[] = [];
    for (let v of this.reqVendorDataList) {
      reqVendorList.push(v.vendorIdStrVal);
    }
    reqVendorList.splice(index, 1);
    for (let i = 0; i < (reqVendorList.length); i++) {
      if (event.value == parseInt(reqVendorList[i])) {
        this.i18UtilService.get('job.warning.userAlreadySelected').subscribe((res: string) => {
          this.setMessage("WARNING", res);
        });
        this.disableAssign = true;
        return;
      } else {
        this.disableAssign = false;
        this.clearMessages();
      }

    }

  }

}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class JobListResponse {
  draftRequisitionID: Number;
  requisitionId: Number;
  hiringManager: String;
  requisitionCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLocation: String;
  sysRequisitionStatus: String;
  jobStatusClass: String;
  createdDate: String;
  modifiedDate: String;
  noOfOpening: number;
  selected: boolean;
}
export class RequisitionVendorData {
  vendorId: Number;
  requisitionIdList: Array<Number>;
  jobCodeList: String[];
  assignedOpenings: number;
  targetDate: Date;
  vendorIdStrVal: string;
}

export class RequisitionVendorResponseData {
  result: string;
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}