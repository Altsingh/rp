export class PostJobToEmpPortal{
  requisitionID: Number;
  empRefStartDate: Date;
  empRefEndDate: Date;
  jobCode: String;
  referralScheme: String;
}