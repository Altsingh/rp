import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatNativeDateModule } from '@angular/material';

import { JobBasicInfo } from '../job-basic-info';
import { JobActionService } from '../job-action.service';
import { SessionService } from '../../../session.service';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { PostJobToEmpPortal } from './post-to-emp-ref-data';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-post-to-emp-ref',
  templateUrl: './post-to-emp-ref.component.html',
  styleUrls: ['./post-to-emp-ref.component.css']
})
export class PostToEmpRefComponent implements OnInit {
  lable:string='post';
  publishEmployee:boolean=false;  
  jobData : JobBasicInfo;
  messageCode : MessageCode;
  empRefStartDate : Date;
  empRefEndDate : Date;
  referralScheme : String;
  postToEmpPortal : PostJobToEmpPortal;
  postToEmpPortalResponseData : PostToEmpPortalResponse;
  message: String;
  severity: String;
  severityClass: string;
  timer: any;
  disableEmpRefStartDate: boolean;
  landscape = 'landscape';

  constructor(public jobActionService:JobActionService, public dialogRef:MatDialogRef<PostToEmpRefComponent>,
            private sessionService:SessionService, private notificationService: NotificationService,private i18UtilService:I18UtilService) {
      this.message='';
      this.referralScheme='';
      this.jobData = new JobBasicInfo();
      this.disableEmpRefStartDate = false;
      this.postToEmpPortalResponseData = new PostToEmpPortalResponse();
  }

  ngOnInit() {
    this.empRefStartDate = new Date();
    this.empRefEndDate = new Date();
     this.jobActionService.getJobDetails().subscribe(res => {
        this.jobData = res.responseData[0];
        this.messageCode = res.messageCode;
        if(this.jobData != null) {
          if(this.jobData.empRefPosted){
            this.disableEmpRefStartDate = true;
            this.lable = 'Update';
            if(this.jobData.empRefStartDate != null) {
              this.empRefStartDate = new Date(this.jobData.empRefStartDate); 
            }
            if(this.jobData.empRefEndDate != null) {
              this.empRefEndDate = new Date(this.jobData.empRefEndDate)
            }
            if(this.jobData.referralScheme != null) {
              this.referralScheme = this.jobData.referralScheme;
            }
          } else {
            this.empRefStartDate = new Date();
            this.empRefEndDate = new Date();
            this.empRefEndDate.setDate(this.empRefStartDate.getDate() + 7);
            this.referralScheme = '';
          }
        }
     });
  }

  postJobToEmpPortal(event) {
    if (this.empRefStartDate != null) {
      this.empRefStartDate = new Date(this.empRefStartDate.toDateString())
    }
    if (this.empRefEndDate != null) {
      this.empRefEndDate = new Date(this.empRefEndDate.toDateString())
    }
    if(this.empRefStartDate > this.empRefEndDate){
        this.setMessage('WARNING','common.warning.endDateStartDate');
    } else if(this.jobData.jobStatus == 'ONHOLD' || this.jobData.jobStatus === 'PHOLD'){
       this.setMessage('WARNING','job.warning.failedPostHoldJob');
    } else if(this.jobData.jobStatus === 'CLOSED' || this.jobData.jobStatus === 'PCLOSE'){
       this.setMessage('WARNING','job.warning.failedPostCloseJob');
    } else if(this.empRefEndDate == null){
      this.setMessage('WARNING','common.warning.endDateRequired');
    } 
    
    else {
      this.publishEmployee=true;
      let postToEmpPortalList: PostJobToEmpPortal[] = [];
      this.postToEmpPortal = new PostJobToEmpPortal();
      this.postToEmpPortal.requisitionID = this.sessionService.object.requisitionId;
      this.postToEmpPortal.empRefStartDate = this.empRefStartDate;
      this.postToEmpPortal.empRefEndDate = this.empRefEndDate;
      this.postToEmpPortal.referralScheme = this.referralScheme;
      this.postToEmpPortal.jobCode = this.jobData.requisitionCode;
      if (this.disableEmpRefStartDate) {
        this.postToEmpPortal.empRefStartDate = new Date();
      }
      postToEmpPortalList.push(this.postToEmpPortal);
      this.jobActionService.postJobToEmpPortal(postToEmpPortalList).subscribe(out => {
            this.messageCode = out.messageCode;
            this.postToEmpPortalResponseData.result = out.responseData[0];
            if(this.postToEmpPortalResponseData.result == 'success') {
              this.i18UtilService.get('employeeRefreal.publish.success').subscribe(res=>{

                this.notificationService.setMessage('INFO',res);
              })
              if(this.disableEmpRefStartDate) {
                this.dialogRef.close(0);
              } else {
                this.publishEmployee=false;
                this.dialogRef.close(1);
              }
            }
          });
    }
  }
public setMessage(severity: string, message: string) {
  this.i18UtilService.get(message).subscribe(res=>{
    this.severity = severity;
    this.message = res;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = 'successful';
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = 'warning';
        break;
      default:
        this.severityClass = '';
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
  });
  
  }

  clearMessages() {
    this.severity = '';
    this.message = '';
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  selectEmpRefStartDate(empRefStartDate: Date) {
    this.empRefStartDate = empRefStartDate;
  }

  selectEmpRefEndDate(empRefEndDate: Date) {
    this.empRefEndDate = empRefEndDate;
  }

}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class PostToEmpPortalResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: 'INFO',
  WARNING: 'WARNING'
}
