import { Component, OnInit} from '@angular/core';
import { NotificationService } from "../../../common/notification-bar/notification.service";
import { MatDialogRef } from '@angular/material';
import { JobActionService } from '../job-action.service'
import { SessionService } from '../../../session.service';
import { PostJobToIJP } from "../post-to-ijp/post-to-ijp-data";
import { DatePipe } from '@angular/common';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-publish-to-internal-bulk',
  templateUrl: './publish-to-internal-bulk.component.html',
  styleUrls: ['./publish-to-internal-bulk.component.css'],
  providers:[DatePipe]
})
export class PublishToInternalBulkComponent implements OnInit {
  publishInternal:boolean=false;
  lable:string;
  datalist: JobListResponse[] = [];
  messageCode: MessageCode;
  ijpStartDate: Date;
  ijpEndDate: Date;
  postToIJP: PostJobToIJP;
  postToIJPResponseData: PostToIJPResponse;
  message: string;
  count: number;
  timer: any;
  jobhold: string;
  severity: String;
  severityClass: string;
  landscape = "landscape";
  posted: boolean = false;
  currentDate : Date;
  

  constructor(public dialogRef: MatDialogRef<PublishToInternalBulkComponent>, private jobActionService: JobActionService,
    private sessionService: SessionService, private notificationService: NotificationService,
    private datePipe:DatePipe, private i18UtilService: I18UtilService) {
    this.postToIJPResponseData = new PostToIJPResponse();
    this.count = 0;
    this.message = "";
    this.currentDate = new Date(Date.now());
  }

  ngOnInit() {
    this.datalist = this.jobActionService.getDataList();
    if (Object.keys(this.datalist).length != 0) {
      this.count = this.datalist.length;
    }
    for (let i of this.datalist) {
      if (i.postedOnIJP == true) {
        this.posted = true;
        break;
      }
    }
    if (this.posted == false) {
      this.ijpStartDate = new Date();
      this.ijpEndDate = new Date();
      this.ijpEndDate.setDate(this.ijpStartDate.getDate() + 7);
    } else { this.ijpStartDate = null }
    this.i18UtilService.get('common.label.post').subscribe((res: string) => {
      this.lable = res;
     });
  }

  postMultipleJobsToIJP(event) {
    this.notificationService.clear();
    if (this.ijpStartDate > this.ijpEndDate) {
    let sDate=Date.parse(this.datePipe.transform(this.ijpStartDate));
    let eDate=Date.parse(this.datePipe.transform(this.ijpEndDate));
    if (sDate > eDate) {
      this.setMessage("WARNING", 'postIjp.warning.IJPEnd_StartDate');
    }
    }else if (this.ijpEndDate == null) {
      this.setMessage("WARNING", "common.warning.endDateRequired");
    } else if (this.jobhold == "HOLD") {
      this.setMessage("WARNING", "job.warning.removeOnHoldJob");
    }
    else if (this.jobhold == "CLOSED") {
      this.setMessage("WARNING", "job.warning.removeClosedJob");
    }
    else {
      this.publishInternal=true;
      let postToIJP : PostJobToIJP;
      let postToIJPList: PostJobToIJP[] = []; 
      for(let req of this.datalist){
        postToIJP = new PostJobToIJP();
        postToIJP.requisitionID = req.requisitionId;
        if(req.postedOnIJP == true){
          postToIJP.ijpStartDate = new Date(req.ijpStartDate);
        }else{
          postToIJP.ijpStartDate = this.ijpStartDate;
          if(this.ijpStartDate == null){
            postToIJP.ijpStartDate = new Date();
          }
        }
        postToIJP.ijpEndDate = this.ijpEndDate;
        postToIJP.jobCode = req.requisitionCode;
        postToIJPList.push(postToIJP);
      }
      this.jobActionService.postJobToIJP(postToIJPList).subscribe(out => {
        this.messageCode = out.messageCode;
        this.postToIJPResponseData.result = out.responseData[0];
        if (this.postToIJPResponseData.result == "success") {
          this.dialogRef.close("success");
          this.i18UtilService.get("postIjp.publish.success").subscribe((res: string) => {
            this.notificationService.setMessage("INFO", res);
           });
          this.jobActionService.getJobComponent().ngOnInit();
        }
      });
    }
  }
    
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      this.jobhold = "CLOSED";
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      this.jobhold = "HOLD";
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }
  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);
    });
   
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  selectIjpStartDate(startDate: Date) {
    this.ijpStartDate = startDate;
  }

  selectIjpEndDate(endDate: Date) {
    this.ijpEndDate = endDate;
  }

}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export class JobListResponse {
  draftRequisitionID: Number;
  requisitionId: Number;
  hiringManager: String;
  requisitionCode: String;
  jobTitle: String;
  orgUnitCode: String;
  joiningLocation: String;
  sysRequisitionStatus: String;
  jobStatusClass: String;
  createdDate: String;
  modifiedDate: String;
  noOfOpening: number;
  selected: boolean;
  postedOnIJP: boolean;
  ijpStartDate: Date;
  disableIJPStartDate : boolean;
}

export class PostToIJPResponse {
  result: String;
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}