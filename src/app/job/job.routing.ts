import { MonsterComponent } from './job-detail/auto-matched-candidates/job-board-profiles/monster/monster/monster.component';
import { SocialCandidateDetailComponent } from './job-detail/auto-matched-candidates/social-profiles/social-candidate-detail/social-candidate-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { JobComponent } from './job.component';
import { NewJobComponent } from './new-job/new-job.component';
import { JobListComponent } from './job-list/job-list.component';
import { DraftJobListComponent } from './draft-job-list/draft-job-list.component';
import { EditJobComponent } from './edit-job/edit-job.component';
import { JobDetailsMainComponent } from './job-detail/job-details-main/job-details-main.component';
import { PerformanceSourceTeamComponent } from './job-detail/performance-source-team/performance-source-team.component';
import { TaggedCandidatesComponent } from './job-detail/tagged-candidates/tagged-candidates.component';
import { AutoMatchedCandidatesComponent } from './job-detail/auto-matched-candidates/auto-matched-candidates.component';
import { RejectedCandidatesComponent } from './job-detail/rejected-candidates/rejected-candidates.component';
import { HistoryChatsComponent } from './job-detail/history-chats/history-chats.component';
import { SessionGuard } from '../session.guard';
import { CompanyProfilesComponent } from './job-detail/auto-matched-candidates/company-profiles/company-profiles.component';
import { SocialProfilesComponent } from './job-detail/auto-matched-candidates/social-profiles/social-profiles.component';
import { JobBoardProfilesComponent } from './job-detail/auto-matched-candidates/job-board-profiles/job-board-profiles.component';
import { MonsterDetailComponent } from 'app/job/job-detail/auto-matched-candidates/job-board-profiles/monster/monster-detail/monster-detail.component';
import { CreatedJobListComponent } from './created-job-list/created-job-list.component';
import { PublishToErJobListComponent } from './publish-to-er-job-list/publish-to-er-job-list.component';
import { JobDetailBusinessComponent } from './job-detail/job-detail-business.component';
import { EditJobBusinessComponent } from './edit-job-business/edit-job-business.component';

const JOB_ROUTES: Routes = [
    {
        path: '', component: JobComponent, children: [
            { path: 'joblist', component: JobListComponent, canActivate: [SessionGuard] },
            { path: 'myjobs', component: JobListComponent, canActivate: [SessionGuard] },
            { path: 'createdjobs', component: CreatedJobListComponent,canActivate: [SessionGuard]},
            { path: 'erp' , component: PublishToErJobListComponent , canActivate:[SessionGuard]},
            { path: 'ijp' , component: PublishToErJobListComponent , canActivate:[SessionGuard]},
            { path: 'draftjoblist', component: DraftJobListComponent, canActivate: [SessionGuard] },
            { path: 'editjob/:id', component: NewJobComponent },
            { path: 'newjob', component: NewJobComponent, canActivate: [SessionGuard] },
            {
                path: 'auto-match-candidates/:id', component: AutoMatchedCandidatesComponent,
                children: [
                    { path: 'company', component: CompanyProfilesComponent },
                    { path: 'social', component: SocialProfilesComponent, },
                    { path: 'jobboard', component: JobBoardProfilesComponent, 
                        children: [
                            { path: '', redirectTo: 'monster' },
                            { path: 'monster', component: MonsterComponent },
                            { path: 'shine', component: MonsterComponent, }
                        ]
                    }
                ]
            },
            { path: 'auto-match-candidates/:id/social/candidate/:sid', component: SocialCandidateDetailComponent },
            { path: 'auto-match-candidates/:id/jobboard/:jobboardname/:sid', component: MonsterDetailComponent },
            { path: 'job-detail/:id' , component:JobDetailBusinessComponent},
            { path: 'edit-job-er/:id',component:EditJobBusinessComponent,
            children:[
                {path: '', redirectTo:'job-details'},
                {path: 'job-details' , component:JobDetailBusinessComponent}
                ]
            },
            { path: 'edit-job-jobApproval/:id',component:EditJobBusinessComponent,
            children:[
                {path: '', redirectTo:'job-details'},
                {path: 'job-details' , component:JobDetailBusinessComponent}
                ]
            },
            { path: 'edit-job-ijp/:id',component:EditJobBusinessComponent,
            children:[
                {path: '', redirectTo:'job-details'},
                {path: 'job-details' , component:JobDetailBusinessComponent}
                ]
            },
            {
                path: 'edit-job/:id', component: EditJobComponent,
                children: [
                    { path: '', redirectTo: 'job-details' },
                    { path: 'job-details', component: JobDetailsMainComponent },
                    { path: 'performance-source-team', component: PerformanceSourceTeamComponent ,canActivate: [SessionGuard] },
                    { path: 'tagged-candidates', component: TaggedCandidatesComponent },
                    { path: 'rejected-candidates', component: RejectedCandidatesComponent },
                    { path: 'history-chats', component: HistoryChatsComponent }
                ]
            }
        ]
    }
];

export const JobRouting = RouterModule.forChild(JOB_ROUTES);
