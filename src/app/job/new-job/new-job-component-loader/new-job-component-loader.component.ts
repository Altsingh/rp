import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-new-job-component-loader',
  templateUrl: './new-job-component-loader.component.html',
  styleUrls: ['./new-job-component-loader.component.css']
})
export class NewJobComponentLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
