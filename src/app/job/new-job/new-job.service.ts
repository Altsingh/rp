import { Injectable } from '@angular/core';
import { RequestResponseModel } from 'app/common/request-response-model';
import { CustomHttpService } from 'app/shared/services/custom-http-service.service';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { NewJobAddToTemplateDetailsRequest } from '../../common/new-job-add-to-template-details-request';
import { NewJobBasicDetailsRequest } from '../../common/new-job-basic-details-request';
import { NewJobCtqDetailsRequest } from '../../common/new-job-ctq-details-request';
import { NewJobDescriptionDetailsRequest } from '../../common/new-job-description-details-request';
import { NewJobOrganisationalDetailsRequest } from '../../common/new-job-organisational-details-request';
import { NewJobSkillDetailsRequest } from '../../common/new-job-skill-details-request';
import { NewJobWorkflowDetailsRequest } from '../../common/new-job-workflow-details-request';
import { SessionService } from '../../session.service';


@Injectable()
export class NewJobService {

  socialMatchApiUrl = environment.socialMatchApiUrl;
  constructor(private sessionService: SessionService, private httpService: CustomHttpService) { }
  createJob(saveJobRequest: SaveJobRequest) {
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/requisition/saveJob/' + this.sessionService.organizationID + '/', JSON.stringify(saveJobRequest));
  }

  withdrawJob(saveJobRequest: SaveJobRequest) {
    console.log(saveJobRequest)
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/requisition/withdrawJob/' + this.sessionService.organizationID + '/', JSON.stringify(saveJobRequest));
  }

  approveRejectJob(jobApprovalTO: any) {    
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/requisition/approveRejectJob', JSON.stringify(jobApprovalTO));
  }

  resubmitJob(saveJobRequest: SaveJobRequest) {
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/requisition/resubmitJob/' + this.sessionService.organizationID + '/', JSON.stringify(saveJobRequest));
  }

  submitDraft(saveJobRequest: SaveJobRequest) {
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/requisition/saveDraft/' + this.sessionService.organizationID + "/", JSON.stringify(saveJobRequest))
  }

  getDraftDetails(draftRequisitionID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/draftJobdetails/' + draftRequisitionID + '/' + this.sessionService.organizationID + "/");
  }

  getJobDetails(requisitionID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/jobdetails/' + requisitionID + '/' + this.sessionService.organizationID + "/");
  }

  getJobTemplateDetails(jobTemplateName: String) {
    let request: GetJobTemplateDetailsRequest = new GetJobTemplateDetailsRequest();
    let input: GetJobTemplateDetailsRequestInput = new GetJobTemplateDetailsRequestInput();
    input.jobTemplateName = jobTemplateName;
    request.input = input;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/JobTemplateDetails/' + this.sessionService.organizationID + "/", JSON.stringify(request));
  }

  getJobTemplateList() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/JobTemplateList/' + this.sessionService.userID + '/' + this.sessionService.organizationID + "/");
  }

  getSaveDraftServiceURL(sectionName: String) {
    return (this.sessionService.orgUrl + '/rest/altone/requisition/saveDraft/' + sectionName + '/' + this.sessionService.organizationID + "/")
  }

  getSaveJobServiceURL(sectionName: String) {
    return (this.sessionService.orgUrl + '/rest/altone/requisition/saveJob/' + sectionName + '/' + this.sessionService.organizationID + "/")
  }

  saveBasicDetails(basicDetailsRequestData: BasicDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('basic'), JSON.stringify(basicDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('basic'), JSON.stringify(basicDetailsRequestData));
    }
  }

  checkForDuplication(requisiitonID, employeeCodeList, isECodeIsDropDown): Promise<any> {
    if (isECodeIsDropDown) {
      if (employeeCodeList === null || employeeCodeList.length === 0) {
        return Observable.of(new RequestResponseModel('EC200', 'DUPLICACY')).toPromise();
      } else {
        let request: EmpCodeDuplicacyRequestData = new EmpCodeDuplicacyRequestData();
        let input: EmpCodeDuplicacyRequest = new EmpCodeDuplicacyRequest();
        input.selectedEmployeeCodeListValues = employeeCodeList;
        input.requisitionID = requisiitonID;
        request.input = input;
        return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/checkForDuplication', JSON.stringify(request)).toPromise();

      }
    }
  }

  saveOrganisationalDetails(organisationalDetailsRequestData: OrganisationalDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('organisational'), JSON.stringify(organisationalDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('organisational'), JSON.stringify(organisationalDetailsRequestData))
    }
  }

  saveSkillDetails(skillDetailsRequestData: SkillDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('skill'), JSON.stringify(skillDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('skill'), JSON.stringify(skillDetailsRequestData));
    }
  }

  saveJobDescriptionDetails(jobDescriptionDetailsRequestData: JobDescriptionDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('jobDesc'), JSON.stringify(jobDescriptionDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('jobDesc'), JSON.stringify(jobDescriptionDetailsRequestData));
    }
  }

  saveWorkflowDetails(workflowDetailsRequestData: WorkflowDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('workflow'), JSON.stringify(workflowDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('workflow'), JSON.stringify(workflowDetailsRequestData));
    }
  }

  saveCTQDetails(ctqDetailsRequestData: CTQDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('ctq'), JSON.stringify(ctqDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('ctq'), JSON.stringify(ctqDetailsRequestData));
    }
  }

  saveAddToTemplateDetails(addToTemplateDetailsRequestData: AddToTemplateDetailsRequestData, isDraft: boolean) {
    if (isDraft) {
      return this.httpService.post(this.getSaveDraftServiceURL('addToTemplate'), JSON.stringify(addToTemplateDetailsRequestData));
    } else {
      return this.httpService.post(this.getSaveJobServiceURL('addToTemplate'), JSON.stringify(addToTemplateDetailsRequestData));
    }
  }

  saveJobTemplate(saveJobRequest: SaveJobRequest) {
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/saveJobTemplate/' + this.sessionService.organizationID + "/", JSON.stringify(saveJobRequest));
  }

  deleteJobDraft(draftRequisitionID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/deleteJobDraft/' + draftRequisitionID + "/")
  }

  deleteJobTemplate(jobTemplateName: String) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/deleteJobTemplate/' + jobTemplateName + '/' + this.sessionService.userID + '/' + this.sessionService.organizationID + "/");
  }

  getContentTypeMaster(contentCategory: string) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_contentTypeByCategory/' + this.sessionService.organizationID + "/", { params: { "contentCategory": contentCategory } })
  }

  getSysIndustryMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/masters/sysIndustry' + "/");
  }

  getEmployeeCategory() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_employeeCategory' + "/")
  }

  getJobFamily(): Promise<any> {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_jobFamily' + "/").toPromise();
  }
  getPositionList() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_position' + "/");
  }

  getCampusType() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_campusType' + "/");
  }
  
  getProgramType() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_programType' + "/");
  }

  getRoleType() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_roleType' + "/");
  }

  getRoleContribution() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_roleContribution' + "/");
  }

  getEmploymentTenure() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_employmentTenure' + "/");
  }

  getResourceType() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_resourceType' + "/");
  }

  getFunctionalAreaMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_functionalArea/' + this.sessionService.organizationID + "/");
  }

  getRequisitionRoleMaster(functionalArea: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_requisitionRole/' + functionalArea + '/' + this.sessionService.organizationID + "/");
  }

  getGradeMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_grade/' + this.sessionService.organizationID + "/");
  }

  getBandMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/masters/masterdata_band/');
  }

  getDesignationMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_designation/');
  }

  getDenominationMaster(currencyCodeID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_denomination/' + currencyCodeID + "/");
  }

  getWorkflowMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_workflowList/' + this.sessionService.organizationID + "/");
  }

  getEmployeeCodeMaster(searchString: String, count: Number) {
    if (searchString == null) {
      return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_employeeCodeListByQuery/' + count + "/");
    } else {
      return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_employeeCodeListByQuery/' + count + '/' + searchString + "/");
    }
  }

  getOrgunitTypesJSONPopulated(orgunitHierarchyName: String): Promise<any> {
    let request = new OrgunitHierarchyRequest();
    let requestInput = new OrgunitHierarchyRequestInput();
    requestInput.orgunitHierarchy = orgunitHierarchyName;
    request.input = requestInput;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_orgunitTypesByHierarchy/' + this.sessionService.organizationID + "/", JSON.stringify(request)).toPromise();
  }


  getOrgunitsByParentID(parentID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_orgunitsByParentID/' + parentID + '/' + this.sessionService.organizationID + "/");
  }

  populatePositionDefault(positionID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/position_default/' + positionID + "/");
  }

  getWorksiteTypesJSONPopulated(worksiteHierarchyName: String): Promise<any> {
    let request = new WorksiteHierarchyRequest();
    let requestInput = new WorksiteHierarchyRequestInput();
    requestInput.worksiteHierarchy = worksiteHierarchyName;
    request.input = requestInput;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_worksiteTypesByHierarchy/', JSON.stringify(request)).toPromise();
  }

  getWorksitesByParentID(parentID: Number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_worksitesByParentID/' + parentID + '/' + this.sessionService.organizationID + "/");
  }

  getSkillsMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata__skillListByTenant/' + this.sessionService.tenantID + "/");
  }

  getSkillsByPattern(searchPattern: String, count: number) {
    let request = new GetSkillsByPatternRequest();
    let requestInput = new GetSkillsByPatternRequestInput();
    requestInput.searchPattern = searchPattern;
    requestInput.count = count;
    request.input = requestInput;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/skillsByPattern' + "/", JSON.stringify(request));
  }

  getCertificationsByPattern(searchPattern: String, count: number) {
    let request = new GetSkillsByPatternRequest();
    let requestInput = new GetSkillsByPatternRequestInput();
    requestInput.searchPattern = searchPattern;
    requestInput.count = count;
    request.input = requestInput;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/certificationsByPattern' + "/", JSON.stringify(request));
  }

  getMinQualificationsByPattern(searchPattern: String, count: number) {
    let request = new GetSkillsByPatternRequest();
    let requestInput = new GetSkillsByPatternRequestInput();
    requestInput.searchPattern = searchPattern;
    requestInput.count = count;
    request.input = requestInput;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/qualificationsByPattern' + "/", JSON.stringify(request));
  }

  getLanguagesByPattern(searchPattern: String, count: number) {
    let request = new GetSkillsByPatternRequest();
    let requestInput = new GetSkillsByPatternRequestInput();
    requestInput.searchPattern = searchPattern;
    requestInput.count = count;
    request.input = requestInput;
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/languagesByPattern' + "/", JSON.stringify(request));
  }

  getCTQMaster(ctqid : string) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_ctq/' + ctqid);
  }

  getCTQListMaster() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_ctqlist/');
  }

  getInterviewPanelist() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_interviewPanelist/' + this.sessionService.organizationID + "/");
  }

  getDraftRequisitionInterviewPanelist(draftRequisitionID: number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/draftRequisitionInterviewPanelist/' + draftRequisitionID + '/' + this.sessionService.organizationID + "/");
  }

  getRequisitionInterviewPanelist(requisitionID: number) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/requisitionInterviewPanelist/' + requisitionID + '/' + this.sessionService.organizationID + "/");
  }


  employeesByRoleType(roleType: string) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/common/employeesByRoleType/' + roleType + '/' + this.sessionService.organizationID + "/");
  }

  usersByRoleType(roleType: string) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/common/usersByRoleType/' + roleType + "/");
  }

  getSuggestedSkills(job_title: string) {
    return this.httpService.get(this.socialMatchApiUrl + '/skill_prediction?job_title=' + job_title + '&company_id=2134');
  }

  fetchMinMaxSalary(request: SaveJobRequest) {
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/fetchMinMaxSalary', JSON.stringify(request));
  }

  getPolicyConfiguration() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/policyConfiguration');
  }

  getRequisitionRoleLevelConfig() {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/requisitionRoleLevelConfig');
  }

  //Default Competency
  saveRequisitionDefaultCompetency(requisitionID) {
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/saveRequisitionDefaultCompetency/' + requisitionID);
  }
  getWorkflowStageActions(stageID: number,workflowStageType : String) {
    return this.httpService.get(this.sessionService.orgUrl + '/rest/altone/jobs/workflowActions/', { params: { "stageID": stageID , "workFlowStageType": workflowStageType} });
  }
}

export class GetSuggestedSkillsRequest {
  input: GetSuggestedSkillsRequestInput;
}

export class GetSuggestedSkillsRequestInput {
  url: string;
  serviceName: string;
  job_title: string;
  company_id: string;
}

export class BasicDetailsRequestData {
  input: NewJobBasicDetailsRequest;
}

export class OrganisationalDetailsRequestData {
  input: NewJobOrganisationalDetailsRequest;
}

export class SkillDetailsRequestData {
  input: NewJobSkillDetailsRequest;
}

export class JobDescriptionDetailsRequestData {
  input: NewJobDescriptionDetailsRequest;
}

export class WorkflowDetailsRequestData {
  input: NewJobWorkflowDetailsRequest;
}

export class CTQDetailsRequestData {
  input: NewJobCtqDetailsRequest;
}

export class AddToTemplateDetailsRequestData {
  input: NewJobAddToTemplateDetailsRequest;
}

export class SaveJobRequest {
  input: CreateJobRequestDetails;
}

export class EmpCodeDuplicacyRequestData {
  input: EmpCodeDuplicacyRequest;
}

export class EmpCodeDuplicacyRequest {
  requisitionID: Number;
  selectedEmployeeCodeListValues: Array<String>;


}
export class CreateJobRequestDetails {
  draftRequisitionID: Number;
  requisitionID: Number;
  jobTemplateName: String;
  noOfOpenings: Number;
  jobTitle: String;
  startDate: String;
  closingDate: String;
  minimumExp: Number;
  maximumExp: Number;
  minSalary: Number;
  maxSalary: Number;
  requisitionTypeID: Number;
  gradeCode: String;
  gradeID: Number;
  designation: String;
  currencyDenominationID: Number;
  hiringManagerName: String;
  hiringManagerID: Number;
  interviewPanelistSelected: Array<Number>;
  functionalAreaIDOrg: Number;
  requisitionRoleIDOrg: Number;
  organizationUnitID: Number;
  worksiteID: Number;
  skillList: any[];
  languageList: String[];
  certificationList: String[];
  minQualificationList: String[];
  jobDescription: String;
  workflowID: Number;
  createdBy: Number;
  modifiedBy: Number;
  industryId: number;
  employeeCategoryId: number;
  jobFamilyId: number;
  positionId: number;
  campusTypeId: number;
  programTypeId: number;
  resourceTypeId: number;
  tat: Number;
  confidential: boolean;
  bandId: number;
  band: string;
  selectedEmployeeCodeListValues: Array<String>;
  tatPolicyEnabled: boolean;
  orgUnitId: Number;
  recruitWorkflowPolicyEnabled: boolean;
  recruitApprovalPolicyEnabled: boolean;
  closingDateAutopopulateEnabled: boolean;
  sourcingGuidline: String;
  ctqID: Number;
  ctqQuesIDs: Array<Number>;
  ctqRemovedIDs: Array<Number>;
  isOrganizationUnitRendered: boolean;
  isJoiningLocationRendered: boolean;
  stageStatusID; Number;
  comments: String;
}

export class OrgunitHierarchyRequest {
  input: OrgunitHierarchyRequestInput;
}

export class OrgunitHierarchyRequestInput {
  orgunitHierarchy: String;
}

export class WorksiteHierarchyRequest {
  input: WorksiteHierarchyRequestInput;
}

export class WorksiteHierarchyRequestInput {
  worksiteHierarchy: String;
}

export class GetJobTemplateDetailsRequest {
  input: GetJobTemplateDetailsRequestInput;
}

export class GetJobTemplateDetailsRequestInput {
  jobTemplateName: String;
  createdBy: Number;
}

export class GetSkillsByPatternRequest {
  input: GetSkillsByPatternRequestInput;
}
export class GetSkillsByPatternRequestInput {
  searchPattern: String;
  count: Number;
  tenantID: Number;
}
