import { TranslateService } from '@ngx-translate/core';
import { DatePipe, Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { CommonService } from '../../common/common.service';
import { ContentTypeMaster } from '../../common/content-type-master';
import { FunctionalAreaMaster } from '../../common/functional-area-master';
import { MessageCode } from '../../common/message-code';
import { NewJobAddToTemplateDetailsRequest } from '../../common/new-job-add-to-template-details-request';
import { NewJobBasicDetailsRequest } from '../../common/new-job-basic-details-request';
import { NewJobCtqDetailsRequest } from '../../common/new-job-ctq-details-request';
import { NewJobDescriptionDetailsRequest } from '../../common/new-job-description-details-request';
import { NewJobOrganisationalDetailsRequest } from '../../common/new-job-organisational-details-request';
import { NewJobSkillDetailsRequest, SkillType } from '../../common/new-job-skill-details-request';
import { NewJobWorkflowDetailsRequest } from '../../common/new-job-workflow-details-request';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { OrgunitMaster } from '../../common/orgunit-master';
import { RequisitionRoleMaster } from '../../common/requisition-role-master';
import { WorkflowMaster } from '../../common/workflow-master';
import { WorksiteMaster } from '../../common/worksite-master';
import { ContainerComponent } from '../../container/container.component';
import { ModuleConfigService } from '../../module-config.service';
import { DialogService } from '../../shared/dialog.service';
import { SelectItem } from '../../shared/select-item';
import { SessionService } from './../../session.service';
import { FormatJobcodeParamPipe } from './../../shared/format-jobcode-param.pipe';
import { AutoMatchCandidatesService } from './../job-detail/auto-match-candidates.service';
import { JobDetailService } from './../job-detail/job-detail.service';
import { NewJobFormSecurityService } from './new-job-form-security.service';
import { NewJobService } from './new-job.service';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { CtqMaster } from 'app/common/ctq-master';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { CustomValidatorService } from 'app/shared/services/custom-validator.service';
import { DomSanitizer } from '@angular/platform-browser';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css'],
  providers: [NewJobService, DialogService, NewJobFormSecurityService, DatePipe, FormatJobcodeParamPipe]
})
export class NewJobComponent implements OnInit {
  jobDescLength: number = 0;
  sourcingGuidLineLength: number = 0;
  jobDescriptionMinLength: number = 0;
  lable: string = "Save as Job";
  volsectionactive: boolean = false;
  dialogResult: any;
  basicsectionactive: boolean = false;
  templatesectionactive: boolean = false;
  organisationalsectionactive: boolean = false;
  skillsectionactive: boolean = false;
  jobsectionactive: boolean = false;
  worksectionactive: boolean = false;
  ctqsectionactive: boolean = false;
  addsectionactive: boolean = false;
  basicsavestatus: boolean = false;
  basicDetailsRequestData: BasicDetailsRequestData;
  organisationalDetailsRequestData: OrganisationalDetailsRequestData;
  skillDetailsRequestData: SkillDetailsRequestData;
  jobDescriptionDetailsRequestData: JobDescriptionDetailsRequestData;
  workflowDetailsRequestData: WorkflowDetailsRequestData;
  ctqDetailsRequestData: CTQDetailsRequestData;
  addToTemplateDetailsRequestData: AddToTemplateDetailsRequestData;
  workFlowOpening: Number;
  SelectedWorkflowName: String;

  basicInfo: FormGroup;
  organisationalInfo: FormGroup;
  skillInfo: FormGroup;
  certificationInfo: FormGroup;
  minQualificationInfo: FormGroup;
  workingLanguageInfo: FormGroup;

  jobDescriptionInfo: FormGroup;
  workflowInfo: FormGroup;
  ctqInfo: FormGroup;
  addToTemplateInfo: FormGroup;

  interviewPanelLoaded: boolean = false;
  interviewPanelistSelected: any[];

  basicInfoContinueBlock: boolean = false;
  organisationalInfoContinueBlock: boolean = false;
  skillInfoContinueBlock: boolean = false;
  jobDescriptionInfoContinueBlock: boolean = false;
  workflowInfoContinueBlock: boolean = false;
  ctqInfoContinueBlock: boolean = false;
  addToTemplateInfoContinueBlock: boolean = false;

  basicInfoComplete: boolean = false;
  organisationalInfoComplete: boolean = false;
  skillInfoComplete: boolean = false;
  jobDescriptionInfoComplete: boolean = false;
  workflowInfoComplete: boolean = false;
  ctqInfoComplete: boolean = false;
  addToTemplateInfoComplete: boolean = false;

  saveDraftDetailsButtonBlock: boolean = false;
  saveJobDetailsButtonBlock: boolean = false;

  saveAsJobFlag: boolean = false;

  commentVisible: boolean = false;

  basicInfoSequence: number;
  organisationalInfoSequence: number;
  skillInfoSequence: number;
  jobDescriptionInfoSequence: number;
  workflowInfoSequence: number;
  ctqInfoSequence: number;
  addToTemplateInfoSequence: number;
  voilaSequence: number;

  skillList: Array<string>;
  certificationList: Array<string>;
  minQualificationList: Array<string>;
  languageList: Array<string>;

  suggestedSkillList: Array<string>;
  suggestedCertificationList: Array<string>;
  suggestedMinQualificationList: Array<string>;
  suggestedLanguageList: Array<string>;
  suggestedSkillLoaderShow: boolean;
  filteredSuggestedSkillList: Array<string>;
  selectedSkillList: Array<SkillType>;
  selectedCertificationList: Array<string>;
  selectedMinQualificationList: Array<string>;
  selectedLanguageList: Array<string>;
  gradeListMaster: any[];

  gradeList: any[];

  bandList: any[];

  extraBandList: any[] = [];

  band: any;
  flag: boolean = false;

  designationListMaster: any[];
  designationList: any[];

  requisitionTypeList: ContentTypeMaster;
  industryList: any[];

  employeeCategoryList: any[];

  jobFamilyList: any[];

  positionList: any[];

  campusTypeList: any[];

  programTypeList: any[];

  roleTypeList: any[];
  
  roleContributionList: any[];
  
  employmentTenureList: any[];

  resourceTypeList: any[];

  ctqList: any[];

  removedQuestionList: CtqMaster[] = [];

  createdBy: string;

  category: any;

  denominationList: SelectItem[];

  functionalAreaList: FunctionalAreaMaster;

  requisitionRoleList: RequisitionRoleMaster;

  workflowList: WorkflowMaster[];

  messageCode: MessageCode;
  draftRequisitionID: Number;
  requisitionID: Number;
  currencyList: SelectItem[];

  orgunitMastersList: Array<OrgunitMaster>;
  selectedOrgunitID: Number;

  worksiteMastersList: Array<WorksiteMaster>;

  selectedWorksiteID: Number;

  selectedGradeID: Number;

  ctqData: Array<CtqMaster>;
  DDRange: SelectItem[];
  MMRange: SelectItem[];
  YYYYRange: SelectItem[];
  EXPRange: SelectItem[];

  jobStartDate: Date;
  jobEndDate: Date;

  security: any = {};
  securitySubscription: Subscription;

  interviewPanelist: SelectItem;
  hiringManagerList: Array<SelectItem>;

  hiringManagerName: String;
  skillMessage: String;
  certificationMessage: String;
  minQualificationMessage: String;
  languageMessage: String;
  jobTemplateList: Array<string> = [];
  tempateList: Item[] = [];
  public filterQuery = "";

  profileCompletionPercentage: Number = 0;
  profileCompletionRange = [];

  pageTitle: string;
  isJobEdit: boolean;
  continueButtonLabel: string;
  autoMatchLabel: string = 'Auto match candidates';
  profileCompletionInProgress: boolean = false;
  reRunProfileCompletion = false;

  skillSelectBoolean: boolean = false;
  certificationSelectBoolean: boolean = false;
  minQualificationSelectBoolean: boolean = false;
  languageSelectBoolean: boolean = false;
  showLoader: boolean = false;
  requisitionCode: string;
  enableCTQ: boolean = false;
  radioButtonEnabled: boolean = false;

  moduleConfig: any;

  tatPolicyEnabled: boolean;
  selectedEmployeeCodeListValues: Array<String> = [];
  selectedEmployeeCodeList: Array<SelectItem> = [];
  filteredEmployeeCodeList: Array<SelectItem> = [];

  recruitWorkflowPolicyEnabled = false;
  recruitApprovalPolicyEnabled = false;

  selectedRequisitionType = '';

  requisitionRoleLevelConfigResponse: RequisitionRoleLevelConfigResponseTO;

  isECodeIsDropDown: boolean = false;
  closingDateAutopopulateEnabled: boolean = false;
  automatchEnabled: boolean;

  syncGradeDesigCall = new Subject<String>();
  flagSync1: boolean;
  flagSync2: boolean;
  flagSync3: boolean;

  syncDesignDone = new Subject<String>();

  disableBandGrade: boolean = false;

  langContinueButtonLabel: string;
  langEnterPlaceholder: string = "Enter";
  langSelectPlaceholder: string = "Select";
  langPageTitle: string;

  stageID: any;
  workflowStageType: any;
  froalakey: any;
  comments: any = "";

  withdrawnSuccess: boolean = false;
  actionList: any[];

  public editorOptions: Object;

  featureDataMap: Map<any, any>;
  constructor(private ACTIVATED_ROUTE: ActivatedRoute,
    private sessionService: SessionService,
    private router: Router,
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private jobDetailService: JobDetailService,
    private formatJobcodeParamPipe: FormatJobcodeParamPipe,
    private formBuilder: FormBuilder,
    private newJobService: NewJobService,
    private dialogsService: DialogService,
    private commonService: CommonService,
    private notificationService: NotificationService,
    private formSecurityService: NewJobFormSecurityService,
    private datePipe: DatePipe,
    private location: Location,
    private i18UtilService: I18UtilService,
    private containerComponent: ContainerComponent,
    private moduleConfigService: ModuleConfigService,
    private sanitizer:DomSanitizer,
  ) {
    if (window.location.hostname.includes(".peoplestrong.com")) {
      this.froalakey = 'aH3H3B8A8bA4B3E3C1I3H2C2C6B3E2uzncH-7bjcef1G-10zrB1twt==';
    } else if (window.location.hostname.includes(".peoplestrongalt.com")) {
      this.froalakey = 'yC5E5B4E4jC10D7A5A5B2A3E4E2B2B5B-16rfkujbF-7A-21xdbjocdI1qA-16y==';
    }

    this.editorOptions = {
      key: this.froalakey,
      placeholderText: this.getTranslatedText('startTyping'),
      charCounterCount: false,
      heightMin: 200,
      heightMax: 250,
      toolbarButtons:['fullscreen', 'bold', 'italic', 'underline', 'fontFamily', 'fontSize', 
      '|','formatOL', 'formatUL', 'align','|', 'selectAll',  '|', 'undo', 'redo']
    };

    /*  i18UtilService.setDefaultLang('en'); */
    this.automatchEnabled = this.sessionService.isAutoMatchEnabled();
    this.showLoader = true;
    this.commonService.hideRHS();
    this.initFormSecurity();
    this.featureDataMap = this.sessionService.featureDataMap;

    this.ACTIVATED_ROUTE.queryParams.subscribe((queryParams) => {
      this.stageID = queryParams['ssid'];
      this.workflowStageType = queryParams['sss'];
    });

    if (this.featureDataMap.get("AUTOMATCH_JOBBOARD") == "3" && this.featureDataMap.get("AUTOMATCH_COMPANYDATA") == "3" && this.featureDataMap.get("AUTOMATCH_SOCIALPROFILE") == "3") {
      this.automatchEnabled = false;
    }

    this.flagSync1 = false;
    this.flagSync2 = false;
    this.flagSync3 = false;



    this.isSyncGradeDesigCall().subscribe((value) => {
      if (value == 'flagSync1') {
        this.flagSync1 = true;
      }
      if (value == 'flagSync2') {
        this.flagSync2 = true;
      }
      if (value == 'flagSync3') {
        this.flagSync3 = true;
      }
      if (this.flagSync1 && this.flagSync2 && this.flagSync3) {
        this.syncBandGradeList();
        this.syncGradeDesignationList();
      }
    });


    this.initializeBasicInfo();
    this.initializeOrganizationalInfo();
    this.initializeSkillInfo();
    this.initializeCertificationInfo();
    this.initializeWorkingLanguageInfo();
    this.initializeMinQualificationInfo();
    this.initializeJobDescriptionInfo();
    this.initializeWorkflowInfo();
    this.initializeTemplateInfo();
    this.initializeCTQInfo();

    this.getPolicyConfig();
    this.populateMasters();
    this.valueChangesListeners();

    const configParameters = ['REQUISITION_BAND_GRADE_LINK', 'REQUISITION_GRADE_DESIGNATION_LINK',
      'Grade_Validation', 'REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION', 'E-code',
      'ClosingDateAutopopulateOnTAT', 'REQUISITION_JOB_DESCRIPTION_MIN_LENGTH'];
    this.moduleConfigService.moduleConfigCache['Hiring'] = null;
    Promise.resolve(this.moduleConfigService.getModuleConfig('Hiring', configParameters)).then((moduleConfigJSON) => {

      if (moduleConfigJSON) {
        if (moduleConfigJSON.response) {
          this.moduleConfig = moduleConfigJSON.response;
          if (this.moduleConfig['E-code'] == null) {
            this.isECodeIsDropDown = false;
          } else {
            this.isECodeIsDropDown = (this.moduleConfig['E-code'].value == 1) ? true : false;
          }

          if (this.moduleConfig['ClosingDateAutopopulateOnTAT'] == null) {
            this.closingDateAutopopulateEnabled = false;
          } else {
            this.closingDateAutopopulateEnabled = (this.moduleConfig['ClosingDateAutopopulateOnTAT'].value == 1) ? true : false;
          }
          if (this.moduleConfig['REQUISITION_JOB_DESCRIPTION_MIN_LENGTH'] == null || isNaN(this.moduleConfig['REQUISITION_JOB_DESCRIPTION_MIN_LENGTH'].value)
            || this.moduleConfig['REQUISITION_JOB_DESCRIPTION_MIN_LENGTH'].value.trim() == '' || (+this.moduleConfig['REQUISITION_JOB_DESCRIPTION_MIN_LENGTH'].value) < 0) {
            this.jobDescriptionMinLength = 0;
          } else {
            this.jobDescriptionMinLength = this.moduleConfig['REQUISITION_JOB_DESCRIPTION_MIN_LENGTH'].value;
          }
        }
      }

      Promise.all([
        this.newJobService.getGradeMaster().toPromise(),
        this.newJobService.getBandMaster().toPromise(),
        this.newJobService.getDesignationMaster().toPromise()
      ]).then((responseArray) => {

        if (responseArray) {

          if (responseArray.length > 0) {

            const gradeResponse = responseArray[0];
            this.gradeListMaster = gradeResponse.responseData;

            if (!(this.moduleConfig['REQUISITION_BAND_GRADE_LINK'] != null && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'].value === '1')) {
              this.gradeList = gradeResponse.responseData;
            }
          }

          if (responseArray.length > 1) {
            const bandResponse = responseArray[1];
            this.bandList = bandResponse.responseData;
          }

          if (responseArray.length > 2) {
            const designationResponse = responseArray[2];
            this.designationListMaster = designationResponse.responseData;
            if (!(this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'] != null && this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'].value === '1')) {
              this.designationListMaster = designationResponse.responseData;
            }
          }
        }

        if (this.moduleConfig['Grade_Validation'] != null && this.moduleConfig['Grade_Validation'].value.toLowerCase() === 'true') {
          for (let band of this.bandList) {
            this.flag = false;
            for (let grade of this.gradeListMaster) {
              if (band.value == grade.bandId) {
                this.flag = true;
                break;
              }
            }
            if (!this.flag) {
              this.extraBandList.push(band);
            }
          }
          for (let band of this.extraBandList) {
            let index = this.bandList.indexOf(band);
            this.bandList.splice(index, 1);
          }
        }

        this.syncGradeDesigCall.next("flagSync2");
      });
    });

    this.basicsectionactive = true;
    this.templatesectionactive = true;
  }


  getRequisitionRoleLevelConfig() {
    this.newJobService.getRequisitionRoleLevelConfig().subscribe((out) => {
      if (out != null && out.messageCode != null && out.messageCode.code != null) {
        if (out.messageCode.code == "EC200") {
          this.requisitionRoleLevelConfigResponse = out.response;
        }
      }
    });
  }

  getPolicyConfig() {
    this.newJobService.getPolicyConfiguration().subscribe((response) => {
      for (let policy of response) {
        if (policy == 'REQUISITION-TAT-POLICY') {
          this.tatPolicyEnabled = true;
        }
        if (policy == 'RECRUIT-WORKFLOW-POLICY') {
          this.recruitWorkflowPolicyEnabled = true;
        }
        if (policy == 'REQUISITION-APPROVAL-POLICY') {
          this.recruitApprovalPolicyEnabled = true;
        }

      }
    })
  }

  populateMasters() {
    this.DDRange = this.createRange(1, 31);
    this.MMRange = this.getMMRange();
    this.YYYYRange = this.createRange(2000, 30);
    this.EXPRange = this.createRange(0, 30);

    this.newJobService.getJobTemplateList().subscribe(out => {
      this.jobTemplateList = out.responseData
      for (var i in this.jobTemplateList) {
        this.tempateList.push({ label: this.jobTemplateList[i], value: this.jobTemplateList[i] })
      }
    });

    this.newJobService.getCTQListMaster().subscribe(out => {
      this.ctqList = out.responseData;
      if (this.ctqList != null) {
        for (let item of this.ctqList) {
          item.id = item.ctqID;
        }
      }
    });

    this.currencyList = [new SelectItem('INR', 1)];

    this.newJobService.getSkillsByPattern('', 50).subscribe(out => {
      this.skillList = out.responseData;
      this.removeSelectedSkillElements();
    });

    this.newJobService.getCertificationsByPattern('', 50).subscribe(out => {
      this.certificationList = out.responseData;
      this.removeSelectedCertificationElements();
    });

    this.newJobService.getMinQualificationsByPattern('', 50).subscribe(out => {
      this.minQualificationList = out.responseData;
      this.removeSelectedMinQualificationElements();
    });

    this.newJobService.getLanguagesByPattern('', 50).subscribe(out => {
      this.languageList = out.responseData;
      this.removeSelectedLanguageElements();
    });

    this.newJobService.getContentTypeMaster("REQUISITION_TYPE").subscribe(out => this.requisitionTypeList = out.responseData.requisitionTypeList);

    this.newJobService.getSysIndustryMaster().subscribe((response) => {
      this.industryList = response.responseData;
    })

    Promise.resolve(this.newJobService.getJobFamily()).then((response) => {
      this.jobFamilyList = response;
      if (this.jobFamilyList != null) {
        this.jobFamilyList.forEach((item, index) => {
          item.id = item.jobFamilyID;
          item.name = item.jobFamilyCode + "-" + item.jobFamilyDescription;
        });
      }

      this.newJobService.getEmployeeCategory().subscribe((response) => {
        this.employeeCategoryList = response;
        if (this.employeeCategoryList != null) {
          this.employeeCategoryList.forEach((item, index) => {
            item.id = item.employeeCategoryTypeID;
            item.name = item.employeeCategoryTypeCode;
          });
        }
      })
    })


    this.newJobService.getPositionList().subscribe((response) => {
      this.positionList = response;
      if (this.positionList != null) {
        for (let item of this.positionList) {
          item.id = item.positionID;
          item.name = item.positionCode + "-" + item.positionName;
        }
      }
    })

    this.newJobService.getCampusType().subscribe((response) => {
      this.campusTypeList = response.responseData;
      if (this.campusTypeList != null) {
        for (let item of this.campusTypeList) {
          item.id = item.contentTypeID;
          item.name = item.label;

        }
      }
    })
    
    this.newJobService.getProgramType().subscribe((response) => {
      this.programTypeList = response.responseData;
      if (this.programTypeList != null) {
        for (let item of this.programTypeList) {
          item.id = item.contentTypeID;
          item.name = item.label;

        }
      }
    })
    
    this.newJobService.getRoleType().subscribe((response) => {
      this.roleTypeList = response.responseData;
      if (this.roleTypeList != null) {
        for (let item of this.roleTypeList) {
          item.id = item.contentTypeID;
          item.name = item.label;

        }
      }
    })
    
    this.newJobService.getRoleContribution().subscribe((response) => {
      this.roleContributionList = response.responseData;
      if (this.roleContributionList != null) {
        for (let item of this.roleContributionList) {
          item.id = item.contentTypeID;
          item.name = item.label;

        }
      }
    })
    
    this.newJobService.getEmploymentTenure().subscribe((response) => {
      this.employmentTenureList = response.responseData;
      if (this.employmentTenureList != null) {
        for (let item of this.employmentTenureList) {
          item.id = item.contentTypeID;
          item.name = item.label;

        }
      }
    })

    this.newJobService.getResourceType().subscribe((response) => {
      this.resourceTypeList = response.responseData;
      for (let item of this.resourceTypeList) {
        item.id = item.contentTypeID;
        item.name = item.label;
      }
    })

    this.newJobService.getEmployeeCodeMaster(null, 10).subscribe(out => this.filteredEmployeeCodeList = out.responseData);

    this.newJobService.getFunctionalAreaMaster().subscribe(out => this.functionalAreaList = out.responseData);

    this.newJobService.getWorkflowMaster().subscribe(out => this.workflowList = out.responseData);

    this.interviewPanelLoaded = false;
    this.newJobService.getInterviewPanelist().subscribe((out) => {
      this.interviewPanelist = out.responseData
      this.interviewPanelLoaded = true;
      if (this.interviewPanelistSelected != null) {
        this.basicInfo.controls['interviewPanelistSelected'].setValue(this.interviewPanelistSelected);
      }
    });

    this.newJobService.usersByRoleType('HIRING_MANAGER').subscribe(out => this.hiringManagerList = out.responseData);
  }

  valueChangesListeners() {
    this.basicInfo.controls.currency.valueChanges.subscribe(data => {
      if (data != null && data != '') {
        //this.newJobService.getDenominationMaster(this.basicInfo.controls.currency.value).subscribe(out => this.denominationList = out.responseData);
      }
    });

    this.basicInfo.controls.employeeCode.valueChanges.subscribe(data => {
      if (this.isECodeIsDropDown) {
        this.newJobService.getEmployeeCodeMaster(this.basicInfo.controls.employeeCode.value, 10).subscribe(out => {
          let resData = [];
          for (let code of out.responseData) {
            if (this.selectedEmployeeCodeListValues == null || !this.selectedEmployeeCodeListValues.includes(code.value)) {
              resData.push(code);
            }
          }
          this.filteredEmployeeCodeList = resData;
        });
      } else {
        this.selectedEmployeeCodeList = new Array<SelectItem>();
        this.selectedEmployeeCodeListValues = new Array<string>();
        if (this.basicInfo.controls.employeeCode.value != null && this.basicInfo.controls.employeeCode.value.includes(",")) {
          for (let ecode of this.basicInfo.controls.employeeCode.value.split(",")) {
            if (ecode != null && ecode.trim() != "") {
              this.selectedEmployeeCodeListValues.push(ecode);
              this.selectedEmployeeCodeList.push(ecode);
            }
          }
        } else {
          this.selectedEmployeeCodeListValues.push(this.basicInfo.controls.employeeCode.value);
          this.selectedEmployeeCodeList.push(this.basicInfo.controls.employeeCode.value);
        }

      }
    });

    this.organisationalInfo.controls.functionalAreaIDOrg.valueChanges.subscribe(data => {

      this.organisationalInfo.controls['requisitionRoleIDOrg'].setValue(0);
      if (data != null && data != '') {
        this.newJobService.getRequisitionRoleMaster(this.organisationalInfo.controls.functionalAreaIDOrg.value).subscribe(out => {
          this.messageCode = out.messageCode;
          this.requisitionRoleList = out.responseData;
        });
      } else {
        this.requisitionRoleList = null;
      }
    });

    this.skillInfo.controls.skill.valueChanges.subscribe(data => {
      if (this.skillSelectBoolean) {
        this.skillSelectBoolean = false;
        data = '';
      }
      this.newJobService.getSkillsByPattern(data, 50).subscribe(out => {
        this.skillList = out.responseData;
        this.removeSelectedSkillElements();
      });
    });

    this.workingLanguageInfo.controls.workingLanguage.valueChanges.subscribe(data => {
      if (this.languageSelectBoolean) {
        this.languageSelectBoolean = false;
        data = '';
      }
      this.newJobService.getLanguagesByPattern(data, 50).subscribe(out => {
        this.languageList = out.responseData;
        this.removeSelectedLanguageElements();
      });
    });

    this.certificationInfo.controls.certification.valueChanges.subscribe(data => {
      if (this.certificationSelectBoolean) {
        this.certificationSelectBoolean = false;
        data = '';
      }
      this.newJobService.getCertificationsByPattern(data, 50).subscribe(out => {
        this.certificationList = out.responseData;
        this.removeSelectedCertificationElements();
      });
    });

    this.minQualificationInfo.controls.minQualification.valueChanges.subscribe(data => {
      if (this.minQualificationSelectBoolean) {
        this.minQualificationSelectBoolean = false;
        data = '';
      }
      this.newJobService.getMinQualificationsByPattern(data, 50).subscribe(out => {
        this.minQualificationList = out.responseData;
        this.removeSelectedMinQualificationElements();
      });
    });

    this.basicInfo.valueChanges.subscribe(data => {
      this.getProfileCompletionRange();
    })
    this.organisationalInfo.valueChanges.subscribe(data => {
      this.getProfileCompletionRange();
    })
    this.skillInfo.valueChanges.subscribe(data => {
      this.getProfileCompletionRange();
    })
    this.jobDescriptionInfo.valueChanges.subscribe(data => {
      this.getProfileCompletionRange();
    })
    this.workflowInfo.valueChanges.subscribe(data => {
      this.getProfileCompletionRange();
    })
  }

  initializeBasicInfo() {
    this.basicInfo = this.formBuilder.group({
      jobTitle: ['',CustomValidatorService.crossSideScriptValidator(this.sanitizer)],
      noOfOpenings: ['', [Validators.minLength(0), Validators.maxLength(4)]],
      startDate: new FormControl(),
      endDate: new FormControl(),
      minimumExp: '',
      maximumExp: '',
      minSalary: 0,
      maxSalary: 0,
      minBudgetSalary: 0,
      maxBudgetSalary: 0,
      requisitionTypeID: '',
      gradeCode: '',
      designation: '',
      currency: '1',
      currencyDenominationID: '',
      hiringManagerID: '',
      employeeCategory: '',
      createdBy: '',
      jobFamily: '',
      position: '',
      campusType: '',
      programType: '',
      roleType: '',
      roleContribution: '',
      employmentTenure: '',
      resourceType: '',
      tat: ['', [Validators.minLength(0), Validators.maxLength(4)]],
      interviewPanelistSelected: [],
      confidential: false,
      bandId: 0,
      band: '',
      employeeCode: ''
    });
  }

  initializeOrganizationalInfo() {
    this.organisationalInfo = this.formBuilder.group({
      industryId: '',
      functionalAreaIDOrg: '',
      requisitionRoleIDOrg: ''
    });
  }

  initializeSkillInfo() {
    this.skillInfo = this.formBuilder.group({
      skill: ''
    });
  }

  initializeCertificationInfo() {
    this.certificationInfo = this.formBuilder.group({
      certification: ''
    });
  }

  initializeMinQualificationInfo() {
    this.minQualificationInfo = this.formBuilder.group({
      minQualification: ''
    });
  }

  initializeWorkingLanguageInfo() {
    this.workingLanguageInfo = this.formBuilder.group({
      workingLanguage: ''
    });
  }

  initializeJobDescriptionInfo() {
    this.jobDescriptionInfo = this.formBuilder.group({
      jobDescription: ''

    });
  }

  initializeWorkflowInfo() {
    this.workflowInfo = this.formBuilder.group({
      workflowID: ''
    });
  }

  initializeTemplateInfo() {
    this.addToTemplateInfo = this.formBuilder.group({
      jobTemplateName: '',
      save: new FormControl()
    });
  }

  initializeCTQInfo() {
    this.ctqInfo = this.formBuilder.group({
      ctqDescription: '',
      ctq: '',
      cqtRadioButton: ''
    });
    this.radioButtonEnabled = true;
  }

  applySecurity() {
    if (this.security.skillsDisabled)
      this.skillInfo.controls['skill'].disable();

    if (this.security.certificationDisabled)
      this.certificationInfo.controls['certification'].disable();

    if (this.security.minQualificationDisabled)
      this.minQualificationInfo.controls['minQualification'].disable();

    if (this.security.workingLanguageDisabled)
      this.workingLanguageInfo.controls['workingLanguage'].disable();

    if (this.security.interviewPanelDisabled)
      this.basicInfo.controls['interviewPanelistSelected'].disable();

    if (this.security.employeeCodeDisabled)
      this.basicInfo.controls['employeeCode'].disable();

  }

  private initFormSecurity() {

    this.securitySubscription = this.formSecurityService.getData().subscribe(
      response => {
        this.security = JSON.parse(JSON.stringify(response.responseData[0]));
        this.getProfileCompletionRange();
        let count = 1;
        this.basicInfoSequence = count++;
        if (this.security.organisationalSectionRendered) {
          this.organisationalInfoSequence = count++;
        }
        if (this.security.skillSectionRendered) {
          this.skillInfoSequence = count++;
        }
        if (this.security.jobdescriptionSectionRendered) {
          this.jobDescriptionInfoSequence = count++;
        }
        this.workflowInfoSequence = count++;
        if (this.security.ctqSectionRendered) {
          this.ctqInfoSequence = count++;
        }
        if (this.security.addtotemplateSectionRendered) {
          this.addToTemplateInfoSequence = count++;
        }
        this.voilaSequence = count++;
        this.applySecurity();
      }
    );

  }

  panelOpenClose(sectionid: string) {
    let count: number = 0;
    if (sectionid == 'basic') {
      this.basicsectionactive = false;
      count = 1;
    } else if (sectionid == 'organisational') {
      this.organisationalsectionactive = false;
      count = 2;
    } else if (sectionid == 'skill') {
      this.skillsectionactive = false;
      count = 3;
    } else if (sectionid == 'job') {
      this.jobsectionactive = false;
      count = 4;
    } else if (sectionid == 'work') {
      this.worksectionactive = false;
      count = 5;
    } else if (sectionid == 'ctq') {
      this.ctqsectionactive = false;
      count = 6;
    } else if (sectionid == 'add') {
      this.addsectionactive = false;
      count = 7;
    } else if (sectionid == 'vol') {
      this.volsectionactive = false;
      count = 8;
    }
    this.openNextPanel(count);
  }

  openNextPanel(count: number) {
    if ((2 > count) && this.security.organisationalSectionRendered) {
      this.organisationalsectionactive = true;
    } else if ((3 > count) && this.security.skillSectionRendered) {
      this.skillsectionactive = true;
    } else if ((4 > count) && this.security.jobdescriptionSectionRendered) {
      this.jobsectionactive = true;
    } else if ((5 > count) && this.security.workflowSectionRendered) {
      this.worksectionactive = true;
    } else if ((6 > count) && this.security.ctqSectionRendered) {
      this.ctqsectionactive = true;
    } else if ((7 > count) && this.security.addtotemplateSectionRendered) {
      this.addsectionactive = true;
    } else {
      this.volsectionactive = true;
    }
  }

  removeSelectedSkillElements() {
    if (this.selectedSkillList != null && this.skillList != null) {
      let lowercaseSkillList: Array<String> = [];
      for (let _skill of this.skillList) {
        lowercaseSkillList.push(_skill.toLowerCase());
      }

      for (let skill of this.selectedSkillList) {
        let index: number = lowercaseSkillList.indexOf(skill.text.toLowerCase());
        if (index !== -1) {
          lowercaseSkillList.splice(index, 1);
          this.skillList.splice(index, 1);
        }
      }
    }
  }

  removeSelectedCertificationElements() {
    if (this.selectedCertificationList != null && this.certificationList != null) {
      let lowercaseCertificationList: Array<String> = [];
      for (let _certification of this.certificationList) {
        lowercaseCertificationList.push(_certification.toLowerCase());
      }

      for (let certification of this.selectedCertificationList) {
        let index: number = lowercaseCertificationList.indexOf(certification.toLowerCase());
        if (index !== -1) {
          lowercaseCertificationList.splice(index, 1);
          this.certificationList.splice(index, 1);
        }
      }
    }
  }


  removeSelectedMinQualificationElements() {
    if (this.selectedMinQualificationList != null && this.minQualificationList != null) {
      let lowercaseMinQualificationList: Array<String> = [];
      for (let _minQualification of this.minQualificationList) {
        lowercaseMinQualificationList.push(_minQualification.toLowerCase());
      }

      for (let minQualification of this.selectedMinQualificationList) {
        let index: number = lowercaseMinQualificationList.indexOf(minQualification.toLowerCase());
        if (index !== -1) {
          lowercaseMinQualificationList.splice(index, 1);
          this.minQualificationList.splice(index, 1);
        }
      }
    }
  }

  removeSelectedLanguageElements() {
    if (this.selectedLanguageList != null && this.languageList != null) {
      let lowercaseLanguageList: Array<String> = [];
      for (let _language of this.languageList) {
        lowercaseLanguageList.push(_language.toLowerCase());
      }

      for (let language of this.selectedLanguageList) {
        let index: number = lowercaseLanguageList.indexOf(language.toLowerCase());
        if (index !== -1) {
          lowercaseLanguageList.splice(index, 1);
          this.languageList.splice(index, 1);
        }
      }
    }
  }

  removeSuggestedSkillElements() {
    if (this.selectedSkillList != null && this.filteredSuggestedSkillList != null) {
      let lowercaseSkillList: Array<String> = [];
      for (let _skill of this.filteredSuggestedSkillList) {
        lowercaseSkillList.push(_skill.toLowerCase());
      }
      for (let skill of this.selectedSkillList) {
        let index: number = lowercaseSkillList.indexOf(skill.text.toLowerCase());
        if (index !== -1) {
          lowercaseSkillList.splice(index, 1);
          this.filteredSuggestedSkillList.splice(index, 1);
        }
      }
    }
  }

  getSuggestedSkills() {
    if (this.basicInfo.controls.jobTitle.value == undefined) {
      this.suggestedSkillList = [];
      this.showLoader = false;
    } else {
      this.suggestedSkillLoaderShow = true;
      this.newJobService.getSuggestedSkills(this.basicInfo.controls.jobTitle.value).subscribe(out => {
        this.suggestedSkillList = out.res;
        if (this.suggestedSkillList == undefined || this.suggestedSkillList.length == 0) {
          this.skillMessage = "No skill suggestions available";
          this.filteredSuggestedSkillList = [];
        } else {
          this.skillMessage = "";
          this.filteredSuggestedSkillList = this.getDuplicateStringArray(this.suggestedSkillList);
          this.removeSuggestedSkillElements();
        }
        this.suggestedSkillLoaderShow = false;
        this.showLoader = false;
      }, error => {
        //this.notificationService.setMessage(NotificationSeverity.INFO, "Something went wrong, please contact your administrator!");
        this.skillMessage = "No skill suggestions available";
        this.filteredSuggestedSkillList = [];
        this.suggestedSkillLoaderShow = false;
        this.showLoader = false;
      });
    }

  }

  goBack() {
    this.location.back();
  }

  createRange(min, noOfItems) {
    var items: SelectItem[] = [];
    for (var i = min; i <= (min + noOfItems - 1); i++) {
      items.push(new SelectItem(i, i));
    }
    return items;
  }

  getMMRange() {
    var items: SelectItem[] = [];
    items.push(new SelectItem('Jan', 0));
    items.push(new SelectItem('Feb', 1));
    items.push(new SelectItem('Mar', 2));
    items.push(new SelectItem('Apr', 3));
    items.push(new SelectItem('May', 4));
    items.push(new SelectItem('Jun', 5));
    items.push(new SelectItem('Jul', 6));
    items.push(new SelectItem('Aug', 7));
    items.push(new SelectItem('Sep', 8));
    items.push(new SelectItem('Oct', 9));
    items.push(new SelectItem('Nov', 10));
    items.push(new SelectItem('Dec', 11));
    return items;
  }

  toTitleCase(str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    ).replace(/\s/g, "");
  }

  ngOnInit() {
    this.i18UtilService.get('common.label.continue').subscribe((res: string) => {
      this.langContinueButtonLabel = res;
    });
    this.i18UtilService.get('common.label.automatch').subscribe((res: string) => {
      this.autoMatchLabel = res;
    });

    this.i18UtilService.get('common.label.enter').subscribe((res: string) => {
      this.langEnterPlaceholder = res;
    });

    this.i18UtilService.get('common.label.select').subscribe((res: string) => {
      this.langSelectPlaceholder = res;
    });

    this.i18UtilService.get('job.new_job.createNewJob').subscribe((res: string) => {
      this.langPageTitle = res;
    });

    this.i18UtilService.get('job.new_job.saveJob').subscribe((res: string) => {
      this.lable = res;
    });

    if (this.stageID != null && this.workflowStageType != null) {
      this.newJobService.getWorkflowStageActions(this.stageID, this.workflowStageType).subscribe(response => {
        this.actionList = response.responseData;
        for (let action of this.actionList) {
          if (action.stageActionVisible == true) {
            this.commentVisible = true;
          }
        }
      });
    }
    this.interviewPanelLoaded = false;
    if (this.ACTIVATED_ROUTE.snapshot.params['id'] != null) {

      this.i18UtilService.get('job.new_job.editJob').subscribe((res: string) => {
        this.langPageTitle = res;
      });

      this.pageTitle = this.langPageTitle;
      if (this.ACTIVATED_ROUTE.snapshot.queryParams['type'] === 'draft') {
        this.draftRequisitionID = this.ACTIVATED_ROUTE.snapshot.params['id'];
        this.newJobService.getDraftDetails(this.draftRequisitionID).subscribe(result => {
          this.setJobDetails(result);
          this.basicsavestatus = true;
          this.removeSelectedSkillElements();
        });
        this.continueButtonLabel = this.langContinueButtonLabel;
        this.isJobEdit = false;
      }
      else {
        this.requisitionID = this.ACTIVATED_ROUTE.snapshot.params['id'];
        this.newJobService.getJobDetails(this.requisitionID).subscribe(result => {
          this.setJobDetails(result);
          this.basicsavestatus = true;
          this.removeSelectedSkillElements();
        });
        this.i18UtilService.get('common.label.update').subscribe((res: string) => {
          this.continueButtonLabel = res;
        });
        this.isJobEdit = true;
      }
      this.autoMatchCandidatesService.jobSummaryData = null;
    } else {

      this.i18UtilService.get('job.new_job.createNewJob').subscribe((res: string) => {
        this.langPageTitle = res;
      });

      this.pageTitle = this.langPageTitle;
      this.continueButtonLabel = this.langContinueButtonLabel;
      Promise.all([this.newJobService.getOrgunitTypesJSONPopulated(null), this.newJobService.getWorksiteTypesJSONPopulated(null)])
        .then(res => {
          this.populateOrgunit(res[0]);
          this.populateWorksite(res[1]);
          this.showLoader = false;
        });
      this.syncGradeDesigCall.next("flagSync1");
      this.isJobEdit = false;
    }
    if (this.isJobEdit == true) {
      this.basicInfoComplete = true;
      this.organisationalInfoComplete = true;
      this.skillInfoComplete = true;
      this.jobDescriptionInfoComplete = true;
      this.workflowInfoComplete = true;
      this.ctqInfoComplete = true;
      this.addToTemplateInfoComplete = true;
    }

    this.jobDescriptionInfo.controls.jobDescription.valueChanges.subscribe(val => {
      if (val == null)
        this.jobDescLength = 0;
      else
        this.jobDescLength = this.calculateJobDescLength(val);
    });

    this.ctqInfo.controls.ctqDescription.valueChanges.subscribe(val => {
      if (val == null)
        this.sourcingGuidLineLength = 0;
      else
        this.sourcingGuidLineLength = val.length;
    });

  }

  calculateJobDescLength(jobDesc) {
    let htmlRegex = /(&nbsp;|<([^>]+)>)/ig;
    let cssRegex = /((<style>)|(<style type=.+))((\s+)|(\S+)|(\r+)|(\n+))(.+)((\s+)|(\S+)|(\r+)|(\n+))(<\/style>)/g;
    let commentsRegex = /\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm;
    let keyboardAscii = /[^\x00-\x7F]/g;
    let spacesRegex = /\s+/g;
    let plainTextJD = jobDesc.replace(/<br\s*\/?>/mg, "\n").replace(commentsRegex, '').replace(cssRegex, '').replace(htmlRegex, "").replace(keyboardAscii, "").replace(spacesRegex,"").trim();
    

    this.jobDescLength = plainTextJD.length;
    return this.jobDescLength;

  }
  populateOrgunit(res: any) {
    this.messageCode = res.messageCode;
    this.orgunitMastersList = res.responseData;
    this.selectedOrgunitID = 0;
    for (let master of this.orgunitMastersList) {
      this.organisationalInfo.addControl(master.orgunitType, new FormControl());
      this.organisationalInfo.controls[master.orgunitType].setValue(master.selectedUnitID);
      if (master.selectedUnitID != 0) {
        this.selectedOrgunitID = master.selectedUnitID;
      }
    }
  }
  populateWorksite(res: any) {
    this.messageCode = res.messageCode;
    this.worksiteMastersList = res.responseData;
    this.selectedWorksiteID = 0;
    for (let master of this.worksiteMastersList) {
      this.organisationalInfo.addControl(master.worksiteType, new FormControl());
      this.organisationalInfo.controls[master.worksiteType].setValue(master.selectedUnitID);
      if (master.selectedUnitID != 0) {
        this.selectedWorksiteID = master.selectedUnitID;
      }
    }
  }

  populateOrgunitTypes(orgunitHierarchy: String) {
    this.newJobService.getOrgunitTypesJSONPopulated(orgunitHierarchy).then(out => {
      this.messageCode = out.messageCode;
      this.orgunitMastersList = out.responseData;
      this.selectedOrgunitID = 0;
      for (let master of this.orgunitMastersList) {
        this.organisationalInfo.addControl(master.orgunitType, new FormControl());
        this.organisationalInfo.controls[master.orgunitType].setValue(master.selectedUnitID);
        if (master.selectedUnitID != 0) {
          this.selectedOrgunitID = master.selectedUnitID;
        }
      }
    });
  }

  populateWorksiteTypes(worksiteHierarchy: String) {
    this.newJobService.getWorksiteTypesJSONPopulated(worksiteHierarchy).then(out => {
      this.messageCode = out.messageCode;
      this.worksiteMastersList = out.responseData;
      this.selectedWorksiteID = 0;
      for (let master of this.worksiteMastersList) {
        this.organisationalInfo.addControl(master.worksiteType, new FormControl());
        this.organisationalInfo.controls[master.worksiteType].setValue(master.selectedUnitID);
        if (master.selectedUnitID != 0) {
          this.selectedWorksiteID = master.selectedUnitID;
        }
      }
    });
  }

  setJobDetails(result: any) {
    let basicDetails = result.responseData.NewJobBasicDetailsRequest;
    /*Set Basic Info in Form*/
    this.requisitionCode = basicDetails.requisitionCode;
    this.basicInfo.controls['jobTitle'].setValue(basicDetails.jobTitle);
    this.basicInfo.controls['confidential'].setValue(basicDetails.confidential);
    this.basicInfo.controls['bandId'].setValue(basicDetails.bandId);
    this.basicInfo.controls['band'].setValue(basicDetails.band);
    if (basicDetails.noOfOpenings != null) {
      this.basicInfo.controls['noOfOpenings'].setValue(basicDetails.noOfOpenings + '');
    }
    if (basicDetails.startDate != null) {
      const [_startDateDD, _startDateMM, _startDateYYYY] = basicDetails.startDate.split('-');
      this.setStartDate(new Date(parseInt(_startDateYYYY), parseInt(_startDateMM) - 1, parseInt(_startDateDD)));

    }
    if (basicDetails.closingDate != null) {
      const [_closingDateDD, _closingDateMM, _closingDateYYYY] = basicDetails.closingDate.split('-');
      this.setEndDate(new Date(parseInt(_closingDateYYYY), parseInt(_closingDateMM) - 1, parseInt(_closingDateDD)));
    }

    if (basicDetails.confidential != undefined && basicDetails.confidential != null) {
      this.basicInfo.controls['confidential'].setValue(basicDetails.confidential);
    }

    if (basicDetails.minimumExp != null) {
      this.basicInfo.controls['minimumExp'].setValue(basicDetails.minimumExp);
    }

    if (basicDetails.maximumExp != null) {
      this.basicInfo.controls['maximumExp'].setValue(basicDetails.maximumExp);
    }

    if (basicDetails.minSalary != null)
      this.basicInfo.controls['minSalary'].setValue(basicDetails.minSalary);

    if (basicDetails.maxSalary != null)
      this.basicInfo.controls['maxSalary'].setValue(basicDetails.maxSalary);
    
      if (basicDetails.minBudgetSalary != null)
      this.basicInfo.controls['minBudgetSalary'].setValue(basicDetails.minBudgetSalary);

    if (basicDetails.maxBudgetSalary != null)
      this.basicInfo.controls['maxBudgetSalary'].setValue(basicDetails.maxBudgetSalary);

    if (basicDetails.requisitionTypeID != null) {
      this.basicInfo.controls['requisitionTypeID'].setValue(basicDetails.requisitionTypeID);
    }

    if (basicDetails.employeeCategoryID != null) {
      this.basicInfo.controls['employeeCategory'].setValue(basicDetails.employeeCategoryID);
    }

    if (basicDetails.jobFamilyID != null) {
      this.basicInfo.controls['jobFamily'].setValue(basicDetails.jobFamilyID);
    }

    if (basicDetails.positionID != null) {
      this.basicInfo.controls['position'].setValue(basicDetails.positionID);
    }

    if (basicDetails.campusTypeID != null) {
      this.basicInfo.controls['campusType'].setValue(basicDetails.campusTypeID);
    }
    
    if (basicDetails.programTypeID != null) {
      this.basicInfo.controls['programType'].setValue(basicDetails.programTypeID);
    }
    
    if (basicDetails.roleTypeID != null) {
      this.basicInfo.controls['roleType'].setValue(basicDetails.roleTypeID);
    }
    
    if (basicDetails.roleContributionID != null) {
      this.basicInfo.controls['roleContribution'].setValue(basicDetails.roleContributionID);
    }
    
    if (basicDetails.employmentTenureID != null) {
      this.basicInfo.controls['employmentTenure'].setValue(basicDetails.employmentTenureID);
    }

    if (basicDetails.resourceTypeID != null) {
      this.basicInfo.controls['resourceType'].setValue(basicDetails.resourceTypeID);
    }

    if (basicDetails.tat != null) {
      this.basicInfo.controls['tat'].setValue(basicDetails.tat + '');
    }

    if (basicDetails.createdBy != null) {
      this.createdBy = basicDetails.createdBy;
    }

    if (basicDetails.gradeCode != null)
      this.basicInfo.controls['gradeCode'].setValue(basicDetails.gradeCode);

    if (basicDetails.designation != null)
      this.basicInfo.controls['designation'].setValue(basicDetails.designation);

    if (basicDetails.currencyDenominationID != null) {
      this.basicInfo.controls['currencyDenominationID'].setValue(basicDetails.currencyDenominationID);
    }
    this.basicInfo.controls['currency'].setValue(1);

    if (basicDetails.hiringManagerID != null) {
      this.basicInfo.controls['hiringManagerID'].setValue(basicDetails.hiringManagerID);
    }

    if (basicDetails.positionID != null) {
      this.populatePositionDefault(basicDetails.positionID, basicDetails.designation);
    } else {
      this.syncGradeDesigCall.next("flagSync3");
    }
    if (basicDetails.interviewPanelistSelected != null) {
      this.interviewPanelistSelected = basicDetails.interviewPanelistSelected;
      if (this.interviewPanelLoaded) {
        this.basicInfo.controls['interviewPanelistSelected'].setValue(basicDetails.interviewPanelistSelected);
      }
    }

    if (basicDetails.selectedEmployeeCodeListValues != null) {
      this.selectedEmployeeCodeListValues = basicDetails.selectedEmployeeCodeListValues;
      this.selectedEmployeeCodeList = basicDetails.selectedEmployeeCodeList;

      if (!this.isECodeIsDropDown) {
        let selectedECodes = [];
        for (let ecode of this.selectedEmployeeCodeList) {
          selectedECodes.push(ecode.value);
        }
        this.basicInfo.controls['employeeCode'].setValue(selectedECodes.toString());
      }

    }

    if (basicDetails.hiringManagerName != null)
      this.hiringManagerName = basicDetails.hiringManagerName;

    this.getSuggestedSkills();
    let basicDetailsRequestData = { input: basicDetails };
    this.checkBasicMandatory(basicDetailsRequestData);
    let organisationalDetails = result.responseData.NewJobOrganisationalDetailsRequest;

    if (organisationalDetails.functionalAreaIDOrg != null)
      this.organisationalInfo.controls['functionalAreaIDOrg'].setValue(organisationalDetails.functionalAreaIDOrg);

    if (organisationalDetails.industryID != null) {
      organisationalDetails.industryId = organisationalDetails.industryID;
      this.organisationalInfo.controls['industryId'].setValue(organisationalDetails.industryID);
    }


    if (organisationalDetails.requisitionRoleIDOrg != null)
      this.organisationalInfo.controls['requisitionRoleIDOrg'].setValue(organisationalDetails.requisitionRoleIDOrg);


    this.populateOrgunitTypes(organisationalDetails.orgunitHierarchy);
    this.populateWorksiteTypes(organisationalDetails.worksiteHierarchy);
    let organisationalDetailsRequestData = { input: organisationalDetails };
    this.checkOrganisationMandatory(organisationalDetailsRequestData);
    let skillDetails = result.responseData.NewJobSkillDetailsRequest;
    if (skillDetails.skillList != null) {
      this.selectedSkillList = skillDetails.skillList;
    }
    if (skillDetails.certificationList != null) {
      this.selectedCertificationList = skillDetails.certificationList;
    }
    if (skillDetails.minQualificationList != null) {
      this.selectedMinQualificationList = skillDetails.minQualificationList;
    }
    if (skillDetails.languageList != null) {
      this.selectedLanguageList = skillDetails.languageList;
    }
    this.checkSkillMandatory(skillDetails);
    let jobDescriptionDetails = result.responseData.NewJobDescriptionDetailsRequest;

    if (jobDescriptionDetails.jobDescription != null) {
      this.jobDescriptionInfo.controls['jobDescription'].setValue(jobDescriptionDetails.jobDescription);
      this.jobDescLength = this.calculateJobDescLength(this.jobDescriptionInfo.controls.jobDescription.value.trim());
    }

    let jobDescriptionDetailsRequestData = { input: jobDescriptionDetails };
    this.checkDescriptionMandatory(jobDescriptionDetailsRequestData);
    let workflowDetails = result.responseData.NewJobWorkflowDetailsRequest;
    if (workflowDetails.workflowID != null)
      this.workflowInfo.controls['workflowID'].setValue(workflowDetails.workflowID);
    let newWorkflowDetails = { input: { workflowID: workflowDetails.workflowID } };
    this.checkWorkflowMandatory(newWorkflowDetails);

    let ctqDetails = result.responseData.NewJobCtqDetailsRequest;

    if (ctqDetails.sourcingGuidline != null)
      this.ctqInfo.controls['ctqDescription'].setValue(ctqDetails.sourcingGuidline.trim());

    let CTQDetailsRequestData = { input: ctqDetails };
    this.checkCTQMandatory(CTQDetailsRequestData);

    if (ctqDetails.sourcingGuidline != null) {
      let htmlRegex = /(&nbsp;|<([^>]+)>)/ig;
      let cssRegex = /((<style>)|(<style type=.+))((\s+)|(\S+)|(\r+)|(\n+))(.+)((\s+)|(\S+)|(\r+)|(\n+))(<\/style>)/g;
      let commentsRegex = /\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm;
      let spacesRegex = /\s\s+/g;
      let newLineRegex = /\n\s*\n/g;
      let keyboardAscii = /[^\x00-\x7F]/g;
      this.ctqInfo.controls['ctqDescription'].setValue(ctqDetails.sourcingGuidline.replace(/<br\s*\/?>/mg, "\n").replace(commentsRegex, '').replace(cssRegex, '').replace(htmlRegex, "").replace(keyboardAscii, "").replace(newLineRegex, "\n").trim());
      this.sourcingGuidLineLength = ctqDetails.sourcingGuidline.trim().length;
    }
    if (ctqDetails.ctqId != 0) {
      this.ctqInfo.controls['ctq'].setValue(ctqDetails.ctqId);
      this.enableCTQ = true;
    } else {
      this.enableCTQ = false;
    }

    this.removedQuestionList = [];
    this.newJobService.getCTQMaster(ctqDetails.ctqId).subscribe(out => {
      this.ctqData = out.responseData;
      let errorList: CtqMaster[] = [];
      for (let item of this.ctqData) {
        let index = ctqDetails.ctqQuestion.indexOf(item.questionId)
        if (index < 0) {
          item.id = item.questionId;
          item.name = item.question;
          errorList.push(item);
          this.removedQuestionList.push(item);
        }
      }
      this.ctqData = this.ctqData.filter(
        function (x) {
          return errorList.indexOf(x) < 0;
        }
      );
      if (this.ctqData != undefined && this.ctqData != null) {
        for (let ctq of this.ctqData) {
          if (ctq.questionType == 'SELECTONEMENU') {
            ctq.selectItemOptions = this.getOptions(ctq.options)
          }
        }
      }
    });

    this.ctqInfoComplete = true;
    this.addToTemplateInfoComplete = true;

    this.syncGradeDesigCall.next("flagSync1");

  }

  getRequisitionRoles(): void {
    this.newJobService.getRequisitionRoleMaster(this.organisationalInfo.controls.functionalAreaIDOrg.value)
      .subscribe(out => this.requisitionRoleList = out.responseData);
  }

  public skillSelectListener(skillName: string) {
    if (!this.security.skillsDisabled) {

      if (this.selectedSkillList == undefined) {
        this.selectedSkillList = new Array<SkillType>();
      }
      let found = false;
      for (let skill of this.selectedSkillList) {
        if (skill.text.toLowerCase() == skillName.toLowerCase()) {
          found = true;
        }
      }
      if (!found) {
        let newSkill = new SkillType();
        newSkill.text = skillName;
        newSkill.mandatory = false;
        this.selectedSkillList.push(newSkill);
      }
      this.removeSuggestedSkillElements();
      this.skillInfo.controls['skill'].setValue('');
      this.skillSelectBoolean = true;
      if (this.filteredSuggestedSkillList.length == 0) {
        this.skillMessage = "All suggested skills have been added"
      }
    }
  }
  public certificationSelectListener(certificationName: string) {
    if (this.selectedCertificationList == undefined) {
      this.selectedCertificationList = new Array<string>();
    }
    if (!this.selectedCertificationList.includes(certificationName)) {
      this.selectedCertificationList.push(certificationName);
    }
    this.certificationInfo.controls['certification'].setValue('');
    this.certificationSelectBoolean = true;
  }

  public minQualificationSelectListener(minQualificationName: string) {
    if (this.selectedMinQualificationList == undefined) {
      this.selectedMinQualificationList = new Array<string>();
    }
    if (!this.selectedMinQualificationList.includes(minQualificationName)) {
      this.selectedMinQualificationList.push(minQualificationName);
    }
    this.minQualificationInfo.controls['minQualification'].setValue('');
    this.minQualificationSelectBoolean = true;
  }

  public languageSelectListener(languageName: string) {
    if (this.selectedLanguageList == undefined) {
      this.selectedLanguageList = new Array<string>();
    }
    if (!this.selectedLanguageList.includes(languageName)) {
      this.selectedLanguageList.push(languageName);
    }
    this.workingLanguageInfo.controls['workingLanguage'].setValue('');
    this.languageSelectBoolean = true;
  }

  public focusOutSkills() {
    this.skillInfo.controls['skill'].setValue('');
  }
  public focusOutMinQualification() {
    this.minQualificationInfo.controls['minQualification'].setValue('');
  }
  public focusOutWorkingLanguage() {
    this.workingLanguageInfo.controls['workingLanguage'].setValue('');
  }

  public focusOutCertification() {
    this.certificationInfo.controls['certification'].setValue('');
  }


  public requestionTypeChangeListener() {
    this.basicInfo.controls['employeeCode'].setValue('');
    this.selectedEmployeeCodeListValues = new Array<string>();
    this.selectedEmployeeCodeList = null;
  }

  public employeeCodeSelectListener(employeeCodeTO: SelectItem) {
    if (this.selectedEmployeeCodeListValues == null) {
      this.selectedEmployeeCodeListValues = new Array<string>();
    }
    if (this.selectedEmployeeCodeList == null) {
      this.selectedEmployeeCodeList = new Array<SelectItem>();
    }
    if (!this.selectedEmployeeCodeListValues.includes(employeeCodeTO.value)) {
      this.selectedEmployeeCodeListValues.push(employeeCodeTO.value);
      this.selectedEmployeeCodeList.push(employeeCodeTO);
    }
    this.basicInfo.controls['employeeCode'].setValue('');
  }

  public employeeCodeDeselectListener(employeeCodeTO: SelectItem) {
    let resultList: Array<SelectItem> = [];
    let resultListValues: Array<String> = [];
    for (let codeTO of this.selectedEmployeeCodeList) {
      if (codeTO.value != employeeCodeTO.value) {
        resultList.push(codeTO);
        resultListValues.push(codeTO.value);
      }
    }
    this.selectedEmployeeCodeList = resultList;
    this.selectedEmployeeCodeListValues = resultListValues;
  }

  public getDuplicateStringArray(array: Array<string>): Array<string> {
    let newArray: Array<string> = [];
    if (array != undefined && array.length > 0) {
      for (let arr of array) {
        newArray.push(arr);
      }
    }
    return newArray;

  }

  public toggleSkillType(selectedSkill: SkillType) {

    if (!this.security.skillsDisabled) {
      for (let skill of this.selectedSkillList) {
        if (skill.text == selectedSkill.text) {
          skill.mandatory = !skill.mandatory;
        }
      }
    }
  }

  public skillDeleteListener(skillName: SkillType) {
    if (!this.security.skillsDisabled) {
      let slicedSkills: Array<SkillType> = new Array<SkillType>();
      for (let skill of this.selectedSkillList) {
        if (skill.text != skillName.text) {
          slicedSkills.push(skill);
        }
        if (this.selectedSkillList.length != 0) {
          this.skillMessage = "";
        }

      }
      this.selectedSkillList = slicedSkills;
      this.newJobService.getSkillsByPattern('', 50).subscribe(out => {
        this.skillList = out.responseData;
        this.removeSelectedSkillElements();
      });
      this.filteredSuggestedSkillList = this.getDuplicateStringArray(this.suggestedSkillList);
      this.removeSuggestedSkillElements();
    }
  }

  public certificationDeleteListener(certificationName: string) {
    let slicedCertifications: Array<string> = new Array<string>();
    for (let certification of this.selectedCertificationList) {
      if (certification != certificationName) {
        slicedCertifications.push(certification);
      }
      if (this.selectedCertificationList.length != 0) {
        this.certificationMessage = "";
      }

    }
    this.selectedCertificationList = slicedCertifications;
    this.newJobService.getCertificationsByPattern('', 50).subscribe(out => {
      this.certificationList = out.responseData;
      this.removeSelectedCertificationElements();
    });
  }

  public minQualificationDeleteListener(minQualificationName: string) {
    let slicedMinQualifications: Array<string> = new Array<string>();
    for (let minQualification of this.selectedMinQualificationList) {
      if (minQualification != minQualificationName) {
        slicedMinQualifications.push(minQualification);
      }
      if (this.selectedMinQualificationList.length != 0) {
        this.minQualificationMessage = "";
      }

    }
    this.selectedMinQualificationList = slicedMinQualifications;
    this.newJobService.getMinQualificationsByPattern('', 50).subscribe(out => {
      this.minQualificationList = out.responseData;
      this.removeSelectedMinQualificationElements();
    });
  }

  public languageDeleteListener(languageName: string) {
    let slicedLanguages: Array<string> = new Array<string>();
    for (let language of this.selectedLanguageList) {
      if (language != languageName) {
        slicedLanguages.push(language);
      }
      if (this.selectedLanguageList.length != 0) {
        this.languageMessage = "";
      }

    }
    this.selectedLanguageList = slicedLanguages;
    this.newJobService.getLanguagesByPattern('', 50).subscribe(out => {
      this.languageList = out.responseData;
      this.removeSelectedLanguageElements();
    });
  }

  orgunitSelectionChangeListener(selectedOrgunitType: string, selectedOrgunitID: any) {
    this.selectedOrgunitID = selectedOrgunitID;
    this.orgunitSelectionChange(selectedOrgunitType, selectedOrgunitID);
  }

  orgunitSelectionChange(selectedOrgunitType: string, orgunitID: Number) {
    this.newJobService.getOrgunitsByParentID(orgunitID).subscribe(out => {
      let trigger: boolean = false;
      let afterTrigger: boolean = false;
      let orgunitType: string;
      for (let master of this.orgunitMastersList) {
        if (afterTrigger) {
          master.orgunits = null;
          this.organisationalInfo.controls[master.orgunitType].setValue(null);
        }
        if (trigger) {
          master.orgunits = out.responseData;
          this.organisationalInfo.controls[master.orgunitType].setValue(null);
          trigger = false;
          afterTrigger = true;
        }
        if (master.orgunitType == selectedOrgunitType) {
          trigger = true;
        }
      }
    });
  }


  worksiteSelectionChangeListener(selectedWorksiteType: string, selectedWorksiteID: any) {
    this.selectedWorksiteID = selectedWorksiteID;
    this.worksiteSelectionChange(selectedWorksiteType, selectedWorksiteID);
  }

  worksiteSelectionChange(selectedWorksiteType: string, worksiteID: Number) {
    this.newJobService.getWorksitesByParentID(worksiteID).subscribe(out => {
      let trigger: boolean = false;
      let afterTrigger: boolean = false;
      let worksiteType: string;
      for (let master of this.worksiteMastersList) {
        if (afterTrigger) {
          master.worksites = null;
          this.organisationalInfo.controls[master.worksiteType].setValue(null);
        }
        if (trigger) {
          master.worksites = out.responseData;
          this.organisationalInfo.controls[master.worksiteType].setValue(null);
          trigger = false;
          afterTrigger = true;
        }
        if (master.worksiteType == selectedWorksiteType) {
          trigger = true;
        }
      }
    });
  }

  ctqOptionChangeListener(selectedValue: string, ctq: CtqMaster) {
    ctq.selectedOption = selectedValue;
  }

  hiringManagerSelectionChangeListener(event: any) {
    this.hiringManagerName = event.source.selected.viewValue;
  }

  getSaveJobRequest() {
    let saveJobRequest: SaveJobRequest = new SaveJobRequest()
    saveJobRequest.input = new CreateJobRequestDetails();
    saveJobRequest.input.draftRequisitionID = this.draftRequisitionID;
    saveJobRequest.input.requisitionID = this.requisitionID;
    saveJobRequest.input.jobTitle = this.basicInfo.controls.jobTitle.value;
    saveJobRequest.input.confidential = this.basicInfo.controls.confidential.value;
    saveJobRequest.input.bandId = this.basicInfo.controls.bandId.value;
    saveJobRequest.input.band = this.basicInfo.controls.band.value;
    saveJobRequest.input.tatPolicyEnabled = (this.tatPolicyEnabled && this.security.tatRequired);
    saveJobRequest.input.recruitWorkflowPolicyEnabled = this.recruitWorkflowPolicyEnabled;
    saveJobRequest.input.recruitApprovalPolicyEnabled = this.recruitApprovalPolicyEnabled;
    saveJobRequest.input.closingDateAutopopulateEnabled = this.closingDateAutopopulateEnabled;
    if (this.basicInfo.controls.noOfOpenings.value == null || this.basicInfo.controls.noOfOpenings.value == '') {
      saveJobRequest.input.noOfOpenings = 0;
    }

    saveJobRequest.input.noOfOpenings = this.basicInfo.controls.noOfOpenings.value;
    saveJobRequest.input.startDate = this.basicInfo.controls.startDate.value;
    saveJobRequest.input.closingDate = this.basicInfo.controls.endDate.value;
    saveJobRequest.input.minimumExp = this.basicInfo.controls.minimumExp.value;
    saveJobRequest.input.maximumExp = this.basicInfo.controls.maximumExp.value;
    saveJobRequest.input.minSalary = this.basicInfo.controls.minSalary.value;
    saveJobRequest.input.maxSalary = this.basicInfo.controls.maxSalary.value;
    saveJobRequest.input.minBudgetSalary = this.basicInfo.controls.minBudgetSalary.value;
    saveJobRequest.input.maxBudgetSalary = this.basicInfo.controls.maxBudgetSalary.value;
    if (this.basicInfo.controls.requisitionTypeID.value == '') {
      saveJobRequest.input.requisitionTypeID = null;
    } else {
      saveJobRequest.input.requisitionTypeID = this.basicInfo.controls.requisitionTypeID.value;
    }
    saveJobRequest.input.selectedEmployeeCodeListValues = this.selectedEmployeeCodeListValues;
    saveJobRequest.input.gradeCode = this.basicInfo.controls.gradeCode.value;
    saveJobRequest.input.gradeID = this.selectedGradeID;
    saveJobRequest.input.industryId = this.organisationalInfo.controls.industryId.value;
    saveJobRequest.input.designation = this.basicInfo.controls.designation.value;
    saveJobRequest.input.currencyDenominationID = this.basicInfo.controls.currencyDenominationID.value;
    saveJobRequest.input.hiringManagerID = this.basicInfo.controls.hiringManagerID.value;
    saveJobRequest.input.hiringManagerName = this.hiringManagerName;
    saveJobRequest.input.interviewPanelistSelected = this.basicInfo.controls.interviewPanelistSelected.value;
    saveJobRequest.input.functionalAreaIDOrg = this.organisationalInfo.controls.functionalAreaIDOrg.value;
    saveJobRequest.input.requisitionRoleIDOrg = this.organisationalInfo.controls.requisitionRoleIDOrg.value;
    saveJobRequest.input.organizationUnitID = this.selectedOrgunitID;
    saveJobRequest.input.worksiteID = this.selectedWorksiteID;
    saveJobRequest.input.skillList = this.selectedSkillList;
    saveJobRequest.input.certificationList = this.selectedCertificationList;
    saveJobRequest.input.minQualificationList = this.selectedMinQualificationList;
    saveJobRequest.input.languageList = this.selectedLanguageList;

    if (this.jobDescriptionInfo.controls.jobDescription.value == null) {
      saveJobRequest.input.jobDescription = null;
    } else {
      saveJobRequest.input.jobDescription = this.jobDescriptionInfo.controls.jobDescription.value;
    }
    saveJobRequest.input.workflowID = this.workflowInfo.controls.workflowID.value;
    saveJobRequest.input.employeeCategoryId = this.basicInfo.controls.employeeCategory.value;
    saveJobRequest.input.jobFamilyId = this.basicInfo.controls.jobFamily.value;
    saveJobRequest.input.positionId = this.basicInfo.controls.position.value;
    saveJobRequest.input.campusTypeId = this.basicInfo.controls.campusType.value;
    saveJobRequest.input.programTypeId = this.basicInfo.controls.programType.value;
    saveJobRequest.input.roleTypeId = this.basicInfo.controls.roleType.value;
    saveJobRequest.input.roleContributionId = this.basicInfo.controls.roleContribution.value;
    saveJobRequest.input.employmentTenureId = this.basicInfo.controls.employmentTenure.value;
    saveJobRequest.input.resourceTypeId = this.basicInfo.controls.resourceType.value;
    saveJobRequest.input.tat = this.basicInfo.controls.tat.value;
    saveJobRequest.input.orgUnitId = this.selectedOrgunitID;
    saveJobRequest.input.sourcingGuidline = this.ctqInfo.controls.ctqDescription.value;
    saveJobRequest.input.stageStatusID = this.stageID;
    saveJobRequest.input.isOrganizationUnitRendered = this.security.organizationUnitRendered;
    saveJobRequest.input.isJoiningLocationRendered = this.security.joiningLocationRendered;
    saveJobRequest.input.ctqID = this.ctqInfo.controls.ctq.value;
    saveJobRequest.input.ctqQuesIDs = [];
    saveJobRequest.input.ctqRemovedIDs = [];
    if(this.comments != null && this.comments != ''){
      saveJobRequest.input.comments = this.comments;
    }
    if (this.enableCTQ) {
      if (this.ctqData == null || this.ctqData == undefined || this.ctqData.length == 0) {
        this.i18UtilService.get('job.error.ctqQuestionError').subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        this.saveJobDetailsButtonBlock = false;
        return saveJobRequest;
      }
      for (let item of this.ctqData) {
        saveJobRequest.input.ctqQuesIDs.push(item.questionId);
      }
      for (let item of this.removedQuestionList) {
        saveJobRequest.input.ctqRemovedIDs.push(item.questionId);
      }
    }
    return saveJobRequest;
  }

  saveJobDetails() {
    this.notificationService.clear();
    this.saveJobDetailsButtonBlock = true;
    this.saveAsJobFlag = true;

    let saveJobRequest: SaveJobRequest = this.getSaveJobRequest();

    let valid: boolean = true;
    if ((this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'] == null || this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'].value == 0) && (saveJobRequest.input.jobTitle == null || saveJobRequest.input.jobTitle.trim() == '')) {

      this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.jobTitleLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    valid = this.checkBasicMandatory(saveJobRequest);
    //
    if (this.basicInfo.controls.noOfOpenings.value != null || this.basicInfo.controls.noOfOpenings.value != '') {
      if (this.checkNumberOfOpening() == false) {
        this.i18UtilService.get('job.warning.workflowOpeningExceededWarning', { workFlowOpening: this.workFlowOpening, workflowName: this.SelectedWorkflowName }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        this.saveJobDetailsButtonBlock = false;
        return;
      }
    }
    //

    if (!valid) {
      this.basicsectionactive = true;
      this.saveJobDetailsButtonBlock = false;
    }
    valid = this.checkOrganisationMandatory(saveJobRequest);
    if (!valid) {
      this.organisationalsectionactive = true;
      this.saveJobDetailsButtonBlock = false;
    }
    valid = this.checkSkillMandatory(saveJobRequest);
    if (!valid) {
      this.skillsectionactive = true;
      this.saveJobDetailsButtonBlock = false;
    }
    valid = this.checkDescriptionMandatory(saveJobRequest);
    if (!valid) {
      this.jobsectionactive = true;
      this.saveJobDetailsButtonBlock = false;
    }
    valid = this.checkWorkflowMandatory(saveJobRequest);
    if (!valid) {
      this.worksectionactive = true;
      this.saveJobDetailsButtonBlock = false;
    }

    valid = this.checkCTQMandatory(saveJobRequest);
    if (!valid) {
      this.ctqsectionactive = true;
      this.saveJobDetailsButtonBlock = false;
    }

    if (this.addToTemplateInfo.controls.save.value === 'TRUE') {
      if (this.addToTemplateInfo.controls.jobTemplateName.value == null || this.addToTemplateInfo.controls.jobTemplateName.value == '') {
        this.addsectionactive = true;
        this.saveJobDetailsButtonBlock = false;
      }
    }
    if (this.saveJobDetailsButtonBlock == false) {
      this.containerComponent.smoothScroll(0);
      return;
    }


    Promise.resolve(this.newJobService.checkForDuplication(0, this.selectedEmployeeCodeListValues, this.isECodeIsDropDown)).then(duplicacyResponse => {
      if (!this.isECodeIsDropDown || duplicacyResponse.messageCode.code == 'EC200') {
        if (!this.isECodeIsDropDown || duplicacyResponse.responseData.employeeCode == null) {
          this.newJobService.createJob(saveJobRequest).subscribe(response => {
            if (response != null && response.messageCode != null && response.messageCode.code != null) {
              if (response.messageCode.code == "EC200") {
                this.requisitionID = response.responseData.requisitionId;
                this.requisitionCode = response.responseData.requisitionCode;
                this.basicInfo.controls['tat'].setValue(response.responseData.tat + '');
                //default Competency start
                this.newJobService.saveRequisitionDefaultCompetency(this.requisitionID).subscribe(response => {
                });
                //End default competency
                if (response.responseData.closingDate != null) {
                  const [_closingDateDD, _closingDateMM, _closingDateYYYY] = response.responseData.closingDate.split('-');
                  this.setEndDate(new Date(parseInt(_closingDateYYYY), parseInt(_closingDateMM) - 1, parseInt(_closingDateDD)));
                }
                this.location.replaceState('/job/editjob/' + this.requisitionID);

                this.newJobService.deleteJobDraft(this.draftRequisitionID).subscribe(response => {
                  this.draftRequisitionID = null;
                });

                //to be verify in detail
                this.basicsavestatus = true;

                this.i18UtilService.get('common.label.update').subscribe((res: string) => {
                  this.continueButtonLabel = res;
                });
                this.isJobEdit = true;
                this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
              } else if (response.messageCode.code == "EC201") {
                this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
              } else {
                this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                });
              }
            }
            this.saveJobDetailsButtonBlock = false;
          });
        } else {
          this.saveJobDetailsButtonBlock = false;
          this.basicInfoContinueBlock = false;
          this.i18UtilService.get('job.warning.duplicateRequisitionWarning', { 'employeeCode': duplicacyResponse.responseData.employeeCode }).subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      } else {
        this.saveJobDetailsButtonBlock = false;
      }
    }, error => {
      this.saveJobDetailsButtonBlock = false;
      this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    });

  }
  goToDefaultAction(uiAction: String) {
    let valid = true;    
    if (this.comments == null || this.comments == '') {
        valid = false;
    }
    if (valid) {
      if (uiAction == 'WITHDRAW') {
        this.withdrawJob();
      }
      else if (uiAction == 'SUBMIT') {
        this.resubmitJob();

      }
    }
    else{
      this.i18UtilService.get('common.warning.enterComments').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }

  withdrawJob() {
    this.notificationService.clear();
    this.saveJobDetailsButtonBlock = true;

    let saveJobRequest: SaveJobRequest = this.getSaveJobRequest();

    this.newJobService.withdrawJob(saveJobRequest).subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          this.i18UtilService.get('common.success.successfulWithdrawn').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
          this.withdrawnSuccess = true;

        }
        else {
          this.i18UtilService.get('common.error.failedWithdraw').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      }
      if (this.withdrawnSuccess) {
        this.router.navigateByUrl('/job/createdjobs');
      }
      this.saveJobDetailsButtonBlock = false;
    });
  }

  resubmitJob() {
    this.notificationService.clear();
    this.saveJobDetailsButtonBlock = true;

    let saveJobRequest: SaveJobRequest = this.getSaveJobRequest();

    this.newJobService.resubmitJob(saveJobRequest).subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          this.i18UtilService.get('common.success.resubmitted').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
          this.withdrawnSuccess = true;

        }
        else {
          this.i18UtilService.get('common.error.failedWithdraw').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      }
      if (this.withdrawnSuccess) {
        this.router.navigateByUrl('/job/createdjobs');
      }
      this.saveJobDetailsButtonBlock = false;
    });
  }

  saveDraftDetails() {
    this.notificationService.clear();
    this.saveDraftDetailsButtonBlock = true;
    let saveJobRequest: SaveJobRequest = new SaveJobRequest()
    saveJobRequest.input = new CreateJobRequestDetails();
    saveJobRequest.input.draftRequisitionID = this.draftRequisitionID;
    saveJobRequest.input.requisitionID = this.requisitionID;
    saveJobRequest.input.jobTitle = this.basicInfo.controls.jobTitle.value;
    saveJobRequest.input.noOfOpenings = this.basicInfo.controls.noOfOpenings.value;
    saveJobRequest.input.bandId = this.basicInfo.controls.bandId.value;
    saveJobRequest.input.band = this.basicInfo.controls.band.value;
    saveJobRequest.input.startDate = this.basicInfo.controls.startDate.value;
    saveJobRequest.input.closingDate = this.basicInfo.controls.endDate.value;
    saveJobRequest.input.minimumExp = this.basicInfo.controls.minimumExp.value;
    saveJobRequest.input.maximumExp = this.basicInfo.controls.maximumExp.value;
    saveJobRequest.input.minSalary = this.basicInfo.controls.minSalary.value;
    saveJobRequest.input.maxSalary = this.basicInfo.controls.maxSalary.value;
    saveJobRequest.input.minBudgetSalary = this.basicInfo.controls.minBudgetSalary.value;
    saveJobRequest.input.maxBudgetSalary = this.basicInfo.controls.maxBudgetSalary.value;
    if (this.basicInfo.controls.requisitionTypeID.value == '') {
      saveJobRequest.input.requisitionTypeID = null;
    } else {
      saveJobRequest.input.requisitionTypeID = this.basicInfo.controls.requisitionTypeID.value;
    }
    saveJobRequest.input.gradeCode = this.basicInfo.controls.gradeCode.value;
    saveJobRequest.input.designation = this.basicInfo.controls.designation.value;
    saveJobRequest.input.currencyDenominationID = this.basicInfo.controls.currencyDenominationID.value;
    saveJobRequest.input.hiringManagerID = this.basicInfo.controls.hiringManagerID.value;
    saveJobRequest.input.hiringManagerName = this.hiringManagerName;
    saveJobRequest.input.interviewPanelistSelected = this.basicInfo.controls.interviewPanelistSelected.value;
    saveJobRequest.input.functionalAreaIDOrg = this.organisationalInfo.controls.functionalAreaIDOrg.value;
    saveJobRequest.input.requisitionRoleIDOrg = this.organisationalInfo.controls.requisitionRoleIDOrg.value;
    saveJobRequest.input.organizationUnitID = this.selectedOrgunitID;
    saveJobRequest.input.worksiteID = this.selectedWorksiteID;
    saveJobRequest.input.skillList = this.selectedSkillList;
    saveJobRequest.input.certificationList = this.selectedMinQualificationList;
    saveJobRequest.input.minQualificationList = this.selectedMinQualificationList;
    saveJobRequest.input.languageList = this.selectedLanguageList;
    if (this.jobDescriptionInfo.controls.jobDescription.value == null) {
      saveJobRequest.input.jobDescription = null;
    } else {
      saveJobRequest.input.jobDescription = this.jobDescriptionInfo.controls.jobDescription.value;
    }
    saveJobRequest.input.workflowID = this.workflowInfo.controls.workflowID.value;

    this.newJobService.submitDraft(saveJobRequest).subscribe(response => {
      let success: boolean = this.handleResponseData(response);
      this.saveDraftDetailsButtonBlock = false;
    }, error => {
      this.saveDraftDetailsButtonBlock = false;
      this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    });
  }

  public basicsection(sectionid) {
    if (sectionid == 'template') {
      this.templatesectionactive = !this.templatesectionactive;
    }
    if (this.basicsavestatus == true) {
      if (sectionid == 'basic') {
        this.basicsectionactive = !this.basicsectionactive;
      } else if (sectionid == 'organisational') {
        this.organisationalsectionactive = !this.organisationalsectionactive;
      } else if (sectionid == 'skill') {
        this.skillsectionactive = !this.skillsectionactive;
      } else if (sectionid == 'job') {
        this.jobsectionactive = !this.jobsectionactive;
      } else if (sectionid == 'work') {
        this.worksectionactive = !this.worksectionactive;
      } else if (sectionid == 'ctq') {
        this.ctqsectionactive = !this.ctqsectionactive;
      } else if (sectionid == 'add') {
        this.addsectionactive = !this.addsectionactive;
      } else if (sectionid == 'vol') {
        this.volsectionactive = !this.volsectionactive;
      }

    } else {
      this.i18UtilService.get('common.warning.saveSection', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
    }
  }

  saveBasicDetails() {
    this.notificationService.clear();
    this.basicInfoContinueBlock = true;
    this.basicDetailsRequestData = new BasicDetailsRequestData();
    this.basicDetailsRequestData.input = new NewJobBasicDetailsRequest();
    this.basicDetailsRequestData.input.requisitionID = this.requisitionID;
    this.basicDetailsRequestData.input.draftRequisitionID = this.draftRequisitionID;
    this.basicDetailsRequestData.input.jobTitle = this.basicInfo.controls.jobTitle.value;
    this.basicDetailsRequestData.input.bandId = this.basicInfo.controls.bandId.value;
    this.basicDetailsRequestData.input.band = this.basicInfo.controls.band.value;
    this.basicDetailsRequestData.input.tatPolicyEnabled = (this.tatPolicyEnabled && this.security.tatRequired);

    try {

      this.basicDetailsRequestData.input.orgUnitId = parseInt(this.selectedOrgunitID + '');
    } catch (e) {
    }

    if (this.basicInfo.controls.noOfOpenings.value == null || this.basicInfo.controls.noOfOpenings.value == '') {
      this.basicDetailsRequestData.input.noOfOpenings = 0;
    } else {
      if (this.checkNumberOfOpening() == false) {
        this.i18UtilService.get('job.warning.workflowOpeningExceededWarning', { workFlowOpening: this.workFlowOpening, workflowName: this.SelectedWorkflowName }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);

        });
        this.basicInfoContinueBlock = false;
        return;
      }
      this.basicDetailsRequestData.input.noOfOpenings = this.basicInfo.controls.noOfOpenings.value;
    }

    this.basicDetailsRequestData.input.startDate = this.basicInfo.controls.startDate.value;
    this.basicDetailsRequestData.input.closingDate = this.basicInfo.controls.endDate.value;

    this.basicDetailsRequestData.input.minimumExp = this.basicInfo.controls.minimumExp.value;
    this.basicDetailsRequestData.input.maximumExp = this.basicInfo.controls.maximumExp.value;
    this.basicDetailsRequestData.input.minSalary = this.basicInfo.controls.minSalary.value;
    this.basicDetailsRequestData.input.maxSalary = this.basicInfo.controls.maxSalary.value;
    this.basicDetailsRequestData.input.minBudgetSalary = this.basicInfo.controls.minBudgetSalary.value;
    this.basicDetailsRequestData.input.maxBudgetSalary = this.basicInfo.controls.maxBudgetSalary.value;
    if (this.basicInfo.controls.requisitionTypeID.value == '') {
      this.basicDetailsRequestData.input.requisitionTypeID = null;
    } else {
      this.basicDetailsRequestData.input.requisitionTypeID = this.basicInfo.controls.requisitionTypeID.value;
    }
    this.basicDetailsRequestData.input.employeeCategoryId = this.basicInfo.controls.employeeCategory.value;
    this.basicDetailsRequestData.input.jobFamilyId = this.basicInfo.controls.jobFamily.value;
    this.basicDetailsRequestData.input.positionId = this.basicInfo.controls.position.value;
    this.basicDetailsRequestData.input.campusTypeId = this.basicInfo.controls.campusType.value;
    this.basicDetailsRequestData.input.programTypeId = this.basicInfo.controls.programType.value;
    this.basicDetailsRequestData.input.roleTypeId = this.basicInfo.controls.roleType.value;
    this.basicDetailsRequestData.input.roleContributionId = this.basicInfo.controls.roleContribution.value;
    this.basicDetailsRequestData.input.employmentTenureId = this.basicInfo.controls.employmentTenure.value;
    this.basicDetailsRequestData.input.resourceTypeId = this.basicInfo.controls.resourceType.value;
    this.basicDetailsRequestData.input.tat = this.basicInfo.controls.tat.value;
    this.basicDetailsRequestData.input.gradeCode = this.basicInfo.controls.gradeCode.value;
    this.basicDetailsRequestData.input.designation = this.basicInfo.controls.designation.value;
    this.basicDetailsRequestData.input.currencyDenominationID = this.basicInfo.controls.currencyDenominationID.value;
    this.basicDetailsRequestData.input.hiringManagerID = this.basicInfo.controls.hiringManagerID.value;
    this.basicDetailsRequestData.input.hiringManagerName = this.hiringManagerName;
    this.basicDetailsRequestData.input.interviewPanelistSelected = this.basicInfo.controls.interviewPanelistSelected.value;
    this.basicDetailsRequestData.input.confidential = this.basicInfo.controls.confidential.value;
    this.basicDetailsRequestData.input.selectedEmployeeCodeListValues = this.selectedEmployeeCodeListValues;
    this.basicDetailsRequestData.input.worksiteID = this.selectedWorksiteID;
    this.basicDetailsRequestData.input.orgUnitId = this.selectedOrgunitID;
    this.basicDetailsRequestData.input.confidential = this.basicInfo.controls.confidential.value;
    this.basicDetailsRequestData.input.recruitWorkflowPolicyEnabled = this.recruitWorkflowPolicyEnabled;
    this.basicDetailsRequestData.input.recruitApprovalPolicyEnabled = this.recruitApprovalPolicyEnabled;


    let valid: boolean = true;
    if (this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'] != null && this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'].value == 0 && this.basicDetailsRequestData.input.jobTitle.trim() == '') {
      this.i18UtilService.get('common.warning.selectLabelDraft', { 'section': this.security.basicSectionLabel, 'label': this.security.jobTitleLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      this.basicInfoContinueBlock = false;
      valid = false;
      return;
    } else if (this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'] != null && this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'].value == 1 && this.basicDetailsRequestData.input.designation == '') {
      this.i18UtilService.get('common.warning.selectLabelDraft', { 'section': this.security.basicSectionLabel, 'label': this.security.designationLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      this.basicInfoContinueBlock = false;
      valid = false;
      return;
    }

    if (this.continueButtonLabel == this.langContinueButtonLabel) {
      this.saveAsJobFlag = false;
    }
    valid = this.checkBasicMandatory(this.basicDetailsRequestData);
    if (valid == false) {
      this.basicInfoContinueBlock = false;
      return;
    }

    Promise.resolve(this.newJobService.checkForDuplication(this.basicDetailsRequestData.input.requisitionID, this.selectedEmployeeCodeListValues, this.isECodeIsDropDown)).then(duplicacyResponse => {
      if (!this.isECodeIsDropDown || duplicacyResponse.messageCode.code == 'EC200') {
        if (!this.isECodeIsDropDown || duplicacyResponse.responseData.employeeCode == null) {
          if (this.basicDetailsRequestData.input.requisitionID != null) {
            this.newJobService.saveBasicDetails(this.basicDetailsRequestData, false).subscribe(response => {
              if (response != null && response.messageCode != null && response.messageCode.code != null) {
                if (response.messageCode.code == "EC200") {
                  this.draftRequisitionID = response.responseData.draftRequisitionID;
                  this.basicInfo.controls['tat'].setValue(response.responseData.tat + '');
                  if (response.responseData.workflowID) {
                    if (response.responseData.workflowID != 0) {
                      this.workflowInfo.controls.workflowID.setValue(response.responseData.workflowID);
                      if (this.checkNumberOfOpening() == false) {
                        this.notificationService.clear();
                        this.i18UtilService.get('job.warning.workflowOpeningExceededWarning', { workFlowOpening: this.workFlowOpening, workflowName: this.SelectedWorkflowName }).subscribe((res: string) => {
                          this.notificationService.addMessage(NotificationSeverity.WARNING, res);

                        });
                      }
                    }
                  }
                  this.basicsavestatus = true;
                  this.panelOpenClose('basic');
                  this.templatesectionactive = false;
                  this.getSuggestedSkills();
                  this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
                    this.notificationService.setMessage(NotificationSeverity.INFO, res);
                  });
                } else if (response.messageCode.code == "EC201") {
                  this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
                } else {
                  this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
                    this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                  });
                }
              }
              this.basicInfoContinueBlock = false;
            }, error => {
              this.basicInfoContinueBlock = false;
              this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            });
          } else {

            this.newJobService.saveBasicDetails(this.basicDetailsRequestData, true).subscribe(response => {
              if (response != null && response.messageCode != null && response.messageCode.code != null) {
                if (response.messageCode.code == "EC200") {
                  this.draftRequisitionID = response.responseData.draftRequisitionID;
                  this.basicInfo.controls['tat'].setValue(response.responseData.tat + '');
                  if (response.responseData.workflowID) {
                    if (response.responseData.workflowID != 0) {
                      this.workflowInfo.controls.workflowID.setValue(response.responseData.workflowID);
                      if (this.checkNumberOfOpening() == false) {
                        this.notificationService.clear();
                        this.i18UtilService.get('job.warning.workflowOpeningExceededWarning', { workFlowOpening: this.workFlowOpening, workflowName: this.SelectedWorkflowName }).subscribe((res: string) => {
                          this.notificationService.addMessage(NotificationSeverity.WARNING, res);

                        });
                      }
                    }
                  }
                  this.basicsavestatus = true;
                  this.panelOpenClose('basic');
                  this.templatesectionactive = false;
                  this.getSuggestedSkills();
                  this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
                    this.notificationService.setMessage(NotificationSeverity.INFO, res);
                  });
                } else if (response.messageCode.code == "EC201") {
                  this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
                } else {
                  this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
                    this.notificationService.setMessage(NotificationSeverity.WARNING, res);
                  });
                }
              }
              this.basicInfoContinueBlock = false;
            }, error => {
              this.basicInfoContinueBlock = false;
              this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            });
          }
        } else {
          this.basicInfoContinueBlock = false;
          this.i18UtilService.get('job.warning.duplicateRequisitionWarning', { 'employeeCode': duplicacyResponse.responseData.employeeCode }).subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      } else {
        this.basicInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      }
    });
  }

  saveOrganisationalDetails() {
    this.notificationService.clear();
    this.organisationalInfoContinueBlock = true;
    this.organisationalDetailsRequestData = new OrganisationalDetailsRequestData();
    this.organisationalDetailsRequestData.input = new NewJobOrganisationalDetailsRequest();
    this.organisationalDetailsRequestData.input.requisitionID = this.requisitionID;
    this.organisationalDetailsRequestData.input.draftRequisitionID = this.draftRequisitionID;
    this.organisationalDetailsRequestData.input.functionalAreaIDOrg = this.organisationalInfo.controls.functionalAreaIDOrg.value;
    this.organisationalDetailsRequestData.input.industryId = this.organisationalInfo.controls.industryId.value;
    this.organisationalDetailsRequestData.input.requisitionRoleIDOrg = this.organisationalInfo.controls.requisitionRoleIDOrg.value;
    this.organisationalDetailsRequestData.input.organizationUnitID = this.selectedOrgunitID;
    this.organisationalDetailsRequestData.input.worksiteID = this.selectedWorksiteID;

    this.organisationalDetailsRequestData.input.bandId = this.basicInfo.controls.bandId.value;
    this.organisationalDetailsRequestData.input.requisitionTypeID = this.basicInfo.controls.requisitionTypeID.value;
    this.organisationalDetailsRequestData.input.confidential = this.basicInfo.controls.confidential.value;
    this.organisationalDetailsRequestData.input.employeeCategoryId = this.basicInfo.controls.employeeCategory.value;
    this.organisationalDetailsRequestData.input.campusTypeId = this.basicInfo.controls.campusType.value;
    this.organisationalDetailsRequestData.input.programTypeId = this.basicInfo.controls.programType.value;
    this.organisationalDetailsRequestData.input.roleTypeId = this.basicInfo.controls.roleType.value;
    this.organisationalDetailsRequestData.input.roleContributionId = this.basicInfo.controls.roleContribution.value;
    this.organisationalDetailsRequestData.input.employmentTenureId = this.basicInfo.controls.employmentTenure.value;
    this.organisationalDetailsRequestData.input.resourceTypeId = this.basicInfo.controls.resourceType.value;
    this.organisationalDetailsRequestData.input.recruitWorkflowPolicyEnabled = this.recruitWorkflowPolicyEnabled;
    this.organisationalDetailsRequestData.input.recruitApprovalPolicyEnabled = this.recruitApprovalPolicyEnabled;

    if (this.continueButtonLabel == this.langContinueButtonLabel) {
      this.saveAsJobFlag = false;
    }
    let valid: boolean = true;
    valid = this.checkOrganisationMandatory(this.organisationalDetailsRequestData);
    if (valid == false) {
      this.organisationalInfoContinueBlock = false;
      return;
    }

    if (this.organisationalDetailsRequestData.input.requisitionID != null) {
      this.newJobService.saveOrganisationalDetails(this.organisationalDetailsRequestData, false).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('organisational');
          if (response.responseData.workflowID) {
            if (response.responseData.workflowID != 0) {
              this.workflowInfo.controls.workflowID.setValue(response.responseData.workflowID);
              if (this.checkNumberOfOpening() == false) {
                this.notificationService.clear();
                this.i18UtilService.get('job.warning.workflowOpeningExceededWarning', { workFlowOpening: this.workFlowOpening, workflowName: this.SelectedWorkflowName }).subscribe((res: string) => {
                  this.notificationService.addMessage(NotificationSeverity.WARNING, res);
                });
              }
            }
          }
        }
        this.organisationalInfoContinueBlock = false;
      }, error => {
        this.organisationalInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    } else {
      this.newJobService.saveOrganisationalDetails(this.organisationalDetailsRequestData, true).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('organisational');
          if (response.responseData.workflowID) {
            if (response.responseData.workflowID != 0) {
              this.workflowInfo.controls.workflowID.setValue(response.responseData.workflowID);
              if (this.checkNumberOfOpening() == false) {
                this.notificationService.clear();
                this.i18UtilService.get('job.warning.workflowOpeningExceededWarning', { workFlowOpening: this.workFlowOpening, workflowName: this.SelectedWorkflowName }).subscribe((res: string) => {
                  this.notificationService.addMessage(NotificationSeverity.WARNING, res);
                });
              }
            }
          }
        }
        this.organisationalInfoContinueBlock = false;
      }, error => {
        this.organisationalInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    }
  }

  saveSkillDetails() {
    this.notificationService.clear();
    this.skillInfoContinueBlock = true;
    this.skillDetailsRequestData = new SkillDetailsRequestData();
    this.skillDetailsRequestData.input = new NewJobSkillDetailsRequest();
    this.skillDetailsRequestData.input.requisitionID = this.requisitionID;
    this.skillDetailsRequestData.input.draftRequisitionID = this.draftRequisitionID;
    this.skillDetailsRequestData.input.skillList = this.selectedSkillList;
    this.skillDetailsRequestData.input.certificationList = this.selectedCertificationList;
    this.skillDetailsRequestData.input.minQualificationList = this.selectedMinQualificationList;
    this.skillDetailsRequestData.input.languageList = this.selectedLanguageList;
    if (this.continueButtonLabel == this.langContinueButtonLabel) {
      this.saveAsJobFlag = false;
    }
    let valid: boolean = true;
    valid = this.checkSkillMandatory(this.skillDetailsRequestData);
    if (valid == false) {
      this.skillInfoContinueBlock = false;
      return;
    }

    if (this.skillDetailsRequestData.input.requisitionID != null) {
      this.newJobService.saveSkillDetails(this.skillDetailsRequestData, false).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('skill');
        }
        this.skillInfoContinueBlock = false;
      }, error => {
        this.skillInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    } else {
      this.newJobService.saveSkillDetails(this.skillDetailsRequestData, true).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('skill');
        }
        this.skillInfoContinueBlock = false;
      }, error => {
        this.skillInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    }
  }

  saveJobDescriptionDetails() {
    this.notificationService.clear();
    this.jobDescriptionInfoContinueBlock = true;
    this.jobDescriptionDetailsRequestData = new JobDescriptionDetailsRequestData();
    this.jobDescriptionDetailsRequestData.input = new NewJobDescriptionDetailsRequest();
    this.jobDescriptionDetailsRequestData.input.requisitionID = this.requisitionID;
    this.jobDescriptionDetailsRequestData.input.draftRequisitionID = this.draftRequisitionID;
    if (this.jobDescriptionInfo.controls.jobDescription.value == null) {
      this.jobDescriptionDetailsRequestData.input.jobDescription = null;
    } else {
      this.jobDescriptionDetailsRequestData.input.jobDescription = this.jobDescriptionInfo.controls.jobDescription.value;
    }
    if (this.continueButtonLabel == this.langContinueButtonLabel) {
      this.saveAsJobFlag = false;
    }
    let valid: boolean = true;
    valid = this.checkDescriptionMandatory(this.jobDescriptionDetailsRequestData);

    if (valid == false) {
      this.jobDescriptionInfoContinueBlock = false;
      return;
    }

    if (this.jobDescriptionDetailsRequestData.input.requisitionID != null) {
      this.newJobService.saveJobDescriptionDetails(this.jobDescriptionDetailsRequestData, false).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('job');
        }
        this.jobDescriptionInfoContinueBlock = false;
      }, error => {
        this.jobDescriptionInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    } else {
      this.newJobService.saveJobDescriptionDetails(this.jobDescriptionDetailsRequestData, true).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('job');
        }
        this.jobDescriptionInfoContinueBlock = false;
      }, error => {
        this.jobDescriptionInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    }
  }

  saveWorkflowDetails() {
    this.notificationService.clear();
    if (typeof this.draftRequisitionID == 'undefined' && typeof this.requisitionID == 'undefined') {
      this.workflowInfoContinueBlock = false;
      this.containerComponent.smoothScroll(0);
      this.i18UtilService.get('common.warning.saveBasic').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    this.workflowInfoContinueBlock = true;
    this.workflowDetailsRequestData = new WorkflowDetailsRequestData();
    this.workflowDetailsRequestData.input = new NewJobWorkflowDetailsRequest();
    this.workflowDetailsRequestData.input.requisitionID = this.requisitionID;
    this.workflowDetailsRequestData.input.draftRequisitionID = this.draftRequisitionID;
    this.workflowDetailsRequestData.input.workflowID = this.workflowInfo.controls.workflowID.value;
    if (this.continueButtonLabel == this.langContinueButtonLabel) {
      this.saveAsJobFlag = false;
    }
    let valid: boolean = true;
    valid = this.checkWorkflowMandatory(this.workflowDetailsRequestData);
    if (valid == false) {
      this.workflowInfoContinueBlock = false;
      return;
    }

    if (this.workflowDetailsRequestData.input.requisitionID != null) {
      this.newJobService.saveWorkflowDetails(this.workflowDetailsRequestData, false).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('work');
        }
        this.workflowInfoContinueBlock = false;
      }, error => {
        this.workflowInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    } else {
      this.newJobService.saveWorkflowDetails(this.workflowDetailsRequestData, true).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('work');
        }
        this.workflowInfoContinueBlock = false;
      }, error => {
        this.workflowInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    }
  }

  saveCTQDetails() {
    this.notificationService.clear();
    this.ctqInfoComplete = true;
    this.ctqInfoContinueBlock = true;
    this.ctqDetailsRequestData = new CTQDetailsRequestData();
    this.ctqDetailsRequestData.input = new NewJobCtqDetailsRequest();
    if (typeof this.draftRequisitionID == 'undefined' && typeof this.requisitionID == 'undefined') {
      this.ctqInfoContinueBlock = false;
      this.containerComponent.smoothScroll(0);
      this.i18UtilService.get('common.warning.saveBasic').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    this.ctqDetailsRequestData.input.requisitionID = this.requisitionID;
    this.ctqDetailsRequestData.input.draftRequisitionID = this.draftRequisitionID;
    this.ctqDetailsRequestData.input.sourcingGuidline = this.ctqInfo.controls.ctqDescription.value;
    this.ctqDetailsRequestData.input.ctqID = this.ctqInfo.controls.ctq.value;
    this.ctqDetailsRequestData.input.ctqQuesIDs = [];
    this.ctqDetailsRequestData.input.ctqRemovedIDs = [];
    if (this.enableCTQ) {
      if (this.ctqData == null || this.ctqData == undefined || this.ctqData.length == 0) {
        this.i18UtilService.get('job.error.ctqQuestionError').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
        this.ctqInfoContinueBlock = false;
        return;
      }
      for (let item of this.ctqData) {
        this.ctqDetailsRequestData.input.ctqQuesIDs.push(item.questionId);
      }
      for (let item of this.removedQuestionList) {
        this.ctqDetailsRequestData.input.ctqRemovedIDs.push(item.questionId);
      }
    }
    let valid: boolean = true;
    valid = this.checkCTQMandatory(this.ctqDetailsRequestData);
    if (valid == false) {
      this.ctqInfoContinueBlock = false;
      return;
    }
    if (this.ctqDetailsRequestData.input.requisitionID != null) {
      this.newJobService.saveCTQDetails(this.ctqDetailsRequestData, false).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.panelOpenClose('ctq');
        }
        this.ctqInfoContinueBlock = false;
      }, error => {
        this.ctqInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    } else {
      this.newJobService.saveCTQDetails(this.ctqDetailsRequestData, true).subscribe(response => {
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        let success: boolean = this.handleResponseData(response);
        if (success) {
          this.ctqInfoComplete = true;
          this.panelOpenClose('ctq');
        }
        this.ctqInfoContinueBlock = false;
      }, error => {
        this.ctqInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    }
  }

  saveAddToTemplateDetails() {
    this.addToTemplateInfoContinueBlock = true;
    if (this.addToTemplateInfo.controls.save.value === 'TRUE') {
      this.notificationService.clear();
      if (this.addToTemplateInfo.controls.jobTemplateName.value == null || this.addToTemplateInfo.controls.jobTemplateName.value == '') {
        this.i18UtilService.get('job.warning.addtotemplateSectionLabelWarning', { 'addtotemplateSectionLabel': this.security.addtotemplateSectionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        this.addToTemplateInfoContinueBlock = false;
        return;
      }
      let saveJobRequest: SaveJobRequest = new SaveJobRequest()
      saveJobRequest.input = new CreateJobRequestDetails();
      saveJobRequest.input.draftRequisitionID = this.draftRequisitionID;
      saveJobRequest.input.requisitionID = this.requisitionID;
      saveJobRequest.input.jobTemplateName = this.addToTemplateInfo.controls.jobTemplateName.value;
      saveJobRequest.input.jobTitle = this.basicInfo.controls.jobTitle.value;
      saveJobRequest.input.noOfOpenings = this.basicInfo.controls.noOfOpenings.value;
      saveJobRequest.input.startDate = this.basicInfo.controls.startDate.value;
      saveJobRequest.input.closingDate = this.basicInfo.controls.endDate.value;
      saveJobRequest.input.minimumExp = this.basicInfo.controls.minimumExp.value;
      saveJobRequest.input.maximumExp = this.basicInfo.controls.maximumExp.value;
      saveJobRequest.input.minSalary = this.basicInfo.controls.minSalary.value;
      saveJobRequest.input.maxSalary = this.basicInfo.controls.maxSalary.value;
      saveJobRequest.input.minBudgetSalary = this.basicInfo.controls.minBudgetSalary.value;
      saveJobRequest.input.maxBudgetSalary = this.basicInfo.controls.maxBudgetSalary.value;
      saveJobRequest.input.requisitionTypeID = this.basicInfo.controls.requisitionTypeID.value;
      saveJobRequest.input.gradeCode = this.basicInfo.controls.gradeCode.value;
      saveJobRequest.input.designation = this.basicInfo.controls.designation.value;
      saveJobRequest.input.currencyDenominationID = this.basicInfo.controls.currencyDenominationID.value;
      saveJobRequest.input.hiringManagerID = this.basicInfo.controls.hiringManagerID.value;
      saveJobRequest.input.hiringManagerName = this.hiringManagerName;
      saveJobRequest.input.interviewPanelistSelected = this.basicInfo.controls.interviewPanelistSelected.value;
      saveJobRequest.input.functionalAreaIDOrg = this.organisationalInfo.controls.functionalAreaIDOrg.value;
      saveJobRequest.input.requisitionRoleIDOrg = this.organisationalInfo.controls.requisitionRoleIDOrg.value;
      saveJobRequest.input.organizationUnitID = this.selectedOrgunitID;
      saveJobRequest.input.worksiteID = this.selectedWorksiteID;
      saveJobRequest.input.skillList = this.selectedSkillList;
      saveJobRequest.input.certificationList = this.selectedCertificationList;
      saveJobRequest.input.minQualificationList = this.selectedMinQualificationList;
      saveJobRequest.input.languageList = this.selectedLanguageList;

      if (this.jobDescriptionInfo.controls.jobDescription.value == null) {
        saveJobRequest.input.jobDescription = null;
      } else {
        saveJobRequest.input.jobDescription = this.jobDescriptionInfo.controls.jobDescription.value;
      }
      saveJobRequest.input.workflowID = this.workflowInfo.controls.workflowID.value;
      saveJobRequest.input.employeeCategoryId = this.basicInfo.controls.employeeCategory.value;
      saveJobRequest.input.jobFamilyId = this.basicInfo.controls.jobFamily.value;
      saveJobRequest.input.positionId = this.basicInfo.controls.position.value;
      saveJobRequest.input.campusTypeId = this.basicInfo.controls.campusType.value;
      saveJobRequest.input.programTypeId = this.basicInfo.controls.programType.value;
      saveJobRequest.input.roleTypeId = this.basicInfo.controls.roleType.value;
      saveJobRequest.input.roleContributionId = this.basicInfo.controls.roleContribution.value;
      saveJobRequest.input.employmentTenureId = this.basicInfo.controls.employmentTenure.value;
      saveJobRequest.input.resourceTypeId = this.basicInfo.controls.resourceType.value;
      saveJobRequest.input.industryId = this.organisationalInfo.controls.industryId.value;
      saveJobRequest.input.bandId = this.basicInfo.controls.bandId.value;
      saveJobRequest.input.band = this.basicInfo.controls.band.value;
      saveJobRequest.input.selectedEmployeeCodeListValues = this.selectedEmployeeCodeListValues;
      saveJobRequest.input.confidential = this.basicInfo.controls.confidential.value;
      saveJobRequest.input.sourcingGuidline = this.ctqInfo.controls.ctqDescription.value;
      saveJobRequest.input.ctqID = this.ctqInfo.controls.ctq.value;
      saveJobRequest.input.ctqQuesIDs = [];
      saveJobRequest.input.ctqRemovedIDs = [];
      if (this.enableCTQ) {
        if (this.ctqData == null || this.ctqData == undefined || this.ctqData.length == 0) {
          this.i18UtilService.get('job.error.ctqQuestionError').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
          this.addToTemplateInfoContinueBlock = false;
          return;
        }
        for (let item of this.ctqData) {
          saveJobRequest.input.ctqQuesIDs.push(item.questionId);
        }
        for (let item of this.removedQuestionList) {
          saveJobRequest.input.ctqRemovedIDs.push(item.questionId);
        }
      }

      this.newJobService.saveJobTemplate(saveJobRequest).subscribe(response => {
        let success: boolean = this.handleResponseData(response);
        this.addToTemplateInfoComplete = true;
        this.jobTemplateList = null;
        this.draftRequisitionID = response.responseData.draftRequisitionID;
        this.newJobService.getJobTemplateList().subscribe(out => this.jobTemplateList = out.responseData);
        this.addToTemplateInfoContinueBlock = false;
        this.panelOpenClose('add');
      }, error => {
        this.addToTemplateInfoContinueBlock = false;
        this.i18UtilService.get('common.error.serverConnectionerror').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
      });
    } else {
      this.panelOpenClose('add');
      this.addToTemplateInfoContinueBlock = false;
      this.addToTemplateInfoComplete = true;
      this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
    }
  }

  templateSelectListener(jobTemplateName: String) {
    this.newJobService.getJobTemplateDetails(jobTemplateName).subscribe(result => {
      this.resetForms();
      this.resetVariousParameters();
      this.setJobDetails(result);
      this.basicInfoComplete = false;
      this.organisationalInfoComplete = false;
      this.skillInfoComplete = false;
      this.jobDescriptionInfoComplete = false;
      this.workflowInfoComplete = false;
      this.ctqInfoComplete = false;
      this.addToTemplateInfoComplete = false;
    });
  }

  clearAllForms() {
    event.stopPropagation();
    this.resetForms();
  }

  ngOnDestroy(): void {
  }

  syncBandGradeList() {
    const bandId = this.basicInfo.controls.bandId.value;
    const gradeCode = this.basicInfo.controls.gradeCode.value;
    this.gradeList = [];

    if (this.moduleConfig && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'] != null && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'].value === '1') {

      let gradeFound = false;

      if (typeof bandId == 'number') {
        if (this.gradeListMaster != null) {

          for (let grade of this.gradeListMaster) {
            if (grade.bandId) {
              if (grade.bandId == bandId) {
                if (grade.label == gradeCode) {
                  gradeFound = true;
                }
                this.gradeList.push(grade);
              }
            }
          }
        }
      }

      if (!gradeFound && this.gradeList != null && this.gradeList.length > 0) {
        this.basicInfo.controls.gradeCode.setValue('');
      }

      if (!gradeFound) {
        if (this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'] != null && this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'].value == '1') {
          this.basicInfo.controls.designation.setValue('');
        }
      }
    } else {
      this.gradeList = this.gradeListMaster;
    }

  }

  syncGradeDesignationList() {

    this.designationList = [];

    let gradeId = 0;
    let bandId = this.basicInfo.controls.bandId.value;
    let gradeCode = this.basicInfo.controls.gradeCode.value;
    let designationCode = this.basicInfo.controls.designation.value;

    if (this.gradeList) {
      for (let grade of this.gradeList) {
        if (grade) {
          if (this.moduleConfig['REQUISITION_BAND_GRADE_LINK'] != null && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'].value == '1' && grade.bandId != null) {
            if (grade.bandId == bandId && gradeCode == grade.label) {
              gradeId = grade.value;
              break;
            }
          } else if (gradeCode == grade.label) {
            gradeId = grade.value;
            break;
          }
        }
      }
    }
    this.selectedGradeID = gradeId;
    let designationFound = false;

    if (this.moduleConfig && this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'] != null && this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'].value === '1') {
      if (typeof gradeId == 'number') {
        if (this.designationListMaster) {

          for (let designation of this.designationListMaster) {
            if (designation.gradeId) {
              if (designation.gradeId == gradeId) {
                if (designation.label == designationCode) {
                  designationFound = true;
                }
                this.designationList.push(designation);
              }
            }
          }
        }
      }
      if (!designationFound && this.designationList != null && this.designationList.length > 0) {
        this.basicInfo.controls.designation.setValue('');
      }
    } else {
      this.designationList = this.designationListMaster;
    }
    this.syncDesignDone.next("done");
  }

  setBandValue(event) {
    this.basicInfo.controls.bandId.setValue(event.value);
    this.basicInfo.controls.band.setValue(event.label);
    this.syncBandGradeList();
  }

  setGradeValue(event) {
    this.basicInfo.controls.gradeCode.setValue(event.label);
    this.syncGradeDesignationList();
  }

  setDesignationValue(event) {
    this.basicInfo.controls.designation.setValue(event.label);
    if ((this.moduleConfig != undefined) && this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'] != null && this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'].value == 1) {
      this.basicInfo.controls.jobTitle.setValue(event.label);
    }
  }

  handleResponseData(response: any): boolean {
    let success: boolean = false;
    if (response.messageCode) {
      if (response.messageCode.code) {
        if (response.messageCode.code === "EC200") {
          this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
          });
          success = true;
        }
      }
    }
    if (!success) {
      this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
    return success;
  }

  deleteJobTemplate(jobTemplateName: String) {
    this.notificationService.clear();
    this.newJobService.deleteJobTemplate(jobTemplateName).subscribe(response => {
      this.i18UtilService.get('job.success.templateDeleted').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
      this.jobTemplateList = null;
      this.newJobService.getJobTemplateList().subscribe(out => this.jobTemplateList = out.responseData);
    });;
  }

  getProfileCompletionPercentage() {
    let total = 0;
    let completed = 0;

    if (true) {
      total++;
      if (this.basicInfo.controls.jobTitle.value != null && this.basicInfo.controls.jobTitle.value.trim() != '') {
        completed++
      }
    }

    if (this.security.organisationalSectionRendered && this.security.organizationUnitRendered) {
      total++;
      if (this.selectedOrgunitID != null && this.selectedOrgunitID != 0) {
        completed++
      }
    }

    if (this.security.experienceRendered) {
      total++;
      if (this.basicInfo.controls.minimumExp.value != null && this.basicInfo.controls.minimumExp.value != '') {
        if (this.basicInfo.controls.maximumExp.value != null && this.basicInfo.controls.maximumExp.value != '') {
          completed++
        }
      }
    }


    if (this.security.hiringManagerRendered) {
      total++;
      if (this.basicInfo.controls.hiringManagerID.value != null && this.basicInfo.controls.hiringManagerID.value != '' && this.basicInfo.controls.hiringManagerID.value != 0) {
        completed++
      }
    }

    if (this.security.interviewPanelRendered) {
      total++;
      if (this.basicInfo.controls.interviewPanelistSelected.value != null && this.basicInfo.controls.interviewPanelistSelected.value != '') {
        completed++
      }
    }

    if (this.security.startDateRendered) {
      total++;

      if (this.basicInfo.controls.startDate.value != null && this.basicInfo.controls.startDate.value != '') {
        completed++
      }
    }

    if (this.security.closingDateRendered) {
      total++;
      if (this.basicInfo.controls.endDate.value != null && this.basicInfo.controls.endDate.value != '') {
        completed++
      }
    }

    if (this.security.organisationalSectionRendered && this.security.functionalAreaRendered) {
      total++;
      if (this.organisationalInfo.controls.functionalAreaIDOrg.value != null && this.organisationalInfo.controls.functionalAreaIDOrg.value != '' && this.organisationalInfo.controls.functionalAreaIDOrg.value != 0) {
        completed++
      }
    }

    if (this.security.organisationalSectionRendered && this.security.roleRendered) {
      total++;
      if (this.organisationalInfo.controls.requisitionRoleIDOrg.value != null && this.organisationalInfo.controls.requisitionRoleIDOrg.value != '' && this.organisationalInfo.controls.requisitionRoleIDOrg.value != 0) {
        completed++
      }
    }

    if (this.security.designationRendered) {
      total++;
      if (this.basicInfo.controls.designation.value != null && this.basicInfo.controls.designation.value != '' && this.basicInfo.controls.designation.value != 0) {
        completed++
      }
    }

    if (this.security.skillSectionRendered && this.security.skillsRendered) {
      total++;
      if (this.selectedSkillList != null && this.selectedSkillList.length != 0) {
        completed++
      }
    } if (total != 0) {
      this.profileCompletionPercentage = ((completed / total) * 100);
    } else {
      this.profileCompletionPercentage = 0;
    }
  }

  gotoAutoMatchCandidates() {
    if (this.autoMatchCandidatesService.jobSummaryData != null && this.requisitionCode == this.autoMatchCandidatesService.jobSummaryData.jobCode) {
      this.checkAllowAutoMatch();
    } else {

      this.jobDetailService
        .getJobSummary(this.requisitionCode)
        .subscribe((p) => {


          this.autoMatchCandidatesService.jobSummaryData = p;
          this.jobDetailService.jobSummaryData = p;
          this.sessionService.jobSummaryData = this.autoMatchCandidatesService.jobSummaryData;

          this.checkAllowAutoMatch();
        });
    }
  }

  private checkAllowAutoMatch() {
    let jobClosed = false;
    let jobHold = false;
    let allowAutoMatch = true;

    if (this.autoMatchCandidatesService.jobSummaryData != null) {
      if (this.autoMatchCandidatesService.jobSummaryData.jobStatus != null
        && (this.autoMatchCandidatesService.jobSummaryData.jobStatus === 'CLOSED' || this.autoMatchCandidatesService.jobSummaryData.jobStatus === 'PCLOSE')) {
        jobClosed = true;
      }
      if (this.autoMatchCandidatesService.jobSummaryData.jobStatus != null
        && (this.autoMatchCandidatesService.jobSummaryData.jobStatus === 'ONHOLD' || this.autoMatchCandidatesService.jobSummaryData.jobStatus === 'PHOLD')) {
        jobHold = true;
      }
    } else {
      allowAutoMatch = false;
    }

    if (jobClosed) {
      this.i18UtilService.get('closedJobAutomatchWarning').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      allowAutoMatch = false;
    } else if (jobHold) {
      this.i18UtilService.get('onHoldJobAutomatchWarning').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      allowAutoMatch = false;
    } else {
      allowAutoMatch = true;
      let path = this.location.path();
    }

    if (allowAutoMatch) {
      this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatJobcodeParamPipe.transform(this.requisitionCode) + '/jobboard');
    }
  }

  getProfileCompletionRange() {
    if (this.security.hiringManagerRendered == null) {
      return;
    }
    if (!this.profileCompletionInProgress) {
      this.profileCompletionInProgress = true;
      this.reRunProfileCompletion = false;

      let index = 0;
      this.getProfileCompletionPercentage();
      let profileCompletionRange = [];
      let percentage = this.profileCompletionPercentage;
      let rangeList = [1, 2, 3, 4, 5, 6, 7];
      let multiplier = 100 / (rangeList.length);
      let first = 0;
      let second = 0;
      for (let range of rangeList) {
        first = second;
        second = range * multiplier;
        let result = "";
        if ((percentage > first) && (percentage >= second)) {
          result = "done";
        } else if ((percentage > first) && (percentage < second)) {
          result = "active";
        } else {
          result = "";
        }
        index++;
        profileCompletionRange.push(result);
        this.profileCompletionRange = profileCompletionRange;
        this.profileCompletionInProgress = false;
        if (this.reRunProfileCompletion) {
          this.reRunProfileCompletion = false;
          setTimeout(() => { this.getProfileCompletionRange(); }, 500);
        }
      }
    } else {
      this.reRunProfileCompletion = true;
    }
  }

  resetBasicSection(event: any) {
    this.disableBandGrade = false;
    event.stopPropagation();
    this.resetBasicInfoForm()
  }
  resetBasicInfoForm() {
    this.basicInfo.reset({
      jobTitle: '',
      noOfOpenings: '',
      minimumExp: '',
      maximumExp: '',
      minSalary: 0,
      maxSalary: 0,
      minBudgetSalary: 0,
      maxBudgetSalary: 0,
      requisitionTypeID: '',
      gradeCode: '',
      designation: '',
      currency: '',
      currencyDenominationID: '',
      hiringManagerID: '',
      interviewPanelistSelected: [],
      selectedEmployeeCodeList: [],
      confidential: false,
      bandId: '',
      band: ''

    });

    this.setStartDate(null);
    this.setEndDate(null);

    if (this.moduleConfig['REQUISITION_BAND_GRADE_LINK'] != null && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'].value === '1') {
      this.gradeList = [];
    }
    if (this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'] != null && this.moduleConfig['REQUISITION_GRADE_DESIGNATION_LINK'].value === '1') {
      this.designationList = [];
    }
  }

  resetOrganisationalSection(event: any) {
    event.stopPropagation();
    this.resetOrganisationalInfoForm();
  }
  resetOrganisationalInfoForm() {
    this.selectedOrgunitID = null;
    this.selectedWorksiteID = null;
    this.organisationalInfo.reset({
      functionalAreaIDOrg: '',
      requisitionRoleIDOrg: ''
    })
  }

  resetSkillSection(event: any) {
    event.stopPropagation();
    this.resetSkillInfoForm();
    this.resetCertificationInfoForm();
    this.resetMinQualificationInfoForm();
    this.resetLanguageInfoForm();
  }
  resetSkillInfoForm() {
    this.selectedSkillList = [];
    this.skillInfo.reset({
      skill: ''
    })
    if (this.basicInfo.controls.jobTitle.value == undefined || this.basicInfo.controls.jobTitle.value == null
      || this.basicInfo.controls.jobTitle.value == "") {
      this.suggestedSkillList = [];
      this.showLoader = false;
    }
    this.filteredSuggestedSkillList = this.getDuplicateStringArray(this.suggestedSkillList);
  }

  resetCertificationInfoForm() {
    this.selectedCertificationList = null;
    this.certificationInfo.reset({
      certification: ''
    })
  }


  resetMinQualificationInfoForm() {
    this.selectedMinQualificationList = null;
    this.minQualificationInfo.reset({
      minQualification: ''
    })
  }

  resetLanguageInfoForm() {
    this.selectedLanguageList = null;
    this.workingLanguageInfo.reset({
      workingLanguage: ''
    })
  }

  resetCTQInfoForm() {
    this.sourcingGuidLineLength = 0;
    this.radioButtonEnabled = false;
    this.ctqInfo.reset();
    this.ctqData = [];
    this.enableCTQ = false;
    this.removedQuestionList = [];
  }

  resetJobDescriptionSection(event: any) {

    this.jobDescriptionInfo.controls['jobDescription'].setValue('');
    this.jobDescLength = 0;
    event.stopPropagation();
  }

  resetCTQSection(event: any) {

    this.enableCTQ = false;
    this.radioButtonEnabled = false;
    this.ctqInfo.controls['ctqDescription'].setValue('');
    this.ctqInfo.controls.cqtRadioButton.reset();
    this.ctqInfo.controls['ctq'].setValue('');
    this.ctqData = [];
    this.removedQuestionList = [];
    event.stopPropagation();
  }

  resetJobDescriptionInfoForm() {
    this.jobDescriptionInfo.reset();
  }

  resetWorkflowSection(event: any) {
    event.stopPropagation();
    this.resetWorkflowInfoForm()
  }
  resetWorkflowInfoForm() {
    this.workflowInfo.reset({
      workflowID: ''
    })
  }

  resetAll(event: any) {
    event.stopPropagation();
    this.resetForms();
  }
  resetForms() {
    this.resetBasicInfoForm();
    this.resetOrganisationalInfoForm();
    this.resetSkillInfoForm();
    this.resetCertificationInfoForm();
    this.resetMinQualificationInfoForm();
    this.resetLanguageInfoForm();
    this.resetJobDescriptionInfoForm();
    this.resetWorkflowInfoForm();
    this.resetCTQInfoForm();
  }
  resetVariousParameters() {
    this.hiringManagerName = null;
    this.selectedSkillList = null;
  }

  populatePositionDefault(event: any, designation: string) {
    let positionID = typeof event == 'number' ? event : event.positionID;
    this.disableBandGrade = false;
    this.newJobService.populatePositionDefault(positionID).subscribe((response) => {
      if (response != undefined && response.response != null) {
        this.populateOrgunitTypes(response.response.orgUnitHierarchy);
        this.populateWorksiteTypes(response.response.workSiteHierarchy);
        this.basicInfo.controls['gradeCode'].setValue(response.response.grade);
        this.basicInfo.controls['band'].setValue(response.response.band);
        this.basicInfo.controls['bandId'].setValue(response.response.bandId);
        if (this.moduleConfig && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'] != null && this.moduleConfig['REQUISITION_BAND_GRADE_LINK'].value === '1') {
          this.disableBandGrade = true;
        }
      }
      this.syncGradeDesigCall.next("flagSync3");
    });
  }
  checkBasicMandatory(basicDetailsRequestData) {
    this.basicInfoComplete = true;
    let valid: boolean = true;
    if ((this.moduleConfig != undefined) && (this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'] == null || this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'].value == 0) && basicDetailsRequestData.input.jobTitle.trim() == '') {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        valid = false;
      }
    }
    if(!isNullOrUndefined(this.basicInfo.controls.jobTitle) && !this.basicInfo.controls.jobTitle.valid){
      this.i18UtilService.get('common.warning.crossScriptError', { 'section': this.security.basicSectionLabel, 'label': this.security.jobTitleLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      valid = false;
    }
    if (this.security.positionRendered
      && this.security.positionRequired
      && (this.basicInfo.controls.position.value == 0
        || this.basicInfo.controls.position.value == ''
        || this.basicInfo.controls.position.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.positionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.positionNumberRendered
      && this.security.positionNumberRequired
      && basicDetailsRequestData.input.noOfOpenings == 0) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.positionNumberLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.gradeRendered
      && this.security.gradeRequired
      && (this.basicInfo.controls.gradeCode.value == 0 || this.basicInfo.controls.gradeCode.value == undefined)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.gradeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.bandRendered
      && this.security.bandRequired
      && (this.basicInfo.controls.bandId.value == 0 || this.basicInfo.controls.bandId.value == undefined)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.bandLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.designationRendered
      && (this.security.designationRequired || (this.moduleConfig != undefined) && (this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'] != null && this.moduleConfig['REQUISITION_MANDATORY_FIELD_JOBTITLE_OR_DESIGNATION'].value == 1))
      && basicDetailsRequestData.input.designation == '') {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.designationLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.startDateRendered
      && this.security.startDateRequired
      && basicDetailsRequestData.input.startDate == null) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.startDateLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.closingDateRendered
      && this.security.closingDateRequired
      && basicDetailsRequestData.input.closingDate == null) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.closingDateLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.startDateRendered && this.security.closingDateRendered
      && basicDetailsRequestData.input.startDate != null && basicDetailsRequestData.input.closingDate != null
      && this.jobStartDate > this.jobEndDate) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.dateOrder', { 'section': this.security.basicSectionLabel, 'date1': this.security.startDateLabel, 'date2': this.security.closingDateLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.experienceRendered
      && this.security.experienceRequired
      && basicDetailsRequestData.input.maximumExp == 0) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.experienceLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }


    if (this.security.employeeCategoryRendered
      && this.security.employeeCategoryRequired
      && (this.basicInfo.controls.employeeCategory.value == 0
        || this.basicInfo.controls.employeeCategory.value == ''
        || this.basicInfo.controls.employeeCategory.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.employeeCategoryLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.jobFamilyRendered
      && this.security.jobFamilyRequired
      && (this.basicInfo.controls.jobFamily.value == 0
        || this.basicInfo.controls.jobFamily.value == ''
        || this.basicInfo.controls.jobFamily.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.jobFamilyLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.campusTypeRendered
      && this.security.campusTypeRequired
      && (this.basicInfo.controls.campusType.value == 0
        || this.basicInfo.controls.campusType.value == ''
        || this.basicInfo.controls.campusType.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.campusTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.programTypeRendered
      && this.security.programTypeRequired
      && (this.basicInfo.controls.programType.value == 0
        || this.basicInfo.controls.programType.value == ''
        || this.basicInfo.controls.programType.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.programTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.roleTypeRendered
      && this.security.roleTypeRequired
      && (this.basicInfo.controls.roleType.value == 0
        || this.basicInfo.controls.roleType.value == ''
        || this.basicInfo.controls.roleType.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.roleTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.roleContributionRendered
      && this.security.roleContributionRequired
      && (this.basicInfo.controls.roleContribution.value == 0
        || this.basicInfo.controls.roleContribution.value == ''
        || this.basicInfo.controls.roleContribution.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.roleContributionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.employmentTenureRendered
      && this.security.employmentTenureRequired
      && (this.basicInfo.controls.employmentTenure.value == 0
        || this.basicInfo.controls.employmentTenure.value == ''
        || this.basicInfo.controls.employmentTenure.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.employmentTenureLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.resourceTypeRendered
      && this.security.resourceTypeRequired
      && (this.basicInfo.controls.resourceType.value == 0
        || this.basicInfo.controls.resourceType.value == ''
        || this.basicInfo.controls.resourceType.value == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.resourceTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.tatRendered
      && this.security.tatRequired && !this.tatPolicyEnabled
      && (basicDetailsRequestData.input.tat == null || basicDetailsRequestData.input.tat == '')) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.tatLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.experienceRendered
      && basicDetailsRequestData.input.maximumExp != null && basicDetailsRequestData.input.minimumExp != null
      && (basicDetailsRequestData.input.minimumExp > basicDetailsRequestData.input.maximumExp)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('job.warning.minExpExceeded', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.hiringManagerRendered
      && this.security.hiringManagerRequired
      && basicDetailsRequestData.input.hiringManagerID == 0) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.hiringManagerLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if ((this.security.ctcRangeRendered && this.security.ctcRangeRequired)) {
      if ((basicDetailsRequestData.input.minSalary == null || basicDetailsRequestData.input.minSalary == 0) || (basicDetailsRequestData.input.maxSalary == null || basicDetailsRequestData.input.maxSalary == 0)) {
        this.basicInfoComplete = false;
        if (this.isJobEdit || this.saveAsJobFlag) {
          this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.ctcRangeLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
      }
    }
    if ((this.security.ctcBudgetRangeRendered && this.security.ctcBudgetRangeRequired)) {
      if ((basicDetailsRequestData.input.minBudgetSalary == null || basicDetailsRequestData.input.minBudgetSalary == 0) || (basicDetailsRequestData.input.maxBudgetSalary == null || basicDetailsRequestData.input.maxBudgetSalary == 0)) {
        this.basicInfoComplete = false;
        if (this.isJobEdit || this.saveAsJobFlag) {
          this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.ctcBudgetRangeLabel }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }
      }
    }

    if (this.security.ctcMaxRendered
      && this.security.ctcMinRendered
      && (basicDetailsRequestData.input.minSalary != null && basicDetailsRequestData.input.maxSalary != null && basicDetailsRequestData.input.minSalary > basicDetailsRequestData.input.maxSalary)
    ) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('job.warning.minMaxSalaryWarning', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    

    if (this.security.ctcMaxRendered
      && this.security.ctcMinRendered
      && (basicDetailsRequestData.input.minBudgetSalary != null && basicDetailsRequestData.input.maxBudgetSalary != null && basicDetailsRequestData.input.minBudgetSalary > basicDetailsRequestData.input.maxBudgetSalary)
    ) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('job.warning.minMaxBudgetSalaryWarning', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    
    if (this.security.ctcMaxRendered
      && this.security.ctcMinRendered
      && (basicDetailsRequestData.input.minBudgetSalary < 0 || basicDetailsRequestData.input.maxBudgetSalary < 0 )
    ) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('job.warning.minMaxNegativeBudgetSalaryWarning', { 'section': this.security.basicSectionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.interviewPanelRendered
      && this.security.interviewPanelRequired
      && (basicDetailsRequestData.input.interviewPanelistSelected == null || basicDetailsRequestData.input.interviewPanelistSelected.length == 0)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.interviewPanelLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.requisitionTypeRendered
      && this.security.requisitionTypeRequired
      && (basicDetailsRequestData.input.requisitionTypeID == null || basicDetailsRequestData.input.requisitionTypeID == 0)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.requisitionTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.employeeCodeRendered && this.security.employeeCodeRequired && this.selectedRequisitionType == 'Replacement'
      && (this.selectedEmployeeCodeListValues == null || this.selectedEmployeeCodeListValues.length == 0 || this.selectedEmployeeCodeListValues[0] == null)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.employeeCodeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.employeeCodeRendered && this.selectedRequisitionType == 'Replacement' && this.security.positionNumberRendered && (this.selectedEmployeeCodeListValues != null && this.basicInfo.controls.noOfOpenings.value != null && (this.selectedEmployeeCodeListValues.length != this.basicInfo.controls.noOfOpenings.value))) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('job.warning.postionCountMismatch', { 'section': this.security.basicSectionLabel, 'employeeCode': this.security.employeeCodeLabel, 'positionNumber': this.security.positionNumberLabel}).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }



    if (this.security.confidentialRendered && this.security.confidentialRequired
      && (this.basicInfo.controls.confidential.value == false || this.basicInfo.controls.confidential.value == undefined)) {
      this.basicInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.basicSectionLabel, 'label': this.security.confidentialLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    return valid;
  }
  checkOrganisationMandatory(organisationalDetailsRequestData) {
    this.organisationalInfoComplete = true;
    let valid: boolean = true;
    if (this.security.organisationalSectionRendered && this.security.organizationUnitRendered
      && this.security.organizationUnitRequired
      && (organisationalDetailsRequestData.input.organizationUnitID == null || organisationalDetailsRequestData.input.organizationUnitID == 0)
      && (organisationalDetailsRequestData.input.orgunitHierarchy == null || organisationalDetailsRequestData.input.orgunitHierarchy == '')) {
      this.organisationalInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.organisationalSectionLabel, 'label': this.security.organizationUnitLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.organisationalSectionRendered && this.security.joiningLocationRendered
      && this.security.joiningLocationRequired
      && (organisationalDetailsRequestData.input.worksiteID == null || organisationalDetailsRequestData.input.worksiteID == 0)
      && (organisationalDetailsRequestData.input.worksiteHierarchy == null || organisationalDetailsRequestData.input.worksiteHierarchy == '')) {
      this.organisationalInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.organisationalSectionLabel, 'label': this.security.joiningLocationLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.organisationalSectionRendered && this.security.functionalAreaRendered
      && this.security.functionalAreaRequired
      && organisationalDetailsRequestData.input.functionalAreaIDOrg == 0) {
      this.organisationalInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.organisationalSectionLabel, 'label': this.security.functionalAreaLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.organisationalSectionRendered && this.security.roleRendered
      && this.security.roleRequired
      && (organisationalDetailsRequestData.input.requisitionRoleIDOrg == null || organisationalDetailsRequestData.input.requisitionRoleIDOrg == 0)) {
      this.organisationalInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.organisationalSectionLabel, 'label': this.security.roleLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.organisationalSectionRendered && this.security.industryRendered
      && this.security.industryRequired
      && (organisationalDetailsRequestData.input.industryId == null || organisationalDetailsRequestData.input.industryId == 0)) {
      this.organisationalInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.organisationalSectionLabel, 'label': this.security.industryLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    return valid;
  }
  checkSkillMandatory(skillDetailsRequestData) {
    this.skillInfoComplete = true;
    let valid: boolean = true;
    if (this.security.skillSectionRendered && this.security.skillsRendered
      && this.security.skillsRequired
      && (this.selectedSkillList == null || this.selectedSkillList.length == 0)) {
      this.skillInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.skillSectionLabel, 'label': this.security.skillsLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.skillSectionRendered && this.security.certificationRendered
      && this.security.certificationRequired
      && (this.selectedCertificationList == null || this.selectedCertificationList.length == 0)) {
      this.skillInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.skillSectionLabel, 'label': this.security.certificationLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.skillSectionRendered && this.security.minQualificationRendered
      && this.security.minQualificationRequired
      && (this.selectedMinQualificationList == null || this.selectedMinQualificationList.length == 0)) {
      this.skillInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.skillSectionLabel, 'label': this.security.minQualificationLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }

    if (this.security.skillSectionRendered && this.security.workingLanguageRendered
      && this.security.workingLanguageRequired
      && (this.selectedLanguageList == null || this.selectedLanguageList.length == 0)) {
      this.skillInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.skillSectionLabel, 'label': this.security.workingLanguageLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    return valid;
  }

  checkCTQMandatory(CTQDetailsRequestData) {
    this.ctqInfoComplete = true;
    let valid: boolean = true;
    if (this.enableCTQ && this.security.ctqRendered && this.security.ctqRequired && (this.ctqInfo.controls.ctq.value == 0 || this.ctqInfo.controls.ctq.value == undefined)) {
      this.ctqInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.ctqSectionLabel, 'label': this.security.sourcingGuidelineLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    if (this.security.ctqSectionRendered && (this.sourcingGuidLineLength > 1000)) {
      this.ctqInfoComplete = false;
      this.i18UtilService.get('job.warning.charLimitWarning', { 'label': this.security.sourcingGuidelineLabel }).subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
      this.notificationService.addMessage(NotificationSeverity.WARNING, this.security.sourcingGuidelineLabel + " : Maximum Limit 1000 Characters");
      valid = false;

    }
    if (this.security.ctqSectionRendered && this.security.sourcingGuidelineRendered && this.security.sourcingGuidelineRequired
      && (CTQDetailsRequestData.input.sourcingGuidline == null || CTQDetailsRequestData.input.sourcingGuidline == '')) {
      this.ctqInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', {'section': this.security.ctqSectionLabel, 'label': this.security.sourcingGuidelineLabel  }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    return valid;
  }

  checkDescriptionMandatory(jobDescriptionDetailsRequestData) {
    this.jobDescriptionInfoComplete = true;
    let valid: boolean = true;
    if (this.security.jobdescriptionSectionRendered && this.security.jobDescriptionRendered && this.security.jobDescriptionRequired
      && (jobDescriptionDetailsRequestData.input.jobDescription == null || jobDescriptionDetailsRequestData.input.jobDescription.trim() == '')) {
      this.jobDescriptionInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.enterLabel', { 'section': this.security.jobdescriptionSectionLabel, 'label': this.security.jobDescriptionLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }

    }
    else
      if (this.security.jobdescriptionSectionRendered && this.security.jobDescriptionRendered
        && this.security.jobDescriptionRequired
        && (this.jobDescLength < this.jobDescriptionMinLength)) {
        this.jobDescriptionInfoComplete = false;
        if (this.isJobEdit || this.saveAsJobFlag) {
          this.i18UtilService.get('job.warning.jDLengthWarning', { 'jDLabel': this.security.jobdescriptionSectionLabel, 'jDMinLength': this.jobDescriptionMinLength }).subscribe((res: string) => {
            this.notificationService.addMessage(NotificationSeverity.WARNING, res);
          });
          valid = false;
        }

      }

    return valid;
  }

  checkWorkflowMandatory(workflowDetailsRequestData) {
    this.workflowInfoComplete = true;
    let valid: boolean = true;
    if (workflowDetailsRequestData.input.workflowID == 0 || workflowDetailsRequestData.input.workflowID == null) {
      this.workflowInfoComplete = false;
      if (this.isJobEdit || this.saveAsJobFlag) {
        this.i18UtilService.get('common.warning.selectLabel', { 'section': this.security.workflowSectionLabel, 'label': this.security.workflowTypeLabel }).subscribe((res: string) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, res);
        });
        valid = false;
      }
    }
    return valid;
  }
  isRequiredAndNull(control: FormControl, isRequired: boolean) {
    if (isRequired) {
      if (control.value == null || control.value == '' || (typeof control.value == 'string' && control.value.trim() == '')) {
        return true;
      }
    }
    return false;
  }

  checkNumberOfOpening() {
    let userInput = this.basicInfo.controls.noOfOpenings.value;

    if (this.workflowList != null) {
      for (let workflow of this.workflowList) {
        if (workflow.value == this.workflowInfo.controls.workflowID.value) {
          this.workFlowOpening = workflow.numberOfOpening;
          this.SelectedWorkflowName = workflow.label;
          return userInput <= workflow.numberOfOpening;
        }
      }
    }
    return true;
  }

  fetchMinMaxSalary() {
    this.notificationService.clear();
    this.newJobService.fetchMinMaxSalary(this.getSaveJobRequest()).subscribe((response) => {
      if (response !== undefined && response.result !== undefined) {
        if (response.result.currency === 'INR') {
          this.basicInfo.controls['currency'].setValue(1);
        }
        this.basicInfo.controls['minSalary'].setValue(response.result.range_minimum);
        this.basicInfo.controls['maxSalary'].setValue(response.result.range_maximum);
      } else {
        this.i18UtilService.get('job.error.failedtoFetchCTCRange').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
        this.basicInfo.controls['minSalary'].setValue(0);
        this.basicInfo.controls['maxSalary'].setValue(0);
      }
    })
  }

  datesStorage: any[] = [];

  isSyncGradeDesigCall(): Observable<String> {
    return this.syncGradeDesigCall.asObservable();
  }

  isSyncDesignDone(): Observable<String> {
    return this.syncDesignDone.asObservable();
  }

  setStartDate(startDate: Date) {
    if (startDate != null) {
      this.basicInfo.controls['startDate'].setValue(this.datePipe.transform(startDate, "dd-MM-yyyy"));
    } else {
      this.basicInfo.controls['startDate'].reset();
    }
    this.jobStartDate = startDate;
  }

  setEndDate(endDate: Date) {
    if (endDate != null) {
      this.basicInfo.controls['endDate'].setValue(this.datePipe.transform(endDate, "dd-MM-yyyy"));
    } else {
      this.basicInfo.controls['endDate'].reset();
    }
    this.jobEndDate = endDate;
  }

  fetchCTQQuestions(event) {
    this.ctqInfo.controls.ctq.setValue(event.ctqID);
    this.newJobService.getCTQMaster(event.ctqID).subscribe(out => {
      this.ctqData = out.responseData;
      this.removedQuestionList = [];
      if (this.ctqData != undefined && this.ctqData != null) {
        for (let ctq of this.ctqData) {
          if (ctq.questionType == 'SELECTONEMENU') {
            ctq.selectItemOptions = this.getOptions(ctq.options)
          }
        }
      }
    });
  }

  removedQuestionFromCTQ(ctq: CtqMaster) {
    let index = this.ctqData.indexOf(ctq);
    if (index >= 0) {
      this.ctqData.splice(index, 1);
    }
    ctq.id = ctq.questionId;
    ctq.name = ctq.question;
    this.removedQuestionList.push(ctq);
  }

  addQuestionToList(event) {
    let index = this.removedQuestionList.indexOf(event);
    if (index >= 0) {
      this.removedQuestionList.splice(index, 1);
    }
    this.ctqData.push(event);
  }
  initCTQForm(event: any, flag: boolean) {
    this.enableCTQ = flag;
    this.radioButtonEnabled = true;
  }
  getOptions(options) {
    let selectItemOptions = [];
    for (let option of options) {
      selectItemOptions.push(new SelectItem(option, option));
    }
   
    return selectItemOptions;
  }
  getTranslatedText(text: string): string{
    let result='';
    this.i18UtilService.get('common.label.'+text).subscribe((res: string) => {
      result = res;
    });
    return result;
  }
}
export class BasicDetailsRequestData {
  input: NewJobBasicDetailsRequest;
}

export class OrganisationalDetailsRequestData {
  input: NewJobOrganisationalDetailsRequest;
}

export class SkillDetailsRequestData {
  input: NewJobSkillDetailsRequest;
}

export class JobDescriptionDetailsRequestData {
  input: NewJobDescriptionDetailsRequest;
}

export class WorkflowDetailsRequestData {
  input: NewJobWorkflowDetailsRequest;
}

export class CTQDetailsRequestData {
  input: NewJobCtqDetailsRequest;
}

export class AddToTemplateDetailsRequestData {
  input: NewJobAddToTemplateDetailsRequest;
}

export class SaveJobRequest {
  input: CreateJobRequestDetails;
}

export class GetJobTemplateDetailsRequest {
  input: GetJobTemplateDetailsRequestInput;
}

export class GetJobTemplateDetailsRequestInput {
  jobTemplateName: String;
}

export class CreateJobRequestDetails {
  draftRequisitionID: Number;
  requisitionID: Number;
  jobTemplateName: String;
  noOfOpenings: Number;
  jobTitle: String;
  startDate: String;
  closingDate: String;
  minimumExp: Number;
  maximumExp: Number;
  minSalary: Number;
  maxSalary: Number;
  minBudgetSalary: Number;
  maxBudgetSalary: Number;
  requisitionTypeID: Number;
  gradeCode: String;
  gradeID: Number;
  designation: String;
  currencyDenominationID: Number;
  hiringManagerName: String;
  hiringManagerID: Number;
  interviewPanelistSelected: Array<Number>;
  functionalAreaIDOrg: Number;
  requisitionRoleIDOrg: Number;
  organizationUnitID: Number;
  worksiteID: Number;
  skillList: any[];
  languageList: String[];
  certificationList: String[];
  minQualificationList: String[];
  jobDescription: String;
  workflowID: Number;
  createdBy: Number;
  modifiedBy: Number;
  industryId: number;
  employeeCategoryId: number;
  jobFamilyId: number;
  positionId: number;
  campusTypeId: number;
  programTypeId: number;
  roleTypeId: number;
  roleContributionId: number;
  employmentTenureId: number;
  resourceTypeId: number;
  tat: Number;
  confidential: boolean;
  bandId: number;
  band: string;
  selectedEmployeeCodeListValues: Array<String>;
  tatPolicyEnabled: boolean;
  orgUnitId: Number;
  recruitWorkflowPolicyEnabled: boolean;
  recruitApprovalPolicyEnabled: boolean;
  closingDateAutopopulateEnabled: boolean;
  sourcingGuidline: String;
  ctqID: Number;
  ctqQuesIDs: Array<Number>;
  ctqRemovedIDs: Array<Number>;
  isOrganizationUnitRendered: boolean;
  isJoiningLocationRendered: boolean;
  stageStatusID; Number;
  comments: String;
}
export class Item {
  label: string;
  value: any;

  constructor(label: string, value: any) {
    this.label = label;
    this.value = value;
  }
}

export class RequisitionRoleLevelConfigResponseTO {
  roleLevelConfigID: Number;
  roleID: Number;
  orgUnitLevel: Number;
  worksiteLevel: Number;
  active: Boolean;
  organizationID: Number;
  tenantID: Number;
  orgUnitID: Number;
  orgUnitHierarchy: String;
  workSiteID: Number;
  workSiteHierarchy: String;
}