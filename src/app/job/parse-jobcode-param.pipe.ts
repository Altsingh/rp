import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'parseJobcodeParam'
})
export class ParseJobcodeParamPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) { return value; }
    value = value.replace(/\_/g, '/');
    value = value.replace(/\@/g, '&');
    return value;
  }

}
