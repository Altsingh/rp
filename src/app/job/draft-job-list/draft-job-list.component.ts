import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DraftJobListService } from './draft-job-list.service';
import { Observable } from 'rxjs/Observable';
import { MessageCode } from '../../common/message-code';
import { JobListResponse } from '../../common/job-list-response';
import { CommonService } from '../../common/common.service';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { DraftJobPagedService }  from './draft-job-paged.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PageState } from '../../shared/paginator/paginator.component';
import { JobFilterTO } from '../job-list/job-filter-to';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-draft-job-list',
  templateUrl: './draft-job-list.component.html',
  providers: [DraftJobListService, DraftJobPagedService]
})

export class DraftJobListComponent implements OnInit, OnDestroy {
  
  pageState: PageState = new PageState();
  isToggled: boolean;
  filtterToggle: boolean;
  totalRowsSubscription:Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription:Subscription;
  queryFormGroup: FormGroup;
  queryTimeout:any;
  filterTO: JobFilterTO;

  loaderIndicator: string;
  isClassVisible: boolean = false;
  filterDropDown: boolean = true;
  filterDropDown2: boolean = true;
  draftjobListData: JobListResponse[];
  DeleteJobDraft: responseData;
  messageCode: MessageCode;
  subscription: any;
  errorMessage: any;
  jobReqIDlist: Array<number>;
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";
  selected: any[] = [];
  selectAll: boolean;
  displayClass: String = "displayNone";
  currentorder: string;
  sortByApplied: boolean;
  
  constructor(private draftJobListService: DraftJobListService,
    private notificationService: NotificationService,
    private commonService: CommonService,
    private draftJobPagedService:DraftJobPagedService,
    private formBuilder: FormBuilder,
    private i18UtilService: I18UtilService) {
      this.filterTO = new JobFilterTO();

    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });

    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {

        let query = this.queryFormGroup.controls.query.value;

        if (query.length >= 3) {

          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          this.draftJobPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.draftJobPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
      }, 200);

    });
  }
    private toggleFilter2() {
    this.filterDropDown2 = !this.filterDropDown2;
    if (!this.filterDropDown2) {
      this.filterDropDown = true;
    }
  }

  ngOnInit() {
    this.pageState.pageSize = 15;
    this.selected = [];
    this.loaderIndicator = "loader";
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.loadData();
    this.loadTotalRows();
    this.commonService.showRHS();
  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.draftJobPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.draftJobPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.draftjobListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;

    this.loadData();
    

  }

  areAllSelected(){
    if(this.draftjobListData != null && this.draftjobListData.length > 0){
      for(let x=0;x<this.draftjobListData.length;x++){
        if(typeof this.draftjobListData[x].selected == 'undefined' || (typeof this.draftjobListData[x].selected == 'boolean' && !this.draftjobListData[x].selected)){
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else{
      this.selectAll = false;
    }
  }

  deleteDraft(ReqID, index) {
    this.draftJobListService.deleteDraft(ReqID).subscribe(res => {
      this.draftJobPagedService.removeCachedItem(this.filterTO.pageNumber, this.filterTO.pageSize, index);
      this.draftjobListData.splice(index, 1);
      this.pageState.totalRows--;
      this.i18UtilService.get('job.success.draftDeleted').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
      this.loadData();
    });
  }
  deleteAll() {
    if (this.selected.length == 0) {
      this.i18UtilService.get('job.warning.selectJob').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    let reqDraftID: any[] = [];
    for (let i of this.selected) {
      reqDraftID.push(i.draftRequisitionID)
    };
    this.jobReqIDlist = reqDraftID;
    this.draftJobListService.deleteDraftBulk(this.jobReqIDlist).subscribe(res => {
      this.draftJobPagedService.clearCache();
      this.ngOnInit();
      this.i18UtilService.get('job.success.draftDeleted').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
    });
    this.selected = [];
  }
  public sort(sortParam: string, order: string) {
    this.currentorder = sortParam + order;
    this.sortByApplied = true;
    this.filterTO.sortBy = sortParam;
    this.filterTO.sortOrder = order;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.draftJobPagedService.clearCache();
    this.loadTotalRows();
    this.loadData();
  }

  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();
  }

  allSelectEvent() {
      let index = 0;
      for (let data of this.draftjobListData) {
        data.selected = this.selectAll;
        index++;
        if (this.selectAll && !this.selected.includes(data)) {
          this.selected.push(data);
        } else if (!this.selectAll) {
          let i = this.selected.indexOf(data);
          this.selected.splice(i, 1);
        }
      }
  }

  ngOnDestroy(): void {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }

  }

  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }
  filtterToggleClick() {
    this.filtterToggle = !this.filtterToggle;
  }
  filtterToggleClosed() {
    this.filtterToggle = false;
  }
  
}

interface messageCode {
  code: string;
  message: string;
  description: string;
}
interface responseData {
  draftRequisitionID: number;
  requestCode: string;
  message: string;
  description: string;
}
