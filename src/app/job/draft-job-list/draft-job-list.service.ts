import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { SessionService } from '../../session.service';
import { JobFilterTO } from '../job-list/job-filter-to';

@Injectable()
export class DraftJobListService {

  constructor(private http: Http, private sessionService: SessionService) { }
  
  getJobList(jobFilterTO:JobFilterTO) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/draftjoblist/', JSON.stringify(jobFilterTO), options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getJobListTotalRows(jobFilterTO:JobFilterTO) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/draftjoblist/totalRows/', JSON.stringify(jobFilterTO), options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  //Delete Job Draft by Requested ID.
  deleteDraft(JobDraftID) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/deleteJobDraft/' + JobDraftID + "/")
  }
  
  deleteDraftBulk(reqDraftIdList:  Array<number>) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/deleteMultipleJobDraft/', JSON.stringify(reqDraftIdList), options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
  getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }
}
