import { Component, OnInit, HostListener } from '@angular/core';
import { SessionService } from 'app/session.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PostToJobBoardService } from 'app/job/job-boards/post-to-job-board.service';
import { MatDialogRef } from '@angular/material';
import { PostToIimjobsModel } from './post-to-iimjobs-model';
import { NotificationSeverity, NotificationService } from '../../../../common/notification-bar/notification.service';
import { SelectItem } from '../../../../shared/select-item';
import { MessageCode } from '../../../../common/message-code';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-post-to-iimjobs',
  templateUrl: './post-to-iimjobs.component.html',
  styleUrls: ['./post-to-iimjobs.component.css'],
  providers: [PostToJobBoardService]
})
export class PostToIimjobsComponent implements OnInit {

  requisitionID: number;
  jobBoardID: number;
  iimJobsForm: FormGroup;
  data: any; service
  iimJobs: boolean = false;
  jobActionList: SelectItem[];
  salaryCurrencyList: SelectItem[];

  message: string;
  severity: String;
  timer: any;
  severityClass: String;
  jobPostingCode: any;
  isRequiredText: string;


  constructor(
    private formBuilder: FormBuilder,
    private sessionService: SessionService,
    private postToJobBoardService: PostToJobBoardService,
    public dialogRef: MatDialogRef<PostToIimjobsComponent>,
    public notificationService: NotificationService,
    private i18UtilService: I18UtilService
  ) {
    this.iimJobsForm = this.formBuilder.group({

      jobAction: 'ADD',
      salaryCurrency: 'INR',
      jobRefID: '',
      jobTitle: '',
      jobIndustry: '',
      minimumSalary: '',
      maximumSalary: '',
      jobSkills: '',
      jobDescription: '',
      name: '',
      daysToAdvertise: '',
      jobType: '',
      jobFunctionalArea: '',
      jobCategory: '',
      minExperience: '',
      maxExperience: '',
      jobLocation: ''

    })
  }

  ngOnInit() {
    this.requisitionID = this.sessionService.object.requisitionId;
    this.jobBoardID = this.sessionService.object.jobBoardID;
    this.salaryCurrencyList = [new SelectItem('INR', 'INR'), new SelectItem('GBP', 'GBP'), new SelectItem('USD', 'USD'), new SelectItem('EUR', 'EUR')];
    this.jobActionList = [new SelectItem('ADD', 'ADD'), new SelectItem('DELETE', 'DELETE')];
    this.initializeData();
    this.i18UtilService.get('common.label.isRequired').subscribe((res: string) => {
      this.isRequiredText = res;
    }); 
  }

  // Parent scroll stop on the child Mouse hover
  @HostListener('window:mousewheel', ['$event']) 
    onMousewheel(event:any){
        event.stopPropagation();
    }

    @HostListener('window:mousewheel', ['$event']) 
    onDOMMouseScroll(event:any){
        event.stopPropagation();
    }

  initializeData() {
    this.postToJobBoardService.getJobBoardDataForNaukri(this.requisitionID).subscribe(out => {
      this.data = out.responseData;
      
      let jobDescription = this.data.jobDescription;
      if( jobDescription != null ){
       let htmlRegex = /(&nbsp;|<([^>]+)>)/ig;
       let cssRegex = /((<style>)|(<style type=.+))((\s+)|(\S+)|(\r+)|(\n+))(.+)((\s+)|(\S+)|(\r+)|(\n+))(<\/style>)/g;
       let commentsRegex = /\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm;
       let spacesRegex = /\s\s+/g;
       let newLineRegex = /\n\s*\n/g;       
       let keyboardAscii = /[^\x00-\x7F]/g;
        jobDescription = jobDescription.replace(/<br\s*\/?>/mg, "\n").replace(commentsRegex, '').replace(cssRegex, '').replace(htmlRegex, "").replace(keyboardAscii, "").replace(newLineRegex, "\n").trim();
      }

      this.iimJobsForm.controls['jobRefID'].setValue(this.requisitionID);
      this.iimJobsForm.controls['jobTitle'].setValue(this.data.jobTitle);
      this.iimJobsForm.controls['jobDescription'].setValue(jobDescription);
      this.iimJobsForm.controls['minimumSalary'].setValue(this.data.minSalary);
      this.iimJobsForm.controls['jobIndustry'].setValue(this.data.industryName);
      this.iimJobsForm.controls['jobSkills'].setValue(this.data.skillList);
      this.iimJobsForm.controls['maximumSalary'].setValue(this.data.maxSalary);
      this.iimJobsForm.controls['jobLocation'].setValue(this.data.joiningLocation);
      this.iimJobsForm.controls['minExperience'].setValue(this.data.minimumExp);
      this.iimJobsForm.controls['maxExperience'].setValue(this.data.maximumExp);
      this.iimJobsForm.controls['jobFunctionalArea'].setValue(this.data.functionalArea);
    });

  }

  postToIIMJobs() {
    let postToIIMJobsTO: PostToIimjobsModel = new PostToIimjobsModel();
    postToIIMJobsTO.jobBoardID = this.jobBoardID;
    postToIIMJobsTO.requisitionID = this.requisitionID;
    postToIIMJobsTO.jobRefID = this.iimJobsForm.controls.jobRefID.value;
    postToIIMJobsTO.jobAction = this.iimJobsForm.controls.jobAction.value;
    postToIIMJobsTO.jobIndustry = this.iimJobsForm.controls.jobIndustry.value;
    postToIIMJobsTO.jobDescription = this.iimJobsForm.controls.jobDescription.value;
    postToIIMJobsTO.jobTitle = this.iimJobsForm.controls.jobTitle.value;
    postToIIMJobsTO.jobType = this.iimJobsForm.controls.jobType.value;
    postToIIMJobsTO.jobSkills = this.iimJobsForm.controls.jobSkills.value.join(",");
    postToIIMJobsTO.maximumSalary = this.iimJobsForm.controls.maximumSalary.value;
    postToIIMJobsTO.minimumSalary = this.iimJobsForm.controls.minimumSalary.value;
    postToIIMJobsTO.minExperience = this.iimJobsForm.controls.minExperience.value;
    postToIIMJobsTO.maxExperience = this.iimJobsForm.controls.maxExperience.value;
    postToIIMJobsTO.jobCategory = this.iimJobsForm.controls.jobCategory.value;
    postToIIMJobsTO.jobLocation = this.iimJobsForm.controls.jobLocation.value;
    postToIIMJobsTO.jobFunctionalArea = this.iimJobsForm.controls.jobFunctionalArea.value;
    postToIIMJobsTO.name = this.iimJobsForm.controls.name.value;
    postToIIMJobsTO.daysToAdvertise = this.iimJobsForm.controls.daysToAdvertise.value;
    postToIIMJobsTO.salaryCurrency = this.iimJobsForm.controls.salaryCurrency.value;
    postToIIMJobsTO.jobCode = this.data.requisitionCode;

    let valid = true;

    if (postToIIMJobsTO.jobAction == null || postToIIMJobsTO.jobAction == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobAction', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.jobTitle == null || postToIIMJobsTO.jobTitle == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobTitle', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.jobDescription == null || postToIIMJobsTO.jobDescription == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jD', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.jobRefID == null || postToIIMJobsTO.jobRefID == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobRefID', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.jobIndustry == null || postToIIMJobsTO.jobIndustry == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobIndustry', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.minExperience == null || postToIIMJobsTO.minExperience == '') {
      this.setMessage("WARNING", 'postJobBoard.label.minExp', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.maxExperience == null || postToIIMJobsTO.maxExperience == '') {
      this.setMessage("WARNING", 'postJobBoard.label.maxExp', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.jobFunctionalArea == null || postToIIMJobsTO.jobFunctionalArea == '') {
      this.setMessage("WARNING", 'postJobBoard.label.functionalArea', true);
      valid = false;
      return;
    }

    if (postToIIMJobsTO.minExperience > postToIIMJobsTO.maxExperience) {

      this.setMessage("WARNING", 'postJobBoard.warning.minExpExceeded', false)
      valid = false;
      return;
    }

    if (postToIIMJobsTO.minimumSalary != null && postToIIMJobsTO.maximumSalary != null) {
      if (postToIIMJobsTO.minimumSalary > postToIIMJobsTO.maximumSalary) {

        this.setMessage("WARNING", 'job.warning.minMaxSalaryWarning', false)
        valid = false;
        return;
      }
    }


    if (valid = true) {
      this.iimJobs = true;
      this.postToJobBoardService.postToIIMJobs(postToIIMJobsTO).subscribe(response => {
        let messageTO: MessageCode = response.messageCode;
        let message: string = '';
        if (messageTO != null) {
          if (messageTO.code != null && messageTO.code === 'EC200') {
            this.jobPostingCode = response.response;
            message = messageTO.message.toString();
            this.dialogRef.close(this.jobPostingCode);

          } else if (messageTO.message != null) {
            message = messageTO.message.toString();
          } else {
            message = "Request failed to execute."
          }
          if (messageTO.code === 'EC200') {
            this.iimJobs = false;
            this.notificationService.setMessage(NotificationSeverity.INFO, message);
          } else {
            this.iimJobs = false;
            this.setMessage("WARNING", message, false);
          }
        }

      }, (onError) => {
        this.setMessage("WARNING", 'common.error.somthingWrong', false);
      }, () => {
      });


    }
  }

  public setMessage(severity: string, message: string, isRequiredValidation: boolean) {

    this.i18UtilService.get(message).subscribe((res: string) => {
      if (isRequiredValidation) {
        this.message = res + this.isRequiredText;
      }
      else {
        this.message = res;
      }
      this.severity = severity;

      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);

    });


  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }
}
