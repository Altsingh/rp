export class PostToIimjobsModel{
    
    jobRefID : String = '';

    jobAction :  String = '';
     
    jobTitle : String = '';

    jobIndustry : String ='';
     
    salaryCurrency : String = '';

    minimumSalary : String = '';

    maximumSalary : String = '';

    jobSkills : String = '';

    jobType : String = '' ;

    jobDescription : String = '';

    name : String = '';

    jobBoardID: number;

    requisitionID: number;

    jobCode : String='';

    daysToAdvertise : String='';

    jobCategory : String='';

    jobFunctionalArea : String='';

    minExperience : String='';

    maxExperience : String='';

    jobLocation : String='';

}