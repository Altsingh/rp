export class JobBoardModel {
    sysJobBoardName :   String;
    jobBoardName    :   String;
    jobPostingCode  :   String;
    jobBoardID      :   Number;
    status          :   boolean;
}