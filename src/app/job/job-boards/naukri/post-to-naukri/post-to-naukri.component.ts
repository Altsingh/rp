import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MatNativeDateModule } from '@angular/material';
import { JobActionService } from '../../../job-action/job-action.service';
import { SessionService } from '../../../../session.service';
import { SelectItem } from '../../../../shared/select-item';
import { PostToNaukriModel } from "app/job/job-boards/naukri/post-to-naukri/post-to-naukri-model";
import { PostToJobBoardService } from "app/job/job-boards/post-to-job-board.service";
import { MessageCode } from '../../../../common/message-code';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-post-to-naukri',
  templateUrl: './post-to-naukri.component.html',
  providers: [PostToJobBoardService]

})
export class PostToNaukriComponent implements OnInit {
  jobActionList: SelectItem[];
  jobTypeList: SelectItem[];
  jobFunctionCodeList: SelectItem[];
  jobIndustryCodeList: SelectItem[];
  jobRoleCodeList: SelectItem[];
  interNationalLocList: SelectItem[];
  indianLocList: SelectItem[];
  salaryCurrencyList: SelectItem[];
  minimumExperienceList: SelectItem[];
  maximumExperienceList: SelectItem[];
  minimumSalaryList: SelectItem[];
  maximumSalaryList: SelectItem[];
  ugQualificationsList: SelectItem[];
  ugSpecializationsList: SelectItem[];
  pgQualificationsList: SelectItem[];
  pgSpecializationsList: SelectItem[];
  doctorateQualificationsList: SelectItem[];
  doctorateSpecializationsList: SelectItem[];
  mastersList: SelectItem[];
  optionsList: SelectItem[];
  selectedIndianLocation: SelectItem[] = [];
  jobPostingCode: String;
  message: String;
  severity: String;
  timer: any;
  severityClass: String;

  naukriForm: FormGroup;
  requisitionID: number;
  jobBoardID: number;
  data: any; service
  functionalAreaID: number;
  selectedLocationlenghth: number = 0;

  naukriJobFunctionCode: any;
  naukriJobRoleCode: any;
  naukriJobIndustryCode: any;
  naukriJobCityCode: any =[];
  naukriJobIndustry: any;
  naukriSalaryCurrenyCode: any;
  naukriJobCountryCode: any;
  naukriMinExperienceCode: any;
  naukriMaxExperienceCode: any;
  naukriMinSalaryCode: any;
  naukriMaxSalaryCode: any;
  naukriUgQualificationCode: any;
  naukriUgSpecializationCode: any;
  naukriPgQualificationCode: any;
  naukriPgSpecializationCode: any;
  naukriDoctorateQualificationCode: any;
  naukriDoctorateSpecializationCode: any;
  isRequiredText: string;


  constructor(
    public dialogRef: MatDialogRef<PostToNaukriComponent>,
    private sessionService: SessionService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private postToJobBoardService: PostToJobBoardService,
    private i18UtilService: I18UtilService
  ) {

    this.optionsList = [];
    this.optionsList.push(new SelectItem('Yes', 'Y'));
    this.optionsList.push(new SelectItem('No', 'N'));

    this.naukriForm = this.formBuilder.group({

      jobAction: 'ADD',
      jobType: 'p',
      hiringOrgName: '',
      summaryText: '',
      jobPositionTitle: '',
      jobFunctionCode: '',
      jobRoleCode: '',
      jobPositionPostingID: '',
      salaryCurrency: 'Rupees',
      minimumSalary: '',
      maximumSalary: '',
      jobSummaryText: '',
      minimumExperience: '',
      maximumExperience: '',
      jobKeywords: '',
      jobIndustryCode: '',
      interNationalLoc: 'India',
      indianLoc: '',
      ugQualifications: '',
      ugSpecializations: '',
      pgQualifications: '',
      pgSpecializations: '',
      email: '',
      url: '',
      doctorateQualifications: '',
      doctorateSpecializations: '',
      displaySalary: ''
    })
  }

  ngOnInit() {


    this.requisitionID = this.sessionService.object.requisitionId;
    this.jobBoardID = this.sessionService.object.jobBoardID;
    this.naukriForm.controls['jobPositionPostingID'].setValue(this.requisitionID);
    this.initializeData();
    this.getMasters();
    this.i18UtilService.get('common.label.isRequired').subscribe((res: string) => {
      this.isRequiredText = res;
    }); 
  }
  
  // Parent scroll stop on the child Mouse hover
  @HostListener('window:mousewheel', ['$event']) 
    onMousewheel(event:any){
        event.stopPropagation();
    }

    @HostListener('window:mousewheel', ['$event']) 
    onDOMMouseScroll(event:any){
        event.stopPropagation();
    }

  postToNaukri() {
    let postToNaukriTO: PostToNaukriModel = new PostToNaukriModel();
    postToNaukriTO.requisitionID = this.requisitionID;
    postToNaukriTO.jobBoardID = this.jobBoardID;
    postToNaukriTO.doctorateQualifications = this.naukriDoctorateQualificationCode;
    postToNaukriTO.doctorateSpecializations = this.naukriDoctorateSpecializationCode;
    postToNaukriTO.email = this.naukriForm.controls.email.value;
    postToNaukriTO.hiringOrgName = this.naukriForm.controls.hiringOrgName.value;
    postToNaukriTO.indianLoc = this.naukriJobCityCode;
    postToNaukriTO.interNationalLoc = this.naukriJobCountryCode;
    postToNaukriTO.jobAction = this.naukriForm.controls.jobAction.value;
    postToNaukriTO.jobFunctionCode = this.naukriJobFunctionCode;
    postToNaukriTO.jobIndustryCode = this.naukriJobIndustryCode;
    postToNaukriTO.jobKeywords = this.naukriForm.controls.jobKeywords.value;
    postToNaukriTO.jobPositionPostingID = this.naukriForm.controls.jobPositionPostingID.value;
    postToNaukriTO.jobPositionTitle = this.naukriForm.controls.jobPositionTitle.value;
    postToNaukriTO.jobRoleCode = this.naukriJobRoleCode;
    postToNaukriTO.jobSummaryText = this.naukriForm.controls.jobSummaryText.value;
    postToNaukriTO.jobType = this.naukriForm.controls.jobType.value;
    postToNaukriTO.maximumExperience = this.naukriMaxExperienceCode;
    postToNaukriTO.maximumSalary = this.naukriMaxSalaryCode;
    postToNaukriTO.minimumExperience = this.naukriMinExperienceCode;
    postToNaukriTO.minimumSalary = this.naukriMinSalaryCode;
    postToNaukriTO.pgQualifications = this.naukriPgQualificationCode;
    postToNaukriTO.pgSpecializations = this.naukriPgSpecializationCode;
    postToNaukriTO.salaryCurrency = this.naukriSalaryCurrenyCode;
    postToNaukriTO.summaryText = this.naukriForm.controls.summaryText.value;
    postToNaukriTO.ugQualifications = this.naukriUgQualificationCode;
    postToNaukriTO.ugSpecializations = this.naukriUgSpecializationCode;
    postToNaukriTO.url = '';
    postToNaukriTO.questionnaire = 'Apply_Integration';
    postToNaukriTO.jobCode = this.data.requisitionCode;
    postToNaukriTO.displaySalary = this.naukriForm.controls.displaySalary.value;

    let valid: boolean = true;
    if (postToNaukriTO.requisitionID == null) {
      this.setMessage("WARNING", 'common.label.requistionId', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobBoardID == null) {
      this.setMessage("WARNING", 'common.label.jobBoardID', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobAction == null || postToNaukriTO.jobAction == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobAction', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobPositionPostingID == null) {
      this.setMessage("WARNING", 'postJobBoard.label.jobPositionId', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobType == null || postToNaukriTO.jobType == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobType', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.hiringOrgName == null || postToNaukriTO.hiringOrgName == '') {
      this.setMessage("WARNING", 'postJobBoard.label.orgName', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.summaryText == null || postToNaukriTO.summaryText == '') {
      this.setMessage("WARNING", 'postJobBoard.label.orgDesc', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobPositionTitle == null || postToNaukriTO.jobPositionTitle == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobTitle', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobFunctionCode == null || postToNaukriTO.jobFunctionCode == '') {
      this.setMessage("WARNING", 'postJobBoard.label.functionalArea', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobRoleCode == null || postToNaukriTO.jobRoleCode == '') {
      this.setMessage("WARNING", 'postJobBoard.label.role', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobIndustryCode == null || postToNaukriTO.jobIndustryCode == '') {
      this.setMessage("WARNING", 'postJobBoard.label.industry' , true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobKeywords == null || postToNaukriTO.jobKeywords == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobKeywords', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.indianLoc == null || postToNaukriTO.indianLoc.length == 0) {
      this.setMessage("WARNING", 'postJobBoard.label.indianLocation' , true);
      valid = false;
      return;
    }
    if (postToNaukriTO.salaryCurrency == null || postToNaukriTO.salaryCurrency == '') {
      this.setMessage("WARNING", 'postJobBoard.label.salaryCurrency', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.minimumSalary == null) {
      this.setMessage("WARNING", 'postJobBoard.label.minSalary', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.maximumSalary == null) {
      this.setMessage("WARNING", 'postJobBoard.label.maxSalary', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.jobSummaryText == null || postToNaukriTO.jobSummaryText == '') {
      this.setMessage("WARNING",'postJobBoard.label.jD' , true);
      valid = false;
      return;
    }
    if( postToNaukriTO.jobSummaryText && postToNaukriTO.jobSummaryText.split(" ").length > 0 ){
      let parts = postToNaukriTO.jobSummaryText.split(" ");
      for( let part of parts ){
        if( part.length > 32 ){
          this.setMessage("WARNING", 'postJobBoard.warning.insertSpace', false);
          valid = false;
          return;
        }
      }
    }
    
    if (postToNaukriTO.minimumExperience == null) {
      this.setMessage("WARNING", 'postJobBoard.label.minExp', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.maximumExperience == null) {
      this.setMessage("WARNING", 'postJobBoard.label.maxExp' , true);
      valid = false;
      return;
    }
    if (postToNaukriTO.ugQualifications == null || postToNaukriTO.ugQualifications == '') {
      this.setMessage("WARNING", 'postJobBoard.label.ug', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.ugSpecializations == null || postToNaukriTO.ugSpecializations == '') {
      this.setMessage("WARNING", 'postJobBoard.label.ugSpecialization', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.pgQualifications == null || postToNaukriTO.pgQualifications == '') {
      this.setMessage("WARNING", 'postJobBoard.label.pg', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.pgSpecializations == null || postToNaukriTO.pgSpecializations == '') {
      this.setMessage("WARNING", 'postJobBoard.label.pgSpecialization', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.email == null || postToNaukriTO.email == '' || postToNaukriTO.email.trim() == '') {
      this.setMessage("WARNING", 'postJobBoard.label.email', true);
      valid = false;
      return;
    }

    if (postToNaukriTO.email) {
      let matches = postToNaukriTO.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
      if (!(matches != null && matches.length > 0 && matches[0] === postToNaukriTO.email)) {
        this.setMessage("WARNING", 'postJobBoard.warning.invailidEmail', false);
        valid = false;
        return;
      }
    }

    if (postToNaukriTO.doctorateQualifications == null || postToNaukriTO.doctorateQualifications == '') {
      this.setMessage("WARNING",'postJobBoard.label.doctorate', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.doctorateSpecializations == null || postToNaukriTO.doctorateSpecializations == '') {
      this.setMessage("WARNING", 'postJobBoard.label.doctorateSpecialization', true);
      valid = false;
      return;
    }
    if (postToNaukriTO.displaySalary == null || postToNaukriTO.displaySalary == '') {
      this.setMessage("WARNING", 'postJobBoard.label.displaySalary', true);
      valid = false;
      return;
    }

    if (valid == true) {

      this.postToJobBoardService.postToNaukri(postToNaukriTO).subscribe(response => {
        let messageTO: MessageCode = response.messageCode;
        let message: string = '';
        if (messageTO != null) {
          if (messageTO.code != null && messageTO.code === 'EC200') {
            this.jobPostingCode = response.response;
            message = messageTO.message.toString();
            this.dialogRef.close(this.jobPostingCode);
          } else if (messageTO.message != null) {
            message = messageTO.message.toString();
          } else {
            message = "Request failed to execute."
          }
          if (messageTO.code === 'EC200') {
            this.notificationService.setMessage(NotificationSeverity.INFO, message);
          } else {
            this.setMessage("WARNING", message, false);
          }
        }
      }, (onError) => {
        this.setMessage("WARNING", 'common.error.somthingWrong', false);
      }, () => {
      });
    }
  }

  initializeData() {
    this.postToJobBoardService.getJobBoardDataForNaukri(this.requisitionID).subscribe(out => {
      this.data = out.responseData;
      let jobDescription = this.data.jobDescription;
      if (jobDescription != null) {
        let htmlRegex = /(&nbsp;|<([^>]+)>)/ig;
        let cssRegex = /((<style>)|(<style type=.+))((\s+)|(\S+)|(\r+)|(\n+))(.+)((\s+)|(\S+)|(\r+)|(\n+))(<\/style>)/g;
        let commentsRegex = /\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm;
        let spacesRegex = /\s\s+/g;
        let newLineRegex = /\n\s*\n/g;
        let keyboardAscii = /[^\x00-\x7F]/g;
        jobDescription = jobDescription.replace(/<br\s*\/?>/mg, "\n").replace(commentsRegex, '').replace(cssRegex, '').replace(htmlRegex, "").replace(keyboardAscii, "").replace(newLineRegex, "\n").trim();
      }
      this.naukriForm.controls['jobSummaryText'].setValue(jobDescription);
      this.naukriForm.controls['jobRoleCode'].setValue(this.data.requisitionRoleIDOrg);
      this.naukriForm.controls['jobPositionTitle'].setValue(this.data.jobTitle);
      this.naukriForm.controls['hiringOrgName'].setValue(this.data.organizationName);
      this.naukriForm.controls['summaryText'].setValue(this.data.organizationDesc);
      this.naukriForm.controls['salaryCurrency'].setValue(this.data.currencyCodeID);
      this.naukriForm.controls['minimumExperience'].setValue(this.data.minimumExp);
      this.naukriForm.controls['maximumExperience'].setValue(this.data.maximumExp);
      this.naukriForm.controls['minimumSalary'].setValue(this.data.minSalary);
      this.naukriForm.controls['maximumSalary'].setValue(this.data.maxSalary);
      this.naukriForm.controls['jobIndustryCode'].setValue(this.data.industryID);
    });
  }

  getMasters() {
    this.jobActionList = [new SelectItem('ADD', 'ADD'), new SelectItem('DELETE', 'DELETE')];

    this.jobTypeList = [new SelectItem('HV', 'h'), new SelectItem('private', 'p')];

    this.salaryCurrencyList = [new SelectItem('Rupees', '1'), new SelectItem('U.S Dollars', '2')];
    this.naukriForm.controls['salaryCurrency'].setValue("Rupees");
    this.naukriSalaryCurrenyCode = "Rupees";

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "INDUSTRY").subscribe(response => {
      this.jobIndustryCodeList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "FUNCTIONAL_AREA").subscribe(response => {
      this.jobFunctionCodeList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "MINEXP").subscribe(response => {
      this.minimumExperienceList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "MINSALARY").subscribe(response => {
      this.minimumSalaryList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "INDIANLOC").subscribe(response => {
      this.indianLocList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "INTERNATIONALLOC").subscribe(response => {
      this.interNationalLocList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "UGQUALIFICATION").subscribe(response => {
      this.ugQualificationsList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "PGQUALIFICATION").subscribe(response => {
      this.pgQualificationsList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "DOCTORATEQUALIFICATION").subscribe(response => {
      this.doctorateQualificationsList = response.responseData;
    });

  }

  getRoleList(functionalAreaID: number) {
    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "ROLE", functionalAreaID).subscribe(response => {
      this.jobRoleCodeList = response.responseData;
    });
  }

  getMaxExperience(minExperience: any) {
    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "MAXEXP", minExperience).subscribe(response => {
      this.maximumExperienceList = response.responseData;
    });
  }

  getMaxSalary(minSalary: any) {
    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "MAXSALARY", minSalary).subscribe(response => {
      this.maximumSalaryList = response.responseData;
    });
  }

  getUGSpecialization(ugQualification: any) {
    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "UGSPECIALIZATION", ugQualification).subscribe(response => {
      this.ugSpecializationsList = response.responseData;
    });
  }

  getPGSpecialization(pgQualification: any) {
    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "PGSPECIALIZATION", pgQualification).subscribe(response => {
      this.pgSpecializationsList = response.responseData;
    });
  }

  getDoctorateSpecialization(doctorateQualification: any) {
    this.postToJobBoardService.getJobBoardMaster("NAUKRI", "DOCTORATESPECIALIZATION", doctorateQualification).subscribe(response => {
      this.doctorateSpecializationsList = response.responseData;
    });
  }

  addIndianLocationListener(event: SelectItem) {
      this.selectedLocationlenghth = this.selectedLocationlenghth + 1;
      this.selectedIndianLocation.push(event);
      this.naukriJobCityCode.push(event.code);
  }

  public setMessage(severity: string, message: string, isRequiredValidation: boolean) {

    this.i18UtilService.get(message).subscribe((res: string) => {
      if (isRequiredValidation) {
        this.message = res + this.isRequiredText;
      }
      else {
        this.message = res;
      }
      this.severity = severity;

      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);

    });


  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

}
