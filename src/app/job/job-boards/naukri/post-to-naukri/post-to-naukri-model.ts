export class PostToNaukriModel {
    jobAction : String ='' ;

    jobPositionPostingID : String ='' ;

    jobType : String ='' ;

    hiringOrgName : String ='' ;

    summaryText : String='' ;

    jobPositionTitle  : String ='' ;

    jobIndustryCode : String='' ;

    jobFunctionCode  : String='' ;

    jobRoleCode : String ='' ;

    jobKeywords  : String ='' ;

    interNationalLoc : String ='' ;

    indianLoc : String[]=[''] ;

    salaryCurrency : String ='' ;

    minimumSalary : String ='' ;

    maximumSalary  : String='' ;

    jobSummaryText : String ='' ;

    minimumExperience : String='' ;

    maximumExperience : String='' ;

    ugQualifications : String='' ;

    ugSpecializations : String ='' ;

    pgQualifications : String ='' ;

    pgSpecializations  : String ='' ;

    email : String ='' ;

    url : String='' ;

    doctorateQualifications : String ='' ;

    doctorateSpecializations : String='' ;

    jobBoardID: number;

    requisitionID: number;

    questionnaire: String;

    jobCode: String = '';

    displaySalary: String = ''
}