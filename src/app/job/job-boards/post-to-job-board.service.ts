import { Injectable } from '@angular/core';
import { SessionService } from "app/session.service";
import { Http } from "@angular/http";
import { Headers, RequestOptions } from '@angular/http';
import { PostToNaukriModel } from "app/job/job-boards/naukri/post-to-naukri/post-to-naukri-model";
import { PostToMonsterModel } from './monster/post-to-monster/post-to-monster-model';
import { PostToIimjobsModel } from './iimjobs/post-to-iimjobs/post-to-iimjobs-model';

@Injectable()
export class PostToJobBoardService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  postToNaukri(postToNaukriTO: PostToNaukriModel) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobboard/posttonaukri/', JSON.stringify(postToNaukriTO), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  postToMonster(postToMonsterTO: PostToMonsterModel) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobboard/posttomonster/', JSON.stringify(postToMonsterTO), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  postToIIMJobs(postToIIMJobsTO: PostToIimjobsModel) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobboard/posttoiimjobs/', JSON.stringify(postToIIMJobsTO), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getJobBoardDataForNaukri(requisitionID: number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobboard/naukri/fetch/' + requisitionID + "/")
      .map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getMonsterJobBoardID() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobboard/monsterjobboardid/').
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getJobBoardMaster(sysJobBoard: string, masterType: string, parentID?: number) {
    let url = this.sessionService.orgUrl + '/rest/altone/jobboard/master/'
      + '?sysJobBoard=' + sysJobBoard + '&masterType=' + masterType
    if (parentID != null && parentID != undefined) {
      url = url + '&parentID=' + parentID
    }
    return this.http.get(url).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });

  }

}
