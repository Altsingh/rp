import { Component, OnInit, Input, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MatNativeDateModule } from '@angular/material';
import { JobActionService } from '../../../job-action/job-action.service';
import { SessionService } from '../../../../session.service';
import { SelectItem } from '../../../../shared/select-item';
import { PostToNaukriModel } from "app/job/job-boards/naukri/post-to-naukri/post-to-naukri-model";
import { PostToJobBoardService } from "app/job/job-boards/post-to-job-board.service";
import { MessageCode } from '../../../../common/message-code';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { PostToMonsterModel } from 'app/job/job-boards/monster/post-to-monster/post-to-monster-model';
import { I18UtilService } from 'app/shared/services/i18-util.service';



@Component({
  selector: 'alt-post-to-monster',
  templateUrl: './post-to-monster.component.html',
  styleUrls: ['./post-to-monster.component.css'],
  providers: [PostToJobBoardService]
})
export class PostToMonsterComponent implements OnInit {
  jobActionList: SelectItem[];
  jobTypeList: SelectItem[];
  jobFunctionCodeList: SelectItem[];
  jobRoleCodeList: SelectItem[];
  jobIndustryCodeList: SelectItem[];
  locationList: SelectItem[];
  degreeList: SelectItem[];
  monsterLocationCode: any;
  monsterDegreeCode: any;
  monsterJobIndustryCode: any;
  monsterJobFunctionCode: any;
  monsterJobRoleCode: any;
  jobPostingCode: String;
  message: String;
  severity: String;
  timer: any;
  severityClass: String;
  monsterForm: FormGroup;
  requisitionID: number;
  jobBoardID: number;
  data: any; service
  functionalAreaID: number;
  monster: boolean = false;
  isRequiredText: string;

  @Input() set spinner(value: boolean) {
    this.monster = false;
  }

  constructor(
    public dialogRef: MatDialogRef<PostToMonsterComponent>,
    private sessionService: SessionService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private postToJobBoardService: PostToJobBoardService,
    private i18UtilService: I18UtilService
  ) {
    
    this.monsterForm = this.formBuilder.group({

      jobAction: 'ADD',
      salaryCurrency: 'INR',
      jobRefID: '',
      jobTitle: '',
      functionalArea: '',
      role: '', 
      jobIndustryCode: '',
      location: '',
      minimumExperience: '', 
      maximumExperience: '', 
      degree: '',
      minimumSalary: '',
      maximumSalary: '',
      keySkills: '',
      jobType: '',
      jobSummaryText: '',
      jobDescription: '',
      name: '',
      contactNo: '',
      email: ''
    })
  }

  ngOnInit() {
    this.requisitionID = this.sessionService.object.requisitionId;
    this.jobBoardID = this.sessionService.object.jobBoardID;
    this.monsterForm.controls['jobRefID'].setValue(this.requisitionID);
    this.getMasters();
    this.initializeData();
    this.i18UtilService.get('common.label.isRequired').subscribe((res: string) => {
      this.isRequiredText = res;
    }); 
  }

  // Parent scroll stop on the child Mouse hover
  @HostListener('window:mousewheel', ['$event']) 
    onMousewheel(event:any){
        event.stopPropagation();
    }

    @HostListener('window:mousewheel', ['$event']) 
    onDOMMouseScroll(event:any){
        event.stopPropagation();
    }

  getFunctionalAreaList(industryID: number) {
    this.postToJobBoardService.getJobBoardMaster("MONSTER", "FUNCTIONAL_AREA", industryID).subscribe(response => {
      this.jobFunctionCodeList = response.responseData;
    });
  }

  getRoleList(functionalAreaID: number) {
    this.postToJobBoardService.getJobBoardMaster("MONSTER", "ROLE", functionalAreaID).subscribe(response => {
      this.jobRoleCodeList = response.responseData;
    });
  }

  getMasters() {
    this.jobTypeList = [new SelectItem('fulltime', 'fulltime'), new SelectItem('contract', 'contract'), new SelectItem('parttime', 'parttime')];
    
    this.postToJobBoardService.getJobBoardMaster("MONSTER", "INDUSTRY").subscribe(response => {
      this.jobIndustryCodeList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("MONSTER", "DEGREE").subscribe(response => {
      this.degreeList = response.responseData;
    });

    this.postToJobBoardService.getJobBoardMaster("MONSTER", "INDIANLOC").subscribe(response => {
      this.locationList = response.responseData;
    });
  }

  initializeData() {

    this.postToJobBoardService.getJobBoardDataForNaukri(this.requisitionID).subscribe(out => {
      this.data = out.responseData;
      
      let jobDescription = this.data.jobDescription;
      if( jobDescription != null ){
       let htmlRegex = /(&nbsp;|<([^>]+)>)/ig;
       let cssRegex = /((<style>)|(<style type=.+))((\s+)|(\S+)|(\r+)|(\n+))(.+)((\s+)|(\S+)|(\r+)|(\n+))(<\/style>)/g;
       let commentsRegex = /\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm;
       let spacesRegex = /\s\s+/g;
       let newLineRegex = /\n\s*\n/g;
       let keyboardAscii = /[^\x00-\x7F]/g;
        jobDescription = jobDescription.replace(/<br\s*\/?>/mg, "\n").replace(commentsRegex, '').replace(cssRegex, '').replace(htmlRegex, "").replace(keyboardAscii, "").replace(newLineRegex, "\n").trim();
      }

      this.monsterForm.controls['jobTitle'].setValue(this.data.jobTitle);
      this.monsterForm.controls['minimumExperience'].setValue(this.data.minimumExp);
      this.monsterForm.controls['maximumExperience'].setValue(this.data.maximumExp);
      this.monsterForm.controls['jobDescription'].setValue(jobDescription);
      this.monsterForm.controls['minimumSalary'].setValue(this.data.minimumSalary);
      this.monsterForm.controls['maximumSalary'].setValue(this.data.maximumSalary);
    });
  }

  postToMonster() {

    let postToMonsterTO: PostToMonsterModel = new PostToMonsterModel();
    postToMonsterTO.jobBoardID = this.jobBoardID;
    postToMonsterTO.requisitionID = this.requisitionID;
    postToMonsterTO.jobRefID = this.monsterForm.controls.jobRefID.value;
    postToMonsterTO.contactNo = this.monsterForm.controls.contactNo.value;
    postToMonsterTO.degree = this.monsterDegreeCode;
    postToMonsterTO.email = this.monsterForm.controls.email.value;
    postToMonsterTO.functionalArea = this.monsterJobFunctionCode;
    postToMonsterTO.jobAction = this.monsterForm.controls.jobAction.value;
    postToMonsterTO.jobIndustryCode = this.monsterJobIndustryCode;
    postToMonsterTO.jobSummaryText = this.monsterForm.controls.jobSummaryText.value;
    postToMonsterTO.jobDescription = this.monsterForm.controls.jobDescription.value;
    postToMonsterTO.jobTitle = this.monsterForm.controls.jobTitle.value;
    postToMonsterTO.jobType = this.monsterForm.controls.jobType.value;
    postToMonsterTO.keySkills = this.monsterForm.controls.keySkills.value;
    postToMonsterTO.location = this.monsterLocationCode
    postToMonsterTO.maximumExperience = this.monsterForm.controls.maximumExperience.value;
    postToMonsterTO.maximumSalary = this.monsterForm.controls.maximumSalary.value;
    postToMonsterTO.minimumExperience = this.monsterForm.controls.minimumExperience.value;
    postToMonsterTO.minimumSalary = this.monsterForm.controls.minimumSalary.value;
    postToMonsterTO.name = this.monsterForm.controls.name.value;
    postToMonsterTO.role = this.monsterJobRoleCode;
    postToMonsterTO.salaryCurrency = this.monsterForm.controls.salaryCurrency.value;
    postToMonsterTO.jobCode = this.data.requisitionCode;

    let valid = true;

    if (postToMonsterTO.jobAction == null || postToMonsterTO.jobAction == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobAction', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.jobTitle == null || postToMonsterTO.jobTitle == '') {
      this.setMessage("WARNING", 'postJobBoard.label.jobTitle', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.jobIndustryCode == null || postToMonsterTO.jobIndustryCode == '') {
      this.setMessage("WARNING", 'postJobBoard.label.industry', true);
      valid = false;
      return;
    }


    if (postToMonsterTO.functionalArea == null || postToMonsterTO.functionalArea == '') {
      this.setMessage("WARNING", 'postJobBoard.label.functionalArea', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.role == null || postToMonsterTO.role == '') {
      this.setMessage("WARNING", 'postJobBoard.label.role', true);
      valid = false;
      return;
    }


    if (postToMonsterTO.location == null || postToMonsterTO.location == '') {
      this.setMessage("WARNING", 'postJobBoard.label.location', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.minimumExperience == null || postToMonsterTO.minimumExperience == '') {
      this.setMessage("WARNING",'postJobBoard.label.minExp', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.maximumExperience == null || postToMonsterTO.maximumExperience == '') {
      this.setMessage("WARNING", 'postJobBoard.label.maxExp', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.degree == null || postToMonsterTO.degree == '') {
      this.setMessage("WARNING", 'postJobBoard.label.degree' , true);
      valid = false;
      return;
    }

    if (postToMonsterTO.keySkills == null || postToMonsterTO.keySkills == '') {
      this.setMessage("WARNING", 'postJobBoard.label.keySkills', true);
      valid = false;
      return;
    }


    if (postToMonsterTO.jobDescription == null || postToMonsterTO.jobDescription == '') {
      this.setMessage("WARNING",'postJobBoard.label.jD', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.name == null || postToMonsterTO.name == '') {
      this.setMessage("WARNING", 'postJobBoard.label.name', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.email == null || postToMonsterTO.email == '') {
      this.setMessage("WARNING", 'postJobBoard.label.email', true);
      valid = false;
      return;
    }

    if (postToMonsterTO.minimumExperience > postToMonsterTO.maximumExperience) {

      this.setMessage("WARNING", 'postJobBoard.warning.minExpExceeded', false)
      valid = false;
      return;
    }

    if (postToMonsterTO.minimumSalary != null && postToMonsterTO.maximumSalary != null) {
      if (postToMonsterTO.minimumSalary > postToMonsterTO.maximumSalary) {

        this.setMessage("WARNING", 'job.warning.minMaxSalaryWarning', false)
        valid = false;
        return;
      }
    }

    if (valid = true) {
      this.monster = true;
      this.postToJobBoardService.postToMonster(postToMonsterTO).subscribe(response => {
        let messageTO: MessageCode = response.messageCode;
        let message: string = '';
        if (messageTO != null) {
          if (messageTO.code != null && messageTO.code === 'EC200') {
            this.jobPostingCode = response.response;
            message = messageTO.message.toString();
            this.dialogRef.close(this.jobPostingCode);

          } else if (messageTO.message != null) {
            message = messageTO.message.toString();
          } else {
            message = "Request failed to execute."
          }
          if (messageTO.code === 'EC200') {
            this.monster = false;
            this.notificationService.setMessage(NotificationSeverity.INFO, message);
          } else {
            this.monster = false;
            this.setMessage("WARNING", message, false);
          }
        }
      }, (onError) => {
        this.monster = false;
        this.setMessage("WARNING", 'common.error.somthingWrong', false);
      }, () => {
      });
    }
  }

  public setMessage(severity: string, message: string, isRequiredValidation: boolean) {

    this.i18UtilService.get(message).subscribe((res: string) => {
      if (isRequiredValidation) {
        this.message = res + this.isRequiredText;
      }
      else {
        this.message = res;
      }
      this.severity = severity;

      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
      }, 3000);

    });


  }


  clearMessages() {
    this.severity = "";
    this.message = "";
  }
}
