export class PostToMonsterModel{

    jobRefID : String = '';

    jobAction :  String = '';
     
    jobTitle : String = '';

    functionalArea : String = '';

    role : String = '';

    jobIndustryCode : String ='';

    location : String = '';
     
    minimumExperience : String = '';

    maximumExperience : String =  '';

    degree : String = '';

    salaryCurrency : String = '';

    minimumSalary : String = '';

    maximumSalary : String = '';

    keySkills : String = '';

    jobType : String = '' ;

    jobSummaryText : String = '';

    jobDescription : String = '';

    name : String = '';

    contactNo : String ='';

    email : String = '';

    jobBoardID: number;

    requisitionID: number;

    jobCode : String='';

}