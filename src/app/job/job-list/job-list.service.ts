import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from '../../session.service';
import { JobFilterTO } from './job-filter-to';

@Injectable()
export class JobListService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getJobList(filterTO: JobFilterTO) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/joblist/', JSON.stringify(filterTO), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getJobListTotalRows(filterTO: JobFilterTO) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/joblist/totalRows/', JSON.stringify(filterTO), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getActiveJobsList(filterTO: JobFilterTO) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/activeJobs/', JSON.stringify(filterTO), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getActiveJobsListTotalRows(filterTO: JobFilterTO) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/activeJobs/totalRows/', JSON.stringify(filterTO), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getPublishToTotalCount() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/allPublishCount/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

}