export class JobFilterTO {
  hiringManagerID: number;
  recruiterID: number;
  vendorID: number;
  jobStatus: string;
  joiningLocation: string;
  startCreationDate: Date;
  endCreationDate: Date;
  startModificationDate: Date;
  endModificationDate: Date;
  postedON: string;
  orgUnit: string;
  worksite: string;
  grade: string;

  pageNumber: number;
  pageSize: number;
  sortBy: string;
  sortOrder: string;
  searchString: string;

  isFiltered(): boolean {
    return this.hiringManagerID != null
      || this.recruiterID != null
      || this.vendorID != null
      || this.jobStatus != null
      || this.joiningLocation != null
      || this.startCreationDate != null
      || this.endCreationDate != null
      || this.startModificationDate != null
      || this.endModificationDate != null
      || this.searchString != null
      || this.orgUnit != null
      || this.worksite != null
  }
}