import { Injectable } from '@angular/core';
import { PagedDataService } from '../../common/paged-data.service';
import { SessionService } from '../../session.service';
import { Http } from '@angular/http';

@Injectable()
export class JobListAlljobsPagedService extends PagedDataService{

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/jobs/joblist/',
      '/rest/altone/jobs/joblist/totalRows/'
    );
  }

}
