import { JobCommonServiceService } from './../job-common-service.service';
import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { JobListService } from './job-list.service';
import { Observable } from 'rxjs/Observable';
import { MessageCode } from '../../common/message-code';
import { JobListResponse } from '../../common/job-list-response';
import { DialogService } from '../../shared/dialog.service';
import { AssignToRecruiterComponent } from '../../job/job-action/assign-to-recruiter/assign-to-recruiter.component';
import { SessionService } from '../../session.service';
import { CommonService } from '../../common/common.service';
import { MatTooltipModule } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { PostToIjpComponent } from '../../job/job-action/post-to-ijp/post-to-ijp.component';
import { PostToEmpRefComponent } from "../../job/job-action/post-to-emp-ref/post-to-emp-ref.component";
import { PostToCandidatePortalComponent } from "../../job/job-action/post-to-candidate-portal/post-to-candidate-portal.component";
import { NameinitialService } from '../../shared/nameinitial.service'
import { JobListAlljobsPagedService } from './job-list-alljobs-paged.service';
import { JobListMyjobsPagedService } from './job-list-myjobs-paged.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PageState } from '../../shared/paginator/paginator.component';
import { JobFilterTO } from './job-filter-to';
import { AssignToVendorsComponent } from '../../job/job-action/assign-to-vendors-bulk/assign-to-vendors-bulk.component';
import { AssignToRecuiterBulkComponent } from '../../job/job-action/assign-to-recuiter-bulk/assign-to-recuiter-bulk.component';
import { PublishToInternalBulkComponent } from '../../job/job-action/publish-to-internal-bulk/publish-to-internal-bulk.component';
import { PublishToEmployeeBulkComponent } from '../../job/job-action/publish-to-employee-bulk/publish-to-employee-bulk.component';
import { PublishToCandidateBulkComponent } from '../../job/job-action/publish-to-candidate-bulk/publish-to-candidate-bulk.component';
import { JobActionService } from '../job-action/job-action.service'
import { NotificationService } from "../../common/notification-bar/notification.service";
import { FormatJobcodeParamPipe } from '../../shared/format-jobcode-param.pipe';
import { JobDetailService } from '../job-detail/job-detail.service';
import 'rxjs/add/operator/toPromise';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css'],
  providers: [JobListService, JobListAlljobsPagedService, JobListMyjobsPagedService]
})

export class JobListComponent implements OnInit, OnDestroy {
  showloader: boolean = false;
  load: number = 3;
  pageState: PageState = new PageState();
  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;
  filterTO: JobFilterTO;
  isToggled: boolean;

  loaderIndicator: string;
  filterHidden: boolean = true;
  applyRecruiterFilter: boolean = true;
  showStyle: false;
  isClassVisible: false;
  jobListData: JobListResponse[];
  messageCode: MessageCode;
  subscription: any;
  JOB_STATUS_CLASS: String = "";
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";
  selected: any[] = [];
  selectAll: boolean;
  currentURL: string;
  pageTitle: string;
  publishedToTotal: number;
  jobDetails: any;
  RecruitersOfJob: any;
  RecruiterList: any;
  formatJobcodeParamPipe: FormatJobcodeParamPipe = new FormatJobcodeParamPipe();
  featureDataMap: Map<any,any>;
  isBulkMenu : Boolean = true;
  allJobsLabel: string;
  myJobLabel: string;

  constructor(private jobListService: JobListService, private DialogService: DialogService,
    private sessionService: SessionService, private commonService: CommonService,
    private nameinitialService: NameinitialService,
    private router: Router, private jobActionBulkService: JobActionService,
    private notificationService: NotificationService,
    private jobDetailService: JobDetailService,
    private jobListAllJobsPagedService: JobListAlljobsPagedService,
    private jobListMyJobsPagedService: JobListMyjobsPagedService,
    private jobCommonServiceService: JobCommonServiceService,
    private formBuilder: FormBuilder,
    private i18UtilService: I18UtilService) {
    this.randomcolor = this.nameinitialService.randomcolor;
    this.filterTO = new JobFilterTO();
    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });
    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {
      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }
      this.queryTimeout = setTimeout(() => {
        let query = this.queryFormGroup.controls.query.value;
        if (query.length >= 3) {
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          if (this.pageTitle === this.allJobsLabel) {
            this.jobListAllJobsPagedService.clearCache();
            this.loadAllJobsData();
            this.loadAllJobsTotalRows();
          } else if (this.pageTitle === this.myJobLabel) {
            this.jobListMyJobsPagedService.clearCache();
            this.loadMyJobsData();
            this.loadMyJobsTotalRows();
          }
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          if (this.pageTitle === this.allJobsLabel) {
            this.jobListAllJobsPagedService.clearCache();
            this.loadAllJobsData();
            this.loadAllJobsTotalRows();
          } else if (this.pageTitle === this.myJobLabel) {
            this.jobListMyJobsPagedService.clearCache();
            this.loadMyJobsData();
            this.loadMyJobsTotalRows();
          }
        }
      }, 200);

    });
    this.jobActionBulkService.setJobComponent(this);
    this.featureDataMap = this.sessionService.featureDataMap;
    if(this.featureDataMap.get("PUBLISH_TO_EMPLOYEE_REFERRAL") == 3 && this.featureDataMap.get("PUBLISH_TO_IJP") == 3 && this.featureDataMap.get("PUBLISH_TO_CP") == 3 && this.featureDataMap.get("PUBLISH_TO_VENDOR") == 3 && this.featureDataMap.get("ASSIGN_TO_RECRUITER") == 3){
      this.isBulkMenu = false;
    }
  }

  ngOnInit() {
    this.selected = [];
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.loaderIndicator = "loader";
    this.filterHidden = true;
    this.commonService.showRHS();

    this.jobListService.getPublishToTotalCount().subscribe(out => {
      this.publishedToTotal = out.response
    });

    if (this.router.url.endsWith('joblist')) {
      this.i18UtilService.get('job.job_list.allJobs').subscribe((res: string) => {
        this.allJobsLabel = res;
        this.pageTitle = this.allJobsLabel;
        this.applyRecruiterFilter = true;
        this.loadAllJobsData();
        this.loadAllJobsTotalRows();
      });
    } else if (this.router.url.endsWith('myjobs')) {
      this.i18UtilService.get('job.job_list.myJobs').subscribe((res: string) => {
        this.myJobLabel = res;
        this.pageTitle = this.myJobLabel;
        this.applyRecruiterFilter = false;
        this.loadMyJobsData();
        this.loadMyJobsTotalRows();
      });
    }
  }
    
    

  public isThisJobDisabled(jobStatus){
    return this.jobCommonServiceService.isDisableCriteria(jobStatus);
  }

  loadAllJobsTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.jobListAllJobsPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadAllJobsData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.jobListAllJobsPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.jobListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  loadMyJobsTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.jobListMyJobsPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadMyJobsData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.jobListMyJobsPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.jobListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;
    if (this.pageTitle === this.allJobsLabel) {
      this.loadAllJobsData();
    } else if (this.pageTitle === this.myJobLabel) {
      this.loadMyJobsData();
    }
  }

  public sort(sortParam: string, order: string) {

    this.sortBy = sortParam;
    this.sortOrder = order;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;

    if (this.pageTitle === this.allJobsLabel) {
      this.jobListAllJobsPagedService.clearCache();
      this.loadAllJobsData();
    } else if (this.pageTitle === this.myJobLabel) {
      this.jobListMyJobsPagedService.clearCache();
      this.loadMyJobsData();
    }
  }

  areAllSelected() {
    if (this.jobListData != null && this.jobListData.length > 0) {
      for (let x = 0; x < this.jobListData.length; x++) {
        if (typeof this.jobListData[x].selected == 'undefined' || (typeof this.jobListData[x].selected == 'boolean' && !this.jobListData[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }

  reloadNgOnInit() {
    if (this.router.url.endsWith('joblist')) {
      this.pageTitle = this.allJobsLabel;
      this.jobListAllJobsPagedService.clearCache();
      this.loadAllJobsData();
      this.loadAllJobsTotalRows();
    } else if (this.router.url.endsWith('myjobs')) {
      this.pageTitle = this.myJobLabel;
      this.jobListMyJobsPagedService.clearCache();
      this.loadMyJobsData();
      this.loadMyJobsTotalRows();
    }
  }

  gotoJobDetail(data) {
    this.jobDetailService.object = data.requisitionId;
    this.jobDetailService.objectType = 'REQUISITION_ID';
    this.router.navigateByUrl('/job/edit-job/' + this.formatJobcodeParamPipe.transform(data.requisitionCode) + '/job-details');
  }

  ngOnDestroy(): void {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }

  }

  toggleFilter() {
    this.filterHidden = !this.filterHidden;
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.city.length;
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  onSorted(params: string[]) {
    this.filterTO.sortBy = params[0];
    this.filterTO.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    
    if (this.pageTitle === this.allJobsLabel) {
      this.jobListAllJobsPagedService.clearCache();
    } else if (this.pageTitle === this.myJobLabel) {
      this.jobListMyJobsPagedService.clearCache();
    }
    this.ngOnInit();
  }

  onFilter(params: any[]) {
    let hiringManagerFilter = false;
    let recruiterFilter = false;
    let jobStatusFilter = false;
    let locationFilter = false;
    let postedFilter = false;
    let orgUnitFilter = false;
    let workSiteFilter = false;
    for (let param of params) {
      if (param.name == 'HIRING_MANAGER') {
        this.filterTO.hiringManagerID = param.value.value;
        hiringManagerFilter = true;
      } else if (param.name == 'RECRUITER') {
        this.filterTO.recruiterID = param.value.value;
        recruiterFilter = true;
      } else if (param.name == 'JOB_STATUS') {
        this.filterTO.jobStatus = param.value.value;
        jobStatusFilter = true;
      } else if (param.name == 'LOCATION') {
        this.filterTO.joiningLocation = param.value.value;
        locationFilter = true;
      } else if (param.name == 'POSTED_ON') {
        this.filterTO.postedON = param.value.value;
        postedFilter = true;
      } else if (param.name == 'ORG_UNIT') {
        this.filterTO.orgUnit = param.value.value;
        orgUnitFilter = true;
      } else if (param.name == 'WORKSITE') {
        this.filterTO.worksite = param.value.value;
        workSiteFilter = true;
      }
    }
    if (!hiringManagerFilter) {
      this.filterTO.hiringManagerID = null;
    }
    if (!recruiterFilter) {
      this.filterTO.recruiterID = null;
    }
    if (!jobStatusFilter) {
      this.filterTO.jobStatus = null;
    }
    if (!locationFilter) {
      this.filterTO.joiningLocation = null;
    }
    if (!postedFilter) {
      this.filterTO.postedON = null;
    }
    if (!orgUnitFilter) {
      this.filterTO.orgUnit = null;
    }
    if (!workSiteFilter) {
      this.filterTO.worksite = null;
    }
    if (this.pageTitle === this.allJobsLabel) {
      this.jobListAllJobsPagedService.clearCache();
    } else if (this.pageTitle === this.myJobLabel) {
      this.jobListMyJobsPagedService.clearCache();
    }
    this.ngOnInit();
  }
  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    }else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    if (this.pageTitle === this.allJobsLabel) {
      this.jobListAllJobsPagedService.clearCache();
    } else if (this.pageTitle === this.myJobLabel) {
      this.jobListMyJobsPagedService.clearCache();
    }
    this.ngOnInit();
  }
  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();

  }
  changed(event) {
    this.showloader = event;
  }
  allSelectEvent() {
    let index = 0;
    for (let data of this.jobListData) {
      data.selected = this.selectAll;
      if( this.isThisJobDisabled(data.sysRequisitionStatus) ){
        data.selected = false;
      }
      index++;
      if (this.selectAll && data.selected && !this.selected.includes(data)) {
        this.selected.push(data);
      } else if (!this.selectAll) {
        let i = this.selected.indexOf(data);
        this.selected.splice(i, 1);
      }
    }
  }

  openBulkAction(actiontype) {
    this.notificationService.clear();
    if (this.selected.length == 0) {
      this.i18UtilService.get('job.warning.selectJob').subscribe((res: string) => {
        this.notificationService.setMessage("WARNING", res);
      });
      return;
    }
    this.jobActionBulkService.setDataList(this.selected)
    if (actiontype == 'V') {
      this.showloader = true;
      this.jobActionBulkService.getVendorsForOrganization().subscribe(res => {
        this.showloader = false;
        this.DialogService.setDialogData(res);
        this.DialogService.open(AssignToVendorsComponent, { width: '935px' }).subscribe(result => {
          if (result != undefined && result == 'success') {
            this.reloadNgOnInit();
          }
        });
      });
    } else if (actiontype == 'R') {
      this.showloader = true;
      this.jobActionBulkService.getRecruiterList().subscribe(res => {
        this.DialogService.setDialogData(res);
        this.showloader = false;
        this.DialogService.open(AssignToRecuiterBulkComponent, { width: '935px' }).subscribe(result => {
          if (result != undefined && result == 'success') {
            this.reloadNgOnInit();
          }
        });
      });
    }
    else if (actiontype == 'I') {
      this.DialogService.open(PublishToInternalBulkComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && result == 'success') {
          this.reloadNgOnInit();
        }
      });
    }
    else if (actiontype == 'C') {
      this.DialogService.open(PublishToCandidateBulkComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && result == 'success') {
          this.reloadNgOnInit();
        }
      });
    }
    else if (actiontype == 'E') {
      this.DialogService.open(PublishToEmployeeBulkComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && result == 'success') {
          this.reloadNgOnInit();
        }
      });
    }
  }
  
  OpenAssignJobToRec(data: JobListResponse) {
    let requisitionId: Number = data.requisitionId;
    let jobCode: String = data.requisitionCode;
    this.sessionService.object = {
      requisitionId: requisitionId,
      jobCode: jobCode
    };
    this.showloader = true;
    Promise.all([this.jobActionBulkService.getJobDetailtoPromise(),
    this.jobActionBulkService.getRecruitersOfJobToPromise(),
    this.jobActionBulkService.getRecruiterListtoPromise()]).then(res => {
      this.DialogService.setDialogData(res[0]);
      this.DialogService.setDialogData1(res[1]);
      this.DialogService.setDialogData2(res[2])
      this.showloader = false;
      this.DialogService.open(AssignToRecruiterComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && typeof result == 'number') {
          data.teamCount = data.teamCount + result;
        }
      });
    });
  }
  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }
}
