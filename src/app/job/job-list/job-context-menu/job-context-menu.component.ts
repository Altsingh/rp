import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { DialogService } from '../../../shared/dialog.service';
import { SessionService } from '../../../session.service';
import { AssignToRecruiterComponent } from "../../job-action/assign-to-recruiter/assign-to-recruiter.component";
import { PostToIjpComponent } from "../../job-action/post-to-ijp/post-to-ijp.component";
import { PostToEmpRefComponent } from "../../job-action/post-to-emp-ref/post-to-emp-ref.component";
import { PostToCandidatePortalComponent } from "../../job-action/post-to-candidate-portal/post-to-candidate-portal.component";
import { PublishedVendorsListPopupComponent } from '../../job-detail/performance-source-team/published-vendors-list-popup/published-vendors-list-popup.component';
import { JobDetailService } from '../../job-detail/job-detail.service';
import { JobListResponse } from '../../../common/job-list-response';

@Component({
  selector: 'alt-job-context-menu',
  templateUrl: './job-context-menu.component.html',
  styleUrls: ['./job-context-menu.component.css']
})
export class JobContextMenuComponent implements OnInit {
  @Output() showloader: EventEmitter<boolean> = new EventEmitter<boolean>();
  displayClass: String = "displayNone";

  @Output() output: EventEmitter<any> = new EventEmitter();

  @Input()
  requisitionId: number;

  @Input()
  isThisJobDisabled: boolean;

  @Input()
  jobCode: string;

  @Input()
  jobData: JobListResponse;

  featureDataMap:Map<any,any>;

  constructor(private dialogsService: DialogService,
    private sessionService: SessionService,
    private jobDetailService: JobDetailService) { }

  ngOnInit() {
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  OpenAssignJobToRec() {
    this.hideContextMenu();
    this.sessionService.object = {
      requisitionId: this.requisitionId,
      jobCode: this.jobCode
    };
    this.dialogsService.open(AssignToRecruiterComponent, { width: '935px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        this.output.emit(result);
      }
    });
  }

  openPostJobToIJP() {
    this.hideContextMenu();
    this.sessionService.object = {
      requisitionId: this.requisitionId,
      jobCode: this.jobCode
    };
    this.dialogsService.open(PostToIjpComponent, { width: '580px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        this.output.emit(result);
      }
    });
  }

  openPostJobToEmpPortal() {
    this.hideContextMenu();
    this.sessionService.object = {
      requisitionId: this.requisitionId,
      jobCode: this.jobCode
    };
    this.dialogsService.open(PostToEmpRefComponent, { width: '580px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        this.output.emit(result);
      }
    });
  }

  openPostJobToCandPortal() {
    this.hideContextMenu();
    this.sessionService.object = {
      requisitionId: this.requisitionId,
      jobCode: this.jobCode
    };
    this.dialogsService.open(PostToCandidatePortalComponent, { width: '580px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        this.output.emit(result);
      }
    });
  }

  OpenAssignJobToVendor() {
    this.hideContextMenu();
    this.sessionService.object = {
      requisitionId: this.requisitionId,
      jobCode: this.jobCode
    };
    this.jobDetailService.objectType = 'REQUISITION_ID';
    this.jobDetailService.object = this.requisitionId;
    this.jobDetailService.requisitionCode = this.jobCode;
    this.showloader.emit(true);
    this.jobDetailService.getJobSummary(this.jobCode).subscribe((response) => {
      this.jobDetailService.jobSummaryData = response;
      this.getJobAssignedVendors();
    });
  }
  getJobAssignedVendors() {
    this.jobDetailService.getJobAssignedVendors(this.requisitionId).subscribe((response) => {
      this.dialogsService.setDialogData(response);
      //this.processVendorsResponse(response);
      this.showloader.emit(false);
      this.jobDetailService.requisitionId = this.requisitionId;
      this.dialogsService.open(PublishedVendorsListPopupComponent, { width: '935px' }).subscribe((res) => {
        if (typeof res == 'number') {
          this.output.emit(res);
        }
      });
    });
  }

  hideContextMenu() {
    this.displayClass = "displayNone";
  }

  showContextMenu() {
    this.displayClass = "displayBlock";
  }

}
