import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'jobListDataFilter'
})
export class JobListDataFilterPipe implements PipeTransform {

  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row => ((row.jobTitle != null && row.jobTitle.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.requisitionCode != null && row.requisitionCode.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.orgUnitCode != null && row.orgUnitCode.toLowerCase().indexOf(query.toLowerCase()) > -1)
       || (row.joiningLocation != null && row.joiningLocation.toLowerCase().indexOf(query.toLowerCase()) > -1)));
    }
    return array;
  }

}
