import { Component, OnInit, OnDestroy } from '@angular/core';
import { JobDetailService } from '../job-detail.service';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { HistoryData } from './history-data';
import { Subscription } from 'rxjs/Subscription'

@Component({
  selector: 'alt-history-chats',
  templateUrl: './history-chats.component.html',
  styleUrls: ['./history-chats.component.css']
})
export class HistoryChatsComponent implements OnInit, OnDestroy {
  toggleOpen: boolean = true;
  historyList: HistoryData[];
  historyDescriptionToolTip:string[]=[];
  jobDetailService: JobDetailService;
  sub: any;
  jobCode: String;
  loaded: boolean = false;
  subscription:Subscription;
  toggleComment : boolean [] =[] ;
  requisitionApprovalHistory : any;
  approvalHistoryDisplay : boolean = true;
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService) {
    this.jobDetailService = jobDetailService;
  }

  ngOnInit() {
    let jobCode = this.route.snapshot.parent.params['id'];
    this.sub = this.route.params.subscribe(params => {

      this.subscription = this.jobDetailService
        .getJobHistory(this.parser.transform(jobCode))
        .subscribe((p) => { 
            this.historyList = p; 
            this.loaded = true;
            this.requisitionApprovalHistory = this.historyList[0];
           
            if(this.requisitionApprovalHistory == null || this.requisitionApprovalHistory.length ==0)
            this.approvalHistoryDisplay = false;

            for(let i=1;i<this.historyList.length;i++){
              const testDescription=this.historyList[i].description.split("\"subject-cell\">");
              const testDescription2= testDescription[1].split("</span>");
              this.historyDescriptionToolTip.push(testDescription2[0]);
              this.toggleComment.push(false);
            }
        })
    });
  }

  ngOnDestroy(){
    if(this.subscription != null){
      this.subscription.unsubscribe();
    }
  }
  toggleBoolean(i :number) {
    this.toggleComment[i] = !this.toggleComment[i];
  }
  toggleDiv() {
      this.toggleOpen = !this.toggleOpen;
  }
}
