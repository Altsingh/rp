import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, RequestMethod } from '@angular/http';
import { SessionService } from '../../session.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class AutoMatchCandidatesService {


  jobSummaryData: any;
  requisitionId: number;
  requisitionCode: string;
  private socialServiceUrl: string = environment.socialMatchApiUrl;
  private jdcvServiceUrl: string = environment.jdCvApiUrl;

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) { }

  getTaggedCandidateIds(requestJSON): Promise<any> {
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/taggedCandidatesIds/', requestJSON).map((response) => {
      this.sessionService.check401Response(response.json());
      return response.json();
    }).toPromise();
  }

  getSocialCandidateIds(requestJSON): Promise<any> {
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/socialCandidatesIds/', requestJSON).map((response) => {
      this.sessionService.check401Response(response.json());
      return response.json();
    }).toPromise();
  }

  checkSocialCandidateIds(requestJSON): Promise<any> {
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/checkSocialUid/', requestJSON).map((response) => {
      this.sessionService.check401Response(response.json());
      return response.json();
    }).toPromise();
  }

  getJobDetailsForSocialMatch(requestJSON): Promise<any> {
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobDetail/jobDetailsForSocial/', requestJSON).map((response) => {
      this.sessionService.check401Response(response.json());
      return response.json();
    }).toPromise();
  }

  getSocialCandidates(requestJSON) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");

    let options = new RequestOptions({
      headers: headers,
    });

    return this.http.post(this.socialServiceUrl + '/J2C', requestJSON, options).map((response) => {
      return response.json();
    });
  }

  getJdCvCandidates(body) {
    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.post(this.jdcvServiceUrl, body.toString(), options).map((response) => {
      return response.json();
    });
  }

  getSocialCandidateDetail(candidateId) {

    return this.http.get(this.socialServiceUrl + '/detailed_profile?amoeba_id=' +
      candidateId +
     '&type=1' + '&requisition_id=' + this.requisitionId).map((response) => {
     
      return response.json();
    });
  }

}
