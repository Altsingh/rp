import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CommonService } from '../../common/common.service';
import { JobDetailWiringService } from './job-detail-wiring.service';
import { SessionService } from '../../session.service';

@Component({
  selector: 'alt-job-detail',
  templateUrl: './job-detail.component.html'
})
export class JobDetailComponent implements OnInit {

  taggedCount: number;
  rejectedCount: number;
  totalCount: number;
  showStageCount: boolean;
  countLoader: string = 'nodata';
  featureDataMap : Map<any,any>;

  currentPath = this.router.url;
  constructor(private router: Router,
    private commonService: CommonService,
    private jobDetailWiringService: JobDetailWiringService,
    private sessionService : SessionService
  ) {
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  ngOnInit() {

    this.showStageCount = false;
    this.commonService.showRHS();
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.currentPath = this.router.url;
        }
      });

    this.jobDetailWiringService.getStageNameCLick().subscribe((stageNames) => {
      this.showStageCount = false;
      if (stageNames) {
        if (stageNames.length > 0) {
          this.showStageCount = true;
        }
      }
    });

    this.jobDetailWiringService.getStageWiseTotalCount().subscribe((totalCount) => {
      this.totalCount = totalCount;
    });

    this.jobDetailWiringService.getStageWiseTaggedCount().subscribe((taggedCount) => {
    if(this.sessionService.isUntag) {
      this.totalCount = this.totalCount - 1;
      this.sessionService.isUntag = false;
      this.jobDetailWiringService.getRefreshStageListComponent().next('OK');
    }
      if (this.totalCount - taggedCount >= 0) {
        this.taggedCount = taggedCount;
        this.rejectedCount = this.totalCount - this.taggedCount;
        this.countLoader = 'data';
      } else {
        this.taggedCount = null;
        this.rejectedCount = null;
        this.countLoader = 'nodata';
      }
    });

    this.jobDetailWiringService.getStageWiseRejectedCount().subscribe((rejectedCount) => {
      if(this.sessionService.isUntag) {
        this.totalCount = this.totalCount - 1;
        this.sessionService.isUntag = false;
        this.jobDetailWiringService.getRefreshStageListComponent().next('OK');
      }

      if (this.totalCount - rejectedCount >= 0) {
        this.rejectedCount = rejectedCount;
        this.taggedCount = this.totalCount - this.rejectedCount;
        this.countLoader = 'data';
      } else {
        this.taggedCount = null;
        this.rejectedCount = null;
        this.countLoader = 'nodata';
      }
    });
  }
}
