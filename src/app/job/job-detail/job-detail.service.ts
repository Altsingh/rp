import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { JobSummaryData } from './job-summary/job-summary-data';
import { SessionService } from '../../session.service';
import { BasicInformationData } from './job-details-main/basic-information/basic-information-data';
import { OrganizationDetailData, OrgUnit } from './job-details-main/organization-detail/organization-detail-data';
import { JobDescriptionData } from './job-details-main/job-description/job-description-data';
import { RequiredSkillsData } from './job-details-main/required-skill-set/required-skill-set-data';
import { WorkflowNotificationData } from './job-details-main/workflow-and-notifications/workflow-notification-data';
import { HistoryData } from './history-chats/history-data';
import { CandidateData } from './tagged-candidates/candidate-list/candidate-data';
import { JobStageStatus } from './jobs-stage-list/job-stage-status';
import { JobSummaryComponent } from './job-summary/job-summary.component';
import { CTQInformationData } from './job-details-main/ctq/ctq-information-data';
import { JobSummaryBpComponent } from './job-summary-bp/job-summary-bp.component';

@Injectable()
export class JobDetailService {

  jobSummaryComponent: JobSummaryComponent
  jobSummaryBPComponent: JobSummaryBpComponent

  private baseUrl: string;

  public object: any;
  public objectType: string;
  public requisitionCode: string;
  public jobSummaryData: any;
  public publishToVendorJSON: any;
  public publishToVendorResult: string;
  public requisitionId: number;

  public publishedToSubject: Subject<any> = new Subject<any>();

  private sourcingGuideLineSubject = new Subject<string>();
  public sourcingGuideLine: string;

  constructor(private http: Http, private sessionService: SessionService) {
    this.baseUrl = sessionService.orgUrl + '/rest/altone/jobDetail/';
  }

  getJobSummary(jobCode: String): Observable<JobSummaryData> {

    let jobSummaryData = this.http
      .get(this.baseUrl + 'summary/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapJobSummaryData);
    return jobSummaryData;

  }

  sourcingGuideLineUpdated(sourcingGuideLine: string){
    this.sourcingGuideLine = sourcingGuideLine;
    this.sourcingGuideLineSubject.next(sourcingGuideLine);
  }

  getSourcingGuideLine(): Observable<string>{
    return this.sourcingGuideLineSubject.asObservable();
  }

  getBasicInformation(jobCode: String): Observable<BasicInformationData> {

    let basicInformationData = this.http
      .get(this.baseUrl + 'basicInformation/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapBasicInformationData);
    return basicInformationData;

  }

  getOrganizationDetails(jobCode: String): Observable<OrganizationDetailData> {

    let organizationDetailData = this.http
      .get(this.baseUrl + 'organizationDetails/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapOrganizationDetailData);
    return organizationDetailData;

  }

  getJobDescription(jobCode: String): Observable<JobDescriptionData> {
    let jobDescriptionData = this.http
      .get(this.baseUrl + 'jobDescription/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapJobDescriptionData);

    return jobDescriptionData;
  }

  getRequiredSkills(jobCode: String): Observable<RequiredSkillsData> {
    let requiredSkillsData = this.http
      .get(this.baseUrl + 'requiredSkillSet/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapRequiredSkillsData);

    return requiredSkillsData;
  }

  getWorkflowNotifications(jobCode: String): Observable<WorkflowNotificationData> {
    let workflowNotificationData = this.http
      .get(this.baseUrl + 'workflowNotifications/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapWorkflowNotificationData);

    return workflowNotificationData;
  }

  getCTQInformation(jobCode: String): Observable<CTQInformationData> {
    let ctqData = this.http
      .get(this.baseUrl + 'ctq/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapCTQ);

    return ctqData;
  }


  getJobHistory(jobCode: String): Observable<HistoryData[]> {
    let histories = this.http
      .get(this.baseUrl + 'history/', { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapJobHistory);

    return histories;
  }

  getTaggedCandidates(jobCode: String): Observable<CandidateData[]> {
    let histories = this.http
      .get(this.baseUrl + 'taggedCandidates/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapTaggedCandidates);

    return histories;
  }

  getRejectedCandidates(jobCode: String): Observable<CandidateData[]> {
    let histories = this.http
      .get(this.baseUrl + 'rejectedCandidates/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapRejectedCandidates);

    return histories;
  }

  getJobStageStatusList(jobCode: String): Observable<JobStageStatus[]> {
    let histories = this.http
      .get(this.baseUrl + 'jobStageStatusList/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(this.mapJobStageStatus);

    return histories;
  }

  getInterviewPanelList(jobCode: String) {
    let panelList = this.http
      .get(this.baseUrl + 'interViewPanel/' + this.sessionService.organizationID, { params: { 'jobCode': jobCode }, headers: this.getHeaders() })
      .map(response => response.json());

    return panelList;
  }

  getAssignedVendors(requisitionId) {
    let vendorList = this.http
      .get(this.baseUrl + 'assignedVendors/' + requisitionId, { params: {}, headers: this.getHeaders() })
      .map(response => response.json());

    return vendorList;
  }

  getJobAssignedVendors(requisitionId) {
    let vendorList = this.http
      .get(this.baseUrl + 'assignedVendors2/' + requisitionId, { params: {}, headers: this.getHeaders() })
      .map(response => response.json());

    return vendorList;
  }

  checkMonsterAccess() {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobboard/checkmonsteraccess').
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });

  }
  getAutoMatchSecurity() {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/formSecurity/automatchSecurity/', null, options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  mapJobStageStatus(response: Response): JobStageStatus[] {

    let json = response.json();

    let jobStages = [];

    if (json.responseData.length > 0) {
      for (let ix = 0; ix < json.responseData[0].stageStatusList.length; ix++) {

        let stage = json.responseData[0].stageStatusList[ix];

        let stageStatus = <JobStageStatus>({
          stageName: stage.stageName,
          count: stage.count
        });

        jobStages.push(stageStatus);
      }
    }

    return jobStages;
  }

  mapRejectedCandidates(response: Response): CandidateData[] {

    let json = response.json();

    let candidates = [];

    if (json.responseData.length > 0) {
      for (let ix = 0; ix < json.responseData[0].rejectedCandidates.length; ix++) {

        let candidate = json.responseData[0].rejectedCandidates[ix];

        let candidateData = <CandidateData>({
          candidateId: candidate.candidateId,
          firstName: candidate.firstName,
          middleName: candidate.middleName,
          lastName: candidate.lastName,
          candidateCode: candidate.candidateCode,
          location: candidate.location,
          requisitionId: candidate.requisitionId,
          requisitionCode: candidate.requisitionCode,
          designation: candidate.designation,
          stageStatus: candidate.stageStatus,
          stageName: candidate.stageName,
          sourceType: candidate.sourceType,
          comment: candidate.comment,
          applicantStatusID: candidate.applicantStatusID
        });

        candidates.push(candidateData);
      }
    }
    return candidates;
  }

  mapTaggedCandidates(response: Response): CandidateData[] {

    let json = response.json();

    let candidates = [];
    if (json.responseData.length > 0) {
      for (let ix = 0; ix < json.responseData[0].taggedCandidates.length; ix++) {

        let candidate = json.responseData[0].taggedCandidates[ix];

        let candidateData = <CandidateData>({
          candidateId: candidate.candidateId,
          firstName: candidate.firstName,
          middleName: candidate.middleName,
          lastName: candidate.lastName,
          candidateCode: candidate.candidateCode,
          location: candidate.location,
          requisitionId: candidate.requisitionId,
          requisitionCode: candidate.requisitionCode,
          designation: candidate.designation,
          stageStatus: candidate.stageStatus,
          stageName: candidate.stageName,
          sourceType: candidate.sourceType,
          applicantStatusID: candidate.applicantStatusID
        });

        candidates.push(candidateData);
      }
    }
    return candidates;
  }

  mapJobHistory(response: Response): HistoryData[] {

    let json = response.json();

    let histories = [];
    if (json.responseData.length > 0) {
      histories.push(json.responseData[0].requisitionApprovalHistory);
      for (let ix = 0; ix < json.responseData[0].historyList.length; ix++) {

        let history = json.responseData[0].historyList[ix];

        let historyData = <HistoryData>({
          actorName: history.actorName,
          description: history.eventDescription.replace('http://static.talentpact.com/talentpact/images/recruit/history/plus.png', ''),
        });

        //code to add class when there is no comment
        try {
          let markup = historyData.description;
          let parser = new DOMParser()
          let el = parser.parseFromString(markup, "text/html");
          let elem = el.getElementsByClassName("chats-job-rejected");

          if (elem && elem.length > 0) {
            if (elem[0].children && elem[0].textContent && elem[0].textContent.trim().length == 0) {
              elem[0].parentElement.classList.add("no-comment");
              historyData.description = el.body.innerHTML;
            }
          }  
        } catch (error) {
          console.log(error);
        }

        histories.push(historyData);
      }
    }
    return histories;
  }

  mapCTQ(response: Response): CTQInformationData {

    let json = response.json();
    return json.responseData[0].ctqs;
  }

  mapWorkflowNotificationData(response: Response): WorkflowNotificationData {

    let json = response.json();
    let requiredSkillsData = <WorkflowNotificationData>({
      workflowType: json.responseData[0].workflowType
    });

    return requiredSkillsData;
  }

  mapRequiredSkillsData(response: Response): RequiredSkillsData {

    let json = response.json();
    let requiredSkillsData = <RequiredSkillsData>({
      requiredSkills: json.responseData[0].requiredSkills,
      requiredCertifications: json.responseData[0].requiredCertifications,
      requiredLanguages: json.responseData[0].requiredLanguages,
      requiredQualifications: json.responseData[0].requiredMinQualification
    });

    return requiredSkillsData;
  }

  mapJobDescriptionData(response: Response): JobDescriptionData {
    let json = response.json();
    let jobDescriptionData = <JobDescriptionData>({
      jobDescriptionHTML: json.responseData[0].jobDescription,
    });

    return jobDescriptionData;
  }

  mapJobSummaryData(response: Response): JobSummaryData {
    let json = response.json();
    let jobSummaryData = <JobSummaryData>({
      jobName: json.responseData[0].jobTitle,
      jobCode: json.responseData[0].jobCode,
      summaryOrgUnit: json.responseData[0].orgUnitCode,
      summaryWorkSite: json.responseData[0].workSite,
      postedOn: json.responseData[0].startDate,
      targetOn: json.responseData[0].closingDate,
      jobStatus: json.responseData[0].jobStatus,
      teamSize: json.responseData[0].teamSize,
      publishedTo: json.responseData[0].publishedTo,
      numberOfOpenings: json.responseData[0].numberOfOpenings,
      requisitionId: json.responseData[0].requisitionId,
      jobStatusLabel: json.responseData[0].jobStatusLabel,
      sourcingGuidling: json.responseData[0].sourcingGuidling,
      minExperience : json.responseData[0].minExperience,
      maxExperience : json.responseData[0].maxExperience
    });

    return jobSummaryData;
  }

  mapBasicInformationData(response: Response): BasicInformationData {
    let json = response.json();
    let basicInformationData = <BasicInformationData>({
      requisitionID: json.responseData[0].requisitionID,
      requisitionType: json.responseData[0].requisitionType,
      numberOfOpenings: json.responseData[0].numberOfOpenings,
      requisitionStartingDate: json.responseData[0].requisitionStartingDate,
      requisitionClosingDate: json.responseData[0].requisitionClosingDate,
      requiredWorkExperience: json.responseData[0].requiredWorkExperience,
      ctcRange: json.responseData[0].ctcRange,
      ctcBudgetRange: json.responseData[0].ctcBudgetRange,
      grade: json.responseData[0].grade,
      hiringManager: json.responseData[0].hiringManager,
      designation: json.responseData[0].designation,
      industry: json.responseData[0].industry,
      createdBy: json.responseData[0].createdBy,
      employeeCategoryName: json.responseData[0].employeeCategoryName,
      jobFamilyName: json.responseData[0].jobFamilyName,
      positionName: json.responseData[0].positionName,
      campusTypeName: json.responseData[0].campusTypeName,
      programTypeName: json.responseData[0].programTypeName,
      roleTypeName: json.responseData[0].roleTypeName,
      roleContributionName: json.responseData[0].roleContributionName,
      employmentTenureName: json.responseData[0].employmentTenureName,
      resourceTypeName: json.responseData[0].resourceTypeName,
      employmentTypeName: json.responseData[0].employmentTypeName,
      confidential: json.responseData[0].confidential,
      tat: json.responseData[0].tat,
      bandId: json.responseData[0].bandId,
      band: json.responseData[0].band,
      employeeCode: json.responseData[0].employeeCode
    });

    return basicInformationData;
  }

  mapOrganizationDetailData(response: Response): OrganizationDetailData {
    let json = response.json();
    let organizationDetailData = <OrganizationDetailData>({
      orgUnitHierarchy: json.responseData[0].orgUnitHierarchy,
      workSiteHierarchy: json.responseData[0].workSiteHierarchy,
      functionalArea: json.responseData[0].functionalArea,
      industry: json.responseData[0].industry,
      role: json.responseData[0].role
    });

    return organizationDetailData;
  }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getRequisitionIDByCode(requisitionCode: String) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    let request = new GetRequisitionIDByCodeRequest();
    let requestInput = new GetRequisitionIDByCodeRequestInput();
    requestInput.requisitionCode = requisitionCode;
    request.input = requestInput;
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/requisitionIDByCode/' + this.sessionService.organizationID + "/", JSON.stringify(request), options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getJobDetailsForSoureAndTeam(requisitionID: number) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobDetail/performanceSourceTeam/' + requisitionID + "/", options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  removeRecruiter(requisitionRecruiterID) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobDetail/deleteRecruiter/' + this.sessionService.organizationID + "/" + requisitionRecruiterID + "/", options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  removeInterviewPanel(userId, requisitionId) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobDetail/deleteInterviewPanel/' + this.sessionService.organizationID + "/" + userId + "/" + requisitionId + "/", options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
  removeAssignedVendor(vendorId, requisitionId) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobDetail/deleteVendor/' + requisitionId + "/" + vendorId + "/", options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
  isJobAssigned(jobCode) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobDetail/isJobAssigned/?jobCode=' + jobCode, options).
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getJobBoardStatus(requisitionID: number) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobboard/status/' + requisitionID + "/").
      map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  public setJobSummaryComponent(jobSummaryComponent: JobSummaryComponent) {
    this.jobSummaryComponent = jobSummaryComponent;
  }

  public getJobSummaryComponent(): JobSummaryComponent {
    return this.jobSummaryComponent;
  }

  public setJobSummaryBPComponent(jobSummaryBPComponent: JobSummaryBpComponent) {
    this.jobSummaryBPComponent = jobSummaryBPComponent;
  }

  public getJobSummaryBPComponent(): JobSummaryBpComponent {
    return this.jobSummaryBPComponent;
  }

}

export class GetRequisitionIDByCodeRequest {
  input: GetRequisitionIDByCodeRequestInput;
}

export class GetRequisitionIDByCodeRequestInput {
  requisitionCode: String;
}
