import { Injectable } from '@angular/core';
import { SessionService } from '../../session.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class AutoMatchSocialPagedService {

  socialMatchApiUrl = environment.socialMatchApiUrl;
  pagedDataUrl = this.socialMatchApiUrl + '/J2C';
  protected cachedList: any[][] = [];
  // protected currentPage: any[];
  match_score: number;
  totalRows: number;
  state: any;
  public currentPosition: number;
  private lastRequestJSON: any;

  private filterOptions = new Subject<any>();
  private filterApplied = new Subject<any>();
  private clearFilters = new Subject<any>();

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) {

  }

  handlePageResponse(responseJSON, pageNumber, pageSize, currentPage) {

    if (responseJSON) {
      if (responseJSON.res) {
        if (responseJSON.res.hits) {
          if (responseJSON.res.hits.hits) {
            if (responseJSON.res.hits.hits.length > 0) {

              for (let x = 0; x < responseJSON.res.hits.hits.length; x++) {
                if (x < pageNumber * pageSize) {
                  currentPage.push(responseJSON.res.hits.hits[x]);
                }
                this.cachedList[(pageNumber - 1) * pageSize + x] = [responseJSON.res.hits.hits[x]];
              }
            }
          }
          if (responseJSON.res.hits.total) {
            this.totalRows = responseJSON.res.hits.total;
          }
        }
      }

      if (responseJSON.res.aggregations) {
        this.filterOptions.next(responseJSON.res.aggregations);
      }
    }

  }

  getItemByIndex(index: number): Observable<any> {
    if (index > 0 && index <= this.totalRows) {

      index--;

      let currentItem = [];

      if (this.cachedList[index] != null) {

        return Observable.of(this.cachedList[index][0]);
      } else {

        let pageNumber = Math.ceil(index / 15);
        let pageSize = 15;

        return this.getPage(this.lastRequestJSON, pageNumber, pageSize).map((response) => {
          if (response instanceof Array) {
            return response[index - ((pageNumber - 1) * pageSize)];
          }
        });
      }
    }
  }


  getPage(requestJSON, pageNumber, pageSize): Observable<any> {

    this.lastRequestJSON = requestJSON;
    requestJSON.J2C.size = pageSize;

    requestJSON.J2C.from = (pageNumber - 1) * pageSize;

    let currentPage = [];


    let startIndex = (pageNumber - 1) * pageSize;
    let endIndex = (pageNumber) * pageSize;

    if (this.totalRows != null) {
      if (endIndex > this.totalRows) {
        endIndex = this.totalRows;

      }
    }

    let fetchStart = 99999999999999999999999999;
    let fetchEnd = 0;

    for (let i = startIndex; i < endIndex; i++) {
      if (this.cachedList[i] != null) {
        currentPage.push(this.cachedList[i][0]);
      } else {
        if (i < fetchStart) {
          fetchStart = i;
        }
        if (i > fetchEnd) {
          fetchEnd = i;
        }
      }
    }
    // if (pageNumber == 1) {
    //   requestJSON.J2C.size = 200
    // }
    if (fetchStart != 99999999999999999999999999
      && fetchEnd != 0) {

      currentPage = [];

      let options = this.getRequestOptions();

      return this.http.post(this.pagedDataUrl, requestJSON, options)
        .map((response) => {

          let responseJSON = response.json();

          this.handlePageResponse(responseJSON, pageNumber, pageSize, currentPage);
          return currentPage;
        });
    }


    return Observable.of(currentPage);

  }

  savePageState(state) {
    this.state = state;
  }

  getPageState() {
    return this.state;
  }



  getHeaders(): Headers {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return headers;
  }

  removeCachedItem(pageNumber: number, pageSize: number, index: number) {
    let removable = (pageNumber - 1) * pageSize + index;
    this.cachedList.splice(removable, 1);
    this.totalRows--;
  }

  syncCandidate(pageNumber: number, pageSize: number, index: number, newcandidate) {
    let updatable = (pageNumber - 1) * pageSize + index;
    this.cachedList[updatable][0] = newcandidate;
  }


  getRequestOptions(): RequestOptions {
    let headers = this.getHeaders();

    let options = new RequestOptions({
      headers: headers
    });

    return options;
  }

  getTotalCandidates() {
    return this.totalRows;
  }

  clearCache() {
    this.cachedList = [];
    this.totalRows = null;
  }

  clearLastRequestJSON() {
    this.lastRequestJSON = null;
  }

  getFilterOptions(): Subject<any> {
    return this.filterOptions;
  }

  getFilterApplied(): Subject<any> {
    return this.filterApplied;
  }

  getClearFilters(): Subject<any> {
    return this.clearFilters;
  }

  getLastRequestJSON() {
    return this.lastRequestJSON;
  }


  getFilterSuggestion(filterType: string, queryString: string): Observable<any> {
    return this.http.get(this.socialMatchApiUrl + '/get_autosuggestion?doc_type=' + filterType + '&query_string=' + queryString).map((response) => {
      return response.json();
    });
  }

}
