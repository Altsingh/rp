import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobStageStatus } from './job-stage-status';
import { ActivatedRoute, Router } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { JobDetailService } from '../job-detail.service';
import { Subscription } from 'rxjs/Subscription'
import { JobDetailWiringService } from '../job-detail-wiring.service';

@Component({
  selector: 'alt-jobs-stage-list',
  templateUrl: './jobs-stage-list.component.html',
  styleUrls: ['./jobs-stage-list.component.css']
})


export class JobsStageListComponent implements OnInit, OnDestroy {

  jobStageList: JobStageStatus[];
  jobDetailService: JobDetailService
  sub: any;
  jobCode: String;
  subscription: Subscription;
  stageNames: string[] = [];
  totalCount: number;

  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe,
    jobDetailService: JobDetailService, private jobDetailWiringService: JobDetailWiringService,
    private router: Router) {
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }

  setLoading() {
    var loading = new JobStageStatus();
    loading.stageName = "Loading...";
    this.jobStageList = [
      loading
    ];
  }


  ngOnInit() {

    this.totalCount = 0;
    this.sub = this.route.params.subscribe((params) => {

      this.setLoading();

      this.subscription = this.jobDetailService
            .getJobStageStatusList(this.parser.transform(params['id']))
            .subscribe(p => this.jobStageList = p)
            
      this.jobDetailWiringService.getRefreshStageListComponent().subscribe((input) => {
          this.subscription = this.jobDetailService
            .getJobStageStatusList(this.parser.transform(params['id']))
            .subscribe(p => this.jobStageList = p)
        });
      });

    this.jobDetailWiringService.getRefreshSelectedStageNames().subscribe((trigger) => {
      this.jobDetailWiringService.getStageNameCLick().next(this.stageNames);
    });
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }
  getFilteredCandidate(jobStage: JobStageStatus) {
    if ((location.href.indexOf('tagged-candidates') < 0) && (location.href.indexOf('rejected-candidates')) < 0) {
      this.router.navigate(['./tagged-candidates'], { relativeTo: this.route });
    }
    if (jobStage.stageName == "Openings") {
      return;
    }
    if (this.stageNames.indexOf(jobStage.stageName) > -1) {
      this.stageNames.splice(this.stageNames.indexOf(jobStage.stageName), 1);
      jobStage.isSelected = false;
      this.totalCount -= jobStage.count;
    } else {
      this.stageNames.push(jobStage.stageName);
      jobStage.isSelected = true;
      this.totalCount += jobStage.count;
    }

    this.jobDetailWiringService.stageNames = this.stageNames;
    this.jobDetailWiringService.getStageNameCLick().next(this.stageNames);
    this.jobDetailWiringService.getStageWiseTotalCount().next(this.totalCount);
  }
}
