export class JobStageStatus {
    public stageName: string;
    public count: number;
    public isSelected: boolean = false;
}