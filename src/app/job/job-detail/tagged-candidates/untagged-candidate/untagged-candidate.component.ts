import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MessageCode } from '../../../../common/message-code';
import { JobActionService } from '../../../job-action/job-action.service';
import { TaggedCandidateService } from '../../../../candidate/candidate-list/taggedCandidates/tagged-candidate.service';
import { NotificationService } from "../../../../common/notification-bar/notification.service";

@Component({
  selector: 'alt-untagged-candidate',
  templateUrl: './untagged-candidate.component.html',
  styleUrls: ['./untagged-candidate.component.css'],
  providers: [TaggedCandidateService]
})
export class UntaggedCandidateComponent implements OnInit {
  lable:string="Untag";
  untagged:boolean=false;
  candidatelist: any[]= []; ;
  list: any[] = [];
  unTagCandidatelist: any[]= [];
  subscription;
  datalist: ResponseData;
  messageCode: MessageCode;
  constructor(public dialogRef: MatDialogRef<UntaggedCandidateComponent>,
    private jobActionService: JobActionService,
    private taggedCandidateService: TaggedCandidateService,
    private notificationService: NotificationService) {
    this.candidatelist = [];
    this.unTagCandidatelist = [];
    this.list=[];
    this.datalist = new ResponseData();
  }

  ngOnInit() {
    this.candidatelist = this.jobActionService.getDataList();
    for (let id of this.candidatelist) {
      if(id.untagCandidateEnabled)
        this.list.push(id);
      else
        this.unTagCandidatelist.push(id);
    }

  }
  untagCanidate() {
    this.untagged=true;
    let list: any[] = [];
    for (let id of this.list) {
      if(id.untagCandidateEnabled)
        list.push(id.applicantStatusID);
    }
    this.subscription = this.taggedCandidateService.unTagApplicantBulkFromJob(list).subscribe(res => {
      this.datalist.result = res.responseData[0],
        this.messageCode = res.messageCode;
      if (this.datalist.result == true) {
        this.dialogRef.close();
        this.jobActionService.isUntag = true;
        this.notificationService.setMessage("success", "Candidates untagged successfully!");
        this.jobActionService.getcandidateComponent().clearCache();
        this.jobActionService.getcandidateComponent().ngOnInit();
      }
    });
    this.candidatelist = [];
  }
}

export class candidateListResponse {
  firstName: Array<string>;
  applicantStatusID: Array<number>;
}
export class ResponseData {
  result: boolean;
}