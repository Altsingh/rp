import { Component, OnInit, Input } from '@angular/core';
import { CandidateData} from '../candidate-list/candidate-data'

@Component({
  selector: 'alt-candidate-card',
  templateUrl: './candidate-card.component.html',
  styleUrls: ['./candidate-card.component.css'],
  providers:[CandidateData]
})
export class CandidateCardComponent implements OnInit {
  
  @Input()
  public taggedCandidate:CandidateData;
  
  constructor() {
    
   }

  ngOnInit() {
  }

}
