import { Component, OnInit } from '@angular/core';
import { EmailTemplateService } from "../../../../candidate/email-template/email-template.service";
import { SessionService } from "../../../../session.service";
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { ValidationService } from "../../../../shared/custom-validation/validation.service";
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TaggedCandidateService } from '../../../../candidate/candidate-list/taggedCandidates/tagged-candidate.service';
import { JobActionService } from '../../../job-action/job-action.service';

@Component({
  selector: 'alt-notify-recruiter',
  templateUrl: './notify-recruiter.component.html',
  styleUrls: ['./notify-recruiter.component.css'],
  providers:[TaggedCandidateService,EmailTemplateService]
})
export class NotifyRecruiterComponent implements OnInit {
  lable:string="send";
  notifyRecruiter:boolean=false;
  emailInfo: FormGroup;
  primaryEmail: String[] = [];
  data: EmailData;
  messageCode: MessageCode;
  responseData: ResponseData;
  candidatelist: candidateData[];
  constructor(public dialogRef: MatDialogRef<NotifyRecruiterComponent>, private emailTemplateService: EmailTemplateService,
    private sessionService: SessionService, private formBuilder: FormBuilder,
    private notificationService: NotificationService, private taggedCandidateService: TaggedCandidateService,
    private jobActionService: JobActionService) {
    this.data = new EmailData();
    this.responseData = new ResponseData();
  }

  ngOnInit() {
    this.emailInfo = this.formBuilder.group({
      toEmail: ['', [ValidationService.emailValidator]],
      ccEmail: ['', [ValidationService.emailValidator]],
      subject: '',
      content: ''
    });
    this.candidatelist = this.jobActionService.getDataList();
    for (let i of this.candidatelist) {
      if (i.primaryEmail) {
        this.primaryEmail.push(i.primaryEmail);
      }
    }
  }
  sendEmail() {
    this.notifyRecruiter=true;
    this.notificationService.clear();
    this.data.sentTo = this.emailInfo.controls.toEmail.value;
    this.data.cc = this.emailInfo.controls.ccEmail.value;
    this.data.subject = this.emailInfo.controls.subject.value;
    this.data.content = this.emailInfo.controls.content.value;
    this.emailTemplateService.sendEmail(this.data).subscribe(out => {
      this.messageCode = out.messageCode;
      this.responseData.result = out.responseData[0];
      if (this.responseData.result == "success") {
        this.dialogRef.close();
        this.jobActionService.getcandidateComponent().ngOnInit();
        this.notificationService.setMessage("INFO", "Email sent successfully!");
      }
    });
  }
}
export class EmailData {
  sentTo: String[];
  cc: String;
  subject: String;
  content: String;
  mailFrom: String;
  organizationId: Number;
  tenantId: Number;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  result: String;
}
export class candidateData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
}