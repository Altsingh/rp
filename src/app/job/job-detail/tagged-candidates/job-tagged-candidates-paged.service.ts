import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { PagedDataService } from '../../../common/paged-data.service';
import { SessionService } from '../../../session.service';

@Injectable()
export class JobTaggedCandidatesPagedService extends PagedDataService {

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/jobDetail/taggedCandidates/',
      '/rest/altone/jobDetail/taggedCandidates/totalRows/'
    );
  }

  setJobCode(jobCode) {
    if (jobCode != null) {
      super.initialize(
        '/rest/altone/jobDetail/taggedCandidates/?jobCode=' + jobCode ,
        '/rest/altone/jobDetail/taggedCandidates/totalRows/?jobCode=' + jobCode
      );
    }

  }

  public updateProfileMatchPercenatage( applicationStatusId: Number ) {
    
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/updateProfileMatchPercentage/' + applicationStatusId + "/", "", options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    
    }
}
