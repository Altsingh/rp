import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { TaggedCandidateService } from '../../../../candidate/candidate-list/taggedCandidates/tagged-candidate.service';
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { SessionService } from '../../../../session.service';
import { DialogService } from '../../../../shared/dialog.service';
import { JobActionService } from '../../../job-action/job-action.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-copy-to-another-job',
  templateUrl: './copy-to-another-job.component.html',
  styleUrls: ['./copy-to-another-job.component.css'],
  providers: [TaggedCandidateService]
})
export class CopyToAnotherJobComponent implements OnInit {
  label: string;
  moveLabel: string;
  copyLabel: string;
  data: ResponseData[];
  jobList: any[];
  filteredJobList: any[];
  candidatelist: candidateData[];
  messageCode: MessageCode;
  candidateInfo: FormGroup;
  timer: any;
  severity: String;
  severityClass: string;
  message: string;
  subscription;
  candidateNewJobInfo: TransferCandidateToAnotherJobData;
  copyCandidateRespone: ResponseData;
  transferCandidate: boolean = false;
  selected: any;
  actionList: Actions[] = [];
  constructor(public dialogRef: MatDialogRef<CopyToAnotherJobComponent>,
    private jobActionService: JobActionService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private sessionService: SessionService, private taggedCandidateService: TaggedCandidateService,
    private dialogService: DialogService, private i18UtilService: I18UtilService) {

    this.data = [];
    this.copyCandidateRespone = new ResponseData();
    this.message = "";
    this.i18UtilService.get('common.label.move').subscribe((res: string) => {
      this.moveLabel = res;
    });
    this.i18UtilService.get('common.label.copy').subscribe((res: string) => {
      this.copyLabel = res;
    });
  }
  ngOnInit() {
    this.initializeCandidateInfo();
    this.initActionList();
    this.candidatelist = this.jobActionService.getDataList();
    let res = this.dialogService.getDialogData();
    this.messageCode = res.messageCode;
    this.data = res.responseData;
    this.filterJobList();
    this.i18UtilService.get('common.label.transfer').subscribe((res: string) => {
      this.label = res;
    });
  }
  public copyCandidateToAnotherJob(event) {
    if (!this.validateCandidateTransfer()) {
      return;
    }
    let transferCandidateData: TransferCandidateToAnotherJobData[] = [];
    for (let candidate of this.candidatelist) {
      this.candidateNewJobInfo = new TransferCandidateToAnotherJobData();
      this.candidateNewJobInfo.transferToRequisitionId = this.candidateInfo.controls.jobIdSelected.value;
      this.candidateNewJobInfo.transferToStage = this.candidateInfo.controls.stageSelected.value;
      this.candidateNewJobInfo.transferActionType = this.label;
      this.candidateNewJobInfo.userID = this.sessionService.userID;
      this.candidateNewJobInfo.applicantStatusId = candidate.applicantStatusID;
      this.candidateNewJobInfo.requisitionID = candidate.requisitionId;
      this.candidateNewJobInfo.candidateID = candidate.candidateId;
      transferCandidateData.push(this.candidateNewJobInfo);
    }
    this.transferCandidate = true;
    this.taggedCandidateService.transferCandidateToAnotherJobBulk(transferCandidateData).subscribe(out => {
      this.copyCandidateRespone.result = out.responseData[0];
      this.messageCode = out.messageCode;
      if (this.copyCandidateRespone.result == "success") {
        this.dialogRef.close();
        this.jobActionService.getcandidateComponent().clearCache();
        this.jobActionService.getcandidateComponent().ngOnInit();
        this.i18UtilService.get('candidate.success.candidateTransferred').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
      } else {
        this.transferCandidate = false;
        this.setMessage("WARNING", this.copyCandidateRespone.result);
      }
    });
  }

  public setMessage(severity: string, message: string, args?: any) {
    if (args) {
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
        this.timer = setTimeout(() => {
          this.clearMessages();
          this.timer = null;
        }, 5000);
      });

    } else {
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.message = res;
        switch (severity) {
          case NotificationSeverity.INFO:
            this.severityClass = "successful";
            break;
          case NotificationSeverity.WARNING:
            this.severityClass = "warning";
            break;
          default:
            this.severityClass = "";
            break;
        }
        this.timer = setTimeout(() => {
          this.clearMessages();
          this.timer = null;
        }, 5000);
      });

    }
  }
  clearMessages() {
    this.severity = "";
    this.message = "";
  }
  bindAction(event) {
    this.i18UtilService.get('common.label.' + event.label).subscribe((res: string) => {
      this.label = res;
    });
  }
  bindWorkflowID(event) {
    this.selected = event;
  }
  initActionList() {
    this.actionList.push({
      value: 1,
      label: this.copyLabel,
    });
    this.actionList.push({
      value: 2,
      label: this.moveLabel,
    });
  }
  initializeCandidateInfo() {
    this.candidateInfo = this.formBuilder.group({
      jobIdSelected: [],
      actionSelected: '',
      stageSelected: ''
    });
  }
  validateCandidateTransfer() {
    let valid: boolean = true;
    this.notificationService.clear();
    if (this.candidateInfo.controls.stageSelected.value == null || this.candidateInfo.controls.stageSelected.value == '') {
      this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.selectStage');
      valid = false;
    }
    if (this.candidateInfo.controls.jobIdSelected.value == null) {
      this.setMessage(NotificationSeverity.WARNING, 'job.warning.selectJob');
      valid = false;
    }
    if (this.candidateInfo.controls.actionSelected.value == null || this.candidateInfo.controls.actionSelected.value == '') {
      this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.selectAction');
      valid = false;
    }
    for (let candidate of this.candidatelist) {
      let candidateName: String = "";
      if (!candidate.allowCopyMove) {
        candidateName = this.fullName(candidate, candidateName);
        this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.candidateNotTransferalbe', {'candidateName': candidateName});
        valid = false;
        break;
      }
      if (this.candidateInfo.controls.stageSelected.value == "current" && candidate.workflowID != this.selected.description) {
        candidateName = this.fullName(candidate, candidateName);
        this.setMessage(NotificationSeverity.WARNING, 'candidate.warning.differentWorkflow', {'candidateName': candidateName});
        valid = false;
        break;
      }
    }
    return valid;
  }

  private fullName(candidate: candidateData, candidateName: String) {
    if (candidate.firstName) {
      candidateName = candidate.firstName;
    }
    if (candidate.middleName) {
      candidateName = candidateName + ' ' + candidate.middleName;
    }
    if (candidate.lastName) {
      candidateName = candidateName + ' ' + candidate.lastName;
    }
    return candidateName;
  }
  syncJobList(flag: boolean) {
    if (flag) {
      this.data = this.filteredJobList;
    } else {
      this.data = this.jobList;
    }
  }
  filterJobList() {
    this.jobList = this.data.slice();
    this.filteredJobList = this.data.slice();
    for (let item of this.filteredJobList) {
      for (let candidate of this.candidatelist) {
        if (item.description != candidate.workflowID) {
          let index = this.filteredJobList.indexOf(item);
          if (index > -1)
            this.filteredJobList.splice(index, 1);
        }
      }
    }
  }
}
export class ResponseData {
  jobCode: String;
  jobTitle: String;
  requisitionID: number;
  result: string;
}
export class candidateData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
  workflowID: Number;
}
export interface MessageCode {
  code: String;
  message: String;
  description: String;
}
export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}
export class TransferCandidateToAnotherJobData {
  requisitionID: Number;
  candidateID: Number;
  userID: Number;
  applicantStatusId: Number;
  transferToRequisitionId: Number;
  transferToStage: String;
  transferActionType: String;
}
export class Actions {
  value: Number;
  label: String;
}