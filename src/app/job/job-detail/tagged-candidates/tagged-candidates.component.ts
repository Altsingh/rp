import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../common/common.service';

@Component({
  selector: 'alt-tagged-candidates',
  templateUrl: './tagged-candidates.component.html',
  styleUrls: ['./tagged-candidates.component.css']
})
export class TaggedCandidatesComponent implements OnInit {

  constructor(private commonService: CommonService) { 
    commonService.showRHS();
  }

  ngOnInit() {
  }

}
