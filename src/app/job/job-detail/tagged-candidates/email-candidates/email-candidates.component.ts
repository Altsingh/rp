import { Component, OnInit } from '@angular/core';
import { EmailTemplateService } from "../../../../candidate/email-template/email-template.service";
import { SessionService } from "../../../../session.service";
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { ValidationService } from "../../../../shared/custom-validation/validation.service";
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TaggedCandidateService } from '../../../../candidate/candidate-list/taggedCandidates/tagged-candidate.service';
import { JobActionService } from '../../../job-action/job-action.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-email-candidates',
  templateUrl: './email-candidates.component.html',
  styleUrls: ['./email-candidates.component.css'],
  providers: [EmailTemplateService, TaggedCandidateService]
})
export class EmailCandidatesComponent implements OnInit {
  lable: string;
  emailCandidate: boolean = false;
  emailInfo: FormGroup;
  primaryEmail: String[] = [];
  data: EmailData;
  messageCode: MessageCode;
  responseData: ResponseData;
  candidatelist: candidateData[];

  constructor(public dialogRef: MatDialogRef<EmailCandidatesComponent>, private emailTemplateService: EmailTemplateService,
    private sessionService: SessionService, private formBuilder: FormBuilder,
    private notificationService: NotificationService, private taggedCandidateService: TaggedCandidateService,
    private jobActionService: JobActionService, private i18UtilService: I18UtilService) {
    this.data = new EmailData();
    this.responseData = new ResponseData();
  }

  ngOnInit() {
    this.emailInfo = this.formBuilder.group({
      toEmail: ['', [ValidationService.emailValidator]],
      ccEmail: ['', [ValidationService.emailValidator]],
      subject: '',
      content: ''
    });
    this.candidatelist = this.jobActionService.getDataList();
    if(this.candidatelist){
      for (let i of this.candidatelist) {
        if (i.primaryEmail) {
          this.primaryEmail.push(i.primaryEmail);
        }
      }
    }else{
      this.primaryEmail=[];
       this.primaryEmail.push(this.sessionService.object.primaryEmail);
    }
    this.i18UtilService.get('common.label.send').subscribe((res: string) => {
      this.lable = res;
    });
  }

  sendEmail() {
    this.emailCandidate=true;
    this.notificationService.clear();
    this.data.sentTo = [];
    this.data.sentTo = this.primaryEmail;
    this.data.cc = this.emailInfo.controls.ccEmail.value;
    this.data.subject = this.emailInfo.controls.subject.value;
    this.data.content = this.emailInfo.controls.content.value;
    this.emailTemplateService.sendEmail(this.data).subscribe(out => {
      this.messageCode = out.messageCode;
      this.responseData.result = out.responseData[0];
      if (this.responseData.result == "success") {
        this.dialogRef.close();
        this.i18UtilService.get('candidate.success.emailSent').subscribe((res: string) => {
          this.notificationService.setMessage("INFO", res);
        });        
        if(this.candidatelist){
          this.jobActionService.getcandidateComponent().ngOnInit();
        }
      }
    });
  }
}
export class EmailData {
  sentTo: String[];
  cc: String;
  subject: String;
  content: String;
  mailFrom: String;
  organizationId: Number;
  tenantId: Number;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  result: String;
}
export class candidateData {
  candidateCode: String;
  firstName: String;
  middleName: String;
  lastName: String;
  location: String;
  requisitionCode: String;
  stageStatus: String;
  stageName: String;
  sourceType: String;
  jobTitle: String;
  applicantStatusID: Number;
  workflowStageID: number;
  requisitionId: Number;
  candidateId: Number;
  allowCopyMove: Boolean;
  selected: boolean;
  createdDate: Date;
  modifiedDate: Date;
  sysSourceType: String;
  index: number;
  primaryEmail: String;
}