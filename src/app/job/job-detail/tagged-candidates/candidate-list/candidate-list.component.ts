import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { CandidateFilterTo } from '../../../../candidate/candidate-list/candidate-filter-to';
import { CandidateListService } from "../../../../candidate/candidate-list/candidate-list.service";
import { ResponseData } from '../../../../candidate/candidate-list/taggedCandidates/tagged-candidate-list.component';
import { NotificationService } from "../../../../common/notification-bar/notification.service";
import { DialogService } from '../../../../shared/dialog.service';
import { NameinitialService } from '../../../../shared/nameinitial.service';
import { PageState } from '../../../../shared/paginator/paginator.component';
import { WorkflowStageMapService } from '../../../../shared/services/workflow-stage-map.service';
import { JobActionService } from '../../../job-action/job-action.service';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { JobDetailWiringService } from '../../job-detail-wiring.service';
import { JobDetailService } from '../../job-detail.service';
import { CopyToAnotherJobComponent } from '../copy-to-another-job/copy-to-another-job.component';
import { EmailCandidatesComponent } from '../email-candidates/email-candidates.component';
import { JobTaggedCandidatesPagedService } from '../job-tagged-candidates-paged.service';
import { NotifyRecruiterComponent } from '../notify-recruiter/notify-recruiter.component';
import { UntaggedCandidateComponent } from '../untagged-candidate/untagged-candidate.component';
import { TaggedCandidatesPagedService } from './../../../../candidate/candidate-list/taggedCandidates/tagged-candidates-paged.service';
import { SessionService } from './../../../../session.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.css'],
  providers: [CandidateListService, JobTaggedCandidatesPagedService, TaggedCandidatesPagedService]
})
export class CandidateListComponent implements OnInit, OnDestroy {
  pageState: PageState = new PageState();

  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;

  workflowStageMapSubscription: Subscription;
  workflowStageMap: any;
  wiringSubscription: Subscription;
  taggedCandidates: ResponseData[] = [];
  sub: any;
  jobCode: String;
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";

  selected: any[] = [];
  selectAll: boolean = false;
  currentURL: string;
  pageTitle: string;
  filterHidden: boolean = true;
  filterTO: CandidateFilterTo;
  loaderIndicator: string;
  showloader: boolean = false;
  jobSummaryData: any;
  CardMenu: boolean = true;
  automatchEnabled:boolean;
  requisitionId:number;
  jobStatus:boolean = true;
  featureDataMap:Map<any,any>;


  public toggleMenu() {
    this.CardMenu = !this.CardMenu;
  }
  public toggleMenuOff() {
    this.CardMenu = true;
  }


  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe,
    private notificationService: NotificationService, private dialogsService: DialogService,
    private jobActionService: JobActionService, private nameinitialService: NameinitialService,
    private jobTaggedCandidatesPagedService: JobTaggedCandidatesPagedService, private formBuilder: FormBuilder,
    private jobDetailService: JobDetailService,
    private sessionService: SessionService,
    private candidateListService: CandidateListService,
    private taggedCandidatesPagedService: TaggedCandidatesPagedService,
    private workflowStageMapService: WorkflowStageMapService, private jobDetailWiringService: JobDetailWiringService,
    private i18UtilService: I18UtilService) {
    this.automatchEnabled = this.sessionService.isAutoMatchEnabled();
    this.featureDataMap = this.sessionService.featureDataMap;
    this.workflowStageMapSubscription = this.workflowStageMapService.getMap().subscribe((response) => {
      this.workflowStageMap = response;
      this.totalRowsSubscription = null
    });
    this.randomcolor = this.nameinitialService.randomcolor;
    this.filterTO = new CandidateFilterTo();
    this.jobActionService.setcandidateComponent(this);
  }
  ngOnInit() {

    this.pageState.currentPageNumber = 1;
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.selected = [];
    this.loaderIndicator = "loader";
    this.jobCode = this.route.snapshot.parent.params['id'];
    this.jobCode = this.parser.transform(this.jobCode);
    this.jobDetailService.getRequisitionIDByCode(this.jobCode).subscribe((out) => {
      this.requisitionId = out;
      this.candidateListService.getJobStatusByRequisitionID(this.requisitionId).subscribe(out =>
      {
        if(out.response !=null)
        {
          if(out.response == "CLOSED" || out.response == "ONHOLD" || out.response == "CANCELLED")
          {
            this.jobStatus = false;
          }
        }
      });
    });

    this.jobTaggedCandidatesPagedService.setJobCode(this.jobCode);
    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });

    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {

        let query = this.queryFormGroup.controls.query.value;

        if (query.length >= 3) {

          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          this.clearCache()
          this.loadData();
          this.loadTotalRowOnSearch();
         /*  this.loadTotalRows(); */
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.clearCache()
          this.loadData();
          this.loadTotalRowOnSearch();
         /*  this.loadTotalRows(); */
        }
      }, 200);

    });

    this.filterTO.stageName = this.jobDetailWiringService.stageNames;
    this.loadData();
    this.loadTotalRows();
    this.wiringSubscription = this.jobDetailWiringService.getStageNameCLick().subscribe((stageName) => {
      this.filterTO.stageName = stageName;
      this.pageState.currentPageNumber = 1;
      this.pageState.pageSize = 15;
      this.filterTO.pageNumber = 1;
      this.filterTO.pageSize = this.pageState.pageSize;
      this.selected = [];
      this.loaderIndicator = "loader";
      this.clearCache()
      this.loadData();
      this.loadTotalRows();
    }
    );
    this.jobDetailWiringService.getRefreshSelectedStageNames().next(true);
  }

  ngOnDestroy() {
    if (this.wiringSubscription != null) {
      this.wiringSubscription.unsubscribe();
    }
  }

  updateProfileMatch(item){
    this.showloader = true;
    this.taggedCandidatesPagedService.updateProfileMatchPercenatage(item.applicantStatusID).subscribe( (res) => {
      if( res && res.response ){
        for( let i = 0; i < this.taggedCandidates.length; i++ ){
          if( this.taggedCandidates[i]['applicantStatusID'] == res.response.applicantStatusID ){
            this.taggedCandidates[i]['profileMatch'] = res.response.profileMatch;
            if( res.response.profileMatch == 0 ){
              this.i18UtilService.get('common.error.errorUnknown').subscribe((res: string) => {
                this.notificationService.addMessage("WARNING", res);
              });
            }
          }
        }
      }
      this.showloader = false;
    } );
  }

  clearCache() {
    this.jobTaggedCandidatesPagedService.clearCache();
  }
  toggleFilter() {
    this.filterHidden = !this.filterHidden;
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.city.length;
  }
  onSorted(params: string[]) {
    this.sortBy = params[0];
    this.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;

    this.ngOnInit();
  }
  onFilter(params: any[]) {
    let genderFilter = false;
    let sourceFilter = false;
    let industryFilter = false;
    let locationFilter = false;
    let functionalAreaFilter = false;
    for (let param of params) {
      if (param.name == 'LOCATION') {
        this.filterTO.location = param.value.value;
        locationFilter = true;
      } else if (param.name == 'GENDER') {
        this.filterTO.genderID = param.value.id;
        genderFilter = true;
      } else if (param.name == 'INDUSTRY') {
        this.filterTO.industryID = param.value.id;
        industryFilter = true;
      } else if (param.name == 'FUNCTIONAL_AREA') {
        this.filterTO.functionalAreaID = param.value.id;
        functionalAreaFilter = true;
      } else if (param.name == 'SOURCE') {
        this.filterTO.sourceID = param.value.id;
        sourceFilter = true;
      }

    }
    if (!genderFilter) {
      this.filterTO.genderID = null;
    }
    if (!industryFilter) {
      this.filterTO.industryID = null;
    }
    if (!functionalAreaFilter) {
      this.filterTO.functionalAreaID = null;
    }
    if (!locationFilter) {
      this.filterTO.location = null;
    }
    if (!sourceFilter) {
      this.filterTO.sourceID = null;
    }
    this.ngOnInit();
  }
  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    } else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    this.ngOnInit();
  }
  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();

  }

  allSelectEvent() {
    let index = 0;
    for (let data of this.taggedCandidates) {
      if (!data.inReview) {
        data.selected = this.selectAll;
        index++;
        if (this.selectAll && !this.selected.includes(data)) {
          this.selected.push(data);
        } else if (!this.selectAll) {
          let i = this.selected.indexOf(data);
          this.selected.splice(i, 1);
        }
      }
    }

    this.areAllSelected();
  }


  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;

    this.loadData();
  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.jobTaggedCandidatesPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
     /*  if (this.totalRowsSubscription != null) { */
        this.jobDetailWiringService.getStageWiseTaggedCount().next(response);
     /*  } */
      this.totalRowsSubscription = null
    });
  }

  loadTotalRowOnSearch(){
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.jobTaggedCandidatesPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }

  getJobTitle() {
    let jobTitle = "";
    if (this.jobSummaryData == null) {
      this.jobSummaryData = this.jobDetailService.jobSummaryData;
    }
    if (this.jobSummaryData != null) {
      if (this.jobSummaryData.jobName != null) {
        return this.jobSummaryData.jobName;
      }
    }
    return jobTitle;
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.jobTaggedCandidatesPagedService.getPage(this.filterTO).subscribe((response) => {
      if (this.jobSummaryData == null) {
        this.jobSummaryData = this.jobDetailService.jobSummaryData;
      }
      if (response) {
        this.taggedCandidates = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }


  areAllSelected() {
    if (this.taggedCandidates != null && this.taggedCandidates.length > 0) {
      for (let x = 0; x < this.taggedCandidates.length; x++) {
        if (typeof this.taggedCandidates[x].selected == 'undefined' || (typeof this.taggedCandidates[x].selected == 'boolean' && !this.taggedCandidates[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }


  openBulkAction(actionType) {
    this.notificationService.clear();
    if (this.selected.length == 0) {
      this.i18UtilService.get('candidate.warning.selectCandidate').subscribe((res: string) => {
        this.notificationService.setMessage("WARNING", res);
      });
      return;
    }
    this.jobActionService.setDataList(this.selected);
    if (actionType == 'U') {
      this.dialogsService.open(UntaggedCandidateComponent, { width: '520px' }).subscribe((event)=>{
        this.sessionService.isUntag = true;
      });
    } else if (actionType == 'C') {
      this.showloader = true;
      this.candidateListService.getJobsOfRecruiter().subscribe(out => {
        this.dialogsService.setDialogData(out);
        this.showloader = false;
        this.dialogsService.open(CopyToAnotherJobComponent, { width: '520px' });
      });
    } else if (actionType == 'E') {
      this.dialogsService.open(EmailCandidatesComponent, { width: '935px' });
    } else if (actionType == 'R') {
      this.dialogsService.open(NotifyRecruiterComponent, { width: '935px' });
    }
  }

  getWorkflowProgressBar(workflowID: Number, item: ResponseData) {
    item.stages = this.workflowStageMap[workflowID + ''];
    return true;
  }
  indexOfStage(stage: string, stageList: string[]) {
    return stageList.indexOf(stage);
  }

}
