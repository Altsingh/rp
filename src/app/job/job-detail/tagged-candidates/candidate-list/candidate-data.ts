export class CandidateData{
    candidateId:number;
	firstName:string;
	middleName:string;
	lastName:string;
	candidateCode:string;
	location:string;
	requisitionId:number;
	requisitionCode:string;
	designation:string;
	stageStatus:string;
	stageName:string;
	sourceType:string;
	applicantStatusID:number;
	comment:string;
	selected: boolean;
	profileImage:string;
}