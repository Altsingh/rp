import { Component, OnInit, Input } from '@angular/core';
import { Http } from "@angular/http";
import { NotificationService , NotificationSeverity} from "../../../../../common/notification-bar/notification.service";

@Component({
  selector: 'alt-candidate-context-menu',
  templateUrl: './candidate-context-menu.component.html',
  styleUrls: ['./candidate-context-menu.component.css']
})
export class CandidateContextMenuComponent implements OnInit {
  displayClass: String = "displayNone";
  @Input()
  mobileNumber: string;
  constructor(private http: Http, private notificationService: NotificationService) { }

  ngOnInit() {
  }
  hideContextMenu() {
    this.displayClass = "displayNone";
  }

  showContextMenu() {
    this.displayClass = "displayBlock";
  }

 checkForOrg(){
  let demo = false;
  if(location.href.indexOf('altrecruit.peoplestrong') > 0
  || location.href.indexOf('hiredplus') > 0){
    demo = true;
  }
  return demo;
}
call(){
  this.http.get('https://www.reckrut.com/api/ps/instant-call/RECidds9239ox7Da89faXzasfkfidaplos4fg0fds0pdsfds?mob='+this.mobileNumber).subscribe(
    (resposne) => {
      this.notificationService.setMessage(NotificationSeverity.INFO, 'Call initiated successfully');
      this.hideContextMenu();
  },
    (error) => {
      this.notificationService.setMessage(NotificationSeverity.INFO, 'Call initiated successfully');
      this.hideContextMenu();
  }

  )
}

}
