import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { JobDetailService } from '../../job-detail.service';
import { SessionService } from '../../../../session.service';
import { DialogService } from '../../../../shared/dialog.service';
import { JobActionService } from '../../../../job/job-action/job-action.service';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { PublishToVendorComponent } from '../publish-to-vendor/publish-to-vendor.component';
import { NameinitialService } from '../../../../shared/nameinitial.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-published-vendors-list',
  templateUrl: './published-vendors-list.component.html',
  styleUrls: ['./published-vendors-list.component.css'],
  providers: [DialogService]
})
export class PublishedVendorsListComponent implements OnInit {

  private parser: ParseJobcodeParamPipe = new ParseJobcodeParamPipe();

  vendorsList: any[] = [];
  vendorTypes: any[] = [];
  vendors: any[] = [];
  jobCode: string;
  requisitionId: number

  vendorFormGroup: FormGroup;

  allOtherSelected: boolean = false;
  showOtherVendors: boolean = false;
  @Input() isJobDisabled: boolean;
  otherVendorSelectedCount: number = 0;
  otherVendorTotalCount: number = 0;
  isOtherOpen: boolean = false;

  noResult: boolean = false;

  allVendorsSelected: boolean = false;

  featureDataMap : Map<any,any>;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    protected jobDetailService: JobDetailService,
    private sessionService: SessionService,
    protected DialogService: DialogService,
    public jobActionService: JobActionService,
    private notificationService: NotificationService,
    private nameinitialService: NameinitialService,
    private i18UtilService: I18UtilService
  ) {
    this.featureDataMap = this.sessionService.featureDataMap;
    this.vendorFormGroup = this.formBuilder.group({
      vendorSearch: ''
    });


    this.vendorFormGroup.controls.vendorSearch.valueChanges.subscribe((event) => {

      let search = this.vendorFormGroup.controls.vendorSearch.value;
      if (search != null && search.trim().length > 0) {

        this.noResult = true;

        for (let vendor of this.vendors) {

          let vendorName = <string>vendor.vendorName;

          if (vendorName.toLowerCase().indexOf(search.toLowerCase()) >= 0) {
            vendor.searchDisplay = true;
            this.noResult = false;
          } else {
            vendor.searchDisplay = false;
          }
        }

        for (let vendorType of this.vendorTypes) {
          vendorType.open = true;
        }
        this.isOtherOpen = true;

      } else {
        this.noResult = false;

        for (let vendor of this.vendors) {
          vendor.searchDisplay = true;
        }
      }

    });
  }

  ngOnInit() {

    this.jobCode = this.jobDetailService.requisitionCode;

    if (this.jobCode == null) {
      this.jobCode = this.route.snapshot.parent.params['id'];
    }

    if (this.jobDetailService.objectType == 'REQUISITION_ID') {
      this.requisitionId = this.jobDetailService.object;
      this.getJobAssignedVendors();

    } else {
      this.jobDetailService.getRequisitionIDByCode(this.parser.transform(this.jobCode)).subscribe((out) => {
        this.requisitionId = parseInt(out);
        this.getJobAssignedVendors();
      });
    }

  }


  toggleOpen(vendorType) {
    vendorType.open = !vendorType.open;
  }
  toggleOpenOther() {
    this.isOtherOpen = !this.isOtherOpen;
  }


  getJobAssignedVendors() {

    this.jobDetailService.getJobAssignedVendors(this.requisitionId).subscribe((response) => {
      this.processVendorsResponse(response);

    });

  }

  processVendorsResponse(response: any) {
    if (response) {
      if (response.responseData) {
        if (response.responseData.length > 0) {
          let jobDetailVendorsJSON = response.responseData[0];
          this.vendors = jobDetailVendorsJSON.vendors;
          this.vendorTypes = jobDetailVendorsJSON.vendorTypes;

          let randomcolor = this.nameinitialService.randomcolor;
          let randomCount = 0;


          for (let vendor of this.vendors) {
            vendor.bgColor=randomcolor[randomCount++];
            vendor.searchDisplay = true;
            if (this.isOtherVendor(vendor)) {
              this.showOtherVendors = true;
              this.otherVendorTotalCount++;

            }
          }
          if(this.otherVendorSelectedCount == this.otherVendorTotalCount ){
            this.allOtherSelected = true;
          }

          this.calculateCounts();

          if (this.vendorTypes.length > 0) {
            this.vendorTypes[0].open = true;
          } else {
            this.isOtherOpen = true;
          }
        }
      }
    }
  }

  calculateCounts() {
    for (let vendorType of this.vendorTypes) {
      vendorType.selectedCount = 0;
      vendorType.totalCount = 0;
      vendorType.allSelected = false;
      vendorType.open = false;

      for (let vendor of this.vendors) {

        if (vendor.vendorTypeIds.includes(vendorType.vendorTypeId)) {
          vendorType.totalCount++;
          if (vendor.selected) {
            vendorType.selectedCount++;
          }
        }

      }

      if (vendorType.selectedCount > 0 && vendorType.selectedCount == vendorType.totalCount) {
        vendorType.allSelected = true;
        vendorType.disableSelection = true;
      }
    }

    for (let vendorType of this.vendorTypes) {
      vendorType.open = true;
      break;
    }


  }
  clearAll() {
    for (let vendor of this.vendors) {
      if (!vendor.disabled && !this.isJobDisabled) {
        vendor.selected = false;
      }
    }
    for (let vendorType of this.vendorTypes) {
      vendorType.allSelected = false;
    }
    this.allOtherSelected = false;
  }
  clearAllOther() {
    this.allOtherSelected = false;
    for (let vendor of this.vendors) {
      if (this.isOtherVendor(vendor)) {
        if (!vendor.disabled && !this.isJobDisabled) {
          vendor.selected = false;
        }
      }
    }
    
    this.otherVendorSelectedCount=0;
  }
  clearAllByVendorType(vendorType) {
    vendorType.allSelected = false;
    for (let vendor of this.vendors) {
      if (!this.isOtherVendor(vendor) && vendor.vendorTypeIds.includes(vendorType.vendorTypeId)) {
        if (!vendor.disabled && !this.isJobDisabled) {
          vendor.selected = false;
        }
      }
    }
  }

  clearMessages() {
    this.notificationService.clear();
  }
  setMessage(notificationSeverity: string, message: string, args?: any) {
       if( isNullOrUndefined(args) ){
         this.i18UtilService.get(message).subscribe((res: string) => {
           this.notificationService.addMessage(notificationSeverity, res);
         });   
       }else{
         this.i18UtilService.get(message, args).subscribe((res: string) => {
           this.notificationService.addMessage(notificationSeverity, res);
         });
       }
         
   }

  selectThis(vendor) {
    if (!vendor.disabled && !this.isJobDisabled) {
      vendor.selected = !vendor.selected;
      if(vendor.selected){
        this.otherVendorSelectedCount++;
      }else{
        this.otherVendorSelectedCount--;
      }
       this.allOtherSelected=((this.otherVendorSelectedCount==this.otherVendorTotalCount));
    }else if(vendor.disabled && !this.isJobDisabled){
      let isVendorSelected:boolean=false;
      for (let vendor of this.vendors) {
        if (!vendor.disabled && vendor.selected && !this.isJobDisabled) {
            isVendorSelected=true;
            break;
        }
      }

      if(!isVendorSelected){
      let jobSummaryData = this.jobDetailService.jobSummaryData;
      let input = [];
      let selectedVendorIds = [];
      input.push({
        requisitionId: this.requisitionId,
        vendorId: vendor.vendorId,
        jobCode: this.jobCode,
        assignedOpenings: vendor.assignedOpening,
        assignDate: vendor.assignedDate,
        targetDate: vendor.targetDate,
        disabled: vendor.disabled,
        vendorName: vendor.vendorName,
        vendorIdStrVal: vendor.vendorId + ""
      });
      selectedVendorIds.push(vendor.vendorId);
      let requestJSON = {
        input: input
      }
      this.jobDetailService.publishToVendorJSON = requestJSON;

      this.jobDetailService.publishToVendorResult = "";
      this.DialogService.open(PublishToVendorComponent, { width: '900px' }).subscribe(() => {
        if (this.jobDetailService.publishToVendorResult == "success") {
          this.afterSuccess(requestJSON.input.length);
          this.clearMessages();
          this.setMessage(NotificationSeverity.INFO, "job.success.requisitionPublished")
          for (let vendor of this.vendors) {
            if (selectedVendorIds.includes(vendor.vendorId)) {
              vendor.targetDate=requestJSON.input[0].targetDate;
            }
          }

          // this.calculateCounts();
        }
      });
    }
    }
  }

  publish() {
    let selectedVendors = [];

    let jobSummaryData = this.jobDetailService.jobSummaryData;

    if (jobSummaryData.jobStatus == 'ONHOLD'
      || jobSummaryData.jobStatus == 'PLANNED'
      || jobSummaryData.jobStatus == 'CANCELLED'
      || jobSummaryData.jobStatus == 'CLOSED') {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, 'job.error.jobPublishedFail',  {'status': jobSummaryData.jobStatus});
      return;
    }

    for (let vendor of this.vendors) {
      if (!vendor.disabled && vendor.selected && !this.isJobDisabled) {
        selectedVendors.push(vendor);
      }
    }
    if (selectedVendors.length == 0) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "job.warning.selectVendor");
    } else {

      let input = [];
      let selectedVendorIds = [];

      for (let vendor of selectedVendors) {
        input.push({
          requisitionId: this.requisitionId,
          vendorId: vendor.vendorId,
          jobCode: this.jobCode,
          assignedOpenings: 0,
          targetDate: null,
          vendorIdStrVal: vendor.vendorId + ""
        });

        selectedVendorIds.push(vendor.vendorId);
      }

      let requestJSON = {
        input: input
      }

      this.jobDetailService.publishToVendorJSON = requestJSON;

      this.jobDetailService.publishToVendorResult = "";
      this.DialogService.open(PublishToVendorComponent, { width: '900px' }).subscribe(() => {
        if (this.jobDetailService.publishToVendorResult == "success") {
          this.jobDetailService.publishedToSubject.next(true);
          this.afterSuccess(requestJSON.input.length);
          this.clearMessages();
          this.setMessage(NotificationSeverity.INFO, "job.success.requisitionPublished")
          for (let vendor of this.vendors) {
            if (selectedVendorIds.includes(vendor.vendorId)) {
              vendor.selected = true;
              vendor.disabled = true;
              vendor.assignedOpening=requestJSON.input[0].assignedOpenings;
              vendor.assignedDate=requestJSON.input[0].assignDate;
              vendor.targetDate=requestJSON.input[0].targetDate;
            }
          }

          this.calculateCounts();
          this.otherVendorSelectedCount= 0;
        }
      });
    }
    
  }

  afterSuccess(data) {

  }

  selectAllTheVendors() {
    this.allVendorsSelected = !this.allVendorsSelected;
    for (let vendor of this.vendors) {
      if (!vendor.disabled && !this.isJobDisabled) {
        vendor.selected = this.allVendorsSelected;
        
      }
    }

    for (let vendorType of this.vendorTypes) {
      vendorType.allSelected = this.allVendorsSelected;
      this.vendorTypes
    }
    this.allOtherSelected = this.allVendorsSelected;
  }
  selectAllOtherVendors() {
    this.allOtherSelected = !this.allOtherSelected;
     if(this.allOtherSelected){
       this.otherVendorSelectedCount= 0;
     }

    for (let vendor of this.vendors) {
      if (this.isOtherVendor(vendor)) {
        if (!vendor.disabled && !this.isJobDisabled) {
          vendor.selected = this.allOtherSelected;
          if(this.allOtherSelected){
            this.otherVendorSelectedCount++
          }else{
            this.otherVendorSelectedCount--;
          }
        }
      }
    }
       
  }


  selectAllVendorsByVendorType(vendorType, event) {
    if (vendorType != null) {

      vendorType.allSelected = !vendorType.allSelected;

      for (let vendor of this.vendors) {

        if (vendor.vendorTypeIds.includes(vendorType.vendorTypeId)) {

          if (!vendor.disabled && !this.isJobDisabled) {
            vendor.selected = vendorType.allSelected;
          }
        }
      }
    }
  }

  isOtherVendor(vendor) {
    if (vendor.vendorTypeIds == null || vendor.vendorTypeIds.length == 0) {
      return true;
    }
    for (let vendorType of this.vendorTypes) {
      if (vendor.vendorTypeIds.includes(vendorType.vendorTypeId)) {
        return false;
      }
    }
    return true;
  }


  deleteVendor(vendor) {
    if (vendor != null) {
      if (vendor.vendorId != null) {
        this.jobDetailService.removeAssignedVendor(vendor.vendorId, this.requisitionId).subscribe(response => {

          let deleted = false;

          if (response) {
            if (response.responseData) {
              if (response.responseData.length > 0) {
                if (response.responseData[0] == true) {
                  this.clearMessages();
                  this.setMessage(NotificationSeverity.INFO, "common.success.removed");
                  deleted = true;
                  let temp = [];
                  for (let x = 0; x < this.vendorsList.length; x++) {
                    if (this.vendorsList[x] == vendor) {
                      continue;
                    } else {
                      temp.push(this.vendorsList[x]);
                    }
                  }
                  this.vendorsList = temp;
                }
              }
            }
          }

          if (!deleted) {
            this.clearMessages();
            this.setMessage(NotificationSeverity.WARNING, "job.error.vedorRemoveFailed");
          }
        });

      }
    }
  }

}
