import { Component, OnInit } from '@angular/core';
import { JobActionService } from '../../../job-action/job-action.service';
import { JobDetailService } from '../../../job-detail/job-detail.service';
import { MatDialogRef } from '@angular/material';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'alt-publish-to-vendor',
  templateUrl: './publish-to-vendor.component.html',
  styleUrls: ['./publish-to-vendor.component.css']
})
export class PublishToVendorComponent implements OnInit {

  jobSummaryData: any;
  publishToVendorJSON: any;
  isPublishRequestInProcess:boolean;

  assignDate: Date = new Date();
  targetDate: Date;
  numberofOpenings: string  = "1";
  vendorDisabled: boolean=false;
  vendorName: string  = "";
  messages: string[] = [];
  severityClass: string = "";

  timer: any;

  constructor(
    private dialogRef: MatDialogRef<PublishToVendorComponent>,
    private jobActionService: JobActionService,
    private jobDetailService: JobDetailService,
    private notificationService: NotificationService,
    private i18UtilService: I18UtilService
  ) {
    this.jobSummaryData = this.jobDetailService.jobSummaryData;
    this.publishToVendorJSON = this.jobDetailService.publishToVendorJSON;
  }

  ngOnInit() {
    this.isPublishRequestInProcess=false;
    this.vendorDisabled=this.publishToVendorJSON.input[0].disabled;
    if(this.vendorDisabled){
      this.assignDate=new Date(this.publishToVendorJSON.input[0].assignDate);
      this.targetDate=new Date(this.publishToVendorJSON.input[0].targetDate);
      this.numberofOpenings=this.publishToVendorJSON.input[0].assignedOpenings;
      this.vendorName=this.publishToVendorJSON.input[0].vendorName;
    }
  }

  selectTargetDate(value) {
    this.targetDate = value;
  }

  selectAssignDate(value) {
    this.assignDate = value;
  }

  cancel() {
    this.dialogRef.close();
  }

  publish() {
    this.isPublishRequestInProcess=true
    let number = 0;
    let numberOfOpenings = this.jobSummaryData.numberOfOpenings;
    try {
      number = Number(this.numberofOpenings);
      if (isNaN(number)) {
        this.clearMessages();
        this.setMessage(NotificationSeverity.WARNING, "job.warning.enterOpeningCount");
        this.isPublishRequestInProcess=false;
        return;
      }
    } catch (e) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "job.warning.enterOpeningCount");
      this.isPublishRequestInProcess=false;
      return;
    }
    if(Number.isInteger(number)){
      if (number <= 0) {
        this.clearMessages();
        this.setMessage(NotificationSeverity.WARNING, "job.warning.openingCountNotZero");
        this.isPublishRequestInProcess=false;
        return;
      }
    } else{
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "job.warning.openingCountInvalid");
      this.isPublishRequestInProcess=false;
      return;
    }
    if (numberOfOpenings < number) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, 'job.warning.OpeningExceededWarning', {'openings': numberOfOpenings });
      this.isPublishRequestInProcess=false;
      return;
    }


    if (this.assignDate == null) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "common.warning.startDateRequired");
      this.isPublishRequestInProcess=false;
      return;
    }


    let now = new Date();
    let assignDateValid = true;
    if(!this.vendorDisabled){
    if (now.getFullYear() > this.assignDate.getFullYear()) {
      assignDateValid = false;
    } else if (now.getMonth() > this.assignDate.getMonth()) {
      assignDateValid = false;
    } else if (now.getDate() > this.assignDate.getDate()) {
      assignDateValid = false;
    }

    if (!assignDateValid) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "common.warning.startDateCurrentDate");
      this.isPublishRequestInProcess=false;
      return;
    }

  }

    if (this.targetDate == null) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "common.warning.targetDateRequired");
      this.isPublishRequestInProcess=false;
      return;
    }

    let targetDateValid = true;
    if (now.getFullYear() > this.targetDate.getFullYear()) {
      targetDateValid = false;
    } else if (now.getMonth() > this.targetDate.getMonth()) {
      targetDateValid = false;
    } else if (now.getDate() > this.targetDate.getDate()) {
      targetDateValid = false;
    }

    if (!targetDateValid) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "common.warning.targetDateCurrentDate");
      this.isPublishRequestInProcess=false;
      return;
    }

    if (this.assignDate.getTime() > this.targetDate.getTime()) {
      this.clearMessages();
      this.setMessage(NotificationSeverity.WARNING, "common.warning.targetDateStartDate");
      this.isPublishRequestInProcess=false;
      return;
    }

    for (let vendor of this.publishToVendorJSON.input) {
      vendor.targetDate = this.targetDate;
      vendor.assignDate = this.assignDate;
      vendor.assignedOpenings = number;
      vendor.jobCode = this.jobSummaryData.jobCode;
    }

    this.jobActionService.assignJobToVendor(this.publishToVendorJSON).subscribe(out => {
     
      this.clearMessages();

      let result = out.responseData[0];

      if (result == "success") {
        this.jobDetailService.publishToVendorResult = "success";

        this.dialogRef.close(this.publishToVendorJSON.input.length);
       /*  this.setMessage(NotificationSeverity.INFO, "job.success.requisitionPublished"); */
      } else {
        this.jobDetailService.publishToVendorResult = "failed";
        this.setMessage(NotificationSeverity.WARNING, "common.label."+result);
      }
      this.isPublishRequestInProcess=false;
    },error=>{
      this.isPublishRequestInProcess=false;
    });
  }


  clearMessages() {
    this.messages = [];
  }

  setMessage(notificationSeverity: string, message: string, arg?: any) {
    if (isNullOrUndefined(arg)) {
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severityClass = notificationSeverity.toLowerCase();
        this.messages.push(res);
        this.timer = setTimeout(() => {
          this.messages = [];
        }, 10000);
      });
    }
    else {
      this.i18UtilService.get(message, arg).subscribe((res: string) => {
        this.severityClass = notificationSeverity.toLowerCase();
        this.messages.push(res);
        if (this.timer != null) {
          clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
          this.messages = [];
        }, 10000);
      });

    }
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

}
