import { JobCommonServiceService } from './../../job-common-service.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { JobDetailService } from '../job-detail.service';
import { SessionService } from '../../../session.service';
import { DialogService } from '../../../shared/dialog.service';
import { AssignToRecruiterComponent } from '../../../job/job-action/assign-to-recruiter/assign-to-recruiter.component';
import { AddInterviewPanelistComponent } from '../../../job/job-action/add-interview-panelist/add-interview-panelist.component';
import { JobActionService } from '../../../job/job-action/job-action.service';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { Subscription } from 'rxjs/Subscription';
import { PublishToVendorComponent } from './publish-to-vendor/publish-to-vendor.component';
import { PostToIjpComponent } from "../../job-action/post-to-ijp/post-to-ijp.component";
import { PostToEmpRefComponent } from "../../job-action/post-to-emp-ref/post-to-emp-ref.component";
import { PostToCandidatePortalComponent } from "../../job-action/post-to-candidate-portal/post-to-candidate-portal.component";
import { PostToNaukriComponent } from "../../job-boards/naukri/post-to-naukri/post-to-naukri.component";
import { JobBoardModel } from "../../job-boards/job-board-model";
import { PostToMonsterComponent } from 'app/job/job-boards/monster/post-to-monster/post-to-monster.component';
import { PostToIimjobsComponent } from 'app/job/job-boards/iimjobs/post-to-iimjobs/post-to-iimjobs.component';
import { isNullOrUndefined } from 'util';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-performance-source-team',
  templateUrl: './performance-source-team.component.html',
  styleUrls: ['./performance-source-team.component.css'],
  providers: [DialogService]
})
export class PerformanceSourceTeamComponent implements OnInit, OnDestroy {

  interviewPanel: any[] = [];

  assignedRecruiters: any[] = [];
  sub: Subscription;
  jobCode: String;
  subscription: Subscription;
  requisitionId: number;
  showloader: boolean = false;
  isJobDisabled: boolean = false;
  jobDetailsForSoureAndTeam: any;
  isERPosted: boolean = false;
  isIJPPosted: boolean = false;
  isCPPosted: boolean = false;
  jobBoardStatus: any;
  sysRequisitionStatus: any = '';
  requisitionStatus: any = '';
  featureDataMap : Map<any,any>;
  isPublishedTo : Boolean = true;
  
  constructor(
    private route: ActivatedRoute,
    private parser: ParseJobcodeParamPipe,
    private jobDetailService: JobDetailService,
    private sessionService: SessionService,
    private DialogService: DialogService,
    public jobActionService: JobActionService,
    private notificationService: NotificationService,
    private jobCommonServiceService: JobCommonServiceService,
    private i18UtilService: I18UtilService

  ) {
    this.featureDataMap = this.sessionService.featureDataMap;
    if(this.featureDataMap.get("PUBLISH_TO_EMPLOYEE_REFERRAL") == 3 && this.featureDataMap.get("PUBLISH_TO_IJP") == 3 && this.featureDataMap.get("PUBLISH_TO_CP") == 3 && this.featureDataMap.get("PUBLISH_TO_JOBBOARD") == 3){
      this.isPublishedTo = false;
    }
  }

  ngOnInit() {
    this.interviewPanel = [];
    let jobCode = this.route.snapshot.parent.params['id'];
    this.sub = this.route.params.subscribe(params => {
      this.jobCode = jobCode;

      this.jobDetailService.getRequisitionIDByCode(this.parser.transform(jobCode)).subscribe((out) => {
        this.requisitionId = parseInt(out);
        this.getJobDetail();
        this.jobDetailService.getJobBoardStatus(this.requisitionId).subscribe((out) => {
          this.jobBoardStatus = out.responseData;
        });
      });

      this.getInterviewPanel(jobCode);
      this.getAssignedRecruiters(jobCode);

      this.jobCommonServiceService.isRecruitersTeamUpdated().subscribe( (res) => {
        if( res.indexOf("RECRUITERS") == -1 ){
          this.getAssignedRecruiters(jobCode);
        }        
      } );

    });
    
    this.isJobDisabled = this.jobCommonServiceService.isJobDisabledVar;
    this.jobCommonServiceService.isJobDisabled().subscribe( (res) => {
      this.isJobDisabled = res;
      this.getJobDetail();
    } );

  }

  getJobDetail(){
    this.jobDetailService.getJobDetailsForSoureAndTeam(this.requisitionId).subscribe(out => {
      this.jobDetailsForSoureAndTeam = out.responseData;
      if (out != null && out.responseData != null) {
        this.isERPosted = out.responseData.postedOnEmpPortal;
        this.isIJPPosted = out.responseData.postedOnIJP;
        this.isCPPosted = out.responseData.postedOnCP;
        this.sysRequisitionStatus = out.responseData.sysRequisitionStatus;
        this.requisitionStatus = out.responseData.requisitionStatus
      }
    });  
  }



  getInterviewPanel(jobCode) {

    this.jobDetailService
      .getInterviewPanelList(this.parser.transform(jobCode))
      .subscribe(response => {
        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {
              this.interviewPanel = response.responseData;
              this.jobActionService.setCurrentInterviewPanel(response.responseData);
            }
          }
        }
      });
  }

  getAssignedRecruiters(jobCode) {

    this.jobDetailService.getRequisitionIDByCode(this.parser.transform(jobCode)).subscribe((out) => {
      this.requisitionId = parseInt(out);
      this.sessionService.object = { requisitionId: parseInt(out) };
      this.subscription = this.jobActionService.getRecruitersOfJob().subscribe((res) => {
        if (res) {
          if (res.responseData) {
            if (res.responseData.length > 0) {
              this.assignedRecruiters = res.responseData;
              this.jobCommonServiceService.recruitersTeamUpdated("_RECRUITERS_" + this.assignedRecruiters.length);
            }
          }
        }
      });
    });
  }


  OpenAssignJobToRec() {
    this.sessionService.object = { requisitionId: this.requisitionId };

    this.showloader = true;
    Promise.all([this.jobActionService.getJobDetailtoPromise(),
    this.jobActionService.getRecruitersOfJobToPromise(),
    this.jobActionService.getRecruiterListtoPromise()]).then((res) => {
      this.DialogService.setDialogData(res[0]);
      this.DialogService.setDialogData1(res[1]);
      this.DialogService.setDialogData2(res[2])
      this.showloader = false;
      this.DialogService.open(AssignToRecruiterComponent, { width: '935px' }).subscribe(result => {
        this.getAssignedRecruiters(this.jobCode);
        this.jobDetailService.getJobSummaryComponent().ngOnInit();
      });
    });
    
    

  }

  OpenAddInterviewPanelist() {
    this.sessionService.object = { requisitionId: this.requisitionId };
    this.DialogService.open(AddInterviewPanelistComponent, { width: '935px' }).subscribe(() => {
      this.getInterviewPanel(this.jobCode);
    });
  }

  deleteRecruiter(recruiter) {
    if (recruiter) {
      this.jobDetailService.removeRecruiter(recruiter.requisitionRecruiterID).subscribe(response => {

        let deleted = false;

        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {
              if (response.responseData[0] == true) {
                this.i18UtilService.get('common.success.removed').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
                deleted = true;
                this.getAssignedRecruiters(this.jobCode);
                this.jobDetailService.getJobSummaryComponent().ngOnInit();
              }
            }
          }
        }

        if (!deleted) {
          this.i18UtilService.get('job.error.recruiterDeletionFailed').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      });
    }

  }



  deleteInterviewPanel(emp) {
    if (emp) {
      this.jobDetailService.removeInterviewPanel(emp.userId, emp.requisitionId).subscribe(response => {

        let deleted = false;

        if (response) {
          if (response.responseData) {
            if (response.responseData.length > 0) {
              if (response.responseData[0] == true) {
                this.i18UtilService.get('common.success.removed').subscribe((res: string) => {
                  this.notificationService.setMessage(NotificationSeverity.INFO, res);
                });
                deleted = true;
                let temp = [];
                for (let x = 0; x < this.interviewPanel.length; x++) {
                  if (this.interviewPanel[x] == emp) {
                    continue;
                  } else {
                    temp.push(this.interviewPanel[x]);
                  }
                }
                this.interviewPanel = temp;
                this.jobActionService.setCurrentInterviewPanel(temp)
              }
            }
          }
        }

        if (!deleted) {
          this.i18UtilService.get('job.error.iPanelDeletionFailed').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
          });
        }
      });
    }
  }

  hideDeletePopup(emp) {
    emp.popup = false;
  }

  showDeletePopup(emp) {
    for (let recruiter of this.assignedRecruiters) {
      recruiter.popup = false;
    }
    emp.popup = true;
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }

  openPostJobToIJP() {
    this.sessionService.object = { requisitionId: this.requisitionId };
    this.DialogService.open(PostToIjpComponent, { width: '580px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        if (result > 0) {
          this.isIJPPosted = true;
          this.jobDetailService.publishedToSubject.next(true);
        }
      }
    });
  }

  openPostJobToEmpPortal() {
    this.sessionService.object = { requisitionId: this.requisitionId };
    this.DialogService.open(PostToEmpRefComponent, { width: '580px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        if (result > 0) {
          this.isERPosted = true;
          this.jobDetailService.publishedToSubject.next(true);
        }
      }
    });
  }

  openPostJobToCandPortal() {
    this.sessionService.object = { requisitionId: this.requisitionId };
    this.DialogService.open(PostToCandidatePortalComponent, { width: '580px' }).subscribe(result => {
      if (result != undefined && typeof result == 'number') {
        if (result > 0) {
          this.isCPPosted = true;
          this.jobDetailService.publishedToSubject.next(true);
        }
      }
    });
  }

  postToNaukri(jobBoardTO: JobBoardModel) {
    if((!isNullOrUndefined(jobBoardTO.status) && !jobBoardTO.status) || isNullOrUndefined(jobBoardTO.status)){
    if(this.sysRequisitionStatus=='OPEN') {
      this.sessionService.object = { requisitionId: this.requisitionId, jobBoardID: jobBoardTO.jobBoardID };
      this.DialogService.open(PostToNaukriComponent, { width: '980px' }).subscribe(result => {
        if (result != undefined && result != 0) {
          jobBoardTO.jobPostingCode = result;
          this.jobDetailService.getJobBoardStatus(this.requisitionId).subscribe((out) => {
            this.jobBoardStatus = out.responseData;
            this.jobDetailService.publishedToSubject.next(true);
          });
        }
      });
    } else {
      this.i18UtilService.get('job.error.jobPostError',{'status': this.requisitionStatus }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }else{
    window.open('https://www.naukri.com/job-listings-'+ jobBoardTO.jobPostingCode, "_blank");
  }
  }

  postToMonster(jobBoardTO: JobBoardModel) {
    if((!isNullOrUndefined(jobBoardTO.status) && !jobBoardTO.status) || isNullOrUndefined(jobBoardTO.status)){
    if(this.sysRequisitionStatus=='OPEN') {
      this.sessionService.object = { requisitionId: this.requisitionId, jobBoardID: jobBoardTO.jobBoardID };
      this.DialogService.open(PostToMonsterComponent, { width: '980px' }).subscribe(result => {
        if (result != undefined && result != 0) {
          jobBoardTO.jobPostingCode = result;
          this.jobDetailService.getJobBoardStatus(this.requisitionId).subscribe((out) => {
            this.jobBoardStatus = out.responseData;
            this.jobDetailService.publishedToSubject.next(true);
          });
        }
      });
    } else {
      this.i18UtilService.get('job.error.jobPostError',{'status': this.requisitionStatus }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }else{
    window.open('http://jobs.monsterindia.com/details/'+ jobBoardTO.jobPostingCode +'.html','_blank');
  }
  }

  postToIIMJobs(jobBoardTO: JobBoardModel) {
    if(this.sysRequisitionStatus=='OPEN') {
      this.sessionService.object = { requisitionId: this.requisitionId, jobBoardID: jobBoardTO.jobBoardID };
      this.DialogService.open(PostToIimjobsComponent, { width: '980px' }).subscribe(result => {
        if (result != undefined && result != 0) {
          jobBoardTO.jobPostingCode = result;
          this.jobDetailService.getJobBoardStatus(this.requisitionId).subscribe((out) => {
            this.jobBoardStatus = out.responseData;
            this.jobDetailService.publishedToSubject.next(true);
          });
        }
      });
    } else {
      this.i18UtilService.get('job.error.jobPostError',{'status': this.requisitionStatus }).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }

}