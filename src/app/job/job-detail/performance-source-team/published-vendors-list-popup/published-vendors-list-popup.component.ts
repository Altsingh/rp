import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PublishedVendorsListComponent } from '../published-vendors-list/published-vendors-list.component';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { JobDetailService } from '../../job-detail.service';
import { SessionService } from '../../../../session.service';
import { DialogService } from '../../../../shared/dialog.service';
import { JobActionService } from '../../../../job/job-action/job-action.service';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { NameinitialService } from '../../../../shared/nameinitial.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-published-vendors-list-popup',
  templateUrl: './published-vendors-list-popup.component.html',
  styleUrls: ['./published-vendors-list-popup.component.css']
})
export class PublishedVendorsListPopupComponent extends PublishedVendorsListComponent implements OnInit {

  messages: string[] = [];
  severityClass: string = "";
  timer: any;
  successCount: number = 0;

  constructor(formBuilder: FormBuilder,
    route: ActivatedRoute,
    jobDetailService: JobDetailService,
    sessionService: SessionService,
    DialogService: DialogService,
    jobActionService: JobActionService,
    notificationService: NotificationService,
    private dialogRef: MatDialogRef<PublishedVendorsListPopupComponent>,
    nameinitialService: NameinitialService,
    i18UtilService: I18UtilService) {

    super(formBuilder,
      route,
      jobDetailService,
      sessionService,
      DialogService,
      jobActionService,
      notificationService,
      nameinitialService,
      i18UtilService);

  }

  ngOnInit() {
    //super.ngOnInit();
    this.processVendorsResponse(this.DialogService.getDialogData());
    this.successCount = 0;
    this.requisitionId = this.jobDetailService.requisitionId;
  }

  cancel() {
    this.dialogRef.close(this.successCount);
  }

  clearMessages() {
    this.messages = [];
  }

  setMessage(notificationSeverity: string, message: string) {
    if(notificationSeverity == 'INFO'){
      notificationSeverity = 'successful';
    }
    this.severityClass = notificationSeverity.toLowerCase();
    this.messages.push(message);
    if (this.timer != null) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.messages = [];
    }, 10000);
  }

  afterSuccess(data:number) {
    this.successCount += data;
  }
}
