import { SessionService } from 'app/session.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RejectedCandidatesService } from '../rejected-candidates.service';
import { MessageCode } from '../../../../common/message-code';
import { RejectedCandidatesComponent } from '../rejected-candidates.component';
import { ResponseData } from '../../../../candidate/candidate-list/taggedCandidates/tagged-candidate-list.component';
import { environment } from 'environments/environment';

@Component({
  selector: 'alt-rejected-candidates-card',
  templateUrl: './rejected-candidates-card.component.html',
  styleUrls: ['./rejected-candidates-card.component.css'],
  providers: [RejectedCandidatesService]
})
export class RejectedCandidatesCardComponent implements OnInit {

  @Input()
  randomcolor:string;

  @Input()
  public taggedCandidate: any;

  @Input()
  parent: RejectedCandidatesComponent;

  @Input()
  workflowStageMap: any;

  @Input() isJobDisabled: boolean = false;

  @Output() output: EventEmitter<boolean> = new EventEmitter();
  @Output() profileMatchUpdate: EventEmitter<boolean> = new EventEmitter();

  automatchEnabled:boolean;
  isActive : boolean;
  featureDataMap:Map<any,any>;

  constructor(private rejectedCandidatesService: RejectedCandidatesService, 
    private sessionService: SessionService
  ) {
    this.automatchEnabled =  this.sessionService.isAutoMatchEnabled();
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  ngOnInit() {
  }

  resumeCandidate(taggedCandidate) {
    let resumeCandidateRequest: ResumeCandidateRequest = new ResumeCandidateRequest();
    resumeCandidateRequest.candidateID = taggedCandidate.candidateId;
    resumeCandidateRequest.requisitionID = taggedCandidate.requisitionId;
    this.rejectedCandidatesService.resumeCandidate(resumeCandidateRequest).subscribe(out => {
      let messageTO: MessageCode = out.messageCode;
      let message: string = '';
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          this.emitOutput(true);
        } else {
          this.emitOutput(false);
        }
      }
    });
  }

  emitOutput(status: boolean) {
    this.output.emit(status);
  }

  updateProfileMatch(taggedCandidate){
    this.profileMatchUpdate.emit(taggedCandidate);
  }

  singleSelectEvent(event: any) {
    this.parent.singleSelectEvent(event);
  }

  getWorkflowProgressBar(workflowID: Number, item: ResponseData){
    item.stages = this.workflowStageMap[workflowID+''];
    return true;
  }
  indexOfStage(stage: string, stageList: string[]){
    return stageList.indexOf(stage);
  }


}

export class ResumeCandidateRequest {
  candidateID: Number;
  requisitionID: Number;
  loggedInUserID: Number;
  tenantID: Number;
}
