import { JobCommonServiceService } from './../../job-common-service.service';
import { TaggedCandidatesPagedService } from './../../../candidate/candidate-list/taggedCandidates/tagged-candidates-paged.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CandidateFilterComponent } from '../tagged-candidates/candidate-filter/candidate-filter.component';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { DialogService } from '../../../shared/dialog.service';
import { TagCandidateToJobComponent } from "../../../candidate/candidate-list/candidate-untagged/tag-candidate-to-job/tag-candidate-to-job.component";
import { RejectedCandidatesPagedService } from './rejected-candidates-paged.service';
import { PageState } from '../../../shared/paginator/paginator.component';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CandidateFilterTo } from '../../../candidate/candidate-list/candidate-filter-to';
import { CandidateListService } from "../../../candidate/candidate-list/candidate-list.service";
import { CopyToAnotherJobComponent } from '../tagged-candidates/copy-to-another-job/copy-to-another-job.component';
import { JobActionService } from '../../job-action/job-action.service';
import { WorkflowStageMapService } from '../../../shared/services/workflow-stage-map.service';
import { NameinitialService } from '../../../shared/nameinitial.service';
import { JobDetailWiringService } from '../../job-detail/job-detail-wiring.service';
import { JobDetailService } from '../job-detail.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-rejected-candidates',
  templateUrl: './rejected-candidates.component.html',
  styleUrls: ['./rejected-candidates.component.css'],
  providers: [DialogService, RejectedCandidatesPagedService, CandidateListService, TaggedCandidatesPagedService]
})
export class RejectedCandidatesComponent implements OnInit, OnDestroy {


  parent: RejectedCandidatesComponent;

  pageState: PageState = new PageState();

  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;
  jobStatus :boolean = true;
  isJobDisabled: boolean = false;
  randomcolor: string[] = [];


  workflowStageMapSubscription: Subscription;
  workflowStageMap: any;

  selected: any[] = [];

  rejectedCandidates: any[];
  sub: any;
  requisitionId: number;
  jobCode: String;
  filterTO: CandidateFilterTo;

  selectAll: boolean;

  loaderIndicator: string;

  showloader: boolean = false;

  wiringSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private parser: ParseJobcodeParamPipe,
    private notificationService: NotificationService,
    private dialogsService: DialogService,
    private rejectedCandidatesPagedService: RejectedCandidatesPagedService,
    private candidateListService: CandidateListService,
    private jobActionService: JobActionService,
    private formBuilder: FormBuilder,
    private workflowStageMapService: WorkflowStageMapService,
    private taggedCandidatesPagedService: TaggedCandidatesPagedService,
    private nameinitialService: NameinitialService,
    private jobDetailWiringService : JobDetailWiringService,
    private jobCommonServiceService: JobCommonServiceService,
    private jobDetailService : JobDetailService,
    private i18UtilService: I18UtilService) {

    this.filterTO = new CandidateFilterTo();
    this.parent = this;
    this.randomcolor = this.nameinitialService.randomcolor;

  }

  ngOnInit() {

    

    this.pageState.currentPageNumber = 1;
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;

    var jobCode = this.route.snapshot.parent.params['id'];
    this.jobCode = this.parser.transform(jobCode);
    /* this.jobDetailService.getRequisitionIDByCode(this.jobCode).subscribe((out) => {
      this.requisitionId = out;
      this.candidateListService.getJobStatusByRequisitionID(this.requisitionId).subscribe(out =>
      {
        if(out.response !=null)
        {
          if(out.response == "CLOSED" || out.response == "ONHOLD" || out.response == "CANCELLED")
          {
            this.jobStatus = false;
          }
        }
      });
    }); */



    this.rejectedCandidatesPagedService.setJobCode(this.jobCode);
    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });



    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {

      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {

        let query = this.queryFormGroup.controls.query.value;

        if (query.length >= 3) {

          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          this.rejectedCandidatesPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.rejectedCandidatesPagedService.clearCache();
          this.loadData();
          this.loadTotalRows();
        }
      }, 200);

    });
    this.filterTO.stageName = this.jobDetailWiringService.stageNames;
    this.loadData();
    this.loadTotalRows();
    this.workflowStageMapSubscription = this.workflowStageMapService.getMap().subscribe((response) => {
      this.workflowStageMap = response;
      this.totalRowsSubscription = null
    });
    this.wiringSubscription = this.jobDetailWiringService.getStageNameCLick().subscribe((stageName)=>{
      this.pageState.currentPageNumber = 1;
      this.pageState.pageSize = 15;
      this.filterTO.pageNumber = 1;
      this.filterTO.pageSize = this.pageState.pageSize;
      this.filterTO.stageName=stageName;
      this.selected = [];
      this.loaderIndicator = "loader";
      this.rejectedCandidatesPagedService.clearCache();
      this.loadData();
      this.loadTotalRows();
    }
    );
    this.jobDetailWiringService.getRefreshSelectedStageNames().next(true);

    this.isJobDisabled = this.jobCommonServiceService.isJobDisabledVar;
    this.jobCommonServiceService.isJobDisabled().subscribe( (res) => {
      this.isJobDisabled = res;
    } );

  }

  ngOnDestroy(){
    if(this.wiringSubscription != null){
      this.wiringSubscription.unsubscribe();
    }
  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.rejectedCandidatesPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.jobDetailWiringService.getStageWiseRejectedCount().next(response)
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.rejectedCandidatesPagedService.getPage(this.filterTO).subscribe((response) => {
      if (response) {
        this.rejectedCandidates = response;
        if (this.rejectedCandidates != null && this.rejectedCandidates.length > 0) {
          for (let can of this.rejectedCandidates) {
            can.jobCode = this.jobCode;
          }
        }
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  areAllSelected() {
    if (this.rejectedCandidates != null && this.rejectedCandidates.length > 0) {
      for (let x = 0; x < this.rejectedCandidates.length; x++) {
        if (typeof this.rejectedCandidates[x].selected == 'undefined' || (typeof this.rejectedCandidates[x].selected == 'boolean' && !this.rejectedCandidates[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }

  updateList(status: boolean) {
    if (status) {
      this.rejectedCandidatesPagedService.clearCache();
      this.ngOnInit();
      this.i18UtilService.get('candidate.success.candidateResumed').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.INFO, res);
      });
    } else {
      this.i18UtilService.get('candidate.error.candidateResumeFailed').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }

  openBulkAction() {
    if (this.selected.length == 0) {
      this.i18UtilService.get('candidate.warning.selectCandidate').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }
    //this.dialogsService.open(TagCandidateToJobComponent, { width: '980px' })
    this.showloader = true;
    this.jobActionService.setDataList(this.selected);
    this.candidateListService.getJobsOfRecruiter().subscribe(out => {
      this.dialogsService.setDialogData(out);
      this.showloader = false;
      this.dialogsService.open(CopyToAnotherJobComponent, { width: '480px' });
    });
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;

    this.loadData();
  }

  updateProfileMatch(item){
    this.showloader = true;
    this.taggedCandidatesPagedService.updateProfileMatchPercenatage(item.applicantStatusID).subscribe( (res) => {
      if( res && res.response ){
        for( let i = 0; i < this.rejectedCandidates.length; i++ ){
          if( this.rejectedCandidates[i]['applicantStatusID'] == res.response.applicantStatusID ){
            this.rejectedCandidates[i]['profileMatch'] = res.response.profileMatch;
            if( res.response.profileMatch == 0 ){
              this.i18UtilService.get('common.error.somthingWrong').subscribe((res: string) => {
                this.notificationService.setMessage(NotificationSeverity.WARNING, res);
              });
            }
          }
        }
      }
      this.showloader = false;
    } );
  }

  onSorted(params: string[]) {
    this.filterTO.sortBy = params[0];
    this.filterTO.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.rejectedCandidatesPagedService.clearCache();
    this.loadData();
  }

  onFilter(params: any[]) {
    let genderFilter = false;
    let sourceFilter = false;
    let industryFilter = false;
    let locationFilter = false;
    let functionalAreaFilter = false;
    for (let param of params) {
      if (param.name == 'LOCATION') {
        this.filterTO.location = param.value.value;
        locationFilter = true;
      } else if (param.name == 'GENDER') {
        this.filterTO.genderID = param.value.id;
        genderFilter = true;
      } else if (param.name == 'INDUSTRY') {
        this.filterTO.industryID = param.value.id;
        industryFilter = true;
      } else if (param.name == 'FUNCTIONAL_AREA') {
        this.filterTO.functionalAreaID = param.value.id;
        functionalAreaFilter = true;
      } else if (param.name == 'SOURCE') {
        this.filterTO.sourceID = param.value.id;
        sourceFilter = true;
      }

    }
    if (!genderFilter) {
      this.filterTO.genderID = null;
    }
    if (!industryFilter) {
      this.filterTO.industryID = null;
    }
    if (!functionalAreaFilter) {
      this.filterTO.functionalAreaID = null;
    }
    if (!locationFilter) {
      this.filterTO.location = null;
    }
    if (!sourceFilter) {
      this.filterTO.sourceID = null;
    }
    this.rejectedCandidatesPagedService.clearCache();
    this.ngOnInit();
  }

  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.filterTO.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    } else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    this.rejectedCandidatesPagedService.clearCache();
    this.ngOnInit();
  }

  doSelectAll() {
    let index = 0;
    for (let data of this.rejectedCandidates) {
      data.selected = this.selectAll;
      index++;
      if (this.selectAll && !this.selected.includes(data)) {
        this.selected.push(data);
      } else if (!this.selectAll) {
        let i = this.selected.indexOf(data);
        this.selected.splice(i, 1);
      }
    }
  }

  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();

  }


  
}