import { Injectable } from '@angular/core';
import { PagedDataService } from '../../../common/paged-data.service';
import { SessionService } from '../../../session.service';
import { Http } from '@angular/http';

@Injectable()
export class RejectedCandidatesPagedService extends PagedDataService {

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/jobDetail/rejectedCandidates/',
      '/rest/altone/jobDetail/rejectedCandidates/totalRows/'
    );
  }

  setJobCode(jobCode) {
    if (jobCode != null) {
      super.initialize(
        '/rest/altone/jobDetail/rejectedCandidates/?jobCode=' + jobCode,
        '/rest/altone/jobDetail/rejectedCandidates/totalRows/?jobCode=' + jobCode
      );
    }

  }

}
