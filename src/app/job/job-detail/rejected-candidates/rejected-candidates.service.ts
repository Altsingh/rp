import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { SessionService } from '../../../session.service';

@Injectable()
export class RejectedCandidatesService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  resumeCandidate(resumeCandidateRequest : ResumeCandidateRequest) {
    let options = new RequestOptions({ headers: this.getHeaders() });
    let request:ResumeCandidateRequestService = new ResumeCandidateRequestService();
    request.input = resumeCandidateRequest;
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/jobs/resumeCandidate/' + this.sessionService.organizationID+"/", JSON.stringify(request), options).
      map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
  }

  

}

export class ResumeCandidateRequestService {
  input: ResumeCandidateRequest;
}

export class ResumeCandidateRequest {
  candidateID   : Number;
  requisitionID : Number;
  loggedInUserID: Number;
  tenantID      : Number;
}
