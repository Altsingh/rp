import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class JobDetailWiringService {
  stageNameClick: Subject<string[]> = new Subject<string[]>();
  refreshSelectedStageNames: Subject<any> = new Subject<any>();
  stageWiseTotalCount: Subject<any> = new Subject<number>();
  stageWiseTaggedCount: Subject<any> = new Subject<number>();
  stageWiseRejectedCount: Subject<any> = new Subject<number>();
  public stageNames: string[] = [];
  refreshStageListComponent: Subject<any> = new Subject<number>();

  constructor() { }

  getStageNameCLick(): Subject<string[]> {
    return this.stageNameClick;
  }

  getRefreshSelectedStageNames(): Subject<any> {
    return this.refreshSelectedStageNames;
  }

  getStageWiseTotalCount(): Subject<any> {
    return this.stageWiseTotalCount;
  }

  getStageWiseTaggedCount(): Subject<any> {
    return this.stageWiseTaggedCount;
  }

  getStageWiseRejectedCount(): Subject<any> {
    return this.stageWiseRejectedCount;
  }

  getRefreshStageListComponent(): Subject<any> {
    return this.refreshStageListComponent;
  }
}
