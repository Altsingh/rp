import { Component, OnInit, OnDestroy,Input } from '@angular/core';
import { JobDetailService } from '../../job-detail.service';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';
import { JobDescriptionData } from './job-description-data';
import { Subscription } from 'rxjs/Subscription'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'alt-job-description',
  templateUrl: './job-description.component.html'
})
export class JobDescriptionComponent implements OnInit, OnDestroy {
  @Input()
  security: any;
  jobDescriptionData: JobDescriptionData;
  jobDetailService: JobDetailService;
  sub: any;
  jobCode: String;
  subscription:Subscription;
  toggleClass: boolean;
  checkData:number=0;
  jobDescHTML: SafeHtml = '';
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService,
    private sanitizer: DomSanitizer) {
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }
 
  setLoading(){
    this.jobDescriptionData = new JobDescriptionData();
    this.jobDescriptionData.jobDescriptionHTML = `Loading...`;
  }

  ngOnInit() {

    this.route.parent.params.subscribe((params) => {
      this.setLoading();
      let jobCode = this.route.snapshot.parent.params['id'];
      if(jobCode== undefined || jobCode == null) {  
        jobCode = this.route.snapshot.params['id'];
      }
      this.subscription = this.jobDetailService
          .getJobDescription(this.parser.transform(jobCode))
          .subscribe((p) => {
            this.jobDescriptionData = p;
            if( this.jobDescriptionData.jobDescriptionHTML == null || (this.jobDescriptionData.jobDescriptionHTML != null && this.jobDescriptionData.jobDescriptionHTML.trim() == "") ){
              this.checkData = 0;
            }else{
              this.jobDescHTML = this.sanitizer.bypassSecurityTrustHtml(this.jobDescriptionData.jobDescriptionHTML);
              this.checkData = Object.keys(this.jobDescHTML).length;
            }
          });
    });
  }

  ngOnDestroy(){
    if(this.subscription != null){
      this.subscription.unsubscribe();
    }
    if(this.sub != null){
      this.sub.unsubscribe();
    }
  }
  toggleBlock() {
    this.toggleClass = !this.toggleClass;
  }
}
