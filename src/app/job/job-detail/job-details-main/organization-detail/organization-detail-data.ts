export class OrganizationDetailData {
    public orgUnitHierarchy: OrgUnit[];
    public workSiteHierarchy: OrgUnit[];
    public functionalArea: string;
    public industry: string;
    public role: string;
}

export class OrgUnit {
    public orgUnitType: string;
    public orgUnitName: string;
}