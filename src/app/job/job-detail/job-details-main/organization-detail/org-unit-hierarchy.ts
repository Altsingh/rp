export class OrgUnitHierarchy {
    constructor(
        public orgUnitType: string,
        public orgUnitName: string
    ) {

    }
}