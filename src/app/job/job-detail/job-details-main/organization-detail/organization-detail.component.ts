import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { OrgUnitHierarchy } from './org-unit-hierarchy';
import { OrganizationDetailData, OrgUnit } from './organization-detail-data';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { JobDetailService } from '../../job-detail.service';
import { Subscription } from 'rxjs/Subscription'
@Component({
  selector: 'alt-organization-detail',
  templateUrl: './organization-detail.component.html'
})
export class OrganizationDetailComponent implements OnInit, OnDestroy {
  @Input()
  security: any;
  organizationDetailData: OrganizationDetailData;
  sub: any;
  jobCode: String;
  jobDetailService: JobDetailService;
  subscription: Subscription;
  @Output()
  toggle: EventEmitter<string> = new EventEmitter();
  className: string = 'panel-open';
  checkData: number = 0;
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService) {
    this.organizationDetailData = new OrganizationDetailData();
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }

  setLoading() {
    this.organizationDetailData.functionalArea = 'Loading...';
    this.organizationDetailData.role = 'Loading...';
    this.organizationDetailData.industry = 'Loading...';
    let orgUnit = new OrgUnit();
    orgUnit.orgUnitType = 'Loading...';
    orgUnit.orgUnitName = 'Loading...';
    this.organizationDetailData.orgUnitHierarchy = [orgUnit];
  }
  toggleOrg() {
    this.className = this.className == 'panel-open' ? 'panel-hide' : 'panel-open';
    this.toggle.emit(this.className);
  }
  ngOnInit() {

    this.route.parent.params.subscribe((params) => {

      this.setLoading();

      let jobCode = this.route.snapshot.parent.params['id'];
      if(jobCode== undefined || jobCode == null) {  
        jobCode = this.route.snapshot.params['id'];
      }


        this.subscription = this.jobDetailService
          .getOrganizationDetails(this.parser.transform(jobCode))
          .subscribe(p => {

            this.organizationDetailData = p;

            let orgUnitHierarchy = [];
            if (p) {
              if (p.orgUnitHierarchy) {
                if (p.orgUnitHierarchy.length > 0) {
                  for (let i = p.orgUnitHierarchy.length - 1; i >= 0; i--) {
                    orgUnitHierarchy.push(p.orgUnitHierarchy[i]);
                  }
                }
              }
            }
            

            this.organizationDetailData.orgUnitHierarchy = orgUnitHierarchy;


            let workSiteHierarchy = [];
            if (p) {
              if (p.workSiteHierarchy) {
                if (p.workSiteHierarchy.length > 0) {
                  for (let i = p.workSiteHierarchy.length - 1; i >= 0; i--) {
                    workSiteHierarchy.push(p.workSiteHierarchy[i]);
                  }
                }
              }
            }
            this.organizationDetailData = p;

            this.organizationDetailData.workSiteHierarchy = workSiteHierarchy;


            this.checkData = Object.keys(this.organizationDetailData).length;
          });

    


    });


  }
  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }

}
