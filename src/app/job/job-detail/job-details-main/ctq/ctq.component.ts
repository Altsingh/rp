import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { JobDetailService } from '../../job-detail.service';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription'
import { CodegenComponentFactoryResolver } from '@angular/core/src/linker/component_factory_resolver';
import { CTQInformationData } from './ctq-information-data';
@Component({
  selector: 'alt-ctq',
  templateUrl: './ctq.component.html'
})
export class CtqComponent implements OnInit, OnDestroy {
  @Input()
  security: any;
  ctqInformationData: CTQInformationData;
  ctqs: string[];
  sourcingGuidline: string;
  jobDetailService: JobDetailService;
  sub: any;
  jobCode: String;
  toggleClass: boolean;
  checkData: number = 0;
  subscription: Subscription;
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService) {
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }

  toggleBlock() {
    this.toggleClass = !this.toggleClass;
  }
  setLoading() {
    this.ctqs = [
      'Loading...'
    ];
  }

  ngOnInit() {
    if (this.jobDetailService.sourcingGuideLine != null)
        this.sourcingGuidline = this.jobDetailService.sourcingGuideLine.trim();
    else
        this.sourcingGuidline = this.jobDetailService.sourcingGuideLine;
    
    this.jobDetailService.getSourcingGuideLine().subscribe((sourcingGuidline) => {
      if (sourcingGuidline != null)
        this.sourcingGuidline = sourcingGuidline.trim();
      else
        this.sourcingGuidline = sourcingGuidline;
    });

    this.route.parent.params.subscribe((params) => {
      this.setLoading();
      let jobCode = this.route.snapshot.parent.params['id'];
      if(jobCode== undefined || jobCode == null) {  
        jobCode = this.route.snapshot.params['id'];
      }
        this.subscription = this.jobDetailService
          .getCTQInformation(this.parser.transform(jobCode))
          .subscribe(p => {
            this.ctqInformationData = p ;
            this.checkData = Object.keys(this.ctqInformationData).length;
          });
    });
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }


}
