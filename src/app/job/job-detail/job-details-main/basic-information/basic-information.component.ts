import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { BasicInformationData } from './basic-information-data';
import { JobDetailService } from '../../job-detail.service';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';
import { NotificationService, NotificationSeverity } from '../../../../common/notification-bar/notification.service';
import { Subscription } from 'rxjs/Subscription'


@Component({
  selector: 'alt-basic-information',
  templateUrl: './basic-information.component.html'
})
export class BasicInformationComponent implements OnInit, OnDestroy {
  @Input()
  security: any;
  basicInformationData: BasicInformationData;
  jobDetailService: JobDetailService;
  sub: any;
  jobCode: String;
  subscription: Subscription;
  @Output()
  toggle: EventEmitter<string> = new EventEmitter();
  className: string = 'panel-open';
  checkData: number = 0;
  
  @Output()
  requisitionID: EventEmitter<number> = new EventEmitter();

  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService, private notificationService: NotificationService) {
    this.basicInformationData = new BasicInformationData();
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }

  setLoading() {
    this.basicInformationData.ctcRange = 'Loading... ';
    this.basicInformationData.numberOfOpenings = 'Loading...';
    this.basicInformationData.requiredWorkExperience = 'Loading...';
    this.basicInformationData.requisitionClosingDate = 'Loading...';
    this.basicInformationData.requisitionStartingDate = 'Loading...';
    this.basicInformationData.requisitionType = 'Loading...';
    this.basicInformationData.grade = 'Loading...';
    this.basicInformationData.hiringManager = 'Loading...';
    this.basicInformationData.designation = 'Loading...';
    this.basicInformationData.industry = 'Loading...';
    this.basicInformationData.createdBy = 'Loading...';
    this.basicInformationData.employeeCategoryName = 'Loading...';
    this.basicInformationData.jobFamilyName = 'Loading...';
    this.basicInformationData.positionName = 'Loading...';
    this.basicInformationData.campusTypeName = 'Loading...';
    this.basicInformationData.resourceTypeName = 'Loading...';
    this.basicInformationData.programTypeName = 'Loading...';
    this.basicInformationData.roleTypeName = 'Loading...';
    this.basicInformationData.roleContributionName = 'Loading...';
    this.basicInformationData.employmentTenureName = 'Loading...';
    this.basicInformationData.employmentTypeName = 'Loading...';
    this.basicInformationData.tat = 'Loading...';
    this.basicInformationData.band = 'Loading...';
    this.basicInformationData.employeeCode = 'Loading...'
  }
  toggleBasic() {
    this.className = this.className == 'panel-open' ? 'panel-hide' : 'panel-open';
    this.toggle.emit(this.className);
  }
  ngOnInit() {
    this.route.parent.params.subscribe((params) => {

      this.setLoading();

      let jobCode = this.route.snapshot.parent.params['id'];
      if(jobCode== undefined || jobCode == null) {  
        jobCode = this.route.snapshot.params['id'];
      }

      this.subscription = this.jobDetailService
        .getBasicInformation(this.parser.transform(jobCode))
        .subscribe(p => {
          this.basicInformationData = p 
          this.checkData = Object.keys(this.basicInformationData).length;
          this.requisitionID.emit(this.basicInformationData.requisitionID);          
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }

}
