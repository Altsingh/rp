export class BasicInformationData {
    public requisitionID: number;
    public requisitionType: string;
    public numberOfOpenings: string;
    public requisitionStartingDate: string;
    public requisitionClosingDate: string;
    public requiredWorkExperience: string;
    public ctcRange: string;
    public hiringManager: string;
    public grade: string;
    public designation: string;
    public industry: string;
    public createdBy: string;
    public employeeCategoryName: string;
    public jobFamilyName: string;
    public positionName: string;
    public campusTypeName: string;
    public programTypeName: string;
    public roleTypeName: string;
    public roleContributionName: string;
    public employmentTenureName: string;
    public resourceTypeName: string;
    public employmentTypeName: string;
    public tat: string;
    public confidential:boolean;
    public band: string;
    public employeeCode: string;
}