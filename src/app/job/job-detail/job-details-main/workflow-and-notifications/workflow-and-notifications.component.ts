import { Component, OnInit, OnDestroy,Input } from '@angular/core';
import { WorkflowNotificationData } from './workflow-notification-data';
import { JobDetailService } from '../../job-detail.service';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription'
@Component({
  selector: 'alt-workflow-and-notifications',
  templateUrl: './workflow-and-notifications.component.html'
})
export class WorkflowAndNotificationsComponent implements OnInit, OnDestroy {
  @Input()
  security: any;
  workflowNotificationData: WorkflowNotificationData;
  jobDetailService: JobDetailService;
  sub: any;
  jobCode: String;
  toggleClass: boolean;	
  subscription:Subscription;
  checkData:number=0;
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService) {
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }
  toggleBlock() {
    this.toggleClass = !this.toggleClass;
  }
  setLoading(){
    this.workflowNotificationData = new WorkflowNotificationData();
    this.workflowNotificationData.workflowType = 'Loading....';
  }

  ngOnInit() {

    this.route.parent.params.subscribe((params) => {

      this.setLoading();

      let jobCode = this.route.snapshot.parent.params['id'];
      if(jobCode== undefined || jobCode == null) {  
        jobCode = this.route.snapshot.params['id'];
      }
        this.subscription = this.jobDetailService
          .getWorkflowNotifications(this.parser.transform(jobCode))
          .subscribe(p => this.workflowNotificationData = p);
          this.checkData = Object.keys(this.workflowNotificationData).length;

    });
    
  }

  
ngOnDestroy(){
    if(this.subscription != null){
      this.subscription.unsubscribe();
    }
    if(this.sub != null){
      this.sub.unsubscribe();
    }
  }

}
