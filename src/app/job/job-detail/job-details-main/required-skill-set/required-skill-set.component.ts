import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { JobDetailService } from '../../job-detail.service';
import { ParseJobcodeParamPipe } from '../../../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';
import { RequiredSkillsData } from './required-skill-set-data';
import { Subscription } from 'rxjs/Subscription';
import { SessionService } from '../../../../session.service';

@Component({
  selector: 'alt-required-skill-set',
  templateUrl: './required-skill-set.component.html'
})

export class RequiredSkillSetComponent implements OnInit, OnDestroy {
  @Input()
  security: any;
  requiredSkillsData: RequiredSkillsData;
  jobDetailService: JobDetailService;
  sub: any;
  jobCode: String;
  subscription: Subscription;
  toggleClass: boolean;
  checkData1: number = 0;
  checkData2: number = 0;
  checkData3: number = 0;
  checkData4: number = 0;
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe, jobDetailService: JobDetailService,
    private sessionService: SessionService) {
    this.jobDetailService = jobDetailService;
    this.setLoading();
  }
  toggleBlock() {
    this.toggleClass = !this.toggleClass;
  }
  setLoading() {
    this.requiredSkillsData = new RequiredSkillsData();
    // this.requiredSkillsData.requiredSkills = ['Loading...']
    this.requiredSkillsData.requiredCertifications = ['Loading...']
    this.requiredSkillsData.requiredLanguages = ['Loading...']
    this.requiredSkillsData.requiredQualifications = ['Loading...']
    this.sessionService.jobSkills = [];
  }

  ngOnInit() {

    this.route.parent.params.subscribe((params) => {

      let jobCode = this.route.snapshot.parent.params['id'];
      if(jobCode== undefined || jobCode == null) {  
        jobCode = this.route.snapshot.params['id'];
      }

        this.setLoading();

        this.subscription = this.jobDetailService
          .getRequiredSkills(this.parser.transform(jobCode))
          .subscribe((p) => {
            this.requiredSkillsData = p;
            if(p.requiredSkills != null){
              this.checkData1 = Object.keys(p.requiredSkills).length;
            }
            if(p.requiredCertifications != null){
              this.checkData2 = Object.keys(p.requiredCertifications).length;
            }
            if(p.requiredLanguages != null){
              this.checkData3 = Object.keys(p.requiredLanguages).length;
            }
            if(p.requiredQualifications != null){
              this.checkData4 = Object.keys(p.requiredQualifications).length;
            }
          });
    });

  }


  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }
  }

}
