import { SkillType } from "../../../../common/new-job-skill-details-request";

export class RequiredSkillsData {
    requiredSkills: SkillType[];
    requiredCertifications: string[];
    requiredLanguages: string[];
    requiredQualifications: string[];
}