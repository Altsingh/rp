import { Component, OnInit } from '@angular/core';
import { NewJobFormSecurityService } from '../../new-job/new-job-form-security.service';
import { Subscription } from 'rxjs/Subscription';
import { NewJobService, SaveJobRequest } from '../../new-job/new-job.service';
import { ActivatedRoute, Router } from '@angular/router';
import { I18UtilService } from '../../../shared/services/i18-util.service';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { NotificationSeverity } from '../../../vendor/new-vendor/company-details/vendor-company-deatils-dialog/vendor-company-deatils-dialog.component';

@Component({
  selector: 'alt-job-details-main',
  templateUrl: './job-details-main.component.html',
  providers: [NewJobService]
})
export class JobDetailsMainComponent implements OnInit {
  security: any;
  securitySubscription: Subscription;
  basicClass:string='panel-open';
  orgClass:string='panel-open';
  actionList: any[];
  stageID: any;
  workflowStageType: any;
  commentVisible: boolean = false;
  comments: any = "";
  requisitionID:any;
  jobApprovalButtonBlock: boolean = false;
  approveSuccess: boolean = false;
  rejectSuccess: boolean = false;
  constructor(private ACTIVATED_ROUTE: ActivatedRoute,
    private i18UtilService: I18UtilService,
    private formSecurityService: NewJobFormSecurityService,
    private notificationService: NotificationService,
    private newJobService: NewJobService,
    private router: Router) { 
    
    this.ACTIVATED_ROUTE.queryParams.subscribe((queryParams) => {
      this.stageID = queryParams['ssid'];
      this.workflowStageType = queryParams['sss'];
    });
  }

  ngOnInit() {
    this.security = {};
    this.initFormSecurity();
    this.initActionListSecurity();   
  }
  private initFormSecurity() {
    this.securitySubscription = this.formSecurityService.getData().subscribe(
      response => {
        this.security = JSON.parse(JSON.stringify(response.responseData[0]));        
      });
  }

  private initActionListSecurity()
  {
    if (this.stageID != undefined && this.workflowStageType != undefined && this.stageID != null && this.workflowStageType != null && this.workflowStageType=="Approval") {
      this.newJobService.getWorkflowStageActions(this.stageID, this.workflowStageType).subscribe(response => {
        this.actionList = response.responseData;        
        for (let action of this.actionList) {
          if (action.stageActionVisible == true) {
            this.commentVisible = true;
          }
        }
      });
    }
  }

  goToDefaultAction(uiAction: String) {
    let valid = true;      
    if (this.comments == null || this.comments == '') {
        valid = false;
    }
    if (valid) {
      if (uiAction == 'APPROVE') {
        this.doWorkflowTask('APPROVE');
      }
      else if (uiAction == 'REJECT') {
        this.doWorkflowTask('REJECT');

      }
    }
    else{
      this.i18UtilService.get('common.warning.enterComments').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
    }
  }

  doWorkflowTask(action: string) {
    this.notificationService.clear();
    this.jobApprovalButtonBlock = true;    


    let jobApprovalTO = {
      requisitionID : this.requisitionID,
      isWorkflowCommentsRequired : this.security.isWorkflowCommentsRequired,
      isOrganizationUnitRendered : this.security.isOrganizationUnitRendered, 
      organizationUnitLabel : this.security.organizationUnitLabel,
      joiningLocationLabel : this.security.joiningLocationLabel,
      isJoiningLocationRendered: this.security.isJoiningLocationRendered,
      comment: this.comments,
      action:action
    }

    if(action == 'APPROVE')
    {
      this.newJobService.approveRejectJob(jobApprovalTO).subscribe(response => {
        if (response != null && response.messageCode != null && response.messageCode.code != null) {
          if (response.messageCode.code == "EC200") {
            this.i18UtilService.get('common.success.successApprove').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);
            });
            this.approveSuccess = true;
  
          }
          else {
            this.i18UtilService.get('common.error.failedApprove').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        }
        if (this.approveSuccess) {
          this.router.navigateByUrl('/home');
        }
        this.jobApprovalButtonBlock = false;
      });  
    }
    else if(action == 'REJECT')
    {
      this.newJobService.approveRejectJob(jobApprovalTO).subscribe(response => {
        if (response != null && response.messageCode != null && response.messageCode.code != null) {
          if (response.messageCode.code == "EC200") {
            this.i18UtilService.get('common.success.successReject').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.INFO, res);

              
            });
            this.rejectSuccess = true;
  
          }
          else {
            this.i18UtilService.get('common.error.failedReject').subscribe((res: string) => {
              this.notificationService.setMessage(NotificationSeverity.WARNING, res);
            });
          }
        }
        if (this.rejectSuccess) {
         this.router.navigateByUrl('/home');
        }
        this.jobApprovalButtonBlock = false;
      });  
    }    
  } 
  goBackToHome()
  {    
    this.router.navigateByUrl('/home');    
  }
}
