import { JobCommonServiceService } from './../../job-common-service.service';
import { statusChangeRequestTO } from './statusChangeRequestTO';
import { StatusChangeService } from './status-change.service';
import { DialogService } from 'app/shared/dialog.service';
import { SessionService } from 'app/session.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-status-change',
  templateUrl: './status-change.component.html',
  styleUrls: ['./status-change.component.css'],
  providers: [StatusChangeService]
})
export class StatusChangeComponent implements OnInit {

  userCommentInfo: FormGroup;
  status: number;
  finalJobStatusText: string;
  jobCode: string;
  contentType: string;
  initialJobStatus: string;
  requisitionID: number;

  constructor(
    public dialogsService: DialogService, 
    public dialogRef: MatDialogRef<StatusChangeComponent>, 
    private formBuilder: FormBuilder, 
    public sessionService: SessionService,
    private jobCommonServiceService:JobCommonServiceService,
    public statusChangeService: StatusChangeService) { }

  ngOnInit() {
    let statusObj = this.dialogsService.getDialogData();
    this.status = statusObj.contentTypeID;
    this.contentType = statusObj.contentType;
    this.finalJobStatusText = statusObj.label;

    let dataObj = this.dialogsService.getDialogData1();
    dataObj = dataObj[0];
    this.requisitionID = dataObj.requisitionID;
    this.jobCode = dataObj.jobCode;
    this.initialJobStatus = dataObj.jobStatusLabel;

    this.userCommentInfo = this.formBuilder.group({
      userComment: null
    });

  }

  submitComment(){
    let comment = this.userCommentInfo.controls.userComment.value;
    let statusChangeRequest: statusChangeRequestTO = new statusChangeRequestTO();
    statusChangeRequest.status = this.status;
    statusChangeRequest.finalJobStatusText = this.finalJobStatusText;
    statusChangeRequest.comment = comment;
    statusChangeRequest.requisitionID = this.requisitionID;
    statusChangeRequest.jobCode = this.jobCode;
    statusChangeRequest.initialJobStatus = this.initialJobStatus;
    this.statusChangeService.changeJobStatus(statusChangeRequest).subscribe( (res) => {
      this.jobCommonServiceService.disableJobByStatus(this.contentType);
      this.jobCommonServiceService.requisitionLabel = this.finalJobStatusText;
      this.dialogRef.close('Success');
    },error=>{
      this.dialogRef.close('Failed');
    });
  }

}
