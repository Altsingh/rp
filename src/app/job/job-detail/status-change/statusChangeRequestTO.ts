
export class statusChangeRequestTO{

  status: Number;
  comment: String;
  requisitionID: Number;
  jobCode           : string;
  initialJobStatus  : string;
  finalJobStatusText: string;

}
