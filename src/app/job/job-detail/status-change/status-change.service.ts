import { statusChangeRequestTO } from './statusChangeRequestTO';
import { SessionService } from 'app/session.service';
import { RequestOptions, Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { CustomHttpService } from 'app/shared/services/custom-http-service.service';

@Injectable()
export class StatusChangeService {

  constructor( public sessionService: SessionService,  private http:Http,private httpService:CustomHttpService ) { }

  getJobStatusesList( systemLevel : boolean){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getJobStatusesList/' , { params: { "systemLevel": systemLevel }})
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  changeJobStatus(statusChangeRequest: statusChangeRequestTO){
    return this.httpService.post(this.sessionService.orgUrl + '/rest/altone/jobs/changeJobStatus' , JSON.stringify(statusChangeRequest))
  }

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

}
