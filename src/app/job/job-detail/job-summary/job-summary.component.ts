import { JobCommonServiceService } from './../../job-common-service.service';
import { AutoMatchSocialPagedService } from './../auto-match-social-paged.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { JobDetailService } from '../job-detail.service';
import { JobSummaryData } from './job-summary-data'
import { SessionService } from '../../../session.service';
import { NotificationSeverity, NotificationService } from '../../../common/notification-bar/notification.service';
import { Subscription } from 'rxjs/Subscription';
import { AutoMatchCandidatesService } from '../auto-match-candidates.service';
import { FormatJobcodeParamPipe } from '../../../shared/format-jobcode-param.pipe';
import { AutoMatchCompanyPagedService } from '../auto-match-company-paged.service';
import { JobActionService } from '../../job-action/job-action.service';
import { DialogService } from '../../../shared/dialog.service';
import { AssignToRecruiterComponent } from '../../job-action/assign-to-recruiter/assign-to-recruiter.component';
import { environment } from '../../../../environments/environment';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-job-summary',
  templateUrl: './job-summary.component.html',
  styleUrls: ['./job-summary.component.css'],
  providers: [FormatJobcodeParamPipe]
})

export class JobSummaryComponent implements OnInit, OnDestroy {

  jobSummaryData: JobSummaryData;
  jobDetailService: JobDetailService
  sub: any;
  jobCode: string;
  requisitionID: string;
  subscription: Subscription;
  showloader   = false;
  id:any;
  isJobDisabled: boolean = false;
  featureDataMap:Map<any,any>;
  isAutomatchEnabled : Boolean = true;
  isERenabled : Boolean = false;
  isJobApproval : Boolean = false;
  isIJPenabled : Boolean = false;
  
  constructor(
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private parser: ParseJobcodeParamPipe,
    private formatter: FormatJobcodeParamPipe,
    jobDetailService: JobDetailService,
    private jobActionBulkService: JobActionService,
    private dialogService: DialogService,
    private sessionService: SessionService,
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private autoMatchSocialPagedService: AutoMatchSocialPagedService,
    private jobCommonServiceService:JobCommonServiceService,
    private autoMatchCompanyPagedService: AutoMatchCompanyPagedService,
    private i18UtilService: I18UtilService) {
    window.scroll(0, 0);
    this.jobDetailService = jobDetailService;
    this.jobSummaryData = new JobSummaryData();
    this.jobSummaryData.jobCode = "Loading...";
    this.jobSummaryData.jobName = "Loading...";
    this.jobSummaryData.postedOn = "Loading...";
    this.jobSummaryData.publishedTo = 0;
    this.jobSummaryData.summaryOrgUnit = "Loading...";
    this.jobSummaryData.summaryWorkSite = "Loading...";
    this.jobSummaryData.targetOn = "Loading...";
    this.jobSummaryData.teamSize = 0;
    this.jobSummaryData.sourcingGuidling = "Loading...";
    this.jobDetailService.setJobSummaryComponent(this);
    this.autoMatchSocialPagedService.clearLastRequestJSON();
    this.autoMatchSocialPagedService.clearCache();
    if(this.route.snapshot.routeConfig.path.includes('er')){
        this.isERenabled = true;
    }
    else if(this.route.snapshot.routeConfig.path.includes('ijp')){
      this.isIJPenabled = true;
    }
    else if(this.route.snapshot.routeConfig.path.includes('jobApproval')){
      this.isJobApproval = true;
    }
    this.featureDataMap = this.sessionService.featureDataMap;
    if(this.featureDataMap.get("AUTOMATCH_JOBBOARD") == 3 && this.featureDataMap.get("AUTOMATCH_SOCIALPROFILE") == 3 && this.featureDataMap.get("AUTOMATCH_COMPANYDATA") == 3){
        this.isAutomatchEnabled = false;
    }
  }


  ngOnInit() {

    this.autoMatchSocialPagedService.clearLastRequestJSON();
    this.autoMatchSocialPagedService.clearCache();

    this.sub = this.route.params.subscribe(params => {

      this.subscription = this.jobDetailService
        .getJobSummary(this.parser.transform(params['id']))
        .subscribe((p) => {
          if (p.summaryWorkSite == null) {
            p.summaryWorkSite = "";
          }
          this.jobDetailService.sourcingGuideLineUpdated(p.sourcingGuidling);

          this.jobSummaryData = p;
          this.jobCommonServiceService.disableJobByStatus(this.jobSummaryData.jobStatus);
          this.jobCommonServiceService.setRequisitionLabel(this.jobSummaryData.jobStatusLabel);
          this.jobDetailService.jobSummaryData = p;

        });
      
        this.id = this.parser.transform(params['id']);
        this.jobDetailService.getRequisitionIDByCode(this.parser.transform(params['id'])).subscribe((out) => {
          this.requisitionID = out;
          this.sessionService.object = parseInt(this.requisitionID);
        });;

    });

    this.jobDetailService.publishedToSubject.subscribe(response => {
      
      this.subscription = this.jobDetailService
        .getJobSummary(this.id)
        .subscribe((p) => {
          if (p.summaryWorkSite == null) {
            p.summaryWorkSite = "";
          }
          this.jobSummaryData = p;
          this.jobCommonServiceService.disableJobByStatus(this.jobSummaryData.jobStatus);
          this.jobCommonServiceService.setRequisitionLabel(this.jobSummaryData.jobStatusLabel);
          this.jobDetailService.jobSummaryData = p;
        })

    });

    this.jobCommonServiceService.isJobDisabled().subscribe( (res) => {
      this.isJobDisabled = res;
    } );

    this.jobCommonServiceService.isRecruitersTeamUpdated().subscribe( (res) => {
      if( res.indexOf("MENU") == -1 ){
        let parts = res.split("_");
        this.jobSummaryData.teamSize = parseInt(parts[2]);
      }        
    } );

  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  updateJobStatus(jobStatus){
    this.jobSummaryData.jobStatus = jobStatus;
  }

  createCandidateForJob() {

    if (this.jobSummaryData.jobCode == "Loading...") {
      this.i18UtilService.get('job.warning.jobDetailLoading').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);  
      });
      return;
    }

    this.subscription = this.jobDetailService.isJobAssigned(this.jobSummaryData.jobCode).subscribe(response => {
      let assigned = false;

      if (response) {
        if (response.responseData) {
          if (response.responseData.length > 0) {
            if (response.responseData[0] == true) {
              assigned = true;
              this.sessionService.objectType = "candidateIdFromJobDetailPage";
              this.sessionService.object = this.requisitionID;
              this.router.navigateByUrl('/candidate/newcandidate');
            }
          }
        }
      }

      if (!assigned) {
        this.i18UtilService.get('job.error.unassignedJob').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);  
        });
      }
    })

  }

  autoMatchCandidates() {

    this.autoMatchCompanyPagedService.clearCache();
    this.autoMatchSocialPagedService.clearLastRequestJSON();
    this.autoMatchSocialPagedService.clearCache();

    if (this.jobSummaryData.jobCode == "Loading...") {
      this.i18UtilService.get('job.warning.jobDetailLoading').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);  
      });
      return;
    }

    let jobClosed = false;
    let jobHold = false;
    let redirect = false;

    if (this.jobSummaryData != null) {
      if (this.jobSummaryData.jobStatus != null
        && (this.jobSummaryData.jobStatus !== 'CLOSED' && this.jobSummaryData.jobStatus !== 'PCLOSE')
        && (this.jobSummaryData.jobStatus !== 'ONHOLD' && this.jobSummaryData.jobStatus !== 'PHOLD')) {
        this.autoMatchCandidatesService.jobSummaryData = this.jobSummaryData;
        this.autoMatchCandidatesService.requisitionId = parseInt(this.requisitionID);
        this.autoMatchCandidatesService.requisitionCode = this.jobSummaryData.jobCode;
        if(this.featureDataMap.get("AUTOMATCH_JOBBOARD") == 1){
        this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatter.transform(this.jobSummaryData.jobCode) + '/jobboard');
        return;
        }
        else if(this.featureDataMap.get("AUTOMATCH_COMPANYDATA") == 1){
          this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatter.transform(this.jobSummaryData.jobCode) + '/company');
        return;
        }
        else if(this.featureDataMap.get("AUTOMATCH_SOCIALPROFILE") == 1){
          this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatter.transform(this.jobSummaryData.jobCode) + '/social');
        return;
        }
        else{
          this.isAutomatchEnabled = false;
        }
      }

      if (this.jobSummaryData.jobStatus != null
        && (this.jobSummaryData.jobStatus === 'CLOSED' || this.jobSummaryData.jobStatus === 'PCLOSE')) {
        jobClosed = true;
      }
      if (this.jobSummaryData.jobStatus != null
        && (this.jobSummaryData.jobStatus === 'ONHOLD' || this.jobSummaryData.jobStatus === 'PHOLD')) {
        jobHold = true;
      }

    }

    if (jobClosed) {
      this.i18UtilService.get('job.warning.closedJobAutomatchWarning').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);  
      });
    } else if (jobHold) {
      this.i18UtilService.get('job.warning.onHoldJobAutomatchWarning').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);  
      });
    } else if (!redirect) {
      this.i18UtilService.get('job.warning.unknownJobStatus').subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);  
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.sub != null) {
      this.sub.unsubscribe();
    }    
  }
  OpenAssignJobToRec(data: JobSummaryData) {
    let requisitionId: Number = data.requisitionId;
    let jobCode: String = data.jobCode;
    this.sessionService.object = {
      requisitionId: requisitionId,
      jobCode: jobCode
    };
    this.showloader = true;
    Promise.all([this.jobActionBulkService.getJobDetailtoPromise(),
    this.jobActionBulkService.getRecruitersOfJobToPromise(),
    this.jobActionBulkService.getRecruiterListtoPromise()]).then(res => {
      this.dialogService.setDialogData(res[0]);
      this.dialogService.setDialogData1(res[1]);
      this.dialogService.setDialogData2(res[2])
      this.showloader = false;
      this.dialogService.open(AssignToRecruiterComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && typeof result == 'number') {
          data.teamSize = data.teamSize + result;
          if( result > 0 ){
            this.jobCommonServiceService.recruitersTeamUpdated("_MENU_0");
          }
        }
      });
    });
  }

}
