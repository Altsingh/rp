export class JobSummaryData{
    public jobName:string;
    public jobCode:string;
    public summaryOrgUnit:string;
    public summaryWorkSite:string;
    public postedOn:string;
    public targetOn:string;
    public teamSize:number;
    public publishedTo:number;
    public jobStatus:string;
    public requisitionId:Number;
    public jobStatusLabel:string;
    public sourcingGuidling:string;
    public minExperience:number;
    public maxExperience:number;
}