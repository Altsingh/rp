import { Injectable } from '@angular/core';
import { MonsterComponent } from 'app/job/job-detail/auto-matched-candidates/job-board-profiles/monster/monster/monster.component';

@Injectable()
export class MonsterControllerService {

  monsterComponent: MonsterComponent;
  
  constructor() { }

  setMonsterComponent(monsterComponent: MonsterComponent) {
    this.monsterComponent = monsterComponent;
  }

  getMonsterComponent(): MonsterComponent {
    return this.monsterComponent;
  }
}
