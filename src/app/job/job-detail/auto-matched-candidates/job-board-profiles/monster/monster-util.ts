import { SaveCandidateDetailsTO } from "app/common/SaveCandidateDetailsTO.model";
import { EducationTO } from "app/common/EducationTO.model";
import { CandidateAddressTO } from "app/common/CandidateAddressTO.model";

export default class MonsterUtil {

    static setCandidateDetailsRequestJSON(requestJSON, resumeData, requisitionID, jobBoardName) {
        requestJSON = this.setBasicDetailsRequestJSON(requestJSON, resumeData, requisitionID, jobBoardName);
        requestJSON = this.setPersonalDetailsRequestJSON(requestJSON, resumeData);
        requestJSON = this.setEducationDetailsRequestJSON(requestJSON, resumeData);
        return requestJSON;
    }

    static initRequestJSON(requestJSON: SaveCandidateDetailsTO) {
        if (requestJSON == null || requestJSON == undefined) {
            requestJSON = new SaveCandidateDetailsTO();
        }
        if (requestJSON.candidateAddressTO == null || requestJSON.candidateAddressTO == undefined) {
            requestJSON.candidateAddressTO = new CandidateAddressTO();
        }
        return requestJSON;
    }

    static setBasicDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, resumeData: any, requisitionID: any, jobBoardName: any) {
        requestJSON = this.initRequestJSON(requestJSON);

        let dob = new Date(resumeData.jobBoardResumeResponseTO.dob);
        let mobileNumber = '';
        if (!(resumeData.jobBoardResumeResponseTO.phone == null || resumeData.jobBoardResumeResponseTO.phone == 'Not Specified' || resumeData.jobBoardResumeResponseTO.phone == 'Confidential' || resumeData.jobBoardResumeResponseTO.phone == '')) {
            mobileNumber = resumeData.jobBoardResumeResponseTO.phone
        }

        requestJSON.candidateId = 0;
        requestJSON.requisitionID = requisitionID;
        requestJSON.firstName = resumeData.jobBoardResumeResponseTO.firstName;
        requestJSON.lastName = resumeData.jobBoardResumeResponseTO.lastName;
        requestJSON.mobileNumber = mobileNumber;
        requestJSON.primaryEmail = resumeData.jobBoardResumeResponseTO.email;
        requestJSON.dateOfBirth = dob;

        requestJSON.sourceTypeID = undefined;
        requestJSON.subSourceTypeID = undefined;
        requestJSON.subSourceUserID = undefined;
        requestJSON.subSourceTypeName = jobBoardName;
        requestJSON.jobBoardName = resumeData.jobBoardResumeResponseTO.jobBoardName.toUpperCase();
        requestJSON.triggerPoint = jobBoardName + ' New RP';

        requestJSON.jobBoardSID = resumeData.jobBoardResumeResponseTO.sid;
        requestJSON.resume_base64 = resumeData.jobBoardResumeResponseTO.detailedResume;
        requestJSON.resumeExt = resumeData.jobBoardResumeResponseTO.resumeExt;

        return requestJSON;
    }

    static setEducationDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, resumeData: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let educationTOs: Array<EducationTO> = [];
        if (resumeData.jobBoardResumeResponseTO.educations != null && resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree != null && resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.level != null
            && resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.institute != null) {
            let educationTO: EducationTO = new EducationTO();
            educationTO.candidateEduDetailId = undefined;
            educationTO.degree = resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.level;
            educationTO.institute = resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.institute;
            educationTOs.push(educationTO);
        }

        if (resumeData.jobBoardResumeResponseTO.educations != null && resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree != null && resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.level != null
            && resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.institute != null) {
            let educationTO: EducationTO = new EducationTO();
            educationTO.candidateEduDetailId = undefined;
            educationTO.degree = resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.level;
            educationTO.institute = resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.institute;
            educationTOs.push(educationTO);
        }

        requestJSON.educationTOs = educationTOs;
        return requestJSON;
    }

    static setPersonalDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, resumeData: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        requestJSON.candidateAddressTO.city = resumeData.jobBoardResumeResponseTO.city;
        requestJSON.candidateAddressTO.country = resumeData.jobBoardResumeResponseTO.country;
        return requestJSON;
    }

}