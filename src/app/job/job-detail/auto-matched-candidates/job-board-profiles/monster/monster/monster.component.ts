import { CamelCaseConverterService } from './../../../../../../common/camel-case-converter.service';
import { RoutingStateService } from './../../../../../../common/routing-state.service';
import { MonsterService } from './monster.service';
import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { NameinitialService } from '../../../../../../shared/nameinitial.service';
import { AutoMatchCandidatesService } from '../../../../auto-match-candidates.service';
import { JobDetailService } from '../../../../job-detail.service';
import { SessionService } from "../../../../../../session.service";
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService, NotificationSeverity } from '../../../../../../common/notification-bar/notification.service';
import { MessageCode } from '../../../../../../common/message-code';
import { ParseJobcodeParamPipe } from './../../../../../parse-jobcode-param.pipe';
import { PageState } from '../../../../../../shared/paginator/paginator.component';
import { JobFilterTO } from '../../../../../job-list/job-filter-to';
import { Subscription } from 'rxjs/Subscription';
import { NewCandidateService } from 'app/candidate/new-candidate/new-candidate.service';
import { FormatCandidatecodeParamPipe } from 'app/shared/format-candidatecode-param.pipe';
import { CommonService } from 'app/common/common.service';
import { AutoMatchMonsterPagedService } from '../../../../auto-match-monster-paged.service';
import MonsterUtil from '../monster-util';
@Component({
  selector: 'alt-monster',
  templateUrl: './monster.component.html',
  styleUrls: ['./monster.component.css'],
  providers: [NewCandidateService, FormatCandidatecodeParamPipe]
})
export class MonsterComponent implements OnInit {
  currentRequestJSON: any;
  monsterFilter= {};
  loader: boolean = false;
  resumes: any;
  clearall: boolean = false;
  nodata: boolean = false;
  responseData: any;
  randomcolor: string[] = [];
  requisitionCode: string;
  requisitionCodePath: string;
  jobTitle: string;
  skills: string;
  skillsPretty: string;
  minimumExp: String;
  maximumExp: String;
  q: string;
  requisitionID: Number;
  sectioncompleted: number = 0;

  jobBoardName: String = "monster";
  isBinarySearchEnabled: boolean = false;

  lastRequestSearchString: any;
  
  pageState: PageState = new PageState();
  filterTO: JobFilterTO;
  totalRowsSubscription: Subscription;
  pageLoadingSubscription: Subscription;
  filterApplied:any;
  @Output() totalRecords: EventEmitter<number> = new EventEmitter();
  
  constructor(
    private monsterService: MonsterService,
    private nameinitialService: NameinitialService,
    private commonService: CommonService,
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private jobDetailService: JobDetailService,
    private sessionService: SessionService,
    private route: ActivatedRoute,
    private parser: ParseJobcodeParamPipe,
    private notificationService: NotificationService,
    private newCandidateService: NewCandidateService,
    private router: Router,
    private formatCandidatecodeParamPipe: FormatCandidatecodeParamPipe,
    private routingStateService:RoutingStateService,
    private camelCaseConverterService:CamelCaseConverterService,
    private autoMatchMonsterPagedService: AutoMatchMonsterPagedService
  ) {
    this.filterTO = new JobFilterTO();
    this.randomcolor = nameinitialService.randomcolor;
    this.loader = true;
  }

  ngOnInit() {
    this.commonService.showRHS();
    this.requisitionCodePath = this.route.snapshot.parent.parent.params['id'];

    if( this.router.url.endsWith('monster') ){
      this.commonService.setSideBar('autoMatchMonster');
      this.jobBoardName = "monster";
      this.isBinarySearchEnabled = true;
    }else if( this.router.url.indexOf('/shine') > -1 ){
      this.commonService.setSideBar('default');
      this.jobBoardName = "shine";
    }

    if( this.routingStateService.getPreviousUrl() != null && this.routingStateService.getPreviousUrl().indexOf( "job/edit-job" ) >= 0 ){
      this.autoMatchMonsterPagedService.clearLastRequestSearchString();
    }

    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.lastRequestSearchString = this.autoMatchMonsterPagedService.getLastRequestSearchString();
    if(this.sessionService.monsterFilter){
      let lastRequestJSON = this.autoMatchMonsterPagedService.getLastRequestJSON();
      if (lastRequestJSON != null) {
        this.monsterFilter = lastRequestJSON;
      }
    }

    this.nameinitialService.randomize();
    this.randomcolor = this.nameinitialService.randomcolor;
    this.requisitionCode = this.parser.transform(this.route.snapshot.parent.parent.params['id']);
    this.monsterService.getJobDetailsForJobBoard(this.requisitionCode).subscribe(out => {
      this.jobTitle = out.responseData.jobTitle.replace(' ', '+');
      out.responseData.skillList = JSON.parse(out.responseData.skillList);
      this.skills = out.responseData.skills = this.convertSkillsToFormattedSkills( out.responseData.skillList );

      if( this.lastRequestSearchString == null || this.lastRequestSearchString == undefined ){
        this.skillsPretty = this.skills.replace(/'/g, '').replace(/\+/g, '').replace(/\s\s+/g, ' ').trim();
      }else{
        this.skillsPretty = this.lastRequestSearchString;
        this.skills = this.convertSkillsPrettyToArray(this.skillsPretty);
      }

      this.q = (this.jobTitle + '+' + this.skills).replace('#', '');
      this.minimumExp = out.responseData.minimumExp;
      this.maximumExp = out.responseData.maximumExp;
      this.requisitionID = out.responseData.requisitionId;
      if( this.autoMatchMonsterPagedService.sortBy!=undefined || this.autoMatchMonsterPagedService.sortBy!=null ){
        this.monsterFilter["sortBy"] = this.autoMatchMonsterPagedService.sortBy;
      }
      this.loadData();
    });

    this.autoMatchMonsterPagedService.getFilterApplied().subscribe((filterApplied) => {
      if (filterApplied) {
        let filterType = filterApplied.type;
        if(filterType == 'clearall'){
          this.clearAllFilter();
        } else if(filterType=='sort'){
          this.monsterFilter["sortBy"]  = filterApplied.event;
        }
         else {
          this.applyFilter(filterApplied);
        }
      }
      this.autoMatchMonsterPagedService.clearCache();
       this.loadData();
    });
  }

  reQueryData(skillsPretty){
    if( skillsPretty.trim().length == 0 ){
      this.skills = "";
      this.autoMatchMonsterPagedService.setLastRequestSearchString(skillsPretty);
      this.loadData();
      return;
    }

    //storing for later display
    this.skillsPretty = skillsPretty;
    this.autoMatchMonsterPagedService.setLastRequestSearchString(skillsPretty);

    this.skills = this.convertSkillsPrettyToArray(skillsPretty);
    this.q = (this.jobTitle + '+' + this.skills).replace('#', '');
    this.loadData();
  }

  convertSkillsPrettyToArray(skillsPretty){
    skillsPretty = skillsPretty.toUpperCase().replace(/\(/g, '').replace(/\)/g, '' ).replace(/\s\s+/g, ' ').trim();
    let SkillsArray = skillsPretty.split(" AND ");
    let filteredSkillsArray = [];
    let SkillsItemParts;

    for( let i = 0; i < SkillsArray.length; i++ ){

      if( SkillsArray[i].indexOf( " OR " ) > 0 ){

        SkillsItemParts = SkillsArray[i].split(" OR ");
        filteredSkillsArray.push({ "text":SkillsItemParts[0], "mandatory":1 });
        let SkillsItemPartslength = SkillsItemParts.length;
        if( i != SkillsArray.length ){
          filteredSkillsArray.push({ "text":SkillsItemParts[SkillsItemPartslength-1], "mandatory":1 });
          SkillsItemPartslength -= 1;
        }
        for( let j = 1; j < SkillsItemPartslength; j++ ){
          filteredSkillsArray.push({ "text":SkillsItemParts[j], "mandatory":0 });
        }

      }else{
        filteredSkillsArray.push({ "text":SkillsArray[i], "mandatory":1 });
      }    

    }

    return this.convertSkillsToFormattedSkills(filteredSkillsArray).replace(/\s\s+/g, ' ').trim();
  }

  convertSkillsToFormattedSkills( skillList ){
    if( !skillList ){
      return "";
    }
    let mandatorySkills = [];
    let optionalSKills = [];
    let skillStore = [];
    let skillString = "";
    for( let i = 0; i < skillList.length; i++ ){
      if( skillStore.indexOf(skillList[i].text.toLowerCase()) == -1 ){
        skillStore.push(skillList[i].text.toLowerCase());
      }else{
        continue;
      }

      if( skillList[i].mandatory == 1 ){
        mandatorySkills.push(skillList[i]);
      }else{
        optionalSKills.push(skillList[i]);
      }
    }
  
    for( let i = 0; i < mandatorySkills.length; i++ ){
      if( mandatorySkills[i].text.indexOf( " " ) > 0 ){
        mandatorySkills[i].text = "'" + mandatorySkills[i].text + "'";
      }
      if( skillString.length == 0 ){
        skillString += ( "( " + mandatorySkills[i].text );
      }else{
        skillString += ( " + AND + " + mandatorySkills[i].text );
      }

      if( i == mandatorySkills.length-1 ){
        if( optionalSKills.length == 0 ){
          skillString += " )";
        }else{
          skillString += " ) + ";
        }
      }
    }
    
    for( let i = 0; i < optionalSKills.length; i++ ){
      if( optionalSKills[i].text.indexOf( " " ) > 0 ){
        optionalSKills[i].text = "'" + optionalSKills[i].text + "'";
      }

      if( skillString.length > 0 ){
        skillString += "OR + ";
      }

      skillString += ( optionalSKills[i].text + " + " );
      if( i == optionalSKills.length-1 ){
        skillString = skillString.substr(0, skillString.length-3);
      }      
    }

    return skillString;
  }

  loadData() {
    this.loader = true;
    this.nodata = false;
    this.autoMatchMonsterPagedService.setValues(this.q, this.pageState.currentPageNumber, this.pageState.pageSize, this.minimumExp, this.maximumExp, this.jobBoardName);
    this.pageLoadingSubscription = this.autoMatchMonsterPagedService.getMonsterSearchRecords(this.monsterFilter).subscribe(out => {
    window.scroll(0, 0);
      if ( out != null && out.response != null && out.response.results != null ) {
        this.resumes = out.response.results;
        this.pageState.totalRows = out.response.totalRecords;
        this.loader = false;
        if(typeof this.pageState.totalRows == 'number') {
          this.totalRecords.emit(this.pageState.totalRows);
        }
      } else{
        this.nodata = true;
        this.loader = false;
      }
    }, () => {
      this.loader = false;
    });
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;
    this.loadData();
  }

  ngOnDestroy() {
    this.commonService.setSideBar('default');
  }

  downloadResume(data: any) {
    data.resumeDownloading = true;
    this.monsterService.downloadResume(data.jobBoardResumeResponseTO).subscribe(out => {
      let messageTO: MessageCode = out.messageCode;
      let message: string = '';
      if (messageTO != null) {
        if (messageTO.code != null && messageTO.code === 'EC200') {
          data.downloaded = true;
          message = "Resume downloaded successfully."
        }
        else if (messageTO.message != null) {
          message = messageTO.message.toString();
        }
        else {
          message = "Request failed to execute."
        }

        if (messageTO.code === 'EC200') {
          this.notificationService.setMessage(NotificationSeverity.INFO, message);
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, message);
        }
      }

    }, (onError) => {
      this.notificationService.setMessage(NotificationSeverity.WARNING, 'Something went wrong. Please contact your System Admin.');
      data.resumeDownloading = false;
    }, () => {
      data.resumeDownloading = false;
    });
  }

  profileCompletionBasic(requestData): number {
    let rangeList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    let count: number = 0;
    
    let json = requestData;
    if (json.firstName != null && json.firstName.trim() != '') {
      count += 1
    }
    if (json.lastName != null && json.lastName.trim() != '') {
      count += 1
    }
    if (json.mobileNumber != null && json.mobileNumber.trim() != '') {
      count += 1
    }
    if (json.primaryEmail != null && json.primaryEmail.trim() != '') {
      count += 1
    }
    if (json.dateOfBirth != undefined && json.dateOfBirth != null) {
      count += 1
    }
    if (json.genderId != null && json.genderId.trim() != '') {
      count += 1
    }
    if (json.aadhaarNumber != null && json.aadhaarNumber.trim() != '') {
      count += 1
    }
    if (json.panNumber != null && json.panNumber.trim() != '') {
      count += 1
    }
    return (count / rangeList.length);
  }

  profileCompletionEducation(requestData): number {
    let rangeList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    let count: number = 0;
    
    let education: string = '';
    for (let x = 0; x < requestData.educationHistory.length; x++) {
      let edu = requestData.educationHistory[x];
      if(edu.degree!=null && edu.degree.trim()!=''){
      education = edu.degree.trim();
      break;
      }
    }
    if (education != '') {
      count += 1
    }

    return (count / rangeList.length);
  }

  profileCompletionPersonal(requestData): number {
    let rangeList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    let count: number = 0;
    let personalsection = requestData;
    if ((personalsection.city != null && personalsection.city!= '')) {
      count += 1
    }
    return (count / rangeList.length);
  }

  saveAsDraft(data: any) {
    data.savingDraft = true;
    this.monsterService.getDownloadedResume(this.jobBoardName, data.jobBoardResumeResponseTO.sid).subscribe(out => {
      let resumeData = out.response;
      
      let requestJSON = MonsterUtil.setCandidateDetailsRequestJSON(undefined, resumeData, this.requisitionID, this.jobBoardName);

      let subscription = this.newCandidateService.saveCandidateDetails(requestJSON.candidateId, requestJSON, true, undefined).subscribe(response => {
        if (response != null && response != undefined) {
          if (response.messageCode.code == 'EC200') {
            data.draftCandidateCode = response.response.candidateCode;
            data.savedDraft = true;
            this.notificationService.setMessage(NotificationSeverity.INFO, "Draft Saved Successfully");
          } else if (response.messageCode.code == 'EC201') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
          } else if (response.messageCode.code == 'EC202') {
            this.notificationService.setMessage(NotificationSeverity.WARNING, "Candidate already exists in the database");
          } else {
            this.notificationService.setMessage(NotificationSeverity.WARNING, "Error Occured while saving ! Please try again later");
            
          }
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not connect to the server. Please try again.");
        }
        data.savingDraft = false;
      },
        () => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not perform save request. Please try again");
          data.savingDraft = false;
        });

    });
  }

  goToCandidateDraft(draftCandidateCode) {
    this.router.navigateByUrl("/candidate/newcandidate/" + this.formatCandidatecodeParamPipe.transform(draftCandidateCode));
  }

  goToCandidate(candidateCode) {
    this.router.navigateByUrl("/candidate/edit-candidate/" + this.formatCandidatecodeParamPipe.transform(candidateCode));
  }
  
  applyFilter(filterApplied: any) {
    let action = filterApplied.action;
    let event = filterApplied.event;
    let filterType = filterApplied.type;
    let filterValue = typeof event == 'string' ? event : event.target.value;
    if(filterType=='all'){
      let allFilters : string[] = ['location','industry','role','functionalArea','salary'];
      for(let item of allFilters){
          this.removeFilter(item,filterValue,action);
      }
    } else{
      this.removeFilter(filterType,filterValue,action);
    } 
  }
  removeFilter(filterType: string,filterValue: string,action: string){
    let filterParams = [];
    if (this.monsterFilter[filterType]) {
      filterParams = this.monsterFilter[filterType];
    } 
    if(filterType=='salary'){
      filterParams = [];
      this.monsterFilter['salary'] = [];
    }
    for (let filterParam of filterParams) {
      if (filterParam == filterValue) {
        if(filterParams.indexOf(filterParam) != -1){
          filterParams.splice(filterParams.indexOf(filterParam),1);
        }
        // filterParams.splice(filterParams.indexOf(filterParam), 1);
        break;
      }
    }
    if (action == 'add') {
      filterParams.push(filterValue);
    }
   
    if (filterParams.length > 0) {
      this.monsterFilter[filterType] = filterParams;
    }
  }
  clearAllFilter(){
    this.monsterFilter['location']=[];
    this.monsterFilter['industry']=[];
    this.monsterFilter['role']=[];
    this.monsterFilter['functionalArea']=[];
    this.monsterFilter['salary']=[];
  }
}