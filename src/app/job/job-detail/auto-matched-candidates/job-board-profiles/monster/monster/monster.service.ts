import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from '../../../../../../session.service';
import 'rxjs/add/operator/map';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class MonsterService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getJobDetailsForJobBoard(jobCode: string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobboard/jobdetails', { params: { 'jobCode': jobCode }})
      .map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  downloadResume(data: any) {
    let url = this.sessionService.orgUrl + '/rest/altone/jobboard/jobboard/download';
    
    let options = this.getRequestOptions();
    return this.http.post(url, JSON.stringify(data), options).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json();
    });    
  }

  getDownloadedResume(jobBoardName: String, sid: string) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobboard/'+ jobBoardName +'/' + sid + '/')
      .map(res => { this.sessionService.check401Response(res.json()); return res.json(); });
  }

  getHeaders(): Headers {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append("Content-Type", "application/json");
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  getRequestOptions(): RequestOptions {
    let headers = this.getHeaders();

    let options = new RequestOptions({
      headers: headers
    });

    return options;
  }


}
