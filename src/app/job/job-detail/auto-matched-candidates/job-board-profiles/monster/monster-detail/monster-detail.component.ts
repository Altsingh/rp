import { Component, OnInit } from '@angular/core';
import { MonsterService } from 'app/job/job-detail/auto-matched-candidates/job-board-profiles/monster/monster/monster.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AutoMatchCandidatesService } from 'app/job/job-detail/auto-match-candidates.service';
import { JobDetailService } from 'app/job/job-detail/job-detail.service';
import { ParseJobcodeParamPipe } from 'app/job/parse-jobcode-param.pipe';
import { NotificationService, NotificationSeverity } from './../../../../../../common/notification-bar/notification.service';
import { SessionService } from 'app/session.service';
import { NameinitialService } from 'app/shared/nameinitial.service';
import { FormatCandidatecodeParamPipe } from 'app/shared/format-candidatecode-param.pipe';
import { NewCandidateService } from 'app/candidate/new-candidate/new-candidate.service';
import { CommonService } from 'app/common/common.service';
import MonsterUtil from '../monster-util';

@Component({
  selector: 'alt-monster-detail',
  templateUrl: './monster-detail.component.html',
  styleUrls: ['./monster-detail.component.css'],
  providers: [MonsterService, ParseJobcodeParamPipe, NewCandidateService, FormatCandidatecodeParamPipe]
})
export class MonsterDetailComponent implements OnInit {

  loader: boolean;
  resume_base64: String;
  resumeExt: String;
  requisitionCode: String;
  resumeData: any;
  randomcolor: string[] = [];

  jobSummaryData: any;
  jobSkills: any[];
  displayHighestEducationDegree: boolean;
  secondHighestEducationDegree: boolean;

  jobBoardName: String;

  constructor(
    private monsterService: MonsterService,
    private commonService: CommonService,
    private route: ActivatedRoute,
    private router: Router,
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private jobDetailService: JobDetailService,
    private parser: ParseJobcodeParamPipe,
    private notificationService: NotificationService,
    private sessionService: SessionService,
    private nameinitialService: NameinitialService,
    private newCandidateService: NewCandidateService,
    private formatCandidatecodeParamPipe: FormatCandidatecodeParamPipe,
  ) {
    this.commonService.showRHS();
    this.commonService.setSideBar('autoMatchMonster');
    
    if( this.router.url.indexOf('/shine') > -1 ){
      this.commonService.setSideBar('default');
    }

    
    this.randomcolor = nameinitialService.randomcolor;
    this.jobSummaryData = {};
    this.jobSummaryData.jobCode = "Loading...";
    this.jobSummaryData.jobName = "Loading...";
    this.jobSummaryData.postedOn = "Loading...";
    this.jobSummaryData.publishedTo = "Loading...";
    this.jobSummaryData.summaryOrgUnit = "Loading...";
    this.jobSummaryData.summaryWorkSite = "Loading...";
    this.jobSummaryData.targetOn = "Loading...";
    this.jobSummaryData.teamSize = "Loading...";
  }

  ngOnInit() {
    this.loader = true;

    this.jobBoardName = this.route.snapshot.params['jobboardname'];
    
    this.monsterService.getDownloadedResume(this.jobBoardName, this.route.snapshot.params['sid']).subscribe(out => {
      
      this.commonService.disableRhsMonster();
      this.resumeData = out.response;
      this.resume_base64 = out.response.jobBoardResumeResponseTO.detailedResume;
      this.resumeExt = out.response.jobBoardResumeResponseTO.resumeExt;
      this.loader = false;
      this.getEducationArray();
    });

    this.requisitionCode = this.parser.transform(this.route.snapshot.params['id']);
    if (this.autoMatchCandidatesService.jobSummaryData != null) {
      this.jobSummaryData = this.autoMatchCandidatesService.jobSummaryData;
    }

    this.jobSkills = this.sessionService.jobSkills;

    if (this.autoMatchCandidatesService.jobSummaryData == null) {
      
      Promise.resolve(this.jobDetailService.getJobSummary(this.requisitionCode).toPromise()).then((response) => {
        this.processJobDetailsResponse(response);
      }).catch((error) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not load job details!");
      });
    }

    if (this.jobSkills.length == 0) {
      Promise.resolve(this.jobDetailService.getRequiredSkills(this.requisitionCode).toPromise()).then((response) => {
        this.processJobSkillsResponse(response);
      }).catch((error) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not find job");
      });
    }
  }

  ngOnDestroy() {
    this.commonService.setSideBar('default');
  }

  processJobDetailsResponse(response) {
    this.jobSummaryData = response;
    this.sessionService.jobSummaryData = this.jobSummaryData;
    this.jobDetailService.jobSummaryData = response;
  }

  processJobSkillsResponse(response) {
    if (response) {
      if (response.requiredSkills) {
        this.jobSkills = response.requiredSkills;
        this.sessionService.jobSkills = response.requiredSkills;
      }
    }
  }

  goBack() {
    this.sessionService.monsterFilter = true;
    this.router.navigateByUrl('/job/auto-match-candidates/' + this.route.snapshot.params['id'] + '/jobboard/' + this.jobBoardName );
  }

  downloadResume() {
    var b64Data = this.resume_base64;
    var blob = this.b64toBlob(b64Data, 'application/pdf');
    var blobUrl = URL.createObjectURL(blob);
    let ext = '.html';
    if( this.jobBoardName == 'shine' ){
      ext = '.' + this.resumeExt;
    }
    const fileName: string = 'resume_' + this.route.snapshot.params['sid'] + ext;
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    a.href = blobUrl;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(blobUrl);
  }

  b64toBlob(b64Data, contentType, sliceSize = 512) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  getlengthLessThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length > 40
  }
  getlengthGreatThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length <= 40
  }
  getlengthLessThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length > 40
  }
  getlengthGreatThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length <= 40
  }
  getlengthZero() {
    return this.jobSummaryData.summaryWorkSite == null || this.jobSummaryData.summaryWorkSite.length == 0
  }

  getEducationArray() {

    if( this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree != null ){
      if ((this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.level != null
        && this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.level != 0
        && this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.level.trim() != '')
        || (this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.stream != null
          && this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.stream != 0
          && this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.stream.trim() != '')
        || ( this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.institute != null
          && this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.institute != 0
          && this.resumeData.jobBoardResumeResponseTO.educations.highestEducationDegree.institute.trim() != '')) {
        this.displayHighestEducationDegree = true;
      }
    }
    
    if( this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree != null ){
      if ((this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.level != null
        && this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.level != 0
        && this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.level.trim() != '')
        || ( this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.stream != null
          && this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.stream != 0
          && this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.stream.trim() != '')
        || ( this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.institute != null
          && this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.institute != 0
          && this.resumeData.jobBoardResumeResponseTO.educations.secondHighestEducationDegree.institute.trim() != '')) {
        this.secondHighestEducationDegree = true;
      }
    }
    
  }

  saveAsDraft(data: any) {
    data.savingDraft = true;
    let requestJSON = MonsterUtil.setCandidateDetailsRequestJSON(undefined, data, this.jobSummaryData.requisitionId, this.jobBoardName);
    let subscription = this.newCandidateService.saveCandidateDetails(requestJSON.candidateId, requestJSON, true, undefined).subscribe(response => {
      if (response != null && response != undefined) {
        if (response.messageCode.code == 'EC200') {
          data.draftCandidateCode = response.response.candidateCode;
          data.savedDraft = true;
          this.notificationService.setMessage(NotificationSeverity.INFO, "Draft Saved Successfully");
        } else if (response.messageCode.code == 'EC201') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else if (response.messageCode.code == 'EC202') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Candidate already exists in the database");
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Error Occured while saving ! Please try again later");
        }
      } else {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not connect to the server. Please try again.");
      }
      data.savingDraft = false;
    },
      () => {
        data.savingDraft = false;
      });
  }

  goToCandidateDraft(draftCandidateCode) {
    this.router.navigateByUrl("/candidate/newcandidate/" + this.formatCandidatecodeParamPipe.transform(draftCandidateCode));
  }

  goToCandidate(candidateCode) {
    this.router.navigateByUrl("/candidate/edit-candidate/" + this.formatCandidatecodeParamPipe.transform(candidateCode));
  }
}
