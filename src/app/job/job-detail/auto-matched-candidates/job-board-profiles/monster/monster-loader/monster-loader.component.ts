import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-monster-loader',
  templateUrl: './monster-loader.component.html',
  styleUrls: ['./monster-loader.component.css']
})
export class MonsterLoaderComponent implements OnInit {

  @Input() isBinarySearchEnabled: boolean;

  constructor() { }

  ngOnInit() {
  }

}
