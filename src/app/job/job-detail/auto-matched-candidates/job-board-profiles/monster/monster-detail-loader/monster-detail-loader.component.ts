import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-monster-detail-loader',
  templateUrl: './monster-detail-loader.component.html',
  styleUrls: ['./monster-detail-loader.component.css']
})
export class MonsterDetailLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
