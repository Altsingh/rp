import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommonService } from 'app/common/common.service';
import { SessionService } from '../../../../session.service';
import { JobDetailService } from '../../job-detail.service';
import { PostToJobBoardService } from '../../../job-boards/post-to-job-board.service';
import { SelectItem } from '../../../../shared/select-item';

@Component({
  selector: 'alt-job-board-profiles',
  templateUrl: './job-board-profiles.component.html',
  styleUrls: ['./job-board-profiles.component.css'],
  providers: [PostToJobBoardService]
})
export class JobBoardProfilesComponent implements OnInit {

  locationList: SelectItem[];
  clearall: boolean = false;
  jobBoardID: number;
  jobBoardCandidates = [];
  totalRecords: number = -1;
  security:any;
  monsterAcess:boolean;
  featureDataMap:Map<any,any>;

  constructor(private commonService: CommonService,
    private jobDetailService: JobDetailService,
    private sessionService: SessionService,
    private route: ActivatedRoute,
    private postToJobBoardService: PostToJobBoardService) {
    this.featureDataMap = this.sessionService.featureDataMap;
    this.jobDetailService.getAutoMatchSecurity().subscribe(
      response => {
        if (response.messageCode.code === 'EC200') {
          this.security = JSON.parse(JSON.stringify(response.responseData[0]));
        }
        else
          this.security = undefined;
      });

    this.jobDetailService.checkMonsterAccess().subscribe(
      response => {
        if (response.messageCode.code === 'EC200') {
          this.monsterAcess = true;
        }
        else {
          this.monsterAcess = false;
        }
      });


  }

  ngOnInit() {
    this.commonService.enableRhsMonster();
  }

  hideContextMenu(item) {
    item.displayClass = "displayNone";
  }

  showContextMenu(item) {
    item.displayClass = "displayBlock";
  }

  getLocationList() {
    this.postToJobBoardService.getJobBoardMaster("MONSTER", "INDIANLOC").subscribe(response => {
      this.locationList = response.responseData;
    });
}

}
