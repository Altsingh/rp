import { Subscription } from 'rxjs';
import { FormatJobcodeParamPipe } from './../../../../shared/format-jobcode-param.pipe';
import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PageState } from '../../../../shared/paginator/paginator.component';
import { AutoMatchCandidatesService } from '../../auto-match-candidates.service';
import { NewCandidateService } from '../../../../candidate/new-candidate/new-candidate.service';
import { NotificationService, NotificationSeverity } from "../../../../common/notification-bar/notification.service";
import { AutoMatchSocialPagedService } from '../../auto-match-social-paged.service';
import { NameinitialService } from '../../../../shared/nameinitial.service';
import { JobDetailService } from '../../job-detail.service';
import { CommonService } from '../../../../common/common.service';
import { SessionService } from "../../../../session.service";
import SocialUtil from "./social-util";
import { DialogService } from '../../../../shared/dialog.service';
import { AutomatchEmailComponent } from '../automatch-email/automatch-email.component';
import { AutoatchEmailService } from '../automatch-email/automatch-email.service';

@Component({
  selector: 'alt-social-profiles',
  templateUrl: './social-profiles.component.html',
  styleUrls: ['./social-profiles.component.css'],
  providers: [NewCandidateService, FormatJobcodeParamPipe, AutoatchEmailService]
})
export class SocialProfilesComponent implements OnInit, OnDestroy {

  //social tab
  jobDetailsForSocialMatch: any;
  socialPageState: PageState = new PageState();
  socialPageLoading: boolean = true;
  allSocialSelected: boolean = false;
  clearall: boolean = false;
  socialSelected: any[] = [];
  currentRequestJSON: any;
  requisitionId: number;
  requisitionCode: string;
  randomcolor: string[] = [];
  socialMailLoader: String;
  savingCandidateDone: boolean = false;
  automatchEnabled: boolean;

  socialProfileCandidates: any[] = [];
  skillList = [];

  pageLoadingSubscription: Subscription;

  messageCode: MessageCode;
  responseData: ResponseData;

  featureDataMap:Map<any,any>;

  socialCandidateRequestJSON = {
    J2C: {
      company_id: null,
      designation: null,
      job_title: null,
      work_ex_start: null,
      work_ex_end: null,
      size: null,
      from: null,
      shortlistedCandidate: [],
      location: null,
      skill_array: null,
      type: "1"
    },
    Filter: {
      include: {

      },
      exclude: {

      }
    }
  };


  constructor(
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private newCandidateService: NewCandidateService,
    private notificationService: NotificationService,
    private autoMatchSocialPagedService: AutoMatchSocialPagedService,
    private nameinitialService: NameinitialService,
    private commonService: CommonService,
    private sessionService: SessionService,
    private router: Router,
    private jobCodeFormatter: FormatJobcodeParamPipe,
    private jobDetailService: JobDetailService,
    private dialogService: DialogService,
    private autoatchEmailService: AutoatchEmailService) {

    this.featureDataMap = this.sessionService.featureDataMap;
    this.automatchEnabled = this.sessionService.isAutoMatchEnabled();
    this.randomcolor = nameinitialService.randomcolor;
    this.requisitionId = this.autoMatchCandidatesService.requisitionId;
    this.requisitionCode = this.autoMatchCandidatesService.requisitionCode;
    this.socialPageState.pageSize = 15;
    let stateJSON = this.autoMatchSocialPagedService.getPageState();
    if (this.sessionService.featureDataMap.get("AUTOMATCH_SOCIALPROFILE") == 2) {
      this.commonService.disableRhsMonster();
    }
    else {
      this.commonService.enableRhsMonster();
    }
  }

  hideContextMenu(item) {
    item.displayClass = "displayNone";
  }

  showContextMenu(item) {
    item.displayClass = "displayBlock";
  }

  ngOnInit() {
    this.socialMailLoader = 'loader';
    this.commonService.showRHS();
    this.commonService.setSideBar('autoMatchSocial');
    // this.autoMatchSocialPagedService.getClearFilters().next(true);
    this.requisitionId = this.autoMatchCandidatesService.requisitionId;
    this.requisitionCode = this.autoMatchCandidatesService.requisitionCode;

    let lastRequestJSON = this.autoMatchSocialPagedService.getLastRequestJSON();
    if (lastRequestJSON != null) {
      let includeJSON = lastRequestJSON.Filter.include;
      this.socialCandidateRequestJSON.Filter.include = includeJSON;
    }


    this.skillList = this.sessionService.jobSkills;

    if (this.skillList == null || this.skillList.length == 0) {
      Promise.resolve(this.getJobSkills()).then((result) => {
        Promise.resolve(this.autoMatchCandidatesService.getSocialCandidateIds({ requisitionId: this.requisitionId })).then((response) => {
          this.processSocialCandidateResponse(response);
          this.loadSocialData();
        });

      });
    } else {
      Promise.resolve(this.autoMatchCandidatesService.getSocialCandidateIds({ requisitionId: this.requisitionId })).then((response) => {
        this.processSocialCandidateResponse(response);
        this.loadSocialData();
      });
    }

    this.autoMatchSocialPagedService.getFilterApplied().subscribe((filterApplied) => {
      let filterJSON = this.socialCandidateRequestJSON.Filter;

      if (filterApplied) {
        let filterType = filterApplied.type;
        if (filterType == 'contact') {
          this.applyContactFilter(filterApplied);
        } else if (filterType == 'experience') {
          this.applyExperienceFilter(filterApplied);
        } else {
          this.applyFilter(filterApplied);
        }
      }

      this.autoMatchSocialPagedService.clearCache();
      this.autoMatchSocialPagedService.savePageState(null);
      this.socialPageState.currentPageNumber = 1;
      this.loadSocialData();
    });
  }

  ngOnDestroy() {
    this.commonService.setSideBar('default');
  }

  applyExperienceFilter(filterApplied: any) {
    let action = filterApplied.action;
    let event = filterApplied.event;
    let filterType = filterApplied.type;
    let filterValue = typeof event == 'string' ? event : event.target.value;

    let start = 0;
    let end = 999999;

    if (filterValue.indexOf('-') > 0) {
      start = parseInt(filterValue.split("-")[0]);
      end = parseInt(filterValue.split("-")[1]);
    }
    if (filterValue.indexOf('+') > 0) {
      start = parseInt(filterValue.split("+")[0]);
    }


    let experienceParams = [];

    if (this.socialCandidateRequestJSON.Filter.include[filterType]) {
      experienceParams = this.socialCandidateRequestJSON.Filter.include[filterType];
    }

    // first remove filter from other
    for (let experienceParam of experienceParams) {
      if (experienceParam['start'] == start) {
        experienceParams.splice(experienceParams.indexOf(experienceParam), 1);
        break;
      }
    }

    if (action == 'add') {
      experienceParams.push({ start: start, end: end });
    }

    // third remove "experience" from Filter.include if it is empty coz then j2c web service throws error
    if (experienceParams.length == 0) {
      delete this.socialCandidateRequestJSON.Filter.include[filterType];
    } else {
      this.socialCandidateRequestJSON.Filter.include[filterType] = experienceParams;
    }
  }

  applyContactFilter(filterApplied: any) {

    let action = filterApplied.action;
    let event = filterApplied.event;
    let filterType = filterApplied.type;
    let filterValue = typeof event == 'string' ? event : event.target.value;

    delete this.socialCandidateRequestJSON.Filter.include[filterType];

    if (action == 'add') {
      this.socialCandidateRequestJSON.Filter.include['contact'] = 1;
    }
  }

  applyFilter(filterApplied: any) {

    let action = filterApplied.action;
    let event = filterApplied.event;
    let filterType = filterApplied.type;
    let filterValue = typeof event == 'string' ? event : event.target.value;

    let filterParams = [];

    if (this.socialCandidateRequestJSON.Filter.include[filterType]) {
      filterParams = this.socialCandidateRequestJSON.Filter.include[filterType];
    }

    // first remove filter from other
    for (let filterParam of filterParams) {
      if (filterParam['name'] == filterValue) {
        filterParams.splice(filterParams.indexOf(filterParam), 1);
        break;
      }
    }
    // second add required filter only in case of add action
    if (action == 'add') {
      filterParams.push({ 'name': filterValue });
    }

    // third remove "filterType" from Filter.include if it is empty coz then j2c web service throws error
    if (filterParams.length == 0) {
      delete this.socialCandidateRequestJSON.Filter.include[filterType];
    } else {
      this.socialCandidateRequestJSON.Filter.include[filterType] = filterParams;
    }
  }

  processSocialCandidateResponse(response) {
    if (response) {
      if (response.responseData) {
        if (response.responseData.length > 0) {
          if (response.responseData[0].socialUids) {
            if (response.responseData[0].socialUids.length > 0) {
              this.socialCandidateRequestJSON.J2C.shortlistedCandidate = response.responseData[0].socialUids;
            }
          }
        }
      }
    }
  }
  getJobSkills(): Promise<any> {
    if(this.featureDataMap.get("AUTOMATCH_SOCIALPROFILE") == 1){
    this.commonService.enableRhsMonster();
    this.skillList = this.sessionService.jobSkills;

    if (this.skillList == null || this.skillList.length == 0) {

      return Promise.resolve(this.jobDetailService.getRequiredSkills(this.requisitionCode).toPromise()).then((skills) => {
        this.commonService.enableRhsMonster();
        if (skills) {
          if (skills.requiredSkills) {
            this.sessionService.jobSkills = skills.requiredSkills;
            this.skillList = skills.requiredSkills;
          }
        }
      });

    } else {

      this.sessionService.jobSkills = this.skillList;

      return new Promise(() => {
        return true;
      });
    }
  }
  }

  loadSocialData() {

    this.socialPageLoading = true;


    let stateJSON = this.autoMatchSocialPagedService.getPageState();
    if (stateJSON != null && stateJSON.pageState != null) {
      this.socialPageState.currentPageNumber = stateJSON.pageState.currentPageNumber;
      this.socialPageState.pageSize = stateJSON.pageState.pageSize;
      this.socialPageState.totalRows = stateJSON.pageState.totalRows;
    }

    if (this.requisitionId != null && this.requisitionId > 0) {
      this.initSocialData();
    } else {
      this.jobDetailService.getRequisitionIDByCode(this.requisitionCode).subscribe((out) => {
        this.requisitionId = out;
        this.autoMatchCandidatesService.requisitionId = this.requisitionId;
        this.initSocialData();
      });
    }
  }

  includesIgnoreCase(list: any[], str): boolean {
    if (list != null && str != null) {
      if (list.length > 0) {
        for (let x of list) {
          if (str.toString().trim().toLowerCase() == x.text.toString().trim().toLowerCase()) {
            return true;
          }
        }
      }
    }
    return false;
  }

  initSocialData() {
    if (this.jobDetailsForSocialMatch == null) {

      let requestJSON = {
        requisitionId: this.requisitionId
      }

      Promise.resolve(this.autoMatchCandidatesService.getJobDetailsForSocialMatch(requestJSON)).then((response) => {
        if (response) {
          if (response.response) {
            this.jobDetailsForSocialMatch = response.response;
            this.socialCandidateRequestJSON.J2C.company_id = this.jobDetailsForSocialMatch.organizationId;
            this.socialCandidateRequestJSON.J2C.designation = this.jobDetailsForSocialMatch.designation;
            this.socialCandidateRequestJSON.J2C.job_title = this.jobDetailsForSocialMatch.jobTitle == null ? "" : this.jobDetailsForSocialMatch.jobTitle;
            this.socialCandidateRequestJSON.J2C.work_ex_start = this.jobDetailsForSocialMatch.workExStart;
            this.socialCandidateRequestJSON.J2C.work_ex_end = this.jobDetailsForSocialMatch.workExEnd;
            this.socialCandidateRequestJSON.J2C.location = this.jobDetailsForSocialMatch.location;
            this.socialCandidateRequestJSON.J2C.skill_array = this.jobDetailsForSocialMatch.skills;
          }
        }
        this.getSocialData();

      });
    } else {
      this.getSocialData();
    }
  }

  getSocialPageNumber(event) {
    this.getSocialData();
  }

  saveAsDraft(candidate) {

    candidate.saving = true;
    candidate.saved = false;

    let requestJSON = SocialUtil.setCandidateDetailsRequestJSON(undefined, candidate, this.requisitionId);

    let subscription = this.newCandidateService.saveCandidateDetails(requestJSON.candidateId, requestJSON, true, undefined).subscribe(response => {
      if (response != null && response != undefined) {
        if (response.messageCode.code == 'EC200') {
          candidate.saved = true;
          candidate.candidateCode = response.response.candidateCode;
          this.notificationService.setMessage(NotificationSeverity.INFO, "Draft Saved Successfully: " + candidate.candidateCode);
        } else if (response.messageCode.code == 'EC201') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else if (response.messageCode.code == 'EC202') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Candidate already exists in the database");
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Error Occured while saving ! Please try again later");

        }
      } else {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not connect to the server. Please try again.");
      }
      candidate.saving = false;
    },
      () => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not perform save request. Please try again");
        candidate.saving = false;
      });

  }

  getSocialData() {

    this.socialPageLoading = true;

    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.autoMatchSocialPagedService.getPage(this.socialCandidateRequestJSON, this.socialPageState.currentPageNumber, this.socialPageState.pageSize).subscribe((response) => {

      if (response) {
        this.socialProfileCandidates = response;
        this.areAllSocialSelected();
        this.socialPageState.totalRows = this.autoMatchSocialPagedService.getTotalCandidates();
      }

      this.socialPageLoading = false;

      if (this.socialPageState.currentPageNumber == 1) {
        this.autoMatchSocialPagedService.getPage(this.socialCandidateRequestJSON, this.socialPageState.currentPageNumber + 1, this.socialPageState.pageSize).subscribe((response) => {

          if (response) {
          }
        });


        this.autoMatchSocialPagedService.getPage(this.socialCandidateRequestJSON, this.socialPageState.currentPageNumber + 2, this.socialPageState.pageSize).subscribe((response) => {

          if (response) {
          }
        });
      }
    },
      (error) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not load data from server. Please Try Again.");
        this.socialPageLoading = false;
      },
      () => {
        this.socialPageLoading = false;
      });


  }

  getSocialCandidateName(candidate) {
    return SocialUtil.getName(candidate);
  }

  getSocialCandidateFullName(candidate) {
    return SocialUtil.getFullName(candidate);
  }

  getSocialDesignation(candidate) {
    let designation = "";
    if (candidate._source) {
      if (candidate._source.experiences) {
        if (candidate._source.experiences.length > 0) {
          for (let experience of candidate._source.experiences) {
            if (experience.current == true) {
              return experience.title
            }
            designation = experience.title;
          }
        }
      }
    }
    return designation;
  }


  getProfileMatch(item) {
    var suitable_match = 0;

    if (item != null) {

      suitable_match = item.match_score * 100;
    }
    return Math.round(suitable_match);
  }


  getProfileMatchAbsoulte(item) {
    var profile_match_absoulte = 0;

    if (item != null) {

      profile_match_absoulte = item.match_score_absolute * 100;
    }

    return Math.round(profile_match_absoulte);
  }


  getEmail(candidate) {
    return SocialUtil.getEmail(candidate);
  }

  isEmail(candidate): boolean {
    if (candidate._source) {
      if (candidate._source.email) {
        if (candidate._source.email.length > 0) {
          return true;
        }
      }
    }
    return false;
  }

  getMobile(candidate) {
    return SocialUtil.getAllPhoneNumbers(candidate);
  }

  isMobile(candidate): boolean {
    if (candidate._source) {
      if (candidate._source.mobile) {
        if (candidate._source.mobile.length > 0) {
          return true;
        }
      }
    }
    return false;
  }

  getPhoneNumber(candidate) {
    let phoneNumber = "";
    if (candidate._source) {
      if (candidate._source.phone_numbers) {
        if (candidate._source.phone_numbers.length > 0) {
          for (let phone of candidate._source.phone_numbers) {
            if (phone.phoneNumber) {
              if (phone.phoneNumber != null && phone.phoneNumber.length > 0) {
                phoneNumber = phone.phoneNumber;
                break;
              }
            }
          }
        }
      }
    }
    return phoneNumber;
  }

  isPhoneNumber(candidate): boolean {
    if (candidate._source) {
      if (candidate._source.phone_numbers) {
        if (candidate._source.phone_numbers.length > 0) {
          for (let phone of candidate._source.phone_numbers) {
            if (phone.phoneNumber) {
              if (phone.phoneNumber != null && phone.phoneNumber.length > 0) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  getSocialDesignationTT(candidate) {
    let designation = this.getSocialDesignation(candidate);
    if (designation.length > 29) {
      return designation;
    }
    return "";
  }

  getSocialCompany(candidate) {
    let company = "";
    if (candidate._source) {
      if (candidate._source.experiences) {
        if (candidate._source.experiences.length > 0) {
          for (let experience of candidate._source.experiences) {
            if (experience.current == true) {
              return experience.company.name
            }
            company = experience.company.name;
          }
        }
      }
    }
    return company;
  }

  getSocialCompanyTT(candidate) {
    let company = this.getSocialCompany(candidate);
    if (company.length > 29) {
      return company;
    }
    return "";
  }

  getSocialSkills(candidate) {

    let array: string[] = [];
    let array2: string[] = [];

    let skills: string[] = [];

    if (candidate._source) {
      if (candidate._source.skill_tagged) {
        if (candidate._source.skill_tagged.length > 0) {
          for (let skill of candidate._source.skill_tagged) {

            if (skill.name.toString().trim() != '' && this.includesIgnoreCase(this.skillList, skill.name)) {
              array.push(skill.name);
            } else {
              array2.push(skill.name)
            }
          }
        }
      }
      if (candidate._source.small_score_skill_tagged) {
        if (candidate._source.small_score_skill_tagged.length > 0) {

          for (let skill of candidate._source.small_score_skill_tagged) {
            if (skill.name.toString().trim() != '' && this.includesIgnoreCase(this.skillList, skill.name)) {
              array.push(skill.name);
            } else {
              array2.push(skill.name)
            }
          }
        }
      }
    }

    for (let x of array2) {
      array.push(x);
    }
    return array;
  }

  getSocialLocation(candidate) {
    let location = "";
    if (candidate._source) {
      if (candidate._source.location) {
        if (candidate._source.location.name) {
          location = candidate._source.location.name;
        }
      }
    }
    return location;
  }

  getSocialLocationTT(candidate) {
    let location = this.getSocialLocation(candidate);
    if (location.length > 29) {
      return location;
    }
    return "";
  }

  imageError(event, candidate) {
    candidate._source.linkedin.profile_pic = null;
  }

  isProfilePic(candidate) {
    return SocialUtil.isProfilePic(candidate);
  }
  getSocialProfilePic(candidate) {
    return SocialUtil.getSocialProfilePic(candidate);
  }

  showSocialLinks(candidate) {
    candidate.socialLinksClass = "displayBlock";
  }

  hideSocialLinks(candidate) {
    candidate.socialLinksClass = "displayNone";
  }

  gotoCandidateDetail(candidate, index: number) {

    this.sessionService.candidateInfo = candidate;
    this.sessionService.requisitionId = this.requisitionId;
    this.autoMatchSocialPagedService.match_score = candidate.match_score;
    this.autoMatchSocialPagedService.currentPosition = ((this.socialPageState.currentPageNumber - 1) * this.socialPageState.pageSize) + index + 1;

    let stateJSON = {
      scrollTop: document.querySelector('body').scrollTop,
      pageState: this.socialPageState
    }
    this.autoMatchSocialPagedService.savePageState(stateJSON);
    this.router.navigateByUrl('/job/auto-match-candidates/' + this.jobCodeFormatter.transform(this.requisitionCode) + '/social/candidate/' + candidate._id);
  }

  getFirstThreeSocialLinks(candidate) {
    return SocialUtil.getFirstThreeSocialLinks(candidate);
  }

  getAfterFirstThreeSocialLinks(candidate) {
    return SocialUtil.getAfterFirstThreeSocialLinks(candidate);
  }

  areAllSocialSelected() {
    let allSelected = true;
    for (let candidate of this.socialProfileCandidates) {
      if (candidate.selected == null || candidate.selected == false) {
        candidate.selected = false;
        allSelected = false;
      }
    }
    if (allSelected) {
      this.allSocialSelected = true;
    } else {
      this.allSocialSelected = false;
    }
  }
  singleSelectSocialEvent(event: any, index: number) {
    if (!event.selected) {
      let index = this.socialSelected.indexOf(event);
      this.socialSelected.splice(index, 1);
    } else {
      this.socialSelected.push(event);
    }
    this.autoMatchSocialPagedService.syncCandidate(this.socialPageState.currentPageNumber, this.socialPageState.pageSize, index, event);
    this.areAllSocialSelected();
  }

  selectAllSocial() {
    this.allSocialSelected = !this.allSocialSelected;
    let i = 0;
    for (let candidate of this.socialProfileCandidates) {
      if (this.allSocialSelected) {
        if (!this.socialSelected.includes(candidate)) {
          this.socialSelected.push(candidate);
        }
      } else {
        if (this.socialSelected.includes(candidate)) {
          this.socialSelected.splice(this.socialSelected.indexOf(candidate), 1);
        }
      }
      candidate.selected = this.allSocialSelected;
      this.autoMatchSocialPagedService.syncCandidate(this.socialPageState.currentPageNumber, this.socialPageState.pageSize, i, candidate);
      i++;
    }
  }
  getCurrentFilters() {
    let lastRequestJSON = this.socialCandidateRequestJSON;
    if (lastRequestJSON) {
      this.currentRequestJSON = lastRequestJSON.Filter.include;
      if (Object.keys(this.currentRequestJSON).length !== 0)
        this.clearall = true;
      else
        this.clearall = false;
    }
    return this.currentRequestJSON;
  }
  convertExp(start: number, end: number): string {
    let exp = "";
    if (start >= 0 && end <= 365)
      exp = "less than 1 years";
    else if (start >= 365 && end <= 1095)
      exp = "1 to 3 years";
    else if (start >= 1095 && end <= 2190)
      exp = "3 to 6 years";
    else if (start >= 2190 && end <= 3650)
      exp = "6 to 10 years";
    else if (start >= 3650)
      exp = "more than 10 years";
    return exp;
  }
  clearAll() {
    this.socialCandidateRequestJSON.Filter.include = {};
    this.autoMatchSocialPagedService.clearCache();
    this.socialPageState.currentPageNumber = 1;
    this.loadSocialData();
  }
  removeFilter(event, filterType: string) {
    this.autoMatchSocialPagedService.getFilterApplied().next({
      'action': 'remove',
      'type': filterType,
      'event': event
    });
  }
  sendEmail(candidate) {
    this.socialMailLoader = 'nodata';
    this.sessionService.jobSummaryData.candidateName = SocialUtil.getName(candidate);
    this.sessionService.jobSummaryData.emailId = SocialUtil.getEmail(candidate);
    this.sessionService.jobSummaryData.socialUid = candidate._id;
    this.autoatchEmailService.getAutomatchEmailTemplateAll(this.jobDetailsForSocialMatch, candidate._id).subscribe(out => {
      if (out.body != null) {
        this.sessionService.jobSummaryData.subject = out.subject;
        this.sessionService.jobSummaryData.content = out.body;
        this.socialMailLoader = 'data';
      } else {
        this.socialMailLoader = 'nodata';
      }
      this.dialogService.open(AutomatchEmailComponent, { width: '935px' });
    });
  }
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  subject: String;
  content: String;
}