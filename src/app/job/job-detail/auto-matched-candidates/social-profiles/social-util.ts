import { SkillTO } from "app/common/SkillTO.model";
import { SaveCandidateDetailsTO } from "app/common/SaveCandidateDetailsTO.model";
import { WorkExpTO } from "app/common/WorkExpTO.model";
import { EducationTO } from "app/common/EducationTO.model";
import { CandidateAddressTO } from "app/common/CandidateAddressTO.model";

export default class SocialUtil {

    public static SOCIAL_STARS_DIVIDER = 12;

    static getFirstThreeSocialLinks(candidate) {

        let linksList = SocialUtil.getSocialLinksList(candidate);

        if (linksList != null && linksList.length > 3) {
            return linksList.slice(0, 3);
        } else {
            return linksList;
        }
    }

    static getAfterFirstThreeSocialLinks(candidate) {

        let linksList = SocialUtil.getSocialLinksList(candidate);

        if (linksList != null && linksList.length > 3) {
            return linksList.slice(3, linksList.length);
        } else {
            return [];
        }
    }

    static getSocialLinksList(candidate): any[] {
        if (candidate.socialLinks) {
            return candidate.socialLinks;
        }

        let linksList = [];

        if (candidate._source) {
            // linkedin
            if (candidate._source.linkedin) {
                let url = "";
                if (candidate._source.linkedin.url) {
                    url = candidate._source.linkedin.url;
                }
                if (candidate._source.linkedin.profile_url) {
                    url = candidate._source.linkedin.profile_url;
                }
                if (url != "") {
                    linksList.push({
                        icon: "linkedin",
                        url: url,
                        name: "LinkedIn"
                    });
                }

            }

            // facebook

            if (candidate._source.facebook) {
                let url = "";
                if (candidate._source.facebook.url) {
                    url = candidate._source.facebook.url;
                }
                linksList.push({
                    icon: "facebook",
                    url: url,
                    name: "Facebook"
                });
            }

            // twitter
            if (candidate._source.twitter) {
                let url = "";
                if (candidate._source.twitter.url) {
                    url = candidate._source.twitter.url;
                }
                linksList.push({
                    icon: "twitter",
                    url: url,
                    name: "Twitter"
                });
            }

            // dribbble
            if (candidate._source.dribbble) {
                let url = "";
                if (candidate._source.dribbble.url) {
                    url = candidate._source.dribbble.url;
                }
                linksList.push({
                    icon: "dribbble",
                    url: url,
                    name: "Dribbble"
                });
            }

            // behance
            if (candidate._source.behance) {
                let url = "";
                if (candidate._source.behance.url) {
                    url = candidate._source.behance.url;
                }
                linksList.push({
                    icon: "behance",
                    url: url,
                    name: "Behance"
                });
            }

            // aboutme
            if (candidate._source.aboutme) {
                let url = "";
                if (candidate._source.aboutme.url) {
                    url = candidate._source.aboutme.url;
                }
                linksList.push({
                    icon: "aboutme",
                    url: url,
                    name: "AboutMe"
                });
            }

            // angelco
            if (candidate._source.angelco) {
                let url = "";
                if (candidate._source.angelco.url) {
                    url = candidate._source.angelco.url;
                }
                linksList.push({
                    icon: "angelco",
                    url: url,
                    name: "AngleCo"
                });
            }

            // gravatar
            if (candidate._source.gravatar) {
                let url = "";
                if (candidate._source.gravatar.url) {
                    url = candidate._source.gravatar.url;
                }
                linksList.push({
                    icon: "gravatar",
                    url: url,
                    name: "Gravatar"
                });
            }

            // github
            if (candidate._source.github) {
                let url = "";
                if (candidate._source.github.url) {
                    url = candidate._source.github.url;
                }
                linksList.push({
                    icon: "github",
                    url: url,
                    name: "github"
                });
            }


            // quora
            if (candidate._source.quora) {
                let url = "";
                if (candidate._source.quora.url) {
                    url = candidate._source.quora.url;
                }
                linksList.push({
                    icon: "quora",
                    url: url,
                    name: "Quora"
                });
            }

            // slideshare
            if (candidate._source.slideshare) {
                let url = "";
                if (candidate._source.slideshare.url) {
                    url = candidate._source.slideshare.url;
                }
                linksList.push({
                    icon: "slideshare",
                    url: url,
                    name: "Slideshare"
                });
            }

            // stackoverflow
            if (candidate._source.stackoverflow) {
                let url = "";
                if (candidate._source.stackoverflow.url) {
                    url = candidate._source.stackoverflow.url;
                }
                linksList.push({
                    icon: "Stack Overflow",
                    url: url,
                    name: "Stack Overflow"
                });
            }

            // soundcloud
            if (candidate._source.soundcloud) {
                let url = "";
                if (candidate._source.soundcloud.url) {
                    url = candidate._source.soundcloud.url;
                }
                linksList.push({
                    icon: "soundcloud",
                    url: url,
                    name: "SoundCloud"
                });
            }

            // medium
            if (candidate._source.medium) {
                let url = "";
                if (candidate._source.medium.url) {
                    url = candidate._source.medium.url;
                }
                linksList.push({
                    icon: "medium",
                    url: url,
                    name: "Medium"
                });
            }

            // pinterest
            if (candidate._source.pinterest) {
                let url = "";
                if (candidate._source.pinterest.url) {
                    url = candidate._source.pinterest.url;
                }
                linksList.push({
                    icon: "pinterest",
                    url: url,
                    name: "Pinterest"
                });
            }

            // askfm
            if (candidate._source.askfm) {
                let url = "";
                if (candidate._source.askfm.url) {
                    url = candidate._source.askfm.url;
                }
                linksList.push({
                    icon: "askfm",
                    url: url,
                    name: "Askfm"
                });
            }


            // blogger
            if (candidate._source.blogger) {
                let url = "";
                if (candidate._source.blogger.url) {
                    url = candidate._source.blogger.url;
                }
                linksList.push({
                    icon: "blogger",
                    url: url,
                    name: "Blogger"
                });
            }

            // brandyourself
            if (candidate._source.brandyourself) {
                let url = "";
                if (candidate._source.brandyourself.url) {
                    url = candidate._source.brandyourself.url;
                }
                linksList.push({
                    icon: "brandyourself",
                    url: url,
                    name: "Brand Yourself"
                });
            }

            // flavoursme
            if (candidate._source.flavoursme) {
                let url = "";
                if (candidate._source.flavoursme.url) {
                    url = candidate._source.flavoursme.url;
                }
                linksList.push({
                    icon: "flavoursme",
                    url: url,
                    name: "FlavoursMe"
                });
            }

            // flickr
            if (candidate._source.flickr) {
                let url = "";
                if (candidate._source.flickr.url) {
                    url = candidate._source.flickr.url;
                }
                linksList.push({
                    icon: "flickr",
                    url: url,
                    name: "Flickr"
                });
            }

            // foursquare
            if (candidate._source.foursquare) {
                let url = "";
                if (candidate._source.foursquare.url) {
                    url = candidate._source.foursquare.url;
                }
                linksList.push({
                    icon: "foursquare",
                    url: url,
                    name: "FourSquare"
                });
            }


            // goodread
            if (candidate._source.goodread) {
                let url = "";
                if (candidate._source.goodread.url) {
                    url = candidate._source.goodread.url;
                }
                linksList.push({
                    icon: "goodread",
                    url: url,
                    name: "goodread"
                });
            }


            // googleplus
            if (candidate._source.googleplus) {
                let url = "";
                if (candidate._source.googleplus.url) {
                    url = candidate._source.googleplus.url;
                }
                linksList.push({
                    icon: "googleplus",
                    url: url,
                    name: "Google+"
                });
            }


            // instagram
            if (candidate._source.instagram) {
                let url = "";
                if (candidate._source.instagram.url) {
                    url = candidate._source.instagram.url;
                }
                linksList.push({
                    icon: "instagram",
                    url: url,
                    name: "Instagram"
                });
            }


            // lastfm
            if (candidate._source.lastfm) {
                let url = "";
                if (candidate._source.lastfm.url) {
                    url = candidate._source.lastfm.url;
                }
                linksList.push({
                    icon: "lastfm",
                    url: url,
                    name: "Lastfm"
                });
            }


            // meetup
            if (candidate._source.meetup) {
                let url = "";
                if (candidate._source.meetup.url) {
                    url = candidate._source.meetup.url;
                }
                linksList.push({
                    icon: "meetup",
                    url: url,
                    name: "Meetup"
                });
            }


            // myspace
            if (candidate._source.myspace) {
                let url = "";
                if (candidate._source.myspace.url) {
                    url = candidate._source.myspace.url;
                }
                linksList.push({
                    icon: "myspace",
                    url: url,
                    name: "MySpace"
                });
            }


            // rdio
            if (candidate._source.rdio) {
                let url = "";
                if (candidate._source.rdio.url) {
                    url = candidate._source.rdio.url;
                }
                linksList.push({
                    icon: "rdio",
                    url: url,
                    name: "Rdio"
                });
            }


            // spotify
            if (candidate._source.spotify) {
                let url = "";
                if (candidate._source.spotify.url) {
                    url = candidate._source.spotify.url;
                }
                linksList.push({
                    icon: "spotify",
                    url: url,
                    name: "Spotify"
                });
            }


            // springme
            if (candidate._source.springme) {
                let url = "";
                if (candidate._source.springme.url) {
                    url = candidate._source.springme.url;
                }
                linksList.push({
                    icon: "springme",
                    url: url,
                    name: "Spring Me"
                });
            }

            // squidoo
            if (candidate._source.squidoo) {
                let url = "";
                if (candidate._source.squidoo.url) {
                    url = candidate._source.squidoo.url;
                }
                linksList.push({
                    icon: "squidoo",
                    url: url,
                    name: "Squidoo"
                });
            }


            // tumblr
            if (candidate._source.tumblr) {
                let url = "";
                if (candidate._source.tumblr.url) {
                    url = candidate._source.tumblr.url;
                }
                linksList.push({
                    icon: "tumblr",
                    url: url,
                    name: "Tumblr"
                });
            }


            // vimeo
            if (candidate._source.vimeo) {
                let url = "";
                if (candidate._source.vimeo.url) {
                    url = candidate._source.vimeo.url;
                }
                linksList.push({
                    icon: "vimeo",
                    url: url,
                    name: "Vimeo"
                });
            }

            // viadeo
            if (candidate._source.viadeo) {
                let url = "";
                if (candidate._source.viadeo.url) {
                    url = candidate._source.viadeo.url;
                }
                linksList.push({
                    icon: "viadeo",
                    url: url,
                    name: "Viadeo"
                });
            }

            // vk
            if (candidate._source.vk) {
                let url = "";
                if (candidate._source.vk.url) {
                    url = candidate._source.vk.url;
                }
                linksList.push({
                    icon: "vk",
                    url: url,
                    name: "Vk"
                });
            }


            // wordpress
            if (candidate._source.wordpress) {
                let url = "";
                if (candidate._source.wordpress.url) {
                    url = candidate._source.wordpress.url;
                }
                linksList.push({
                    icon: "wordpress",
                    url: url,
                    name: "WordPress"
                });
            }

            // yelp
            if (candidate._source.yelp) {
                let url = "";
                if (candidate._source.yelp.url) {
                    url = candidate._source.yelp.url;
                }
                linksList.push({
                    icon: "yelp",
                    url: url,
                    name: "Yelp"
                });
            }


            // youtube
            if (candidate._source.youtube) {
                let url = "";
                if (candidate._source.youtube.url) {
                    url = candidate._source.youtube.url;
                }
                linksList.push({
                    icon: "youtube",
                    url: url,
                    name: "YouTube"
                });
            }

            // zerply
            if (candidate._source.zerply) {
                let url = "";
                if (candidate._source.zerply.url) {
                    url = candidate._source.zerply.url;
                }
                linksList.push({
                    icon: "zerply",
                    url: url,
                    name: "Zerply"
                });
            }
        }

        candidate.socialLinks = linksList;
        return linksList;
    }


    static getSocialProfilePic(candidate) {
        let url: string = null;
        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.profile_pic) {
                    url = candidate._source.linkedin.profile_pic.toString();
                    if (url != null && url.indexOf("http://") > 0) {
                        url = url.replace("http://", "https://");
                    }
                }
            }
        }
        return url;
    }

    static isProfilePic(candidate) {
        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.profile_pic != null && candidate._source.linkedin.profile_pic != '') {
                    return true;
                }
            }
        }
        return false;
    }

    static getEmail(candidate) {
        let email = "";
        if (candidate._source) {
            if (candidate._source.email) {
                email = candidate._source.email;
            }
        }
        return email;
    }

    static getAllPhoneNumbers(candidate) {
        let phoneNumbers = "";
        let mobileNumbers = "";
        if (candidate._source) {

            if (candidate._source.phone_numbers) {
                if (candidate._source.phone_numbers.length > 0) {
                    for (let x = 0; x < candidate._source.phone_numbers.length; x++) {
                        phoneNumbers += candidate._source.phone_numbers[x].phoneNumber;
                        if (x != candidate._source.phone_numbers.length - 1) {
                            phoneNumbers += ", ";
                        }
                    }
                }
            }

            if (candidate._source.mobile) {
                if (candidate._source.mobile.length > 0) {
                    for (let x = 0; x < candidate._source.mobile.length; x++) {
                        mobileNumbers += candidate._source.mobile[x];
                        if (x != candidate._source.mobile.length - 1) {
                            mobileNumbers += ", ";
                        }
                    }
                }
            }

            if (mobileNumbers.length > 5) {
                if (phoneNumbers.length < 5) {
                    return mobileNumbers;
                }
                return phoneNumbers + ", " + mobileNumbers;
            }

        }
        return phoneNumbers;
    }

    static getPhoneNumber(candidate) {
        let phoneNumber = "";
        if (candidate._source) {
            if (candidate._source.phone_numbers) {
                if (candidate._source.phone_numbers.length > 0) {
                    phoneNumber = candidate._source.phone_numbers[0].phoneNumber;
                }
            }
        }
        return phoneNumber;
    }

    static getCandidateSkills(candidate) {
        let skills = [];
        if (candidate._source) {
            if (candidate._source.skill_tagged) {
                if (candidate._source.skill_tagged.length > 0) {

                    for (let skill of candidate._source.skill_tagged) {
                        skills.push({
                            skillName: skill.name,
                            score: Math.round((skill.score / SocialUtil.SOCIAL_STARS_DIVIDER) * 5)
                        });
                    }
                }
            }
            if (candidate._source.small_score_skill_tagged) {
                if (candidate._source.small_score_skill_tagged.length > 0) {

                    for (let skill of candidate._source.small_score_skill_tagged) {
                        skills.push({
                            skillName: skill.name,
                            score: Math.round((skill.score / SocialUtil.SOCIAL_STARS_DIVIDER) * 5)
                        });
                    }
                }
            }
        }

        let score = 0;

        for (let i = 0; i < skills.length - 1; i++) {
            for (let j = 0; j < skills.length - i - 1; j++) {
                if (skills[j].score > skills[j + 1].score) {
                    let temp = skills[j + 1];
                    skills[j + 1] = skills[j];
                    skills[j] = temp;
                }
            }
        }

        let newSkills = [];
        for (let x = skills.length - 1; x >= 0; x--) {
            newSkills.push(skills[x]);
        }

        return newSkills;
    }

    static getName(candidate) {
        let candidate_name = "";
        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.name) {
                    candidate_name = candidate._source.linkedin.name;
                }
            }
        }

        if (candidate_name.length > 20) {
            candidate_name = candidate_name.substr(0, 20);
        }
        return candidate_name;
    }
    static getFullName(candidate) {
        let candidate_name = "";
        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.name) {
                    candidate_name = candidate._source.linkedin.name;
                }
            }
        }

        return candidate_name;
    }

    static getGender(candidate) {
        return "";
    }

    static getDateOfBirth(candidate) {
        return "";
    }

    static getCountry(candidate) {
        let country = "";
        if (candidate) {
            if (candidate._source) {
                if (candidate._source.location) {
                    if (candidate._source.location.country) {
                        country += candidate._source.location.country;
                    }
                }
            }
        }
        return country;
    }

    static getState(candidate) {
        let state = "";
        if (candidate) {
            if (candidate._source) {
                if (candidate._source.location) {
                    if (candidate._source.location.state) {
                        state += candidate._source.location.state;
                    }
                }
            }
        }
        return state;
    }

    static getCity(candidate) {
        let city = "";
        if (candidate) {
            if (candidate._source) {
                if (candidate._source.location) {
                    if (candidate._source.location.name) {
                        city = candidate._source.location.name;
                    }
                }
            }
        }
        return city;
    }
    static getMobileNumber(candidate) {
        return ""
    }
    static getWorkRequestJSON(candidate) {

        if (candidate.workHistory != null) {
            return candidate.workHistory;
        }
        let workHistoryJSON = [];

        if (candidate) {
            if (candidate._source) {
                if (candidate._source.experiences) {
                    if (candidate._source.experiences.length > 0) {
                        for (let record of candidate._source.experiences) {


                            let company = "";
                            let jobTitle = "";
                            let companyStartDate = null;
                            let currentlyWorkHere = false;
                            let companyEndDate = null;
                            let ctcCurrency = "";
                            let ctcAmount = 0;
                            let ctcDenomination = "";
                            let description = "";

                            if (record.company) {
                                if (record.company.name) {
                                    company = record.company.name;
                                }
                            }

                            if (record.start_date) {
                                companyStartDate = new Date(record.start_date);
                            }

                            if (record.end_date) {
                                companyEndDate = new Date(record.end_date);
                            }

                            if (record.current) {
                                if (record.current == true) {
                                    currentlyWorkHere = true;
                                }
                            }

                            if (record.title) {
                                jobTitle = record.title;
                            }

                            if (record.description) {
                                description = record.description;
                            }

                            let targetPos = 0;

                            if (companyStartDate == null) {
                                targetPos = workHistoryJSON.length;
                            } else {
                                for (let work of workHistoryJSON) {
                                    if (work.startDate != null && companyStartDate != null && work.startDate > companyStartDate) {
                                        targetPos++;
                                    } else {
                                        break;
                                    }
                                }
                            }

                            let newWork = {
                                workDetailId: 0,
                                company: company,
                                jobTitle: jobTitle,
                                startDate: companyStartDate,
                                endDate: currentlyWorkHere ? null : companyEndDate,
                                currentlyWorkHere: currentlyWorkHere,
                                ctcCurrency: ctcCurrency,
                                ctcAmount: ctcAmount,
                                ctcDenomination: ctcDenomination,
                                description: description
                            }

                            workHistoryJSON.splice(targetPos, 0, newWork);


                        }
                    }
                }
            }
        }




        let workRequestJSON = {
            totalExperienceYears: 0,
            totalExperienceMonths: 0,
            noticePeriod: 0,
            employmentHistory: workHistoryJSON
        }

        candidate.workHistory = workRequestJSON;

        return workRequestJSON;
    }

    static getInternshipRequestJSON(candidate) {

        if (candidate.internshipHistory != null) {
            return candidate.internshipHistory;
        }
        let internshipHistoryJSON = [];

        if (candidate) {
            if (candidate._source) {
                if (candidate._source.internships) {
                    if (candidate._source.internships.length > 0) {
                        for (let record of candidate._source.internships) {


                            let company = "";
                            let jobTitle = "";
                            let companyStartDate = null;
                            let currentlyWorkHere = false;
                            let companyEndDate = null;
                            let ctcCurrency = "";
                            let ctcAmount = 0;
                            let ctcDenomination = "";
                            let description = "";

                            if (record.company) {
                                if (record.company.name) {
                                    company = record.company.name;
                                }
                            }

                            if (record.start_date) {
                                companyStartDate = new Date(record.start_date);
                            }

                            if (record.end_date) {
                                companyEndDate = new Date(record.end_date);
                            }

                            if (record.current) {
                                if (record.current == true) {
                                    currentlyWorkHere = true;
                                }
                            }

                            if (record.title) {
                                jobTitle = record.title;
                            }

                            if (record.description) {
                                description = record.description;
                            }


                            let targetPos = 0;

                            if (companyStartDate == null) {
                                targetPos = internshipHistoryJSON.length;
                            } else {
                                for (let work of internshipHistoryJSON) {
                                    if (work.startDate != null && companyStartDate != null && work.startDate > companyStartDate) {
                                        targetPos++;
                                    } else {
                                        break;
                                    }
                                }
                            }

                            let newInt = {
                                workDetailId: 0,
                                company: company,
                                jobTitle: jobTitle,
                                startDate: companyStartDate,
                                endDate: currentlyWorkHere ? null : companyEndDate,
                                currentlyWorkHere: currentlyWorkHere,
                                ctcCurrency: ctcCurrency,
                                ctcAmount: ctcAmount,
                                ctcDenomination: ctcDenomination,
                                description: description
                            }

                            internshipHistoryJSON.splice(targetPos, 0, newInt);
                        }
                    }
                }
            }
        }



        let internshipRequestJSON = {
            totalExperienceYears: 0,
            totalExperienceMonths: 0,
            noticePeriod: 0,
            employmentHistory: internshipHistoryJSON
        }


        candidate.internshipHistory = internshipRequestJSON;

        return internshipRequestJSON;
    }

    static getEducationRequestJSON(candidate) {

        if (candidate.educationHistory != null) {
            return candidate.educationHistory;
        }
        let educationHistoryJSON = [];
        if (candidate._source) {
            if (candidate._source.educations) {
                if (candidate._source.educations.length > 0) {

                    for (let record of candidate._source.educations) {
                        let degree = "";
                        let batchStart = null;
                        let batchEnd = null;
                        let institute = "";
                        let gradePercentage = "";

                        if (record.institute) {
                            if (record.institute.name) {
                                institute = record.institute.name;
                            }
                        }

                        if (record.degree) {
                            if (record.degree.name) {
                                degree = record.degree.name;
                            }
                        }

                        if (record.start_date) {
                            batchStart = new Date(record.start_date);
                        }

                        if (record.end_date) {
                            batchEnd = new Date(record.end_date);
                        }

                        let targetPos = 0;

                        if (batchStart == null) {
                            targetPos = educationHistoryJSON.length;
                        } else {
                            for (let edu of educationHistoryJSON) {
                                if (edu.batchStartDate != null && batchStart != null && edu.batchStartDate > batchStart) {
                                    targetPos++;
                                } else {
                                    break;
                                }
                            }
                        }

                        let newEdu = {
                            candidateEduDetailId: 0,
                            degree: degree,
                            batchStartDate: batchStart,
                            batchEndDate: batchEnd,
                            institute: institute,
                            gradePercentage: gradePercentage
                        }


                        educationHistoryJSON.splice(targetPos, 0, newEdu);
                    }
                }
            }
        }

        let educationRequestJSON = {
            educationHistory: educationHistoryJSON
        };

        candidate.educationHistory = educationRequestJSON;

        return educationRequestJSON;
    }

    static getLanguages(candidate) {

        if (candidate.languages != null) {
            return candidate.languages;
        }
        let languages = [];

        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.languages) {
                    if (candidate._source.linkedin.languages.length > 0) {
                        for (let language of candidate._source.linkedin.languages) {
                            languages.push(language);
                        }
                    }
                }
            }
        }

        candidate.languages = languages;

        return languages;
    }

    static getSkillRequestJSON(candidate) {
        let skillsJSON = [];
        if (candidate._source) {
            if (candidate._source.skill_tagged) {
                if (candidate._source.skill_tagged.length > 0) {
                    for (let record of candidate._source.skill_tagged) {
                        if (record.name) {
                            skillsJSON.push({
                                id: 0,
                                competency: record.name
                            });
                        }
                    }
                }
            }
        }


        let skillRequestJSON = {
            skills: skillsJSON
        }

        return skillRequestJSON;
    }

    static getLanguageRequestJSON(candidate) {
        let languageJSON = [];
        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.languages) {
                    if (candidate._source.linkedin.languages.length > 0) {
                        for (let record of candidate._source.linkedin.languages) {
                            languageJSON.push({
                                id: 0,
                                competency: record
                            });
                        }
                    }
                }
            }
        }


        let skillRequestJSON = {
            skills: languageJSON
        }

        return skillRequestJSON;
    }

    static getBasicRequestJSON(candidate, requisitionId) {


        let firstName = "";
        let middleName = "";
        let lastName = "";
        let mobileNumber = "";
        let primaryEmail = "";
        let secondaryEmail = "";
        let dateOfBirth = "";
        let gender = 0;
        let panNumber = "";
        let voterId = "";
        let passportNumber = "";
        let aadhaarNumber = "";
        let industry = 0;
        let functionalArea = 0;
        let sourceType = "SOCIAL MEDIA";
        let sourceTypeId = 0;
        let sourceCode = "";
        let sourceName = "";
        let sourceCodeId = 0;
        let sourceNameId = 0;
        let socialUID = null;
        let jobBoardName = 'SOCIAL MEDIA';
        let triggerPoint = 'SOCIAL New RP';


        if (candidate) {

            if (candidate._id) {
                socialUID = candidate._id;
            }

            if (candidate._source) {
                if (candidate._source.email) {
                    primaryEmail = candidate._source.email;
                }
                if (candidate._source.linkedin) {
                    if (candidate._source.linkedin.name) {
                        let regex = new RegExp('[^a-zA-Z]+', 'g')
                        let nameParts = candidate._source.linkedin.name.split(regex);
                        if (nameParts.length > 0) {
                            firstName = nameParts[0];
                        }

                        if (nameParts.length > 1) {
                            lastName = nameParts[nameParts.length - 1];

                            for (let i = 1; i < nameParts.length - 1; i++) {
                                middleName += nameParts[i];
                            }
                        }

                    }
                }
            }
        }

        let basicRequestJSON = {
            "requisitionId": requisitionId,
            "firstName": firstName,
            "middleName": middleName,
            "lastName": lastName,
            "mobileNumber": mobileNumber,
            "primaryEmail": primaryEmail,
            "secondaryEmail": secondaryEmail,
            "candidateAge": "",
            "dateOfBirth": dateOfBirth,
            "genderId": gender,
            "panNumber": panNumber,
            "voterId": voterId,
            "passportNumber": passportNumber,
            "aadhaarNumber": aadhaarNumber,
            "industryId": industry,
            "functionalAreaId": functionalArea,
            "sourceType": sourceType,
            "sourceTypeId": sourceTypeId,
            "sourceCode": sourceCode,
            "sourceCodeId": sourceCodeId,
            "sourceName": sourceName,
            "sourceNameId": sourceNameId,
            "userId": 0,
            "socialUID": socialUID,
            "triggerPoint": triggerPoint,
            "jobBoardName": jobBoardName
        };

        return basicRequestJSON;
    }

    static getPersonalRequestJSON(candidate) {

        let addressLine1 = "";
        let facebookProfile = "";
        let twitterProfile = "";
        let linkedInProfile = "";
        let googlePlusProfile = "";
        let pinCode = "";

        if (candidate) {
            if (candidate._source) {
                if (candidate._source.linkedin) {
                    if (candidate._source.linkedin.url) {
                        linkedInProfile = candidate._source.linkedin.url
                    }
                }

                if (candidate._source.googleplus) {
                    if (candidate._source.googleplus.url) {
                        googlePlusProfile = candidate._source.googleplus.url;
                    }
                }

                if (candidate._source.twitter) {
                    if (candidate._source.twitter.url) {
                        twitterProfile = candidate._source.twitter.url;
                    }
                }

                if (candidate._source.facebook) {
                    if (candidate._source.facebook.url) {
                        facebookProfile = candidate._source.facebook.url;
                    }
                }

                if (candidate._source.location) {
                    if (candidate._source.location.name) {
                        addressLine1 = candidate._source.location.name;
                    }
                }
            }
        }


        let personalRequestJSON = {
            "addressLine1": addressLine1,
            "facebookProfile": facebookProfile,
            "twitterProfile": twitterProfile,
            "linkedInProfile": linkedInProfile,
            "googlePlusProfile": googlePlusProfile,
            "pinCode": pinCode,
            "userId": 0
        }

        return personalRequestJSON;
    }

    static setCandidateDetailsRequestJSON(requestJSON, resumeData, requisitionID) {
        requestJSON = this.setBasicDetailsRequestJSON(requestJSON, resumeData, requisitionID);
        requestJSON = this.setPersonalDetailsRequestJSON(requestJSON, resumeData);
        requestJSON = this.setWorkDetailsRequestJSON(requestJSON, resumeData);
        requestJSON = this.setEducationDetailsRequestJSON(requestJSON, resumeData);
        requestJSON = this.setSkillDetailsRequestJSON(requestJSON, resumeData);
        requestJSON = this.setInternshipDetailsRequestJSON(requestJSON, resumeData);
        requestJSON = this.setLanguageDetailsRequestJSON(requestJSON, resumeData);
        return requestJSON;
    }

    static initRequestJSON(requestJSON: SaveCandidateDetailsTO) {
        if (requestJSON == null || requestJSON == undefined) {
            requestJSON = new SaveCandidateDetailsTO();
        }
        if (requestJSON.candidateAddressTO == null || requestJSON.candidateAddressTO == undefined) {
            requestJSON.candidateAddressTO = new CandidateAddressTO();
        }
        return requestJSON;
    }

    static setBasicDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any, requisitionID: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let socialUID: any;
        let primaryEmail: any;
        let firstName: any = "";
        let middleName: any = "";
        let lastName: any = "";

        if (candidate) {
            if (candidate._id) {
                socialUID = candidate._id;
            }
            if (candidate._source) {
                if (candidate._source.email) {
                    primaryEmail = candidate._source.email;
                }
                if (candidate._source.linkedin) {
                    if (candidate._source.linkedin.name) {
                        let regex = new RegExp('[^a-zA-Z]+', 'g')
                        let nameParts = candidate._source.linkedin.name.split(regex);
                        if (nameParts.length > 0) {
                            firstName = nameParts[0];
                        }
                        if (nameParts.length > 1) {
                            lastName = nameParts[nameParts.length - 1];
                            for (let i = 1; i < nameParts.length - 1; i++) {
                                middleName += nameParts[i];
                            }
                        }

                    }
                }
            }
        }

        requestJSON.candidateId = 0;
        requestJSON.requisitionID = requisitionID;
        if(firstName != undefined){
            requestJSON.firstName = firstName;
        }
        if(middleName != undefined){
            requestJSON.middleName = middleName;
        }
        if(lastName != undefined){
            requestJSON.lastName = lastName;
        }
        requestJSON.primaryEmail = primaryEmail;

        requestJSON.sourceTypeID = undefined;
        requestJSON.subSourceTypeID = undefined;
        requestJSON.subSourceUserID = undefined;
        requestJSON.subSourceTypeName = 'SOCIAL MEDIA';
        requestJSON.jobBoardName = 'SOCIAL MEDIA';
        requestJSON.triggerPoint = 'SOCIAL New RP';
        requestJSON.socialUID = socialUID;

        return requestJSON;
    }

    static setPersonalDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let addressLine1 = "";
        let faceBookProfile = "";
        let twitterProfile = "";
        let linkedInProfile = "";
        let googlePlusProfile = "";

        if (candidate) {
            if (candidate._source) {
                if (candidate._source.linkedin) {
                    if (candidate._source.linkedin.url) {
                        linkedInProfile = candidate._source.linkedin.url
                    }
                }

                if (candidate._source.googleplus) {
                    if (candidate._source.googleplus.url) {
                        googlePlusProfile = candidate._source.googleplus.url;
                    }
                }

                if (candidate._source.twitter) {
                    if (candidate._source.twitter.url) {
                        twitterProfile = candidate._source.twitter.url;
                    }
                }

                if (candidate._source.facebook) {
                    if (candidate._source.facebook.url) {
                        faceBookProfile = candidate._source.facebook.url;
                    }
                }

                if (candidate._source.location) {
                    if (candidate._source.location.name) {
                        addressLine1 = candidate._source.location.name;
                    }
                }
            }
        }

        requestJSON.faceBookProfile = faceBookProfile;
        requestJSON.twitterProfile = twitterProfile;
        requestJSON.linkedInProfile = linkedInProfile;
        requestJSON.googlePlusProfile = googlePlusProfile;
        requestJSON.candidateAddressTO.addressLine1 = addressLine1;

        return requestJSON;
    }

    static setWorkDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let workExpTOs: Array<WorkExpTO> = [];
        if (candidate) {
            if (candidate._source) {
                if (candidate._source.experiences) {
                    if (candidate._source.experiences.length > 0) {
                        for (let record of candidate._source.experiences) {
                            let company = "";
                            let jobTitle = "";
                            let companyStartDate = null;
                            let currentlyWorkHere = false;
                            let companyEndDate = null;
                            let ctcCurrency = "";
                            let ctcAmount = 0;
                            let ctcDenomination = "";
                            let description = "";

                            if (record.company) {
                                if (record.company.name) {
                                    company = record.company.name;
                                }
                            }

                            if (record.start_date) {
                                companyStartDate = new Date(record.start_date);
                            }

                            if (record.end_date) {
                                companyEndDate = new Date(record.end_date);
                            }

                            if (record.current) {
                                if (record.current == true) {
                                    currentlyWorkHere = true;
                                }
                            }

                            if (record.title) {
                                jobTitle = record.title;
                            }

                            if (record.description) {
                                description = record.description;
                            }

                            let targetPos = 0;

                            if (companyStartDate == null) {
                                targetPos = workExpTOs.length;
                            } else {
                                for (let work of workExpTOs) {
                                    if (work.startDate != null && companyStartDate != null && work.startDate > companyStartDate) {
                                        targetPos++;
                                    } else {
                                        break;
                                    }
                                }
                            }

                            let workExpTO: WorkExpTO = new WorkExpTO();
                            workExpTO.candidateEmpDetailId = 0
                            workExpTO.startDate = companyStartDate;
                            workExpTO.endDate = currentlyWorkHere ? null : companyEndDate;
                            workExpTO.companyName = company;
                            workExpTO.jobTitle = jobTitle;
                            workExpTO.currentEmployer = currentlyWorkHere;
                            workExpTO.currencyCode = ctcCurrency;
                            workExpTO.ctc = ctcAmount;
                            workExpTO.ctcPart = ctcDenomination;
                            workExpTO.jobDescription = description;
                            workExpTOs.push(workExpTO);
                        }
                    }
                }
            }
        }

        requestJSON.workExpTOs = workExpTOs

        return requestJSON;
    }

    static setEducationDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let educationTOs: Array<EducationTO> = [];
        if (candidate._source) {
            if (candidate._source.educations) {
                if (candidate._source.educations.length > 0) {
                    for (let record of candidate._source.educations) {
                        let degree = "";
                        let batchStart = null;
                        let batchEnd = null;
                        let institute = "";
                        let gradePercentage = "";

                        if (record.institute) {
                            if (record.institute.name) {
                                institute = record.institute.name;
                            }
                        }

                        if (record.degree) {
                            if (record.degree.name) {
                                degree = record.degree.name;
                            }
                        }

                        if (record.start_date) {
                            batchStart = new Date(record.start_date);
                        }

                        if (record.end_date) {
                            batchEnd = new Date(record.end_date);
                        }

                        let targetPos = 0;

                        if (batchStart == null) {
                            targetPos = educationTOs.length;
                        } else {
                            for (let edu of educationTOs) {
                                if (edu.startDate != null && batchStart != null && edu.startDate > batchStart) {
                                    targetPos++;
                                } else {
                                    break;
                                }
                            }
                        }

                        let educationTO: EducationTO = new EducationTO();
                        educationTO.candidateEduDetailId = undefined;
                        educationTO.degree = degree;
                        educationTO.startDate = batchStart;
                        educationTO.endDate = batchEnd;
                        educationTO.institute = institute;
                        educationTOs.push(educationTO);
                    }
                }
            }
        }

        requestJSON.educationTOs = educationTOs;

        return requestJSON;
    }

    static setSkillDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let skillTOs: Array<SkillTO> = []
        if (candidate._source) {
            if (candidate._source.skill_tagged) {
                if (candidate._source.skill_tagged.length > 0) {
                    for (let record of candidate._source.skill_tagged) {
                        if (record.name) {
                            let skillTO = new SkillTO();
                            skillTO.skillId = undefined;
                            skillTO.skillName = record.name;
                            skillTOs.push(skillTO);
                        }
                    }
                }
            }
        }
        requestJSON.skillTOs = skillTOs;
        return requestJSON;
    }

    static setInternshipDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let workExpTOs: Array<WorkExpTO> = [];
        if (candidate) {
            if (candidate._source) {
                if (candidate._source.internships) {
                    if (candidate._source.internships.length > 0) {
                        for (let record of candidate._source.internships) {
                            let company = "";
                            let jobTitle = "";
                            let companyStartDate = null;
                            let currentlyWorkHere = false;
                            let companyEndDate = null;
                            let ctcCurrency = "";
                            let ctcAmount = 0;
                            let ctcDenomination = "";
                            let description = "";

                            if (record.company) {
                                if (record.company.name) {
                                    company = record.company.name;
                                }
                            }

                            if (record.start_date) {
                                companyStartDate = new Date(record.start_date);
                            }

                            if (record.end_date) {
                                companyEndDate = new Date(record.end_date);
                            }

                            if (record.current) {
                                if (record.current == true) {
                                    currentlyWorkHere = true;
                                }
                            }

                            if (record.title) {
                                jobTitle = record.title;
                            }

                            if (record.description) {
                                description = record.description;
                            }


                            let targetPos = 0;

                            if (companyStartDate == null) {
                                targetPos = workExpTOs.length;
                            } else {
                                for (let work of workExpTOs) {
                                    if (work.startDate != null && companyStartDate != null && work.startDate > companyStartDate) {
                                        targetPos++;
                                    } else {
                                        break;
                                    }
                                }
                            }

                            let workExpTO: WorkExpTO = new WorkExpTO();
                            workExpTO.candidateEmpDetailId = undefined;
                            workExpTO.startDate = companyStartDate;
                            workExpTO.endDate = currentlyWorkHere ? null : companyEndDate;
                            workExpTO.companyName = company;
                            workExpTO.jobTitle = jobTitle;
                            workExpTO.currentEmployer = currentlyWorkHere;
                            workExpTO.jobDescription = description;
                            workExpTOs.push(workExpTO);
                        }
                    }
                }
            }
        }
        requestJSON.internshipWorkExpTOs = workExpTOs;
        return requestJSON;
    }

    static setLanguageDetailsRequestJSON(requestJSON: SaveCandidateDetailsTO, candidate: any) {
        requestJSON = this.initRequestJSON(requestJSON);
        let languageTOs: Array<SkillTO> = []
        if (candidate._source) {
            if (candidate._source.linkedin) {
                if (candidate._source.linkedin.languages) {
                    if (candidate._source.linkedin.languages.length > 0) {
                        for (let record of candidate._source.linkedin.languages) {
                            let languageTO = new SkillTO();
                            languageTO.skillId = undefined;
                            languageTO.skillName = record;
                            languageTOs.push(languageTO);
                        }
                    }
                }
            }
        }

        requestJSON.languageTOs = languageTOs;

        return requestJSON;
    }
}