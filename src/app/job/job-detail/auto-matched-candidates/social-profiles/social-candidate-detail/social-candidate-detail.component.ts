import { FormatJobcodeParamPipe } from './../../../../../shared/format-jobcode-param.pipe';
import { AutoMatchSocialPagedService } from './../../../auto-match-social-paged.service';
import { FormatCandidatecodeParamPipe } from './../../../../../shared/format-candidatecode-param.pipe';
import { NewCandidateService } from './../../../../../candidate/new-candidate/new-candidate.service';
import { SessionService } from './../../../../../session.service';
import { NotificationService, NotificationSeverity } from './../../../../../common/notification-bar/notification.service';
import { ParseJobcodeParamPipe } from './../../../../parse-jobcode-param.pipe';
import { JobDetailService } from './../../../job-detail.service';
import { CommonService } from './../../../../../common/common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AutoMatchCandidatesService } from './../../../auto-match-candidates.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import SocialUtil from "app/job/job-detail/auto-matched-candidates/social-profiles/social-util";
import { NameinitialService } from '../../../../../shared/nameinitial.service';
import { AutomatchEmailComponent } from '../../automatch-email/automatch-email.component';
import { AutoatchEmailService } from '../../automatch-email/automatch-email.service';
import { DialogService } from '../../../../../shared/dialog.service';

@Component({
  selector: 'alt-social-candidate-detail',
  templateUrl: './social-candidate-detail.component.html',
  styleUrls: ['./social-candidate-detail.component.css'],
  providers: [ParseJobcodeParamPipe, NewCandidateService, FormatCandidatecodeParamPipe, FormatJobcodeParamPipe,AutoatchEmailService]
})
export class SocialCandidateDetailComponent implements OnInit {


  jobSummaryData: any;
  requisitionId: number;
  requisitionCode: string;
  socialUID: string;

  jobSkills: any[];
  candidateData: any = {};
  randomcolor: string[] = [];
  candidateDetailLoaded: boolean = false;
  savingAsDraft: boolean = false;
  draftSaved: boolean = false;
  candidateCode: string = null;
  showSaveAsDraftButton: boolean = false;
  automatchEnabled: boolean;

  currentPosition: number;
  totalRows: number;
  messageCode: MessageCode;
  responseData: ResponseData;
  socialMailLoader : String;

  constructor(
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private autoMatchSocialPagedService: AutoMatchSocialPagedService,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private jobDetailService: JobDetailService,
    private parser: ParseJobcodeParamPipe,
    private notificationService: NotificationService,
    public location: Location,
    private formatJobcodeParamPipe: FormatJobcodeParamPipe,
    private router: Router,
    private sessionService: SessionService,
    private newCandidateService: NewCandidateService,
    private formatCandidatecodeParamPipe: FormatCandidatecodeParamPipe,
    private nameinitialService: NameinitialService,
    private autoatchEmailService: AutoatchEmailService,
    private dialogService: DialogService,
  ) {
    this.automatchEnabled =  this.sessionService.isAutoMatchEnabled();
    this.jobSummaryData = {};
    this.jobSummaryData.jobCode = "Loading...";
    this.jobSummaryData.jobName = "Loading...";
    this.jobSummaryData.postedOn = "Loading...";
    this.jobSummaryData.publishedTo = "Loading...";
    this.jobSummaryData.summaryOrgUnit = "Loading...";
    this.jobSummaryData.summaryWorkSite = "Loading...";
    this.jobSummaryData.targetOn = "Loading...";
    this.jobSummaryData.teamSize = "Loading...";
    this.randomcolor = nameinitialService.randomcolor;
    this.requisitionCode = "";
    this.commonService.showRHS();
    this.commonService.setSideBar('autoMatchSocial');
    this.commonService.disableRhsMonster();
  }

  ngOnInit() {
    this.socialMailLoader = 'loader';
    this.totalRows = this.autoMatchSocialPagedService.getTotalCandidates();
    this.currentPosition = this.autoMatchSocialPagedService.currentPosition;


    window.scroll(0, 0);

    if (this.autoMatchCandidatesService.jobSummaryData != null) {
      this.jobSummaryData = this.autoMatchCandidatesService.jobSummaryData;
    }

    this.requisitionId = this.autoMatchCandidatesService.requisitionId;
    this.requisitionCode = this.autoMatchCandidatesService.requisitionCode;
    this.jobSkills = this.sessionService.jobSkills;

    this.route.params.subscribe((params) => {
      if (params['sid'] != null && params['sid'].trim() != '') {
        this.socialUID = params['sid'];


        if (this.requisitionId != null && this.requisitionId > 0) {

          if (this.autoMatchCandidatesService.jobSummaryData == null) {
            Promise.resolve(this.jobDetailService.getJobSummary(this.requisitionCode).toPromise()).then((response) => {
              this.processJobDetailsResponse(response);
            }).catch((error) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not load job details!");
            });
          }

          if (this.jobSkills == null) {
            Promise.resolve(this.jobDetailService.getRequiredSkills(this.requisitionCode).toPromise()).then((response) => {
              this.processJobSkillsResponse(response);
            }).catch((error) => {
              this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not find job");
            });
          }

          Promise.resolve(this.autoMatchCandidatesService.checkSocialCandidateIds({ requisitionId: this.requisitionId, socialUid: this.socialUID })).then((response) => {
            this.processSocialUidsResponse(response);
          });

        } else {

          this.route.params.subscribe((params) => {

            let jobCodeParam = params['id'];
            if (jobCodeParam != null) {
              this.requisitionCode = this.parser.transform(params['id']);
              if (this.totalRows == null) {
                this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatJobcodeParamPipe.transform(this.requisitionCode) + '/social');
                return;
              }

              Promise.all([this.jobDetailService.getRequisitionIDByCode(this.requisitionCode).toPromise(),
              this.jobDetailService.getJobSummary(this.requisitionCode).toPromise(),
              this.jobDetailService.getRequiredSkills(this.requisitionCode).toPromise()]

              ).then((responseArray) => {

                this.processRequisitionIdResponse(responseArray[0]);
                this.processJobDetailsResponse(responseArray[1]);
                this.processJobSkillsResponse(responseArray[2]);
              }).catch((error) => {
                this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not load job details. Please try again");
              });
            }
          });
        }


        Promise.resolve(this.autoMatchCandidatesService.getSocialCandidateDetail(this.socialUID).toPromise()).then((response) => {
          this.processSocialDataResponse(response);
        }).catch((error) => {
          this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not load candidate details. Please try again later");
        });

      }
    });
  }


  processSocialUidsResponse(response) {
    this.draftSaved = false;
    this.showSaveAsDraftButton = true;
    if (response) {
      if (response.responseData) {
        if (response.responseData[0].socialUids) {
          let draftCandidateCode = response.responseData[0].draftCandidateCode;
          let candidateCode = response.responseData[0].candidateCode;
          if (draftCandidateCode != null) {
            this.candidateCode = draftCandidateCode;
            this.draftSaved = true;
            return;
          } else if (candidateCode != null) {
            this.candidateCode = candidateCode;
            this.draftSaved = true;
            return;
          }

        }
      }
    }
  }


  processRequisitionIdResponse(response) {
    if (response != null && response == 0) {
      this.notificationService.clear();
      this.notificationService.setMessage(NotificationSeverity.WARNING, "Error: Could not find job details");
    } else {
      this.requisitionId = response;
      Promise.resolve(this.autoMatchCandidatesService.checkSocialCandidateIds({ requisitionId: this.requisitionId, socialUid: this.socialUID })).then((response) => {
        this.processSocialUidsResponse(response);
      });
    }
  }

  processJobSkillsResponse(response) {
    if (response) {
      if (response.requiredSkills) {
        this.jobSkills = response.requiredSkills;
        this.sessionService.jobSkills = response.requiredSkills;

        if (this.candidateData != null) {
          this.processFirstFiveCandidateSkills();
        }
      }
    }
  }

  previousCandidate() {
    if (!this.candidateDetailLoaded) {
      return;
    }

    if (this.currentPosition == 1) {
      return;
    } else {
      this.candidateCode = null;
      this.showSaveAsDraftButton = false;
      this.candidateDetailLoaded = false;
      this.currentPosition--;
      this.loadCandidateDetail();
    }
  }

  nextCandidate() {
    if (!this.candidateDetailLoaded) {
      return;
    }

    if (this.currentPosition == this.totalRows) {
      return;
    } else {
      this.candidateCode = null;
      this.showSaveAsDraftButton = false;
      this.candidateDetailLoaded = false;
      this.currentPosition++;
      this.loadCandidateDetail();
    }
  }

  loadCandidateDetail() {
    this.autoMatchSocialPagedService.getItemByIndex(this.currentPosition).subscribe((response) => {

      if (response) {
        if (response instanceof Array) {
          this.socialUID = response[0]._id;
        } else {
          this.socialUID = response._id;
        }
      }

      this.location.replaceState('job/auto-match-candidates/' + this.formatJobcodeParamPipe.transform(this.requisitionCode) + '/social/candidate/' + this.socialUID);

      Promise.resolve(this.autoMatchCandidatesService.getSocialCandidateDetail(this.socialUID).toPromise()).then((response) => {
        this.processSocialDataResponse(response);

      }).catch((error) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, "Error: Could not load candidate details. Please try again later");
      });

      Promise.resolve(this.autoMatchCandidatesService.checkSocialCandidateIds({ requisitionId: this.requisitionId, socialUid: this.socialUID })).then((response) => {
        this.processSocialUidsResponse(response);
      });
    });
  }

  processJobDetailsResponse(response) {
    this.jobSummaryData = response;
    this.sessionService.jobSummaryData = this.jobSummaryData;
    this.jobDetailService.jobSummaryData = response;
  }

  processSocialDataResponse(response) {
    this.candidateDetailLoaded = false;
    if (response) {
      if (response.res) {
        this.candidateData = response.res;
        this.candidateData.firstFiveSkills = [];
        if (this.jobSkills != null) {
          this.processFirstFiveCandidateSkills();
        }
        this.candidateDetailLoaded = true;
      }
    }
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  getProfileMatch(){

    let score = 0;
    if(this.autoMatchSocialPagedService.match_score)
    score = this.autoMatchSocialPagedService.match_score * 100;
    
    return Math.round(score);
    
  }

  getProfileMatchAbsoulte(){

    let score = 0;
    if(this.candidateData.match_score_absolute)
    score = this.candidateData.match_score_absolute * 100;
    
    return Math.round(score);
    
  }

  getSocialCandidateName() {
    let candidate_name = "";
    if (this.candidateData._source) {
      if (this.candidateData._source.linkedin) {
        if (this.candidateData._source.linkedin.name) {
          candidate_name = this.candidateData._source.linkedin.name;
        }
      }
    }
    if (candidate_name.length > 20) {
      candidate_name = candidate_name.substr(0, 20);
    }
    return candidate_name;
  }
  imageError() {
    this.candidateData._source.linkedin.profile_pic = null;
  }
  getSocialProfilePic() {
    let url: string = null;
    if (this.candidateData._source) {
      if (this.candidateData._source.linkedin) {
        if (this.candidateData._source.linkedin.profile_pic) {
          url = this.candidateData._source.linkedin.profile_pic.toString();
          if (url != null && url.indexOf("http://") > 0) {
            url = url.replace("http://", "https://");
          }
        }
      }
    }
    return url;
  }

  getCandidateLocation() {
    let location = "";
    if (this.candidateData) {
      if (this.candidateData._source) {
        if (this.candidateData._source.location) {
          if (this.candidateData._source.location.name) {
            location = this.candidateData._source.location.name;
          }
          if (this.candidateData._source.location.state) {
            location += ", " + this.candidateData._source.location.state;
          }
        }
      }
    }
    return location;
  }

  getFirstThreeSocialLinks() {
    return SocialUtil.getFirstThreeSocialLinks(this.candidateData);
  }

  getAfterFirstThreeSocialLinks() {
    return SocialUtil.getAfterFirstThreeSocialLinks(this.candidateData);
  }

  getEmail() {
    return SocialUtil.getEmail(this.candidateData);
  }

  getAllPhoneNumbers() {
    return SocialUtil.getAllPhoneNumbers(this.candidateData);
  }

  processFirstFiveCandidateSkills() {
    let fiveSkills = [];
    let skills = SocialUtil.getCandidateSkills(this.candidateData);


    for (let skill of skills) {
      for (let jobSkill of this.jobSkills) {
        if (skill.skillName.toLowerCase() == jobSkill.text.toLowerCase()) {
          fiveSkills.push(skill);
          break;
        }
      }
      if (fiveSkills.length >= 8) {
        break;
      }
    }


    let i = 0;

    while (fiveSkills.length < 8 && i < skills.length) {

      if (!fiveSkills.includes(skills[i])) {

        fiveSkills.push(skills[i]);
      }
      i++;
    }

    this.candidateData.firstFiveSkills = fiveSkills;

    return true;
  }
  getCandidateSkills() {
    return SocialUtil.getCandidateSkills(this.candidateData);
  }

  showSocialLinks() {
    this.candidateData.socialLinksClass = "displayBlock";
  }

  hideSocialLinks() {
    this.candidateData.socialLinksClass = "displayNone";
  }

  getName() {
    return SocialUtil.getName(this.candidateData);
  }
  getGender() {
    return SocialUtil.getGender(this.candidateData);
  }
  getDateOfBirth() {
    return SocialUtil.getDateOfBirth(this.candidateData);
  }
  getCountry() {
    return SocialUtil.getCountry(this.candidateData);
  }
  getState() {
    return SocialUtil.getState(this.candidateData);
  }
  getCity() {
    return SocialUtil.getCity(this.candidateData);
  }

  getMobileNumber() {
    return SocialUtil.getMobileNumber(this.candidateData);
  }

  getWorkDetails() {
    return SocialUtil.getWorkRequestJSON(this.candidateData).employmentHistory;
  }
  getInternshipDetails() {
    return SocialUtil.getInternshipRequestJSON(this.candidateData).employmentHistory;
  }

  getEducationDetails() {
    return SocialUtil.getEducationRequestJSON(this.candidateData).educationHistory;
  }
  getLanguages() {
    return SocialUtil.getLanguages(this.candidateData);
  }

  goBack() {
    this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatJobcodeParamPipe.transform(this.requisitionCode) + '/social');
  }

  saveAsDraft() {

    let position = this.currentPosition;

    this.savingAsDraft = true;

    let requestJSON = SocialUtil.setCandidateDetailsRequestJSON(undefined, this.candidateData, this.requisitionId);

    let subscription = this.newCandidateService.saveCandidateDetails(requestJSON.candidateId, requestJSON, true, undefined).subscribe(response => {
      if (response != null && response != undefined) {
        if (response.messageCode.code == 'EC200') {
          this.candidateCode = response.response.candidateCode;
          this.autoMatchSocialPagedService.getItemByIndex(position).subscribe((candidateInfo) => {
            if (candidateInfo != null) {
              candidateInfo.saved = true;
              candidateInfo.saving = false;
              candidateInfo.candidateCode = this.candidateCode;
            }
          });
          this.draftSaved = true;
          this.notificationService.setMessage(NotificationSeverity.INFO, "Draft Saved Successfully: " + this.candidateCode);
        } else if (response.messageCode.code == 'EC201') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else if (response.messageCode.code == 'EC202') {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Candidate already exists in the database");
        } else {
          this.notificationService.setMessage(NotificationSeverity.WARNING, "Error Occured while saving ! Please try again later");

        }
      } else {
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not connect to the server. Please try again.");
      }
      this.savingAsDraft = false;
    }, 
    (failure) => {
      this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not create candidate. Please try again");
      this.savingAsDraft = false;
    });

  }

  sendEmail(candidate) {
    this.socialMailLoader = 'nodata';
    this.sessionService.jobSummaryData.candidateName = SocialUtil.getName(candidate);
    this.sessionService.jobSummaryData.emailId = SocialUtil.getEmail(candidate);
    this.sessionService.jobSummaryData.socialUid = candidate._id;
    this.autoatchEmailService.getAutomatchEmailTemplate().subscribe(out => {
      this.messageCode = out.messageCode;
      this.responseData = out.response;
      if (this.responseData != null) {
        this.sessionService.jobSummaryData.subject = this.responseData.subject;
        this.sessionService.jobSummaryData.content = this.responseData.content;
        this.socialMailLoader = 'data';
      }else {
        this.socialMailLoader = 'nodata';
      } 
      this.dialogService.open(AutomatchEmailComponent, { width: '935px' });
    });
  }

  goToCandidateDraft() {
    this.router.navigateByUrl("/candidate/newcandidate/" + this.formatCandidatecodeParamPipe.transform(this.candidateCode));
  }
  getlengthLessThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length > 40
  }
  getlengthGreatThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length <= 40
  }
  getlengthLessThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length > 40
  }
  getlengthGreatThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length <= 40
  }
  getlengthZero() {
    return this.jobSummaryData.summaryWorkSite == null || this.jobSummaryData.summaryWorkSite.length == 0
  }

}


export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  subject: String;
  content: String;
}
