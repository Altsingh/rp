import { FormatJobcodeParamPipe } from './../../../../../shared/format-jobcode-param.pipe';
import { Component, OnInit, Output, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'alt-social-candidate-detail-loading-screen',
  templateUrl: './social-candidate-detail-loading-screen.component.html',
  styleUrls: ['./social-candidate-detail-loading-screen.component.css'],
  providers:[FormatJobcodeParamPipe]
})
export class SocialCandidateDetailLoadingScreenComponent implements OnInit {

  @Input()
  jobSummaryData: any;

  @Input()
  currentPosition: number;

  @Input()
  totalRows: number;

  @Input()
  jobSkills:any;

  @Input()
  requisitionCode:any;

  @Input()
  candidateCode:any;

  constructor(
    private router: Router,
    private formatJobcodeParamPipe: FormatJobcodeParamPipe
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.router.navigateByUrl('/job/auto-match-candidates/' + this.formatJobcodeParamPipe.transform(this.requisitionCode) + '/social');
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  } 

  getlengthLessThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length > 40
  }
  getlengthGreatThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length <= 40
  }
  getlengthLessThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length > 40
  }
  getlengthGreatThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length <= 40
  }
  getlengthZero() {
    return this.jobSummaryData.summaryWorkSite == null || this.jobSummaryData.summaryWorkSite.length == 0
  }

}
