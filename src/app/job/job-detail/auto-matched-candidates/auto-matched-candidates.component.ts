import { AutoMatchStarsDividerService } from './../../../common/auto-match-stars-divider.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from '../../../common/common.service';
import { AutoMatchCandidatesService } from '../auto-match-candidates.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobDetailService } from '../job-detail.service';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { PageState } from '../../../shared/paginator/paginator.component';
import { AutoMatchCompanyPagedService } from '../auto-match-company-paged.service';
import { NotificationService, NotificationSeverity } from "../../../common/notification-bar/notification.service";
import { SessionService } from "../../../session.service";
import { NameinitialService } from '../../../shared/nameinitial.service'
import { NewCandidateService } from '../../../candidate/new-candidate/new-candidate.service';
import { Location } from '@angular/common';
import SocialUtil from 'app/job/job-detail/auto-matched-candidates/social-profiles/social-util';
import { AutoMatchSocialPagedService } from '../auto-match-social-paged.service';
import { AutoMatchMonsterPagedService } from '../auto-match-monster-paged.service';


@Component({
  selector: 'alt-auto-matched-candidates',
  templateUrl: './auto-matched-candidates.component.html',
  providers: [ParseJobcodeParamPipe, NewCandidateService]
})
export class AutoMatchedCandidatesComponent implements OnInit, OnDestroy {

  activeSection: string;
  jobSummaryData: any = {};
  requisitionId: number;
  requisitionCode: string;
  jobSkills: any;
  contextMenuOpen: boolean = false;
  allowAutoMatch: boolean = false;
  security: any
  securitySubscription : any;
  featureDataMap: Map<any,any>;

  constructor(
    private commonService: CommonService,
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private route: ActivatedRoute,
    private jobDetailService: JobDetailService,
    private parser: ParseJobcodeParamPipe,
    private autoMatchCompanyPagedService: AutoMatchCompanyPagedService,
    private notificationService: NotificationService,
    private newCandidateService: NewCandidateService,
    public location: Location,
    private router: Router,
    private sessionService: SessionService,
    private autoMatchStarsDividerService: AutoMatchStarsDividerService,
    private autoMatchSocialPagedService: AutoMatchSocialPagedService,
    private autoMatchMonsterPagedService: AutoMatchMonsterPagedService,
  ) {

    this.initFormSecurity();
    this.jobSummaryData = {};
    this.jobSummaryData.jobCode = "Loading...";
    this.jobSummaryData.jobName = "Loading...";
    this.jobSummaryData.postedOn = "Loading...";
    this.jobSummaryData.publishedTo = "Loading...";
    this.jobSummaryData.summaryOrgUnit = "Loading...";
    this.jobSummaryData.summaryWorkSite = "Loading...";
    this.jobSummaryData.targetOn = "Loading...";
    this.jobSummaryData.teamSize = "Loading...";
    
    this.featureDataMap = this.sessionService.featureDataMap;
  }

  initFormSecurity() {
    this.securitySubscription = this.jobDetailService.getAutoMatchSecurity().subscribe(
      response => {
        if (response.messageCode.code === 'EC200') {
          this.security = JSON.parse(JSON.stringify(response.responseData[0]));
        }
        else
          this.security = undefined;
      });
     
  }

  ngOnInit() {

    this.autoMatchStarsDividerService.getData().subscribe((response) => {
      try {
        SocialUtil.SOCIAL_STARS_DIVIDER = parseInt(response);
      } catch (e) {
        console.log(e);
      }
    });

    this.commonService.showRHS();
    this.commonService.setSideBar('autoMatch');
    this.activeSection = 'COMPANY_DATA';
    if(this.autoMatchCandidatesService.jobSummaryData != null){
      this.jobSummaryData = this.autoMatchCandidatesService.jobSummaryData;
    }
    this.sessionService.jobSummaryData = this.jobSummaryData;
    this.requisitionId = this.autoMatchCandidatesService.requisitionId;
    this.requisitionCode = this.autoMatchCandidatesService.requisitionCode;

    if (this.requisitionId != null && this.requisitionId > 0) {

      if (this.autoMatchCandidatesService.jobSummaryData == null) {
        this.route.params.subscribe(params => {
          this.requisitionCode = this.parser.transform(params['id']);
          this.sessionService.jobSkills = [];
          this.jobDetailService.getRequiredSkills(this.requisitionCode).subscribe((skills) => {
            if (skills) {
              if (skills.requiredSkills) {
                this.jobSkills = skills.requiredSkills;
                this.sessionService.jobSkills = skills.requiredSkills;
              }
            }

            this.jobDetailService
              .getJobSummary(this.requisitionCode)
              .subscribe((p) => {
                this.jobSummaryData = p;
                this.jobDetailService.jobSummaryData = p;
                this.sessionService.jobSummaryData = this.jobSummaryData;
                this.checkAllowAutoMatch();
              });
          });
        });

      } else {
        this.jobDetailService.getRequiredSkills(this.requisitionCode).subscribe((skills) => {
          if (skills) {
            if (skills.requiredSkills) {
              this.jobSkills = skills.requiredSkills;
              this.sessionService.jobSkills = skills.requiredSkills;
            }
          }
        });
      }

      this.checkAllowAutoMatch();
    } else {
      this.route.params.subscribe(params => {

        this.requisitionCode = this.parser.transform(params['id'])
        this.jobDetailService.getRequiredSkills(this.requisitionCode).subscribe((skills) => {
          if (skills) {
            if (skills.requiredSkills) {
              this.jobSkills = skills.requiredSkills;
              this.sessionService.jobSkills = skills.requiredSkills;
            }
          }
        });

        this.autoMatchCandidatesService.requisitionCode = this.requisitionCode;
        this.jobDetailService.getRequisitionIDByCode(this.parser.transform(params['id'])).subscribe((out) => {
          this.requisitionId = out;
          this.autoMatchCandidatesService.requisitionId = this.requisitionId;
        });

        this.jobDetailService
          .getJobSummary(this.parser.transform(params['id']))
          .subscribe((p) => {
            this.jobSummaryData = p;
            this.sessionService.jobSummaryData = this.jobSummaryData;
            this.checkAllowAutoMatch();
            this.jobDetailService.jobSummaryData = p;
          });
      });
    }

  }

  checkAllowAutoMatch() {
    let jobClosed = false;
    let jobHold = false;

    if (this.jobSummaryData != null) {
      if (this.jobSummaryData.jobStatus != null
        && (this.jobSummaryData.jobStatus === 'CLOSED' || this.jobSummaryData.jobStatus === 'PCLOSE')) {
        jobClosed = true;
      }
      if (this.jobSummaryData.jobStatus != null
        && (this.jobSummaryData.jobStatus === 'ONHOLD' && this.jobSummaryData.jobStatus === 'PHOLD')) {
        jobHold = true;
      }
    } else {
      this.allowAutoMatch = false;
    }

    if (jobClosed) {
      this.notificationService.setMessage(NotificationSeverity.WARNING, "Auto-Match candidates not allowed since this job is closed");
      this.allowAutoMatch = false;
    } else if (jobHold) {
      this.notificationService.setMessage(NotificationSeverity.WARNING, "Auto-Match candidates not allowed since this job is on hold");
      this.allowAutoMatch = false;
    } else {
      this.allowAutoMatch = true;
      let path = this.location.path();
    }
  }

  setActive(section) {

    if( section == 'JOB_BOARDS' ){
      this.autoMatchMonsterPagedService.sortBy = null;
      this.autoMatchMonsterPagedService.clearLastRequestJSON();
    }

    if( section == 'SOCIAL_PROFILES' ){
      this.autoMatchSocialPagedService.clearCache();
      this.autoMatchSocialPagedService.clearLastRequestJSON();
      this.autoMatchSocialPagedService.savePageState(null);
    }

    this.activeSection = section;
  }
  
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  ngOnDestroy() {
    this.commonService.setSideBar('default');
  }

  getlengthLessThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length > 40
  }
  getlengthGreatThanLi1() {
    return this.jobSummaryData.summaryOrgUnit != null && this.jobSummaryData.summaryOrgUnit.length <= 40
  }
  getlengthLessThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length > 40
  }
  getlengthGreatThanLi2() {
    return this.jobSummaryData.summaryWorkSite != null && this.jobSummaryData.summaryWorkSite.length <= 40
  }
  getlengthZero() {
    return this.jobSummaryData.summaryWorkSite == null || this.jobSummaryData.summaryWorkSite.length == 0
  }

  hideContextMenu() {
    this.contextMenuOpen = false;
  }
  showContextMenu() {
    this.contextMenuOpen = true;
  }

}