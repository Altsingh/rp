import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService, NotificationSeverity } from "../../../../common/notification-bar/notification.service";
import { PageState } from '../../../../shared/paginator/paginator.component';
import { TagCandidateToJobData } from "../../../../candidate/candidate-list/candidate-untagged/candidate-untag-actions/candidate-job-data";
import { SessionService } from "../../../../session.service";
import { AutoMatchCompanyPagedService } from '../../auto-match-company-paged.service';
import { CandidateListService } from "../../../../candidate/candidate-list/candidate-list.service";
import { NameinitialService } from '../../../../shared/nameinitial.service'
import { DialogService } from "../../../../shared/dialog.service";
import { EmailCandidatesComponent } from "../../../../job/job-detail/tagged-candidates/email-candidates/email-candidates.component";
import { AutoMatchCandidatesService } from '../../auto-match-candidates.service';
import { JobDetailService } from '../../job-detail.service';
import { FormatCandidatecodeParamPipe } from '../../../../shared/format-candidatecode-param.pipe';
import { CommonService } from '../../../../common/common.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'alt-company-profiles',
  templateUrl: './company-profiles.component.html',
  styleUrls: ['./company-profiles.component.css'],
  providers: [CandidateListService]
})
export class CompanyProfilesComponent implements OnInit, OnDestroy {

  candidateCodeFormatter = new FormatCandidatecodeParamPipe();
  // company tab
  companyPageState: PageState;
  companyPageLoading: boolean = true;
  companyPageLoaded: boolean = false;
  companyLoaderIndicator: string = ''
  taggedCandidateIds: number[];
  organizationId: number;
  newTagCandidateData: TagCandidateData;
  tagResponse: ResponseData;
  messageCode: MessageCode;
  companySelected: any[] = [];
  allCompanySelected: boolean = false;
  tagMultipleCandidates: candidateListData;
  taggingCandidates: number[] = [];
  requisitionId: number;
  requisitionCode: string;
  randomcolor: string[] = [];

  companyDataCandidates: any[] = [];
  skillList = [];

  pageSubscription: Subscription;
  automatchEnabled:boolean;

  featureDataMap:Map<any,any>;
   constructor(
    private notificationService: NotificationService,
    private sessionService: SessionService,
    private autoMatchCompanyPagedService: AutoMatchCompanyPagedService,
    private candidateListService: CandidateListService,
    private nameinitialService: NameinitialService,
    private dialogsService: DialogService,
    private autoMatchCandidatesService: AutoMatchCandidatesService,
    private jobDetailService: JobDetailService,
    private router: Router,
    private commonService: CommonService,
    private route: ActivatedRoute) {
    this.featureDataMap = this.sessionService.featureDataMap;
    this.automatchEnabled =  this.sessionService.isAutoMatchEnabled();
    this.requisitionId = this.autoMatchCandidatesService.requisitionId;
    this.requisitionCode = this.autoMatchCandidatesService.requisitionCode;
    this.randomcolor = nameinitialService.randomcolor;
    this.tagResponse = new ResponseData();
    this.companySelected = [];

    let stateJSON = this.autoMatchCompanyPagedService.getPageState();

    this.companyPageState = new PageState();

    if (stateJSON != null && stateJSON.pageState != null) {
      this.companyPageState.currentPageNumber = stateJSON.pageState.currentPageNumber;
      this.companyPageState.pageSize = stateJSON.pageState.pageSize;
      this.companyPageState.totalRows = stateJSON.pageState.totalRows;
    } else {
      this.companyPageState.pageSize = 15;
    }

  }

  getJobSkills(): Promise<any> {
    this.skillList = this.autoMatchCompanyPagedService.getSkills();

    if (this.skillList == null || this.skillList.length == 0) {

      return Promise.resolve(this.jobDetailService.getRequiredSkills(this.requisitionCode).toPromise()).then((skills) => {
        if (skills) {
          if (skills.requiredSkills) {
            this.sessionService.jobSkills = skills.requiredSkills;
            this.skillList = skills.requiredSkills;
          }
        }
      });

    } else {

      this.sessionService.jobSkills = this.skillList;

      return new Promise(() => {
        return true;
      });
    }
  }

  ngOnInit() {
    this.commonService.hideRHS();
    this.commonService.setSideBar('autoMatch');
    this.commonService.showRHS();
    this.companySelected = [];
    this.requisitionId = this.autoMatchCandidatesService.requisitionId;
    this.requisitionCode = this.autoMatchCandidatesService.requisitionCode;

    if (this.requisitionId != null && this.requisitionId > 0) {

      this.loadData();

    } else {

      this.jobDetailService.getRequisitionIDByCode(this.requisitionCode).subscribe((out) => {
        this.requisitionId = out;
        this.autoMatchCandidatesService.requisitionId = this.requisitionId;
        this.loadData();
      });
    }
  }

  ngOnDestroy() {
    if (this.pageSubscription != null) {
      this.pageSubscription.unsubscribe();
    }
  }

  loadData() {

    if (this.taggedCandidateIds == null) {
      Promise.resolve(this.loadTaggedCandidateIds()).then((taggedCandidatesResponse) => {

        this.taggedCandidateIds = [];

        if (taggedCandidatesResponse) {
          if (taggedCandidatesResponse.responseData) {
            if (taggedCandidatesResponse.responseData[0]) {
              if (taggedCandidatesResponse.responseData[0].taggedCandidateIds) {
                this.taggedCandidateIds = taggedCandidatesResponse.responseData[0].taggedCandidateIds;
              }
              if (taggedCandidatesResponse.responseData[0].organizationId) {
                this.organizationId = taggedCandidatesResponse.responseData[0].organizationId;
              }
            }
          }
        }

        Promise.resolve(this.getJobSkills()).then((result) => {
          this.loadCompanyData();
        })

      });

    } else {
      Promise.resolve(this.getJobSkills()).then((result) => {
        this.loadCompanyData();
      })
    }
  }

  getCompanyPageNumber(event) {
    this.allCompanySelected = false;
    this.autoMatchCompanyPagedService.savePageState(null);
    Promise.resolve(this.getJobSkills()).then((result) => {
      this.loadCompanyData();
    })
  }

  loadTaggedCandidateIds(): Promise<any> {

    let tcRequestJSON = {
      requisitionId: this.requisitionId,
    }

    return this.autoMatchCandidatesService.getTaggedCandidateIds(tcRequestJSON);
  }

  loadCompanyData() {


    this.companyPageLoading = true;

    let stateJSON = this.autoMatchCompanyPagedService.getPageState();
    if (stateJSON != null && stateJSON.pageState != null) {
      this.companyPageState.currentPageNumber = stateJSON.pageState.currentPageNumber;
      this.companyPageState.pageSize = stateJSON.pageState.pageSize;
      this.companyPageState.totalRows = stateJSON.pageState.totalRows;
    }



    let body = new URLSearchParams();
    body.set("requisitionID", this.requisitionId + "");
    body.set("organizationID", this.organizationId + "");


    let hasTagged = false;
    if (this.taggedCandidateIds != null) {
      for (let i = 0; i < this.taggedCandidateIds.length; i++) {
        body.append("taggedCandidateIds", this.taggedCandidateIds[i] + "");
        hasTagged = true;
      }
    }
    if (!hasTagged) {
      body.set("taggedCandidateIds", "");
    }

    body.set("size", this.companyPageState.pageSize + "");

    let from = (this.companyPageState.currentPageNumber - 1) * this.companyPageState.pageSize;
    body.set("from", from + "");


    this.pageSubscription = this.autoMatchCompanyPagedService.getPage(body, this.companyPageState.currentPageNumber, this.companyPageState.pageSize).subscribe((response) => {
      this.companyPageLoaded = true;
      if (response) {
        this.companyDataCandidates = response;
        if (this.companyDataCandidates.length > 0) {
          for (let candidate of this.companyDataCandidates) {
            if (this.taggedCandidateIds.includes(candidate.candidate_id)) {
              this.autoMatchCompanyPagedService.removeCachedItem(this.companyPageState.currentPageNumber, this.companyPageState.pageSize, this.companyDataCandidates.indexOf(candidate));
              this.companyDataCandidates.splice(this.companyDataCandidates.indexOf(candidate), 1);
            }
            if (!this.companySelected.includes(candidate)) {
              candidate.selected = false;
            }
            this.hideContextMenu(candidate);
          }
        }
        this.areAllCompanySelected();
      }

      let stateJSON = this.autoMatchCompanyPagedService.getPageState();

      if (stateJSON != null) {

        if (stateJSON.scrollTop != null) {
          setTimeout(() => {
            window.scrollTo(0, stateJSON.scrollTop);
          }, 200);
        }
      } else {
        if (this.companyPageState.totalRows != null && this.companyPageState.totalRows > 0 && this.companyPageState.currentPageNumber == 1) {
          let from = (this.companyPageState.currentPageNumber) * this.companyPageState.pageSize;
          body.set("from", from + "");
          this.pageSubscription = this.autoMatchCompanyPagedService.getPage(body, this.companyPageState.currentPageNumber + 1, this.companyPageState.pageSize).subscribe((res1) => { });
        }

        this.companyPageState.totalRows = this.autoMatchCompanyPagedService.getTotalCandidates();
      }
      this.companyPageLoading = false;
    },
      () => {
        this.companyPageLoading = false;
        this.notificationService.setMessage(NotificationSeverity.WARNING, "Could not load data from server. Please try again.");
        this.companyDataCandidates = [];
      },
      () => {
        this.companyPageLoading = false;
      });

  }

  gotoCandidateDetail(candidate) {


    this.sessionService.candidateInfo = candidate;
    this.sessionService.taggedCandidateIds = this.taggedCandidateIds;
    this.sessionService.requisitionId = this.requisitionId;

    let stateJSON = {
      scrollTop: document.querySelector('body').scrollTop,
      pageState: this.companyPageState
    }
    this.autoMatchCompanyPagedService.savePageState(stateJSON);
    this.router.navigateByUrl('/candidate/edit-candidate/' + (this.candidateCodeFormatter.transform(candidate.candidate_code)) + "?f=1");
  }

  assignJobToMultipleCandidates() {
    this.notificationService.clear();
    if (this.companySelected.length == 0) {
      this.notificationService.setMessage("WARNING", "Please select a candidate.");
      return;
    }
    let candidateIdList: any[] = [];
    for (let canId of this.companySelected) {
      candidateIdList.push(canId.candidate_id);
    }
    this.tagMultipleCandidates = new candidateListData();
    this.tagMultipleCandidates.requisitionIDList = [];
    this.tagMultipleCandidates.requisitionIDList.push(this.requisitionId);
    this.tagMultipleCandidates.candidateIDList = [];
    this.tagMultipleCandidates.candidateIDList = candidateIdList;
    this.tagMultipleCandidates.userID = this.sessionService.userID;
    this.tagMultipleCandidates.organizationID = this.sessionService.organizationID;
    this.candidateListService.tagCandidateToJobBulk(this.tagMultipleCandidates).subscribe(out => {
      this.tagResponse.result = out.responseData[0],
        this.messageCode = out.messageCode;
      if (this.tagResponse.result == "success") {
        this.notificationService.setMessage("success", "Candidates tagged successfully!");

        for (let candidate of this.companySelected) {
          this.taggedCandidateIds.push(candidate.candidate_id);
          if (this.companyDataCandidates.indexOf(candidate) >= 0) {

            this.autoMatchCompanyPagedService.removeCachedItem(this.companyPageState.currentPageNumber, this.companyPageState.pageSize, this.companyDataCandidates.indexOf(candidate));
            this.companyDataCandidates.splice(this.companyDataCandidates.indexOf(candidate), 1);
          }
        }

        this.companyPageState.totalRows -= this.companySelected.length;
        this.checkPaginationValid();

        this.companySelected = [];
      }
    });
  }

  getDesignation(candidate) {
    let designation = "";
    if (candidate.previous_designation) {
      if (candidate.previous_designation.length > 0) {
        for (let designation of candidate.previous_designation) {
          if (designation.isCurrent) {
            if (designation.isCurrent == "true") {
              if (designation.designation) {
                return designation.designation;
              } else {
                return "";
              }
            }
          }
        }
        designation = candidate.previous_designation[0].designation;
      }
    }
    return designation;
  }
  getDesignationToolTip(candidate) {

    let designation = this.getDesignation(candidate);

    if (designation != null && designation.length > 29) {
      return designation;
    } else {
      return "";
    }
  }

  getCompanyName(candidate) {
    let designation = "";
    if (candidate.previous_designation) {
      if (candidate.previous_designation.length > 0) {
        for (let designation of candidate.previous_designation) {
          if (designation.isCurrent) {
            if (designation.isCurrent == "true") {
              if (designation.company) {
                return designation.company;
              } else {
                return "";
              }
            }
          }
        }
        designation = candidate.previous_designation[0].company;
      }
    }
    return designation;
  }

  getCompanyToolTip(candidate) {

    let companyName = this.getCompanyName(candidate);

    if (companyName != null && companyName.length > 29) {
      return companyName;
    } else {
      return "";
    }
  }

  getPercentage(candidate) {
    let percentage = "0";
    if (candidate.percentage) {
      percentage = ((candidate.percentage) + "").split(".")[0];
    }
    return percentage;
  }

  includesIgnoreCase(list: any[], str): boolean {
    if (list != null && str != null) {
      if (list.length > 0) {
        for (let x of list) {
          if (str.toString().trim().toLowerCase() == x.text.toString().trim().toLowerCase()) {
            return true;
          }
        }
      }
    }
    return false;
  }

  splitSkills(candidate): string[] {
    let array: string[] = [];
    let array2: string[] = [];

    if (candidate.competencies != null) {
      let skills = candidate.competencies.split(',');
      for (let skill of skills) {
        if (skill.trim() != '' && this.includesIgnoreCase(this.skillList, skill.trim())) {
          array.push(skill);
        } else {
          array2.push(skill)
        }
      }
    }
    for (let x of array2) {
      if (x != '') {
        array.push(x);
      }

    }
    return array;
  }


  singleSelectCompanyEvent(event: any, index: number) {
    if (!event.selected) {
      let index = this.companySelected.indexOf(event);
      this.companySelected.splice(index, 1);
    } else {
      this.companySelected.push(event);
    }
    this.autoMatchCompanyPagedService.syncCandidate(this.companyPageState.currentPageNumber, this.companyPageState.pageSize, index, event);
    this.areAllCompanySelected();
  }



  selectAllCompany() {
    this.allCompanySelected = !this.allCompanySelected;
    let i = 0;
    for (let candidate of this.companyDataCandidates) {
      if (this.allCompanySelected) {
        if (!this.companySelected.includes(candidate)) {
          this.companySelected.push(candidate);
        }
      } else {
        if (this.companySelected.includes(candidate)) {
          this.companySelected.splice(this.companySelected.indexOf(candidate), 1);
        }
      }

      candidate.selected = this.allCompanySelected;
      this.autoMatchCompanyPagedService.syncCandidate(this.companyPageState.currentPageNumber, this.companyPageState.pageSize, i, candidate);
      i++;
    }
  }

  areAllCompanySelected() {
    let allSelected = true;
    for (let candidate of this.companyDataCandidates) {
      if (candidate.selected == null || candidate.selected == false) {
        candidate.selected = false;
        allSelected = false;
      }
    }
    if (allSelected && this.companyDataCandidates != null && this.companyDataCandidates.length > 0) {
      this.allCompanySelected = true;
    } else {
      this.allCompanySelected = false;
    }
  }

  saveTagCandidateData(candidateId: number, index: number) {
    if (this.taggedCandidateIds.includes(candidateId)) {
      return;
    } else {
      this.taggedCandidateIds.push(candidateId);
    }
    this.newTagCandidateData = new TagCandidateData();
    this.newTagCandidateData.input = new TagCandidateToJobData();
    this.newTagCandidateData.input.requisitionIDList = [];
    this.newTagCandidateData.input.requisitionIDList.push(this.requisitionId);
    this.newTagCandidateData.input.candidateID = candidateId;
    this.candidateListService.tagCandidateToJob(this.newTagCandidateData).subscribe((out) => {
      this.tagResponse.result = out.responseData[0],
        this.messageCode = out.messageCode;
      if (this.tagResponse.result == "success") {
        this.notificationService.setMessage("success", "Candidate tagged successfully!");
        this.companyDataCandidates.splice(index, 1);
        this.taggedCandidateIds.push(candidateId);
        this.autoMatchCompanyPagedService.removeCachedItem(this.companyPageState.currentPageNumber, this.companyPageState.pageSize, index);
        this.companyPageState.totalRows--;
        this.checkPaginationValid();
      }
    });
  }

  checkPaginationValid() {
    if (this.companyPageState.pageSize * (this.companyPageState.currentPageNumber - 1) <= this.companyPageState.totalRows) {
      if (this.companyPageState.currentPageNumber != 1) {
        this.companyPageState.currentPageNumber--;
        this.getCompanyPageNumber(null);
      }
    }
  }

  public openEmailPopUp(automatchCandidate: any) {
    this.hideContextMenu(automatchCandidate);
    this.sessionService.object = {
      currentReqId: this.requisitionId,
      candidateId: automatchCandidate.candidate_id,
      primaryEmail: automatchCandidate.email
    };
    this.dialogsService.open(EmailCandidatesComponent, { width: '935px' });
  }

  hideContextMenu(item) {
    item.displayClass = "displayNone";
  }

  showContextMenu(item) {
    item.displayClass = "displayBlock";
  }


}

export class ResponseData {
  result: string;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class TagCandidateData {
  input: TagCandidateToJobData;
}

export class candidateListData {
  candidateIDList: Array<Number>;
  requisitionIDList: Array<Number>;
  userID: Number;
  organizationID: Number;
}