import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AutoatchEmailService } from './automatch-email.service';
import { NotificationService } from '../../../../common/notification-bar/notification.service';
import { SessionService } from '../../../../session.service';
import { ValidationService } from '../../../../shared/custom-validation/validation.service';

@Component({
  selector: 'alt-automatch-email',
  templateUrl: './automatch-email.component.html',
  styleUrls: ['./automatch-email.component.css'],
  providers: [AutoatchEmailService]
})

export class AutomatchEmailComponent implements OnInit {
  lable: string = "Send";
  emailCandidate: boolean = false;
  data: EmailData = new EmailData();
  recipient: Recipient = new Recipient();
  recipientList: Recipient[];

  messageCode: MessageCode;
  responseData: ResponseData;
  applicantStatusId: String;
  emailInfo: FormGroup;
  primaryEmail: String;

  constructor(public dialogRef: MatDialogRef<AutomatchEmailComponent>, private sanitizer: DomSanitizer, private autoatchEmailService: AutoatchEmailService,
    private sessionService: SessionService, private formBuilder: FormBuilder, private notificationService: NotificationService) {
    this.data = new EmailData();
    this.responseData = new ResponseData();
  }

  ngOnInit() {
    this.emailInfo = this.formBuilder.group({
      toEmail: [this.sessionService.jobSummaryData.emailId, [ValidationService.emailValidator]],
      // fromEmail: [this.sessionService.jobSummaryData.emailId, [ValidationService.emailValidator]],
      ccEmail: ['', [ValidationService.emailValidator]],
      subject: this.sessionService.jobSummaryData.subject,
      content: [this.sessionService.jobSummaryData.content, [ValidationService.isRequiredAndNull]]
    });
  }

  sendEmail(event) {
    this.emailCandidate = true;
    this.recipient.socialUid = this.sessionService.jobSummaryData.socialUid;
    this.recipient.emailId = [];
    this.recipient.emailId.push(this.emailInfo.controls.toEmail.value);
    this.recipient.emailCC = this.emailInfo.controls.ccEmail.value;
    this.recipient.name = this.sessionService.jobSummaryData.candidateName;

    this.recipientList = [];
    this.recipientList.push(this.recipient);

    this.data.subject = this.emailInfo.controls.subject.value;
    this.data.content = this.emailInfo.controls.content.value;
    this.data.jobName = this.sessionService.jobSummaryData.jobName;
    this.data.recipientList = this.recipientList;

    if (this.data.content == null || this.data.content == '') {
      this.notificationService.setMessage("ERROR", "Email body can not be blank");
      return;
    }
    this.autoatchEmailService.sendEmail(this.data).subscribe(out => {
      this.messageCode = out.messageCode;
      this.responseData = out.responseData[0];
      if (this.responseData.status == "success") {
        this.dialogRef.close();
        this.notificationService.setMessage("INFO", "Email sent successfully!");
      }
      else{
        this.dialogRef.close();
        this.notificationService.setMessage("ERROR", "Email couldn't be sent to " + this.responseData.status );
      }
    });
  }
}

export class EmailData {
  recipientList: Recipient[];
  subject: String;
  content: String;
  jobName: String
}

export class Recipient {
  socialUid: string;
  emailId: String[];
  emailCC: String;
  name: String;
}

export interface MessageCode {
  code: String;
  message: String;
  description: String;
}

export class ResponseData {
  status: String;
}
