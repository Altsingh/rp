import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from 'app/session.service';
import { environment } from '../../../../../environments/environment';


@Injectable()
export class AutoatchEmailService {

    constructor(private http: Http, private sessionService: SessionService) { }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }

    getURL() {
        return (this.sessionService.orgUrl + '/rest/altone/sendEmail/socialemail/');
    }
    getTemplateURL(){
        return (this.sessionService.orgUrl + '/rest/altone/sendEmail/socialemailtemplate/');
    }

    getTemplateDemoURL(){
        // let jdCvApiUrl = environment.jdCvApiUrl;
        // jdCvApiUrl = jdCvApiUrl.replace('/Match_Candidates/', '');
        // return (jdCvApiUrl + '/generate_email/');
        return "https://jd-cv.peoplestrong.com/generate_email/";
    }

    public sendEmail(emailData: EmailData) {
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getURL(), JSON.stringify(emailData), options).
            map(res => {
                this.sessionService.check401Response(res.json());
                return res.json()
            });
    }
    public getAutomatchEmailTemplate(){
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.get(this.getTemplateURL(), options).map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()
        });
    }
    
    public getAutomatchEmailTemplateAll(jobDetailsForSocialMatch:any, candidateID:string){
        let request = new CotextualMailRequest();
        request.job_title = jobDetailsForSocialMatch.jobTitle;
        request.skill_array = jobDetailsForSocialMatch.skills;
        request.candidate_id = candidateID;
        request.organization_name = jobDetailsForSocialMatch.organizationName;
        request.recruiter_name = jobDetailsForSocialMatch.userName;
        request.type = 'social';
        let options = new RequestOptions({ headers: this.getHeaders() });
        return this.http.post(this.getTemplateDemoURL(), JSON.stringify(request), options).map(res => {
            this.sessionService.check401Response(res.json());
            return res.json()
        });
    }
}

export class EmailData {
    recipientList: Recipient[];
    content: String;
    subject: String;
}

export class Recipient {
    socialUid: string;
    emailId: String[];
    emailCC: String;
    name: String;
}

export class CotextualMailRequest {
    job_title: string;
    skill_array: string;
    candidate_id: string;
    organization_name: string;
    recruiter_name: string;
    type: string;

}