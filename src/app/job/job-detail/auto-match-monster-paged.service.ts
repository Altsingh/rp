import { Injectable } from '@angular/core';
import { SessionService } from '../../session.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { CamelCaseConverterService } from '../../common/camel-case-converter.service';

@Injectable()
export class AutoMatchMonsterPagedService {

  socialMatchApiUrl = environment.socialMatchApiUrl;
  pagedDataUrl = this.socialMatchApiUrl + '/J2C';
  protected cachedList: any[][] = [];
  totalRows: number;
  state: any;
  cachedData: any;
  q: string;
  page: number;
  pagesize: number;
  minimumExp: String;
  maximumExp: String;
  jobBoardName: String;
  sortByLabel:string;
  sortBy:string;
  public currentPosition: number;
  private lastRequestJSON: any;
  lastRequestSearchString: any;

  private filterOptions = new Subject<any>();
  private filterApplied = new Subject<any>();
  private clearFilters = new Subject<any>();

  constructor(
    private http: Http,
    private sessionService: SessionService,
    private camelCaseConverterService:CamelCaseConverterService
  ) {

  }
  getHeaders(): Headers {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append("Content-Type", "application/json");
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  removeCachedItem(pageNumber: number, pageSize: number, index: number) {
    let removable = (pageNumber - 1) * pageSize + index;
    this.cachedList.splice(removable, 1);
    this.totalRows--;
  }

  syncCandidate(pageNumber: number, pageSize: number, index: number, newcandidate) {
    let updatable = (pageNumber - 1) * pageSize + index;
    this.cachedList[updatable][0] = newcandidate;
  }


  getRequestOptions(): RequestOptions {
    let headers = this.getHeaders();

    let options = new RequestOptions({
      headers: headers
    });

    return options;
  }

  getTotalCandidates() {
    return this.totalRows;
  }

  clearCache() {
    this.cachedList = [];
    this.totalRows = null;
  }

  clearLastRequestJSON() {
    this.lastRequestJSON = null;
  }

  getFilterOptions(): Subject<any> {
    return this.filterOptions;
  }

  getFilterApplied(): Subject<any> {
    return this.filterApplied;
  }

  getClearFilters(): Subject<any> {
    return this.clearFilters;
  }

  getLastRequestJSON() {
    return this.lastRequestJSON;
  }


  getFilterSuggestion(filterType: string, queryString: string): Observable<any> {
    return this.http.get(this.socialMatchApiUrl + '/get_autosuggestion?doc_type=' + filterType + '&query_string=' + queryString).map((response) => {
      return response.json();
    });
  }

  setLastRequestSearchString(searchString) {
    this.lastRequestSearchString = searchString;
  }

  clearLastRequestSearchString() {
    this.lastRequestSearchString = null;
  }

  getLastRequestSearchString() {
    return this.lastRequestSearchString;
  }

  setValues(q: string, page: number, pagesize: number, minimumExp: String, maximumExp: String, jobBoardName: String) {
    this.q = q;
    this.page = page;
    this.pagesize = pagesize;
    this.minimumExp = minimumExp;
    this.maximumExp = maximumExp;
    this.jobBoardName = jobBoardName;
  }
  getMonsterSearchRecords(requestJSON) {
    this.lastRequestJSON = requestJSON;
    this.pagesize = this.pagesize - 1;
    let url = this.sessionService.orgUrl + '/rest/altone/jobboard/'+ this.jobBoardName +'/?'
      + 'page=' + this.page
      + '&pagesize=' + this.pagesize
      + '&q=' + this.q
    if (this.minimumExp != undefined || this.minimumExp != null) {
      requestJSON.minExp = this.minimumExp;
    }
    if (this.maximumExp != undefined || this.maximumExp != null) {
      requestJSON.maxExp = this.minimumExp;
    }
    let options = this.getRequestOptions();
    return this.http.post(url, JSON.stringify(requestJSON), options).map(res => {
      this.sessionService.check401Response(res.json());
      return res.json();// this.camelCaseConverterService.convert(res.json());
    });
  }

}
