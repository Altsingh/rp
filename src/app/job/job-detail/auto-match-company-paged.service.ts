import { Injectable } from '@angular/core';
import { SessionService } from '../../session.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/timeout';

@Injectable()
export class AutoMatchCompanyPagedService {

  pagedDataUrl = environment.jdCvApiUrl;
  protected cachedList: any[][] = [];
  protected currentPage: any[];
  totalRows: number;
  state: any;

  skillList: any[];

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) {

  }

  handlePageResponse(responseJSON, pageNumber, pageSize) {

    if (responseJSON) {
      if (responseJSON.candidates) {
        if (typeof responseJSON.candidates != 'string' && responseJSON.candidates.length > 0) {

          for (let x = 0; x < responseJSON.candidates.length; x++) {
            if (x < pageNumber * pageSize) {
              this.currentPage.push(responseJSON.candidates[x]);
            }
            this.cachedList[(pageNumber - 1) * pageSize + x] = [responseJSON.candidates[x]];
          }
        }
      }
      if (responseJSON.total_candidates) {
        this.totalRows = responseJSON.total_candidates;
      }

    }

  }

  getPage(body, pageNumber, pageSize): Observable<any> {

    this.currentPage = [];


    let startIndex = (pageNumber - 1) * pageSize;
    let endIndex = (pageNumber) * pageSize;

    if (this.totalRows != null) {
      if (endIndex > this.totalRows) {
        endIndex = this.totalRows;

      }
    }

    let fetchStart = 99999999999999999999999999;
    let fetchEnd = 0;

    for (let i = startIndex; i < endIndex; i++) {
      if (this.cachedList[i] != null) {
        this.currentPage.push(this.cachedList[i][0]);
      } else {
        if (i < fetchStart) {
          fetchStart = i;
        }
        if (i > fetchEnd) {
          fetchEnd = i;
        }
      }
    }

    if (fetchStart != 99999999999999999999999999
      && fetchEnd != 0) {

      this.currentPage = [];

      let options = this.getRequestOptions();

      return this.http.post(this.pagedDataUrl, body.toString(), options).timeout(60000)
        .map((response) => {
          let responseJSON = response.json();
          this.sessionService.check401Response(responseJSON);

          this.handlePageResponse(responseJSON, pageNumber, pageSize);

          return this.currentPage;
        });
    }


    return Observable.of(this.currentPage);

  }


  getHeaders(): Headers {
    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    //headers.append("Request-Timeout", "600000");
    return headers;
  }

  removeCachedItem(pageNumber: number, pageSize: number, index: number) {
    let removable = (pageNumber - 1) * pageSize + index;
    this.cachedList.splice(removable, 1);
    this.totalRows--;
  }

  syncCandidate(pageNumber: number, pageSize: number, index: number, newcandidate) {
    let updatable = (pageNumber - 1) * pageSize + index;
    this.cachedList[updatable][0] = newcandidate;
  }

  getRequestOptions(): RequestOptions {
    let headers = this.getHeaders();

    let options = new RequestOptions({
      headers: headers
    });

    return options;
  }

  getTotalCandidates() {
    return this.totalRows;
  }
  clearCache() {
    this.cachedList = [];
    this.totalRows = null;
    this.state = null;
  }

  savePageState(state) {
    this.state = state;
  }

  getPageState() {
    return this.state;
  }

  setSkills(list) {
    this.skillList = list;
  }
  getSkills() {
    return this.skillList;
  }

}
