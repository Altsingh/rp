import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../common/notification-bar/notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParseJobcodeParamPipe } from '../../parse-jobcode-param.pipe';
import { FormatJobcodeParamPipe } from '../../../shared/format-jobcode-param.pipe';
import { JobDetailService } from '../job-detail.service';
import { JobActionService } from '../../job-action/job-action.service';
import { DialogService } from '../../../shared/dialog.service';
import { SessionService } from '../../../session.service';
import { JobCommonServiceService } from '../../job-common-service.service';
import { I18UtilService } from '../../../shared/services/i18-util.service';
import { JobSummaryData } from '../job-summary/job-summary-data';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alt-job-summary-bp',
  templateUrl: './job-summary-bp.component.html',
  styleUrls: ['./job-summary-bp.component.css']
})
export class JobSummaryBpComponent implements OnInit {

  jobSummaryData: JobSummaryData;
  jobDetailService: JobDetailService
  sub: any;
  jobCode: string;
  requisitionID: string;
  subscription: Subscription;
  showloader   = false;
  id:any;
  isJobDisabled: boolean = false;
  featureDataMap:Map<any,any>;
  isAutomatchEnabled : Boolean = true;

  constructor(
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private parser: ParseJobcodeParamPipe,
    private formatter: FormatJobcodeParamPipe,
    jobDetailService: JobDetailService,
    private jobActionBulkService: JobActionService,
    private dialogService: DialogService,
    private sessionService: SessionService,
    private jobCommonServiceService:JobCommonServiceService,
    private i18UtilService: I18UtilService ) {
      window.scroll(0, 0);
      this.jobDetailService = jobDetailService;
      this.jobSummaryData = new JobSummaryData();
      this.jobSummaryData.jobCode = "Loading...";
      this.jobSummaryData.jobName = "Loading...";
      this.jobSummaryData.postedOn = "Loading...";
      this.jobSummaryData.publishedTo = 0;
      this.jobSummaryData.summaryOrgUnit = "Loading...";
      this.jobSummaryData.summaryWorkSite = "Loading...";
      this.jobSummaryData.targetOn = "Loading...";
      this.jobSummaryData.teamSize = 0;
      this.jobSummaryData.sourcingGuidling = "Loading...";
      this.jobDetailService.setJobSummaryBPComponent(this);
     }

  ngOnInit() {
  }

}
