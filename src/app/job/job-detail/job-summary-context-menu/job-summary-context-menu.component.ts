import { JobCommonServiceService } from './../../job-common-service.service';
import { NotificationSeverity } from './../../../candidate/candidate-list/taggedCandidates/copy-to-another-job/copy-to-another-job.component';
import { NotificationService } from 'app/common/notification-bar/notification.service';
import { StatusChangeService } from './../status-change/status-change.service';
import { StatusChangeComponent } from './../status-change/status-change.component';
import { DialogService } from 'app/shared/dialog.service';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { SessionService } from 'app/session.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-job-summary-context-menu',
  templateUrl: './job-summary-context-menu.component.html',
  styleUrls: ['./job-summary-context-menu.component.css'],
  providers: [StatusChangeService]
})
export class JobSummaryContextMenuComponent implements OnInit {

  displayClass: String = "displayNone";
  statusTypeList: any[];
  isJobDisabled: boolean = false;
  featureDataMap: Map<any,any>;
  systemLevel : boolean = false;

  @Input() requisitionID:Number;
  @Input() jobCode:String;
  @Input() jobStatusLabel:String;
  @Input() jobStatus:any;
  @Output() jobStatusOutput: EventEmitter<any> = new EventEmitter();

  constructor(
    private dialogsService: DialogService,
    public statusChangeService: StatusChangeService,
    private notificationService: NotificationService,
    private jobCommonServiceService:JobCommonServiceService,
    private sessionService : SessionService,
    private i18UtilService: I18UtilService
  ) { }

  ngOnInit() { 

    this.featureDataMap=this.sessionService.featureDataMap;
    this.jobCommonServiceService.isJobDisabled().subscribe( (res) => {
    this.isJobDisabled = res;
  } );

  this.statusChangeService.getJobStatusesList(this.systemLevel).subscribe( (response) => {
    if( response && response.responseData ){
      this.statusTypeList = response.responseData;
      for( var i = 0; i < this.statusTypeList.length; i++ ){
        if( this.statusTypeList[i].contentType == "CLOSED" ){
          this.statusTypeList.splice( i, 1 );
        }
      }
    }
  } );
  }

  hideContextMenu() {
    this.displayClass = "displayNone";
  }
  

  showContextMenu() {
    this.displayClass = "displayBlock";
  }

  changeStatus(status){
    if(this.jobStatusLabel==status.label || this.jobStatus.toLowerCase() ==='CLOSED'.toLowerCase()){
      return;
    }

    this.notificationService.clear();
    if( this.jobStatusLabel == status.label ){
      this.i18UtilService.get('job.warning.sameState', {'state': status.label}).subscribe((res: string) => {
        this.notificationService.setMessage(NotificationSeverity.WARNING, res);
      });
      return;
    }

    this.dialogsService.setDialogData(status);

    let dialogData = [];
    dialogData.push({
      jobCode: this.jobCode,
      requisitionID: this.requisitionID,
      jobStatusLabel: this.jobStatusLabel
    });
    this.dialogsService.setDialogData1(dialogData);

    this.dialogsService.open(StatusChangeComponent, { width: '285px' }).subscribe((event)=>{
      if((event==="Success") ){
        this.jobStatusLabel = status.label;
        this.jobStatusOutput.emit(status.contentType);
        this.hideContextMenu();
        this.i18UtilService.get('job.success.jobStateChanged', {'state': status.label}).subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
      }if(event==="Failed"){
        this.i18UtilService.get('job.success.jobStateChangeFailed').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.INFO, res);
        });
      }        
    });;
  }

  
  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    } else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

checkForOrg(){
  let demo = false;
  if(location.href.indexOf('altrecruit.peoplestrong') > 0
  || location.href.indexOf('hiredplus') > 0){
    demo = true;
  }
  return demo;
}

}
