import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class JobCommonServiceService {

  private jobDisabled = new Subject<boolean>();
  private recruitersTeamUpdate = new Subject<string>();
  public isJobDisabledVar :boolean = false;
  public sysRequisitionStatus :any;
  public requisitionLabel :any;

  constructor() {
    this.jobDisabled.next(false);
    this.isJobDisabledVar = false;
  }
  
  public isDisableCriteria(status: any){
    if( status == 'CLOSED' || status == 'PCLOSE' || status == 'CANCELLED' || status == 'ONHOLD' ){
      return true;
    }
    return false;
  }

  public disableJobByStatus(status: any){
    this.sysRequisitionStatus = status;
    if( this.isDisableCriteria(status) ){
      this.jobDisabled.next(true);
      this.isJobDisabledVar = true;
    }else{
      this.jobDisabled.next(false);
      this.isJobDisabledVar = false;
    }
  }

  public recruitersTeamUpdated(count: string){
    this.recruitersTeamUpdate.next(count);
  }

  public isJobDisabled(): Observable<boolean>{
    return this.jobDisabled.asObservable();
  }

  public isRecruitersTeamUpdated(): Observable<string>{
    return this.recruitersTeamUpdate.asObservable();
  }

  public setRequisitionLabel(label: any){
    this.requisitionLabel = label;
  }

  public getRequisitionLabel(){
    return this.requisitionLabel;
  }

}
