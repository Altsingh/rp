import { Component, OnInit } from '@angular/core';
import { JobListMyjobsPagedService } from '../job-list/job-list-myjobs-paged.service';
import { JobListAlljobsPagedService } from '../job-list/job-list-alljobs-paged.service';
import { JobListService } from '../job-list/job-list.service';
import { PageState } from '../../shared/paginator/paginator.component';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder } from '@angular/forms';
import { JobFilterTO } from '../job-list/job-filter-to';
import { JobListResponse } from 'app/common/job-list-response';
import { MessageCode } from 'app/common/message-code';
import { FormatJobcodeParamPipe } from '../../shared/format-jobcode-param.pipe';
import { SessionService } from '../../session.service';
import { DialogService } from '../../shared/dialog.service';
import { CommonService } from '../../common/common.service';
import { NameinitialService } from '../../shared/nameinitial.service';
import { Router } from '@angular/router';
import { JobActionService } from '../job-action/job-action.service';
import { NotificationService } from '../../common/notification-bar/notification.service';
import { JobDetailService } from '../job-detail/job-detail.service';
import { JobCommonServiceService } from '../job-common-service.service';
import { AssignToVendorsComponent } from '../job-action/assign-to-vendors-bulk/assign-to-vendors-bulk.component';
import { PublishToInternalBulkComponent } from '../job-action/publish-to-internal-bulk/publish-to-internal-bulk.component';
import { PublishToCandidateBulkComponent } from '../job-action/publish-to-candidate-bulk/publish-to-candidate-bulk.component';
import { PublishToEmployeeBulkComponent } from '../job-action/publish-to-employee-bulk/publish-to-employee-bulk.component';
import { AssignToRecruiterComponent } from '../job-action/assign-to-recruiter/assign-to-recruiter.component';
import { AssignToRecuiterBulkComponent } from '../job-action/assign-to-recuiter-bulk/assign-to-recuiter-bulk.component';
import { CreatedJobListPagedService } from './created-job-list-paged.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';


@Component({
  selector: 'alt-created-job-list',
  templateUrl: './created-job-list.component.html',
  styleUrls: ['./created-job-list.component.css'],
  providers:[JobListService, JobListAlljobsPagedService, JobListMyjobsPagedService,CreatedJobListPagedService]
})
export class CreatedJobListComponent implements OnInit {
  showloader: boolean = false;
  load: number = 3;
  pageState: PageState = new PageState();
  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;
  filterTO: JobFilterTO;

  loaderIndicator: string;
  filterHidden: boolean = true;
  applyRecruiterFilter: boolean = true;
  showStyle: false;
  isClassVisible: false;
  jobListData: JobListResponse[];
  messageCode: MessageCode;
  subscription: any;
  JOB_STATUS_CLASS: String = "";
  randomcolor: string[] = [];
  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";
  selected: any[] = [];
  selectAll: boolean;
  currentURL: string;
  pageTitle: string;
  pageTitleLabel: string;
  publishedToTotal: number;
  jobDetails: any;
  RecruitersOfJob: any;
  RecruiterList: any;
  formatJobcodeParamPipe: FormatJobcodeParamPipe = new FormatJobcodeParamPipe();
  featureDataMap: Map<any,any>;
  isBulkMenu : Boolean = true;

  constructor(private jobListService: JobListService, private DialogService: DialogService,
    private sessionService: SessionService, private commonService: CommonService,
    private nameinitialService: NameinitialService,
    private router: Router, private jobActionBulkService: JobActionService,
    private notificationService: NotificationService,
    private jobDetailService: JobDetailService,
    private jobListAllJobsPagedService: JobListAlljobsPagedService,
    private createdJobListPagedService: CreatedJobListPagedService,
    private jobListMyJobsPagedService: JobListMyjobsPagedService,
    private jobCommonServiceService: JobCommonServiceService,
    private formBuilder: FormBuilder, private i18UtilService: I18UtilService) {
    this.randomcolor = this.nameinitialService.randomcolor;
    this.filterTO = new JobFilterTO();
    this.queryFormGroup = this.formBuilder.group({
      query: ['']
    });
    this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {
      if (this.queryTimeout != null) {
        clearTimeout(this.queryTimeout);
      }
      this.queryTimeout = setTimeout(() => {
        let query = this.queryFormGroup.controls.query.value;
        if (query.length >= 3) {
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          this.filterTO.searchString = query;
          if (this.pageTitle === this.pageTitleLabel) {
            this.createdJobListPagedService.clearCache();
            this.loadCreatedJobsData();
            this.loadCreatedJobsTotalRows();
          } 
        }
        if (query.trim().length == 0) {
          this.filterTO.searchString = null;
          this.filterTO.pageNumber = 1;
          this.pageState.currentPageNumber = 1;
          this.filterTO.pageSize = this.pageState.pageSize;
          if (this.pageTitle === this.pageTitleLabel) {
            this.createdJobListPagedService.clearCache();
            this.loadCreatedJobsData();
            this.loadCreatedJobsTotalRows();
          } 
        }
      }, 200);

    });
    //this.jobActionBulkService.setJobComponent(this);
    this.featureDataMap = this.sessionService.featureDataMap;
    if(this.featureDataMap.get("PUBLISH_TO_EMPLOYEE_REFERRAL") == 3 && this.featureDataMap.get("PUBLISH_TO_IJP") == 3 && this.featureDataMap.get("PUBLISH_TO_CP") == 3 && this.featureDataMap.get("PUBLISH_TO_VENDOR") == 3 && this.featureDataMap.get("ASSIGN_TO_RECRUITER") == 3){
      this.isBulkMenu = false;
    }
  }

  ngOnInit() {
    this.selected = [];
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.loaderIndicator = "loader";
    this.filterHidden = true;
    this.commonService.showRHS();

    this.jobListService.getPublishToTotalCount().subscribe(out => {
      this.publishedToTotal = out.response
    });
    this.i18UtilService.get('job.job_list.createdJobs').subscribe((res: string) => {
      this.pageTitleLabel = res;
      if (this.router.url.endsWith('createdjobs')) {
        this.pageTitle = this.pageTitleLabel;
        this.applyRecruiterFilter = true;
        this.loadCreatedJobsData();
        this.loadCreatedJobsTotalRows();
      } 
    }); 
  }

  public isThisJobDisabled(jobStatus){
    return this.jobCommonServiceService.isDisableCriteria(jobStatus);
  }
  loadCreatedJobsData() {
    this.selectAll = false;
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }

    this.pageLoadingSubscription = this.createdJobListPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        console.log(response)
        this.jobListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
      this.areAllSelected();
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  loadCreatedJobsTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.createdJobListPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }

  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;
    if (this.pageTitle === this.pageTitleLabel) {
      this.loadCreatedJobsData();
    } 
  }

  public sort(sortParam: string, order: string) {

    this.sortBy = sortParam;
    this.sortOrder = order;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;

    if (this.pageTitle === this.pageTitleLabel) {
      this.createdJobListPagedService.clearCache();
      this.loadCreatedJobsData();
    } 
  }

  areAllSelected() {
    if (this.jobListData != null && this.jobListData.length > 0) {
      for (let x = 0; x < this.jobListData.length; x++) {
        if (typeof this.jobListData[x].selected == 'undefined' || (typeof this.jobListData[x].selected == 'boolean' && !this.jobListData[x].selected)) {
          this.selectAll = false;
          return;
        }
      }
      this.selectAll = true;
    } else {
      this.selectAll = false;
    }
  }

  reloadNgOnInit() {
    if (this.router.url.endsWith('createdjobs')) {
      this.pageTitle = this.pageTitleLabel;
      this.createdJobListPagedService.clearCache();
      this.loadCreatedJobsData();
      this.loadCreatedJobsTotalRows();
    } 
  }

  gotoJobDetail(data) {
    this.jobDetailService.object = data.requisitionId;
    this.jobDetailService.objectType = 'REQUISITION_ID';
    this.router.navigateByUrl('/job/edit-job/' + this.formatJobcodeParamPipe.transform(data.requisitionCode) + '/job-details');
  }

  ngOnDestroy(): void {
    if (this.subscription != null) {
      this.subscription.unsubscribe();
    }
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }

  }

  toggleFilter() {
    this.filterHidden = !this.filterHidden;
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.city.length;
  }

  getJobStatusClass(requisitionStatus: String) {
    if (requisitionStatus === 'OPEN') {
      return "job-active";
    } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
      return "job-close";
    } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
      return "job-hold";
    } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
      return "job-pending";
    } else if (requisitionStatus === 'CANCELLED' ) {
      return "job-cancelled";
    }else if (requisitionStatus === 'APPROVALPENDING') {
      return "job-approved-pending";
    } else if (requisitionStatus === 'WITHDRAWN') {
      return "job-withdrawn";
    } else if (requisitionStatus === 'REJECTED') {
      return "job-rejected";
    } 
  }

  onSorted(params: string[]) {
    this.filterTO.sortBy = params[0];
    this.filterTO.sortOrder = params[1];
    this.filterTO.startCreationDate = null;
    this.filterTO.endCreationDate = null;
    this.filterTO.startModificationDate = null;
    this.filterTO.endModificationDate = null;
    
    if (this.pageTitle === this.pageTitleLabel) {
      this.createdJobListPagedService.clearCache();
    } 
    this.ngOnInit();
  }

  onFilter(params: any[]) {
    let hiringManagerFilter = false;
    let recruiterFilter = false;
    let jobStatusFilter = false;
    let locationFilter = false;
    let postedFilter = false;
    for (let param of params) {
      if (param.name == 'HIRING_MANAGER') {
        this.filterTO.hiringManagerID = param.value.value;
        hiringManagerFilter = true;
      } else if (param.name == 'RECRUITER') {
        this.filterTO.recruiterID = param.value.value;
        recruiterFilter = true;
      } else if (param.name == 'JOB_STATUS') {
        this.filterTO.jobStatus = param.value.value;
        jobStatusFilter = true;
      } else if (param.name == 'LOCATION') {
        this.filterTO.joiningLocation = param.value.value;
        locationFilter = true;
      } else if (param.name == 'POSTED_ON') {
        this.filterTO.postedON = param.value.value;
        postedFilter = true;
      }
    }
    if (!hiringManagerFilter) {
      this.filterTO.hiringManagerID = null;
    }
    if (!recruiterFilter) {
      this.filterTO.recruiterID = null;
    }
    if (!jobStatusFilter) {
      this.filterTO.jobStatus = null;
    }
    if (!locationFilter) {
      this.filterTO.joiningLocation = null;
    }
    if (!postedFilter) {
      this.filterTO.postedON = null;
    }
    if (this.pageTitle === this.pageTitleLabel) {
      this.createdJobListPagedService.clearCache();
    } 
    this.ngOnInit();
  }
  onDateFilter(param: any) {
    if (param.name == 'start_date' && this.sortBy == 'createdDate') {
      this.filterTO.startCreationDate = param.value;
    } else if (param.name == 'start_date' && this.sortBy == 'modifiedDate') {
      this.filterTO.startModificationDate = param.value;
    } else if (param.name == 'end_date' && this.sortBy == 'createdDate') {
      this.filterTO.endCreationDate = param.value;
    } else if (param.name == 'end_date' && this.sortBy == 'modifiedDate') {
      this.filterTO.endModificationDate = param.value;
    } else if(param.name == 'start_date'){
      this.filterTO.startCreationDate = param.value;
    } else if(param.name == 'end_date'){
      this.filterTO.endCreationDate = param.value;
    }
    if (this.pageTitle === this.pageTitleLabel) {
      this.createdJobListPagedService.clearCache();
    } 
    this.ngOnInit();
  }
  singleSelectEvent(event: any) {
    if (!event.selected) {
      let index = this.selected.indexOf(event);
      this.selected.splice(index, 1);
    } else {
      this.selected.push(event);
    }
    this.areAllSelected();

  }
  changed(event) {
    this.showloader = event;
  }
  allSelectEvent() {
    let index = 0;
    for (let data of this.jobListData) {
      data.selected = this.selectAll;
      if( this.isThisJobDisabled(data.sysRequisitionStatus) ){
        data.selected = false;
      }
      index++;
      if (this.selectAll && data.selected && !this.selected.includes(data)) {
        this.selected.push(data);
      } else if (!this.selectAll) {
        let i = this.selected.indexOf(data);
        this.selected.splice(i, 1);
      }
    }
  }

  openBulkAction(actiontype) {
    this.notificationService.clear();
    if (this.selected.length == 0) {
      this.i18UtilService.get('job.warning.selectJob').subscribe((res: string) => {
        this.notificationService.setMessage("WARNING", res);
      });
      this.notificationService.setMessage("WARNING", "Please select a Job!");
      return;
    }
    this.jobActionBulkService.setDataList(this.selected)
    if (actiontype == 'V') {
      this.showloader = true;
      this.jobActionBulkService.getVendorsForOrganization().subscribe(res => {
        this.showloader = false;
        this.DialogService.setDialogData(res);
        this.DialogService.open(AssignToVendorsComponent, { width: '935px' }).subscribe(result => {
          if (result != undefined && result == 'success') {
            this.reloadNgOnInit();
          }
        });
      });
    } else if (actiontype == 'R') {
      this.showloader = true;
      this.jobActionBulkService.getRecruiterList().subscribe(res => {
        this.DialogService.setDialogData(res);
        this.showloader = false;
        this.DialogService.open(AssignToRecuiterBulkComponent, { width: '935px' }).subscribe(result => {
          if (result != undefined && result == 'success') {
            this.reloadNgOnInit();
          }
        });
      });
    }
    else if (actiontype == 'I') {
      this.DialogService.open(PublishToInternalBulkComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && result == 'success') {
          this.reloadNgOnInit();
        }
      });
    }
    else if (actiontype == 'C') {
      this.DialogService.open(PublishToCandidateBulkComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && result == 'success') {
          this.reloadNgOnInit();
        }
      });
    }
    else if (actiontype == 'E') {
      this.DialogService.open(PublishToEmployeeBulkComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && result == 'success') {
          this.reloadNgOnInit();
        }
      });
    }
  }
  
  OpenAssignJobToRec(data: JobListResponse) {
    let requisitionId: Number = data.requisitionId;
    let jobCode: String = data.requisitionCode;
    this.sessionService.object = {
      requisitionId: requisitionId,
      jobCode: jobCode
    };
    this.showloader = true;
    Promise.all([this.jobActionBulkService.getJobDetailtoPromise(),
    this.jobActionBulkService.getRecruitersOfJobToPromise(),
    this.jobActionBulkService.getRecruiterListtoPromise()]).then(res => {
      this.DialogService.setDialogData(res[0]);
      this.DialogService.setDialogData1(res[1]);
      this.DialogService.setDialogData2(res[2])
      this.showloader = false;
      this.DialogService.open(AssignToRecruiterComponent, { width: '935px' }).subscribe(result => {
        if (result != undefined && typeof result == 'number') {
          data.teamCount = data.teamCount + result;
        }
      });
    });
  }
}
