import { Component, OnInit } from '@angular/core';
import { ParseJobcodeParamPipe } from '../parse-jobcode-param.pipe';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'alt-edit-job-business',
  templateUrl: './edit-job-business.component.html',
  styleUrls: ['./edit-job-business.component.css'],
  providers: [ParseJobcodeParamPipe]
})
export class EditJobBusinessComponent implements OnInit {

  id: number;
  private sub: any
  constructor(private route: ActivatedRoute, private parser: ParseJobcodeParamPipe) { }

  ngOnInit() {
   
  }

}
