export class JobListResponse {
    draftRequisitionID: Number;
    requisitionId: Number;
    hiringManager: String;
    requisitionCode: String;
    jobTitle: String;
    orgUnitCode: String;
    joiningLocation: String;
    sysRequisitionStatus: String;
    jobStatusClass: String;
    createdDate: String;
    modifiedDate: String;
    selected: boolean;
    remainingDays: number;
    remainingOpenings: number;
    teamCount: number;
    publishedToCount: number;
    actorUserName: String;
    stageStatus: String;
    stageStatusID: Number;
    sysStageStatus: String;
}
