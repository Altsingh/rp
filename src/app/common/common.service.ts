import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { SessionService } from '../session.service';
import { Http, RequestOptions, Headers } from '@angular/http';


@Injectable()
export class CommonService {

  private rhsHidden = new Subject<boolean>();
  private whichSidebar = new Subject<string>();
  private windowClick = new Subject<any>();
  
  private rhsMonsterDisabled = new Subject<boolean>();

  constructor(private http: Http, private sessionService:SessionService) {
    this.rhsHidden.next(true);
    this.rhsMonsterDisabled.next(true);
  }

  public isRHSHidden(): Observable<boolean> {
    return this.rhsHidden.asObservable();
  }

  public getSideBar(): Observable<string> {
    return this.whichSidebar.asObservable();
  }

  public setSideBar(sideBar) {
    return this.whichSidebar.next(sideBar);
  }

  hideRHS() {
    this.rhsHidden.next(true);
  }

  showRHS() {
    this.rhsHidden.next(false);
  }

  public isrhsMonsterDisabled(): Observable<boolean> {
    return this.rhsMonsterDisabled.asObservable();
  }

  disableRhsMonster() {
    this.rhsMonsterDisabled.next(true);
  }

  enableRhsMonster() {
    this.rhsMonsterDisabled.next(false);
  }

  click(event) {
    this.windowClick.next(event);
  }

  onclick(): Observable<any> {
    return this.windowClick.asObservable();
  }
  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
}
  getCPCredentials(applicantStatusId){
    let options = new RequestOptions({ headers: this.getHeaders() });
      return this.http.post(this.sessionService.orgUrl + '/rest/altone/candidate/cpcredentials/' + applicantStatusId,{"cpFilter":"cpmail"},options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
    }
}
