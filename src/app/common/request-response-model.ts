export class RequestResponseModel {
    responseData    :   any;
    messageCode     :   MessageCode;

    constructor(code:string, entity:String) {
        this.messageCode = new MessageCode();
        this.messageCode.code = code;
        if(entity != null && entity == 'DUPLICACY') {
            this.responseData = new DuplicacyCheckResponseData();
        } else {
            this.responseData = null;
        }
    }

}

class MessageCode {
    code            :   string;
    message         :   string;
    description     :   string;   
}

class DuplicacyCheckResponseData {
    employeeCode    :   string;
}