export class StageActionListMaster {
    workflowActionID    :   Number;
    actionLabel         :   String;
    sysActionType       :   String;
    value               :   Number;
}
