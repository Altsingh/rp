export class VendorListRequest {
    cityID: Number;
    cityName: String;
    description: String;
    groupID: Number;
    groupName: String;
    industryName: String;
    industyID: Number;
    location: String;
    organizationID: Number;
    others: String;
    pincodeID: Number;
    specialisation: String;
    userID: Number;
}


export class VendorListResponse {
    cityID: Number;
    cityName: String;
    description: String;
    groupID: Number;
    groupName: String;
    industryName: String;
    industyID: Number;
    location: String;
    organizationID: Number;
    others: String;
    pincodeID: Number;
    specialisation: String;
    userID: Number;
    value: Number;
    label: String;
}

export class VendorRoleListResponse {
    roleId: Number;
    roleName: String;
    roleType:String;
    description: String;
    id: Number;
    name: String;
}

export class VendorActiveListResponse {
    id: Number;
    name: String;
    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}
export class IndustryResponse {
    id : Number;
    name : String;
    code: String;
    value: Number;
    label: String;
}