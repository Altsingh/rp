export class CandidateAddressTO {
    addressLine1: any;
    addressLine2: any;
    city: any;
    state: any;
    country: any;
    pincode: any;
}