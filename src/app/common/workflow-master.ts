export class WorkflowMaster {
    workflowID      :   Number;
    workflowName    :   String;
    numberOfOpening :   Number;
    value           :   any;
    label           :   String;
}
