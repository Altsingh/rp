import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class PagedDataService {

  pagedDataUrl: string;
  pagedDataRowCountUrl: string;
  protected cachedList: any[][] = [];
  protected currentPage: any[];
  private postData: any;
  totalRows: number;

  constructor(
    protected http: Http,
    protected sessionService: SessionService
  ) {
    this.handlePageResponse.bind(this);
    this.getHeaders.bind(this);
    this.getRequestOptions.bind(this);
  }

  initialize(
    pagedDataUrl: string,
    pagedDataRowCountUrl: string
  ) {
    this.pagedDataUrl = pagedDataUrl;
    this.pagedDataRowCountUrl = pagedDataRowCountUrl;
  }

  clearCache() {
    this.cachedList = [];
    this.totalRows = null;
  }

  getPage(filterTO): Observable<any> {

    this.currentPage = [];
    let pageNumber = filterTO.pageNumber;
    let pageSize = filterTO.pageSize;

    let startIndex = (pageNumber - 1) * pageSize;
    let endIndex = (pageNumber) * pageSize;

    if (this.totalRows != null) {
      if (endIndex > this.totalRows) {
        endIndex = this.totalRows;

      }
    }

    let fetchStart = 99999999999999999999999999;
    let fetchEnd = 0;

    for (let i = startIndex; i < endIndex; i++) {
      if (this.cachedList[i] != null) {
        this.currentPage.push(this.cachedList[i][0]);
      } else {
        if (i < fetchStart) {
          fetchStart = i;
        }
        if (i > fetchEnd) {
          fetchEnd = i;
        }
      }
    }

    if (fetchStart != 99999999999999999999999999
      && fetchEnd != 0) {

      this.currentPage = [];

      let options = this.getRequestOptions();

      return this.http.post(this.sessionService.orgUrl + this.pagedDataUrl, filterTO, options)
        .map((response) => {
          let responseJSON = response.json();
          this.sessionService.check401Response(responseJSON);

          this.handlePageResponse(responseJSON, pageNumber, pageSize);

          return this.currentPage;
        });
    }


    return Observable.of(this.currentPage);

  }

  getPageWithTotalRecords(filterTO): Observable<any> {
    let pageData: PageData = new PageData();
    this.currentPage = [];
    let pageNumber = filterTO.pageNumber;
    let pageSize = filterTO.pageSize;

    let startIndex = (pageNumber - 1) * pageSize;
    let endIndex = (pageNumber) * pageSize;

    if (this.totalRows != null) {
      if (endIndex > this.totalRows) {
        endIndex = this.totalRows;

      }
    }

      this.currentPage = [];

      let options = this.getRequestOptions();

      return this.http.post(this.sessionService.orgUrl + this.pagedDataUrl, filterTO, options)
        .map((response) => {
          let responseJSON = response.json();
          this.sessionService.check401Response(responseJSON);

          this.handlePageResponse(responseJSON, pageNumber, pageSize);
          pageData.data = this.currentPage;
          pageData.totalRecords = responseJSON.totalRecords;
          return pageData;
        });

  }


  handlePageResponse(responseJSON, pageNumber, pageSize) {

    if (responseJSON) {
      if (responseJSON.responseData) {
        if (responseJSON.responseData.length > 0) {

          for (let x = 0; x < responseJSON.responseData.length; x++) {
            if (x < pageNumber * pageSize) {
              this.currentPage.push(responseJSON.responseData[x]);
            }
            this.cachedList[(pageNumber - 1) * pageSize + x] = [responseJSON.responseData[x]];
          }
        }
      }
    }
  }


  getRequestOptions(): RequestOptions {
    let headers = this.getHeaders();

    let options = new RequestOptions({
      headers: headers
    });
    return options;
  }

  getTotalRows(filterTO): Observable<any> {

    let options = new RequestOptions({ headers: this.getHeaders() });

    return this.http.post(this.sessionService.orgUrl + this.pagedDataRowCountUrl, filterTO, options)
      .map((response) => {
        let responseJSON = response.json();
        this.sessionService.check401Response(responseJSON);

        if (responseJSON) {
          if (typeof responseJSON.response == 'number' && responseJSON.response != -1) {
            this.totalRows = responseJSON.response;
            return responseJSON.response;
          }else{
            return -1;
          }
        }
        return 0;
      });

  }

  removeCachedItem(pageNumber: number, pageSize: number, index: number) {
    let removable = (pageNumber - 1) * pageSize + index;
    this.cachedList.splice(removable, 1);
  }

  getHeaders(): Headers {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  public getDefaultPageConfig() {
    let config: PageConfig = new PageConfig();
    config.pageNumber = 1;
    config.pageSize = 5;
  }

}

export class PageConfig {
  pageNumber: number;
  pageSize: number;
  searchKeywords: string[];
  filterKeywords: any;
  sortBy: string[];
  totalRecords: number;
}

export class PageData {
  data: any[];
  totalRecords: number;
}
