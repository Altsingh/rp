export class OrgunitMaster {
    orgunitType :   string;
    orgunits    : SelectItem[];
    parentOrgunitID :   Number;
    selectedUnitID : Number;

    constructor (orgunitType : string, orgunits : SelectItem[], parentOrgunitID :   Number) {
        this.orgunitType = orgunitType;
        this.orgunits = orgunits;
        this.parentOrgunitID = parentOrgunitID;
    }
}

export class OrgunitTO {
    unitID   :   Number;
    unitName :   String;

    constructor (unitID : Number, unitName : String) {
        this.unitID = unitID;
        this.unitName = unitName;
    }
}

export class SelectItem {
    label: string;
    value: any;
}
