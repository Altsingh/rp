export class PincodeMaster {
    pinCodeID       :   Number;
    pinCode         :   Number;
    postOfficeName  :   String;
    pinCodeDisplay  :   String;

    constructor(pinCodeID : Number, pinCodeDisplay : String) {
        this.pinCodeID = pinCodeID;
        this.pinCodeDisplay = pinCodeDisplay;
    }
}
