import { Injectable } from '@angular/core';
import { SessionService } from '../session.service';
import { Http, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class CachedData {

  private value: any;


  private restUrl: String;
  private httpMethod: String;
  private postData: any;

  constructor(
    private http: Http,
    private sessionService: SessionService) {

  }

  initialize(restUrl: String,
    httpMethod: String,
    postData: any) {
    this.restUrl = restUrl;
    this.httpMethod = httpMethod;
    this.postData = postData;
  }

  getData(): Observable<any> {

    if (this.value) {

      return Observable.of(this.value);
    } else {

      if (this.httpMethod == 'GET') {

        return this.http.get(this.sessionService.orgUrl + "" + this.restUrl, { params: this.postData })
          .map(response => { this.value = response.json(); return response.json(); });

      } else if (this.httpMethod == 'POST') {

        return this.http.post(this.sessionService.orgUrl + "" + this.restUrl, this.postData)
          .map(response => { this.value = response.json(); return response.json(); });

      }

      return Observable.of(this.value);
    }
  }

  public clearCache() {
    this.value = null;
  }

}
