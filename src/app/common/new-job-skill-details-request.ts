export class NewJobSkillDetailsRequest {
    requisitionID           :   Number;
    draftRequisitionID  :   Number;
    skillList           :   SkillType[];
    certificationList           :   String[];
    minQualificationList           :   String[];
    languageList           :   String[];
    modifiedBy              :   Number;
}

export class SkillType{
    text : string;
    mandatory:boolean;
}
