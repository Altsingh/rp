import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NotificationService {

  private messages = new Subject<string[]>();
  private messageStore: string[] = [];
  severity: string;
  icon: string;
  timer: any;

  constructor() {
    this.messages.next([]);
    this.severity = "";
    this.icon = "";
  }

  public getMessages(): Observable<string[]> {
    return this.messages.asObservable();
  }
  public getTranslatedMessage( message: string){

  }

  public clear() {
    this.messageStore = [];
    this.severity = "";
    this.icon = "";
    this.messages.next([]);
  }

  public setMessage(severity: string, message: string) {
    this.severity = severity;
    this.messageStore = [message];
    this.messages.next([message]);

    if (this.timer != null) {
      clearTimeout(this.timer);
    }

    this.timer = setTimeout(() => {
      this.clear();
      this.timer = null;
    }, 5000);
  }

  public getSeverity(): string {
    return this.severity;
  }

  public addMessage(severity: string, message: string) {
    if (this.messageStore.indexOf(message) == -1) {
      this.severity = severity;
      this.messageStore.push(message);
      this.messages.next(this.messageStore);

      if (this.timer != null) {
        clearTimeout(this.timer);
      }

      this.timer = setTimeout(() => {
        this.clear();
        this.timer = null;
      }, 5000);
    }
  }

}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}

