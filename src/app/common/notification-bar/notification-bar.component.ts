import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { NotificationService, NotificationSeverity } from './notification.service';

@Component({
  selector: 'alt-notification-bar',
  templateUrl: './notification-bar.component.html',
  styleUrls: ['./notification-bar.component.css']
})
export class NotificationBarComponent implements OnInit {

  messages: string[];
  severityClass: string;
  showClass: string;
  severityIcon: string;
  shouldStick: boolean;
  subscription: Subscription;

  constructor(private notificationService: NotificationService) {
    this.messages = [];
    this.severityClass = "";
    this.severityIcon = "";
    this.subscription = this.notificationService.getMessages().subscribe(messages => {
    this.messages = messages;
      this.setSeverity(this.notificationService.getSeverity())
    });
  }


  ngOnInit() {
        window.addEventListener('scroll', (e) => {
        if (window.pageYOffset > 100) {
            this.shouldStick = true;
        } else {
            this.shouldStick = false;
        }
    });
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  clearMessages() {
    this.notificationService.clear();
  }

  setSeverity(severity: string) {
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityIcon = "successful";
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityIcon = "warning-icon";
        this.severityClass = "warning";
        break;
      default:
        this.severityIcon = "";
        this.severityClass = "";
        break;
    }
  }
}
