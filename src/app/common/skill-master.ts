export class SkillMaster {
    skillID         :   Number;
    skillName       :   string;

    constructor(skillID : Number, skillName : string) {
        this.skillID = skillID;
        this.skillName = skillName;
    }
}
