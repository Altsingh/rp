export class CurrencyMaster {
    currencyCodeID      :   Number;
    currencyCode        :   String;
    currency            :   String;
    symbol              :   String;

    constructor(currencyCodeID:Number, currencyCode:String, currency:String, symbol:String) {
        this.currencyCodeID=currencyCodeID;
        this.currencyCode=currencyCode;
        this.currency=currency;
        this.symbol=symbol;
    }
}
