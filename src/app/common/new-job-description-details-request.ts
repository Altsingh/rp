export class NewJobDescriptionDetailsRequest {
    requisitionID           :   Number;
    draftRequisitionID  :   Number;
    jobDescription      :   String;
    modifiedBy              :   Number;
}
