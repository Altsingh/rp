import { PaystructureInstalments } from "app/candidate/candidate-tagged-detail/hiring-process/fitment/paystructure-instalments.model";

export class ApplicantPaystructureDetailsTo {
    payCodeName : String;
    sysPayCode  : String;
    effectiveDate: Date;
    amount      : any;
    annualAmount: any;
    paystructureType: any;
    frequency: any;
    toBeGiven: any;
    detailsInstalments: Array<PaystructureInstalments>;
}