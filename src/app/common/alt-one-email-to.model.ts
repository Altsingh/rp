export class AltOneEmailTo {
    mailFROM: String;
	mailTO: String;
    mailCC: String;
    mailBCC: String;
	subject: String;
	content: String;
	sequence: String;
}