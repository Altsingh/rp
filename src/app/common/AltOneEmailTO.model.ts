export class AltOneEmailTO {
    mailFROM: String;
	mailTO: String;
    mailCC: String;
    mailBCC: String;
	subject: String;
	content: String;
    sequence: String;
    nonEditable: Boolean;
    confirmMessage: String;
}