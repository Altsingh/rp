export class NewJobAddToTemplateDetailsRequest {
    requisitionID           :   Number;
    draftRequisitionID      :   Number;
    modifiedBy              :   Number;
}
