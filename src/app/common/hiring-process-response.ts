export class HiringProcessResponse {
    sequence                    :   Number;
    applicantStatusHistoryID    :   Number;
    workflowStageName           :   String;
    actionLabel                 :   String;
    isCurrentStageStatus        :   Boolean;
    feedbackDocumentPath        :   String;
    sysWorkflowstageActionType  :   string;
    
    
}