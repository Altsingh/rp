import { SkillTO } from "app/common/SkillTO.model";
import { EducationTO } from "app/common/EducationTO.model";
import { WorkExpTO } from "app/common/WorkExpTO.model";
import { CandidateAddressTO } from "app/common/CandidateAddressTO.model";

export class SaveCandidateDetailsTO {
    age: any;
    candidateCode: any;
    candidateId: any;
    requisitionID: any;
    firstName: any;
    middleName: any;
    lastName: any;
    internationalID: any;
    mobileNumber: any;
    primaryEmail: any;
    secondaryEmail: any;
    dateOfBirth: any;
    genderID: any;
    panNumber: any;
    voterID: any;
    passportNumber: any;
    aadhar: any;
    industryID: any;
    functionalAreaID: any;
    sourceTypeID: any;
    sourceID: any;
    subSourceUserID: any;
    subSourceTypeID: any;
    subSourceTypeName: any;
    inReview: any;
    triggerPoint: any;

    candidateAddressTO: CandidateAddressTO;
    permanentAddressTO: CandidateAddressTO;
    faceBookProfile: any;
    twitterProfile: any;
    linkedInProfile: any;
    googlePlusProfile: any;

    expInYears: any;
    expInMonths: any;
    noticePeriod: any;
    workExpTOs: WorkExpTO[];
    internshipWorkExpTOs: WorkExpTO[];
    educationTOs: EducationTO[];
    skillTOs: SkillTO[];
    languageTOs: SkillTO[];

    jobBoardName: any;
    jobBoardSID: any;
    socialUID: any
    resume_base64: any;
    resumeExt: any;

    security: any;
}