export class EducationTO {
    candidateEduDetailId: any;
    degree: any;
    startDate: any;
    endDate: any;
    institute: any;
    percentage: any;
}