export class WorksiteMaster {
    worksiteType    :   string;
    worksites       : Array<WorksiteTO>;
    parentWorksiteID :   Number;
    selectedUnitID : Number;

    constructor (worksiteType : string, worksites : Array<WorksiteTO>, parentWorksiteID : Number) {
        this.worksiteType = worksiteType;
        this.worksites = worksites;
        this.parentWorksiteID = parentWorksiteID;
    }
}

export class WorksiteTO {
    worksiteID   :   Number;
    worksiteName :   String;

    constructor (worksiteID : Number, worksiteName : String) {
        this.worksiteID = worksiteID;
        this.worksiteName = worksiteName;
    }
}
