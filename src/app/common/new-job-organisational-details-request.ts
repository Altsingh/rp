export class NewJobOrganisationalDetailsRequest {
    requisitionID       :   Number;
    draftRequisitionID  :   Number;
    functionalAreaIDOrg :   Number;
    requisitionRoleIDOrg:   Number;
    organizationUnitID  :   Number;
    worksiteID          :   Number;
    orgunitHierarchy    :   String;
    worksiteHierarchy   :   String;
    modifiedBy          :   Number;
    bandId              :   Number;
    requisitionTypeID   :   Number;
    confidential        :   Number;
    employeeCategoryId  :   Number;
    campusTypeId        :   Number;
    programTypeId        :   Number;
    roleTypeId        :   Number;
    roleContributionId        :   Number;
    employmentTenureId        :   Number;
    resourceTypeId        :   Number;
    recruitWorkflowPolicyEnabled : boolean;
    recruitApprovalPolicyEnabled: boolean;
    industryId              :  Number;
}
