export class CurrencyDenominationMaster {
    currencyDenominationID  :   Number;
    currencyCodeID          :   Number;
    denomination            :   String;
    factor                  :   Number;
}
