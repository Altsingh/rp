export class CtqMaster {
    question        :   String;
    questionType    :   String;
    options         :   Array<String>;
    selectedOption  :   String;
    questionId : number;
    id : number;
    name : String;
    selectItemOptions: any[];
}