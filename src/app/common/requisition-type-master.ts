export class RequisitionTypeMaster {
    contentTypeID       :   Number;
    contentType         :   String;
    contentCategory     :   String;
    sysContentType      :   String;
}
