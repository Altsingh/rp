export class NewJobWorkflowDetailsRequest {
    requisitionID           :   Number;
    draftRequisitionID  :   Number;
    workflowID          :   Number;
    modifiedBy              :   Number;
}
