import { FormControl } from "@angular/forms";
import { ApplicantPaystructureDetailsTo } from "app/common/applicant-paystructure-details-to";

export class FitmentBasicDetailsTo{

   
    hiringManagerName   : String;
    companyName         : String;
    totalExperience     : String;
    tenureWithCurrentOrganization  : String;
    candidateDesignation:String;
    lastIncrementDate   : Date;
    lastIncrementAmount : String;
    currentProfile      : String;
    highestEducation    : String;
    address             : String;
    nationality         : String;
    employedWithSameOrg : String;
    previousOrgDetails  : String;
    dateOfJoining       : Date;
    currentFixedPay     :String;
    currentVariablePay  :String;
    currentAnnualGross  :String;
    offeredFixedPay     :String;
    offeredVariablePay  :String;
    offeredAnnualGross  :String;
    payReviewResponse   :any;
    sectionName         :String;
    applicantPaystructureDetailsList: Array<ApplicantPaystructureDetailsTo>;
    paystructure        :String;
    hikePercentage      :any;
}