import { Observable } from 'rxjs';
import { SessionService } from 'app/session.service';
import { Http } from '@angular/http';
import { CachedData } from './cached-data';
import { Injectable } from '@angular/core';
import SocialUtil from 'app/job/job-detail/auto-matched-candidates/social-profiles/social-util';

@Injectable()
export class AutoMatchStarsDividerService {

  public value: any;
  private restUrl: String;

  constructor(private http: Http, private sessionService: SessionService) {
    this.restUrl = '/rest/altone/common/stars';
  }


  getData(): Observable<any> {

    if (this.value) {
      return Observable.of(this.value);
    } else {

      return this.http.get(this.sessionService.orgUrl + "" + this.restUrl)
        .map((response) => {
          this.value = response.text();
          return response.text();
        });
    }
  }

}
