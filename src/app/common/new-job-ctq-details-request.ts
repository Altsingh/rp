export class NewJobCtqDetailsRequest {
    requisitionID           :   Number;
    draftRequisitionID  :   Number;
    jobTemplateID  :   Number;
    sourcingGuidline      :   String;
    modifiedBy              :   Number;
    ctqID : Number;
    ctqQuesIDs : Array<Number>;
    ctqRemovedIDs : Array<Number>;

}
