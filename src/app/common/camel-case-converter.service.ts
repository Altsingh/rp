import { Injectable } from '@angular/core';

@Injectable()
export class CamelCaseConverterService {

  constructor() { }

  public convert(obj) {
    let d = this.toCamel(obj);
    return d;
  }

  toCamel(o) {
    let newO, origKey, newKey, value;
    if (o instanceof Array) {
      return o.map(value => {
        if (typeof value === "object") {
          value = this.toCamel(value);
        }
        return value;
      })
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          //origKey = (origKey === origKey.toString().toUpperCase()) ? origKey.toString().toLowerCase() : origKey;
          if( (/^[^a-z]*$/).test(origKey) ){
            newKey = origKey.toLowerCase().toString();
          }else{
            newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString();
          }
          //origKey = this.isUpperCase(origKey) ? origKey.toLowerCase() : origKey;
          if(origKey==="E-mail"){
            newKey = "email";
          }
          value = o[origKey];
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value);
          }
          newO[newKey] = value;
        }
      }
    }
    return newO;
  }


}
