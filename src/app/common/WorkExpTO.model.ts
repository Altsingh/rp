export class WorkExpTO {
    candidateEmpDetailId: any;
    companyName: any;
    jobTitle: any;
    startDate: any;
    endDate: any;
    currentEmployer: any;
    currencyCode: any;
    ctc: any;
    ctcPart: any;
    jobDescription: any;
}