import { EventAnalyticsService } from './event-analytics.service';
import { JobCommonServiceService } from './job/job-common-service.service';
import { CamelCaseConverterService } from './common/camel-case-converter.service';
import { CustomSingleSelectService } from './shared/custom-single-select/custom-single-select.service';
import { RoutingStateService } from './common/routing-state.service';
import { HomeCardsCachedService } from './home/home-cards/home-cards-cached.service';
import { AutoMatchStarsDividerService } from './common/auto-match-stars-divider.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatSelectModule, MatProgressSpinnerModule, MatTooltipModule, MatTabsModule,  MatRadioModule } from '@angular/material';
import 'hammerjs';

import { LocationDialogComponent } from './shared/location-dialog/location-dialog.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContainerComponent } from './container/container.component';
import { AltRouting } from './alt.routing';
import { SharedComponent } from './shared/shared.component';
import { HomeComponent } from './home/home.component';
import { SessionService } from './session.service';
import { ModuleConfigService } from './module-config.service';
import { ErrorComponent } from './home/error.component';
import { RhsComponent } from './rhs/rhs.component';
import { NotificationBarComponent } from './common/notification-bar/notification-bar.component';
import { NotificationService } from './common/notification-bar/notification.service';
import { CommonService } from './common/common.service';
import { MenuComponent } from './header/menu/menu.component';
import { ValidationService } from './shared/custom-validation/validation.service';
import { SharedModule } from './shared/shared.module';
import { AppliedForComponent } from './header/applied-for/applied-for.component'
import { LivefeedComponent } from './header/livefeed/livefeed.component'
import { DownloadComponent } from './header/download/download.component'
import { PendingTaskComponent } from './header/pending-task/pending-task.component'
import { SearchComponent } from './header/search/search.component'
import { ProfileComponent } from './header/profile/profile.component';
import { TaskDayComponent } from './header/pending-task/task-day/task-day.component';
import { TaskMonthComponent } from './header/pending-task/task-month/task-month.component';
import { TaskWeekComponent } from './header/pending-task/task-week/task-week.component';
import { MyTeamComponent } from './header/my-team/my-team.component';
import { MyReferralsComponent } from './header/my-referrals/my-referrals.component'
import { NameinitialService } from './shared/nameinitial.service';
import { HomeCardsComponent } from './home/home-cards/home-cards.component'
import { ProfilecompletionService } from './shared/profilecompletion/profilecompletion.service';
import { LogoutComponent } from './home/logout/logout.component'
import { SessionGuard } from './session.guard';
import { HomePageDetailService } from './home/home-page-detail.service';
import { MenuService } from './header/menu/menu.service';
import { HomeCardsLoaderComponent } from './home/home-cards-loader/home-cards-loader.component';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { AutoMatchSocialPagedService } from './job/job-detail/auto-match-social-paged.service';
import { AutoMatchCompanyPagedService } from './job/job-detail/auto-match-company-paged.service';
import { InterviewLoaderComponent } from './rhs/rhs-loader/interview-loader.component';
import { DownloadBlankScreenComponent } from './header/download-blank-screen/download-blank-screen.component';
import { DownloadLoaderComponent } from './header/download/download-loader/download-loader.component';
import { WorkflowStageMapService } from './shared/services/workflow-stage-map.service';
import { RhsJobboardComponent } from './rhs/rhs-jobboard/rhs-jobboard.component';
import { MonsterService } from 'app/job/job-detail/auto-matched-candidates/job-board-profiles/monster/monster/monster.service';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AutoMatchMonsterPagedService } from './job/job-detail/auto-match-monster-paged.service';
import { CandidateAddressService } from './candidate/candidate-address.service';
import { MeshAppService } from './meshapps/meshapp.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { I18UtilService } from './shared/services/i18-util.service';
import { JinieComponent } from './home/jinie/jinie.component';
import { JinieService } from './home/jinie/jinie.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule, FormsModule, HttpModule, AltRouting, BrowserAnimationsModule, NgSlimScrollModule,
    MatButtonModule, MatCheckboxModule, ReactiveFormsModule, SharedModule.forRoot(), MatSelectModule, MatProgressSpinnerModule, MatTooltipModule, MatRadioModule, MatTabsModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(), 
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],

  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContainerComponent,
    SharedComponent,
    HomeComponent,
    ErrorComponent,
    RhsComponent,
    NotificationBarComponent,
    LocationDialogComponent,
    MenuComponent,
    AppliedForComponent,
    LivefeedComponent,
    DownloadComponent,
    PendingTaskComponent,
    SearchComponent,
    ProfileComponent,
    TaskDayComponent,
    TaskMonthComponent,
    TaskWeekComponent,
    MyTeamComponent,
    MyReferralsComponent,
    HomeCardsComponent,
    LogoutComponent,
    HomeCardsLoaderComponent,
    InterviewLoaderComponent,
    DownloadBlankScreenComponent,
    DownloadLoaderComponent,
    RhsJobboardComponent,
    JinieComponent
  ],

  entryComponents: [LocationDialogComponent],
  providers: [SessionService, NotificationService, CommonService, ValidationService, NameinitialService, ProfilecompletionService,
    SessionGuard, HomePageDetailService, MenuService, AutoMatchSocialPagedService, AutoMatchCompanyPagedService,
    WorkflowStageMapService,
     AutoMatchStarsDividerService,AutoMatchMonsterPagedService,CandidateAddressService,
    HomeCardsCachedService,
     MonsterService, ModuleConfigService,
     RoutingStateService,
     CustomSingleSelectService,
     CamelCaseConverterService,
     JobCommonServiceService,
     MeshAppService, 
     EventAnalyticsService,JinieService],
  bootstrap: [AppComponent]
})
export class AppModule{ 
  constructor(private i18UtilService:I18UtilService ){}
}
