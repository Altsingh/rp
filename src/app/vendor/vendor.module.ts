import { GroupListService } from './group-list/group-list.service';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { SharedModule } from './../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogService } from './../shared/dialog.service';
import { VendorRouting } from './vendor.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorComponent } from './vendor.component';
import { GroupListComponent } from './group-list/group-list.component';
import { NewGroupComponent } from './new-group/new-group.component';
import { EditGroupComponent } from './edit-group/edit-group.component';
import { MatSelectModule, MatSliderModule, MatInputModule, MatTabsModule, MatDialogModule, MatOption,  MatAutocompleteModule, MatRadioModule, MatNativeDateModule, MatTooltipModule, MatDatepickerModule, MatProgressSpinnerModule } from '@angular/material';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { VendorListService } from './vendor-list/vendor-list.service';
import { VendorContextMenuComponent } from './vendor-list/vendor-context-menu/vendor-context-menu.component';
import { NewVendorComponent } from './new-vendor/new-vendor.component';
import { EditVendorComponent } from './edit-vendor/edit-vendor.component';
import { CompanyDetailsComponent } from './new-vendor/company-details/company-details.component';
import { UserDetailsComponent } from './new-vendor/user-details/user-details.component';
import { VendorListLoaderComponent } from './vendor-list/vendor-list-loader/vendor-list-loader.component';
import { VendorListBlankScreenComponent } from './vendor-list/vendor-list-blank-screen/vendor-list-blank-screen.component';
import { VendorCompanyDeatilsDialogComponent } from './new-vendor/company-details/vendor-company-deatils-dialog/vendor-company-deatils-dialog.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@NgModule({
  imports: [CommonModule, VendorRouting, SharedModule, NgSlimScrollModule, ReactiveFormsModule, MatSelectModule,
    MatRadioModule, MatInputModule, MatTabsModule, MatSliderModule, MatDialogModule, MatAutocompleteModule,
    MatNativeDateModule, MatTooltipModule, MatDatepickerModule, MatProgressSpinnerModule, FormsModule, TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })],
  declarations: [VendorComponent, GroupListComponent, NewGroupComponent, EditGroupComponent, VendorListComponent, VendorContextMenuComponent, NewVendorComponent, EditVendorComponent, CompanyDetailsComponent, UserDetailsComponent, VendorListLoaderComponent, VendorListBlankScreenComponent, VendorCompanyDeatilsDialogComponent],
  entryComponents: [NewGroupComponent,EditGroupComponent, VendorCompanyDeatilsDialogComponent],
  providers: [DialogService,GroupListService,VendorListService]
})
export class VendorModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
