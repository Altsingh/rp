import { CommonService } from 'app/common/common.service';
import { NotificationSeverity } from './../../candidate/candidate-list/taggedCandidates/email-candidates/email-candidates.component';
import { NotificationService } from 'app/common/notification-bar/notification.service';
import { EditGroupComponent } from './../edit-group/edit-group.component';
import { NewGroupComponent } from './../new-group/new-group.component';

import { VendorListResponse, VendorListRequest } from './../../common/vendor/vendor-list-request-response';
import { GroupListService } from './group-list.service';
import { Component, OnInit } from '@angular/core';
import { DialogService } from './../../shared/dialog.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-group-list',
  templateUrl: './group-list.component.html',
})
export class GroupListComponent implements OnInit {

  vendorGroupListData: VendorListResponse[];
  editvendorGroup: VendorListResponse;
  pageTitle: string;
  showLoader: boolean = false;
  showDeleteLoader: boolean = false;

  constructor(
    private groupListService: GroupListService,
    private dialogsService: DialogService,
    private notificationService: NotificationService,
    private commonService:CommonService,
    private i18UtilService: I18UtilService
  ) {
    this.showLoader = true;
    this.groupListService.isVendorGroupListUpdated().subscribe((out) => {
      this.groupListService.getVendorGroupList().subscribe(out => {
        this.vendorGroupListData = out.response
        this.showLoader = false;
      });
    });

  }

  ngOnInit() {
    this.commonService.showRHS();
    this.i18UtilService.get('vendor.vendor_groups.title').subscribe((res: string) => {
      this.pageTitle=res;
     });
    this.groupListService.getVendorGroupList().subscribe(out => {
      this.vendorGroupListData = out.response
      this.showLoader = false;
    });
  }



  createNewVendorDialog() {
    //this.dialogsService.setDialogData(res);
    this.showLoader = false;
    this.dialogsService.open(NewGroupComponent, { width: '680px' });

  }

  editJobDialog(groupID) {
    for (let i = 0; i < this.vendorGroupListData.length; i++) {
      if (this.vendorGroupListData[i].groupID == groupID) {
        this.editvendorGroup = this.vendorGroupListData[i];
      }
    }

    this.dialogsService.setDialogData(this.editvendorGroup);
    this.showLoader = false;
    this.dialogsService.open(EditGroupComponent, { width: '680px' });
  }

  deleteJobDialog(groupId) {
    this.showDeleteLoader = true;
    let vendorListRequest: VendorListRequest = new VendorListRequest();
    vendorListRequest.groupID = groupId;
    this.groupListService.deleteVendorGroup(vendorListRequest).subscribe( (out) => {
      if( out && out.messageCode && out.messageCode.code ){
        if( out.messageCode.code == "EC422" ){
          this.notificationService.setMessage(NotificationSeverity.WARNING, out.messageCode.description);
        }else if( out.messageCode.code == "EC200" ){
          this.notificationService.setMessage(NotificationSeverity.INFO, out.messageCode.description);
        }else{
          this.notificationService.setMessage(NotificationSeverity.WARNING, out.messageCode.description);
        }
      }
      this.groupListService.updateVendorGroupList();
    } );
    this.showDeleteLoader = false;
  }


}
