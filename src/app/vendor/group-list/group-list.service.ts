import { Subject, Observable } from 'rxjs';
import { VendorListRequest } from './../../common/vendor/vendor-list-request-response';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from './../../session.service';


@Injectable()
export class GroupListService {

  vendorId: string;
  private vendorGroupListUpdated = new Subject<boolean>();

  constructor(
    private http:Http,
    private sessionService:SessionService
  ) {this.vendorGroupListUpdated.next(false);}

  updateVendorGroupList() {
    this.vendorGroupListUpdated.next(true);
  }

  public isVendorGroupListUpdated(): Observable<boolean> {
    return this.vendorGroupListUpdated.asObservable();
  }

  getVendorGroupList(){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/vendorGroupList/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getIndustryList(){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/getIndustriesList/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }


  saveVendorGroup(vendorListRequest: VendorListRequest) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/vendor/vendorGroup/save' , JSON.stringify(vendorListRequest), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  deleteVendorGroup(vendorListRequest: VendorListRequest) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/vendor/vendorGroup/delete' , JSON.stringify(vendorListRequest), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getPincodeByID(pincodeID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/pincode/' + pincodeID + "/").map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getPincodeMaster(searchString: String, count: Number) {
    if (searchString == null) {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + "/").map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    } else {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + '/' + searchString + "/").map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }
  }

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

}
