import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { VendorListRequest } from './../../common/vendor/vendor-list-request-response';
import { SelectItem } from './../../common/orgunit-master';
import { GroupListService } from './../group-list/group-list.service';
import { PincodeMaster } from './../../common/pincode-master';
import { MessageCode } from './../../job/job-detail/auto-matched-candidates/social-profiles/social-candidate-detail/social-candidate-detail.component';
import { LocationDialogComponent } from './../../shared/location-dialog/location-dialog.component';
import { DialogService } from './../../shared/dialog.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css'],
})
export class NewGroupComponent implements OnInit {

  newGroupForm: FormGroup;
  selectedPincodeID: Number;
  messageCode: MessageCode;
  pincodeList: Array<PincodeMaster>;
  jobIndustryCodeList: SelectItem[];
  lable: String;
  saveVendorGroupButtonBlock: boolean = false;
  groupNameErrorDisplay: boolean = false;

  message: String = "";
  severity: String;
  severityClass: string;
  timer: any;


  constructor(
    public dialogRef: MatDialogRef<NewGroupComponent>,
    private formBuilder: FormBuilder,
    private dialogsService: DialogService,
    private groupListService: GroupListService,
    private notificationService: NotificationService,
    private i18UtilService: I18UtilService
  ) {
    this.createForm();
  }

  ngOnInit() {


    this.groupListService.getPincodeMaster(null, 10).subscribe(out => this.pincodeList = out.responseData);

    this.groupListService.getIndustryList().subscribe(res => {
      this.jobIndustryCodeList = res.responseData;
      for (let i = 0; i < this.jobIndustryCodeList.length; i++) {
        this.jobIndustryCodeList[i]['label'] = this.jobIndustryCodeList[i]['name'];
        this.jobIndustryCodeList[i]['value'] = this.jobIndustryCodeList[i]['id'];
      }
    });

    this.newGroupForm.controls.pincode.valueChanges.subscribe(data => {
      this.groupListService.getPincodeMaster(this.newGroupForm.controls.pincode.value, 10).subscribe(out => {
        this.messageCode = out.messageCode,
          this.pincodeList = out.responseData;
      });
    });

    this.newGroupForm.controls['groupName'].valueChanges.subscribe((data) => {
      this.groupNameErrorDisplay = false;
      this.clearMessages();
      this.timer = null;
    });

    this.i18UtilService.get('common.label.save').subscribe((res: string) => {
      this.lable = res;
     });

  }

  createForm() {
    this.newGroupForm = this.formBuilder.group({
      groupName: ['', Validators.required],
      description: '',
      specialisation: '',
      others: '',
      pincode: '',
      pinCodeID: '',
      industyID: ''
    });
  }


  saveVendorGroupDetails() {
    this.notificationService.clear();

    if (this.newGroupForm.controls['groupName'].value.trim() == null || this.newGroupForm.controls['groupName'].value.trim() == '') {
      this.groupNameErrorDisplay = true;
      this.setMessage("WARNING", 'vendor.warning.groupNameRequired');
    } 
    else
    if (this.newGroupForm.controls['groupName'].value.trim().length > 100 ) {
      this.groupNameErrorDisplay = true;
      this.setMessage("WARNING", 'vendor.warning.groupNameLengthExceeded');
    } 
    else {
      this.groupNameErrorDisplay = false;
      this.clearMessages();
      this.timer = null;
    }

    if (this.newGroupForm.valid && !this.groupNameErrorDisplay) {
      this.saveVendorGroupButtonBlock = true;
      this.groupListService.saveVendorGroup(this.getVendorListRequest()).subscribe((out) => {
        if (out && out.messageCode && out.messageCode.code) {
          this.groupListService.updateVendorGroupList();
          if (out.messageCode.code == "EC422") {
            this.setMessage("WARNING", out.messageCode.description);
            this.groupNameErrorDisplay = true;
          } else if (out.messageCode.code == "EC200") {
            this.dialogRef.close('Cancel');
            this.notificationService.addMessage(NotificationSeverity.INFO, out.messageCode.description);
          } else {
            this.setMessage("WARNING", out.messageCode.description);
          }
        } else {
          this.setMessage("WARNING", 'common.error.serverConnectionerror');
        }
        this.saveVendorGroupButtonBlock = false;
      });
    }
  }

  getVendorListRequest() {
    let values: String[] = this.newGroupForm.value;
    let vendorListRequest: VendorListRequest = new VendorListRequest();
    vendorListRequest.groupName = this.trimVal(values['groupName']);
    vendorListRequest.description = this.trimVal(values['description']);
    vendorListRequest.specialisation = this.trimVal(values['specialisation']);
    vendorListRequest.others = this.trimVal(values['others']);
    vendorListRequest.pincodeID = values['pinCodeID'];
    vendorListRequest.industyID = values['industyID'];
    return vendorListRequest;
  }

  trimVal(str: any) {
    if (str != undefined && str != null && str.length > 0) {
      return str.trim();
    }
    return str;
  }

  public pincodeSelectListener(pincode: PincodeMaster) {
    this.newGroupForm.controls['pinCodeID'].setValue(pincode.pinCodeID);
    this.newGroupForm.controls['pincode'].setValue(pincode.pinCodeDisplay);
    this.selectedPincodeID = pincode.pinCodeID;
  }

  //custom notification service
  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
        this.groupNameErrorDisplay = false;
      }, 5000);
    });

  }


  clearMessages() {
    this.severity = "";
    this.message = "";
  }

}
