import { Injectable } from '@angular/core';
import { PagedDataService } from '../../common/paged-data.service';
import { Http } from '@angular/http';
import { SessionService } from '../../session.service';

@Injectable()
export class VendorListPagedService  extends PagedDataService{

  constructor(
    http: Http,
    sessionService: SessionService
  ) {
    super(http, sessionService);
    super.initialize(
      '/rest/altone/vendor/vendorlist/',
      '/rest/altone/vendor/vendorlist/totalRows/'
    );
   }
}
