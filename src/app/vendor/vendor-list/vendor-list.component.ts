import { Component, OnInit } from '@angular/core';
import { VendorListService } from './vendor-list.service';
import { DialogService } from '../../shared/dialog.service';
import { NotificationService } from '../../common/notification-bar/notification.service';
import { NameinitialService } from '../../shared/nameinitial.service';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../session.service';
import { CommonService } from '../../common/common.service';
import { PageState } from '../../shared/paginator/paginator.component';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VendorFilterTO } from './vendor-filter-to';
import { VendorListPagedService } from './vendor-list-paged.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.css'],
  providers: [VendorListService,VendorListPagedService]
})
export class VendorListComponent implements OnInit {
  
  loaderIndicator: string;
  filterHidden: boolean = true;
  showloader: boolean = false;
  load: number = 3;
  pageState: PageState = new PageState();
  totalRowsSubscription: Subscription;
  pageLoading: boolean = false;
  pageLoadingSubscription: Subscription;
  queryFormGroup: FormGroup;
  queryTimeout: any;
  filterTO: VendorFilterTO;

  public filterQuery = "";
  public rowsOnPage = 15;
  public sortBy = "email";
  public sortOrder = "asc";
  selected: any[] = [];

  pageTitle: string;
  addVendorLabel: string;
  randomcolor: string[] = [];
  vendorListData: any;


  constructor( private vendorListService: VendorListService,
    private dialogsService: DialogService,
    private notificationService: NotificationService,
    private nameinitialService: NameinitialService,
    private sessionService: SessionService, 
    private commonService: CommonService,
    private route: ActivatedRoute,
    private vendorListPagedService: VendorListPagedService,
    private formBuilder: FormBuilder, private i18UtilService: I18UtilService ) {
      
      this.randomcolor = this.nameinitialService.randomcolor;  
      this.filterTO = new VendorFilterTO();
      this.queryFormGroup = this.formBuilder.group({
        query: ['']
      });
      this.queryFormGroup.controls.query.valueChanges.subscribe((data) => {
          if (this.queryTimeout != null) {
            clearTimeout(this.queryTimeout);
          }
          this.queryTimeout = setTimeout(() => {
            let query = this.queryFormGroup.controls.query.value;
            if (query.length >= 3) {
                this.filterTO.pageNumber = 1;
                this.pageState.currentPageNumber = 1;
                this.filterTO.pageSize = this.pageState.pageSize;
                this.filterTO.searchString = query;
                this.vendorListPagedService.clearCache();
                this.loadData();
                this.loadTotalRows();
            }
            if (query.trim().length == 0) {
              this.filterTO.searchString = null;
              this.filterTO.pageNumber = 1;
              this.pageState.currentPageNumber = 1;
              this.filterTO.pageSize = this.pageState.pageSize;
              this.vendorListPagedService.clearCache();
              this.loadData();
              this.loadTotalRows();
            }
          }, 200);
      });
    }

  ngOnInit() {
   
    this.i18UtilService.get('vendor.vendor_list.title').subscribe((res: string) => {
      this.pageTitle=res;
     });
    this.addVendorLabel='Add New Vendor'
    this.selected = [];
    this.pageState.pageSize = 15;
    this.filterTO.pageNumber = 1;
    this.pageState.currentPageNumber = 1;
    this.filterTO.pageSize = this.pageState.pageSize;
    this.loaderIndicator = "loader";
    this.filterHidden = true;
    this.loadData();
    this.loadTotalRows();
    this.commonService.showRHS();
  }

  loadTotalRows() {
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
    this.totalRowsSubscription = this.vendorListPagedService.getTotalRows(this.filterTO).subscribe((response) => {
      this.pageState.totalRows = response;
      this.totalRowsSubscription = null
    });
  }
  loadData() {
    this.pageLoading = true;
    this.loaderIndicator = 'loader';
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    this.pageLoadingSubscription = this.vendorListPagedService.getPage(this.filterTO).subscribe((response) => {
      window.scroll(0, 0);
      if (response) {
        this.vendorListData = response;
        if (response.length == 0 && this.filterTO.pageNumber == 1 && !this.filterTO.isFiltered()) {
          this.loaderIndicator = "nodata";
        } else {
          this.loaderIndicator = "data";
        }
      }
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    }, () => {
      this.pageLoading = false;
      this.pageLoadingSubscription = null;
    });
  }

  
  getPageNumber(pageConfig) {
    this.filterTO.pageNumber = pageConfig.pageNumber;
    this.filterTO.pageSize = pageConfig.pageSize;
    this.loadData();
  }

  ngOnDestroy(): void {
    if (this.pageLoadingSubscription != null) {
      this.pageLoadingSubscription.unsubscribe();
    }
    if (this.totalRowsSubscription != null) {
      this.totalRowsSubscription.unsubscribe();
    }
  }
  tabChanges(){
    this.sessionService.vendorTabDisabled=false;
  }
  vendorGroupLength(vendorGroup):number{
    let count=0;
    for(let i=0;i<vendorGroup.length;i++){
      count+=vendorGroup[i].length;
    }    
    return count;
  }
}
