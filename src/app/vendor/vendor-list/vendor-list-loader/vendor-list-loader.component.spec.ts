import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorListLoaderComponent } from './vendor-list-loader.component';

describe('VendorListLoaderComponent', () => {
  let component: VendorListLoaderComponent;
  let fixture: ComponentFixture<VendorListLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorListLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorListLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
