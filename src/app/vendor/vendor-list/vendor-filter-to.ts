export class VendorFilterTO {

  pageNumber: number;
  pageSize: number;
  sortBy: string;
  sortOrder: string;
  searchString: string;

  isFiltered(): boolean {
    return this.searchString != null
  }
}