import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alt-vendor-list-blank-screen',
  templateUrl: './vendor-list-blank-screen.component.html',
  styleUrls: ['./vendor-list-blank-screen.component.css']
})
export class VendorListBlankScreenComponent implements OnInit {

  @Input() title : string;

  constructor() { }

  ngOnInit() {
  }

}
