import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorListBlankScreenComponent } from './vendor-list-blank-screen.component';

describe('VendorListBlankScreenComponent', () => {
  let component: VendorListBlankScreenComponent;
  let fixture: ComponentFixture<VendorListBlankScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorListBlankScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorListBlankScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
