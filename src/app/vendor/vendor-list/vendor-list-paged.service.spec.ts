import { TestBed, inject } from '@angular/core/testing';

import { VendorListPagedService } from './vendor-list-paged.service';

describe('VendorListPagedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VendorListPagedService]
    });
  });

  it('should be created', inject([VendorListPagedService], (service: VendorListPagedService) => {
    expect(service).toBeTruthy();
  }));
});
