import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SessionService } from '../../session.service';

@Injectable()
export class VendorListService {

  constructor(private http: Http, private sessionService: SessionService) { }

  getVendorList() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/vendorlist/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }
}
