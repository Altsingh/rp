import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorContextMenuComponent } from './vendor-context-menu.component';

describe('VendorContextMenuComponent', () => {
  let component: VendorContextMenuComponent;
  let fixture: ComponentFixture<VendorContextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorContextMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  isClassVisible: boolean = false;
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
