import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DialogService } from '../../../shared/dialog.service';
import { SessionService } from '../../../session.service';
import { NewVendorService } from '../../new-vendor/new-vendor.service';

@Component({
  selector: 'alt-vendor-context-menu',
  templateUrl: './vendor-context-menu.component.html',
  styleUrls: ['./vendor-context-menu.component.css'],
  providers: [NewVendorService]
})
export class VendorContextMenuComponent implements OnInit {
  
  responseData: any;
  displayClass: String = "displayNone";

  @Output() showloader: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() output: EventEmitter<any> = new EventEmitter();
  @Input()
  vendorId: number;

  constructor(private dialogsService: DialogService,
    private sessionService: SessionService,
    private newVendorService : NewVendorService) { }

  ngOnInit() {
  }

  hideContextMenu() {
    this.displayClass = "displayNone";
  }

  showContextMenu() {
    this.displayClass = "displayBlock";
  }
  deleteVendor(vendorId: number){
    this.newVendorService.deleteVendor(vendorId).subscribe((response) => {
      this.responseData = response.response;
    })
  }
}
