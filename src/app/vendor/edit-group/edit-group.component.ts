import { LocationDialogComponent } from './../../shared/location-dialog/location-dialog.component';
import { GroupListService } from './../group-list/group-list.service';
import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
import { SelectItem } from './../../shared/select-item';
import { PincodeMaster } from './../../common/pincode-master';
import { MessageCode } from 'app/candidate/candidate-list/taggedCandidates/email-candidates/email-candidates.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from './../../shared/dialog.service';
import { VendorListResponse, VendorListRequest } from './../../common/vendor/vendor-list-request-response';
import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.css']
})
export class EditGroupComponent implements OnInit {

  editvendorGroup: VendorListResponse;
  editGroupForm: FormGroup;
  selectedPincodeID: Number;
  messageCode: MessageCode;
  pincodeList: Array<PincodeMaster>;
  jobIndustryCodeList: SelectItem[];
  lable: String;
  saveVendorGroupButtonBlock: boolean = false;
  groupNameErrorDisplay: boolean = false;

  message: String = "";
  severity: String;
  severityClass: string;
  timer: any;

  constructor(
    public dialogRef: MatDialogRef<EditGroupComponent>,
    private formBuilder: FormBuilder,
    private dialogsService: DialogService,
    private groupListService: GroupListService,
    private notificationService: NotificationService,
    private i18UtilService: I18UtilService

  ) { }

  createForm() {
    this.editGroupForm = this.formBuilder.group({
      groupName: [this.editvendorGroup.groupName, Validators.required],
      description: this.editvendorGroup.description,
      specialisation: this.editvendorGroup.specialisation,
      others: this.editvendorGroup.others,
      pincode: '',
      pinCodeID: this.editvendorGroup.pincodeID,
      industyID: this.editvendorGroup.industyID
    });
  }

  ngOnInit() {
    this.editvendorGroup = this.dialogsService.getDialogData();
    this.createForm();

    this.groupListService.getPincodeMaster(null, 10).subscribe(out => this.pincodeList = out.responseData);

    this.groupListService.getPincodeByID(this.editvendorGroup.pincodeID).subscribe(out => {
      this.messageCode = out.messageCode;
      this.editGroupForm.controls['pincode'].setValue(out.responseData.pinCodeDisplay);
      let a: Number = out.responseData.pinCodeID;
      let b: String = out.responseData.pinCodeDisplay;
      this.pincodeList = [new PincodeMaster(a, b)];
    });

    this.groupListService.getIndustryList().subscribe(res => {
      this.jobIndustryCodeList = res.responseData;
      for (let i = 0; i < this.jobIndustryCodeList.length; i++) {
        this.jobIndustryCodeList[i]['label'] = this.jobIndustryCodeList[i]['name'];
        this.jobIndustryCodeList[i]['value'] = this.jobIndustryCodeList[i]['id'];
      }
    });

    this.editGroupForm.controls.pincode.valueChanges.subscribe(data => {
      this.groupListService.getPincodeMaster(this.editGroupForm.controls.pincode.value, 10).subscribe(out => {
        this.messageCode = out.messageCode,
          this.pincodeList = out.responseData;
      });
    });

    this.editGroupForm.controls['groupName'].valueChanges.subscribe((data) => {
      this.groupNameErrorDisplay = false;
      this.clearMessages();
      this.timer = null;
    });

    this.i18UtilService.get('common.label.save').subscribe((res: string) => {
      this.lable = res;
     });
  }

  saveVendorGroupDetails() {
    this.notificationService.clear();

    if (this.editGroupForm.controls['groupName'].value.trim() == null || this.editGroupForm.controls['groupName'].value.trim() == '') {
      this.groupNameErrorDisplay = true;
      this.setMessage("WARNING", 'vendor.warning.groupNameRequired');
    } 
    else
    if (this.editGroupForm.controls['groupName'].value.trim().length > 100 ) {
      this.groupNameErrorDisplay = true;
      this.setMessage("WARNING",  'vendor.warning.groupNameLengthExceeded');
    } 
    else {
      this.groupNameErrorDisplay = false;
      this.clearMessages();
      this.timer = null;
    }
    

    if (this.editGroupForm.valid && !this.groupNameErrorDisplay) {
      this.saveVendorGroupButtonBlock = true;
      this.groupListService.saveVendorGroup(this.getVendorListRequest()).subscribe((out) => {
        if (out && out.messageCode && out.messageCode.code) {
          if (out.messageCode.code == "EC422") {
            this.setMessage("WARNING", out.messageCode.description);
            this.groupNameErrorDisplay = true;
          } else if (out.messageCode.code == "EC200") {
            this.groupListService.updateVendorGroupList();
            this.dialogRef.close('Cancel');
            this.notificationService.addMessage(NotificationSeverity.INFO, out.messageCode.description);
          } else {
            this.setMessage("WARNING", out.messageCode.description);
          }
        }else{
          this.setMessage("WARNING", 'common.error.serverConnectionerror');
        }
        this.saveVendorGroupButtonBlock = false;
      });
    }
  }

  getVendorListRequest() {
    let values: String[] = this.editGroupForm.value;
    let vendorListRequest: VendorListRequest = new VendorListRequest();
    vendorListRequest.groupName = this.trimVal(values['groupName']);
    vendorListRequest.description = this.trimVal(values['description']);
    vendorListRequest.specialisation = this.trimVal(values['specialisation']);
    vendorListRequest.others = this.trimVal(values['others']);
    vendorListRequest.pincodeID = values['pinCodeID'];
    vendorListRequest.industyID = values['industyID'];
    vendorListRequest.groupID = this.editvendorGroup.groupID;
    return vendorListRequest;
  }

  trimVal(str: any) {
    if (str != undefined && str != null && str.length > 0) {
      return str.trim();
    }
    return str;
  }

  public pincodeSelectListener(pincode: PincodeMaster) {
    this.editGroupForm.controls['pinCodeID'].setValue(pincode.pinCodeID);
    this.editGroupForm.controls['pincode'].setValue(pincode.pinCodeDisplay);
    this.selectedPincodeID = pincode.pinCodeID;
  }
  //custom notification service
  public setMessage(severity: string, message: string) {
    this.i18UtilService.get(message).subscribe((res: string) => {
      this.severity = severity;
      this.message = res;
      switch (severity) {
        case NotificationSeverity.INFO:
          this.severityClass = "successful";
          break;
        case NotificationSeverity.WARNING:
          this.severityClass = "warning";
          break;
        default:
          this.severityClass = "";
          break;
      }
      this.timer = setTimeout(() => {
        this.clearMessages();
        this.timer = null;
        this.groupNameErrorDisplay = false;
      }, 5000);
    });
  }


  clearMessages() {
    this.severity = "";
    this.message = "";
  }

}
