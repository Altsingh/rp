import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';
import { SessionService } from '../../session.service';
import { SaveVendorRequest } from './company-details/company-details.component';
import { SaveVendorUserRequest } from './user-details/user-details.component';
import { VendorFilterTO } from '../vendor-list/vendor-filter-to';

@Injectable()
export class NewVendorService {

  constructor(
    private http:Http,
    private sessionService:SessionService
  ) { }

  getVendorGroupList(){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/vendorGroupList/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getVendorList(filterTO: VendorFilterTO) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/vendor/vendorlist/', JSON.stringify(filterTO), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getVendorListTotalRows(filterTO: VendorFilterTO) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/vendor/vendorlist/totalRows/', JSON.stringify(filterTO), options)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getVendorRoleList(){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/vendorRoleList/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
  
  getVendorAdminID(){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/vendoradmin/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getIndustryList(){
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/getIndustriesList/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getPincodeByID(pincodeID: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/pincode/' + pincodeID + "/").map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getPincodeMaster(searchString: String, count: Number) {
    if (searchString == null) {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + "/").map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    } else {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + '/' + searchString + "/").map(res => { this.sessionService.check401Response(res.json()); return res.json() });
    }
  }

  createVendor(saveVendorRequest: SaveVendorRequest) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/vendor/newvendor/company-details/save', JSON.stringify(saveVendorRequest), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  createVendorUser(saveVendorUserRequest: SaveVendorUserRequest) {
    let options = new RequestOptions({ headers: this.getPOSTHeaders() });
    return this.http.post(this.sessionService.orgUrl + '/rest/altone/vendor/newvendor/user-details/save/' + this.sessionService.vendorId , JSON.stringify(saveVendorUserRequest), options).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getVendorDetails(vendorId: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/editvendor/company-details/' + vendorId , )
      .map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  getVendorUserDetails(vendorId: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/editvendor/user-details/' + vendorId , )
      .map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }

  deleteVendor(vendorId: Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/vendor/deletevendor/' + vendorId).
      map(res => { this.sessionService.check401Response(res.json()); return res.json() });
  }
  

  private getPOSTHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

}
