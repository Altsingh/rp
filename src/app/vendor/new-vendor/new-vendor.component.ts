import { Component, OnInit,OnDestroy, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CommonService } from '../../common/common.service';
import { SessionService } from '../../session.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-new-vendor',
  templateUrl: './new-vendor.component.html',
  styleUrls: ['./new-vendor.component.css']
})
export class NewVendorComponent implements OnInit,OnDestroy {

  @Input()
  vendorId: number;
  currentPath = this.router.url;
  pageTitle: string;
  constructor(private router: Router,
    private commonService: CommonService,
    private sessionService: SessionService,
    private i18UtilService: I18UtilService) { }

  ngOnInit() {
    this.sessionService.vendorTabDisabled=!this.sessionService.vendorTabDisabled;
    this.i18UtilService.get('vendor.create_vendor.title').subscribe((res: string) => {
      this.pageTitle = res;
     });
    this.commonService.showRHS();
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.currentPath = this.router.url;
        }
      });
  }
  ngOnDestroy(){
    this.sessionService.vendorTabDisabled=true;
  }

}
