import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorCompanyDeatilsDialogComponent } from './vendor-company-deatils-dialog.component';

describe('VendorCompanyDeatilsDialogComponent', () => {
  let component: VendorCompanyDeatilsDialogComponent;
  let fixture: ComponentFixture<VendorCompanyDeatilsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorCompanyDeatilsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorCompanyDeatilsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
