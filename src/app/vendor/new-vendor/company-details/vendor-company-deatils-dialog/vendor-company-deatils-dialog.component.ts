import { Component, OnInit } from '@angular/core';
import { DialogService } from '../../../../shared/dialog.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatDialogRef } from '@angular/material';
import { NotificationService } from '../../../../common/notification-bar/notification.service';

@Component({
  selector: 'alt-vendor-company-deatils-dialog',
  templateUrl: './vendor-company-deatils-dialog.component.html',
  styleUrls: ['./vendor-company-deatils-dialog.component.css'],
  providers: [DatePipe]
})
export class VendorCompanyDeatilsDialogComponent implements OnInit {

  interviewStartDate: Date;
  interviewEndDate: Date;
  todayDate: Date;

  vendorContractDeatils: FormGroup;
  businessDetails: FormGroup;
  message: string;
  timer: any;
  severity: String;
  severityClass: string;

  constructor(public dialogRef: MatDialogRef<VendorCompanyDeatilsDialogComponent>, private dialogService: DialogService, private formBuilder: FormBuilder, private datePipe: DatePipe, private notificationService: NotificationService) {
    this.message = "";
    this.initializeBusinessDetails();
  }

  ngOnInit() {
    this.todayDate=new Date();
    this.todayDate.setHours(0,0,0,0);
    console.log(this.dialogService.getDialogData1());
    if (this.dialogService.getDialogData1() != null) {
      // const [_startDateDD, _startDateMM, _startDateYYYY] = this.dialogService.getDialogData1().split('-');
      // this.businessDetails.controls['startDateDD'].setValue(parseInt(_startDateDD));
      // this.businessDetails.controls['startDateMM'].setValue(parseInt(_startDateMM));
      // this.businessDetails.controls['startDateYYYY'].setValue(parseInt(_startDateYYYY));
      // this.businessDetails.controls['startDate'].setValue(result.response.startDate);
      // this.setStartDate(new Date(parseInt(_startDateYYYY),parseInt(_startDateMM)-1,parseInt(_startDateDD)));
      this.setStartDate(this.dialogService.getDialogData1());
    }

    if (this.dialogService.getDialogData2() != null) {
      // const [_closingDateDD, _closingDateMM, _closingDateYYYY] = this.dialogService.getDialogData2().split('-');
      // this.businessDetails.controls['closingDateDD'].setValue(parseInt(_closingDateDD));
      // this.businessDetails.controls['closingDateMM'].setValue(parseInt(_closingDateMM));
      // this.businessDetails.controls['closingDateYYYY'].setValue(parseInt(_closingDateYYYY));
      // this.setEndDate(new Date(parseInt(_closingDateYYYY),parseInt(_closingDateMM)-1,parseInt(_closingDateDD)));
      this.setEndDate(this.dialogService.getDialogData2());
    }



  }

  initializeBusinessDetails() {
    this.businessDetails = this.formBuilder.group({
      startDate: new FormControl(),
      endDate: new FormControl(),
    });
  }


  setStartDate(startDate: Date) {
    if (startDate != null) {
      let _STARTDATE: String = startDate.getDate() + '-' + ((startDate.getMonth())) + '-' + startDate.getFullYear() + ' ' + startDate.getHours() + ':' + startDate.getMinutes();
      this.businessDetails.controls['startDate'].setValue(this.datePipe.transform(startDate, "dd-MM-yyyy"));
      // this.businessDetails.controls['startDate'].setValue(startDate);
    } else {
      this.businessDetails.controls['startDate'].reset();
    }
    this.interviewStartDate = startDate;

  }

  setEndDate(endDate: Date) {

    if (endDate != null) {
      let _ENDDATE: String = endDate.getDate() + '-' + ((endDate.getMonth())) + '-' + endDate.getFullYear() + ' ' + endDate.getHours() + ':' + endDate.getMinutes();
      // this.businessDetails.controls['endDate'].setValue(_ENDDATE);
      this.businessDetails.controls['endDate'].setValue(this.datePipe.transform(endDate, "dd-MM-yyyy"));
    } else {
      this.businessDetails.controls['endDate'].reset();
    }
    this.interviewEndDate = endDate;

  }


  assignCandidateJob(event) {
    this.notificationService.clear();
    let valid: boolean = true;
    if (this.interviewStartDate == null) {
      this.setMessage(NotificationSeverity.WARNING, "Business Information: Please select Contract Start Date");
      valid = false;
    }
    
    
    if(this.interviewEndDate==null){
      this.setMessage(NotificationSeverity.WARNING, "Business Information: Please select Contract End Date");
      valid = false;
    }else if(this.interviewStartDate!=null && this.interviewEndDate < this.interviewStartDate){
      this.setMessage(NotificationSeverity.WARNING, 'Business Information' + " : " + 'Contract End Date' + " should be after the " + 'Contract Start Date');
      valid = false;
  
    }
    
    if(!valid)
    return;

    // if (Object.keys(this.candidateInfo.controls.jobIdSelected.value).length == 0) {
    //   this.setMessage("WARNING", "Please select a job!");
    //   return;
    // }
let messagess= [this.interviewStartDate, this.interviewEndDate];
        this.dialogRef.close(messagess);
        this.dialogRef.close
        this.notificationService.setMessage("success", "Vendor Contract Details Updated");
        //this.candidateListService.getUntaggedCandidateComponent().removeCardFromList(this.sessionService.object.index);

  }


  public setMessage(severity: string, message: string) {
    this.severity = severity;
    this.message = message;
    switch (severity) {
      case NotificationSeverity.INFO:
        this.severityClass = "successful";
        break;
      case NotificationSeverity.WARNING:
        this.severityClass = "warning";
        break;
      default:
        this.severityClass = "";
        break;
    }
    this.timer = setTimeout(() => {
      this.clearMessages();
      this.timer = null;
    }, 3000);
  }

  clearMessages() {
    this.severity = "";
    this.message = "";
  }

  
}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}