import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { VendorListResponse, IndustryResponse } from '../../../common/vendor/vendor-list-request-response';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Location, DatePipe } from '@angular/common';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { SelectItem } from '../../../shared/select-item';
import { NewVendorService } from '../new-vendor.service';
import { MessageCode } from '../../../common/message-code';
import { DialogService } from '../../../shared/dialog.service';
import { SessionService } from '../../../session.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidationService } from '../../../shared/custom-validation/validation.service';
import { VendorCompanyDeatilsDialogComponent } from './vendor-company-deatils-dialog/vendor-company-deatils-dialog.component';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css'],
  providers: [NewVendorService,DatePipe]
})
export class CompanyDetailsComponent implements OnInit{

  basicDetails: FormGroup;
  businessDetails: FormGroup;
  bankDetails: FormGroup;

  vendorId: Number; 
  
  vendorUserId: Number;
  editVendor: boolean = false;
  saveFlag: boolean = false;
  showLoader: boolean = false;
  altLoader: boolean = false;
  saveButtonDisabled: boolean = false;
  saveLabel: string;
  basicSectionActive: boolean = false;
  businessSectionActive: boolean = false;
  bankSectionActive: boolean = false;
  vendorGroupListData: VendorListResponse[];
  indutryListData: IndustryResponse[];

  selectedPincodeID: Number;
  messageCode: MessageCode;
  interviewStartDate: Date;
  interviewEndDate: Date;
  todayDate: Date;

  field: Field;

  enterLabel: string= 'common.warning.enterLabelCommon';
  selectLabel: string= 'common.warning.selectLabelCommon';
  basicDetailLabel: string;
  businessInfoLabel: string;
  bankDetailLabel: string;



  constructor(
    private ACTIVATED_ROUTE: ActivatedRoute,
    private newVendorService : NewVendorService,
    private router: Router,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private dialogsService: DialogService,
    private sessionService: SessionService,
    private datePipe: DatePipe,
    private i18UtilService: I18UtilService
  ) { 

    this.showLoader = true;
    this.basicSectionActive = true;
    this.businessSectionActive = true;
    this.bankSectionActive = true;

    this.initializeBasicDetails();
    this.initializeBusinessDetails();
    this.initializeBankDetails();
    this.populateMasters();

  }

  ngOnInit() {
    this.todayDate=new Date();
    this.todayDate.setHours(0,0,0,0);
    this.altLoader = true;
    this.ACTIVATED_ROUTE.parent.params.subscribe((params) => {
      this.vendorId= this.ACTIVATED_ROUTE.snapshot.parent.params['id'];
    });
    if (this.vendorId != null) {
      this.saveLabel = "Update"

      Promise.all([this.newVendorService.getVendorGroupList().toPromise()]).then((res) => {
        if(res.length>0){
          this.vendorGroupListData = res[0].response;
          if(this.vendorGroupListData!=null){
            for (let item of this.vendorGroupListData) {
              item.value = item.groupID;
              item.label = item.groupName;
            }
          }
        }
        this.newVendorService.getVendorDetails(this.vendorId).subscribe(result => {
          this.setVendorDetails(result);
          this.editVendor = true;
          this.showLoader = false;
          this.altLoader = false;
          this.sessionService.vendorTabDisabled=false;
        });
    });
      
    } else {
      this.editVendor = false;
      this.saveLabel = "Save"
      this.altLoader = false;
    }
    this.i18UtilService.get('vendor.create_vendor.basicDetailLabel').subscribe((res: string) => {
      this.basicDetailLabel = res;
     });
    this.i18UtilService.get('vendor.create_vendor.businessInfoLabel').subscribe((res: string) => {
      this.businessInfoLabel = res;
    });
    this.i18UtilService.get('vendor.create_vendor.bankDetailLabel').subscribe((res: string) => {
      this.bankDetailLabel = res;
     }); 
     this.i18UtilService.get('common.label.save').subscribe((res: string) => {
      this.saveLabel = res;
     });
  }

  goBack() {
    this.router.navigateByUrl("/vendor/vendorlist");
  }

    
  setStartDate(startDate: Date) {
    if (startDate != null) {
    let _STARTDATE: String = startDate.getDate() + '-' + ((startDate.getMonth())) + '-' + startDate.getFullYear() + ' ' + startDate.getHours() + ':' + startDate.getMinutes();
    this.businessDetails.controls['startDate'].setValue(this.datePipe.transform(startDate, "dd-MM-yyyy"));
    // this.businessDetails.controls['startDate'].setValue(startDate);
    } else {
      this.businessDetails.controls['startDate'].reset();
    }
    this.interviewStartDate = startDate;

  }

  setEndDate(endDate: Date) {

    if (endDate != null) {
    let _ENDDATE: String = endDate.getDate() + '-' + ((endDate.getMonth())) + '-' + endDate.getFullYear() + ' ' + endDate.getHours() + ':' + endDate.getMinutes();
    // this.businessDetails.controls['endDate'].setValue(_ENDDATE);
    this.businessDetails.controls['endDate'].setValue(this.datePipe.transform(endDate, "dd-MM-yyyy"));
    } else {
      this.businessDetails.controls['endDate'].reset();
    }
    this.interviewEndDate = endDate;

  }



  toggleDiv(section) {
    if (section == 'basic') {
      this.basicSectionActive = !this.basicSectionActive;
    } else if (section == 'business') {
      this.businessSectionActive = !this.businessSectionActive;
    } else if (section == 'bank') {
      this.bankSectionActive = !this.bankSectionActive;
    }
  }

  isNull(control: FormControl) {
    if (control.value == null || control.value == '' || (typeof control.value == 'string' && control.value.trim() == '')) {
        return true;
    }
    return false;
  }

  populateMasters(){

    this.newVendorService.getVendorGroupList().subscribe((response) => {
      this.vendorGroupListData = response.response;
      if(this.vendorGroupListData!=null){
        for (let item of this.vendorGroupListData) {
          item.value = item.groupID;
          item.label = item.groupName;
        }
      }
    })

    this.newVendorService.getIndustryList().subscribe((response) => {
      this.indutryListData = response.responseData;
      if(this.indutryListData!=null){
        for (let item of this.indutryListData) {
          item.value = item.id;
          item.label = item.code;
        }
      }
    })

  }

  initializeBasicDetails() {
    this.basicDetails = this.formBuilder.group({
      group: '',
      company: '',
      vendorCode: '',
      contactPerson: '',
      emailID:  ['', [ValidationService.emailValidator]],
      contactNumber: ['', [ValidationService.MobileValidator]],
      address: '',
      industry: '',
      specialization: '',
      website: [null,Validators.required],
      noOfAllowedUsers: '',
    });
  }
  initializeBusinessDetails() {
    this.businessDetails = this.formBuilder.group({
      startDate: new FormControl(),
      endDate: new FormControl(),
      panNo: ['', [ValidationService.PanNoValidator]],
      serviceTaxNo: '',
      vatNo: ''
    });
  }
  initializeBankDetails() {
    this.bankDetails = this.formBuilder.group({
      bankName: '',
      ifscCode: '',
      accountNo: '',
      branchName: '',
      branchAddress: '',
    });
  }
  

  datesStorage: any[] = [];


  
  saveVendorDetails(){
    this.saveFlag = true;
    this.notificationService.clear();
    let valid: boolean = true;
    let saveVendorRequest: SaveVendorRequest = this.getNewVendorRequest();
    valid = this.checkMandatory(saveVendorRequest);
    if(!valid){
      this.saveFlag=false;
      return;
    }
    this.editVendor = true;
    this.newVendorService.createVendor(saveVendorRequest).subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          this.vendorId = response.responseData.VendorId;
          this.sessionService.vendorId = response.responseData.VendorId;
          this.sessionService.vendorTabDisabled = false;
          this.saveLabel = "Update"
          this.router.navigateByUrl("/vendor/editvendor/" + this.vendorId  +  "/company-details");
          this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
           });
         
        } else if (response.messageCode.code == "EC201") {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
           });
        }
      }
      this.saveFlag=false;
    });
  }

  getNewVendorRequest() {
    let saveVendorRequest: SaveVendorRequest = new SaveVendorRequest()
    saveVendorRequest.input = new NewVendorRequest();
    saveVendorRequest.input.vendorID = this.vendorId;
    saveVendorRequest.input.group = this.basicDetails.controls.group.value;
    saveVendorRequest.input.company = this.basicDetails.controls.company.value;
    saveVendorRequest.input.vendorCode = this.basicDetails.controls.vendorCode.value;
    saveVendorRequest.input.contactPerson = this.basicDetails.controls.contactPerson.value;
    saveVendorRequest.input.emailID = this.basicDetails.controls.emailID.value;
    saveVendorRequest.input.contactNumber = this.basicDetails.controls.contactNumber.value;
    saveVendorRequest.input.address = this.basicDetails.controls.address.value;
    saveVendorRequest.input.industry = this.basicDetails.controls.industry.value;
    saveVendorRequest.input.specialization = this.basicDetails.controls.specialization.value;
    saveVendorRequest.input.website = this.basicDetails.controls.website.value;
    saveVendorRequest.input.noOfAllowedUsers = this.basicDetails.controls.noOfAllowedUsers.value;
    saveVendorRequest.input.startDate = this.businessDetails.controls.startDate.value;
    saveVendorRequest.input.endDate = this.businessDetails.controls.endDate.value;

    saveVendorRequest.input.panNo = this.businessDetails.controls.panNo.value;
    saveVendorRequest.input.serviceTaxNo = this.businessDetails.controls.serviceTaxNo.value;
    saveVendorRequest.input.vatNo = this.businessDetails.controls.vatNo.value;
    saveVendorRequest.input.bankName = this.bankDetails.controls.bankName.value;
    saveVendorRequest.input.ifscCode = this.bankDetails.controls.ifscCode.value;
    saveVendorRequest.input.accountNo = this.bankDetails.controls.accountNo.value;
    saveVendorRequest.input.branchName = this.bankDetails.controls.branchName.value;
    saveVendorRequest.input.branchAddress = this.bankDetails.controls.branchAddress.value;
    return saveVendorRequest;
  }

 
  public openVendorDateDialog() {
    if(!this.saveFlag){
    this.dialogsService.setDialogData1(this.interviewStartDate);
    this.dialogsService.setDialogData2(this.interviewEndDate);
    this.dialogsService.open(VendorCompanyDeatilsDialogComponent, { width: '350px' }).subscribe(result => {
        if (result != undefined && result != 'Cancel') {

          this.setStartDate(result[0]);
          this.setEndDate(result[1]);
          console.log(result);
        }
      });
    }
  }

  checkMandatory(saveVendorRequest){
    let valid: boolean = true;
    if ( saveVendorRequest.input.group == 0) {
        this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.vendor_groups.group');
        valid = false;
    }
    if ( saveVendorRequest.input.company == null || saveVendorRequest.input.company.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'common.label.company' );
      valid = false;
    }
    if ( saveVendorRequest.input.vendorCode == null || saveVendorRequest.input.vendorCode.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.vendorCode');
      valid = false;
    }
    if ( saveVendorRequest.input.contactPerson == null || saveVendorRequest.input.contactPerson.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.vendor_list.contactPerson');
      valid = false;
    }
    if ( saveVendorRequest.input.emailID == null || saveVendorRequest.input.emailID.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.emailID');
      valid = false;
    }
    if ( saveVendorRequest.input.emailID != null || saveVendorRequest.input.emailID.trim() !='') {
      this.field = new Field();
      this.field.value =  saveVendorRequest.input.emailID ;
      if(ValidationService.emailValidator(this.field)){
        this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.emailIDCorrectFormate');
        valid = false;
      }
    }
    if ( saveVendorRequest.input.contactNumber == null || saveVendorRequest.input.contactNumber.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.contactNumber');
      valid = false;
    }
    if ( saveVendorRequest.input.contactNumber != null || saveVendorRequest.input.contactNumber.trim() !='') {
      this.field = new Field();
      this.field.value =  saveVendorRequest.input.contactNumber ;
      if(ValidationService.MobileValidator(this.field)){
        this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.contactNumberCorrectFormate');
        valid = false;
      }
    } 
  
    if ( saveVendorRequest.input.address == null || saveVendorRequest.input.address.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel ,'common.label.address');
      valid = false;
    }
    if ( saveVendorRequest.input.industry == 0) {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'postJobBoard.label.industry');
      valid = false;
    }
    if ( saveVendorRequest.input.specialization == null || saveVendorRequest.input.specialization.trim() =='') {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.vendor_groups.specialization' );
      valid = false;
    }
    if ( saveVendorRequest.input.noOfAllowedUsers == null || saveVendorRequest.input.noOfAllowedUsers ==0) {
      this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.allowedUsersCount');
      valid = false;
    }
    if ( saveVendorRequest.input.noOfAllowedUsers != null || saveVendorRequest.input.noOfAllowedUsers !=0) {
      this.field = new Field();
      this.field.value =  saveVendorRequest.input.noOfAllowedUsers ;
      if(ValidationService.NumberOnly2(this.field)){
        this.addMessage(this.basicDetailLabel,  this.enterLabel , 'vendor.create_vendor.allowedUsersCountNumeric');
        valid = false;
      }
    }

    if (this.interviewStartDate == null) {
      this.addMessage(this.businessInfoLabel,  this.selectLabel , 'vendor.create_vendor.contractStartDate');
      valid = false;
    }
    
    if(this.interviewEndDate==null){
      this.addMessage(this.businessInfoLabel,  this.selectLabel , 'vendor.create_vendor.contractEndDate');
      valid = false;
    }else if(this.interviewStartDate!=null && this.interviewEndDate < this.interviewStartDate){
      this.i18UtilService.get('vendor.warning.contractEnd_StartDate').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res)
      });
      valid = false;
    }

    return valid;
  }
  addMessage(section: string, action: string, label: string) {
    this.i18UtilService.get(label).subscribe((res: string) => {
      label = res;
    });

    this.i18UtilService.get(action , { 'label': label }).subscribe((res: string) => {
      this.notificationService.addMessage(NotificationSeverity.WARNING, res)
    });

  }

  setVendorDetails(result: any) {
    this.vendorId = result.response.vendorID; 
    this.basicDetails.controls['group'].setValue(result.response.group);
    this.basicDetails.controls['company'].setValue(result.response.company);
    this.basicDetails.controls['vendorCode'].setValue(result.response.vendorCode);
    this.basicDetails.controls['contactPerson'].setValue(result.response.contactPerson);
    this.basicDetails.controls['emailID'].setValue(result.response.emailID);
    this.basicDetails.controls['contactNumber'].setValue(result.response.contactNumber);
    this.basicDetails.controls['address'].setValue(result.response.address);
    this.basicDetails.controls['industry'].setValue(result.response.industry);
    this.basicDetails.controls['specialization'].setValue(result.response.specialization);
    this.basicDetails.controls['website'].setValue(result.response.website);
    this.basicDetails.controls['noOfAllowedUsers'].setValue(result.response.noOfAllowedUsers);
    if (result.response.startDate != null) {
      const [_startDateDD, _startDateMM, _startDateYYYY] = result.response.startDate.split('-');
      this.setStartDate(new Date(parseInt(_startDateYYYY),parseInt(_startDateMM)-1,parseInt(_startDateDD)));
    }
    if (result.response.endDate != null) {
      const [_closingDateDD, _closingDateMM, _closingDateYYYY] = result.response.endDate.split('-');
      this.setEndDate(new Date(parseInt(_closingDateYYYY),parseInt(_closingDateMM)-1,parseInt(_closingDateDD)));
    }
    this.businessDetails.controls['panNo'].setValue(result.response.panNo);
    this.businessDetails.controls['serviceTaxNo'].setValue(result.response.serviceTaxNo);
    this.businessDetails.controls['vatNo'].setValue(result.response.vatNo);
    this.bankDetails.controls['bankName'].setValue(result.response.bankName);
    this.bankDetails.controls['ifscCode'].setValue(result.response.ifscCode);
    this.bankDetails.controls['accountNo'].setValue(result.response.accountNo);
    this.bankDetails.controls['branchName'].setValue(result.response.branchName);
    this.bankDetails.controls['branchAddress'].setValue(result.response.branchAddress);
  }
}

export class SaveVendorRequest {
  input: NewVendorRequest;
}

export class NewVendorRequest {
  vendorID: Number;
  group: string;
  company: string;
  vendorCode: string;
  contactPerson: string;
  emailID: string;
  contactNumber: string;
  address: string;
  industry: string;
  specialization: string;
  website: string;
  noOfAllowedUsers: Number;
  startDate: string;
  endDate: string;
  panNo: string;
  serviceTaxNo: string;
  vatNo: string;
  bankName: string;
  ifscCode: string;
  accountNo: string;
  branchName: string;
  branchAddress: string;
}

export class Field{
  value : string;
}
