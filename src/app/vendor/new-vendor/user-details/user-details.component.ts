import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl,FormArray, FormArrayName } from '@angular/forms';
import { NewVendorService } from '../new-vendor.service';
import { Location } from '@angular/common';
import { NotificationService, NotificationSeverity } from '../../../common/notification-bar/notification.service';
import { VendorRoleListResponse, VendorActiveListResponse } from '../../../common/vendor/vendor-list-request-response';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from '../../../shared/dialog.service';
import { SessionService } from '../../../session.service';
import { ValidationService } from '../../../shared/custom-validation/validation.service';
import { I18UtilService } from 'app/shared/services/i18-util.service';

@Component({
  selector: 'alt-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
  providers: [NewVendorService]
})
export class UserDetailsComponent implements OnInit {

  items: FormArray;
  vendorAdminId: Number;
  userDetails: FormArray;
  formGroup: FormGroup;
  showLoader: boolean = false;
  userSectionActive: boolean = false;
  saveLabel: string;
  vendorRoleListData: VendorRoleListResponse[];
  vendorActiveListData: VendorActiveListResponse[] = [];
  userDetailsLabel: string;
  enterLabel: string= 'common.warning.enterLabelCommon';
  selectLabel: string= 'common.warning.selectLabelCommon';

  @Input()
  vendorId: number;
  vendorUserId: number;
  editVendor: boolean = false;
  disabled : boolean = false;
  firstTime : boolean = false;
  altLoader: boolean = false;
  dummyPassword: string;
  dummyPasswordEntered: boolean = true;
  field: Field;

  constructor(
    private ACTIVATED_ROUTE: ActivatedRoute,
    private router: Router,
    private newVendorService : NewVendorService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private location: Location,
    private dialogsService: DialogService,
    private sessionService: SessionService,
    private i18UtilService: I18UtilService
  ) {
    this.showLoader = true;
    this.userSectionActive = true;
    this.firstTime = true;
    this.dummyPassword='********';
    this.initializeUserDetails();
    this.populateMasters();
  }

  ngOnInit() {
    this.altLoader = true;
    this.ACTIVATED_ROUTE.parent.params.subscribe((params) => {
      this.vendorId= this.ACTIVATED_ROUTE.snapshot.parent.params['id'];
    });
    if (this.vendorId != null) {
      this.editVendor = true;
      this.firstTime = false;
      this.i18UtilService.get('common.label.update').subscribe((res: string) => {
        this.saveLabel = res;
       });
      this.newVendorService.getVendorUserDetails(this.vendorId).subscribe(result => {
        if(result.responseData.length==0){
          this.newVendorService.getVendorAdminID().subscribe((response) => {
            this.vendorAdminId = response.response.roleId;
            this.editVendor = false;
            this.firstTime = true;
            this.items = this.formGroup.get('items') as FormArray;
            this.items.push(this.createItem());
            this.i18UtilService.get('common.label.save').subscribe((res: string) => {
              this.saveLabel = res;
             });
            this.altLoader = false;
          })
        } else{
          this.firstTime = true;
          this.setVendorUserDetails(result);
          this.altLoader = false;
        }
      });
    } else {
      this.newVendorService.getVendorAdminID().subscribe((response) => {
        this.i18UtilService.get('vendor.warning.saveCompanyDetails').subscribe((res: string) => {
          this.notificationService.setMessage(NotificationSeverity.WARNING, res);
        });
        this.vendorAdminId = response.response.roleId;
        this.editVendor = false;
        this.items = this.formGroup.get('items') as FormArray;
        this.items.push(this.createItem());
        this.i18UtilService.get('common.label.save').subscribe((res: string) => {
          this.saveLabel = res;
         });
        this.altLoader = false;
      })
    }
    this.i18UtilService.get('vendor.create_vendor.userDetails').subscribe((res: string) => {
      this.userDetailsLabel = res;
     });
  }
  
  goBack() {
    this.router.navigateByUrl("/vendor/vendorlist");
  }

  toggleDiv() {
    this.userSectionActive = !this.userSectionActive;
  }

  isNull(control: FormControl) {
    if (control.value == null || control.value == '' || (typeof control.value == 'string' && control.value.trim() == '')) {
        return true;
    }
    return false;
  }

  populateMasters(){
    
    this.vendorActiveListData.push(new VendorActiveListResponse(1,'Yes'));
    this.vendorActiveListData.push(new VendorActiveListResponse(2,'No'));

     this.newVendorService.getVendorRoleList().subscribe((response) => {
      this.vendorRoleListData = response.response;
      if(this.vendorRoleListData!=null){
        for (let item of this.vendorRoleListData) {
          item.id = item.roleId;
          item.name = item.roleName;
          if(item.name.toLowerCase() =='vendor admin'){
            this.vendorAdminId= item.id;
          }
        }
      }
    })
  }

  initializeUserDetails() {
    this.formGroup = this.formBuilder.group({
        items : this.formBuilder.array([])
    });
  }

  enableFields(){
  }

  createItem(): FormGroup{
    return this.formBuilder.group({
    firstName: '',
    lastName: '',
    emailID: ['', [ValidationService.emailValidator]],
    mobile: ['', [ValidationService.MobileValidator]],
    userName: '',
    password: ['', [ValidationService.passwordValidator]],
    role: this.vendorAdminId,
    active: '1',
    saveFlag: false
    });
  }

  saveVendorUserDetails(item,i){
    this.notificationService.clear();
    this.editVendor = true;
    let valid: boolean = true;
    this.dummyPasswordEntered=true;
    item.saveFlag = true;
    let saveVendorUserRequest: SaveVendorUserRequest = this.getNewVendorUserRequest(item,i);
    valid = this.checkMandatory(saveVendorUserRequest);
    if(!valid){
      item.saveFlag = false;
      return;
    }
    this.newVendorService.createVendorUser(saveVendorUserRequest).subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code != null) {
        if (response.messageCode.code == "EC200") {
          this.i18UtilService.get('common.label.update').subscribe((res: string) => {
            this.saveLabel = res;
           });
          this.i18UtilService.get('common.success.savedsuccess').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.INFO, res);
           });
        } else if (response.messageCode.code == "EC201") {
          this.notificationService.setMessage(NotificationSeverity.WARNING, response.messageCode.message);
        } else {
          this.i18UtilService.get('common.error.failedsave').subscribe((res: string) => {
            this.notificationService.setMessage(NotificationSeverity.WARNING, res);
           });
        }
      }
      item.saveFlag = false;
    });
  }

  getNewVendorUserRequest(item,i) {
    let saveVendorUserRequest: SaveVendorUserRequest = new SaveVendorUserRequest()
    saveVendorUserRequest.input = new NewVendorUserRequest();
    saveVendorUserRequest.input.vendorID = this.vendorId;
    if(item.controls.vendorUserId != null && item.controls.vendorUserId!= undefined)
      saveVendorUserRequest.input.vendorUserID = item.controls.vendorUserId.value;
    saveVendorUserRequest.input.firstName = item.controls.firstName.value;
    saveVendorUserRequest.input.lastName = item.controls.lastName.value;
    saveVendorUserRequest.input.userEmailID = item.controls.emailID.value;
    saveVendorUserRequest.input.mobile = item.controls.mobile.value;
    saveVendorUserRequest.input.userName = item.controls.userName.value;
    
    if(item.controls.password.value == this.dummyPassword){
      this.dummyPasswordEntered =  true;
      
    //   if (!saveVendorUserRequest.input.password.match(/((?=.*\d)(?=.*[a-zA-Z])(?=.*[`!~@#$%^&*]).{6,20})/)) {
    //     this.notificationService.addMessage(NotificationSeverity.WARNING, 'User Details' + " : Please enter Password.");
    //     item.saveFlag = false;
    //     return;
    //   } else{
    //     this.dummyPasswordEntered = true;
    //   }
    }else{
      saveVendorUserRequest.input.password = item.controls.password.value;
      this.dummyPasswordEntered =  false;
    }
    saveVendorUserRequest.input.role = item.controls.role.value;
    saveVendorUserRequest.input.active = item.controls.active.value; 
    return saveVendorUserRequest;
  }
  checkMandatory(saveVendorUserRequest){
    let valid: boolean = true;
    if ( saveVendorUserRequest.input.firstName == null || saveVendorUserRequest.input.firstName.trim() == '') {
        this.addMessage(this.userDetailsLabel, this.enterLabel,  'vendor.create_vendor.firstName');
        valid = false;
    }
    if ( saveVendorUserRequest.input.userEmailID == null || saveVendorUserRequest.input.userEmailID.trim() == '') {
      this.addMessage(this.userDetailsLabel, this.enterLabel,  'vendor.create_vendor.emailID');
      valid = false;
    }
    if ( saveVendorUserRequest.input.userEmailID != null || saveVendorUserRequest.input.userEmailID.trim() != '') {
      this.field = new Field();
      this.field.value =  saveVendorUserRequest.input.userEmailID ;
      if(ValidationService.emailValidator(this.field)){
        this.addMessage(this.userDetailsLabel, this.enterLabel,  'vendor.create_vendor.emailIDCorrectFormate');
        valid = false;
      }
    }
    if ( saveVendorUserRequest.input.mobile == null || saveVendorUserRequest.input.mobile.trim() == '') {
      this.addMessage(this.userDetailsLabel, this.enterLabel, 'common.label.mobile');
      valid = false;
    }
    if ( saveVendorUserRequest.input.mobile != null || saveVendorUserRequest.input.mobile.trim() != '') {
      this.field = new Field();
      this.field.value =  saveVendorUserRequest.input.mobile ;
      if(ValidationService.MobileValidator(this.field)){
        this.addMessage(this.userDetailsLabel, this.enterLabel, 'vendor.create_vendor.mobileNumberCorrectFormate');
        valid = false;
      }
    }
    if ( saveVendorUserRequest.input.userName == null || saveVendorUserRequest.input.userName.trim() == '') {
      this.addMessage(this.userDetailsLabel, this.enterLabel, 'vendor.create_vendor.userName');
      valid = false;
    }
    
if(!this.dummyPasswordEntered){
    if ( saveVendorUserRequest.input.password == null || saveVendorUserRequest.input.password.trim() == '') {
      this.addMessage(this.userDetailsLabel, this.enterLabel, 'vendor.create_vendor.password');
      valid = false;
    }else if (!saveVendorUserRequest.input.password.match(/((?=.*\d)(?=.*[a-zA-Z])(?=.*[`!~@#$%^&*]).{6,20})/)) {
            this.addMessage(this.userDetailsLabel, this.enterLabel, 'vendor.create_vendor.correctPassword');
            valid=false;    
    }

  }
    if ( saveVendorUserRequest.input.role == null || saveVendorUserRequest.input.role == 0) {
      this.addMessage(this.userDetailsLabel, this.enterLabel, 'postJobBoard.label.role');
      valid = false;
    }
    if ( saveVendorUserRequest.input.active == null ||  saveVendorUserRequest.input.active == 0) {
      this.addMessage(this.userDetailsLabel, this.enterLabel, 'vendor.create_vendor.correctPassword');
      valid = false;
    }
    return valid;
  }
  addMessage(section: string, action: string, label: string) {
    this.i18UtilService.get(label).subscribe((res: string) => {
      label = res;
    });
    this.i18UtilService.get(action , { 'label': label }).subscribe((res: string) => {
      this.notificationService.addMessage(NotificationSeverity.WARNING, res)
    });

  }

  setVendorUserDetails(resultList: any) : void {
    this.items = this.formGroup.get('items') as FormArray;
    for(let result of resultList.responseData){
      let userDetail = new FormGroup({
        vendorUserId: new FormControl(result.vendorUserID),
        firstName: new FormControl(result.firstName),
        lastName: new FormControl(result.lastName),
        emailID: new FormControl(result.emailID,[ValidationService.emailValidator]),
        mobile: new FormControl(result.mobile,[ValidationService.MobileValidator]),
        userName: new FormControl(result.userName),
        password: new FormControl(this.dummyPassword,[ValidationService.passwordValidator]),
        role: new FormControl(result.role),
        active: new FormControl(result.active)
      });
      this.items.push(userDetail);
    }
  }  
}


export class SaveVendorUserRequest {
  input: NewVendorUserRequest;
}

export class NewVendorUserRequest {
  vendorID: number;
  vendorUserID: number;
  firstName: string;
  lastName: string;
  userEmailID: string;
  mobile: string;
  userName: string;
  password: string;
  role: number;
  active: string;
}
export class Field{
  value : string;
}