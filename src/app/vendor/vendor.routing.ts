import { GroupListComponent } from './group-list/group-list.component';
import { VendorComponent } from './vendor.component';
import { RouterModule, Routes } from '@angular/router';
import { SessionGuard } from '../session.guard';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { NewVendorComponent } from './new-vendor/new-vendor.component';
import { UserDetailsComponent } from './new-vendor/user-details/user-details.component';
import { CompanyDetailsComponent } from './new-vendor/company-details/company-details.component';

const VENDOR_ROUTES: Routes = [
    {
        path: '', component: VendorComponent, children: [
            { path: 'grouplist', component: GroupListComponent },
            { path: 'vendorlist', component: VendorListComponent, canActivate: [SessionGuard]  },
            { path: 'editvendor/:id', component: NewVendorComponent,   
                children: [
                    { path: '', redirectTo: 'company-details' },
                    { path: 'company-details', component: CompanyDetailsComponent },
                    { path: 'user-details', component: UserDetailsComponent }
                ]
            },
            { 
                path: 'newvendor', component: NewVendorComponent,
                children: [
                    { path: '', redirectTo: 'company-details' },
                    { path: 'company-details', component: CompanyDetailsComponent },
                    { path: 'user-details', component: UserDetailsComponent }
                ]
            }
        ]
    }
];

export const VendorRouting = RouterModule.forChild(VENDOR_ROUTES);
