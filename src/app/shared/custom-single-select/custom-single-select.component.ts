import { CustomSingleSelectService } from './custom-single-select.service';
import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ViewChild, ElementRef, NgZone, HostListener } from '@angular/core';
import { SelectItem } from '../select-item';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'alt-custom-single-select',
  templateUrl: './custom-single-select.component.html',
  styleUrls: ['./custom-single-select.component.css']
})
export class CustomSingleSelectComponent implements OnInit, OnChanges {
  @Input() isDisabled: boolean = false;
  @Input() hiddenToggle: boolean;
  @Input() valueBindType: string;
  @Input() value: any;
  @Input() maxResults: Number;
  
  @Input() inputDataList: any;
  @Input() placeholder: String;
  @Input() placeholderHard: String;
  @Input() searchEnable: boolean;
  @Input() valueKey: string;
  @Input() labelKey: string;
  @Input() allowZeroValue: boolean;
  @Output() valueBind: EventEmitter<any> = new EventEmitter();
  @Output() emitSelected: EventEmitter<any> = new EventEmitter();
  @Input() tooltipLength: any;
  @Input() tooltipEnabled: boolean = false;
  @ViewChild('dropdownposition') dropdownposition: ElementRef;
  @ViewChild('dropdownbtn') dropdownbtn: ElementRef;
  @Input() isDynamicSearch: boolean = false;
  @Input() selectName: any;
  @Input() inputList: any;
  
  @Input() disableList: boolean = false;

  @ViewChild('dropdownCont') dropdownWidth: ElementRef;


  eventOptions: boolean | { capture?: boolean, passive?: boolean };
  alreadyTested: boolean = false;
  passiveSupportedd: boolean = false;

  filteredList: any[];
  searchString = new FormControl();
  resultList: any;
  buttonLabel: String;
  divClass: string;
  changedValue: string;

  dropdownContWidth: any;


  reverseClassVisibility: boolean = false;

  constructor(private ngZone: NgZone, private customSingleSelectService: CustomSingleSelectService) {
    this.resultList = new Array<any>();
    this.divClass = "";
    this.searchEnable = false;
  }

  ngOnInit() {

    if (this.isDynamicSearch) {
      this.customSingleSelectService.name = this.selectName;
      let subs = this.customSingleSelectService.getDataList().subscribe((data) => {
        if (this.customSingleSelectService.name == this.selectName) {
          this.inputList = data;
          this.searchListener(this.customSingleSelectService.searchString);
        }
      },
        () => {
          subs.unsubscribe();
        },
        () => {
          subs.unsubscribe();
        },
      );
    }

    if(this.maxResults == undefined || this.maxResults == null)
    {
      this.maxResults = 20;
    }

    if (this.passiveSupported()) {
      this.eventOptions = {
        capture: true,
        passive: true
      };
    } else {
      this.eventOptions = true;
    }
    this.ngZone.runOutsideAngular(() => {
      window.addEventListener('scroll', this.scroll, <any>this.eventOptions);
    });

    this.hiddenToggle = false;
    this.searchString.valueChanges.subscribe(changedValue => {
      this.changedValue = changedValue;
      if (this.isDynamicSearch) {
        this.customSingleSelectService.searchString = this.changedValue;
        this.customSingleSelectService.refreshData();
      } else {
        this.searchListener(changedValue);
      }
    });

  }

  adjustToolTip() {
    this.dropdownContWidth = this.dropdownWidth.nativeElement.offsetWidth - 20;
  }

  passiveSupported() {
    if (this.alreadyTested) {
      return this.passiveSupported;
    } else {
      this.alreadyTested = true;

      // Test via a getter in the options object to see if the passive property is accessed
      try {
        var opts = Object.defineProperty({}, 'passive', {
          get: function () {
            this.passiveSupported = true;
          }
        });
        window.addEventListener('test', null, opts);
      } catch (e) { }
      return this.passiveSupported;
    }
  }

  ngOnChanges() {
    this.filteredList = this.inputList;
    let buttonLabelSet = false;
    if (this.inputList != null && this.value != null && (this.value != '' && this.value != 0 || this.allowZeroValue)) {
      if (this.valueBindType != null && this.valueBindType.trim().toLowerCase() == 'label') {
        for (let item of this.inputList) {
          if (item.label == this.value) {
            this.buttonLabel = item.label;
            buttonLabelSet = true;
            break;
          }
        }
      } else {
        for (let item of this.inputList) {
          if (this.valueKey == null && item.value == this.value) {
            this.buttonLabel = item.label;
            buttonLabelSet = true;
            this.emitSelected.emit(item);
            break;
          } else if (this.valueKey != null) {
            if (this.valueKey == 'id' && item.id == this.value) {
              if (item.label) {
                this.buttonLabel = item.label;
                buttonLabelSet = true;
              } else if (item.name) {
                this.buttonLabel = item.name;
                buttonLabelSet = true;
              }
              this.emitSelected.emit(item);
              break;
            } else if (this.valueKey == 'sourceID' && item.sourceID == this.value) {
              if (item.sourceName != null && item.sourceName.indexOf(" - ") == -1) {
                this.buttonLabel = (item.sourceCode != null ? item.sourceCode : '') + ' - ' + (item.sourceName != null ? item.sourceName : '');
              } else {
                this.buttonLabel = (item.sourceName != null ? item.sourceName : '');
              }
              buttonLabelSet = true;
              this.emitSelected.emit(item);
              break;
            } else if (this.valueKey == 'requisitionId' && item.requisitionId === this.value) {
              this.buttonLabel = item.requisitionCode;
              buttonLabelSet = true;
              this.filteredList.filter(item=>item.requisitionId === this.value)[0].isSelected=true;
              this.emitSelected.emit(item);
              break;
            }
          }
        }
      }
    }
    if (!buttonLabelSet && this.placeholder != null && this.placeholder.trim() != '') {
      this.buttonLabel = this.placeholder;
    } else if (!buttonLabelSet) {
      this.buttonLabel = "Select";
    }
  }

  searchListener(searchString: string) {
    this.filteredList = new Array<any>();
    if (this.inputList != null) {
      for (let item of this.inputList) {
        if (
          this.labelKey == null
          && item.label != null
          && (
            (typeof item.label == 'string' && item.label.toLowerCase().indexOf(searchString.toLowerCase()) != -1)
            || (typeof item.label == 'number' && (item.label + "").indexOf(searchString.toLowerCase()) != -1)

          )) {
          this.filteredList.push(item);
        } else if (
          this.labelKey == "sourceID"
          && (
            (item.sourceName != null && item.sourceName.toLowerCase().indexOf(searchString.toLowerCase()) != -1)
            || (item.sourceCode != null && item.sourceCode.toLowerCase().indexOf(searchString.toLowerCase()) != -1))
        ) {
          this.filteredList.push(item);
        } else if (this.labelKey == 'requisitionCode' && item.requisitionCode.toLowerCase().indexOf(searchString.toLowerCase()) != -1) {
          this.filteredList.push(item);
        } else if (this.labelKey == 'name' && item.name != null && item.name.toLowerCase().indexOf(searchString.toLowerCase()) != -1) {
          this.filteredList.push(item);
        }
      }
    }
  }

  selectListener(item: any) {
    if (this.disableList) {
      return;
    }
    this.clearAllSelects();
    if (this.placeholderHard != null && this.placeholderHard.length > 0) {
      this.buttonLabel = this.placeholderHard;
    } else {
      if (this.labelKey == null) {
        this.buttonLabel = item.label;
      } else {
        if (this.labelKey == 'sourceID') {
          if (item.sourceName != null && item.sourceName.indexOf(" - ") == -1) {
            this.buttonLabel = (item.sourceCode != null ? item.sourceCode : '') + ' - ' + (item.sourceName != null ? item.sourceName : '');
          } else {
            this.buttonLabel = (item.sourceName != null ? item.sourceName : '');
          }
        } else if (this.labelKey == 'requisitionCode') {
          this.buttonLabel = item.requisitionCode;
        } else if (this.labelKey == 'name') {
          this.buttonLabel = item.name;
        }
      }
    }
    item.isSelected = true;
    this.valueBind.emit(item);
    this.toggle();
  }

  clearAllSelects() {
    for (let i of this.filteredList) {
      if (i.hasOwnProperty("isSelected")) {
        i.isSelected = false;
      }
    }
  }

  toggle() {
    this.hiddenToggle = !this.hiddenToggle;
    this.searchString.setValue("");
    if (this.hiddenToggle) {
      this.divClass = "open";
    } else {
      this.divClass = "";
    }
  }

  toggleClose() {
    this.hiddenToggle = false;
    this.searchString.setValue("");
    this.divClass = "";
  }

  scroll = (): void => {
    this.splitDropdown();
  };

  // Parent scroll stop on the child Mouse hover
  @HostListener('window:mousewheel', ['$event'])
  onMousewheel(event: any) {
    event.stopPropagation();
  }

  @HostListener('window:mousewheel', ['$event'])
  onDOMMouseScroll(event: any) {
    event.stopPropagation();
  }

  splitDropdown() {
    if (!(this.dropdownbtn == undefined || this.dropdownbtn == null || !this.dropdownbtn || this.dropdownposition == undefined || this.dropdownposition == null || !this.dropdownposition)) {
      let y = this.dropdownbtn.nativeElement;
      let x = this.dropdownposition.nativeElement;
      let doc = document.documentElement;
      let scrolltop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

      let viewportOffset = y.getBoundingClientRect();
      let topOffset = viewportOffset.top;
      let bottomTemp = viewportOffset.bottom;
      var windHeight = window.innerHeight;
      let bottomOffset = windHeight - bottomTemp;

      let dropdownHeight = 0;
      if (this.searchEnable) {
        dropdownHeight += 80;
      }
      if (!(typeof this.filteredList == undefined || this.filteredList == undefined || this.filteredList.length == undefined)) {
        dropdownHeight += (this.filteredList.length * 40);
      }
      if (dropdownHeight >= 280) {
        dropdownHeight = 280;
      }
      if (bottomOffset < dropdownHeight) {
        this.reverseClassVisibility = true;
      }
      else {
        this.reverseClassVisibility = false;
      }
    }
  }

}
