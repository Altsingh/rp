import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { SessionService } from './../../session.service';
import { Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomSingleSelectService {

  public searchString: string;
  public name: string;
  private dataList = new Subject<string>();
  private refreshDataFlag = new Subject<boolean>();

  constructor(private http: Http, private sessionService:SessionService) {}


  public refreshData(){
    this.refreshDataFlag.next(true);
  }

  public isRefresshData(): Observable<boolean> {
    return this.refreshDataFlag.asObservable();
  }

  public setDataList(list){
    this.dataList.next(list);
  }

  public getDataList(): Observable<string>{
    return this.dataList.asObservable();
  }

}
