import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'loader-indicator',
  templateUrl: './loader-indicator.component.html',
  styleUrls: ['./loader-indicator.component.css']
})
export class LoaderIndicatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
