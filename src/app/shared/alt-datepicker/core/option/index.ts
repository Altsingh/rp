import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MdRippleModule} from '../ripple/index';
import {MdSelectionModule} from '../selection/index';
import {MatOption} from './option';
import {MdOptgroup} from './optgroup';


@NgModule({
  imports: [MdRippleModule, CommonModule, MdSelectionModule],
  exports: [MatOption, MdOptgroup],
  declarations: [MatOption, MdOptgroup]
})
export class MatOptionModule {}


export * from './option';
export * from './optgroup';
