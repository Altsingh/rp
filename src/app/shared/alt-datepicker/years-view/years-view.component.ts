import { AfterContentInit, ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewEncapsulation } from '@angular/core';
import { Md2CalendarCell } from '../calendar-body';
import { MdDateFormats, MD_DATE_FORMATS } from '../core/datetime/date-formats';
import { DateLocale } from '../date-locale';
import { DateUtil } from '../date-util';
import { slideCalendar } from '../datepicker-animations';

@Component({
  selector: 'alt-years-view',
  templateUrl: './years-view.component.html',
  styleUrls: ['./years-view.component.css'],
  animations: [slideCalendar],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class YearsViewComponent implements OnInit, AfterContentInit {


  @Input()
  get activeDate(): Date { return this._activeDate; }
  set activeDate(value: Date) {
    let oldActiveDate = this._activeDate;
    this._activeDate = value || this._util.today();
    if (oldActiveDate && this._activeDate &&
      !this._util.isSameYear(oldActiveDate, this._activeDate)) {
      this._init();
      // if (oldActiveDate < this._activeDate) {
      //  this.calendarState('right');
      // } else {
      //  this.calendarState('left');
      // }
    }
  }
  private _activeDate: Date;

  /** The currently selected date. */
  @Input()
  get selected(): Date { return this._selected; }
  set selected(value: Date) {
    this._selected = value;
  }
  private _selected: Date;

  /** A function used to filter which dates are selectable. */
  @Input() dateFilter: (date: Date) => boolean;

  /** Emits when a new month is selected. */
  @Output() selectedChange = new EventEmitter<Date>();

  /** Grid of calendar cells representing the months of the year. */
  _years: Md2CalendarCell[][];

  /** The label for this year (e.g. "2017"). */
  _yearLabel: string;

  /** The month in this year that today falls on. Null if today is in a different year. */
  _todayYear: number;

  /**
   * The month in this year that the selected Date falls on.
   * Null if the selected Date is in a different year.
   */
  _selectedYear: number;

  _calendarState: string;

  constructor(private _locale: DateLocale, public _util: DateUtil,
    @Optional() @Inject(MD_DATE_FORMATS) private _dateFormats: MdDateFormats) {
    if (!this._dateFormats) {
      throw Error('MD_DATE_FORMATS');
    }
    this._activeDate = this._util.today();
  }

  ngOnInit() {
  }

  
  ngAfterContentInit() {
    this._init()
  }

  /**
   * get the year range to be represented on calender,
   * and changes based on currently active date(year)
   */
  get years(): Md2CalendarCell[][] {
    let yearNames = this._locale.getYearNames(this.activeDate,6)
    return [[0, 1, 2, 3,], [4, 5, 6, 7], [8, 9, 10, 11]].map(row => row.map(
      year => this._createCellForYear(yearNames[year])));
  }

  /** Initializes this years view. */
  private _init() {
    this._selectedYear = this._getYearInCalender(this.selected);
    this._todayYear = this._getYearInCalender(this._util.today());
    //below section will be required for future
    /*    let yearNames=this._locale.getYearNames(this._util.today())
       this._years = [[0, 1, 2, 3, 4,5], [6, 7, 8, 9,10,11]].map(row => row.map(
         year => this._createCellForYear(yearNames[year]))); */
  }

  /**
  * Gets the year in this calender that the given Date falls on.
  * 
  */
  private _getYearInCalender(date: Date) {
    return date ? this._util.getYear(date) : null;
  }

  /** Creates an MdCalendarCell for the year range determined by currently selected year. */
  private _createCellForYear(yearName: string) {
    return new Md2CalendarCell(
      Number.parseInt(yearName), yearName, yearName, true);
  }

  private calendarState(direction: string): void {
    this._calendarState = direction;
  }

  _calendarStateDone() {
    this._calendarState = '';
  }

  /** Emits the date based on current years but set the date to 1st of month for previously */
  _yearSelected(year: number) {
    this.selectedChange.emit(this._util.createDate(
      year,0,
      1,
      this._util.getHours(this.activeDate),
      this._util.getMinutes(this.activeDate),
      this._util.getSeconds(this.activeDate)));
  }
}
