import {Component, Input, OnChanges} from "@angular/core";
import {DataTable} from "./DataTable";
import * as _ from "lodash";

@Component({
    selector: "mfBootstrapPaginator",
    template: `
    <mfPaginator #p [mfTable]="mfTable">
    <div class="card-pagination fl-left">
        <label class="pagination-text" for="PaginationTop">showing</label>
        <div class="dropdown-11">
            <a href="#" class="dropdown-button dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="text-dropdown" *ngIf="p.rowsOnPage <= p.dataLength" >{{p.rowsOnPage}}</span>
            <span class="text-dropdown" *ngIf="p.rowsOnPage > p.dataLength" >{{p.dataLength}}</span>
            <span class="mdi mdi-chevron-down md24"></span></a>
            <ul class="list-reset dropdown-1 dropdown-style" *ngIf="p.dataLength > minRowsOnPage">
                <li *ngFor="let rows of rowsOnPageSet" [class.active]="p.rowsOnPage===rows" (click)="p.setRowsOnPage(rows)">
                    <a>{{rows}}</a>
                </li>
            </ul>
      </div>
     <span class="totale-num text-uppercase">of {{p.dataLength}}</span>
</div>
         <ul class="pagination pull-right" *ngIf="p.dataLength > p.rowsOnPage">
            <li class="page-item" [class.disabled]="p.activePage <= 1" (click)="p.setPage(1)"><span>first</span></li>
            <li class="perview" [class.disabled]="p.activePage <= 1" (click)="p.setPage(1)"><a><span class="mdi mdi-chevron-left md24"></span></a></li>

            <li class="page-item" *ngIf="p.activePage > 4 && p.activePage + 1 > p.lastPage" (click)="p.setPage(p.activePage - 4)">
                <a>{{p.activePage-4}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage > 3 && p.activePage + 2 > p.lastPage" (click)="p.setPage(p.activePage - 3)">
                <a>{{p.activePage-3}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage > 2" (click)="p.setPage(p.activePage - 2)">
                <a>{{p.activePage-2}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage > 1" (click)="p.setPage(p.activePage - 1)">
                <a>{{p.activePage-1}}</a>
            </li>
            <li class="page-item active">
                <a>{{p.activePage}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage + 1 <= p.lastPage" (click)="p.setPage(p.activePage + 1)">
                <a>{{p.activePage+1}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage + 2 <= p.lastPage" (click)="p.setPage(p.activePage + 2)">
                <a>{{p.activePage+2}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage + 3 <= p.lastPage && p.activePage < 3" (click)="p.setPage(p.activePage + 3)">
                <a>{{p.activePage+3}}</a>
            </li>
            <li class="page-item" *ngIf="p.activePage + 4 <= p.lastPage && p.activePage < 2" (click)="p.setPage(p.activePage + 4)">
                <a>{{p.activePage+4}}</a>
            </li>
            <li class="nextview" [class.disabled]="p.activePage >= p.lastPage" (click)="p.setPage(p.lastPage)"><a><span class="mdi mdi-chevron-right md24"></span></a></li>
            <li class="page-item" [class.disabled]="p.activePage >= p.lastPage" (click)="p.setPage(p.lastPage)"><span>last</span></li>
        </ul>
   
    </mfPaginator>
    `
})
export class BootstrapPaginator implements OnChanges {
    @Input("rowsOnPageSet") rowsOnPageSet = [];
    @Input("mfTable") mfTable: DataTable;

    minRowsOnPage = 0;

    ngOnChanges(changes: any): any {
        if (changes.rowsOnPageSet) {
            this.minRowsOnPage = _.min(this.rowsOnPageSet)
        }
    }
}