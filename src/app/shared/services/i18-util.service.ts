import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from 'app/session.service';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';


@Injectable()
export class I18UtilService {


  constructor(private translateService: TranslateService, private sessionService: SessionService) {
    //this.translateService.use('en'); 
    this.sessionService.langObservable.subscribe (lang=>{
      this.translateService.use(lang);
    })
   }

  get(code: string, args?: any):Observable<string> {
    if (!isNullOrUndefined(args)) {
      return this.translateService.get(code, args);
    } else {
      return this.translateService.get(code);
    }
  }
  


  
}

