import { Injectable, SecurityContext, ValueProvider } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { isNullOrUndefined } from 'util';

@Injectable()
export class CustomValidatorService {

  constructor(private sanitizer:DomSanitizer) { }


  static crossSideScriptValidator(sanitizer:DomSanitizer): ValidatorFn {
   
    return (control: AbstractControl): {[key: string]: any} | null => {
      try{

        const forbidden =sanitizer.sanitize(SecurityContext.HTML,control.value);
        console.log(forbidden);
        if(forbidden!== control.value && forbidden!=CustomValidatorService.getCharacterCodeValue(control.value)){
          return    {'crossSideScriptValidation': true}
        }
        return null
      }catch(error){
        console.log(error)
         return  {'crossSideScriptValidation': true}
      }
    }
 }
 static getCharacterCodeValue(value:string){
  var NON_ALPHANUMERIC_REGEXP = /([^\#-~ |!])/g;
   if(isNullOrUndefined(value)|| value===""){
     return value;
   }else{ 
    return value.replace(NON_ALPHANUMERIC_REGEXP,  (match)=> {
       return '&#' + match.charCodeAt(0) + ';';
       })
   }
 }
}
