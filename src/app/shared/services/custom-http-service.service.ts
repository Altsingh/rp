import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { isNullOrUndefined } from 'util';

@Injectable()
export class CustomHttpService {

  constructor(private httpClient:HttpClient) { }

  public get<T>(url:string,options?:any):Observable<any>{
    if(isNullOrUndefined(options)){
      return this.httpClient.get<T>(url);
    }else{
      return this.httpClient.get<T>(url,options)
    }
  }

  public post<T>(url:string,body?:any,header?:Headers):Observable<any>{
    if(isNullOrUndefined(body)){
      return this.httpClient.post<T>(url,{});
    }else{
      return this.httpClient.post<T>(url,body);
    }
  }


}
