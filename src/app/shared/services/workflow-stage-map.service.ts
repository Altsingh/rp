import { Injectable } from '@angular/core';
import { CachedData } from '../../common/cached-data';
import { Http } from '@angular/http';
import { SessionService } from '../../session.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WorkflowStageMapService extends CachedData {

  constructor(http: Http,
    sessionService: SessionService) {
    super(http, sessionService);
    this.initialize('/rest/altone/common/workflowStageMap', 'GET', null);
  }

  getMap(): Observable<any> {
    return this.getData().map(
      response => { return response.responseMap; }
    );
  }

}
