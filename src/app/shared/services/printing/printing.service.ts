import { Injectable } from '@angular/core';
import { Subject,  } from 'rxjs';

@Injectable()
export class PrintingService {

  private printingState= PRINTINGSTATE.PRINTABLE;
  constructor() { }

  private printingObservable = new Subject<PRINTINGSTATE>();
  printingObservable$ = this.printingObservable.asObservable();

  public initiatePrinting(){
    this.printingObservable.next(PRINTINGSTATE.INTIALIZE);
    this.setPrintingState(PRINTINGSTATE.INTIALIZE)
  }

  public setPrintStateToReady(){
    this.printingObservable.next(PRINTINGSTATE.READY);
    this.setPrintingState(PRINTINGSTATE.READY)
  }

  public setPrintStateTOComplete():void{
    this.printingObservable.next(PRINTINGSTATE.COMPLETED);
    this.setPrintingState(PRINTINGSTATE.COMPLETED)
  }

  public getPrintingState():PRINTINGSTATE{
    return this.printingState;
  }

  public setPrintingState(printingState:PRINTINGSTATE){
    this.printingState = printingState;
  }

}

export enum PRINTINGSTATE{
  PRINTABLE,
  INTIALIZE,
  READY,
  COMPLETED
}
