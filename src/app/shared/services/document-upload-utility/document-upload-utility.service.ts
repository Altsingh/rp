import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import * as JSZip from 'jszip';

/**
 * this service helps user to validate files and extract zipped file
 */
@Injectable()
export class DocumentUploadUtilityService {

  constructor() { }

  /**
   * This is asynchronous funtion taking File list and options of type DocumentValidationOptions
   * it performs the validation of FileList by using the options passed as arguments
   * @param fileList 
   * @param options 
   */
  async validateFileBeforeUpload(fileList: File[], options: DocumentValidationOptions): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!isNullOrUndefined(fileList) && fileList.length > 0) {
        const zippedFiles: File[] = [];
        if (options._supportedExtension.indexOf('zip') !== -1) {
          for (let file of fileList) {
            if (file.name.split('.').pop().toLowerCase() === 'zip') {
              zippedFiles.push(file);
            }
          }
          if (zippedFiles.length > 0) {
            if (zippedFiles.length > 1) {
              reject(DocumentValidationErrorCodes.MULTIPLE_ZIP_ERROR);
            } else if (!options._isOtherFileWithZipAllowed && zippedFiles.length < fileList.length) {
              reject(DocumentValidationErrorCodes.File_ALONG_ZIP);
            } else if (zippedFiles.length == 1) {
              this.getZipContent(zippedFiles[0]).then(fileList => {
                resolve(this.validateFiles(fileList, options));
              }).catch((error) => {
                reject(error);
              });
            }
          } else {
            resolve(this.validateFiles(fileList, options));
          }
        } else {
          resolve(this.validateFiles(fileList, options));
        }

      } else {
        reject(DocumentValidationErrorCodes.NULL_FILE_OBJECT)
      }
    })

  }

  /**
   * this fuction takes Only the zipped file and extract them to actual files.
   * @param zippedFiles 
   */
  private async getZipContent(zippedFiles: File): Promise<any> {
    return new Promise((resolve, reject) => {
      const jszip = new JSZip();
      let fileList = [];
      let numberofEnteries;
      jszip.loadAsync(zippedFiles).then((zip) => {
        Object.keys(zip['files']).forEach((filename) => {
          zip['files'][filename].async('blob').then((blobFile) => {
            fileList.push(new File([blobFile], filename));
            numberofEnteries = Object.keys(zip.files).length
            if (fileList.length === numberofEnteries) {
              resolve(fileList);
            }
          }).catch(error => {
            reject(DocumentValidationErrorCodes.ZIP_PARSING_ERROR)
          })
        });
      }).catch(error => {
        reject(DocumentValidationErrorCodes.ZIP_PARSING_ERROR)
      })
    });
  }


  /**
   * this function takes document validation options and filelist and
   * verifies against the size, number of file and
   * supported extenstion based on options passed
   * @param fileList 
   * @param options 
   */
  private validateFiles(fileList: File[], options: DocumentValidationOptions): any {
    let unsupportedFiles = [];
    let sizeNotSupportedFiles = [];
    let invalidFileCount = 0;
    if (isNullOrUndefined(fileList)) {
      console.log('failed to validate null as file Object');
      throw DocumentValidationErrorCodes.NULL_FILE_OBJECT;
    }
    else {
      if (options._isTotalFileSizeChecked) {
        let totalFileSize = 0;
        for (let file of fileList) {
          totalFileSize += file.size;
        }
        if (!isNullOrUndefined(options._totalFileSize) && totalFileSize > options._totalFileSize) {
          throw DocumentValidationErrorCodes.MAX_TOTAL_SIZE;
        }
      }
      if (fileList.length > options._totalNumberOfFiles) {
        throw DocumentValidationErrorCodes.MAX_NUMBER_OF_FILE;
      } else {
        let tempFileList = [];
        for (let file of fileList) {
          let extenstion = file.name.split('.').pop().toLowerCase();
          if(extenstion.toLowerCase()==='zip'){
            throw DocumentValidationErrorCodes.NESTED_ZIP_ERROR;
          }
          if (options._isIndividualFileChecked && file.size > options._individualFileSize) {
            sizeNotSupportedFiles.push(file.name)
            invalidFileCount++;
          } else if (!options._supportedExtension.includes(extenstion)) {
            unsupportedFiles.push(file.name);
            invalidFileCount++;
          } else {
            tempFileList.push(file);
          }
        }
        const result: UploadDocumentValidationResult = {
          sizeNotSupportedFiles: sizeNotSupportedFiles,
          unsupportedFiles: unsupportedFiles,
          invalidFileCount: invalidFileCount,
          fileList: tempFileList
        }
        return result;
      }
    }
  }
}



export class DocumentValidationOptions {
  _individualFileSize: number = 8388608;
  _isIndividualFileChecked: boolean = false;
  _isTotalFileSizeChecked: boolean = true;
  _totalFileSize: number = 8388608;
  _isOtherFileWithZipAllowed: boolean = false;
  _totalNumberOfFiles: number = 100;
  _supportedExtension: string[]

  constructor(supportedExtension: string[], individualFileSize?: number, isIndividualFileChecked?: boolean, isTotalFileSizeChecked?: boolean,
    totalFileSize?: number, isOtherFileWithZipAllowed?: boolean, totalNumberOfFiles?: number) {
    this._supportedExtension = supportedExtension;
    this._individualFileSize = isNullOrUndefined(individualFileSize) ? this._individualFileSize : individualFileSize;
    this._isIndividualFileChecked = isNullOrUndefined(isIndividualFileChecked) ? this._isIndividualFileChecked : isIndividualFileChecked;
    this._isTotalFileSizeChecked = isNullOrUndefined(isTotalFileSizeChecked) ? this._isTotalFileSizeChecked : isTotalFileSizeChecked;
    this._totalFileSize = isNullOrUndefined(totalFileSize) ? this._totalFileSize : totalFileSize;
    this._isOtherFileWithZipAllowed = isNullOrUndefined(isOtherFileWithZipAllowed) ? this._isOtherFileWithZipAllowed : isOtherFileWithZipAllowed;
    this._totalNumberOfFiles = isNullOrUndefined(totalNumberOfFiles) ? this._totalNumberOfFiles : totalNumberOfFiles;
  }

  set individualFileSize(individualFileSize: number) {
    this._individualFileSize = individualFileSize;
  }

  set isIndividualFileChecked(isIndividualFileChecked: boolean) {
    this._isIndividualFileChecked = isIndividualFileChecked;
  }

  set isTotalFileSizeChecked(isTotalFileSizeChecked: boolean) {
    this._isTotalFileSizeChecked = isTotalFileSizeChecked;
  }

  set totalFileSize(totalFileSize: number) {
    this._totalFileSize = totalFileSize
  }
  /* set numberOfZipFile(numberOfZipFile: number) {
    this._numberOfZipFile = numberOfZipFile;
  } */
  set isOtherFileWithZipAllowed(isOtherFileWithZipAllowed: boolean) {
    this._isOtherFileWithZipAllowed = isOtherFileWithZipAllowed;
  }

  set totalNumberOfFiles(totalNumberOfFiles: number) {
    this._totalNumberOfFiles = totalNumberOfFiles;
  }
  set supportedExtension(supportedExtension: string[]) {
    this._supportedExtension = supportedExtension;
  }

  get individualFileSize(): number {
    return this._individualFileSize;
  }

  get isIndividualFileChecked(): boolean {
    return this._isIndividualFileChecked;
  }

  get isTotalFileSizeChecked(): boolean {
    return this._isTotalFileSizeChecked;
  }

  get totalFileSize(): number {
    return this._totalFileSize;
  }
  /* get numberOfZipFile(): number {
    return this._numberOfZipFile
  } */
  get isOtherFileWithZipAllowed(): boolean {
    return this._isOtherFileWithZipAllowed
  }

  get totalNumberOfFiles(): number {
    return this._totalNumberOfFiles
  }
  get supportedExtension(): string[] {
    return this._supportedExtension;
  }


}

export enum DocumentValidationErrorCodes {
  MAX_TOTAL_SIZE = 'MAX_TOTAL_SIZE',
  MAX_NUMBER_OF_FILE = 'MAX_NUMBER_OF_FILE',
  MAX_INDIVIDUAL_SIZE = 'MAX_INDIVIDUAL_SIZE',
  NULL_FILE_OBJECT = 'NULL_FILE_OBJECT',
  File_ALONG_ZIP = 'File_ALONG_ZIP',
  ZIP_PARSING_ERROR = 'ZIP_PARSING_ERROR',
  MULTIPLE_ZIP_ERROR = 'MULTIPLE_ZIP_ERROR',
  NESTED_ZIP_ERROR='NESTED_ZIP_ERROR'
}

export interface UploadDocumentValidationResult {
  sizeNotSupportedFiles: string[],
  unsupportedFiles: string[],
  invalidFileCount: number,
  fileList: File[]

}