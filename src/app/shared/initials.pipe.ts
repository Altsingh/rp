import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initials'
})
export class InitialsPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (value != null && typeof value == 'string') {

      value = value.trim();

      let regex = new RegExp('[^a-zA-Z]+', 'g')
      let splits = value.split(regex);
      let retVal = "";

      if (splits.length > 0) {
        retVal += splits[0].charAt(0);
      }
      if (splits.length > 1) {
        retVal += splits[splits.length - 1].charAt(0);
      }
      return retVal.toUpperCase();
    }

    return null;
  }

}
