import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { SelectItem } from '../select-item';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'alt-custom-multi-select',
  templateUrl: './custom-multi-select.component.html',
  styleUrls: ['./custom-multi-select.component.css']
})
export class CustomMultiSelectComponent implements OnInit, OnChanges {

  @Input() returnObject: boolean;
  @Input() hiddenToggle: boolean;
  @Input() inputList: any;
  @Input() selectedInputList: any;
  @Output() output: EventEmitter<any> = new EventEmitter();
  
  filteredList : SelectItem[];
  searchString = new FormControl();
  resultList : SelectItem[];
  sliceSize: Number = 20;

  constructor() { 
    this.returnObject = false;
    this.resultList = new Array<any>();
    
  }

  ngOnInit() {
    this.hiddenToggle = false;
    this.searchString.valueChanges.subscribe(changedValue => {

      if(changedValue!=null) {
        this.searchListener(changedValue);
        
      }
    });
    
  }

  ngOnChanges() {
    this.filteredList=this.inputList;
    if(this.selectedInputList != null) {
      this.resultList=this.selectedInputList;
    }
  }

  searchListener(searchString:string) {
    this.filteredList = new Array<SelectItem>();
    for(let item of this.inputList) {
      if(item.label!=null && item.label.toLowerCase().indexOf(searchString.toLowerCase()) != -1) {
        this.filteredList.push(item);
      }
    }
    if( this.filteredList != null && this.filteredList.length > 10 ){
      // this.sliceSize = this.filteredList.length * 2;
    }
  }

  

  selectListener(item:SelectItem, selected:boolean) {
    if(selected) {
      if(this.returnObject) {
        this.resultList.push(item);
      } else {
        this.resultList.push(item.value);
      }
    } else {
      var resultListNew = new Array<any>();
      if(this.returnObject) {
        for(let resultListItem of this.resultList) {
          if(resultListItem != null){
          if(resultListItem.value != item.value) {
            resultListNew.push(resultListItem);
          }
        }
        }
      } else {
        for(let resultListItem of this.resultList) {
          if(resultListItem != null){
          if(resultListItem != item.value) {
            resultListNew.push(resultListItem);
          }
        }
        }
      }
      this.resultList = resultListNew;
    }
    this.output.emit(this.resultList);
  }

  toggle() {
    this.hiddenToggle = !this.hiddenToggle;
    this.searchString.setValue("");
  }

  isChecked(item:SelectItem) {
    for(let result of this.resultList) {
      if(result != undefined && result != null) {
        if(result.value == item.value) {
          return true;  
        }
      }
    }
    return false;
  }
}

