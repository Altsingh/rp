import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material';

@Component({
  selector: 'alt-spinner-button',
  templateUrl: './spinner-button.component.html',
  styleUrls: ['./spinner-button.component.css']
})
export class SpinnerButtonComponent implements OnInit {
  @Input() text: string;
  @Input() spinner: boolean;
  @Input() spinnerdisabled: boolean;
  @Input() done: boolean=false;
  @Output() myEvent = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onClick(button) {
    this.myEvent.emit(button);
  }
}
