import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { I18UtilService } from '../services/i18-util.service';

@Injectable()
export class NotificationPopupService {

  private messages = new Subject<string[]>();
  private messageStore: string[] = [];
  severity: string;
  icon: string;
  timer: any;

  constructor(private i18UtilService: I18UtilService) {
    this.messages.next([]);
    this.severity = "";
    this.icon = "";
  }

  public getMessages(): Observable<string[]> {
    return this.messages.asObservable();
  }

  public clear() {
    this.messageStore = [];
    this.severity = "";
    this.icon = "";
    this.messages.next([]);
  }

  public setMessage(severity: string, message: string, args?: any) {

    if(args){
      this.i18UtilService.get(message, args).subscribe((res: string) => {
        this.severity = severity;
        this.messageStore = [res];
        this.messages.next([res]);
    
        if (this.timer != null) {
          clearTimeout(this.timer);
        }
    
        this.timer = setTimeout(() => {
          this.clear();
          this.timer = null;
        }, 5000);
      });

    }else{
      this.i18UtilService.get(message).subscribe((res: string) => {
        this.severity = severity;
        this.messageStore = [res];
        this.messages.next([res]);
    
        if (this.timer != null) {
          clearTimeout(this.timer);
        }
    
        this.timer = setTimeout(() => {
          this.clear();
          this.timer = null;
        }, 5000);
      });
    } 
  }

  public getSeverity(): string {
    return this.severity;
  }

  public addMessage(severity: string, message: string) {
    if (this.messageStore.indexOf(message) == -1) {
      this.severity = severity;
      this.messageStore.push(message);
      this.messages.next(this.messageStore);

      if (this.timer != null) {
        clearTimeout(this.timer);
      }

      this.timer = setTimeout(() => {
        this.clear();
        this.timer = null;
      }, 5000);
    }
  }

}

export const NotificationSeverity = {
  INFO: "INFO",
  WARNING: "WARNING"
}

