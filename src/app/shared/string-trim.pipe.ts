import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringTrim'
})
export class StringTrimPipe implements PipeTransform {

  transform(value: string, args?: any): string {

    let maxLength = 35;

    if(args!=null){
      maxLength = args;
    }
    let trimed: string = value;
    if (value !== undefined && trimed !== null &&  trimed.length >= maxLength) {
      trimed = trimed.substring(0, maxLength) + '...';
    }
    return trimed;
  }

}
