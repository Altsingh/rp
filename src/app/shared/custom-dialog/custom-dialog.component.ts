import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alt-custom-dialog',
  templateUrl: './custom-dialog.component.html',
  styleUrls: ['./custom-dialog.component.css']
})
export class CustomDialogComponent implements OnInit {

  @Input() hiddenToggle: boolean;
  
  constructor() {  }

  ngOnInit() {
    this.hiddenToggle = false;
  }

  toggle() {
    this.hiddenToggle = !this.hiddenToggle;
  }

}
