import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Component, OnInit, Input } from '@angular/core';
import { CountryMaster } from '../../common/country-master';
import { StateMaster } from '../../common/state-master';
import { DistrictMaster } from '../../common/district-master';
import { CityMaster } from '../../common/city-master';
import { PincodeMaster } from '../../common/pincode-master';
import { LocationDialogService } from './location-dialog.service';
import { SessionService } from '../../session.service';

@Component({
  selector: 'alt-location-dialog',
  templateUrl: './location-dialog.component.html',
  styleUrls: ['./location-dialog.component.css'],
  providers: [LocationDialogService]
})
export class LocationDialogComponent implements OnInit{

  countryListMasterSubscription : any;
  countryList : CountryMaster;

  stateListMasterSubscription : any;
  stateList : StateMaster;

  districtListMasterSubscription : any;
  districtList : DistrictMaster;

  cityListMasterSubscription : any;
  cityList : CityMaster;

  pincodeListMasterSubscription : any;
  pincodeList : PincodeMaster;

  locationInfo : FormGroup;
  selectedPincodeID : Number;
  countryID : any;
  stateID : any;
  districtID : any;
  cityID : any;
  pincode : any;
  count = 0;
  countryloaded = true;
  stateloaded = true;
  districtloaded = true;
  cityloaded = true;
  pincodeloaded = true;

  constructor(public dialogRef: MatDialogRef<LocationDialogComponent>, private locationService:LocationDialogService, private formBuilder: FormBuilder, public sessionService: SessionService) { 
    this.countryloaded = false;
    this.countryListMasterSubscription = this.locationService.getCountryMaster().subscribe(out =>{
        this.countryList = out.responseData
        this.countryloaded = true;
      });
    }

  ngOnInit() {
    this.selectedPincodeID = 0;
    this.locationInfo = this.formBuilder.group({
        countryID:'',
        stateID:'',
        districtID:'',
        cityID:'',
        pincodeID:''
      });

      this.pincode = this.sessionService.dialogData;
      if(this.pincode!=undefined && this.pincode!=null) {
       this.locationService.getSelectedLocationPopulate(this.pincode).subscribe(out =>{
          this.countryID = out.response.countryID;
          this.stateID = out.response.stateID;
          this.districtID = out.response.districtID;
          this.cityID = out.response.cityID;
          this.locationInfo.controls['countryID'].setValue(this.countryID);
        });     
      }
  
      
    this.locationInfo.controls.countryID.valueChanges.subscribe(data => {
      this.stateloaded = false;
      this.stateListMasterSubscription = this.locationService.getStateMaster(this.locationInfo.controls.countryID.value).subscribe(out =>{
        this.stateList = out.responseData
        if(this.stateID != 0){
          this.locationInfo.controls['stateID'].setValue(this.stateID);
        }
        else{
          this.locationInfo.controls['stateID'].setValue(0);  
        }
        this.stateloaded = true;
      } , err => {
        this.stateloaded=true;
      });
      this.locationInfo.controls['stateID'].setValue(0);  
    });

    this.locationInfo.controls.stateID.valueChanges.subscribe(data => {
      this.districtloaded = false;
      this.districtListMasterSubscription = this.locationService.getDistrictMaster(this.locationInfo.controls.stateID.value).subscribe(out =>{
        this.districtList = out.responseData
        if(this.districtID != 0){
          this.locationInfo.controls['districtID'].setValue(this.districtID);
        }
        else{
          this.locationInfo.controls['districtID'].setValue(0);
        }
        this.districtloaded = true;
      }, err => {
        this.districtloaded=true;
      });
      this.locationInfo.controls['districtID'].setValue(0);
    });

    this.locationInfo.controls.districtID.valueChanges.subscribe(data => {
      this.cityloaded = false;
      this.cityListMasterSubscription = this.locationService.getCityMaster(this.locationInfo.controls.districtID.value).subscribe(out =>{
        this.cityList = out.responseData
        if(this.cityID != 0){
          this.locationInfo.controls['cityID'].setValue(this.cityID);
        }
        else{
          this.locationInfo.controls['cityID'].setValue(0);
        }
        this.cityloaded = true;
      }, err => {
        this.cityloaded=true;
      });
      this.locationInfo.controls['cityID'].setValue(0); 
    });


    this.locationInfo.controls.cityID.valueChanges.subscribe(data => {   
      this.pincodeloaded = false;
      this.pincodeListMasterSubscription = this.locationService.getPincodeMaster(this.locationInfo.controls.cityID.value).subscribe(out =>{
        this.pincodeList = out.responseData
        if(this.pincode != null){
          this.locationInfo.controls['pincodeID'].setValue(this.pincode);
        }
        else{
          this.locationInfo.controls['pincodeID'].setValue(0);
        }
        this.pincodeloaded = true;
      }, err => {
        this.pincodeloaded=true;
      });
      this.locationInfo.controls['pincodeID'].setValue(0);
    
    });

    this.locationInfo.controls.pincodeID.valueChanges.subscribe(data => {
      this.dialogRef.componentInstance.selectedPincodeID = this.locationInfo.controls.pincodeID.value;
    });

 
  
    

  }
}
