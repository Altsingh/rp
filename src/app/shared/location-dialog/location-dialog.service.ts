import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { SessionService } from '../../session.service';
import { Http } from '@angular/http';

@Injectable()
export class LocationDialogService {

  selectedOption : String;
  constructor(private http: Http, private sessionService: SessionService) { }

  getCountryMaster() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_countryList/').map(res => {         this.sessionService.check401Response(res.json());         return res.json()});
  }

  getStateMaster(countryID : Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_stateList/' + countryID+"/").map(res => {         this.sessionService.check401Response(res.json());         return res.json()});
  }

  getDistrictMaster(stateID : Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_districtList/' + stateID+"/").map(res => {         this.sessionService.check401Response(res.json());         return res.json()});
  }

  getCityMaster(districtID : Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_cityList/' + districtID+"/").map(res => {         this.sessionService.check401Response(res.json());         return res.json()});
  }

  getPincodeMaster(cityID : Number) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeList/' + cityID+"/").map(res => {         this.sessionService.check401Response(res.json());         return res.json()});
  }

  getSelectedLocationPopulate(pincode : any) {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/fetchLocation/' + pincode+"/").map(res => {         this.sessionService.check401Response(res.json());         return res.json()});
  }

}
