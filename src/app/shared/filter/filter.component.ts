import { ModuleConfigService } from './../../module-config.service';
import { StatusChangeService } from './../../job/job-detail/status-change/status-change.service';
import { Component, OnInit, Output, EventEmitter, OnDestroy, Input, ViewChild, ElementRef } from '@angular/core';
import { FilterService } from './filter.service';
import { NotificationService, NotificationSeverity } from '../../common/notification-bar/notification.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CandidateGenderListService } from '../../candidate/candidate-gender-list.service';
import { CandidateIndustryListService } from '../../candidate/candidate-industry-list.service';
import { CandidateFunctionalAreaListService } from '../../candidate/candidate-functional-area-list.service';
import { CandidateSourceTypeListService } from '../../candidate/candidate-source-type-list.service';
import { SelectItem } from '../../shared/select-item';
import { CustomTreeNode } from 'app/shared/custom-tree/CustomTreeNode.model';
import { CustomTreeComponent } from 'app/shared/custom-tree/custom-tree.component';
import { SessionService } from 'app/session.service';
import { NewJobService } from '../../job/new-job/new-job.service';
import { I18UtilService } from '../services/i18-util.service';

@Component({
  selector: 'alt-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
  providers: [FilterService, CandidateGenderListService, CandidateIndustryListService,
    CandidateFunctionalAreaListService, CandidateSourceTypeListService, StatusChangeService,
     NewJobService]
})
export class FilterComponent implements OnInit, OnDestroy {
  heightStyle: string;
  isToggled: boolean;
  Loader: number = 0;
  isClassVisible: boolean;
  filterDropDown: boolean = true;
  filterDropDown2: boolean = true;
  atsTeams: Array<any>;
  hiringManagers: Array<any>;
  recruiters: Array<any> = [];
  processLeads: Array<any> = [];
  processSpecialists;
  sourcingSpecialists: Array<any> = [];
  offerTeam: Array<any> = [];
  joiningTeam: Array<any> = [];
  vendors: Array<any> = [];
  sources: Array<any> = [];
  stages: Array<any> = [];
  jobHealthList: Array<any> = [];
  joiningLocations: Array<any> = [];
  candidateLocations: Array<any> = [];
  orgUnitHierarchy: any[] = [];
  orgUnitHierarchyMap: Array<any> = [];
  workSiteHierarchy: any[] = [];
  startDate: Date;
  endDate: Date;
  selectItem: [any];
  selectedParams = [];
  visibleSelectedParamsLength = 0;
  filterHidden: boolean = true;
  location: FormGroup;
  hirarchy : FormGroup;
  genderList: any[];
  minExperienceList: any[] = [];
  maxExperienceList: any[] = [];
  genderListSubscription: any;
  industryList: any[];
  industryListSubscription: any;
  functionalAreaList: any[];
  functionalAreaListSubscription: any;
  sourceTypeList: any[] = [];
  sourceTypeListSubscription: any;
  jobStatus: SelectItem[];
  gradeList : SelectItem[];
  postedOn: SelectItem[] = [];
  hiringManagerFilterApplied: boolean;
  recruiterFilterApplied: boolean;
  jobstatusFilterApplied: boolean;
  locationFilterApplied: boolean;
  publishedToFilterApplied: boolean;
  gradeFilterApplied: boolean;
  showPublishedToFilter: boolean = false;
  genderFilterApplied: boolean;
  minExperienceFilterApplied: boolean;
  maxExperienceFilterApplied: boolean;
  industryFilterApplied: boolean;
  functionalAreaFilterApplied: boolean;
  sourceFilterApplied: boolean;
  stageStatusFilterApplied: boolean;
  orgUnitHierarchyFilterApplied: boolean;
  workSiteHierarchyFilterApplied: boolean;
  orgUnitworkSiteFilterEnabled: boolean;
  orgUnitworkSiteFilterLocationEnabled: boolean;
  featureDataMap : Map<any,any>;
  sortByApplied: boolean;
  stageStatusFilterSource: CustomTreeNode;
  isDatePickerEnable: boolean= false;
  systemLevel : boolean = false;
  clearAllSelects = {
    'HIRING_MANAGER' : false,
    'RECRUITER' : false,
    'JOB_STATUS' : false,
    'POSTED_ON' : false,
    'ORG_UNIT' : false,
    'WORKSITE' : false,
    'GENDER' : false,
    'INDUSTRY' : false,
    'FUNCTIONAL_AREA' : false,
    'SOURCE' : false,
    'MIN_EXPERIENCE' : false,
    'MAX_EXPERIENCE' : false,
    'GRADE' : false
  };
  myJobsLabel: string;
  allJobsLabel: string;
  taggedCandidatesLabel: string;
  @Input() viewHeight: number;

  @Output() onSorted = new EventEmitter<string[]>();
  @Output() onFilter = new EventEmitter<any[]>();
  @Output() onDateFilter = new EventEmitter<any>();
  @Input() filterType: string;
  @Input() pageTitle: string;
  @Input() applyRecruiterFilter: boolean;
  currentorder: string;
  
  @ViewChild(CustomTreeComponent)
  STAGESTATUSFILTER: CustomTreeComponent;

  moduleConfig: any;

  constructor(private filterService: FilterService,
    private notificationService: NotificationService,
    private sessionService: SessionService,
    private formBuilder: FormBuilder,
    private candidateGenderListService: CandidateGenderListService,
    private industryListService: CandidateIndustryListService,
    private functionalAreaListService: CandidateFunctionalAreaListService,
    private statusChangeService: StatusChangeService,
    private moduleConfigService: ModuleConfigService,
    private candidateSourceTypeListService: CandidateSourceTypeListService,
    private newJobService : NewJobService,
    private i18UtilService: I18UtilService) {
    this.featureDataMap = this.sessionService.featureDataMap;
    if(this.filterType == 'CREATED_JOB_LIST'){
      this.systemLevel = true;

    }
    this.location = this.formBuilder.group({
      joiningLocation: '',
      candidateLocation:''
    });

    this.hirarchy = this.formBuilder.group({
      orgUnit: '',
      workSite:''
    });

    this.location.controls.joiningLocation.valueChanges.subscribe(data => {
      if( this.location.controls.joiningLocation.value == null || this.location.controls.joiningLocation.value == "" ){
        this.joiningLocations = [];
      }else{
        this.filterService.getJoiningLocationListQuery(this.location.controls.joiningLocation.value, 10).subscribe(out => {
          this.joiningLocations = out.responseData;
        });        
      }      
    });

    this.location.controls.candidateLocation.valueChanges.subscribe(data => {
      this.filterService.getCandidateLocationListQuery(this.location.controls.candidateLocation.value, 10).subscribe(out => {
        this.candidateLocations = out.responseData;
      });
    });

     }
  ngOnInit() {
    this.Loader = 0;
    this.filterHidden = true;
    this.orgUnitHierarchy = [];
    this.workSiteHierarchy = [];
    this.orgUnitHierarchyMap = [];

    this.jobStatus = [new SelectItem('Active', 'OPEN'), new SelectItem('Closed', 'CLOSED'), new SelectItem('Cancelled', 'CANCELLED'), new SelectItem('On Hold', 'ONHOLD')];
    this.statusChangeService.getJobStatusesList((this.filterType == 'CREATED_JOB_LIST' || this.filterType == 'MY_REFERRALS')?true:false).subscribe((response) => {
      if (response && response.responseData) {
        this.jobStatus = [];
        for (let res of response.responseData) {
          this.jobStatus.push(new SelectItem(res.label, res.contentType));
        }
      }
    });
    

    if (this.filterType == 'UNTAGGED_CANDIDATE_LIST' || this.filterType == 'TAGGED_CANDIDATE_LIST' || this.filterType == 'MY_REFERRALS') {
      this.initializeStageStatusFilter();
      this.initGenderList();
      this.initIndustryList();
      this.initFunctionalAreaList();
      this.initSourceTypeList();
      this.initMinExperienceList();
      this.initMaxExperienceList();
    }
    if (this.filterType == 'JOB_LIST' || this.filterType == 'CREATED_JOB_LIST' || this.filterType == 'ERP_JOB_LIST') {
      this.populateHiringManagers();
      if (this.applyRecruiterFilter)
        this.populateAtsTeams();
      //this.AllServices();
      this.jobHealthList = [{ 'label': 'Poor', 'value': 'POOR' }, { 'label': 'Average', 'value': 'AVERAGE' }, { 'label': 'Good', 'value': 'GOOD' }];
   
      this.initPostedOnFilters();
    }
    if(this.filterType == 'ERP_JOB_LIST'){
      this.populateGradeMaster();
    }
    //this.filterService.getJoiningLocationListQuery(null, 10).subscribe(out => this.joiningLocations = out.responseData);

    this.filterService.getCandidateLocationListQuery(null, 10).subscribe(out => this.candidateLocations = out.responseData);

    this.populateOrgUnitAndWorkSiteFilters(); 
    this.i18UtilService.get('job.job_list.myJobs').subscribe((res: string) => {
      this.myJobsLabel = res;
     });
     this.i18UtilService.get('job.job_list.allJobs').subscribe((res: string) => {
      this.allJobsLabel = res;
     });
     this.i18UtilService.get('candidate.list.taggedCandidates').subscribe((res: string) => {
      this.taggedCandidatesLabel = res;
     });
  }

  populateOrgUnitAndWorkSiteFilters(){
    
    this.orgUnitworkSiteFilterLocationEnabled=false;
    if( this.pageTitle == this.myJobsLabel || this.filterType == "ERP_JOB_LIST" ){
      this.orgUnitworkSiteFilterLocationEnabled = true;
    }
    
    if ( this.pageTitle && ( ( this.pageTitle == this.allJobsLabel && this.filterType == 'JOB_LIST' ) || ( this.pageTitle == this.taggedCandidatesLabel && this.filterType == 'TAGGED_CANDIDATE_LIST' ) ) ) {
      const configParameters = ['ORGUNIT_WORKSITEE_FILTERS_ENABLED'];
      this.moduleConfigService.moduleConfigCache['Hiring'] = null;
      Promise.resolve(this.moduleConfigService.getModuleConfig('Hiring', configParameters)).then((moduleConfigJSON) => {
        if (moduleConfigJSON) {
          if (moduleConfigJSON.response) {
            this.moduleConfig = moduleConfigJSON.response;
            if (this.moduleConfig['ORGUNIT_WORKSITEE_FILTERS_ENABLED'] != null && this.moduleConfig['ORGUNIT_WORKSITEE_FILTERS_ENABLED'] && parseInt(this.moduleConfig['ORGUNIT_WORKSITEE_FILTERS_ENABLED'].value) > 0 ) {
              this.orgUnitworkSiteFilterEnabled=true;
              this.orgUnitworkSiteFilterLocationEnabled=false;
              this.filterService.getOrgUnitAndLochirarchy().subscribe(out => {
                this.orgUnitHierarchyMap = [];
                this.orgUnitHierarchy = [];
                this.workSiteHierarchy = [];
                let workSiteHierarchy;
                
                if(out && out.responseData){
                  let i = 0;
                  if( out.responseData.orgUnitHierarchy ){
                    for (let hirarchyKey in out.responseData.orgUnitHierarchy) {
                      this.orgUnitHierarchy.push( { 'label': hirarchyKey, 'value': hirarchyKey } );
                      workSiteHierarchy = [];
                      if( out.responseData.orgUnitHierarchy[hirarchyKey] && out.responseData.orgUnitHierarchy[hirarchyKey].length > 0 ){
                        for (let item of out.responseData.orgUnitHierarchy[hirarchyKey]) {
                          workSiteHierarchy.push( { 'label': item, 'value': item } );
                        }
                      }                      
                      this.orgUnitHierarchyMap[hirarchyKey] = workSiteHierarchy;
                      if( i == 0 ){
                        this.workSiteHierarchy = workSiteHierarchy;
                      }
                      i++;
                    }
                  }
                }

              });
            }else{
              this.orgUnitworkSiteFilterEnabled=false;
              this.orgUnitworkSiteFilterLocationEnabled=true;
            }
          }
        }
      } );
    }

  }

  ngOnDestroy(): void {
    if (this.filterType == 'UNTAGGED_CANDIDATE_LIST' || this.filterType == 'TAGGED_CANDIDATE_LIST') {
      this.genderListSubscription.unsubscribe();
      this.industryListSubscription.unsubscribe();
      this.functionalAreaListSubscription.unsubscribe();
      this.sourceTypeListSubscription.unsubscribe();
    }
  }
  getHeight(event) {
    this.heightStyle = "";
    this.viewHeight = 0;
    if (window.matchMedia(this.rules.phone).matches) {
      this.viewHeight = event - 58;
    }
  }
  rules =
    {
      phone: '(max-width: 767px)',
      tablet: '(min-width: 768px) and (max-width: 1024px)',
      desktop: '(min-width: 1025px)',
    }
  private initGenderList() {
    this.genderListSubscription = this.candidateGenderListService.getData().subscribe(
      response => {
        this.genderList = response.responseData;

      }
    );
  }
  private initIndustryList() {
    this.industryListSubscription = this.industryListService.getData().subscribe(
      response => {
        this.industryList = response.responseData;
      }
    );
  }
  private initMinExperienceList(){
    this.minExperienceList = [];
    for(var i=0;i<=50;i++)
    {
      this.minExperienceList.push({ 'label': ''+i+'', 'value': i });
    }
  }

  private initPostedOnFilters(){
    if(this.featureDataMap.get('PUBLISH_TO_IJP') == 1){
      this.showPublishedToFilter = true;
      this.postedOn.push(new SelectItem('Internal Transfer', 'IJP'));
    }
    if(this.featureDataMap.get('PUBLISH_TO_EMPLOYEE_REFERRAL') == 1){
      this.showPublishedToFilter = true;
      this.postedOn.push(new SelectItem('Employee Referral', 'ER'));
    }
    if(this.featureDataMap.get('PUBLISH_TO_CP') == 1){
      this.showPublishedToFilter = true;
      this.postedOn.push(new SelectItem('Candidate Portal', 'CP'));
    }
    if(this.featureDataMap.get('PUBLISH_TO_JOBBOARD') == 1){
      this.showPublishedToFilter = true;
      this.postedOn.push(new SelectItem('Job Boards', 'JOBBOARD'));
    }
    
    if(this.featureDataMap.get('PUBLISH_TO_VENDOR') == 1){
      this.showPublishedToFilter = true;
      this.postedOn.push(new SelectItem('Vendors', 'VENDOR'));
    }

  }
  private initMaxExperienceList(){
    this.maxExperienceList = [];
    for(var i=0;i<=50;i++)
    {
      this.maxExperienceList.push({ 'label': ''+i+'', 'value': i });
    }
  }
  private initFunctionalAreaList() {
    this.functionalAreaListSubscription = this.functionalAreaListService.getData().subscribe(
      response => {
        this.functionalAreaList = response.responseData;
      }
    );
  }
  private initSourceTypeList() {
    this.sourceTypeListSubscription = this.candidateSourceTypeListService.getData().subscribe(
      response => {
        this.sourceTypeList = response.responseData;
      }
    );
  }
  private toggleFilter() {
    this.filterDropDown = !this.filterDropDown;
    if (!this.filterDropDown) {
      this.filterDropDown2 = true;
    }
  }
  private toggleFilter2() {
    this.filterDropDown2 = !this.filterDropDown2;
    if (!this.filterDropDown2) {
      this.filterDropDown = true;
    }
  }
  private populateHiringManagers() {
    if (this.hiringManagers == null) {
      this.filterService.getHiringManagers().subscribe(
        response => {
          this.hiringManagers = response.responseData;
          this.Loader = 0;
        });
    }
  }

  private populateGradeMaster(){
    this.newJobService.getGradeMaster().subscribe(
      response=>{
        this.gradeList = [];
        for (let res of response.responseData) {
          this.gradeList.push(new SelectItem(res.label, res.label));
        }
      });
  }

  private populateAtsTeams() {
    if (this.atsTeams == null) {
      this.filterService.getTeams().subscribe(
        response => {
          this.atsTeams = response.responseData;
          let rec: Array<any> = [];
          let ps: Array<any> = [];
          let pl: Array<any> = [];
          let ss: Array<any> = [];
          let ven: Array<any> = [];
          for (let member of this.atsTeams) {
            if (member.roleType === 'SOURCING_SPECIALIST' || member.roleType === 'PROCESS_LEAD' || member.roleType === 'PROCESS_SPECIALIST') {
              rec.push({ 'value': member.userID, 'label': member.userName });
            } else if (member.roleType === 'PROCESS_SPECIALIST') {
              ps.push({ 'value': member.userID, 'label': member.userName });
            } else if (member.roleType === 'PROCESS_LEAD') {
              pl.push({ 'value': member.userID, 'label': member.userName });
            } else if (member.roleType === 'SOURCING_SPECIALIST') {
              ss.push({ 'value': member.userID, 'label': member.userName });
            } else if (member.roleType === 'VENDOR') {
              ven.push({ 'value': member.userID, 'label': member.userName });
            }
          }
          this.recruiters = rec;
          this.processSpecialists = ps;
          this.processLeads = pl;
          this.sourcingSpecialists = ss;
          this.vendors = ven;
          this.Loader = 0;
        }
      );
    }
  }

  AllServices() {
    Promise.all([this.populateAtsTeams(), this.populateHiringManagers()])
      .then(() => {
        this.Loader = 0;
      });
  }

  public getMinExperienceList(param:any){
    this.minExperienceList = [];
    for(var i=0;i<=param.value;i++)
    {
      this.minExperienceList.push({'label':''+i+'' , 'value' : i });
    }
  }

  public getMaxExperienceList(param:any){
    this.maxExperienceList = [];
    for(var i=param.value;i<=50;i++)
    {
      this.maxExperienceList.push({'label' :''+i+'','value' : i});
    }
  }
  public chooseFilterParam(param: any, name: string, showApplied?:true, anyKeyValueMap?:any) {
    if( showApplied === undefined ){
      showApplied = true;
    }
    let shouldFireFilter = true;
    //this.viewHeight;
    this.heightStyle = "inherit";
    let alreadySelected = false;
    if (window.matchMedia(this.rules.desktop).matches) {

      if (name == 'HIRING_MANAGER') {
        this.hiringManagerFilterApplied = true;
        this.clearAllSelects.HIRING_MANAGER = false;
      } else if (name == 'RECRUITER') {
        this.recruiterFilterApplied = true;
        this.clearAllSelects.RECRUITER = false;
      } else if (name == 'JOB_STATUS') {
        this.jobstatusFilterApplied = true;
        this.clearAllSelects.JOB_STATUS = false;
      } else if(name == 'GRADE') {
        this.gradeFilterApplied = true;
        this.clearAllSelects.GRADE = false;
      } else if (name == 'POSTED_ON') {
        this.publishedToFilterApplied = true;
        this.clearAllSelects.POSTED_ON = false;
      } else if (name == 'ORG_UNIT') {
        this.orgUnitHierarchyFilterApplied = true;
        this.clearAllSelects.ORG_UNIT = false;

        if( anyKeyValueMap != undefined && anyKeyValueMap != null && anyKeyValueMap ){
          if( anyKeyValueMap.hasOwnProperty(param.value) ){
            let oldWorkSiteHierarchy = this.workSiteHierarchy;
            this.workSiteHierarchy = [];
            this.workSiteHierarchy = anyKeyValueMap[param.value];
            if( anyKeyValueMap[param.value].length > 0 ){
              shouldFireFilter = false;
            }else{
              for( let itm of this.selectedParams ){
                if( itm.name == "WORKSITE" ){
                  let index = this.selectedParams.indexOf(itm);
                  this.selectedParams.splice(index, 1);
                }                
              }
            }

            if( oldWorkSiteHierarchy.length > 0 && this.workSiteHierarchy.length > 0 && this.workSiteHierarchy != oldWorkSiteHierarchy ){
              shouldFireFilter = true;
              for( let itm of this.selectedParams ){
                if( itm.name == "WORKSITE" ){
                  let index = this.selectedParams.indexOf(itm);
                  this.selectedParams.splice(index, 1);
                }
              }
              this.selectedParams.push({ 'name': 'WORKSITE', 'value': this.workSiteHierarchy[0], 'isVisible':false });
            }

          }
        }

      }else if (name == 'WORKSITE') {
        this.workSiteHierarchyFilterApplied = true;   
        this.clearAllSelects.WORKSITE = false;      
      } else if (name == 'LOCATION') {

        if( param.event != undefined && param.event && !param.event.source.selected ){
          return;
        }
         this.locationFilterApplied = true;
      } else if (name == 'GENDER') {
        this.genderFilterApplied = true;
        this.clearAllSelects.GENDER = false;
      } else if (name == 'INDUSTRY') {
        this.industryFilterApplied = true;
        this.clearAllSelects.INDUSTRY = false;
      } else if (name == 'FUNCTIONAL_AREA') {
        this.functionalAreaFilterApplied = true;
        this.clearAllSelects.FUNCTIONAL_AREA = false;
      } else if (name == 'SOURCE') {
        this.sourceFilterApplied = true;
        this.clearAllSelects.SOURCE = false;
      } else if(name == 'MIN_EXPERIENCE'){
        this.minExperienceFilterApplied = true;
        this.clearAllSelects.MIN_EXPERIENCE = false;
      }else if(name == 'MAX_EXPERIENCE'){
        this.maxExperienceFilterApplied = true;
        this.clearAllSelects.MAX_EXPERIENCE = false;
      }else if (name == 'STAGE/STATUS') {
        let allUnchecked = true;
        
        if (param.children != undefined && param.children != null) {
          for (let childOne of param.children) {
            if(childOne.selected == true) {
              allUnchecked = false;
            }
          }
        }
        if(allUnchecked == true) {
          this.removeFilterParam(param);
          return;
        }
        this.stageStatusFilterApplied = true;
      }

    }
    for (let selectedParam of this.selectedParams) {
      if (selectedParam.name == name && window.matchMedia(this.rules.desktop).matches) {
        alreadySelected = true;
        selectedParam.value = param;
        break;
      }
    }
    if (!alreadySelected) {
      this.selectedParams.push({ 'name': name, 'value': param, 'isVisible':showApplied });
    }
    this.recalculateSelectedLength();
    if( shouldFireFilter ){
      this.filter(this.selectedParams);
    }
  }

  recalculateSelectedLength( ){
    this.visibleSelectedParamsLength = 0;
    for( let item of this.selectedParams ){
      if( item && item.isVisible ){
        this.visibleSelectedParamsLength++;
      }
    }
  }


  public removeFilterParam(param: any) {

    if (param.name == 'HIRING_MANAGER') {
      this.hiringManagerFilterApplied = false;
      this.clearAllSelects.HIRING_MANAGER = true;
    } else if (param.name == 'RECRUITER') {
      this.recruiterFilterApplied = false;
      this.clearAllSelects.RECRUITER = true;
    } else if (param.name == 'JOB_STATUS') {
      this.jobstatusFilterApplied = false;
      this.clearAllSelects.JOB_STATUS = true;
    } else if( param.name == 'GRADE'){
      this.gradeFilterApplied = false;
      this.clearAllSelects.GRADE = true;
    } else if (param.name == 'POSTED_ON') {
      this.publishedToFilterApplied = false;
      this.clearAllSelects.POSTED_ON = true;
    } else if (param.name == 'ORG_UNIT') {
      this.orgUnitHierarchyFilterApplied = false;
      this.clearAllSelects.ORG_UNIT = true;
    } else if (param.name == 'WORKSITE') {
      this.workSiteHierarchyFilterApplied = false;
      this.clearAllSelects.WORKSITE = true; 
    } else if (param.name == 'LOCATION') {
      this.locationFilterApplied = false;
      this.location.controls['joiningLocation'].setValue(null);
      this.location.controls['candidateLocation'].setValue(null);
    } else if (param.name == 'GENDER') {
      this.genderFilterApplied = false;
      this.clearAllSelects.GENDER = true;
    } else if (param.name == 'INDUSTRY') {
      this.industryFilterApplied = false;
      this.clearAllSelects.INDUSTRY = true;
    } else if (param.name == 'FUNCTIONAL_AREA') {
     this.functionalAreaFilterApplied = false;
     this.clearAllSelects.FUNCTIONAL_AREA = true;
    } else if (param.name == 'SOURCE') {
      this.sourceFilterApplied = false;
      this.clearAllSelects.SOURCE = true;
    } else if(param.name == 'MIN_EXPERIENCE'){
      this.minExperienceFilterApplied = false;
      this.clearAllSelects.MIN_EXPERIENCE = true;
    } else if(param.name == 'MAX_EXPERIENCE'){
      this.maxExperienceFilterApplied = false;
      this.clearAllSelects.MAX_EXPERIENCE = true;
    } else if (param.name == 'STAGE/STATUS') {
      this.stageStatusFilterApplied = false;
      this.STAGESTATUSFILTER.resetFilters();
    }
    let index = this.selectedParams.indexOf(param);
    this.selectedParams.splice(index, 1);
    this.recalculateSelectedLength();
    this.filter(this.selectedParams);
  }

  public clearFilter() {

     for(let filter in this.clearAllSelects){
      this.clearAllSelects[filter] = true;
     }

    this.hiringManagerFilterApplied = false;
    this.recruiterFilterApplied = false;
    this.jobstatusFilterApplied = false;
    this.publishedToFilterApplied = false;
    this.gradeFilterApplied =  false;
    this.locationFilterApplied = false;
    this.genderFilterApplied = false;
    this.industryFilterApplied = false;
    this.functionalAreaFilterApplied = false;
    this.sourceFilterApplied = false;
    this.stageStatusFilterApplied = false;
    this.minExperienceFilterApplied = false;
    this.maxExperienceFilterApplied = false;
    this.orgUnitHierarchyFilterApplied = false;
    this.workSiteHierarchyFilterApplied = false;
    
    this.selectedParams = this.selectedParams.filter(function(value){
      return ( !value.isVisible ) ? value : null;
    });

    this.location.controls['joiningLocation'].setValue(null);
    this.location.controls['candidateLocation'].setValue(null);
    this.recalculateSelectedLength();
    this.filter(this.selectedParams);
    this.initMinExperienceList();
    this.initMaxExperienceList();
    this.initPostedOnFilters();
    if(this.STAGESTATUSFILTER){
      this.STAGESTATUSFILTER.resetFilters();
    }    
  }
  public clearExperienceFilter()
  {

  }

  public sort(sortParam: string, order: string) {
    this.currentorder = sortParam + order;
    this.sortByApplied = true;
    this.startDate = null;
    this.endDate = null;
    if(sortParam === 'createdDate' || sortParam === 'modifiedDate'){
      this.isDatePickerEnable=true;
    }else{
      this.isDatePickerEnable =false;
    }
    this.onSorted.emit([sortParam, order]);
  }

  public filter(filterParams: any[]) {
    this.onFilter.emit(filterParams);
  }

  selectStartDate(startDate: Date) {
    this.startDate = startDate;
    if (this.endDate != null && startDate != null && startDate > this.endDate) {
      this.notificationService.clear();
      this.i18UtilService.get('common.warning.endDateStartDate').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
    } else {
      this.onDateFilter.emit({ 'name': 'start_date', 'value': startDate });
    }
  }

  selectEndDate(endDate: Date) {
    this.endDate = endDate;
    if (this.startDate != null && endDate != null && endDate < this.startDate) {
      this.notificationService.clear();
      this.i18UtilService.get('common.warning.statrtDateEnddate').subscribe((res: string) => {
        this.notificationService.addMessage(NotificationSeverity.WARNING, res);
      });
    } else {
      this.onDateFilter.emit({ 'name': 'end_date', 'value': endDate });
    }
  }

  initializeStageStatusFilter() {
    this.filterService.getWorkflowStageStatusFilter().subscribe(response => {
      if (response != null && response.messageCode != null && response.messageCode.code === 'EC200')
        this.stageStatusFilterSource = response.response;
      else
        this.stageStatusFilterSource = undefined;
    });
  }
  toggleClick() {
    this.isToggled = !this.isToggled;
  }
  toggleClickClosed() {
    this.isToggled = false;
  }

}
