import { Injectable } from '@angular/core';
import { Http } from '@angular/http/http';
import { SessionService } from '../../session.service';

@Injectable()
export class FilterServiceService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getTeams() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/teams/' + this.sessionService.organizationID)
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()});
  }

}
