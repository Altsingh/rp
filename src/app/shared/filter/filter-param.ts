export class FilterParam {
    hiringManagerID: number;
    recruiterID: number;
    processLeadID: number;
    offerTeamID: number;
    joiningTeamID: number;
    vendorID: number;
    sourceID: number;
    stageStatusID: number;
    jobStatus: string;
    jobHealth: string;
    locationID: number;
    publishedTo: string;
}
