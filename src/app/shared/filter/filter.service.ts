import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SessionService } from '../../session.service';

@Injectable()
export class FilterService {

  constructor(private http: Http, private sessionService: SessionService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }

  getHiringManagers() {

    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/usersByRoleType/HIRING_MANAGER/')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }
  getTeams() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/common/atsteams/' + this.sessionService.organizationID + "/")
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

  getPincodeMaster(searchString: String, count: Number) {
    if (searchString == null) {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + "/").map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
    } else {
      return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/masterdata_pincodeListQuery/' + count + '/' + searchString + "/").map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
    }
  }

  getJoiningLocationListQuery(searchString: String, count: Number) {
    if (searchString == null || searchString == "") {
      searchString = "NA";
    }
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/joiningLocationListQuery/' + count + '/' + searchString.trim() + "/").map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getCandidateLocationListQuery(searchString: String, count: Number) {
    if (searchString == null) {
      searchString = "NA";
    }
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/candidate/candidateLocationListQuery/' + count + '/' + searchString + "/").map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }
  
  getOrgUnitAndLochirarchy() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/jobs/getOrgAndLocHir/').map(res => {
      this.sessionService.check401Response(res.json());
      return res.json()
    });
  }

  getWorkflowStageStatusFilter() {
    return this.http.get(this.sessionService.orgUrl + '/rest/altone/masters/workflowStageStatusFilter')
      .map(res => {
        this.sessionService.check401Response(res.json());
        return res.json()
      });
  }

}
