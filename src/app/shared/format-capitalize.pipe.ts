import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatCapitalize'
})
export class FormatCapitalizePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let result: string = '';

    if (value != null) {
      if (typeof value == 'string') {
        let split: string[] = value.split(" ");

        for (let x = 0; x < split.length; x++) {
          result += split[x].charAt(0).toUpperCase();
          result += split[x].substring(1).toLowerCase();
          result += " ";
        }
      }
    }

    return result;
  }

}
