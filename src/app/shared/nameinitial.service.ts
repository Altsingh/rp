import { Injectable } from '@angular/core';

@Injectable()
export class NameinitialService {
  randomcolor: string[]=[];
  constructor() { 
    this.randomize();
  }

  randomize(){
    for(let x=0;x<1000;x++){
      this.randomcolor.push(this.getRandomColor(x));
    }
  }
   getRandomColor(index) {
    let letters = '0123456789ABCDEF'.split('');
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}

