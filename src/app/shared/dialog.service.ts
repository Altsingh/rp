import { map } from 'rxjs/operator/map';
import { Observable } from 'rxjs/';
import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { SessionService } from '../session.service';
@Injectable()
export class DialogService {

  constructor(private dialog: MatDialog, private sessionService: SessionService) {

  }

  public open(component, config) {
    let dialogRef: MatDialogRef<any>;
    dialogRef = this.dialog.open(component, config);
    document.getElementsByTagName('body')[0].className = "overflow-hidden";

    return dialogRef.afterClosed().map((response) => {
      document.getElementsByTagName('body')[0].className = "overflow-scroll";
      return response;
    });
  }

  public getDialogData() {
    return this.sessionService.dialogData
  }

  public setDialogData(data) {
    this.sessionService.dialogData = data;
  }
  public getDialogData1() {
    return this.sessionService.dialogData1
  }

  public setDialogData1(data) {
    this.sessionService.dialogData1 = data;
  }
  public getDialogData2() {
    return this.sessionService.dialogData2;
  }

  public setDialogData2(data) {
    this.sessionService.dialogData2 = data;
  }
}
