import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lowercasepipe'
})
export class LowercasepipePipe implements PipeTransform {

  transform(value: string): string {
    if (!value)return value;
    return value.toLocaleLowerCase();
  }
}
