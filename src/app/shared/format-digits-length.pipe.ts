import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDigitsLength'
})
export class FormatDigitsLengthPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    
    if (typeof value == 'number') {
      let retVal = value + "";

      let x = 0;

      if (args != null) {
        x = args;
      }

      let z = 1;

      for (let y = 1; y < x; y++) {
        z = z * 10;
      }

      while (value < z && z>1) {
        retVal = "0" + retVal;
        z /= 10;
      }

      return retVal;
    } else {
      return value;
    }
  }
}
