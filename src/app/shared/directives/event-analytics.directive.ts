import { Directive, ElementRef, Output, EventEmitter, HostListener, Input } from '@angular/core';
import { EventAnalyticsService } from '../../event-analytics.service';
import { Router } from '@angular/router';

declare var ga: Function;

@Directive({
  selector: '[gaevent]'
})
export class EventAnalytics {

  @Output("gaevent")
  public flag = new EventEmitter<boolean>();
  
  @Input("gaevent") gaevent;
  @Input("gaRouterLink") gaRouterLink;
  @Input("queryParams") queryParams;
  
  constructor(private _el: ElementRef, private eventAnalyticsService: EventAnalyticsService,
    private router: Router) { }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement: Element) {
    const clickedInside = this._el.nativeElement.contains(targetElement);
    if (clickedInside) {
      
      let event = targetElement.parentElement.getAttribute("gaevent");
      if( event == null && this.gaevent !== undefined && this.gaevent ){
        event = this.gaevent;
      }

      if( event != null && event.indexOf("#") > -1 ){
        let currentTime  = new Date().getTime();
        if( (currentTime - this.eventAnalyticsService.lastGEventTime) > 60 ){
                  
          let parts = event.split("#");
          ga('send', {
            hitType: 'event',
            eventCategory: parts[0],
            eventLabel: parts[1],
            eventAction: (parts[2] === undefined)?'Click':parts[2]
          });
        }
        
        this.eventAnalyticsService.lastGEventTime = currentTime;

        if( this.gaRouterLink && this.queryParams ){
          this.router.navigate(this.gaRouterLink, { queryParams: this.queryParams });
        }else if( this.gaRouterLink ){
          this.router.navigate(this.gaRouterLink);
        }

      }

    }
  }

}
