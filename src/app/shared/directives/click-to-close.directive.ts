import { Directive, ElementRef, Output, EventEmitter, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[clickToClose]'
})
export class ClickToClose {
  @Input("srcTag") tag: Element;
  @Output("clickToClose")
  public flag = new EventEmitter<boolean>();

  constructor(private _el: ElementRef) { }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement: Element) {
    const clickedInside = this._el.nativeElement.contains(targetElement);
    if (!clickedInside) {
      if( targetElement.hasAttribute("ClickToCloseAllow") ){
        return targetElement.getAttribute("ClickToCloseAllow");
      }
      this.flag.emit(true);
    }
  }

}
