import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CustomValidationComponent } from './custom-validation/custom-validation.component';
import { CustomDialogComponent } from './custom-dialog/custom-dialog.component';
import { CustomMultiSelectComponent } from './custom-multi-select/custom-multi-select.component';
import { CustomSingleSelectComponent } from './custom-single-select/custom-single-select.component';
import { StringTrimPipe } from './string-trim.pipe';
import { LoaderIndicatorComponent } from './loader-indicator/loader-indicator.component';
import { FilterComponent } from './filter/filter.component';
import { DataTableModule } from './alt-data-table/DataTableModule';
import {AltSelectModule} from './alt-select/alt-select.module';
import { FilterLoaderComponent } from './filter/filter-loader/filter-loader.component';
import { AltDatepickerModule } from './alt-datepicker/index';
import { AltNativeDateModule } from './alt-datepicker/core/datetime/index';
import { FormatCandidatecodeParamPipe } from './format-candidatecode-param.pipe';
import { FormatJobcodeParamPipe } from './format-jobcode-param.pipe';
import { ProfilecompletionComponent } from './profilecompletion/profilecompletion.component';
import { MatAutocompleteModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatRadioModule, MatTooltipModule, MatDialogModule } from '@angular/material';
import { FormatCapitalizePipe } from './format-capitalize.pipe';
import { FormatDigitsLengthPipe } from './format-digits-length.pipe';
import { ClickToClose } from './directives/click-to-close.directive';
import { InitialsPipe } from './initials.pipe';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { SpinnerButtonComponent } from './spinner-button/spinner-button.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { LoaderComponent } from './loader/loader.component';
import { LowercasepipePipe } from './lowercasepipe.pipe';
import { CustomEmailComponent } from './custom-email/custom-email.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NotificationBarPopupComponent } from 'app/shared/notification-bar-popup/notification-bar-popup.component';
import { CustomTreeComponent } from './custom-tree/custom-tree.component';
import { EventAnalytics } from './directives/event-analytics.directive';
import { CustomHttpinterceptorService } from './services/custom-httpinterceptor.service';
import { CustomHttpService } from './services/custom-http-service.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { I18UtilService } from './services/i18-util.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
import { PrintingService } from './services/printing/printing.service';
import { CustomValidatorService } from './services/custom-validator.service';
import { DocumentUploadUtilityService } from './services/document-upload-utility/document-upload-utility.service';
import { WarningDialogComponent } from './warning-dialog/warning-dialog.component';

@NgModule({
  // tslint:disable-next-line:max-line-length 
  imports: [CommonModule, NgSlimScrollModule , MatDialogModule,
     ReactiveFormsModule, AltSelectModule, AltDatepickerModule,
      AltNativeDateModule, FormsModule, MatAutocompleteModule,
       MatInputModule, MatProgressSpinnerModule, MatSelectModule, 
       MatRadioModule, MatTooltipModule,FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
    }
  })
        
  ],
  declarations: [CustomValidationComponent, CustomDialogComponent, CustomMultiSelectComponent, CustomSingleSelectComponent, StringTrimPipe, FilterComponent,LoaderIndicatorComponent, FilterLoaderComponent, FormatCandidatecodeParamPipe,FormatCandidatecodeParamPipe,ProfilecompletionComponent, FormatJobcodeParamPipe, FormatCapitalizePipe, ClickToClose, EventAnalytics, FormatDigitsLengthPipe, InitialsPipe, SpinnerButtonComponent, PaginatorComponent,LoaderComponent, LowercasepipePipe,CustomEmailComponent, NotificationBarPopupComponent, CustomTreeComponent, WarningDialogComponent ],
  exports: [CustomValidationComponent, CustomDialogComponent, CustomMultiSelectComponent, CustomSingleSelectComponent, StringTrimPipe, DataTableModule, FilterComponent,LoaderIndicatorComponent, AltSelectModule, AltDatepickerModule, FormatCandidatecodeParamPipe,ProfilecompletionComponent,FormatJobcodeParamPipe,FormatCapitalizePipe, ClickToClose, EventAnalytics, FormatDigitsLengthPipe, InitialsPipe,SpinnerButtonComponent, PaginatorComponent,LoaderComponent,LowercasepipePipe,CustomEmailComponent, NotificationBarPopupComponent,WarningDialogComponent],
  entryComponents: [CustomEmailComponent,WarningDialogComponent],
  
})

export class SharedModule {
 static forRoot():ModuleWithProviders{
    return {
      ngModule: SharedModule,
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: CustomHttpinterceptorService, multi: true } ,CustomHttpService,I18UtilService,PrintingService,CustomValidatorService,DocumentUploadUtilityService
      ]
    };
  }
 }
