import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'alt-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {


  @Input() pageState: PageState;
  @Input() pageLoading: boolean = false;
  @Input() rowsPerPageTemplate: string;

  @Output() pageChange: EventEmitter<any> = new EventEmitter();

  pageNumbers: number[] = [];

  pageTemplateOpen: boolean = false;



  constructor() {
    if (this.rowsPerPageTemplate != null) {
      this.pageState.rowsPerPageTemplate = this.rowsPerPageTemplate;
    }
  }

  ngOnInit() {
    if (this.rowsPerPageTemplate != null) {
      this.pageState.rowsPerPageTemplate = this.rowsPerPageTemplate;
    }
  }
  toggleClose() {
    this.pageTemplateOpen = false;
    
  }
  previousPage() {
    if (this.pageLoading) {
      return;
    }
    if (this.isFirst()) {
      return;
    } else {
      this.pageState.currentPageNumber = this.pageState.currentPageNumber - 1;

      this.pageChange.emit({ pageNumber: this.pageState.currentPageNumber, pageSize: this.pageState.pageSize });
    }
  }

  nextPage() {
    if (this.pageLoading) {
      return;
    }
    if (this.isLast()) {
      return;
    } else {
      this.pageState.currentPageNumber = this.pageState.currentPageNumber + 1;

      this.pageChange.emit({ pageNumber: this.pageState.currentPageNumber, pageSize: this.pageState.pageSize });
    }
  }

  getPageNumber(pageNumber: number) {
    if (this.pageState.currentPageNumber == pageNumber) {
      return;
    } else {
      window.scroll(0, 0);
      this.pageState.currentPageNumber = pageNumber;
      this.pageChange.emit({ pageNumber: this.pageState.currentPageNumber, pageSize: this.pageState.pageSize });
    }
  }

  togglePageTemplate() {
    this.pageTemplateOpen = !this.pageTemplateOpen;
  }

  getPageNumbers(): number[] {
    let pageNumbers: number[] = [];

    let count = 0;

    for (let x = this.pageState.currentPageNumber - 2; x < this.pageState.currentPageNumber; x++) {
      if (x > 0) {
        pageNumbers.push(x);
        count++;
      }
    }

    pageNumbers.push(this.pageState.currentPageNumber);
    count++;

    let maxPages = this.getMaxPages();

    for (let x = this.pageState.currentPageNumber + 1; (x < this.pageState.currentPageNumber + 3 || count < 5) && x <= maxPages; x++) {
      pageNumbers.push(x);
      count++;
    }


    return pageNumbers;
  }

  setPageSize(pageSize) {

    this.pageTemplateOpen = false;

    if (this.pageLoading) {
      return;
    }

    this.pageState.pageSize = pageSize;



    let maxPages = this.getMaxPages();

    if (this.pageState.currentPageNumber > maxPages) {
      this.pageState.currentPageNumber = maxPages;
    }

    this.pageChange.emit({ pageNumber: this.pageState.currentPageNumber, pageSize: this.pageState.pageSize });
  }

  setFirst() {
    if (this.pageLoading) {
      return;
    }
    this.pageState.currentPageNumber = 1;
    this.pageChange.emit({ pageNumber: this.pageState.currentPageNumber, pageSize: this.pageState.pageSize });
  }

  getMaxPages(): number {
    let maxPages = this.pageState.totalRows / this.pageState.pageSize;
    let pageCountFloored = Math.floor(this.pageState.totalRows / this.pageState.pageSize);

    if (maxPages > pageCountFloored) {
      maxPages = pageCountFloored + 1;
    }

    return maxPages;
  }

  isFirst(): boolean {
    return this.pageState.currentPageNumber == 1;
  }

  setLast() {
    if (this.pageLoading) {
      return;
    }
    if (this.isLast()) {
      return;
    }

    let maxPages = this.getMaxPages();

    this.pageState.currentPageNumber = maxPages;
    this.pageChange.emit({ pageNumber: this.pageState.currentPageNumber, pageSize: this.pageState.pageSize });
  }

  isLast(): boolean {
    let maxPages = this.getMaxPages();

    return this.pageState.currentPageNumber == maxPages;
  }

  convertInt(str) {
    return parseInt(str);
  }
}

export class PageState {

  totalRows: number;
  pageSize: number;
  currentPageNumber: number = 1;
  lessRows: boolean = false;
  rowsPerPageTemplate: string = "15,30,45";

  setRowsPerPageTemplate(template: string) {
    this.rowsPerPageTemplate = template;
  }
}