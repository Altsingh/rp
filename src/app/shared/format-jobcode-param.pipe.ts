import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatJobcodeParam'
})
export class FormatJobcodeParamPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (!value) { return value; }

    value = value.replace(/\//g, '_');
    value = value.replace(/\&/g, '@');

    return value;
  }

}
