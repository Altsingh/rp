export class SelectItem {
    label: string;
    value: any;
    comment: String;
    altid: any;
    code: string;

    constructor(label: string, value: any) {
        this.label = label;
        this.value = value;
    }
}
