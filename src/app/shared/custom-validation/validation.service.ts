import { isNullOrUndefined } from "util";

export class ValidationService {
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'Required',
      'invalidCreditCard': 'Is invalid credit card number',
      'invalidEmailAddress': 'Invalid email address',
      'invalidPassword': 'Invalid password.',
      'minlength': `Minimum length ${validatorValue.requiredLength}`,
      'invalidMobile' :'Invalid Mobile number',
      'invalidThaiID' :'Invalid International ID',
      'invalidPanNo':'Please enter valid pan no. in the format 5char/4Numeric/1char',
      'invalidPassport':'Please enter valid passport no. in the format 1char/7Numeric',
      'invalidAdhar':'Please enter valid aadhaarNumber in the format 12Numeric',
      'NumericWithDecimal':'Please enter numeric value. In the format 0-9.09 or 0-9',
      'NumberOnly':'Please enter numbers only. In the format 0-9'
  };
    return config[validatorName];
  }

  static creditCardValidator(control) {
    // Visa, MasterCard, American Express, Diners Club, Discover, JCB
    if (control.value){
    if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
      return null;
    } else {
      return { 'invalidCreditCard': true };
    }
  }
  }
  static emailValidator(control) {
    if (control.value){
    // RFC 2822 compliant regex
    let matches = control.value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
    if (matches != null && matches.length > 0 && matches[0] === control.value) {
      return null;
    } else {
      return { 'invalidEmailAddress': true };
    }
  }
  }
  static passwordValidator(control) {
    if (control.value) {
      if (control.value.match(/((?=.*\d)(?=.*[a-zA-Z])(?=.*[`!~@#$%^&*]).{6,20})/)) {
        if(control.value.indexOf(' ') >= 0){
          return { 'invalidPassword': true };
        }
        return null;
      }  
      else {
        return { 'invalidPassword': true };
      }
    }
  }
  static MobileValidator(control) {
    if (control.value){
      if (control.value.match(/^[0-9+\-]{10,14}$/)) {
      return null;
    } else {
      return { 'invalidMobile': true };
    }
    }
    
  }
  static PanNoValidator(control) {
    if (control.value){
    if (control.value.match(/[a-zA-Z]{5}\d{4}[a-zA-Z]{1}/)) {
      return null;
    } else {
      return { 'invalidPanNo': true };
    }
    }
  }
  //
  static PassportValidator(control) {
    if (control.value){
    if (control.value.match(/[a-zA-Z]{1}[0-9]{7}/)) {
      return null;
    } else {
      return { 'invalidPassport': true };
    }
  }
  }
  static AdharCardValidator(control) {
    if (control.value){
    if (control.value.match(/\d{12}/)) {
      return null;
    } else {
      return { 'invalidAdhar': true };
    }
  }
}
static NumericWithDecimal(control) {
    if (control.value){
    if (control.value.match(/^(?:[0-9]+(?:\.[0-9]{0,2})?)?$/)) {
      return null;
    } else {
      return { 'NumericWithDecimal': true };
    }
  }
}
static NumberOnly(control) {
    if (control.value){
    if (control.value.match(/^[0-9]*$/)) {
      return null;
    } else {
      return { 'NumberOnly': true };
    }
  }
}

static NumberOnly2(control) {
  if (control.value){
  if (control.value.toString().match(/^[0-9]*$/)) {
    return null;
  } else {
    return { 'NumberOnly': true };
  }
}
}

  static isRequiredAndNull(control) {
    if (control.value == null || control.value == '' || (typeof control.value == 'string' && control.value.trim() == '')) {
      return {'required' : true};
    }
    return null;
  }

  static ThaiIDValidator(control) {
    let id : string = control.value;
    if(!isNullOrUndefined(control.value)){
      id = id.replace(/-/g,'');
      id = id.replace(/ /g,'');
      let sum = 0;
      if (id.length != 13) {
        return { 'invalidThaiID': true }; 
      }
      for (let i = 0; i < 12; i++) {
        sum += parseInt(id.charAt(i)) * (13 - i);
      }
      if ((11 - sum % 11) % 10 != parseInt(id.charAt(12))){
        return { 'invalidThaiID': true }; 
      }
    }
    return null;
  }
}