import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from './validation.service';

@Component({
  selector: 'control-message',
  templateUrl: './custom-validation.component.html',
  styleUrls: ['./custom-validation.component.css']
})
export class CustomValidationComponent {
  @Input() public control: FormControl;
  @Input() public fieldLabel: string;
  @Input() public required: boolean = false;
  
  constructor() { }

  get errorMessage() {
    if (this.control) {
      for (let propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && this.control.dirty && this.control.value) {
          return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
        }
      }
      if(this.required && (this.control.value==null || this.control.value=='')){
        return this.fieldLabel + " is required."
      }
      return null;
    }
  }
}
