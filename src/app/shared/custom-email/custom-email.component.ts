import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { SessionService } from "../../session.service";
import { CustomEmailService } from './custom-email.service';
import { MAT_DIALOG_DATA } from "@angular/material";
import { MessageCode } from 'app/common/message-code';
import { NotificationPopupService, NotificationSeverity } from 'app/shared/notification-bar-popup/notification-popup.service';

@Component({
  selector: 'alt-custom-email',
  templateUrl: './custom-email.component.html',
  styleUrls: ['./custom-email.component.css'],
  providers: [CustomEmailService, NotificationPopupService]
})
export class CustomEmailComponent implements OnInit {
  isProcessing: boolean = false;
  templates: any[];
  attachedDocuments: Attachdocuments;
  total_attachment_size = 0;
  toggleOpen: boolean[] = [];
  public options: Object;
  froalakey: any;
  fileinProgress: boolean = false;

  constructor(
    private sessionService: SessionService,
    private notificationService: NotificationPopupService,
    private customEmailService: CustomEmailService,
    public dialogRef: MatDialogRef<CustomEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (window.location.hostname.includes(".peoplestrong.com")) {
      this.froalakey = 'aH3H3B8A8bA4B3E3C1I3H2C2C6B3E2uzncH-7bjcef1G-10zrB1twt=='
    } else if (window.location.hostname.includes(".peoplestrongalt.com")) {
      this.froalakey = 'yC5E5B4E4jC10D7A5A5B2A3E4E2B2B5B-16rfkujbF-7A-21xdbjocdI1qA-16y==';
    }
    this.options = {
      key: this.froalakey
    }
    this.templates = data;

    let index: number = 1;
    for (let template of this.templates) {
      template.sequence = index++;
      template.attachdocuments = null;
      template.totalAttachedDocuments = 0;
    }
  }

  ngOnInit() {

  }

  toggleDiv(i: number) {
    this.toggleOpen[i] = !this.toggleOpen[i];
  }


  delete(item) {
    let templatesAfterRemoval = [];
    let index: number = 1;
    for (let template of this.templates) {
      if (template.sequence != item.sequence) {
        template.sequence = index++;
        templatesAfterRemoval.push(template);
      }
    }
    this.templates = templatesAfterRemoval;
  }

  confirmDelete(item) {
    for (let template of this.templates) {
      if (template.sequence == item.sequence) {
        template.confirmMessage = 'DELETE';
      }
      else {
        template.confirmMessage = null;
      }
    }
    console.log(this.templates)
  }

  send() {
    this.isProcessing = true;
    let valid: boolean = true;
    valid = this.validate();
    if (valid) {
      let fileUploadError = "Could not upload Document. Please try again.";
      this.customEmailService.sendHiringEmails(this.templates).subscribe(
        (result) => {
          let messageTO: MessageCode = result.messageCode;
          let message: string = '';
          if (messageTO != null) {
            if (messageTO.code != null && messageTO.code === 'EC200') {
              this.dialogRef.close('SUCCESS');
            } else if (messageTO.code === 'EC201') {
              fileUploadError = messageTO.message.toString();
              this.notificationService.addMessage(NotificationSeverity.WARNING, fileUploadError);
            } else {
              this.dialogRef.close('FAILURE');
            }
          }
          this.isProcessing = false;
        }, (onError) => {
          this.isProcessing = false;
        }, () => {
          this.isProcessing = false;
        }
      );
    } else {
      this.isProcessing = false;
    }
  }

  validate(): boolean {
    this.notificationService.clear();
    let index: number = 0;
    let valid: boolean = true;
    let sequence = '';
    for (let template of this.templates) {
      index++;
      if (index < 10) {
        sequence = '0' + index;
      } else {
        sequence = '' + index;
      }
      if (template.mailTO == null || template.mailTO.trim().length == 0) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'Mail ' + sequence + ' : Please enter at least one Email-ID in TO');
        valid = false
      } else {
        template.mailTO = template.mailTO.replace(new RegExp(' ', 'g'), '');
        if (!this.emailValidator(template.mailTO)) {
          this.notificationService.addMessage(NotificationSeverity.WARNING, 'Mail ' + sequence + ' : Please enter a valid Email-ID in TO');
          valid = false
        }
      }
      if (template.mailCC != null && template.mailCC.trim().length > 0) {
        template.mailCC = template.mailCC.replace(new RegExp(' ', 'g'), '');
        if (!this.emailValidator(template.mailCC)) {
          this.notificationService.addMessage(NotificationSeverity.WARNING, 'Mail ' + sequence + ' : Please enter a valid Email-ID in CC');
          valid = false
        }
      }
      if (template.subject == null || template.subject.trim().length == 0) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'Mail ' + sequence + ' : You cannot send an E-mail without a Subject. Please enter Subject');
        valid = false
      }
      if (template.content == null || template.content.replace(/<[^>]+>/g, '').replace(new RegExp('&nbsp;', 'g'), '').trim().length == 0) {
        this.notificationService.addMessage(NotificationSeverity.WARNING, 'Mail ' + sequence + ' : You cannot send a blank E-mail. Please enter Content ');
        valid = false
      }
    }
    return valid;
  }

  emailValidator(email): boolean {
    let valid: boolean = true;
    var emailsIDs: Array<string> = email.split(',');
    for (let emailID of emailsIDs) {
      // let matches = emailID.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
      // if (matches == null) {
      //   valid = false;
      // }
      if (emailID.indexOf('@') == -1) {
        valid = false;
      } else {
        var parts = emailID.split('@');
        var domain = parts[1];
        if (domain.indexOf('.') == -1) {
          valid = false;
        } else {
          var domainParts = domain.split('.');
          var ext = domainParts[1];
          if (ext.length > 4 || ext.length < 2) {
            valid = false;
          }
        }
      }
    }
    return valid;
  }

  uploadDocumentListener(event: any, template: any) {
    let fileList: FileList = event.target.files;
    this.fileinProgress = true;
    let singleFileCapaictyExceeded = false;
    let supportedTypes = ['txt', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'rtf', 'msg', 'zip', 'rar', 'jpg', 'png', 'jpeg', 'bmp'];
    this.getAttachmentsTotalSize(template);
    this.notificationService.clear();
    if (fileList) {
      if (fileList.length > 0) {
        for (var i = 0; i < fileList.length; i++) {
          let file = fileList[i];

          let extenstion = file.name.split('.').pop();
          if (!supportedTypes.includes(extenstion.toLowerCase())) {
            this.notificationService.addMessage(NotificationSeverity.WARNING, 'Only ' + supportedTypes.toString() + ' files are allowed!');
            event.target.value = null;
            this.fileinProgress = false;
            return;
          }

          if (file.size > (8 * 1024 * 1024)) {
            singleFileCapaictyExceeded = true;
          }
          this.total_attachment_size += file.size;
        }

        if (this.total_attachment_size > (10 * 1024 * 1024)) {
          this.notificationService.setMessage(NotificationSeverity.WARNING, 'Total Size cannot be greater then 10 MB! ');
          event.target.value = null;
          this.fileinProgress = false;
          return;
        }        

        if (singleFileCapaictyExceeded) {
          this.notificationService.addMessage(NotificationSeverity.WARNING, 'File size can not be more than 8 MB');
          event.target.value = null;
          this.fileinProgress = false;
          return;
        }

        template.totalAttachedDocuments = 0;
        for (var i = 0; i < fileList.length; i++) {
          let file = fileList[i];
          let fr = new FileReader();
          fr.readAsDataURL(file);
          fr.onloadend = (e) => {
            this.attachedDocuments = new Attachdocuments();
            this.attachedDocuments.attachmentData_base64 = fr.result;
            this.attachedDocuments.attachmentName = file.name;
            this.attachedDocuments.attachmentSize = file.size;

            if (template.attachdocuments == null) {
              template.attachdocuments = new Array<Attachdocuments>();
            }
            template.attachdocuments.push(this.attachedDocuments);

            //called in loop because of delay by onloadend in async
            if( template.attachdocuments && template.attachdocuments.length != undefined ){
              template.totalAttachedDocuments = template.attachdocuments.length;
            }
          }
        }
        
      }
    }
    event.target.value = null;
    this.fileinProgress = false;
  }


  deleteDocumentListener(file: Attachdocuments, template: any) {

    for (var i = 0; i < template.attachdocuments.length; i++) {
      if (file == template.attachdocuments[i]) {
        this.total_attachment_size = this.total_attachment_size - template.attachdocuments[i].attachmentSize;
        template.attachdocuments = template.attachdocuments.filter(item => item !== file);
        template.totalAttachedDocuments = template.attachdocuments.length;
        break;
      }
    }
  }


  getAttachmentsTotalSize(template: any){
    this.total_attachment_size = 0;
    if( template && template.attachdocuments && template.attachdocuments.length != undefined ){
      for (var i = 0; i < template.attachdocuments.length; i++) {
        this.total_attachment_size += template.attachdocuments[i].attachmentSize;
      }
    }    
  }


}

export class Attachdocuments {
  attachmentData_base64: string | ArrayBuffer;
  attachmentName: string;
  attachmentSize: number;
}