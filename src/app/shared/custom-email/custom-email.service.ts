import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SessionService } from "../../session.service";
import { AltOneEmailTO } from 'app/common/AltOneEmailTO.model';

@Injectable()
export class CustomEmailService {

    constructor(private http: Http, private sessionService: SessionService) { }

    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'POST');
        headers.append('Access-Control-Allow-Origin', '*');
        return headers;
    }

    public sendHiringEmails(requestObject){
        let options = new RequestOptions({ headers: this.getHeaders() });
           return this.http.post(this.sessionService.orgUrl + '/rest/altone/sendEmail/hiring/', JSON.stringify(requestObject), options).
               map(res => {
                   this.sessionService.check401Response(res.json());
                   return res.json()
               });
     }
}