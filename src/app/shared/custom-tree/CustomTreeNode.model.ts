export class CustomTreeNode {
    label: String;
    level: String;
    value: any;
    selected: boolean = false;
    children: Array<CustomTreeNode>;
    parent: CustomTreeNode;
    filterApplicable: boolean;

    constructor(sourceData?: CustomTreeNode, nullifyParent?:boolean) {
        if (sourceData != null && sourceData != undefined) {
            this.label = sourceData.label;
            this.level = sourceData.level;
            this.value = sourceData.value;
            this.selected = sourceData.selected;
            this.children = new Array<CustomTreeNode>();
            this.parent = null;
            this.filterApplicable = sourceData.filterApplicable;

            for (let levelOne of sourceData.children) {
                let childOne = new CustomTreeNode();
                childOne.label = levelOne.label;
                childOne.level = levelOne.level;
                childOne.value = levelOne.value;
                childOne.selected = levelOne.selected;
                childOne.children = new Array<CustomTreeNode>();
                if(nullifyParent!=null && nullifyParent != undefined && nullifyParent==true) {
                    childOne.parent = null;
                } else {
                    childOne.parent = this;
                }
                childOne.filterApplicable = levelOne.filterApplicable;

                if (levelOne.children != null && levelOne.children != undefined) {
                    for (let levelTwo of levelOne.children) {
                        let childTwo = new CustomTreeNode();
                        childTwo.label = levelTwo.label;
                        childTwo.level = levelTwo.level;
                        childTwo.value = levelTwo.value;
                        childTwo.selected = levelTwo.selected;
                        childTwo.children = new Array<CustomTreeNode>();
                        if(nullifyParent!=null && nullifyParent != undefined && nullifyParent==true) {
                            childTwo.parent = null;
                        } else {
                            childTwo.parent = childOne;
                        }
                        childTwo.filterApplicable = levelTwo.filterApplicable;

                        if (levelTwo.children != null && levelTwo.children != undefined) {
                            let countlevelThree = 0;
                            for (let levelThree of levelTwo.children) {
                                let childThree = new CustomTreeNode();
                                childThree.label = levelThree.label;
                                childThree.level = levelThree.level;
                                childThree.value = levelThree.value;
                                childThree.selected = levelThree.selected;
                                childThree.children = null;
                                if(nullifyParent!=null && nullifyParent != undefined && nullifyParent==true) {
                                    childThree.parent = null;
                                } else {
                                    childThree.parent = childTwo;
                                }
                                childThree.filterApplicable = levelThree.filterApplicable;
                                childTwo.children.push(childThree);
                            }
                        }
                        childOne.children.push(childTwo);
                    }
                }
                this.children.push(childOne);
            }
        }
    }
}