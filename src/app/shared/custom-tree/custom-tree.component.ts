import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomTreeNode } from './CustomTreeNode.model';
import { CustomTreeService } from './custom-tree.service';

@Component({
  selector: 'alt-custom-tree',
  templateUrl: './custom-tree.component.html',
  styleUrls: ['./custom-tree.component.css'],
  providers: [CustomTreeService]
})
export class CustomTreeComponent implements OnInit {

  sourceData: CustomTreeNode;

  @Input() set source(value: CustomTreeNode) {
    this.setParent(value);
    this.sourceData = value;
  }

  @Output() selected = new EventEmitter<any>();

  @Input() placeholder: String;

  hiddenToggle: boolean;

  divClass: string;

  constructor() {
    this.divClass = "";
  }

  ngOnInit() { }

  checkboxListener(selectedSource: CustomTreeNode) {
    if (selectedSource.selected == true) {
      this.checkboxListenerOnSelect(selectedSource);
    } else {
      this.checkboxListenerOnUnselect(selectedSource);
    }
    this.selected.emit(new CustomTreeNode(this.sourceData, true));
  }

  checkboxListenerOnSelect(selectedSource: CustomTreeNode) {
    selectedSource.filterApplicable = true;
    if (selectedSource.children != undefined && selectedSource.children != null) {
      for (let childOne of selectedSource.children) {
        childOne.selected = true;
        if (childOne.children != undefined && childOne.children != null) {
          for (let childTwo of childOne.children) {
            childTwo.selected = true;
            if (childTwo.children != undefined && childTwo.children != null) {
              for (let childThree of childTwo.children) {
                childThree.selected = true;
              }
            }
          }
        }
      }
    }

    if (selectedSource.parent != undefined && selectedSource.parent != null) {
      selectedSource.parent.selected = true;
      if (selectedSource.parent.parent != null) {
        selectedSource.parent.parent.selected = true;
      }
    }

    let allChecked = true;
    if (selectedSource.parent != undefined && selectedSource.parent != null) {

      for (let child of selectedSource.parent.children) {
        if (child.selected == false || child.filterApplicable == false) {
          allChecked = false
        }
      }
      if (allChecked) {
        selectedSource.parent.filterApplicable = true;
      } else {
        selectedSource.parent.filterApplicable = false;
      }

      allChecked = true;
      if (selectedSource.parent.parent != null) {
        for (let child of selectedSource.parent.parent.children) {
          if (child.selected == false || child.filterApplicable == false) {
            allChecked = false
          }
        }
        if (allChecked) {
          selectedSource.parent.parent.filterApplicable = true;
        } else {
          selectedSource.parent.parent.filterApplicable = false;
        }
      }
    }
  }

  checkboxListenerOnUnselect(selectedSource:CustomTreeNode) {
    selectedSource.filterApplicable = false;
      if (selectedSource.parent != null && selectedSource.parent != undefined) {
        selectedSource.parent.filterApplicable = false;
        if (selectedSource.parent.parent != null && selectedSource.parent.parent != undefined) {
          selectedSource.parent.parent.filterApplicable = false;
        }
      }

      if (selectedSource.children != undefined && selectedSource.children != null) {
        for (let childOne of selectedSource.children) {
          childOne.selected = false;
          if (childOne.children != undefined && childOne.children != null) {
            for (let childTwo of childOne.children) {
              childTwo.selected = false;
              if (childTwo.children != undefined && childTwo.children != null) {
                for (let childThree of childTwo.children) {
                  childThree.selected = false;
                }
              }
            }
          }
        }
      }

      let allUnchecked = true;
      if (selectedSource.parent != undefined && selectedSource.parent != null) {

        for (let child of selectedSource.parent.children) {
          if (child.selected == true) {
            allUnchecked = false
          }
        }
        if (allUnchecked) {
          selectedSource.parent.selected = false;
        } else {
          selectedSource.parent.selected = true;
        }

        allUnchecked = true;
        if (selectedSource.parent.parent != null) {
          for (let child of selectedSource.parent.parent.children) {
            if (child.selected == true) {
              allUnchecked = false
            }
          }
          if (allUnchecked) {
            selectedSource.parent.parent.selected = false;
          } else {
            selectedSource.parent.parent.selected = true;
          }
        }
      }
  }

  resetFilters() {
    if (this.sourceData.children != undefined && this.sourceData.children != null) {
      for (let childOne of this.sourceData.children) {
        childOne.selected = false;
        childOne.filterApplicable = false;
        if (childOne.children != undefined && childOne.children != null) {
          for (let childTwo of childOne.children) {
            childTwo.selected = false;
            childTwo.filterApplicable = false;
            if (childTwo.children != undefined && childTwo.children != null) {
              for (let childThree of childTwo.children) {
                childThree.selected = false;
                childThree.filterApplicable = false;
              }
            }
          }
        }
      }
    }
  }

  toggle() {
    this.hiddenToggle = !this.hiddenToggle;
    if (this.hiddenToggle) {
      this.divClass = "open";
    } else {
      this.divClass = "";
    }
  }

  toggleClose() {
    this.hiddenToggle = false;
    this.divClass = "";
  }

  setParent(value: CustomTreeNode) {
    if (value != null && value.children != null && value.children != undefined) {
      for (let levelOne of value.children) {
        levelOne.parent = value;
        if (levelOne.children != null && levelOne.children != undefined) {
          for (let levelTwo of levelOne.children) {
            levelTwo.parent = levelOne;
            if (levelTwo.children != null && levelTwo.children != undefined) {
              for (let levelThree of levelTwo.children) {
                levelThree.parent = levelTwo;
              }
            }
          }
        }
      }
    }
  }
}
