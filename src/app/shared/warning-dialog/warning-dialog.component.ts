import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogService } from '../dialog.service';

@Component({
  selector: 'alt-warning-dialog',
  templateUrl: './warning-dialog.component.html',
  styleUrls: ['./warning-dialog.component.css']
})
export class WarningDialogComponent implements OnInit {

  message: string;
  isWarningORConfirmation:boolean;

  constructor(public dialogRef: MatDialogRef<WarningDialogComponent>, private dialogService: DialogService) {
    this.message = "";
    this.isWarningORConfirmation=false;
    this.dialogRef.disableClose=true;
   }

  ngOnInit() {
    if (this.dialogService.getDialogData() != null) {
      this.message=this.dialogService.getDialogData();
    }
    if (this.dialogService.getDialogData1() != null) {
      this.isWarningORConfirmation=this.dialogService.getDialogData1();
    }
  }

}
