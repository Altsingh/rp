import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AltSelectComponent} from './alt-select.component';
import {SelectDropdownComponent} from './select-dropdown.component';

export * from './option.interface';
export * from './alt-select.component';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { MatTooltipModule } from '@angular/material';



@NgModule({
    declarations: [
        AltSelectComponent,
        SelectDropdownComponent
    ],
    exports: [
        AltSelectComponent
    ],
    imports: [
        CommonModule, NgSlimScrollModule,
        FormsModule,MatTooltipModule
    ]
})
export class AltSelectModule {}
