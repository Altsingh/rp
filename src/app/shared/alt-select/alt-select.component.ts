import { Component, Input, OnChanges, OnInit, Output, ElementRef, EventEmitter, ExistingProvider, ViewChild, ViewEncapsulation, forwardRef, NgZone } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { SelectDropdownComponent } from './select-dropdown.component';
import { IOption } from './option.interface';
import { Option } from './option';
import { OptionList } from './option-list';

export const SELECT_VALUE_ACCESSOR: ExistingProvider = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AltSelectComponent),
    multi: true
};

@Component({
    selector: 'alt-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss'],
    providers: [SELECT_VALUE_ACCESSOR],
    encapsulation: ViewEncapsulation.None
})

export class AltSelectComponent implements ControlValueAccessor, OnChanges, OnInit {

    @Input() options: Array<IOption>;

    @Input() allowClear: boolean = false;
    @Input() disabled: boolean = false;
    @Input() highlightColor: string;
    @Input() highlightTextColor: string;
    @Input() multiple: boolean = false;
    @Input() noFilter: number = 0;
    @Input() isDefaultSelected: boolean = false;
    @Input() notFoundMsg: string = 'No results found';
    @Input() placeholder: string = '';
    @Input() filterPlaceholder: string = '';
    @Input() label: string = '';
    @Input() showIcon: boolean = false;
    @Input() set clearAll(clear: boolean){
        if(clear){
            this.clear();
        }
    }

    @Input() showSelection: boolean = true;

    @Output() opened: EventEmitter<null> = new EventEmitter<null>();
    @Output() closed: EventEmitter<null> = new EventEmitter<null>();
    @Output() selected: EventEmitter<IOption> = new EventEmitter<IOption>();
    @Output() deselected: EventEmitter<IOption | IOption[]> =
        new EventEmitter<IOption | IOption[]>();
    @Output() noOptionsFound: EventEmitter<string> =
        new EventEmitter<string>();

    @ViewChild('selection') selectionSpan: any;
    @ViewChild('dropdown') dropdown: SelectDropdownComponent;
    @ViewChild('filterInput') filterInput: any;
    @Output() viewHeight = new EventEmitter<number>();
    @ViewChild('dropdownbtn') dropdownbtn: ElementRef;
    @ViewChild('dropdownposition') dropdownposition: ElementRef;

    private _value: Array<any> = [];
    private optionList: OptionList;


    eventOptions: boolean | { capture?: boolean, passive?: boolean };
    alreadyTested: boolean = false;
    passiveSupportedd: boolean = false;
    reverseClassVisibility: boolean = false;
    // Selection state variables.
    hasSelected: boolean = false;

    // View state variables.
    hasFocus: boolean = false;
    isOpen: boolean = false;
    isBelow: boolean = true;
    private filterEnabled: boolean = true;
    private filterInputWidth: number = 1;
    private isDisabled: boolean = false;
    private placeholderView: string = '';

    private clearClicked: boolean = false;
    private selectContainerClicked: boolean = false;
    dropdownContWidth: any;

    // Width and position for the dropdown container.
    private width: number;
    private top: number;
    private left: number;

    private onChange = (_: any) => { };
    private onTouched = () => { };

    /** Event handlers. **/

    // Angular lifecycle hooks.

    //Zone for scroll
    constructor(private ngZone: NgZone) { }

    ngOnInit() {
        //scroll check
        if (this.passiveSupported()) {
            this.eventOptions = {
                capture: true,
                passive: true
            };
        } else {
            this.eventOptions = true;
        }
        this.ngZone.runOutsideAngular(() => {
            window.addEventListener('scroll', this.scroll, <any>this.eventOptions);
        });

        this.placeholderView = this.placeholder;

        if( this.isDefaultSelected ){
            this.select(this.optionList.getFirstShown().value);
        }

    }
    // scroll
    passiveSupported() {
        if (this.alreadyTested) {
            return this.passiveSupported;
        } else {
            this.alreadyTested = true;

            // Test via a getter in the options object to see if the passive property is accessed
            try {
                var opts = Object.defineProperty({}, 'passive', {
                    get: function () {
                        this.passiveSupported = true;
                    }
                });
                window.addEventListener('test', null, opts);
            } catch (e) { }
            return this.passiveSupported;
        }
    }

    ngOnChanges(changes: any) {
        if (changes.hasOwnProperty('options')) {
            this.updateOptionsList(changes['options'].isFirstChange());
        }
        if (changes.hasOwnProperty('noFilter')) {
            let numOptions: number = this.optionList.options.length;
            let minNumOptions: number = changes['noFilter'].currentValue;
            this.filterEnabled = numOptions >= minNumOptions;
        }
    }

    // Window.

    onWindowClick() {
        if (!this.selectContainerClicked) {
            this.closeDropdown();
        }
        this.clearClicked = false;
        this.selectContainerClicked = false;



    }

    onWindowResize() {
        this.updateWidth();
    }

    // Select container.

    onSelectContainerClick(event: any) {
        this.selectContainerClicked = true;
        if (!this.clearClicked) {
            this.toggleDropdown();
        }
    }

    onSelectContainerFocus() {
        this.onTouched();
    }

    onSelectContainerKeydown(event: any) {
        this.handleSelectContainerKeydown(event);
    }

    // Dropdown container.

    onDropdownOptionClicked(option: Option) {
        this.multiple ? this.toggleSelectOption(option) : this.selectOption(option);
        this.selectContainerClicked = false;
    }

    onDropdownClose(focus: any) {
        this.closeDropdown(focus);
    }

    scrollEventsCapture(event) {
        this.selectContainerClicked = true;
    }

    // Single filter input.

    onSingleFilterClick() {
        this.selectContainerClicked = true;
    }

    onSingleFilterInput(term: string) {
        let hasShown: boolean = this.optionList.filter(term);
        if (!hasShown) {
            this.noOptionsFound.emit(term);
        }
    }
    getHeight(event) {
        this.viewHeight.emit(event + 150);
    }
    onSingleFilterKeydown(event: any) {
        this.handleSingleFilterKeydown(event);
    }

    // Multiple filter input.

    onMultipleFilterInput(event: any) {
        if (!this.isOpen) {
            this.openDropdown();
        }
        this.updateFilterWidth();
        setTimeout(() => {
            let term: string = event.target.value;
            let hasShown: boolean = this.optionList.filter(term);
            if (!hasShown) {
                this.noOptionsFound.emit(term);
            }
        });
    }

    onMultipleFilterKeydown(event: any) {
        this.handleMultipleFilterKeydown(event);
    }

    // Single clear select.

    onClearSelectionClick(event: any) {
        this.clearClicked = true;
        this.clearSelection();
        this.closeDropdown(true);
    }

    // Multiple deselect option.

    onDeselectOptionClick(option: Option) {
        this.clearClicked = true;
        this.deselectOption(option);
    }

    /** API. **/

    // TODO fix issues with global click/key handler that closes the dropdown.
    open() {
        this.openDropdown();
    }

    close() {
        this.closeDropdown();
    }

    clear() {
        this.clearSelection();
    }

    select(value: string) {
        if (value != undefined) {
            this.optionList.getOptionsByValue(value).forEach((option) => {
                this.selectOption(option);
            });
        }
    }

    /** ControlValueAccessor interface methods. **/

    writeValue(value: any) {
        if (value != undefined)
            this.value = value;
    }

    registerOnChange(fn: (_: any) => void) {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void) {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean) {
        this.disabled = isDisabled;
    }

    /** Value. **/

    get value(): string | string[] {
        return this.multiple ? this._value : this._value[0];
    }

    set value(v: string | string[]) {
        if (typeof v === 'undefined' || v === null || v === '') {
            v = [];
        }
        else if (typeof v === 'string') {
            v = [v];
        }
        else if (!Array.isArray(v)) {
            throw new TypeError('Value must be a string or an array.');
        }

        if (!OptionList.equalValues(v, this._value)) {
            this.optionList.value = v;
            this.valueChanged();
        }
    }

    private valueChanged() {
        this._value = this.optionList.value;

        this.hasSelected = this._value.length > 0;
        this.placeholderView = this.hasSelected ? '' : this.placeholder;
        this.updateFilterWidth();

        this.onChange(this.value);
    }

    /** Initialization. **/

    private updateOptionsList(firstTime: boolean) {
        let v: Array<string>;

        if (!firstTime) {
            v = this.optionList.value;
        }

        this.optionList = new OptionList(this.options);

        if (!firstTime) {
            this.optionList.value = v;
            this.valueChanged();
        }
    }

   

    /** Dropdown. **/

    private toggleDropdown() {
        if (!this.isDisabled) {
            this.isOpen ? this.closeDropdown(true) : this.openDropdown();
        }
    }

    private openDropdown() {
        if (!this.isOpen) {
            this.updateWidth();
            this.updatePosition();
            this.isOpen = true;
            if (this.multiple && this.filterEnabled) {
                this.filterInput.nativeElement.focus();
            }
            this.opened.emit(null);
        }
    }

    private closeDropdown(focus: boolean = false) {
        if (this.isOpen) {
            this.clearFilterInput();
            this.isOpen = false;
            if (focus) {
                this.focus();
            }
            this.closed.emit(null);
        }
    }
    //toggle
    toggleClose() {
        this.isOpen = false;
      }
      
    /** Select. **/

    private selectOption(option: Option) {
        if (!option.selected) {
            this.optionList.select(option, this.multiple);
            this.valueChanged();
            this.selected.emit(option.wrappedOption);
        }
    }

    private deselectOption(option: Option) {
        if (option.selected) {
            this.optionList.deselect(option);
            this.valueChanged();
            this.deselected.emit(option.wrappedOption);
            setTimeout(() => {
                if (this.multiple) {
                    this.updatePosition();
                    this.optionList.highlight();
                    if (this.isOpen) {
                        this.dropdown.moveHighlightedIntoView();
                    }
                }
            });
        }
    }

    private clearSelection() {
        let selection: Array<Option> = this.optionList.selection;
        if (selection.length > 0) {
            this.optionList.clearSelection();
            this.valueChanged();

            if (selection.length === 1) {
                this.deselected.emit(selection[0].wrappedOption);
            }
            else {
                this.deselected.emit(selection.map((option) => {
                    return option.wrappedOption;
                }));
            }
        }
    }

    private toggleSelectOption(option: Option) {
        option.selected ?
            this.deselectOption(option) : this.selectOption(option);
    }

    private selectHighlightedOption() {
        let option: Option = this.optionList.highlightedOption;
        if (option !== null) {
            this.selectOption(option);
            this.closeDropdown(true);
        }
    }

    private deselectLast() {
        let sel: Array<Option> = this.optionList.selection;

        if (sel.length > 0) {
            let option: Option = sel[sel.length - 1];
            this.deselectOption(option);
            this.setMultipleFilterInput(option.label + ' ');
        }
    }

    /** Filter. **/

    private clearFilterInput() {
        if (this.multiple && this.filterEnabled) {
            this.filterInput.nativeElement.value = '';
        }
        else {
            this.dropdown.clearFilterInput();
        }
    }

    private setMultipleFilterInput(value: string) {
        if (this.filterEnabled) {
            this.filterInput.nativeElement.value = value;
        }
    }

    /** Keys. **/

    private KEYS: any = {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        ESC: 27,
        SPACE: 32,
        UP: 38,
        DOWN: 40
    };

    private handleSelectContainerKeydown(event: any) {
        let key = event.which;

        if (this.isOpen) {
            if (key === this.KEYS.ESC ||
                (key === this.KEYS.UP && event.altKey)) {
                this.closeDropdown(true);
            }
            else if (key === this.KEYS.TAB) {
                this.closeDropdown();
            }
            else if (key === this.KEYS.ENTER) {
                this.selectHighlightedOption();
            }
            else if (key === this.KEYS.UP) {
                this.optionList.highlightPreviousOption();
                this.dropdown.moveHighlightedIntoView();
                if (!this.filterEnabled) {
                    event.preventDefault();
                }
            }
            else if (key === this.KEYS.DOWN) {
                this.optionList.highlightNextOption();
                this.dropdown.moveHighlightedIntoView();
                if (!this.filterEnabled) {
                    event.preventDefault();
                }
            }
        }
        else {
            if (key === this.KEYS.ENTER || key === this.KEYS.SPACE ||
                (key === this.KEYS.DOWN && event.altKey)) {

                /* FIREFOX HACK:
                 *
                 * The setTimeout is added to prevent the enter keydown event
                 * to be triggered for the filter input field, which causes
                 * the dropdown to be closed again.
                 */
                setTimeout(() => { this.openDropdown(); });
            }
        }

    }

    private handleMultipleFilterKeydown(event: any) {
        let key = event.which;

        if (key === this.KEYS.BACKSPACE) {
            if (this.hasSelected && this.filterEnabled &&
                this.filterInput.nativeElement.value === '') {
                this.deselectLast();
            }
        }
    }

    private handleSingleFilterKeydown(event: any) {
        let key = event.which;

        if (key === this.KEYS.ESC || key === this.KEYS.TAB
            || key === this.KEYS.UP || key === this.KEYS.DOWN
            || key === this.KEYS.ENTER) {
            this.handleSelectContainerKeydown(event);
        }
    }

    /** View. **/

    focus() {
        this.hasFocus = true;
        if (this.multiple && this.filterEnabled) {
            this.filterInput.nativeElement.focus();
        }
        else {
            this.selectionSpan.nativeElement.focus();
        }
    }

    blur() {
        this.hasFocus = false;
        this.selectionSpan.nativeElement.blur();
    }

    private updateWidth() {
        this.width = this.selectionSpan.nativeElement.offsetWidth;
    }

    private updatePosition() {
        let e = this.selectionSpan.nativeElement;
        this.left = e.offsetLeft;
        this.top = e.offsetTop + e.offsetHeight;
    }

    private updateFilterWidth() {
        if (typeof this.filterInput !== 'undefined') {
            let value: string = this.filterInput.nativeElement.value;
            this.filterInputWidth = value.length === 0 ?
                1 + this.placeholderView.length * 10 : 1 + value.length * 10;
        }
    }
    trimString(value, maxLength) {
        let trimed: string = value;
        if (value !== undefined && trimed !== null && trimed.length >= maxLength) {
            trimed = trimed.substring(0, maxLength) + '...';
        }
        return trimed;
    }
    scroll = (): void => {
        this.splitDropdown();
    };

    adjustToolTip(){
        this.dropdownContWidth = this.dropdownbtn.nativeElement.offsetWidth - 20;
      }
    //Dropdonw
    splitDropdown() {
        if (!(this.dropdownbtn == undefined || this.dropdownbtn == null || !this.dropdownbtn || this.dropdownposition == undefined || this.dropdownposition == null || !this.dropdownposition)) {
            let y = this.dropdownbtn.nativeElement;
            let x = this.dropdownposition.nativeElement;
            let doc = document.documentElement;
            let scrolltop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

            let viewportOffset = y.getBoundingClientRect();
            let topOffset = viewportOffset.top;
            let bottomTemp = viewportOffset.bottom;
            var windHeight = window.innerHeight;
            let bottomOffset = windHeight - bottomTemp;

            let dropdownHeight = 0;
            if (!this.multiple && this.filterEnabled) {
                dropdownHeight += 80;
            }
            if (!(typeof this.optionList.filtered.length == undefined || this.optionList.filtered == undefined || this.optionList.filtered.length == undefined)) {
                dropdownHeight += (this.optionList.filtered.length * 40);
            }
            if (!(typeof this.optionList.selection.length == undefined || this.optionList.selection == undefined || this.optionList.selection.length == undefined)) {
                bottomOffset += (this.optionList.selection.length * 15);
            }
            //dropdownHeight += (this.optionList.filtered.length * 40);
            //bottomOffset += (this.optionList.selection.length * 15);
            if (dropdownHeight >= 280) {
                dropdownHeight = 280;
            }
            if (bottomOffset < dropdownHeight) {
                this.reverseClassVisibility = true;
            } else {
                this.reverseClassVisibility = false;
            }
        }
    }
}
