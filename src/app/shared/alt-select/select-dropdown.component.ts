import { NgSlimScrollModule, SlimScrollEvent } from 'ngx-slimscroll';
import {
    AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    ViewChild,
    ViewEncapsulation, ElementRef, HostListener
} from '@angular/core';


import { Option } from './option';
import { OptionList } from './option-list';
import { OnDestroy, AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'select-dropdown',
    templateUrl: './select-dropdown.component.html',
    styleUrls: ['./select-dropdown.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class SelectDropdownComponent
    implements AfterViewInit, OnChanges, OnInit {
        

    @Input() filterEnabled: boolean;
    @Input() highlightColor: string;
    @Input() highlightTextColor: string;
    @Input() left: number;
    @Input() multiple: boolean;
    @Input() notFoundMsg: string;
    @Input() optionList: OptionList;
    @Input() dropdownContWidth: any;
    @Input() top: number;
    @Input() width: number;
    @Input() placeholder: string;
    @Input() showIcon: boolean;

    @Output() close = new EventEmitter<boolean>();
    @Output() optionClicked = new EventEmitter<Option>();
    @Output() singleFilterClick = new EventEmitter<null>();
    @Output() singleFilterInput = new EventEmitter<string>();
    @Output() singleFilterKeydown = new EventEmitter<any>();

    @ViewChild('filterInput') filterInput: any;
    @ViewChild('optionsList') optionsList: any;

    @Output() scrollEvents = new EventEmitter<SlimScrollEvent>();

    disabledColor: string = '#fff';
    disabledTextColor: string = '9e9e9e';
    @Output() viewHeight = new EventEmitter<number>();
    /** Event handlers. **/

    // Angular life cycle hooks.

    ngOnInit() {
        this.optionsReset();
    }

    ngOnChanges(changes: any) {

        if (changes.hasOwnProperty('optionList')) {
            this.optionsReset();
        }
    }

    ngAfterViewInit() {
        this.moveHighlightedIntoView();
        if (!this.multiple && this.filterEnabled) {
            this.filterInput.nativeElement.focus();
        }
    }
    // Filter input (single select).

    onSingleFilterClick(event: any) {
        this.singleFilterClick.emit(null);
    }

    onSingleFilterInput(event: any) {
        this.singleFilterInput.emit(event.target.value);
    }

    onSingleFilterKeydown(event: any) {
        this.singleFilterKeydown.emit(event);
    }

    // Parent scroll stop on the child Mouse hover
    @HostListener('window:mousewheel', ['$event']) 
    onMousewheel(event:any){
        event.stopPropagation();
    }

    @HostListener('window:mousewheel', ['$event']) 
    onDOMMouseScroll(event:any){
        event.stopPropagation();
    }

    onMouseUp(event: any){
        this.scrollEvents.emit(event);
    }

    // Options list.

    onOptionsWheel(event: any) {
        this.handleOptionsWheel(event);
    }

    onOptionMouseover(option: Option) {
        this.optionList.highlightOption(option);
    }

    onOptionClick(option: Option) {
        this.optionClicked.emit(option);
    }

   /** Initialization. **/

    private optionsReset() {
        this.optionList.filter('');
        this.optionList.highlight();
    }

    /** View. **/

    getOptionStyle(option: Option): any {
        if (option.highlighted) {
            let style: any = {};

            if (typeof this.highlightColor !== 'undefined') {
                style['background-color'] = this.highlightColor;
            }
            if (typeof this.highlightTextColor !== 'undefined') {
                style['color'] = this.highlightTextColor;
            }
            return style;
        }
        else {
            return {};
        }
    }

    clearFilterInput() {
        if (this.filterEnabled) {
            this.filterInput.nativeElement.value = '';
        }
    }

    moveHighlightedIntoView() {
        let list = this.optionsList.nativeElement;
        let listHeight = list.offsetHeight;
        this.viewHeight.emit(listHeight);
        let itemIndex = this.optionList.getHighlightedIndex();

        if (itemIndex > -1) {
            let item = list.children[0].children[itemIndex];
            let itemHeight = item.offsetHeight;
            let itemTop = itemIndex * itemHeight;
            let itemBottom = itemTop + itemHeight;

            let viewTop = list.scrollTop;
            let viewBottom = viewTop + listHeight;

            if (itemBottom > viewBottom) {
                list.scrollTop = itemBottom - listHeight;
            }
            else if (itemTop < viewTop) {
                list.scrollTop = itemTop;
            }
        }
    }

    private handleOptionsWheel(e: any) {
        let div = this.optionsList.nativeElement;
        let atTop = div.scrollTop === 0;
        let atBottom = div.offsetHeight + div.scrollTop === div.scrollHeight;

        if (atTop && e.deltaY < 0) {
            e.preventDefault();
        }
        else if (atBottom && e.deltaY > 0) {
            e.preventDefault();
        }
    }

    getJobStatusClass(requisitionStatus: String) {
        if (requisitionStatus === 'OPEN') {
          return "job-active";
        } else if (requisitionStatus === 'CLOSED' || requisitionStatus === 'PCLOSE') {
          return "job-close";
        } else if (requisitionStatus === 'ONHOLD' || requisitionStatus === 'PHOLD') {
          return "job-hold";
        } else if (requisitionStatus === 'PLANNED' || requisitionStatus === 'Saved') {
          return "job-pending";
        } else if (requisitionStatus === 'CANCELLED' ) {
          return "job-cancelled";
        } else if (requisitionStatus === 'APPROVALPENDING') {
            return "job-approved-pending";
          } else if (requisitionStatus === 'WITHDRAWN') {
            return "job-withdrawn";
          } else if (requisitionStatus === 'REJECTED') {
            return "job-rejected";
          } 
      }

    
}
