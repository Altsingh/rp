import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatCandidatecodeParam'
})
export class FormatCandidatecodeParamPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (!value) { return value; }

    return value.replace(/\//g, '-');
  }

}
